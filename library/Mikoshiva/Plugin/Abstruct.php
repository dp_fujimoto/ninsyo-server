<?php
require_once 'Mikoshiva/Http/Client.php';
require_once 'Mikoshiva/Plugin/Interface.php';

/**
 * 外部API用基底クラス
 *
 * @author k.shimowaki
 * @since version - 2010/03/17
 */
abstract class Mikoshiva_Plugin_Abstruct implements Mikoshiva_Plugin_Interface {

	protected $client = null;

	/**
	 * コンストラクタ
	 *
	 * @param string $url
	 */
	public function __construct($url) {

		$config = array(
		'adapter' => 'Zend_Http_Client_Adapter_Socket',
		'ssltransport' => 'ssl'
		);
		$this->client = new Mikoshiva_Http_Client($url, $config);
    }

    protected function error($e) {

/*		// エラーメール送信
		$mail = new Mikoshiva_Mail();
		$mail->setMailTemplateByKey('MstMailTemplate', 'ERROR_PLUGIN');

		$mail->setBodyText(print_r($e->getTraceAsString(),true)."\n".print_r($this->client->getParameterRequest(),true)."\n".$this->client->getUri());
		$mail->addTo(MIKOSHIVA_ERROR_MAIL);

		$mail->send();
*/
        // 2011-02-08 谷口 エラーメールの表示の仕様変更によりコメントアウトし、引数の値をそのまま入れるように変更
		//$exception = new Exception(print_r($e->getTraceAsString(),true)."\n".print_r($this->client->getParameterRequest(),true)."\n".$this->client->getUri());
		//errorMail($exception,"外部連携エラー");
		errorMail($e,"外部連携エラー:".$this->client->getUri().":",true);
    }

	/**
	 * setter
	 *
	 * @param $client
	 * @since 2010/03/17
	 * @version SVN:$Id:
	 **/
	public function setClient($client) {
		$this->client = $client;
	}

	/**
	 * getter
	 *
	 * @return $client
	 * @since 2010/03/17
	 * @version SVN:$Id:
	 **/
	public function getClient() {
		return $this->client;
	}
}