<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CreateUserHash.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Content_GetContentData extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $productCode = "'" . str_replace('-',"','",$actionForm->getProductCode()) . "'";

        //-----------------------------------------------
        // コンテンツ条件情報取得
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT *";
        $sql .= " FROM mst_condition";
        $sql .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mcd_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND mpr_product_code IN (" . $productCode . ")";
        $sql .= "   AND mct_application_type = ?";

        $params   = array();
        $params[] = 'APPLICATION_TYPE_WEB_PAGE_AUTH';

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $data = array();
        $contentCount = array();
        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            if (!isset($data[$v['mct_content_id']])) {
                $data[$v['mct_content_id']] = $v['mct_content_id'] . '|++|';
                $data[$v['mct_content_id']] .= '<table border="0"><tr><td style="padding:5px 15px;">ID:<span style="font-weight:bold;">' . $v['mct_content_id'] . '</span></td><td>コンテンツ名：' . $v['mct_content_name'] . '<br>';
                if (!isBlankOrNull($v['mct_memo'])) {
                    $data[$v['mct_content_id']] .= '<span style="color:gray;">' . $v['mct_memo'] . '</span><br>';
                }
                if ($v['mct_all_condition_flag'] === '1') {
                    $data[$v['mct_content_id']] .= '　・右記の全てに該当すれば権利あり<br>';
                } else {
                    $data[$v['mct_content_id']] .= '　・右記のどれかに該当すれば権利あり<br>';
                }
                if ($v['mct_available_check_flag'] === '1') {
                    $data[$v['mct_content_id']] .= '　・請求取消分・商品金額以上の返金がある分はサービス対象外とする<br>';
                } else {
                    //$data[$v['mct_content_id']] .= '　・なし<br>';
                }
                if ($v['mct_auth_flag'] === '1') {
                    $data[$v['mct_content_id']] .= '　・一度でも条件を満たした人は閲覧可能<br>';
                } else {
                    $data[$v['mct_content_id']] .= '　・サービス期間中のみ閲覧可能<br>';
                }
                $data[$v['mct_content_id']] .= '</td><td><table border="0" style="border-collapse:collapse;">';
                $contentCount[$v['mct_content_id']] = 1;
            } else {
                $contentCount[$v['mct_content_id']]++;
            }

            $data[$v['mct_content_id']] .= '<tr>';
            $data[$v['mct_content_id']] .= '<td style="color:gray;border:0px solid;padding:2px;">条件 ' . $contentCount[$v['mct_content_id']] . '</td>';

            $data[$v['mct_content_id']] .= '<td>';
            if ($v['mcd_having_flag'] === '1') {
                $data[$v['mct_content_id']] .= '指定日以前に購入';
            }
            if ($v['mcd_servise_in_flag'] === '1') {
                $data[$v['mct_content_id']] .= '指定日にサービスIN';
            }
            if ($v['mcd_subscription_flag'] === '1') {
                $data[$v['mct_content_id']] .= '購読中';
            }
            if ($v['mcd_bundle_flag'] === '1') {
                $data[$v['mct_content_id']] .= '一括購読中';
            }
            if ($v['mcd_purchase_flag'] === '1') {
                $data[$v['mct_content_id']] .= '購入したことがある';
            }
            if ($v['mcd_shipping_data_flag'] === '1') {
                $data[$v['mct_content_id']] .= '指定した発送物を持っている';
            }
            if ($v['mcd_none_condition_flag'] === '1') {
                $data[$v['mct_content_id']] .= '無条件に見れる';
            }
            $data[$v['mct_content_id']] .= '</td>';
            $data[$v['mct_content_id']] .= '<td><table border="0">';
            $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">サーバー名：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_server_name'] . '</td></tr>';
            if (!isBlankOrNull($v['mcd_product_code'])) {
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">商品コード：</td><td style="border:0px solid;padding:2px;color:blue;">' . $v['mcd_product_code'] . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_campaign_code'])) {
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">オーダーフォームコード：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_campaign_code'] . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_campaign_sales_code'])) {
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">キャンペーンコード：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_campaign_sales_code'] . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_product_shipping_code'])) {
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">商品発送コード：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_product_shipping_code'] . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_package_number'])) {
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">パッケージナンバー：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_package_number'] . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_subscription_type'])) {
                $subscriptionType = '';
                switch ($v['mcd_subscription_type']) {
                    case 'SUBSCRIPTION_TYPE_NORMAL': $subscriptionType = '通常'; break;
                    case 'SUBSCRIPTION_TYPE_YEAR': $subscriptionType = '年間'; break;
                    case 'SUBSCRIPTION_TYPE_YEAR_CONTINUITY': $subscriptionType = '年間追加'; break;
                    case 'SUBSCRIPTION_TYPE_MICRO_CONTINUITY_UPGRADE': $subscriptionType = 'マイクロアップグレード'; break;
                    case 'VIP': $subscriptionType = 'VIP'; break;
                }
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">購読タイプ：</td><td style="border:0px solid;padding:2px;">' . $subscriptionType . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_product_type'])) {
                $productType = '';
                switch ($v['mcd_product_type']) {
                    case 'PRODUCT_TYPE_SINGLE': $productType = '単発'; break;
                    case 'PRODUCT_TYPE_MICRO_CONTINUITY': $productType = 'マイクロコンティニュイティ'; break;
                    case 'PRODUCT_TYPE_CONTINUITY': $productType = 'コンティニュイティ'; break;
                }
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">商品タイプ：</td><td style="border:0px solid;padding:2px;">' . $productType . '</td></tr>';
            }
            if (!isBlankOrNull($v['mcd_charge_type'])) {
                $chargeType = '';
                switch ($v['mcd_charge_type']) {
                    case 'CHARGE_TYPE_CARD': $chargeType = 'カード'; break;
                    case 'CHARGE_TYPE_CASH_ON_DELIVERY': $chargeType = '代引き'; break;
                    case 'CHARGE_TYPE_BANK_TRANSFER': $chargeType = '銀行振込'; break;
                }
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">請求タイプ：</td><td style="border:0px solid;padding:2px;">' . $chargeType . '</td></tr>';
            }
            if ($v['mcd_having_flag'] === '1') {
                // '指定日以前に購入';
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">指定日：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_designated_date'] . '</td></tr>';
            }
            if ($v['mcd_servise_in_flag'] === '1') {
                // '指定日にサービスIN';
                $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">指定日：</td><td style="border:0px solid;padding:2px;">' . $v['mcd_designated_date'] . '</td></tr>';
            }
            if ($v['mcd_subscription_flag'] === '1') {
                // '購読中';
            }
            if ($v['mcd_bundle_flag'] === '1') {
                // '一括購読中';
            }
            if ($v['mcd_purchase_flag'] === '1') {
                // '購入したことがある';
                if ($v['mcd_cancel_ok_flag'] === '1') {
                    $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">解約した時：</td><td style="border:0px solid;padding:2px;">サービス継続' . '</td></tr>';
                } else {
                    $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">解約した時：</td><td style="border:0px solid;padding:2px;">サービス停止' . '</td></tr>';
                }
                if (!isBlankOrNull($v['mcd_permission_month']) || !isBlankOrNull($v['mcd_permission_day'])) {
                    $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">許可期間：</td><td style="border:0px solid;padding:2px;">';
                    if ($v['mcd_permission_month'] > 0) {
                        $data[$v['mct_content_id']] .= $v['mcd_permission_month'] . 'ヶ月';
                    }
                    if ($v['mcd_permission_day'] > 0) {
                        $data[$v['mct_content_id']] .= $v['mcd_permission_day'] . '日';
                    }
                    $data[$v['mct_content_id']] .= '</td></tr>';
                }
                if (!isBlankOrNull($v['mcd_need_period_month']) || !isBlankOrNull($v['mcd_need_period_day'])) {
                    $data[$v['mct_content_id']] .= '<tr><td style="color:gray;border:0px solid;padding:2px;">必要サービス期間：</td><td style="border:0px solid;padding:2px;">';
                    if ($v['mcd_need_period_month'] > 0) {
                        $data[$v['mct_content_id']] .= $v['mcd_need_period_month'] . 'ヶ月';
                    }
                    if ($v['mcd_need_period_day'] > 0) {
                        $data[$v['mct_content_id']] .= $v['mcd_need_period_day'] . '日';
                    }
                    $data[$v['mct_content_id']] .= '</td></tr>';
                }
            }

            $data[$v['mct_content_id']] .= '</table></td>';
            $data[$v['mct_content_id']] .= '</tr>';
        }

        foreach ($data as $k => $v) {
            $data[$k] = $v . '</table></td></tr></table>';
        }

        echo implode('|+++|',$data);

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
