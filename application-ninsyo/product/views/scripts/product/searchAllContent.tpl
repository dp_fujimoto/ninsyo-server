<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">対象商品のコンテンツ一覧</h2>

            {$rowCount = 0}

            {* 検索結果あり *}
            {if count($search) > 0}

                <!-- // 検索結果リスト開始 -->
                <table class="COM_table PDT_tableWidthBasic">
                    <tr>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">ロゴ画像</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">ブランド名</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">商品画像</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">商品名</td>
                    </tr>

                    {foreach from=$search key="k" item="v"}
                    <tr>
                        {foreach from=$v['data'] key="k2" item="v2"}
                        <td class="COM_nowrap " rowspan="{$v['info']['rowCount']}">
                            {if !isBlankOrNull($v['info']['mbr_logo_url'])}
                                <img src="{$v['info']['mbr_logo_url']}" width="{$smarty.const.BLAND_LOGO_W}px" height="{$smarty.const.BLAND_LOGO_H}px">
                            {else}なし
                            {/if}
                        </td>
                        <td class="COM_nowrap " rowspan="{$v['info']['rowCount']}">
                            {$v['info']['mbr_brand_name']}
                        </td>
                        <td class="COM_nowrap" title="{$v2['info']['mpr_description']}" rowspan="{$v2['info']['rowCount']}">
                            {if !isBlankOrNull($v2['info']['mpr_image_url'])}
                                <img src="{$v2['info']['mpr_image_url']}" width="{$smarty.const.PRODUCT_IMAGE_W}px" height="{$smarty.const.PRODUCT_IMAGE_H}px">
                            {else}なし
                            {/if}
                        </td>
                        <td class="COM_nowrap" title="{$v2['info']['mpr_description']}" rowspan="{$v2['info']['rowCount']}">
                            {$v2['info']['mpr_product_name']}
                        </td>
                        {/foreach}
                    </tr>
                    {/foreach}
                </table>
                <!-- // 検索結果リスト終了 -->

                <br>
                <hr>
                <br>

                <input type="button" value="　　コンテンツ画像表示OFF　　" id="chgButton" onClick="PDT_chgContetImageDisplay{$tabId}();">

                <!-- // 検索結果リスト開始 -->
                <table class="COM_table PDT_tableWidthBasic">
                    <tr>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap" id="th1">コンテンツ画像</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">コンテンツID</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">コンテンツ名</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">用途</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">タイプ</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">コンテンツURL</td>
                    </tr>

                    {assign var="lineCount" value='0'}
                    {foreach from=$search key="k" item="v"}
                        {foreach from=$v['data'] key="k2" item="v2"}
                            {foreach from=$v2['data'] key="k3" item="v3"}
                            {if $lineCount === '1'}
                                {assign var="lineColor" value="COM_bgColorLight"}
                                {assign var="lineCount" value='0'}
                            {else}
                                {assign var="lineColor" value=''}
                                {assign var="lineCount" value='1'}
                            {/if}
                            <tr>
                                <td class="COM_nowrap {$lineColor}" title="{$v3['mct_description']}" id="td1-{$rowCount}">
                                    {if !isBlankOrNull($v3['mct_image_url'])}
                                        <img src="{$v3['mct_image_url']}" width="{$smarty.const.CONTENT_IMAGE_W}px" height="{$smarty.const.CONTENT_IMAGE_H}px">
                                    {else}なし
                                    {/if}
                                </td>
                                <td class="COM_nowrap {$lineColor}" title="{$v3['mct_description']}">
                                    {$v3['mct_content_id']}
                                </td>
                                <td class="COM_nowrap {$lineColor}" title="{$v3['mct_description']}">
                                    {$v3['mct_content_name']}
                                </td>
                                <td class="COM_nowrap {$lineColor}">
                                    {statusMapping code=$v3['mct_application_type']}
                                </td>
                                <td class="COM_nowrap {$lineColor}">
                                    {$v3['mct_media_type']|strtoupper}
                                </td>
                                <td class="COM_nowrap {$lineColor}">
                                    <a href="{$v3['mct_media_url']}" target="_blank">{$v3['mct_media_url']}</a>
                                </td>
                            </tr>
                            {$rowCount = $rowCount + 1}
                            {/foreach}
                        {/foreach}
                    {/foreach}
                </table>
                <!-- // 検索結果リスト終了 -->

            {else}

                <p>該当する検索結果はありません。</p>

            {/if}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 表示切替
    //-----------------------------------------------
    function PDT_chgContetImageDisplay{$tabId}(){

        var flag = 0;
        var obj = document.getElementById('th1');

        if (obj) {
            var className = obj.className;
            if (className == 'COM_bgColorLight COM_bold COM_center COM_nowrap COM_displayNone') {
                obj.className = 'COM_bgColorLight COM_bold COM_center COM_nowrap';
                document.getElementById('chgButton').value = '　　コンテンツ画像表示OFF　　';
            } else {
                obj.className = 'COM_bgColorLight COM_bold COM_center COM_nowrap COM_displayNone';
                document.getElementById('chgButton').value = '　　コンテンツ画像表示ON　　';
                flag = 1;
            }
        }

        if (flag == 0) {
            for (var i = 0;i <= {$rowCount};i++) {
                obj = document.getElementById('td1-' + i);
                if (obj) {
                    obj.className = 'COM_nowrap';
                }
            }
        } else {
            for (var i = 0;i <= {$rowCount};i++) {
                obj = document.getElementById('td1-' + i);
                if (obj) {
                    obj.className = 'COM_nowrap COM_displayNone';
                }
            }
        }
    }

</script>
<!-- // カレントJavascript終了 -->