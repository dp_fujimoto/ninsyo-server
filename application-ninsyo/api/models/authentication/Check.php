<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Authentication_Check extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 結果
        $result = null;

        $log = '';
        $log .= 'appName:' . $actionForm->getAppName();
        $log .= '|protocolVersion:' . $actionForm->getProtocolVersion();
        $log .= '|appId:' . $actionForm->getAppId();
        $log .= '|appKey:' . $actionForm->getAppKey();

        $config = $context->getConfig();
        $ninsyo_app_id_list = $config['NINSYO_APP_ID_LIST'];

        if (!isBlankOrNull($actionForm->getProtocolVersion()) && !isBlankOrNull($actionForm->getAppId()) && !isBlankOrNull($actionForm->getAppKey())) {
            if (isset($ninsyo_app_id_list[$actionForm->getProtocolVersion()][$actionForm->getAppId()])) {
                if ($ninsyo_app_id_list[$actionForm->getProtocolVersion()][$actionForm->getAppId()] === $actionForm->getAppKey()) {

                    // ログイン成功した情報を記録
                    logTemporary($log,'authentication-ok');

                    // セッションを生成
                    $session = new Zend_Session_Namespace('app');

                    // 60秒
                    $session->setExpirationSeconds(60);

                    $session->clientIp = getenv('REMOTE_ADDR'); // クライアントIP
                    $session->timestamp = time();               // 現在時間

                    // STAFF を読み取り専用にする
                    $session->lock();

                    $result = session_id();
                }
            }
        }

        if ($result === null) {
            // ログイン失敗した情報を記録
            logTemporary($log,'authentication-ng');
        }


        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $result;
    }
}
