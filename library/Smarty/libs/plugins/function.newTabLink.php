<?php
require_once 'Mikoshiva/View/Helper/Exception.php';

require_once 'Mikoshiva/Logger.php';
//function.newTab.phpが必須

 /**
 * 新しいタブを開くjavascriptをリンク(aタグ)に実装した形で返します。
 *
 * @param  array   $params
 *                       string  name             この文字列を&lt;a&gt;～&lt;/a&gt;で囲って出力
 *                       string  url              遷移先 URL
 *                       boolean protected false |タブを保護するか
 *                       string  formId    null  |フォームの ID 名
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.newTabLink.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_newTabLink($params, &$smarty) {


    // ログ開始
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['name']     = '';
    $default['url']      = '';
    $default['formId']   = '';
    $default['saveName'] = '';


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }


    //-------------------------------------------------------------------------------------------
    //■onclick のスクリプトを生成
    //-------------------------------------------------------------------------------------------
    require_once dirname(__FILE__) . '/function.newTab.php';
    $script = '';
    $script = smarty_function_newTab($params, $smarty);


    //-------------------------------------------------------------------------------------------
    //■値を取得
    //-------------------------------------------------------------------------------------------
    $_name = $data['name'];




    Mikoshiva_Logger::info(LOG_END);
    return "<a href=\"#\" onclick=\"{$script}\">{$_name}</a>";
}


















