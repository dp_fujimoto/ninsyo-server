<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Gmo_Alter
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Alter extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'ShopID' => null,
        'ShopPass' => null,
        'AccessID' => null,
        'AccessPass' => null,
        'JobCd' => null,
        'Amount' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * ShopID
	 *
     * @return  ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getShopID() {
		return $this->_properties['ShopID'];
    }


    /**
     * ShopID
	 *
	 * @param  $ShopID ShopID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setShopID($ShopID) {
		$this->_properties['ShopID'] = $ShopID;
    }


    /**
     * ShopPass
	 *
     * @return  ShopPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getShopPass() {
		return $this->_properties['ShopPass'];
    }


    /**
     * ShopPass
	 *
	 * @param  $ShopPass ShopPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setShopPass($ShopPass) {
		$this->_properties['ShopPass'] = $ShopPass;
    }


    /**
     * AccessID
	 *
     * @return  AccessID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessID() {
		return $this->_properties['AccessID'];
    }


    /**
     * AccessID
	 *
	 * @param  $AccessID AccessID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessID($AccessID) {
		$this->_properties['AccessID'] = $AccessID;
    }


    /**
     * AccessPass
	 *
     * @return  AccessPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessPass() {
		return $this->_properties['AccessPass'];
    }


    /**
     * AccessPass
	 *
	 * @param  $AccessPass AccessPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessPass($AccessPass) {
		$this->_properties['AccessPass'] = $AccessPass;
    }


    /**
     * JobCd
	 *
     * @return  JobCd
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getJobCd() {
		return $this->_properties['JobCd'];
    }


    /**
     * JobCd
	 *
	 * @param  $JobCd JobCd
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setJobCd($JobCd) {
		$this->_properties['JobCd'] = $JobCd;
    }


    /**
     * Amount
	 *
     * @return  Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAmount() {
		return $this->_properties['Amount'];
    }


    /**
     * Amount
	 *
	 * @param  $Amount Amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Alter.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAmount($Amount) {
		$this->_properties['Amount'] = $Amount;
    }


}

























