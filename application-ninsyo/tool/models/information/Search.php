<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstInformationHistory.php';
require_once 'Mikoshiva/Db/Pager.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Tool_Models_Information_Search extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        //
        // 検索フォームからサブミットされたら、条件を追加して検索を行う。
        //
        //-----------------------------------------------
        // SQL作成
        $sql = '';
        $sql .= 'SELECT ';
        $sql .= ' *';
        $sql .= ' FROM mst_information_history';

        // パラメータセット
        $params = array();

        $sql .= ' WHERE mih_delete_flag = ? ';
        $params[] = '0';

        $sql .= ' ORDER BY mih_registration_datetime DESC, mih_information_history_id DESC';

        // SQL実行＋全件取得
        $res = array();

        $pager = new Mikoshiva_Db_Pager();
        $res  = $pager->fetchAllLimit($actionForm, 5, $sql, $params);

        //-----------------------------------------------
        // オブジェクトへの詰め替え処理
        //-----------------------------------------------
        // リターンするクラス配列
        $arrayClass = array();
        $staffIdArr = array();

        // Popoに検索結果をセットしていく
        logInfo('(' . __LINE__ . ')', 'foreach：ループ開始');
        foreach ($res as $k => $v) {
            logInfo('(' . __LINE__ . ')', 'loop：ループキー：' . $k);

            // 配列から接頭語指定した要素だけ、取得
            $row = array();
            $row = Mikoshiva_Utility_Array::extractBeginningOfPrefix( 'mih_' , $v );

            // Popoに一行ずつ入れていく
            $arrayClass[] = Mikoshiva_Utility_Copy::copyDbProperties( new Popo_MstInformationHistory() , $row );
        }


        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $arrayClass;

    }
}
