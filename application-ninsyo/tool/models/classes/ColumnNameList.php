<?php
require_once 'Mikoshiva/Controller/Model/Class/Db/Abstract.php';

/**
 * カラムの日本語名リストを作成します。
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2011/09/09
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Classes_ColumnNameList
    extends Mikoshiva_Controller_Model_Class_Db_Abstract {

    /**
    * カラムの日本語名リストを作成します。
    *
    * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
    * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
    * @param Mikoshiva_Controller_Mapping_ActionContext $context
    * @return object
    */
    public function execute() {

        $column_explain = array();

        //require ('../application/tool/parts/MikoshivaColumnExplain.php');

        return $column_explain;
    }
}
