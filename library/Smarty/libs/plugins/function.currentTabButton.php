<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/View/Helper/Exception.php';
//function.currentTab.phpが必須

/**
 * 画面遷移を現在開いているタブに対して行うjavascriptをボタンタグに実装した形で返します。
 *
 * @param array   $params
 *                      string name        ボタンのvalue
 *                      string url         遷移先 URL
 *                      string tabId  null|タブ ID
 *                      string formId null|フォームの ID 名
 * @param Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/28
 * @version SVN:$Id: function.currentTabButton.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_currentTabButton($params, &$smarty) {


    // ログ開始
    Mikoshiva_Logger::info(LOG_START);


    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default               = array();
    $default['name']       = '';
    $default['url']        = '';
    $default['tabId']      = null;
    $default['formId']     = null;
    $default['onloadList'] = '[]';


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(isBlankOrNull($data['url']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 url に値がありません。');
    }
    if(isBlankOrNull($data['tabId']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 tabId に値がありません。');
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once dirname(__FILE__) . '/function.currentTab.php';
    $script = smarty_function_currentTab($data, $smarty);

    Mikoshiva_Logger::info(LOG_END);
    return "<input type=\"button\" value=\"{$data['name']}\" onclick=\"this.disabled=true;{$script}\">";
}

