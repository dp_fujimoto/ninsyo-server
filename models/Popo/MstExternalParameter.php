<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_external_parameter
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstExternalParameter extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mep';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'externalParameterId' => null,
        'externalCooperationId' => null,
        'externalParameterName' => null,
        'externalParameterCode' => null,
        'className' => null,
        'groupKey' => null,
        'privateParameterFlag' => null,
        'alignmentSequence' => null,
        'memo' => null,
        'valueNumber' => null,
        'value1' => null,
        'value2' => null,
        'value3' => null,
        'value4' => null,
        'value5' => null,
        'cancelValue1Flag' => null,
        'cancelValue2Flag' => null,
        'cancelValue3Flag' => null,
        'cancelValue4Flag' => null,
        'cancelValue5Flag' => null,
        'registBatchFlag' => null,
        'cancelBatchFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 外部連携パラメータID の値を返します
	 *
     * @return int 外部連携パラメータID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExternalParameterId() {
		return $this->_properties['externalParameterId'];
    }


    /**
     * 外部連携パラメータID の値をセットします
	 *
	 * @param int $externalParameterId 外部連携パラメータID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExternalParameterId($externalParameterId) {
		$this->_properties['externalParameterId'] = $externalParameterId;
    }


    /**
     * 外部連携ID の値を返します
	 *
     * @return int 外部連携ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExternalCooperationId() {
		return $this->_properties['externalCooperationId'];
    }


    /**
     * 外部連携ID の値をセットします
	 *
	 * @param int $externalCooperationId 外部連携ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExternalCooperationId($externalCooperationId) {
		$this->_properties['externalCooperationId'] = $externalCooperationId;
    }


    /**
     * 外部連携パラメータ名 の値を返します
	 *
     * @return varchar 外部連携パラメータ名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExternalParameterName() {
		return $this->_properties['externalParameterName'];
    }


    /**
     * 外部連携パラメータ名 の値をセットします
	 *
	 * @param varchar $externalParameterName 外部連携パラメータ名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExternalParameterName($externalParameterName) {
		$this->_properties['externalParameterName'] = $externalParameterName;
    }


    /**
     * 外部連携パラメータコード の値を返します
	 *
     * @return varchar 外部連携パラメータコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExternalParameterCode() {
		return $this->_properties['externalParameterCode'];
    }


    /**
     * 外部連携パラメータコード の値をセットします
	 *
	 * @param varchar $externalParameterCode 外部連携パラメータコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExternalParameterCode($externalParameterCode) {
		$this->_properties['externalParameterCode'] = $externalParameterCode;
    }


    /**
     * クラス名 の値を返します
	 *
     * @return varchar クラス名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClassName() {
		return $this->_properties['className'];
    }


    /**
     * クラス名 の値をセットします
	 *
	 * @param varchar $className クラス名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClassName($className) {
		$this->_properties['className'] = $className;
    }


    /**
     * グループキー の値を返します
	 *
     * @return char グループキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGroupKey() {
		return $this->_properties['groupKey'];
    }


    /**
     * グループキー の値をセットします
	 *
	 * @param char $groupKey グループキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGroupKey($groupKey) {
		$this->_properties['groupKey'] = $groupKey;
    }


    /**
     * 非公開パラメータフラグ の値を返します
	 *
     * @return char 非公開パラメータフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrivateParameterFlag() {
		return $this->_properties['privateParameterFlag'];
    }


    /**
     * 非公開パラメータフラグ の値をセットします
	 *
	 * @param char $privateParameterFlag 非公開パラメータフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrivateParameterFlag($privateParameterFlag) {
		$this->_properties['privateParameterFlag'] = $privateParameterFlag;
    }


    /**
     * 公開順序 の値を返します
	 *
     * @return int 公開順序
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAlignmentSequence() {
		return $this->_properties['alignmentSequence'];
    }


    /**
     * 公開順序 の値をセットします
	 *
	 * @param int $alignmentSequence 公開順序
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAlignmentSequence($alignmentSequence) {
		$this->_properties['alignmentSequence'] = $alignmentSequence;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 有効値保持数 の値を返します
	 *
     * @return int 有効値保持数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValueNumber() {
		return $this->_properties['valueNumber'];
    }


    /**
     * 有効値保持数 の値をセットします
	 *
	 * @param int $valueNumber 有効値保持数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValueNumber($valueNumber) {
		$this->_properties['valueNumber'] = $valueNumber;
    }


    /**
     * 値１ の値を返します
	 *
     * @return varchar 値１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue1() {
		return $this->_properties['value1'];
    }


    /**
     * 値１ の値をセットします
	 *
	 * @param varchar $value1 値１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue1($value1) {
		$this->_properties['value1'] = $value1;
    }


    /**
     * 値２ の値を返します
	 *
     * @return varchar 値２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue2() {
		return $this->_properties['value2'];
    }


    /**
     * 値２ の値をセットします
	 *
	 * @param varchar $value2 値２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue2($value2) {
		$this->_properties['value2'] = $value2;
    }


    /**
     * 値３ の値を返します
	 *
     * @return varchar 値３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue3() {
		return $this->_properties['value3'];
    }


    /**
     * 値３ の値をセットします
	 *
	 * @param varchar $value3 値３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue3($value3) {
		$this->_properties['value3'] = $value3;
    }


    /**
     * 値４ の値を返します
	 *
     * @return varchar 値４
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue4() {
		return $this->_properties['value4'];
    }


    /**
     * 値４ の値をセットします
	 *
	 * @param varchar $value4 値４
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue4($value4) {
		$this->_properties['value4'] = $value4;
    }


    /**
     * 値５ の値を返します
	 *
     * @return varchar 値５
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue5() {
		return $this->_properties['value5'];
    }


    /**
     * 値５ の値をセットします
	 *
	 * @param varchar $value5 値５
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue5($value5) {
		$this->_properties['value5'] = $value5;
    }


    /**
     * キャンセル値１フラグ の値を返します
	 *
     * @return char キャンセル値１フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue1Flag() {
		return $this->_properties['cancelValue1Flag'];
    }


    /**
     * キャンセル値１フラグ の値をセットします
	 *
	 * @param char $cancelValue1Flag キャンセル値１フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue1Flag($cancelValue1Flag) {
		$this->_properties['cancelValue1Flag'] = $cancelValue1Flag;
    }


    /**
     * キャンセル値２フラグ の値を返します
	 *
     * @return char キャンセル値２フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue2Flag() {
		return $this->_properties['cancelValue2Flag'];
    }


    /**
     * キャンセル値２フラグ の値をセットします
	 *
	 * @param char $cancelValue2Flag キャンセル値２フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue2Flag($cancelValue2Flag) {
		$this->_properties['cancelValue2Flag'] = $cancelValue2Flag;
    }


    /**
     * キャンセル値３フラグ の値を返します
	 *
     * @return char キャンセル値３フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue3Flag() {
		return $this->_properties['cancelValue3Flag'];
    }


    /**
     * キャンセル値３フラグ の値をセットします
	 *
	 * @param char $cancelValue3Flag キャンセル値３フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue3Flag($cancelValue3Flag) {
		$this->_properties['cancelValue3Flag'] = $cancelValue3Flag;
    }


    /**
     * キャンセル値４フラグ の値を返します
	 *
     * @return char キャンセル値４フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue4Flag() {
		return $this->_properties['cancelValue4Flag'];
    }


    /**
     * キャンセル値４フラグ の値をセットします
	 *
	 * @param char $cancelValue4Flag キャンセル値４フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue4Flag($cancelValue4Flag) {
		$this->_properties['cancelValue4Flag'] = $cancelValue4Flag;
    }


    /**
     * キャンセル値５フラグ の値を返します
	 *
     * @return char キャンセル値５フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue5Flag() {
		return $this->_properties['cancelValue5Flag'];
    }


    /**
     * キャンセル値５フラグ の値をセットします
	 *
	 * @param char $cancelValue5Flag キャンセル値５フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue5Flag($cancelValue5Flag) {
		$this->_properties['cancelValue5Flag'] = $cancelValue5Flag;
    }


    /**
     * 購入バッチフラグ の値を返します
	 *
     * @return char 購入バッチフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistBatchFlag() {
		return $this->_properties['registBatchFlag'];
    }


    /**
     * 購入バッチフラグ の値をセットします
	 *
	 * @param char $registBatchFlag 購入バッチフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistBatchFlag($registBatchFlag) {
		$this->_properties['registBatchFlag'] = $registBatchFlag;
    }


    /**
     * キャンセルバッチフラグ の値を返します
	 *
     * @return char キャンセルバッチフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelBatchFlag() {
		return $this->_properties['cancelBatchFlag'];
    }


    /**
     * キャンセルバッチフラグ の値をセットします
	 *
	 * @param char $cancelBatchFlag キャンセルバッチフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelBatchFlag($cancelBatchFlag) {
		$this->_properties['cancelBatchFlag'] = $cancelBatchFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























