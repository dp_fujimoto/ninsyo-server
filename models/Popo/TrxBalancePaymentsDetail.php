<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_balance_payments_detail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxBalancePaymentsDetail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tbd';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'balancePaymentsDetailId' => null,
        'balancePaymentsId' => null,
        'balancePaymentsExecutionId' => null,
        'balancePaymentsType' => null,
        'refundFlag' => null,
        'amount' => null,
        'reportedDate' => null,
        'reportedServiceEndDate' => null,
        'divisionId' => null,
        'directionStatusId' => null,
        'licenseId' => null,
        'moneyReceivedId' => null,
        'realRefundId' => null,
        'shippingId' => null,
        'productOfferedDetailId' => null,
        'searchExcludedFlag' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tbd_balance_payments_detail_id の値を返します
	 *
     * @return int tbd_balance_payments_detail_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBalancePaymentsDetailId() {
		return $this->_properties['balancePaymentsDetailId'];
    }


    /**
     * tbd_balance_payments_detail_id の値をセットします
	 *
	 * @param int $balancePaymentsDetailId tbd_balance_payments_detail_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBalancePaymentsDetailId($balancePaymentsDetailId) {
		$this->_properties['balancePaymentsDetailId'] = $balancePaymentsDetailId;
    }


    /**
     * tbd_balance_payments_id の値を返します
	 *
     * @return int tbd_balance_payments_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBalancePaymentsId() {
		return $this->_properties['balancePaymentsId'];
    }


    /**
     * tbd_balance_payments_id の値をセットします
	 *
	 * @param int $balancePaymentsId tbd_balance_payments_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBalancePaymentsId($balancePaymentsId) {
		$this->_properties['balancePaymentsId'] = $balancePaymentsId;
    }


    /**
     * tbd_balance_payments_execution_id の値を返します
	 *
     * @return int tbd_balance_payments_execution_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBalancePaymentsExecutionId() {
		return $this->_properties['balancePaymentsExecutionId'];
    }


    /**
     * tbd_balance_payments_execution_id の値をセットします
	 *
	 * @param int $balancePaymentsExecutionId tbd_balance_payments_execution_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBalancePaymentsExecutionId($balancePaymentsExecutionId) {
		$this->_properties['balancePaymentsExecutionId'] = $balancePaymentsExecutionId;
    }


    /**
     * tbd_balance_payments_type の値を返します
	 *
     * @return varchar tbd_balance_payments_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBalancePaymentsType() {
		return $this->_properties['balancePaymentsType'];
    }


    /**
     * tbd_balance_payments_type の値をセットします
	 *
	 * @param varchar $balancePaymentsType tbd_balance_payments_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBalancePaymentsType($balancePaymentsType) {
		$this->_properties['balancePaymentsType'] = $balancePaymentsType;
    }


    /**
     * tbd_refund_flag の値を返します
	 *
     * @return char tbd_refund_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundFlag() {
		return $this->_properties['refundFlag'];
    }


    /**
     * tbd_refund_flag の値をセットします
	 *
	 * @param char $refundFlag tbd_refund_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundFlag($refundFlag) {
		$this->_properties['refundFlag'] = $refundFlag;
    }


    /**
     * tbd_amount の値を返します
	 *
     * @return int tbd_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAmount() {
		return $this->_properties['amount'];
    }


    /**
     * tbd_amount の値をセットします
	 *
	 * @param int $amount tbd_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAmount($amount) {
		$this->_properties['amount'] = $amount;
    }


    /**
     * tbd_reported_date の値を返します
	 *
     * @return date tbd_reported_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReportedDate() {
		return $this->_properties['reportedDate'];
    }


    /**
     * tbd_reported_date の値をセットします
	 *
	 * @param date $reportedDate tbd_reported_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReportedDate($reportedDate) {
		$this->_properties['reportedDate'] = $reportedDate;
    }


    /**
     * tbd_reported_service_end_date の値を返します
	 *
     * @return date tbd_reported_service_end_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReportedServiceEndDate() {
		return $this->_properties['reportedServiceEndDate'];
    }


    /**
     * tbd_reported_service_end_date の値をセットします
	 *
	 * @param date $reportedServiceEndDate tbd_reported_service_end_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReportedServiceEndDate($reportedServiceEndDate) {
		$this->_properties['reportedServiceEndDate'] = $reportedServiceEndDate;
    }


    /**
     * tbd_division_id の値を返します
	 *
     * @return int tbd_division_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * tbd_division_id の値をセットします
	 *
	 * @param int $divisionId tbd_division_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * tbd_direction_status_id の値を返します
	 *
     * @return int tbd_direction_status_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * tbd_direction_status_id の値をセットします
	 *
	 * @param int $directionStatusId tbd_direction_status_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * tbd_license_id の値を返します
	 *
     * @return int tbd_license_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLicenseId() {
		return $this->_properties['licenseId'];
    }


    /**
     * tbd_license_id の値をセットします
	 *
	 * @param int $licenseId tbd_license_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLicenseId($licenseId) {
		$this->_properties['licenseId'] = $licenseId;
    }


    /**
     * tbd_money_received_id の値を返します
	 *
     * @return int tbd_money_received_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedId() {
		return $this->_properties['moneyReceivedId'];
    }


    /**
     * tbd_money_received_id の値をセットします
	 *
	 * @param int $moneyReceivedId tbd_money_received_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedId($moneyReceivedId) {
		$this->_properties['moneyReceivedId'] = $moneyReceivedId;
    }


    /**
     * tbd_real_refund_id の値を返します
	 *
     * @return int tbd_real_refund_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealRefundId() {
		return $this->_properties['realRefundId'];
    }


    /**
     * tbd_real_refund_id の値をセットします
	 *
	 * @param int $realRefundId tbd_real_refund_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealRefundId($realRefundId) {
		$this->_properties['realRefundId'] = $realRefundId;
    }


    /**
     * tbd_shipping_id の値を返します
	 *
     * @return int tbd_shipping_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * tbd_shipping_id の値をセットします
	 *
	 * @param int $shippingId tbd_shipping_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * tbd_product_offered_detail_id の値を返します
	 *
     * @return int tbd_product_offered_detail_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedDetailId() {
		return $this->_properties['productOfferedDetailId'];
    }


    /**
     * tbd_product_offered_detail_id の値をセットします
	 *
	 * @param int $productOfferedDetailId tbd_product_offered_detail_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedDetailId($productOfferedDetailId) {
		$this->_properties['productOfferedDetailId'] = $productOfferedDetailId;
    }


    /**
     * tbd_search_excluded_flag の値を返します
	 *
     * @return char tbd_search_excluded_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSearchExcludedFlag() {
		return $this->_properties['searchExcludedFlag'];
    }


    /**
     * tbd_search_excluded_flag の値をセットします
	 *
	 * @param char $searchExcludedFlag tbd_search_excluded_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSearchExcludedFlag($searchExcludedFlag) {
		$this->_properties['searchExcludedFlag'] = $searchExcludedFlag;
    }


    /**
     * tbd_imperfection_flag の値を返します
	 *
     * @return char tbd_imperfection_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * tbd_imperfection_flag の値をセットします
	 *
	 * @param char $imperfectionFlag tbd_imperfection_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * tbd_delete_flag の値を返します
	 *
     * @return char tbd_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tbd_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tbd_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tbd_deletion_datetime の値を返します
	 *
     * @return datetime tbd_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tbd_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tbd_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tbd_update_datetime の値を返します
	 *
     * @return datetime tbd_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tbd_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tbd_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tbd_registration_datetime の値を返します
	 *
     * @return datetime tbd_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tbd_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tbd_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tbd_update_timestamp の値を返します
	 *
     * @return timestamp tbd_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tbd_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tbd_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























