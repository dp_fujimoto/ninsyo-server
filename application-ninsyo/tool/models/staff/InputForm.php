<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * -----------------------------------------------------------------------
 *    ワンポイント
 * -----------------------------------------------------------------------
 *    ■クラス名
 *    アクションフォームクラスのクラス名は、
 *    必ず「～Form」としてください。
 *
 *    ■resetメソッド
 *    任意の初期化処理を行う場合に実装します。
 *    (メソッド定義は省略可能です)
 *
 *    ■validateメソッド
 *    任意のバリデート処理を行う場合に実装します。(独自バリデート)
 *    validation.yml定義による固定バリデート処理を実行するには、
 *    スーパークラスのvalidateメソッドを呼び出してください。
 *    「parent::reset($config, $request);」
 *    その場合、スーパークラスのvalidateメソッドが返す
 *    固定バリデートの実行結果値は、適切に処理してください。
 *    (メソッド定義は省略可能です)
 * -----------------------------------------------------------------------
 *
 * @author        K.Fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/22
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Staff_InputForm
    extends Mikoshiva_Controller_Mapping_Form_Abstract {




    /**
     * アクションフォームの初期化を行います。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');
        parent::reset($config, $request);
        logInfo('(' . __LINE__ . ')', '終了');
    }

    /**
     * バリデートを行います。エラーMSGをリターンでかえす
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        //
        // 基本バリデートチェック
        //
        //-----------------------------------------------
        $message = array();
        $message = parent::validate($config, $request);



        //-----------------------------------------------
        //
        // 独自バリデートチェック
        //
        //-----------------------------------------------

        //-----------------------------------------------
        // エラーメッセージを返す
        //-----------------------------------------------
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $message;
    }

}
