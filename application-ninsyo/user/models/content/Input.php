<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/Operation.php';

/**
 * 指定したコンテンツIDの情報を入力画面にプリセットする。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class User_Models_Content_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したコンテンツIDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $serverName = $actionForm->getServerName();
        $email = $actionForm->getEmail();
        $password = $actionForm->getPassword();
        $contentId = $actionForm->getContentId();

        // 共通クラス
        $class = new Api_Models_Operation();

        $hashArr = array();
        $hashArr[] = $serverName;
        $hashArr[] = $class->createUserHash($email);
        $hashArr[] = $class->createUserHash($password);

        $hash = implode('|',$hashArr);

        $list = array();
        $list[$hash] = $contentId;

        $actionForm->setList($list);

        //-----------------------------------------------
        //
        // コンテンツ検索
        //
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT";
        $sql .= "    mbr_brand_name";
        $sql .= "   ,mpr_product_name";
        $sql .= "   ,mct_content_name";
        $sql .= "   ,mct_content_id";
        $sql .= " FROM mst_content";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1 ";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mct_content_id IN (?)";

        $params = array();
        $params[] = explode(',',$contentId);

        $sql .= " ORDER BY mbr_brand_name, mpr_product_name, mct_content_name";

        // SQL実行＋全件取得
        $res = array();
        for ($i = 0; $i < count($params); $i++) {
            $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
        }
        $res = $this->_db->query($sql);

        $contentList = array();
        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $contentList[$v['mct_content_id']] = array();
            $contentList[$v['mct_content_id']][0] = $v['mbr_brand_name'];
            $contentList[$v['mct_content_id']][1] = $v['mpr_product_name'];
            $contentList[$v['mct_content_id']][2] = $v['mct_content_name'];
        }
        $actionForm->setContentList($contentList);

        //-----------------------------------------------
        // 無条件コンテンツID取得
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT ";
        $sql .= "    mbr_brand_name";
        $sql .= "   ,mpr_product_name";
        $sql .= "   ,mct_content_name";
        $sql .= "   ,mct_content_id";
        $sql .= " FROM mst_condition";
        $sql .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mcd_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mcd_none_condition_flag = '1'";
        $sql .= "   AND mct_test_flag = '0'";
        $sql .= "   AND mct_publication_date <= ?";
        $sql .= "   AND mct_content_id NOT IN (?)";

        $params   = array();
        $params[] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');
        $params[] = explode(',',$contentId);

        $res = array();
        for ($i = 0; $i < count($params); $i++) {
            $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
        }
        $res = $this->_db->query($sql);

        $freeContentList = array();
        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $freeContentList[$v['mct_content_id']] = array();
            $freeContentList[$v['mct_content_id']][0] = $v['mbr_brand_name'];
            $freeContentList[$v['mct_content_id']][1] = $v['mpr_product_name'];
            $freeContentList[$v['mct_content_id']][2] = $v['mct_content_name'];
        }
        $actionForm->setFreeContentList($freeContentList);

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }

}
