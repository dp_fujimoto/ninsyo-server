<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CheckSessionId.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Authentication_GetHashAlgo extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 検索結果
        $result = '';

        $class = new Api_Models_Classes_CheckSessionId();
        $class->execute($actionForm->getSessionId());

        $result = HASH_ALGO;

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $result;

    }
}
