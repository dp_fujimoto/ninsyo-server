<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Format - CSVを配列にする</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="CSVを配列にする">CSVを配列にする</h1>



            <!-- ========================================
              ■CSVを配列にする
            ==========================================-->
            <h2 id="CSVを配列にする">CSVを配列にする</h2>

            <pre class="wiki">
<?php
echo preg_replace('/[\r\n]*/', '', highlight_string('<?php

        require_once \'Mikoshiva/Format/Decode/Csv.php\';
        $converter = new Mikoshiva_Format_Decode_Csv();
        
        // デフォルトではダブルクォートで囲ったCSVを想定
        $str = \'"1","二","""３""",""\';
        dumper($converter->process($str));
        
        // ダブルクォート無しの場合
        $converter->setFormat(\'NOT_ENCLOSED_IN_DOUBLE_QUOTES\'); // フォーマットを "ダブルクォート囲みなし" に変更
        $str = \'1,二,""３"",""\';
        dumper($converter->process($str));

?>
', true));
?>
</pre>
            <pre class="wiki">
array(4) {
  [0] => string(1) "1"
  [1] => string(3) "二"
  [2] => string(5) ""３""
  [3] => string(0) ""
}

array(4) {
  [0] => string(1) "1"
  [1] => string(3) "二"
  [2] => string(7) """３"""
  [3] => string(2) """"
}

            </pre>











            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















