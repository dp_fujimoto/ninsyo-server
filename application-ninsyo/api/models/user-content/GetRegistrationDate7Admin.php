<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CreateUserHash.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_GetRegistrationDate7Admin extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 各コンテンツの経過日数も返す
        $output = '';
        $flag = '0';

        $class = new Api_Models_Classes_CreateUserHash();
        $passwordHash = $class->execute(NINSYO_ADMIN_PASSWORD);

        if ($actionForm->getId() === 'admin') {
            if ($actionForm->getPassword() === $passwordHash || $actionForm->getPassword() === NINSYO_ADMIN_PASSWORD || $actionForm->getPassword() === NINSYO_TRAINING_PASSWORD) {
                $flag = '1';
            }
        }
        if ($flag === '1') {
            $productCode = "'" . str_replace('-',"','",$actionForm->getProductCode()) . "'";

            //-----------------------------------------------
            // ユーザー確認
            //-----------------------------------------------
            // SQL作成
            $sql = "";
            $sql .= "SELECT ";
            $sql .= "   mct_content_id";
            $sql .= " FROM mst_content";
            $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
            $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
            $sql .= " WHERE 1 = 1";
            $sql .= "   AND mct_delete_flag = '0'";
            $sql .= "   AND mpr_delete_flag = '0'";
            $sql .= "   AND mbr_delete_flag = '0'";
            $sql .= "   AND mpr_product_code IN (" . $productCode . ")";
            $sql .= "   AND mct_application_type = ?";

            $params   = array();
            $params[] = 'APPLICATION_TYPE_WEB_PAGE_AUTH';

            // SQL実行＋全件取得
            $res = array();
            $res = $this->_db->query($sql,$params);

            $nowDate = date('Y-m-d');
            $min = '2000-01-01';
            $max = $nowDate;
            $ids = array();

            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                $ids[$v['mct_content_id']] = array();
                $ids[$v['mct_content_id']]['min'] = $min;
                $ids[$v['mct_content_id']]['max'] = $nowDate;
            }

            $tempArr = array();
            foreach ($ids as $contentId => $v) {
                $tempArr[] = $contentId . '|' . str_replace('-','/',$v['min']) . '|' . str_replace('-','/',$v['max']);
            }
            $output = $min . ',' . $max . ',' . implode('-',$tempArr) . ',' . $passwordHash . ',' . ',' . 'admin';
        }

        echo $output;

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
