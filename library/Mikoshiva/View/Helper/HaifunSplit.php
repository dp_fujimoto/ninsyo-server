<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';
/**
 * 文字列をハイフン（-）で分割し、指定した添字の値を返す。
 *
 * @package Mikoshiva_View
 * @author K.Sekiya <kurachi0223@gmail.com>
 * @since 2009/12/07
 * @version SVN:$Id: HaifunSplit.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_HaifunSplit extends Mikoshiva_View_Helper_Abstract {


    /**
     * 文字列をハイフン（-）で分割し、指定した添字の値を返す。
     *
     * ※入力画面の電話番号・郵便番号などのプリセットで使用する。
     *
     * @author K.Sekiya <kurachi0223@gmail.com>
     * @since 2009/12/07
     * @version SVN:$Id:
     * @param string $str 文字列（電話番号・郵便番号など）
     * @param string $no  配列分割後の取得したい添字
     * @return string
     */
    public function haifunSplit($str, $no) {
        logInfo(LOG_START);

        $expArr = array();
        $expArr = explode('-',$str);
        
       	logInfo(LOG_END);
        return $expArr[$no];
    }
}
