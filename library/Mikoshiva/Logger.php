<?php
/**
 * @package Mikoshiva_Logger
 */

/**
 * require
 */
require_once 'Zend/Registry.php';


/**
 * 
 * @package Mikoshiva_Logger
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Logger {



    // const EMERG   = 0;  // 緊急事態 (Emergency): システムが使用不可能です
    // const ALERT   = 1;  // 警報 (Alert): 至急対応が必要です
    // const CRIT    = 2;  // 危機 (Critical): 危機的な状況です
    // const ERR     = 3;  // エラー (Error): エラーが発生しました
    // const WARN    = 4;  // 警告 (Warning): 警告が発生しました
    // const NOTICE  = 5;  // 注意 (Notice): 通常動作ですが、注意すべき状況です
    // const INFO    = 6;  // 情報 (Informational): 情報メッセージ
    // const DEBUG   = 7;  // デバッグ (Debug): デバッグメッセージ


    /**
     * 緊急事態 (Emergency): システムが使用不可能です
     *
     * @param unknown_type $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function emerg($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::EMERG);
    }


    /**
     * 警報 (Alert): 至急対応が必要です
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function alert($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::ALERT);
    }


    /**
     * 危機 (Critical): 危機的な状況です
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function crit($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::CRIT);
    }


    /**
     * エラー (Error): エラーが発生しました
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function err($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::ERR);
    }


    /**
     * 警告 (Warning): 警告が発生しました
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function warn($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::WARN);
    }


    /**
     * 注意 (Notice): 通常動作ですが、注意すべき状況です
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function notice($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::NOTICE);
    }


    /**
     * 情報 (Informational): 情報メッセージ
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function info($message, $addMessage = '') {

        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::INFO);
    }


    /**
     * デバッグ (Debug): デバッグメッセージ
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function debug($message, $addMessage = '') {
        Zend_Registry::get('logger')->logger($message, $addMessage, zend_log::DEBUG);
    }
    
    
    /**
     *  開発(Devel): 開発メッセージ
     *
     * @param $message
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Logger.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function devel($message, $addMessage = '') {
        Zend_Registry::get('develLogger')->logger($message, $addMessage, zend_log::DEBUG);
    }
}



















