<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


 /**
 * 新しいタブを開くjavascriptを返します。
 *
 * @param  array   $params
 *                       string  url              遷移先 URL
 *                       boolean protected false |タブを保護するか
 *                       string  formId    null  |フォームの ID 名
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/28
 * @version SVN:$Id: function.newTab.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_newTab($params, &$smarty) {


    // ログ開始
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['url']      = '';
    $default['formId']   = '';
    $default['saveName'] = '';


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['url']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 url に値がありません。');
    }


    //-------------------------------------------------------------------------------------------
    //■値を取得
    //-------------------------------------------------------------------------------------------
    $_url      = $data['url'];
    $_formId   = $data['formId'];
    $_saveName = $data['saveName'];


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    return "KO_newTab('{$_url}', '{$_formId}', '{$_saveName}');";
/**
    //protected=trueなら文字列'true'をセット
    $_protected = $data['protected'] ? 'true' : 'false';

    if ($data['formId'] === null ) {
        return 'KO_newTab(\'' . $data['url'] . '\', ' . $_protected . ');';
    } else {
        return 'KO_newTab(\'' . $data['url'] . '\', ' . $_protected . ', \''  . $data['formId'] . '\');';
    }
**/
    Mikoshiva_Logger::info(LOG_END);

}


























