<?php
/** @package Mikoshiva_View */

/**
 * Smarty用ビュースクリプト
 *
 * @package Mikoshiva_View
 * @author admine
 * @since 2010/02/17
 * @version SVN:$Id: Smarty.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_View_Smarty extends Zend_View_Abstract
{

    /**
     * Smarty object
     * @var Smarty
     */
    protected $_smarty;


    /**
     * 文字列のエスケープに使う関数
     * @var string
     */
    protected $_escape	=	'htmlentities';

    /**
     * コンストラクタ
     *
     * @param Smarty $smarty
     * @return void
     *
     */
    public function __construct( Smarty $smarty = null )
    {
    	if( is_null($smarty) ){
    		$smarty = new Smarty();
    	}

		$this->_smarty = $smarty;
		$this->_smarty->allow_php_tag = true; // {php}{/php}の有効化

		$this->assign('layout', $this->layout());
		$this->assign('h', $this);
	}

    /**
     * テンプレートエンジンオブジェクトを返します
     *
     * @return Smarty
     */
    public function getEngine()
    {
        return $this->_smarty;
    }

    /**
     * テンプレートへのパスを設定します
     *
     * @param string $path パスとして設定するディレクトリ
     * @return void
     */
    public function setScriptPath($path)
    {
        if (is_readable($path)) {
            $this->_smarty->template_dir = $path;
            return;
        }

        throw new Exception("無効なパスが指定されました : '$path'");
    }

    /**
     * スクリプトパスの追加
     * @param string $name
     */
    public function addScriptPath($name)
    {
        $this->setScriptPath($name);
    }

    /**
     * 現在のテンプレートディレクトリを取得します
     *
     * @return string
     */
    public function getScriptPaths()
    {
        if (is_array($this->_smarty->template_dir)) {
            return $this->_smarty->template_dir;
        }

        return array($this->_smarty->template_dir);
    }

    /**
     * setScriptPath へのエイリアス
     *
     * @param string $path
     * @param string $prefix Unused
     * @return void
     */
    public function setBasePath($path, $prefix = 'Zend_View')
    {
        return $this->setScriptPath($path);
    }

    /**
     * setScriptPath へのエイリアス
     *
     * @param string $path
     * @param string $prefix Unused
     * @return void
     */
    public function addBasePath($path, $prefix = 'Zend_View')
    {
        return $this->setScriptPath($path);
    }

    /**
     * 変数をテンプレートに代入します
     *
     * @param string $key 変数名
     * @param mixed $val 変数の値
     * @return void
     */
    public function __set($key, $val)
    {
        $this->_smarty->assign($key, $val);
    }

    /**
     * 代入された変数を取得します
     *
     * @param string $key 変数名
     * @return mixed 変数の値
     */
    public function __get($key)
    {
        return $this->_smarty->get_template_vars($key);
    }

    /**
     * empty() や isset() のテストが動作するようにします
     *
     * @param string $key
     * @return boolean
     */
    public function __isset($key)
    {
        return (null !== $this->_smarty->get_template_vars($key));
    }

    /**
     * オブジェクトのプロパティに対して unset() が動作するようにします
     *
     * @param string $key
     * @return void
     */
    public function __unset($key)
    {
        $this->_smarty->clear_assign($key);
    }

    /**
     * 変数をテンプレートに代入します
     *
     * 指定したキーを指定した値に設定します。あるいは、
     * キー => 値 形式の配列で一括設定します
     *
     * @see __set()
     * @param string|array $spec 使用する代入方式 (キー、あるいは キー => 値 の配列)
     * @param mixed $value (オプション) 名前を指定して代入する場合は、ここで値を指定します
     * @return void
     */
    public function assign($spec, $value = null)
    {
        if (is_array($spec)) {
            $this->_smarty->assign($spec);
            return;
        }
        // エスケープ
        if ($spec !== 'MIKOSHIVA___REQUEST') {
            $value = htmlspecialchars2($value);
        }

        $this->_smarty->assign($spec, $value);
    }

    /**
     * 代入済みのすべての変数を削除します
     *
     * Zend_View に {@link assign()} やプロパティ
     * ({@link __get()}/{@link __set()}) で代入された変数をすべて削除します
     *
     * @return void
     */
    public function clearVars()
    {
        $this->_smarty->clear_all_assign();
    }

    /**
     * テンプレートを処理し、結果を出力します
     *
     * @param string $name 処理するテンプレート
     * @return string 出力結果
     */
    public function render($name) {


        $filePath = $name;

        $p = is_array($this->getScriptPaths()) ? implode('', $this->getScriptPaths()): $p;
//dumper($p);
        //if (preg_match('#' . $p . '#', $filePath) !== 1) {
        if (preg_match('#' . str_replace(array('\\', '.','/'), array('\\\\','\.','\/'), $p) . '#', $filePath) !== 1) {
        //if (strpos($filePath, $p) !== false) {
//dumper($filePath);
            $scripts = (preg_match('#layouts#', $p) !== 1) ? '/scripts/' : '/';
            $filePath = $p
                      . $scripts
                      . $name;
                      //dumper($filePath);
        }

//dumper($filePath);//exit;


$b = $this->_smarty->fetch($filePath);


        if (preg_match('#layouts#', $p) !== 1
            && (DEBUG_VIEW_HISTORY === 1 || DEBUG_VIEW_REQUEST === 1 || DEBUG_VIEW_HTML === 1)) {

                // アクションを叩かれたときに表示される HTML
                $phtml = $b;
                $phtml = htmlspecialchars($phtml, ENT_QUOTES);
                $phtml = str_replace("\r\n", "\n",  $phtml);
                $phtml = str_replace("\r",   "\n",  $phtml);
                $phtml = str_replace("\n",   "\\n", $phtml);
                $phtml = '<pre>' . $phtml . '</pre>';

                // リクエストの価
                $req = $this->MIKOSHIVA___REQUEST;
                $req = str_replace("\r\n", "\n",  $req);
                $req = str_replace("\r",   "\n",  $req);
                $req = str_replace("\n",   "\\n", $req);

                //$c = str_replace(array("\r\n", "\r","\n"), '<br>', htmlspecialchars($b, ENT_QUOTES));
                //$c = str_replace(array("　", " ", "\t"), array('&emsp;', '&nbsp;', '&nbsp;&nbsp;&nbsp;&nbsp;'), $c);

                //
                $f5 = $this->F5Former();
                $f5 = str_replace("\r\n", "\n",  $f5);
                $f5 = str_replace("\r",   "\n",  $f5);
                $f5 = str_replace("\n",   "\\n", $f5);
                $f5 = str_replace("'",   "\'", $f5);
    //Zend_Debug::dump($f5);


                $b  = $b;
                $b .= '<script>';

                // 履歴
                if (DEBUG_VIEW_HISTORY === 1) {
                    $b .= "var f5 = $('#debug_f5').html();";
                    $b .= "f5 = f5 === null ? '' : f5;";
                }

                $b .= "$('#debug').html('";

                // 履歴
                if (DEBUG_VIEW_HISTORY === 1) {
                    $b .=     '<div class="MK_bgColorDeep" id="debug_f5">\' + f5 + \'' . $f5 . '</div> ';
                }

                // リクエスト
                if (DEBUG_VIEW_REQUEST === 1) {
                    $b .=     '<div class="MK_bgColorDeep">' . $req . '</div>';
                }


                // HTML ソース
                if (DEBUG_VIEW_HTML === 1) {
                    $b .=     '<div class="MK_bgColorDeep">' .  $phtml . '</div>';
                }
                $b .=     "');";
                $b .= "if ($('#debug_f5 form').length > 10) $('#debug_f5 form:lt(1)').remove();";
                $b .= '</script>';
                $b .= '';
        }


        if (preg_match('#layouts|mypage#', $p) !== 1) {
            if (preg_match('#/order/#', $filePath) !== 1) {
                $mes = $this->_smarty->fetch(APPLICATION_PATH . '/layouts/system-message.tpl');
                $mes = str_replace("\r\n", "\n",  $mes);
                $mes = str_replace("\r",   "\n",  $mes);
                $mes = str_replace("\n",   "\\n", $mes);
                $mes = str_replace("'",   "\'", $mes);
                $mes = str_replace('"',   '\"', $mes);
                //$b = "{$b}<script>$('#systemMessage').html('{$mes}');</script>"; // 2010/08/02 Ajax 関連のページでエラーになるとのことなので停止
            }
        }










return $b;


		//return $this->_smarty->fetch($filePath);
    }


    /**
     * プレフィルターのセット
     *
     * @param string $filter
     * @param string $key
     */
    public function setPreFilter( $filter, $key )
    {
    	$this->_smarty->autoload_filters['pre'][$key] = $filter;
    }

    /**
     * ポストフィルターのセット
     *
     * @param string $filter
     * @param string $key
     */
    public function setPostFilter( $filter, $key )
    {
    	$this->_smarty->autoload_filters['post'][$key] = $filter;
    }

    /**
     * アウトプットフィルターのセット
     * @param string $filter
     * @param string $key
     */
    public function setOutputFilter( $filter, $key )
    {
		$this->_smarty->autoload_filters['output'][$key] = $filter;
    }


    /**
     * Escapes a value for output in a view script.
     *
     * If escaping mechanism is one of htmlspecialchars or htmlentities, uses
     * {@link $_encoding} setting.
     *
     * @param mixed $var The output to escape.
     * @return mixed The escaped value.
     */
    public function escape($var)
    {
        if (in_array($this->_escape, array('htmlspecialchars', 'htmlentities'))) {
            return call_user_func($this->_escape, $var, ENT_COMPAT, mb_internal_encoding() );
        }

        return call_user_func($this->_escape, $var, ENT_COMPAT, mb_internal_encoding());
    }


    /**
     * Accesses a helper object from within a script.
     *
     * If the helper class has a 'view' property, sets it with the current view
     * object.
     *
     * @param string $name The helper name.
     * @param array $args The parameters for the helper.
     * @return string The result of the helper output.
     */
    public function __call($name, $args)
    {
        // is the helper already loaded?
        $helper = $this->getHelper($name);

        // call the helper method
        return call_user_func_array(
            array($helper, $name),
            $args
        );
    }


    /**
     * Includes the view script in a scope with only public $this variables.
     * @param string The view script to execute.
     */
    protected function _run()
    {
		include func_get_arg(0);
    }
public function __clone()
{
    $this->_smarty = clone $this->_smarty;
}

}
