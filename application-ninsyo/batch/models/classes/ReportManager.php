<?php

/**
 * レポート情報を管理し処理するクラス
 *
 * @author fujimoto <fujimoto@d-publishing.jp>
 * @since  2012/05/23
 */
abstract class Batch_Models_Classes_ReportManager {


    /**
     * ファイル名
     * @var string
     */
    private $_logFileName = 'test';

    /**
     * 処理時間関連
     */
    private $_stockProcessTimeSart = array();
    private $_stockProcessTimeEnd  = array();


    /**
     * インスタンスを取得
     *
     *  private static $_instance = null; を忘れない
     */
    abstract public static function getInstance();

// サンプル
//
//    /**
//     * 自身のオブジェクト
//     */
//    private static $_instance = null;
//
//    /**
//     * このクラスのインスタンスを取得する
//     * ※singleton
//     *
//     */
//    public static function getInstance() {
//
//        if (isBlankOrNull(self::$_instance) === true) {
//            self::$_instance = new self();
//            self::$_instance->setStockProcessTimeStart('__TOTAL__');
//        }
//
//        return self::$_instance;
//    }
//
//
// php 5.3以降だとnew static() があるので、ここでインスタンス生成も可能
//self は記述されたクラスに束縛されるのに対して、static は呼び手のクラスに束縛される。
//
//class A {
//    public static function getSelfName() {
//        return get_class(new self());
//    }
//
//    public static function getStaticName() {
//        return get_class(new static());
//    }
//}
//
//class B extends A {
//    public static function getParentName() {
//        return get_class(new parent());
//    }
//}
//
//echo A::getSelfName(),PHP_EOL;   //A
//echo B::getSelfName(),PHP_EOL;   //A
//
//echo A::getStaticName(),PHP_EOL; //A
//echo B::getStaticName(),PHP_EOL; //B
//
//echo B::getParentName(),PHP_EOL; //A
//メソッドを呼び出したクラスの名前の取得には get_called_class() 関数を使うことができる。
//
//class A {
//    static public function getCalledClass() {
//        return get_called_class();
//    }
//}
//
//class B extends A {
//
//}
//
//echo A::getCalledClass(),PHP_EOL; //A
//echo B::getCalledClass(),PHP_EOL; //B


    /**
     * ログファイル名を設定します
     *
     *
     * @param string $key
     */
    public function setLogFileName($name) {
        $this->_logFileName = $name;
    }


    /**
     * 処理時間の計測を開始します
     *
     *
     * @param string $key
     */
    public function setStockProcessTimeStart($key) {
        $this->_stockProcessTimeSart[$key] = time();
    }



    /**
     * 処理時間の計測を終了しますします
     *
     *
     * @param string $key
     */
    public function setStockProcessTimeEnd($key) {
        $this->_stockProcessTimeEnd[$key] =  time();//dumper($this->_stockProcessTimeEnd);exit;
    }



    /**
     * 処理時間の計測を開始した時間をUNIXタイムスタンプで返します
     *
     *
     * @param string $key
     */
    public function getStockProcessTimeStart($key) {
        return $this->_stockProcessTimeSart[$key];
    }



    /**
     * 処理時間の計測を終了した時間をUNIXタイムスタンプで返します
     *
     *
     * @param string $key
     */
    public function getStockProcessTimeEnd($key) {
        return $this->_stockProcessTimeEnd[$key];
    }



    /**
     * 処理時間の計測を開始した時間をUNIXタイムスタンプでログに書込みます
     *
     *
     * @param string $key
     */
    public function logStockProcessTimeStart($key) {
        $this->log($this->_stockProcessTimeSart[$key]);
    }



    /**
     * 処理時間の計測を終了した時間をUNIXタイムスタンプでログに書込みます
     *
     *
     * @param string $key
     */
    public function logStockProcessTimeEnd($key) {
        $this->log($this->_stockProcessTimeEnd[$key]);
    }



    /**
     * 処理時間を返します
     *
     *
     * @param string $key
     */
    public function logStockProcessTimeDifference($key) {

        // キーの存在チェック
        if (isset($this->_stockProcessTimeSart[$key]) === false) {
            $message = "{$key}: START TIME UNKNOWN 秒";
            $this->log($message);
            return $message;
        }
        if (isset($this->_stockProcessTimeEnd[$key]) === false) {
            $message = "{$key}: END TIME UNKNOWN 秒";
            $this->log($message);
            return $message;
        }

        // 差分を取得
        $s = $this->_stockProcessTimeSart[$key];
        $e = $this->_stockProcessTimeEnd[$key];
        $d = $e - $s;
        //$d = round($e-$s,5);
        // メッセージを加工
        $s = date('Y-m-d H:i:s', $s);
        $message = "{$key}: {$d}秒";
        $this->log($message);
        return $message;
    }



    /**
     * 全ての処理時間を返します
     *
     *
     */
    public function logStockProcessTimeDifferenceAll($logWrite = true) {

        //
        $this->setStockProcessTimeEnd('__TOTAL__');

        //
        $keys = array_keys($this->_stockProcessTimeSart);

        $message = '';
        foreach ($keys as $key) {
            $message .= $this->logStockProcessTimeDifference($key) . "\n";
        }

        if ($logWrite === true) {
            $this->log($message);
        }
        return $message;
    }



    /**
     * logTemporary を使用してログの書込みを行います
     *
     *
     * @param string $message
     */
    public function log($message) {
        logTemporary($message, $this->_logFileName, '', 3);
    }



    /**
     * logTemporary を使用して現在のメモリ使用量の書込みを行います
     *
     *
     * @param string $message
     */
    public function logMemoryGetUsage($logWrite = true) {
        $log = ' -- 現在のメモリ使用量：' . number_format(memory_get_usage()) . ' / ピーク時のメモリ最大使用量：' . number_format(memory_get_peak_usage()) . ' --';
        if ($logWrite === true) {
            logTemporary($log, self::$_logFileName, '', 3);
        }
        return $log;
    }


    /**
     * レポートメールを送信します
     *
     *
     */
     public function sendReportMailBase($title,$body) {

        //------------------------------------------------------------------
        // ■本文を生成
        //------------------------------------------------------------------
        $body .= ""                                                               . "\n"
              .  "--------------------------------------------------------------" . "\n"
              .  "[実行時間]"                                                     . "\n"
              .  $this->logStockProcessTimeDifferenceAll(false)                   . "\n"
              .  ""                                                               . "\n"
              .  "[メモリ使用率]"                                                 . "\n"
              .  $this->logMemoryGetUsage(false)                                  . "\n"
              .  "";


        //------------------------------------------------------------------
        // ■送信
        //------------------------------------------------------------------
        $this->log($title . "\n" . $body);
        reportMail($title, $body);
    }
}

















































