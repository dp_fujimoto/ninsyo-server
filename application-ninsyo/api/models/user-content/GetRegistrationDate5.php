<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CreateUserHash.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_GetRegistrationDate5 extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // chopper 7.0用
        // ハッシュパスワードを返すようにした

        // 各コンテンツの経過日数も返す

        // 検索結果
        $output = '';

        $class = new Api_Models_Classes_CreateUserHash();
        $idHash = $class->execute($actionForm->getId());

        if (strlen($actionForm->getPassword()) === HASH_LENGTH) {
            $passwordHash = $actionForm->getPassword();
        } else {
            $passwordHash = $class->execute($actionForm->getPassword());
        }
        $productCode = "'" . str_replace('-',"','",$actionForm->getProductCode()) . "'";

        //-----------------------------------------------
        // ユーザー確認
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT ";
        $sql .= "   muc_service_start_date";
        $sql .= "  ,muc_service_end_date";
        $sql .= "  ,mct_content_id";
        $sql .= " FROM mst_user";
        $sql .= " INNER JOIN mst_user_content ON mus_user_id  = muc_user_id";
        $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND muc_delete_flag = '0'";
        $sql .= "   AND mus_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND (muc_auth_flag = '1' OR (mct_auth_flag = '1' AND muc_auth_flag = '0'))";
        $sql .= "   AND mus_id_hash = ?";
        $sql .= "   AND mus_password_hash = ?";
        $sql .= "   AND mpr_product_code IN (" . $productCode . ")";
        $sql .= "   AND mct_publication_date <= ?";
        $sql .= "   AND mct_application_type = ?";

        $params   = array();
        $params[] = $idHash;
        $params[] = $passwordHash;
        $params[] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');
        $params[] = 'APPLICATION_TYPE_WEB_PAGE_AUTH';

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $min = '';
        $max = '-';
        $ids = array();
        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            if (isBlankOrNull($min)) {
                $min = $v['muc_service_start_date'];
            } else if ($min > $v['muc_service_start_date']) {
                $min = $v['muc_service_start_date'];
            }
            if (isBlankOrNull($v['muc_service_end_date'])) {
                $max = $v['muc_service_end_date'];
            }
            if (!isBlankOrNull($max)) {
                if ($max === '-') {
                    $max = $v['muc_service_end_date'];
                } else if ($max < $v['muc_service_end_date']) {
                    $max = $v['muc_service_end_date'];
                }
            }

            // content単位
            if (!isset($ids[$v['mct_content_id']])) {
                $ids[$v['mct_content_id']] = array();
                $ids[$v['mct_content_id']]['min'] = '';
                $ids[$v['mct_content_id']]['max'] = '-';
            }
            if (isBlankOrNull($ids[$v['mct_content_id']]['min'])) {
                $ids[$v['mct_content_id']]['min'] = $v['muc_service_start_date'];
            } else if ($ids[$v['mct_content_id']]['min'] > $v['muc_service_start_date']) {
                $ids[$v['mct_content_id']]['min'] = $v['muc_service_start_date'];
            }
            if (isBlankOrNull($v['muc_service_end_date'])) {
                $ids[$v['mct_content_id']]['max'] = $v['muc_service_end_date'];
            }
            if (!isBlankOrNull($ids[$v['mct_content_id']]['max'])) {
                if ($ids[$v['mct_content_id']]['max'] === '-') {
                    $ids[$v['mct_content_id']]['max'] = $v['muc_service_end_date'];
                } else if ($ids[$v['mct_content_id']]['max'] < $v['muc_service_end_date']) {
                    $ids[$v['mct_content_id']]['max'] = $v['muc_service_end_date'];
                }
            }
        }
        if (isBlankOrNull($max)) {
            $max = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');
        }

        $tempArr = array();
        foreach ($ids as $contentId => $v) {
            if (isBlankOrNull($v['max'])) {
                $v['max'] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');
            }
            $tempArr[] = $contentId . '|' . str_replace('-','/',$v['min']) . '|' . str_replace('-','/',$v['max']);
        }

        $output = $min . ',' . $max . ',' . implode('-',$tempArr) . ',' . $passwordHash;

        echo $output;

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
