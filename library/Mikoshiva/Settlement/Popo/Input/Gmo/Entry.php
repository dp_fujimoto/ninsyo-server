<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Input_Entry
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Entry extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'ShopID' => null,
        'ShopPass' => null,
        'OrderID' => null,
        'JobCd' => null,
        'ItemCode' => null,
        'Amount' => null,
        'Tax' => null,
        'TdFlag' => null,
        'TdTenantName' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * ShopID
	 *
     * @return  ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getShopID() {
		return $this->_properties['ShopID'];
    }


    /**
     * ShopID
	 *
	 * @param  $ShopID ShopID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setShopID($ShopID) {
		$this->_properties['ShopID'] = $ShopID;
    }


    /**
     * ShopPass
	 *
     * @return  ShopPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getShopPass() {
		return $this->_properties['ShopPass'];
    }


    /**
     * ShopPass
	 *
	 * @param  $ShopPass ShopPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setShopPass($ShopPass) {
		$this->_properties['ShopPass'] = $ShopPass;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * JobCd
	 *
     * @return  JobCd
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getJobCd() {
		return $this->_properties['JobCd'];
    }


    /**
     * JobCd
	 *
	 * @param  $JobCd JobCd
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setJobCd($JobCd) {
		$this->_properties['JobCd'] = $JobCd;
    }


    /**
     * ItemCode
	 *
     * @return  ItemCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getItemCode() {
		return $this->_properties['ItemCode'];
    }


    /**
     * ItemCode
	 *
	 * @param  $ItemCode ItemCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setItemCode($ItemCode) {
		$this->_properties['ItemCode'] = $ItemCode;
    }


    /**
     * Amount
	 *
     * @return  Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAmount() {
		return $this->_properties['Amount'];
    }


    /**
     * Amount
	 *
	 * @param  $Amount Amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAmount($Amount) {
		$this->_properties['Amount'] = $Amount;
    }


    /**
     * Tax
	 *
     * @return  Tax
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getTax() {
		return $this->_properties['Tax'];
    }


    /**
     * Tax
	 *
	 * @param  $Tax Tax
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setTax($Tax) {
		$this->_properties['Tax'] = $Tax;
    }


    /**
     * TdFlag
	 *
     * @return  TdFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getTdFlag() {
		return $this->_properties['TdFlag'];
    }


    /**
     * TdFlag
	 *
	 * @param  $TdFlag TdFlag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setTdFlag($TdFlag) {
		$this->_properties['TdFlag'] = $TdFlag;
    }


    /**
     * TdTenantName
	 *
     * @return  TdTenantName
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getTdTenantName() {
		return $this->_properties['TdTenantName'];
    }


    /**
     * TdTenantName
	 *
	 * @param  $TdTenantName TdTenantName
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Entry.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setTdTenantName($TdTenantName) {
		$this->_properties['TdTenantName'] = $TdTenantName;
    }


}

























