<?php
require_once 'PHPUnit/Framework.php';

require_once('../DPMemberShip/DPAuthzMerge.php');

class MemberShipTest extends PHPUnit_Framework_TestCase
{
	public function test_load(){
		$data = array("hoge1@example.jp,LOADTEST",
		"hoge2@example.jp,LOADTEST",
		"hoge3@example.jp,LOADTEST");
		
    	$loadtest = AUTH_FILE_SERVER_DIR."/LOADTEST.csv";
    	$loadtest2 = AUTH_FILE_SERVER_DIR."/LOD201101.csv";
		copy($loadtest,$loadtest2);
		$authz = new DPAuthzMerge(MERGE_KEY,'LOD','201101');
		$users = $authz->_loadAuthz(false);
		$this->assertEquals($data,$users);
    }
    
    public function testmerge(){
    	$mlt = AUTH_FILE_SERVER_DIR."/LOD201101.csv";
    	$loadtest = AUTH_FILE_SERVER_DIR."/LOADTEST.csv";
		copy($loadtest,$mlt);
		
		$authz = new DPAuthzMerge(MERGE_KEY,'LOD','201101');
		$res = $authz->merge("hoge4@example.jp,LOD\nhoge5@example.jp,LOD\n");
		
		//$this->assertEquals($res,true);
		//
		$data = array("hoge1@example.jp,LOADTEST",
		"hoge2@example.jp,LOADTEST",
		"hoge3@example.jp,LOADTEST",
		"hoge4@example.jp,LOD",
		"hoge5@example.jp,LOD");
		
		$m = $authz->_loadAuthz(false);
		$this->assertEquals($data,$m);
		
		unlink($mlt);
    }
    // 初期登録
    public function testmerge2(){
    	$mlt = AUTH_FILE_SERVER_DIR."/LOD201101.csv";
    	
		
		$authz = new DPAuthzMerge(MERGE_KEY,'LOD','201101');
		$res =$authz->merge("hoge4@example.jp,LOD\nhoge5@example.jp,LOD\n");
	
		$this->assertTrue($res);
		$data = array("hoge4@example.jp,LOD",
		"hoge5@example.jp,LOD");
		$m = $authz->_loadAuthz(false);
		$this->assertEquals($data,$m);
		unlink($mlt);
    }
    // 重複登録防止
    public function testmerge3(){
    	$mlt = AUTH_FILE_SERVER_DIR."/LOD201101.csv";
    	$loadtest = AUTH_FILE_SERVER_DIR."/LOADTEST.csv";
		copy($loadtest,$mlt);
		
		$authz = new DPAuthzMerge(MERGE_KEY,'LOD','201101');
		$res = $authz->merge("hoge1@example.jp,LOADTEST\nhoge5@example.jp,LOD\n");
	
		$this->assertTrue($res);
		//
		$data = array("hoge1@example.jp,LOADTEST",
		"hoge2@example.jp,LOADTEST",
		"hoge3@example.jp,LOADTEST",
		"hoge5@example.jp,LOD");
		$m = $authz->_loadAuthz(false);
		$this->assertEquals($data,$m);
		
		unlink($mlt);
    }
    
}
