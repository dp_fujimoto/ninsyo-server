<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstExternalCooperation
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstExternalCooperation extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_external_cooperation';
    protected $_primary  = 'meo_external_cooperation_id';
    protected $_metadata = array (
  'meo_external_cooperation_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_external_cooperation_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'meo_external_cooperation_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_external_cooperation_name',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'meo_plugin' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_plugin',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'meo_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_delete_flag',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'meo_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_deletion_datetime',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'meo_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_update_datetime',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'meo_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_registration_datetime',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'meo_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_external_cooperation',
    'COLUMN_NAME' => 'meo_update_timestamp',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

