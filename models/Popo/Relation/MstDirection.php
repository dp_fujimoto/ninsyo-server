<?php
/** @package Popo */

require_once 'Popo/MstDirection.php';


/**
 * mst_direction
 *
 * @package Popo
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: MstDirection.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
 */
class Popo_Relation_MstDirection extends Popo_MstDirection {

    /**
     * 指示ステータス配列 <指示ステータスPOPO>
     * @var $_directionStatus array <Popo_TrxDirectionStatus>
     */
    protected $_directionStatus = array();


    /**
     * getDirectionStatus
	 *
     * @return array <Popo_TrxDirectionStatus>
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: MstDirection.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
     *
     */
    public function getDirectionStatus() {
		return $this->_directionStatus;
    }


    /**
     * setDirectionStatus
	 *
	 * @param array $directionStatus <Popo_TrxDirectionStatus>
	 *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: MstDirection.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
     *
     */
    public function setDirectionStatus(array $directionStatus) {
		$this->_directionStatus = $directionStatus;
    }


}

























