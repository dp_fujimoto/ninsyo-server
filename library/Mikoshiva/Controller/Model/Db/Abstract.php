<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Zend/Db/Table/Abstract.php';
require_once 'Mikoshiva/Controller/Model/Abstract.php';
require_once 'Mikoshiva/Db/Simple.php';

/**
 * Model_Db_Abstract
 * Model クラスを作成する際は、必ずこのクラスを継承し、
 * execute メソッドを実行メソッドすること
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Model_Db_Abstract extends Mikoshiva_Controller_Model_Abstract {

    /**
     * DB クラス
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    /**
     * リクエストクラスと DB クラスを初期化します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @return unknown_type
     */
    public function __construct() {
        // スーパークラスのメソッドを呼びます
        parent::__construct();

        // 各種コンポーネントを取得
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
    }

}










