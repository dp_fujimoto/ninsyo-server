<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_execution_writing
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstExecutionWriting extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mew';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'executionWritingId' => null,
        'divisionId' => null,
        'productId' => null,
        'executionWritingName' => null,
        'executionWritingFrontBody' => null,
        'executionWritingTimeBody' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mew_execution_writing_id の値を返します
	 *
     * @return int mew_execution_writing_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionWritingId() {
		return $this->_properties['executionWritingId'];
    }


    /**
     * mew_execution_writing_id の値をセットします
	 *
	 * @param int $executionWritingId mew_execution_writing_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionWritingId($executionWritingId) {
		$this->_properties['executionWritingId'] = $executionWritingId;
    }


    /**
     * mew_division_id の値を返します
	 *
     * @return int mew_division_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * mew_division_id の値をセットします
	 *
	 * @param int $divisionId mew_division_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * mew_product_id の値を返します
	 *
     * @return int mew_product_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * mew_product_id の値をセットします
	 *
	 * @param int $productId mew_product_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * mew_execution_writing_name の値を返します
	 *
     * @return varchar mew_execution_writing_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionWritingName() {
		return $this->_properties['executionWritingName'];
    }


    /**
     * mew_execution_writing_name の値をセットします
	 *
	 * @param varchar $executionWritingName mew_execution_writing_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionWritingName($executionWritingName) {
		$this->_properties['executionWritingName'] = $executionWritingName;
    }


    /**
     * mew_execution_writing_front_body の値を返します
	 *
     * @return mediumtext mew_execution_writing_front_body
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionWritingFrontBody() {
		return $this->_properties['executionWritingFrontBody'];
    }


    /**
     * mew_execution_writing_front_body の値をセットします
	 *
	 * @param mediumtext $executionWritingFrontBody mew_execution_writing_front_body
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionWritingFrontBody($executionWritingFrontBody) {
		$this->_properties['executionWritingFrontBody'] = $executionWritingFrontBody;
    }


    /**
     * mew_execution_writing_time_body の値を返します
	 *
     * @return mediumtext mew_execution_writing_time_body
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionWritingTimeBody() {
		return $this->_properties['executionWritingTimeBody'];
    }


    /**
     * mew_execution_writing_time_body の値をセットします
	 *
	 * @param mediumtext $executionWritingTimeBody mew_execution_writing_time_body
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionWritingTimeBody($executionWritingTimeBody) {
		$this->_properties['executionWritingTimeBody'] = $executionWritingTimeBody;
    }


    /**
     * mew_delete_flag の値を返します
	 *
     * @return char mew_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mew_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mew_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mew_deletion_datetime の値を返します
	 *
     * @return datetime mew_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mew_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mew_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mew_update_datetime の値を返します
	 *
     * @return datetime mew_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mew_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mew_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mew_registration_datetime の値を返します
	 *
     * @return datetime mew_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mew_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mew_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mew_update_timestamp の値を返します
	 *
     * @return timestamp mew_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mew_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mew_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























