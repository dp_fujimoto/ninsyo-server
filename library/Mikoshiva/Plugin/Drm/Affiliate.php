<?php

require_once 'Zend/Http/Client.php';
require_once 'Zend/Http/Client/Adapter/Socket.php';
require_once 'Zend/Http/Client/Adapter/Exception.php';

require_once 'Mikoshiva/Plugin/Drm/Abstruct.php';

/**
 * DRMアフィリエイト関連のDRMAPIを呼び出します。
 *
 * @author k.shimowaki
 * @since version - 2010/03/18
 */
class Mikoshiva_Plugin_Drm_Affiliate extends Mikoshiva_Plugin_Drm_Abstruct {

	/**
	 * DRMへアフィリエイト登録を行います。
	 *
	 * @param Popo_MstCustomer $customer 顧客情報
	 * @param int $affiliateHtmlFormId アフィリエイトHTMLフォームID
	 * @return string
	 *
	 * @author k.shimowaki
	 * @since version - 2010/03/18
	 */
	public function createAffiliate(Popo_MstCustomer $customer, $affiliateHtmlFormId) {

		$c = new Popo_MstCustomer();
		$c = $customer;

		// パラメータセット
		$this->client->setParameterPost('pathCode','AF_CREATE');							// パスコード

		$this->client->setParameterPost('affiliateHtmlFormId', $affiliateHtmlFormId);		// アフィリエイトHTMLフォームID
		$this->client->setParameterPost('affiliateLastName', $c->getLastName());			// 姓
		$this->client->setParameterPost('affiliateFirstName', $c->getFirstName());			// 名
		$this->client->setParameterPost('affiliateLastNameKana', $c->getLastNameKana());	// 姓カナ
		$this->client->setParameterPost('affiliateFirstNameKana', $c->getFirstNameKana());	// 名カナ
		$this->client->setParameterPost('email', $c->getEmail());							// メールアドレス
		$this->client->setParameterPost('zip', $c->getZip());								// 郵便番号
		$this->client->setParameterPost('prefectureName', $c->getPrefecture());				// 都道府県
		$this->client->setParameterPost('address1', $c->getAddress1());						// 住所１
		$this->client->setParameterPost('address2', $c->getAddress2());						// 住所２
		$this->client->setParameterPost('tel', $c->getTel());								// 電話番号
		$this->client->setParameterPost('fax', $c->getFax());								// FAX番号
		$this->client->setParameterPost('companyName', $c->getCompanyName());				// 会社名
		$this->client->setParameterPost('registIpAddress', '111.111.111.111');				// TODO IPアドレス

		$response = null;
		// 実行
		try{

			logDebug($this->client->getParameterRequest());
			logDebug($this->client->getUri());

			// 実行
			$response = $this->client->request('POST');
		} catch (Exception $e) {

			// エラー処理
			$this->error($e);
			return $this->retFormat($response);
		}
		logDebug($this->client->getLastRequest());
		logDebug($this->client->getLastResponse());

		return $this->retFormat($response->getBody());

	}

	/**
	 * DRMへアフィリエイト削除を行います。
	 *
	 * @param Popo_MstCustomer $customer 顧客情報
	 * @param int $affiliateHtmlFormId アフィリエイトHTMLフォームID
	 * @return string
	 *
	 * @author k.shimowaki
	 * @since version - 2010/03/18
	 */
	public function cancelAffiliate(Popo_MstCustomer $customer, $affiliateHtmlFormId) {

		$c = new Popo_MstCustomer();
		$c = $customer;

		// パラメータセット
		$this->client->setParameterPost('pathCode','AF_CANCEL');							// パスコード

		$this->client->setParameterPost('affiliateHtmlFormId', $affiliateHtmlFormId);		// アフィリエイトHTMLフォームID
		$this->client->setParameterPost('email', $c->getEmail());							// メールアドレス

		$response = null;
		// 実行
		try{

			logDebug($this->client->getParameterRequest());
			logDebug($this->client->getUri());

			// 実行
			$response = $this->client->request('POST');
		} catch (Exception $e) {

			// エラー処理
			$this->error($e);
			return $this->retFormat($response);
		}
		logDebug($this->client->getLastRequest());
		logDebug($this->client->getLastResponse());

		return $this->retFormat($response->getBody());

	}

	/**
	 * DRMへ売上登録を行います。
	 *
	 * @param array $drmCookie DRM用クッキー
	 * @param string $productKey 商品キー
	 * @param string $orderNo オーダーNo
	 * @return string
	 *
	 * @author k.shimowaki
	 * @since version - 2010/03/18
	 */
	public function createSales(array $drmCookie, $productKey, $orderNo) {

		// パラメータセット
		$this->client->setParameterPost('pathCode','SALES_CREATE');							// パスコード

		$this->client->setParameterPost('cookie', $drmCookie);								// DRMクッキー
		$this->client->setParameterPost('productKey', $productKey);							// 商品キー
		$this->client->setParameterPost('orderNo', $orderNo);								// オーダーNo（pdid=tpd_purchase_detail_id)

		$response = null;
		// 実行
		try{

			logDebug($this->client->getParameterRequest());
			logDebug($this->client->getUri());

			// 実行
			$response = $this->client->request('POST');
		} catch (Exception $e) {

			// エラー処理
			$this->error($e);
			return $this->retFormat($response);
		}
		logDebug($this->client->getLastRequest());
		logDebug($this->client->getLastResponse());

		return $this->retFormat($response->getBody());

	}

	/**
	 * DRMへ継続売上の停止を行います。
	 *
	 * @param string $productKey 商品キー
	 * @param int $purchaseDetailId 購入履歴詳細ID
	 * @return string
	 *
	 * @author k.shimowaki
	 * @since version - 2010/03/18
	 */
	public function cancelContinuitySales($productKey, $purchaseDetailId) {

		// パラメータセット
		$this->client->setParameterPost('pathCode','CONTINUITY_COMMISSION_CANCEL');			// パスコード

		$this->client->setParameterPost('productKey', $productKey);							// 商品キー
		$this->client->setParameterPost('purchaseDetailId', $purchaseDetailId);				// 購入履歴詳細ID

		$response = null;
		// 実行
		try{

			logDebug($this->client->getParameterRequest());
			logDebug($this->client->getUri());

			// 実行
			$response = $this->client->request('POST');
		} catch (Exception $e) {

			// エラー処理
			$this->error($e);
			return $this->retFormat($response);
		}
		logDebug($this->client->getLastRequest());
		logDebug($this->client->getLastResponse());

		return $this->retFormat($response->getBody());

	}

    /**
     * DRMへ売上の停止を行います。
     *
     * @param string $productKey 商品キー
     * @param int $purchaseDetailId 購入履歴詳細ID
     * @return string
     *
     * @author k.shimowaki
     * @since version - 2012/05/22
     */
    public function cancelSales($productKey, $purchaseDetailId) {

        // パラメータセット
        $this->client->setParameterPost('pathCode','SALES_CANCEL');                         // パスコード

        $this->client->setParameterPost('productKey', $productKey);                         // 商品キー
        $this->client->setParameterPost('purchaseDetailId', $purchaseDetailId);             // 購入履歴詳細ID

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

}