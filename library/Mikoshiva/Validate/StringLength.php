<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_StringLength
 */
require_once 'Zend/Validate/StringLength.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: StringLength.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_StringLength extends Zend_Validate_StringLength
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::TOO_SHORT => "【%COLUMN_NAME%】は %min% 文字以上で入力して下さい。",
        self::TOO_LONG  => "【%COLUMN_NAME%】は %max% 文字以内で入力して下さい。"
    );

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if the string length of $value is at least the min option and
     * no greater than the max option (when the max option is not null).
     *
     * @param  string $value
     * @return boolean
     */
    public function isValid($value)
    {
        if (!is_string($value)) {
            $this->_error(self::INVALID);
            return false;
        }

        $this->_setValue($value);
        if ($this->_encoding !== null) {
            $length = mb_strlen($value, ini_get('mbstring.internal_encoding'));
        } else {
            $length = mb_strlen($value, ini_get('mbstring.internal_encoding'));
        }

        if ($length < $this->_min) {
            $this->_error(self::TOO_SHORT);
        }

        if (null !== $this->_max && $this->_max < $length) {
            $this->_error(self::TOO_LONG);
        }

        if (count($this->_messages)) {
            return false;
        } else {
            return true;
        }
    }
}
