<?php
/**
 * @package Mikoshiva_Utility
 */



/**
 * 変換ユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: Convert.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Convert {
    /**
     * フォームのname・変数名を、DBのカラム名に変換します。
     *
     * 入力される文字列は、以下の形式で作成されているものとして扱います。
     * 　　DBのカラム名からプレフィックスの3文字とアンダースコアを除外し、
     * 　　先頭の単語以外をキャピタライズした文字列
     *
     * @param string $name
     * @param string $prefix
     * @return string
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/18
     * @version SVN:$Id: Convert.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function variableNameToColumnName($name, $prefix) {
        //logInfo(LOG_START);

        $columnName = '';
        $columnName = strtolower(preg_replace('/([A-Z])/', '_${1}', $name) );

        //logInfo(LOG_END);

        return $prefix . '_' . $columnName;
    }


    /**
     * DBのカラム名を、フォームのname・変数名に変換します。
     *
     * 出力される文字列は、以下の形式です。
     * 　　DBのカラム名からプレフィックスの3文字とアンダースコアを除外し、
     * 　　先頭の単語以外をキャピタライズした文字列
     *
     * @param string $columnName
     * @return string
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/18
     * @version SVN:$Id: Convert.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function columnNameToVariableName($columnName) {
        //logInfo(LOG_START);


        $parts = array();
        $parts = explode('_', $columnName);
        //テーブル毎のプレフィックスを削除
        array_shift($parts);

        $work   = array();
        $work[] = array_shift($parts);
        //単語の先頭を大文字にして、再度連結
        foreach($parts as $v) {
            $work[] = ucfirst($v);
        }

        $name = '';
        $name = implode('', $work);

        //logInfo(LOG_END);

        return $name;
    }

    /**
     * 引数の値が空文字の場合nullに変換して返します。
     * それ以外の値の場合は引数の値をそのまま返します。
     *
     * @param mixed $value
     * @return null | mixed
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2010/02/23
     * @version SVN:$Id: Convert.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function blankToNull($value) {

        if (!is_array($value) && !is_object($value) && isBlankOrNull($value)) {
            return null;
        } else {
            return $value;
        }

    }


    /**
     * template に含まれる %～% で囲まれた文字列と
     * $replaceList のキー名がマッチしたものを $replaceList の値で置換を行います
     *
     * また、値が POPO である場合は POPO のプロパティをがマッチしたものを
     * プロパティの値で置換を行います
     *
     * ※ POPO は Popo_Table_Abstract を継承しているものに限ります
     *
     * @param string $template
     * @param array  $replaceList
     * @return string 置換処理を行ったテンプレート
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/23
     * @version SVN:$Id: Convert.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function templateReplacer($template, array $replaceList) {


        include_once 'Mikoshiva/Utility/Mapping.php';

        // 入れ替え
        $_template = $template;


        //------------------------------------------
        // ■置換処理
        //------------------------------------------
        foreach ($replaceList as $key => $value) {

            if ($value instanceof Popo_Table_Abstract) {

                //-------------------------------------------
                // POPO の場合
                //-------------------------------------------
                $popoProperties = array();
                $popoProperties = $value->getProperties();

                // Prefix を取得
                $prefix = '';
                $prefix = $value->getColumnPrefix();


                // キー名をカラム名に戻し置換を行う
                foreach ($popoProperties as $k => $v) {

                    // カラム名を生成
                    $columnName = '';
                    $columnName = self::variableNameToColumnName($k, $prefix);

                    // 大文字にする
                    $columnName = strtoupper($columnName);

                    // 置換後の文字列がステータスで有れば日本語名称に置き換え
                    $_v = null;
                    $_v = $v;
                    try {
                        $_v = Mikoshiva_Utility_Mapping::statusMapping($_v);
                    } catch(Exception $e) {
                        // 何もしない
                    }


                    $_template = str_replace("%{$columnName}%",  $_v, $_template);
                }

            } else {

                //-------------------------------------------
                // POPO 以外の場合
                //-------------------------------------------
                // 置換後の文字列がステータスで有れば日本語名称に置き換え
                $_v = null;
                $_v = $value;
                try {
                    $_v = Mikoshiva_Utility_Mapping::statusMapping($_v);
                } catch(Exception $e) {
                    // 何もしない
                }

                $_template = str_replace("%{$key}%",  $_v, $_template);
            }
        }



        return $_template;
    }


    /**
     * クレジットカードをケツ４桁以降を隠蔽します
     *
     * @param $cardNo
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/03
     * @version SVN:$Id: Convert.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function creditCardSecret($cardNo) {


        //-------------------------------------------
        // ■形式チェック
        //--------------------------------------------
        if (preg_match('#^\d{16}$#', $cardNo) !== 1) {

            // ▼クレジットカードの形式が不正で有れば例外を投げる
            include_once 'Mikoshiva/Utility/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】”{$cardNo}” はクレジットカード番号の形式ではありません。";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }


        return 'XXXXXXXXXXXX' . substr($cardNo, -4);
    }



    /**
     * カラム名を日本語名称にします
     *
     *
     * @param array $columnList カラム名のみの配列
     * @return array
     */
    public static function columnNmaeJpMapper(array $columnList) {

        // カラム名のリストの読み込み
        // 配列が巨大なため、メソッド内で include を行います
        include_once CONFIGS_DIR . '/' . _SERVER_MODE_ID_ . '/column-jp-mapping.php';

        // 日本語名称の配列を生成
        $jpList = array();
        foreach ($columnList as $columnName) {
            $jpList[$columnName] = isset($aryMap[$columnName]) ? $aryMap[$columnName] : '';
        }

        // メモリ上から削除
        unset($aryMap);

        return $jpList;
    }
}











































