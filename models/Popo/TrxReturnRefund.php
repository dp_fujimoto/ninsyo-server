<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_return_refund
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxReturnRefund extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'trr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'returnRefundId' => null,
        'divisionId' => null,
        'customerId' => null,
        'shippingId' => null,
        'chargeId' => null,
        'directInstallmentPaymentId' => null,
        'shopId' => null,
        'requestStaffId' => null,
        'requestDatetime' => null,
        'requestFlag' => null,
        'refundStaffId' => null,
        'refundAmount' => null,
        'refundType' => null,
        'refundOrderId' => null,
        'refundDatetime' => null,
        'refundFlag' => null,
        'refundErrorFlag' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 返金 ID の値を返します
	 *
     * @return int 返金 ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnRefundId() {
		return $this->_properties['returnRefundId'];
    }


    /**
     * 返金 ID の値をセットします
	 *
	 * @param int $returnRefundId 返金 ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnRefundId($returnRefundId) {
		$this->_properties['returnRefundId'] = $returnRefundId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 発送 ID の値を返します
	 *
     * @return int 発送 ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * 発送 ID の値をセットします
	 *
	 * @param int $shippingId 発送 ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * 請求 ID の値を返します
	 *
     * @return int 請求 ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * 請求 ID の値をセットします
	 *
	 * @param int $chargeId 請求 ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * ダイレクト分割払い請求ID の値を返します
	 *
     * @return int ダイレクト分割払い請求ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPaymentId() {
		return $this->_properties['directInstallmentPaymentId'];
    }


    /**
     * ダイレクト分割払い請求ID の値をセットします
	 *
	 * @param int $directInstallmentPaymentId ダイレクト分割払い請求ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPaymentId($directInstallmentPaymentId) {
		$this->_properties['directInstallmentPaymentId'] = $directInstallmentPaymentId;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * 返金依頼スタッフID の値を返します
	 *
     * @return int 返金依頼スタッフID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRequestStaffId() {
		return $this->_properties['requestStaffId'];
    }


    /**
     * 返金依頼スタッフID の値をセットします
	 *
	 * @param int $requestStaffId 返金依頼スタッフID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRequestStaffId($requestStaffId) {
		$this->_properties['requestStaffId'] = $requestStaffId;
    }


    /**
     * 返金依頼日時 の値を返します
	 *
     * @return datetime 返金依頼日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRequestDatetime() {
		return $this->_properties['requestDatetime'];
    }


    /**
     * 返金依頼日時 の値をセットします
	 *
	 * @param datetime $requestDatetime 返金依頼日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRequestDatetime($requestDatetime) {
		$this->_properties['requestDatetime'] = $requestDatetime;
    }


    /**
     * 返金依頼済フラグ の値を返します
	 *
     * @return char 返金依頼済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRequestFlag() {
		return $this->_properties['requestFlag'];
    }


    /**
     * 返金依頼済フラグ の値をセットします
	 *
	 * @param char $requestFlag 返金依頼済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRequestFlag($requestFlag) {
		$this->_properties['requestFlag'] = $requestFlag;
    }


    /**
     * 返金スタッフID の値を返します
	 *
     * @return int 返金スタッフID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundStaffId() {
		return $this->_properties['refundStaffId'];
    }


    /**
     * 返金スタッフID の値をセットします
	 *
	 * @param int $refundStaffId 返金スタッフID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundStaffId($refundStaffId) {
		$this->_properties['refundStaffId'] = $refundStaffId;
    }


    /**
     * 返金金額 の値を返します
	 *
     * @return int 返金金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundAmount() {
		return $this->_properties['refundAmount'];
    }


    /**
     * 返金金額 の値をセットします
	 *
	 * @param int $refundAmount 返金金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundAmount($refundAmount) {
		$this->_properties['refundAmount'] = $refundAmount;
    }


    /**
     * 返金タイプ の値を返します
	 *
     * @return varchar 返金タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundType() {
		return $this->_properties['refundType'];
    }


    /**
     * 返金タイプ の値をセットします
	 *
	 * @param varchar $refundType 返金タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundType($refundType) {
		$this->_properties['refundType'] = $refundType;
    }


    /**
     * 返金オーダーID の値を返します
	 *
     * @return char 返金オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundOrderId() {
		return $this->_properties['refundOrderId'];
    }


    /**
     * 返金オーダーID の値をセットします
	 *
	 * @param char $refundOrderId 返金オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundOrderId($refundOrderId) {
		$this->_properties['refundOrderId'] = $refundOrderId;
    }


    /**
     * 返金日時 の値を返します
	 *
     * @return datetime 返金日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundDatetime() {
		return $this->_properties['refundDatetime'];
    }


    /**
     * 返金日時 の値をセットします
	 *
	 * @param datetime $refundDatetime 返金日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundDatetime($refundDatetime) {
		$this->_properties['refundDatetime'] = $refundDatetime;
    }


    /**
     * 返金済フラグ の値を返します
	 *
     * @return char 返金済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundFlag() {
		return $this->_properties['refundFlag'];
    }


    /**
     * 返金済フラグ の値をセットします
	 *
	 * @param char $refundFlag 返金済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundFlag($refundFlag) {
		$this->_properties['refundFlag'] = $refundFlag;
    }


    /**
     * 返金エラーフラグ の値を返します
	 *
     * @return char 返金エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundErrorFlag() {
		return $this->_properties['refundErrorFlag'];
    }


    /**
     * 返金エラーフラグ の値をセットします
	 *
	 * @param char $refundErrorFlag 返金エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundErrorFlag($refundErrorFlag) {
		$this->_properties['refundErrorFlag'] = $refundErrorFlag;
    }


    /**
     * メモ の値を返します
	 *
     * @return varchar メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param varchar $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























