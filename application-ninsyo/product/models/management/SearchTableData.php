<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'tool/models/Operation.php';

/**
 * モデルクラス
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/02/03
 * @version        SVN: $Id:
 *
 */
class Product_Models_Management_SearchTableData extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // リターンするクラス配列
        $arrayClass = array();

        // 初期化
        $mode = $actionForm->getMode();
        $updateFlag = $actionForm->getUpdateFlag();

        $contentAllId = $actionForm->getContentAllId();

        $data = Mikoshiva_Utility_Copy::copyToArray($actionForm);
        foreach ($data as $k => $v) {
            if (!empty($contentAllId)) {
                $data[$k] = null;
            } else {
                $data[$k] = Mikoshiva_Utility_Convert::blankToNull($v);
            }
        }
        if (!empty($contentAllId)) {
            $data['tctContentId'] = $contentAllId;
        }

        $data['mode'] = $mode;
        $data['updateFlag'] = $updateFlag;
        Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($actionForm,$data);

        //-----------------------------------------------
        // ｽﾃｰﾀｽ名一覧作成
        //-----------------------------------------------
        $statusArr = array();
        $statusList = array();
        $temp = $context->getConfig();
        foreach ($temp['SHORT_CODE_MAP_LIST'] as $k => $v) {
            foreach ($v as $k2 => $v2) {
                $statusArr[$k2] = $v2;
            }
            $statusList[$k] = '1';
        }
        $actionForm->setStatusName($statusArr);
        $actionForm->setStatusList($statusList);

        //-----------------------------------------------
        // 更新テーブル情報作成
        //-----------------------------------------------
        $tableList = array();
        $tableList['mbr'] = array();
        $tableList['mbr']['name'] = 'ブランド';
        $tableList['mbr']['table'] = 'mst_brand';
        $tableList['mbr']['primary'] = 'mbr_brand_id';
        $tableList['mbr']['no'] = '1';
        $tableList['mpr'] = array();
        $tableList['mpr']['name'] = '商品';
        $tableList['mpr']['table'] = 'mst_product';
        $tableList['mpr']['primary'] = 'mpr_product_id';
        $tableList['mpr']['no'] = '2';
        $tableList['mct'] = array();
        $tableList['mct']['name'] = 'コンテンツ';
        $tableList['mct']['table'] = 'mst_content';
        $tableList['mct']['primary'] = 'mct_content_id';
        $tableList['mct']['no'] = '3';
        $tableList['mcd'] = array();
        $tableList['mcd']['name'] = '条件';
        $tableList['mcd']['table'] = 'mst_condition';
        $tableList['mcd']['primary'] = 'mcd_condition_id';
        $tableList['mcd']['no'] = '4';
        $tableList['mus'] = array();
        $tableList['mus']['name'] = 'ユーザー';
        $tableList['mus']['table'] = 'mst_user';
        $tableList['mus']['primary'] = 'mus_user_id';
        $tableList['mus']['no'] = '5';
        $tableList['muc'] = array();
        $tableList['muc']['name'] = 'ユーザーコンテンツ';
        $tableList['muc']['table'] = 'mst_user_content';
        $tableList['muc']['primary'] = 'muc_user_content_id';
        $tableList['muc']['no'] = '6';

        $tableNameList = array();
        foreach ($tableList as $k => $v) {
            $tableNameList[] = $v['table'];
        }

        $actionForm->setTableList($tableList);
        $actionForm->setColumnName(Mikoshiva_Utility_Convert::columnNmaeJpMapper($tableNameList));


        // 検索開始
        if ($mode === 'search') {

            //-----------------------------------------------
            // コンテンツ
            //-----------------------------------------------
            if (!isBlankOrNull($contentAllId) ||
                !isBlankOrNull($actionForm->getMctContentId()) ||
                !isBlankOrNull($actionForm->getMctProductId())) {

                // SQL作成
                $sql = '';
                $sql .= 'SELECT';
                $sql .= ' *';
                $sql .= ' FROM mst_content';
                $sql .= ' WHERE 1 = 1 ';

                // パラメータセット
                $params = array();

                if (!isBlankOrNull($actionForm->getMctContentId())) {
                    $sql .= ' AND mct_content_id IN (?)';
                    $params[] = explode(',',$actionForm->getMctContentId());
                }
                if (!isBlankOrNull($actionForm->getMctProductId())) {
                    $sql .= ' AND mct_product_id IN (?)';
                    $params[] = explode(',',$actionForm->getMctProductId());
                }
                if (!isBlankOrNull($contentAllId)) {
                    $sql .= ' AND mct_content_id IN (?)';
                    $params[] = explode(',',$contentAllId);
                }

                $sql .= ' ORDER BY mct_content_id ';

                // SQL実行
                $res = array();
                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                $res = $this->_db->query($sql);

                $tempProductId = array();
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $arrayClass['mct'][] = $v;
                    $tempProductId[$v['mct_product_id']] = $v['mct_product_id'];
                }

                if (!isBlankOrNull($contentAllId)) {
                    $actionForm->setMprProductId(implode(',',$tempProductId));
                }
            }

            //-----------------------------------------------
            // 商品
            //-----------------------------------------------
            if (!isBlankOrNull($actionForm->getMprBrandId()) ||
                !isBlankOrNull($actionForm->getMprProductId())) {

                // SQL作成
                $sql = '';
                $sql .= 'SELECT';
                $sql .= ' *';
                $sql .= ' FROM mst_product';
                $sql .= ' WHERE 1 = 1 ';

                // パラメータセット
                $params = array();

                if (!isBlankOrNull($actionForm->getMprBrandId())) {
                    $sql .= ' AND mpr_brand_id IN (?)';
                    $params[] = explode(',',$actionForm->getMprBrandId());
                }
                if (!isBlankOrNull($actionForm->getMprProductId())) {
                    $sql .= ' AND mpr_product_id IN (?)';
                    $params[] = explode(',',$actionForm->getMprProductId());
                }

                $sql .= ' ORDER BY mpr_product_id ';

                // SQL実行
                $res = array();
                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                $res = $this->_db->query($sql);

                $tempBrandId = array();
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $arrayClass['mpr'][] = $v;
                    $tempBrandId[$v['mpr_brand_id']] = $v['mpr_brand_id'];
                }

                if (!isBlankOrNull($contentAllId)) {
                    $actionForm->setMbrBrandId(implode(',',$tempBrandId));
                }
            }

            //-----------------------------------------------
            // ブランド
            //-----------------------------------------------
            if (!isBlankOrNull($actionForm->getMbrBrandId()) ||
                !isBlankOrNull($actionForm->getMbrDivisionId())) {

                // SQL作成
                $sql = '';
                $sql .= 'SELECT';
                $sql .= ' *';
                $sql .= ' FROM mst_brand';
                $sql .= ' WHERE 1 = 1 ';

                // パラメータセット
                $params = array();

                if (!isBlankOrNull($actionForm->getMbrBrandId())) {
                    $sql .= ' AND mbr_brand_id  IN (?)';
                    $params[] = explode(',',$actionForm->getMbrBrandId());
                }
                if (!isBlankOrNull($actionForm->getMbrDivisionId())) {
                    $sql .= ' AND mbr_division_id IN (?)';
                    $params[] = explode(',',$actionForm->getMbrDivisionId());
                }

                $sql .= ' ORDER BY mbr_brand_id ';

                // SQL実行
                $res = array();
                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                $res = $this->_db->query($sql);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $arrayClass['mbr'][] = $v;
                }
            }

            //-----------------------------------------------
            // 条件
            //-----------------------------------------------
            if (!isBlankOrNull($actionForm->getMcdConditionId()) ||
                !isBlankOrNull($actionForm->getMcdContentId())) {

                // SQL作成
                $sql = '';
                $sql .= 'SELECT';
                $sql .= ' *';
                $sql .= ' FROM mst_condition';
                $sql .= ' WHERE 1 = 1 ';

                // パラメータセット
                $params = array();

                if (!isBlankOrNull($actionForm->getMcdConditionId())) {
                    $sql .= ' AND mcd_condition_id  IN (?)';
                    $params[] = explode(',',$actionForm->getMcdConditionId());
                }
                if (!isBlankOrNull($actionForm->getMcdContentId())) {
                    $sql .= ' AND mcd_content_id IN (?)';
                    $params[] = explode(',',$actionForm->getMcdContentId());
                }

                $sql .= ' ORDER BY mcd_condition_id ';

                // SQL実行
                $res = array();
                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                $res = $this->_db->query($sql);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $arrayClass['mcd'][] = $v;
                }
            }

            //-----------------------------------------------
            // ユーザー
            //-----------------------------------------------
            if (!isBlankOrNull($actionForm->getMusUserId())) {

                // SQL作成
                $sql = '';
                $sql .= 'SELECT';
                $sql .= ' *';
                $sql .= ' FROM mst_user';
                $sql .= ' WHERE 1 = 1 ';

                // パラメータセット
                $params = array();

                if (!isBlankOrNull($actionForm->getMusUserId())) {
                    $sql .= ' AND mus_user_id  IN (?)';
                    $params[] = explode(',',$actionForm->getMusUserId());
                }

                $sql .= ' ORDER BY mus_user_id ';

                // SQL実行
                $res = array();
                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                $res = $this->_db->query($sql);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $arrayClass['mus'][] = $v;
                }
            }

            //-----------------------------------------------
            // ユーザーコンテンツ
            //-----------------------------------------------
            if (!isBlankOrNull($actionForm->getMucUserContentId()) ||
                !isBlankOrNull($actionForm->getMucUserId()) ||
                !isBlankOrNull($actionForm->getMucContentId())) {

                // SQL作成
                $sql = '';
                $sql .= 'SELECT';
                $sql .= ' *';
                $sql .= ' FROM mst_user_content';
                $sql .= ' WHERE 1 = 1 ';

                // パラメータセット
                $params = array();

                if (!isBlankOrNull($actionForm->getMucUserContentId())) {
                    $sql .= ' AND muc_user_content_id  IN (?)';
                    $params[] = explode(',',$actionForm->getMucUserContentId());
                }
                if (!isBlankOrNull($actionForm->getMucUserId())) {
                    $sql .= ' AND muc_user_id IN (?)';
                    $params[] = explode(',',$actionForm->getMucUserId());
                }
                if (!isBlankOrNull($actionForm->getMucContentId())) {
                    $sql .= ' AND muc_content_id IN (?)';
                    $params[] = explode(',',$actionForm->getMucContentId());
                }

                $sql .= ' ORDER BY muc_user_content_id ';

                // SQL実行
                $res = array();
                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                $res = $this->_db->query($sql);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $arrayClass['muc'][] = $v;
                }
            }
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $arrayClass;
    }
}
