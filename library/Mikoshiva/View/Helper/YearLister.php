<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Zend/View/Interface.php';
require_once 'Zend/Validate/Date.php';

/**
 * 年のリストを作成します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/16
 * @version SVN:$Id: YearLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_YearLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;


    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param string $name name 属性
     * @param string $selected selected
     * @param string $start 開始年
     * @param string $end 終了年
     * @param int $figure 表示桁数
     * @return string
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/22
     * @version SVN:$Id: YearLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function yearLister($name, $selected = null, $start = 2000, $end = 2030, $figure = 4, $attribs = null)
    {
        if ($figure > 4 || $figure < 1) {
            throw new Mikoshiva_View_Helper_Exception('【Mikoshiva_View_Helper_YearLister#yearLister】'
                                                    . '第四引数は1-4までの数字しか許可されていません。');
        }

        $yearList = array();
        $yearList[''] = '';
        $_figure = 4 - (int)$figure;
        $years = range($start, $end);
        foreach ($years as $v) {

            $_v = $v;
            $_v = preg_replace('#^\d{' . $_figure . '}#', '', $_v, 1);
            $yearList[$_v] = $_v;
        }

        // ◆selected 2009/12/24 00:00:00 で来た場合にも対応
        $_selected  = $selected;
        if (isset($_selected) ) {
            $_selected  = str_replace('/', '-', $_selected );
            $validator1 = new Zend_Validate_Date('Y-m-d H:i:s');
            $validator2 = new Zend_Validate_Date('Y-m-d');
            if ($validator1->isValid($_selected) || $validator2->isValid($_selected)) {

                $matche = array();
                preg_match('#^(\d{2,4})-#', $_selected, $matche);
                $_selected = $matche[1];
            }
        }


        return $this->view->formSelect($name,
                                       $_selected,
                                       $attribs,
                                       $yearList);
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
