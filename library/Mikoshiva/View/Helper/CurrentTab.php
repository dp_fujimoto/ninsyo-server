<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 画面遷移を現在開いているタブに対して行うjavascriptを返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: CurrentTab.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_CurrentTab extends Mikoshiva_View_Helper_Abstract {


    /**
     * 画面遷移を現在開いているタブに対して行うjavascriptを返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: CurrentTab.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $url 遷移先 URL
     * @param string $tabId タブ ID
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function currentTab($url, $tabId = null, $formId = null) {
        logInfo(LOG_START);
        if ($tabId === null && $formId === null) {
            logInfo(LOG_END);
            return 'KO_currentTab(\'' . $url . '\');';
        } else if ($tabId === null && $formId !== null) {
            logInfo(LOG_END);
            return 'KO_currentTab(\'' . $url . '\', null, \'' . $formId . '\');';
        } else if ($tabId !== null && $formId === null) {
            logInfo(LOG_END);
            return 'KO_currentTab(\'' . $url . '\', \'' . $tabId . '\', null);';
        } else {
            logInfo(LOG_END);
            return 'KO_currentTab(\'' . $url . '\', \'' . $tabId . '\', \'' . $formId . '\');';
        }
    }
}
