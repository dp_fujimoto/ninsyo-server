<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Mapping.php';


/**
 * ステータス・タイプを元に、それが含む意味のリスト(select/radioフォーム)を作成します。
 *
 * @param array $params
 *                      string  name                  フォームのname, id
 *                      string  selected   空文字   | 選択された要素
 *                      string  status                ステータス・タイプ
 *                      string  mode       select   | select/radio
 *                      array   deleteList array()  | 削除リスト
 *                      array   attribs    array()  | selectタグに指定する追加の属性 '属性名' => 値
 *                      boolean addEmpty   false    | リストに空白を追加
 *                      boolean addAllItem false    | リストに｢全て｣を追加
 *                      string  listsep    '<br>'   | 各エンティティを区切る文字列
 * @param Smarty   $smarty
 * @param Template $template
 *
 * @return string                    リスト(select/radio)
 *
 * @package Smarty_libs
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.statusLister.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_statusLister($params, &$smarty, &$template) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['name']         = '';
    $default['selected']     = '';
    $default['status']       = '';
    $default['mode']         = 'select';
    $default['deleteList']   = array();
    $default['attribs']      = array();
    $default['addEmpty']     = false;
    $default['addAllItem']   = false;
    $default['listsep']      = '<br>';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(isBlankOrNull($data['status']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 status に値がありません。');
    }
    if($data['mode'] !== 'select' && $data['mode'] !== 'radio') {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】mode の指定が誤っています。指定値=>' . $data['mode']);
    }
    if(!is_array($data['deleteList'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 deleteList は配列でなければなりません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }
    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    // ステータスマッピングから値を取得
    $options = array();
    $options = Mikoshiva_Utility_Mapping::statusMappingList($data['status'], $data['deleteList']);
    $data['options'] = $options;

        //==========================================
        // mode によって表示を切り替える
        // 不明な mode であれば空文字が返ります
        //==========================================

    if ( $data['mode'] === 'select' ) {
        // select -------------------------------
        require_once SMARTY_PLUGINS_DIR . 'function.formSelect.php';
        Mikoshiva_Logger::info(LOG_END);
        return smarty_function_formSelect($data, $smarty, $template);
    }

    if ( $data['mode'] === 'radio' ) {
        // radio -------------------------------
        require_once SMARTY_PLUGINS_DIR . 'function.formRadio.php';
        Mikoshiva_Logger::info(LOG_END);
        return smarty_function_formRadio($data, $smarty, $template);
    }



}


?>
