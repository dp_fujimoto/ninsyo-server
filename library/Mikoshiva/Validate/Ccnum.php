<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Ccnum
 */
require_once 'Zend/Validate/Ccnum.php';


/**
 *
 *
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Ccnum.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Ccnum extends Zend_Validate_Ccnum
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        // self::LENGTH   => "%COLUMN_NAME%は13桁から19桁の数字でなければなりません",
        // self::CHECKSUM => "%COLUMN_NAME%はルーンアルゴリズムでのチェックに失敗しました",
        self::LENGTH   => "【%COLUMN_NAME%】は正しいカード番号ではありません", // 2012-07-09 谷口 変更
        self::CHECKSUM => "【%COLUMN_NAME%】は正しいカード番号ではありません", // 2012-07-09 谷口 変更
    );
}
