<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>認証APIチェックフォーム</title>
</head>

<body>
<form name="form" method="post" action="../api/authentication/check">
    <input type="hidden" name="flag" value="1">
    <table>
        <tr>
            <td>appID:</td>
            <td><input type="text" name="appId" value=""></td>
        </tr>
        <tr>
            <td>appキー:</td>
            <td><input type="text" name="appKey" value=""></td>
        </tr>
        <tr>
            <td>app名:</td>
            <td><input type="text" name="appName" value=""></td>
        </tr>
        <tr>
            <td>認証バージョン:</td>
            <td><input type="text" name="protocolVersion" value=""></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="送信"></td>
        </tr>
    </table>
</form>
</body>
</html>