<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * windows用CSVをmac用に変換する
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Tool_Models_SearchSpecialData_ConvertMacCsv
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * windows用CSVをmac用に変換する
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $output = '';
        $contentList = array();
        $list = array();

        //-----------------------------------------------------
        // ▼UploadFile 操作クラスのインスタンスを生成
        //-----------------------------------------------------
        $file = null;
        $file = new Zend_File_Transfer_Adapter_Http();

        //-----------------------------------------------------
        // ▼ファイルの移動場所をセット（確認画面では一時保存ディレクトリを指定する）
        //-----------------------------------------------------
        $file->setDestination(UPLOADS_TEMP_DIR);
        $fileInfo = $file->getFileInfo(); // アップロードファイルを取得

        // ファイルがある場合
        if (isset($fileInfo['addressFile']['tmp_name']) && !isBlankOrNull($fileInfo['addressFile']['tmp_name'])) {
            $fileData = file_get_contents($fileInfo['addressFile']['tmp_name']);
            $fileName = mb_convert_encoding($fileInfo['addressFile']['name'],'SJIS','UTF-8');

            //-----------------------------------------------
            // csvダウンロード
            //-----------------------------------------------
            // レスポンス
            $response = null;
            $response = $context->getResponse();

            // レスポンスをセット
            $response->setHeader('Content-disposition', 'attachment; filename=' . $fileName);
            $response->setHeader('Content-type', 'application/octet-stream; name=' . $fileName);
            $response->appendBody(mb_convert_encoding($fileData,'SJIS-mac','SJIS-win'));

            $actionForm->setFlag('1');
        }


        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }

}
