<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CreateUserHash.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_GetEdgeTradeItem extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 検索結果
        $output = array();

        $class = new Api_Models_Classes_CreateUserHash();
        $idHash = $class->execute($actionForm->getId());
        $passwordHash = $class->execute($actionForm->getPassword());

        //-----------------------------------------------
        // ユーザー確認
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT mpr_product_code";
        $sql .= " FROM mst_user";
        $sql .= " INNER JOIN mst_user_content ON mus_user_id  = muc_user_id";
        $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mus_delete_flag = '0'";
        $sql .= "   AND muc_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND muc_auth_flag = '1'";
        $sql .= "   AND mus_id_hash = ?";
        $sql .= "   AND mus_password_hash = ?";
        $sql .= "   AND mct_publication_date <= ?";
        $sql .= "   AND mct_application_type = ?";
        $sql .= "   AND mpr_product_code = ?";

        $params   = array();
        $params[] = $idHash;
        $params[] = $passwordHash;
        $params[] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');
        $params[] = 'APPLICATION_TYPE_EDGE_TRADER';
        $params[] = $actionForm->getProductCode();

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $output[$v['mpr_product_code']] = $v['mpr_product_code'];
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }
}
