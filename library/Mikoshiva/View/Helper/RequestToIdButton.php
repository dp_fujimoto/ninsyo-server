<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/RequestToId.php';


/**
 * 
 * 
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: RequestToIdButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_RequestToIdButton extends Mikoshiva_View_Helper_Abstract {


    /**
     * Ajax で指定されたアクションへリクエストを行い、
     * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うボタンタグを返します
     *
     * @param string name ボタン名
     * @param string $url アクション名
     * @param string $targetId アクション URL
     * @param string $formId フォーム ID
     * @param string $append 追記モード FRONT:前方へ追記|BACK:後方へ追記

     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: RequestToIdButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function requestToIdButton($name, $url, $targetId, $formId = '', $append = '') {


        $a = new Mikoshiva_View_Helper_RequestToId();
        return '<input type="button" value="' . $name . '" onclick="' . $a->requestToId($url, $targetId, $formId, $append) . '">';


        return $_str;
    }
}
