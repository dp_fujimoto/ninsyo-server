<?php
require_once 'Mikoshiva/View/Helper/Exception.php';

require_once 'Mikoshiva/Logger.php';

 /**
 * 画面遷移を現在開いているタブに対して行うjavascriptを返します。
 *
 * @param array   $params
 *                      string url    遷移先 URL
 *                      string tabId  null|タブ ID
 *                      string formId null|フォームの ID 名
 * @param Smarty  &$smarty
 * @return
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/28
 * @version SVN:$Id: function.currentTab.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_currentTab($params, &$smarty) {


    // ログ開始
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default               = array();
    $default['url']        = '';
    $default['tabId']      = null;
    $default['formId']     = null;
    $default['onloadList'] = '[]';


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['url']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 url に値がありません。');
    }
    if(isBlankOrNull($data['tabId']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 tabId に値がありません。');
    }

    $url        = $data['url'];
    $tabId      = $data['tabId'];
    $formId     = ($data['formId'] === null) ? 'null' : $data['formId'];
    $onloadList = $data['onloadList'];


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    return "KO_currentTab('{$url}', '{$tabId}', '{$formId}', {$onloadList});";
/*
    if ($data['tabId'] === null && $data['formId'] === null) {
        Mikoshiva_Logger::info(LOG_END);
        return 'KO_currentTab(\'' . $data['url'] . '\');';
    } else if ($data['tabId'] === null && $data['formId'] !== null) {
        Mikoshiva_Logger::info(LOG_END);
        return 'KO_currentTab(\'' . $data['url'] . '\', null, \'' . $data['formId'] . '\');';
    } else if ($data['tabId'] !== null && $data['formId'] === null) {
        Mikoshiva_Logger::info(LOG_END);
        return 'KO_currentTab(\'' . $data['url'] . '\', \'' . $data['tabId'] . '\', null);';
    } else {
        Mikoshiva_Logger::info(LOG_END);
        return 'KO_currentTab(\'' . $data['url'] . '\', \'' . $data['tabId'] . '\', \'' . $data['formId'] . '\');';
    }
*/
    //どの条件でも場所まで通過してこない

}



