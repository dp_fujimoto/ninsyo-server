<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Db/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';
require_once 'Mikoshiva/View/Helper/FormRadio.php';
require_once 'Ar/MstDivision.php';
require_once 'Zend/View/Interface.php';

/**
 * 部署データを返します
 *
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/11
 * @version SVN:$Id: DivisionLister.php,v 1.1 2012/09/21 07:08:06 fujimoto Exp $
 */
class Mikoshiva_View_Helper_DivisionLister extends Mikoshiva_View_Helper_Abstract {


    public $view;


    /**
     *
     * @return string
     */
    public function divisionLister($name, $selected = null, $mode = 'select', $attribs = null, $divisionZero = true, $addEmpty = false) {
        logInfo(LOG_START);

        //==========================================
        // 部署テーブルからリストを取得
        //==========================================
        $cache = Zend_Registry::get('cache');
        $divisionList = array();
        $row            = array();
        if ( ! $row = $cache->load('Mikoshiva_View_Helper_DivisionLister') ) {
            $division       = new Ar_MstDivision();
            $select         = $division->select()->where('mdi_delete_flag = ?', '0');
            $divisionList   = $division->fetchAll( $select )->toArray();

            // キャッシュに書き込み
            $cache->save( $divisionList, 'Mikoshiva_View_Helper_DivisionLister');
        } else {

            // キャッシュから読み込み
            $divisionList = $row;
        }

        // 1 件も取得出来なければ空文字を返す
        if( count( $divisionList ) === 0 ) {
            logInfo(LOG_END);
            return '';
        }

        //------------------------------------
        // form ヘルパーに渡せる様に配列を整形
        //------------------------------------
        $d = array();
        foreach($divisionList as $k => $v) {
            $d[ $v['mdi_division_id'] ] = $v['mdi_division_name'];
        }
        if( $divisionZero === false) {
            unset($d[1]);
        }

        //==========================================
        // mode によって表示を切り替える
        // 不明な mode であれば空文字が返ります
        //==========================================
        $tag = '';

        $result = '';
        if ( $mode === 'select' ) {
            // select -------------------------------
            //$formSelect = new Mikoshiva_View_Helper_FormSelect();
            logInfo(LOG_END);
            return $this->view->formSelect($name,
                                           $selected,
                                           $attribs,
                                           $d,
                                           $addEmpty);
        }

        if ( $mode === 'radio' ) {
            // radio -------------------------------
            //$formRadio = new Mikoshiva_View_Helper_FormRadio();
            logInfo(LOG_END);
            return $this->view->formRadio($name,
                                         $selected,
                                         $attribs,
                                         $d);
        }
        logInfo(LOG_END);
        return '';
    }


    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}















