<?php
/**
 * @package Mikoshiva_Validate
 */


/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Exception extends Zend_Exception
{}
