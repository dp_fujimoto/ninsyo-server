<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Content_CreateJson extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 出力内容
        $output = '';

        if (file_exists(CONDITION_JSON_FILES . '/' . $actionForm->getServerName() . '-' . $actionForm->getProductCode())) {
            $output = file_get_contents(CONDITION_JSON_FILES . '/' . $actionForm->getServerName() . '-' . $actionForm->getProductCode());
        } else {
            $output = json_encode($context->getResult('conditionList'));
            file_put_contents(CONDITION_JSON_FILES . '/' . $actionForm->getServerName() . '-' . $actionForm->getProductCode(), $output);
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }
}
