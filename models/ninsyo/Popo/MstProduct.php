<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProduct extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mpr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productId' => null,
        'brandId' => null,
        'productName' => null,
        'productCode' => null,
        'imageUrl' => null,
        'imageUpdateDatetime' => null,
        'description' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 商品ID の値を返します
	 *
     * @return int 商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * 商品ID の値をセットします
	 *
	 * @param int $productId 商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * mpr_brand_id の値を返します
	 *
     * @return int mpr_brand_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBrandId() {
		return $this->_properties['brandId'];
    }


    /**
     * mpr_brand_id の値をセットします
	 *
	 * @param int $brandId mpr_brand_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBrandId($brandId) {
		$this->_properties['brandId'] = $brandId;
    }


    /**
     * 商品名 の値を返します
	 *
     * @return varchar 商品名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductName() {
		return $this->_properties['productName'];
    }


    /**
     * 商品名 の値をセットします
	 *
	 * @param varchar $productName 商品名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductName($productName) {
		$this->_properties['productName'] = $productName;
    }


    /**
     * 商品コード の値を返します
	 *
     * @return varchar 商品コード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductCode() {
		return $this->_properties['productCode'];
    }


    /**
     * 商品コード の値をセットします
	 *
	 * @param varchar $productCode 商品コード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductCode($productCode) {
		$this->_properties['productCode'] = $productCode;
    }


    /**
     * mpr_image_url の値を返します
	 *
     * @return varchar mpr_image_url
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImageUrl() {
		return $this->_properties['imageUrl'];
    }


    /**
     * mpr_image_url の値をセットします
	 *
	 * @param varchar $imageUrl mpr_image_url
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImageUrl($imageUrl) {
		$this->_properties['imageUrl'] = $imageUrl;
    }


    /**
     * mpr_image_update_datetime の値を返します
	 *
     * @return datetime mpr_image_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImageUpdateDatetime() {
		return $this->_properties['imageUpdateDatetime'];
    }


    /**
     * mpr_image_update_datetime の値をセットします
	 *
	 * @param datetime $imageUpdateDatetime mpr_image_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImageUpdateDatetime($imageUpdateDatetime) {
		$this->_properties['imageUpdateDatetime'] = $imageUpdateDatetime;
    }


    /**
     * mpr_description の値を返します
	 *
     * @return text mpr_description
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDescription() {
		return $this->_properties['description'];
    }


    /**
     * mpr_description の値をセットします
	 *
	 * @param text $description mpr_description
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDescription($description) {
		$this->_properties['description'] = $description;
    }


    /**
     * mpr_memo の値を返します
	 *
     * @return text mpr_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * mpr_memo の値をセットします
	 *
	 * @param text $memo mpr_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























