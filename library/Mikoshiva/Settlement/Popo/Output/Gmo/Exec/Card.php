<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Output_Exec_Card
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Card extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'ACS' => null,
        'OrderID' => null,
        'Forward' => null,
        'Method' => null,
        'PayTimes' => null,
        'Approve' => null,
        'TranID' => null,
        'TranDate' => null,
        'CheckString' => null,
        'ClientField1' => null,
        'ClientField2' => null,
        'ClientField3' => null,
        'ClientFieldFlag' => null,
        'ErrCode' => null,
        'ErrInfo' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * ACS
	 *
     * @return  ACS
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getACS() {
		return $this->_properties['ACS'];
    }


    /**
     * ACS
	 *
	 * @param  $ACS ACS
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setACS($ACS) {
		$this->_properties['ACS'] = $ACS;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * Forward
	 *
     * @return  Forward
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getForward() {
		return $this->_properties['Forward'];
    }


    /**
     * Forward
	 *
	 * @param  $Forward Forward
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setForward($Forward) {
		$this->_properties['Forward'] = $Forward;
    }


    /**
     * Method
	 *
     * @return  Method
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMethod() {
		return $this->_properties['Method'];
    }


    /**
     * Method
	 *
	 * @param  $Method Method
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMethod($Method) {
		$this->_properties['Method'] = $Method;
    }


    /**
     * PayTimes
	 *
     * @return  PayTimes
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getPayTimes() {
		return $this->_properties['PayTimes'];
    }


    /**
     * PayTimes
	 *
	 * @param  $PayTimes PayTimes
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setPayTimes($PayTimes) {
		$this->_properties['PayTimes'] = $PayTimes;
    }


    /**
     * Approve
	 *
     * @return  Approve
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getApprove() {
		return $this->_properties['Approve'];
    }


    /**
     * Approve
	 *
	 * @param  $Approve Approve
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setApprove($Approve) {
		$this->_properties['Approve'] = $Approve;
    }


    /**
     * TranID
	 *
     * @return  TranID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getTranID() {
		return $this->_properties['TranID'];
    }


    /**
     * TranID
	 *
	 * @param  $TranID TranID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setTranID($TranID) {
		$this->_properties['TranID'] = $TranID;
    }


    /**
     * TranDate
	 *
     * @return  TranDate
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getTranDate() {
		return $this->_properties['TranDate'];
    }


    /**
     * TranDate
	 *
	 * @param  $TranDate TranDate
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setTranDate($TranDate) {
		$this->_properties['TranDate'] = $TranDate;
    }


    /**
     * CheckString
	 *
     * @return  CheckString
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getCheckString() {
		return $this->_properties['CheckString'];
    }


    /**
     * CheckString
	 *
	 * @param  $CheckString CheckString
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setCheckString($CheckString) {
		$this->_properties['CheckString'] = $CheckString;
    }


    /**
     * ClientField1
	 *
     * @return  ClientField1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getClientField1() {
		return $this->_properties['ClientField1'];
    }


    /**
     * ClientField1
	 *
	 * @param  $ClientField1 ClientField1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setClientField1($ClientField1) {
		$this->_properties['ClientField1'] = $ClientField1;
    }


    /**
     * ClientField2
	 *
     * @return  ClientField2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getClientField2() {
		return $this->_properties['ClientField2'];
    }


    /**
     * ClientField2
	 *
	 * @param  $ClientField2 ClientField2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setClientField2($ClientField2) {
		$this->_properties['ClientField2'] = $ClientField2;
    }


    /**
     * ClientField3
	 *
     * @return  ClientField3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getClientField3() {
		return $this->_properties['ClientField3'];
    }


    /**
     * ClientField3
	 *
	 * @param  $ClientField3 ClientField3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setClientField3($ClientField3) {
		$this->_properties['ClientField3'] = $ClientField3;
    }


    /**
     * ClientFieldFlag
	 *
     * @return  ClientFieldFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getClientFieldFlag() {
		return $this->_properties['ClientFieldFlag'];
    }


    /**
     * ClientFieldFlag
	 *
	 * @param  $ClientFieldFlag ClientFieldFlag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setClientFieldFlag($ClientFieldFlag) {
		$this->_properties['ClientFieldFlag'] = $ClientFieldFlag;
    }


    /**
     * ErrCode
	 *
     * @return  ErrCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getErrCode() {
		return $this->_properties['ErrCode'];
    }


    /**
     * ErrCode
	 *
	 * @param  $ErrCode ErrCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setErrCode($ErrCode) {
		$this->_properties['ErrCode'] = $ErrCode;
    }


    /**
     * ErrInfo
	 *
     * @return  ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getErrInfo() {
		return $this->_properties['ErrInfo'];
    }


    /**
     * ErrInfo
	 *
	 * @param  $ErrInfo ErrInfo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setErrInfo($ErrInfo) {
		$this->_properties['ErrInfo'] = $ErrInfo;
    }


}

























