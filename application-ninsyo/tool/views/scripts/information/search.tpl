<script type="text/javascript">document.getElementById('tab_text_{$tabId}').title = 'お知らせ';</script>
<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">【お知らせ】</h2>

            <table class="COM_tableBorderZero">
                <tr>
                    <td class="COM_nowrap COM_top COM_left">
                        {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                            <!-- // 新規作成ボタン -->
                            {newTabButton name="新規作成" url=$smarty.const.SUB_FOLDER_PATH|cat:"/tool/information/input/"}
                        {/if}
                    </td>
                    {*
                    <td class="COM_nowrap COM_top COM_right">
                        <img src="{$smarty.const.SUB_FOLDER_PATH}/img/testing.jpg">
                        <b>トラブル履歴・改善要望など、参考情報として気軽に書いてください</b>
                        　※個人情報は記入しないでください。<br>
                        <form action="https://docs.google.com/a/d-publishing.jp/spreadsheet/formResponse?formkey=dHNlOV9sQ085dTJINlNGVGJMNVl4UVE6MQ&embedded=true&ifq" method="POST" target="_blank">
                            <textarea name="entry.0.single" rows="1" cols="70"></textarea>
                            <input type="hidden" name="entry.1.single" value="{$smarty.session.staff.mstStaff->getStaffId()}">
                            <input type="hidden" name="entry.2.single" value="{$smarty.const._SERVER_MODE_ID_}">
                            <input type="hidden" name="pageNumber" value="0">
                            <input type="hidden" name="backupCache" value="">
                            <input type="submit" name="submit" value="送信">
                        </form>
                    </td>
                    *}
                </tr>
            </table>

            <hr>

            {* 検索結果あり *}
            {if count($search) > 0}

                <!-- // ページャーを操作するボタン定義 -->
                {$form->getPagerBar()|hd}
                <hr>

                <!-- // 検索結果リスト開始 -->
                {$new = date("Y-m-d h:i:s",strtotime("-1 week"))}

                {foreach from=$search key="k" item="v"}
                <fieldset>
                    <legend class="COM_nowrap COM_bgColorDeep COM_bold" style="font-size:18px">
                        {if $new < $v->getRegistrationDatetime()}<img src="{$smarty.const.SUB_FOLDER_PATH}/img/new.gif">{/if}
                        {$v->getSubject()}
                    </legend>
                    <div class="COM_nowrap COM_right">
                        【投稿者／投稿日時】　
                        {dbWrite arClassName='Mikoshiva_Db_Ar_MstStaff' pkey=$v->getStaffId() column=array('lastName','firstName')}
                        　{$v->getRegistrationDatetime()}
                        {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                            　{newTabButton name="編集" url=$smarty.const.SUB_FOLDER_PATH|cat:"/tool/information/input/informationHistoryId/"|cat:$v->getInformationHistoryId() protected=true}
                        {/if}
                    </div>
                    <br>
                    {if strstr($v->getBody(),'___FILEDATA___')}
                        {$temp = explode('___FILEDATA___',$v->getBody())}
                        <div>{$temp[0]|nl2br}</div><br>
                        【添付ファイル】<br>
                        {foreach from=$temp key="k2" item="v2"}
                            {if $k2 > 0}
                                {$temp2 = explode('|||',$v2)}
                                {if count($temp2) === 2}
                                    <a href='{$smarty.const.SUB_FOLDER_PATH}/tool/information/download-file/hash/{$temp2[1]}/name/{$temp2[0]|urlencode}'>{$temp2[0]}</a>
                                    {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                                        {currentTabLink name='削除' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/information/delete-file/informationHistoryId/'|cat:$v->getInformationHistoryId()|cat:'/hash/'|cat:$temp2[1] tabId=$tabId}
                                    {/if}
                                    <br>
                                {/if}
                            {/if}
                        {/foreach}
                    {else}
                        <div>{$v->getBody()|nl2br}</div>
                    {/if}
                    {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                        <br>
                        ＜添付ファイルをつける場合は、ファイルを選択＞
                        <form name="inputForm{$v->getInformationHistoryId()}" id="inputForm{$v->getInformationHistoryId()}" enctype="multipart/form-data" method="post" onSubmit="return false;">
                            <input type="file" name="file" size="30" onChange="TOL_setFile{$tabId}('{$v->getInformationHistoryId()}');">
                        </form>
                    {/if}
                </fieldset>
                <br>
                <br>
                {/foreach}

                <!-- // 検索結果リスト終了 -->

            {* 検索結果なし *}
            {else}

                <p>お知らせはありません。</p>

            {/if}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->

<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // ファイルアップロード
    //-----------------------------------------------
    function TOL_setFile{$tabId}(id){
        KO_currentTab('{$smarty.const.SUB_FOLDER_PATH}/tool/information/upload-file/informationHistoryId/'+id, '{$tabId}', 'inputForm'+id);
    }
</script>
<!-- // カレントJavascript終了 -->