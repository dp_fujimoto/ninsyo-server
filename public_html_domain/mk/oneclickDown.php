<?php
	require_once './init.php';

	if (!isset($_SESSION['transactionNumber']) || !isset($_POST['transactionNumber']) || !isset($_SESSION['email'])) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:googleCheck:true]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$ret = downsell($agent,$basePath,$errorLogTime);
		error_log($errorLogTime . '['  . session_id() . ']' . print_r($ret,true) . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		echo $ret;
		exit;
	}

	error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[case:ONECLICK_DOWN]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
	if ($_POST['transactionNumber'] == $_SESSION['transactionNumber']) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[if:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$_POST['transactionNumber'] = $_POST['transactionNumber'] += 1;
		$_SESSION['campaignType'] = $_POST['campaignType'];
        // プレビューキーをセッションに保存
        $_SESSION['previewKey'] = $_POST['previewKey'];
	} else {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[else:セッションのトランザクションNoと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$_POST['transactionNumber'] = $_SESSION['transactionNumber'];
		$_POST['campaignType'] = $_SESSION['campaignType'];
		$_POST['previewKey'] = $_SESSION['previewKey'];
	}

    // mikoshivaにpost
    $response = $agent->getResponseByPost($basePath.'downsell', $_POST);

    echo $response;
    $_SESSION['transactionNumber'] = $_POST['transactionNumber'];

    error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
