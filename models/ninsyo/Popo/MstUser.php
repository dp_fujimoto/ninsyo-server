<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_user
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstUser extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mus';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'userId' => null,
        'serverName' => null,
        'idHash' => null,
        'passwordHash' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mus_user_id の値を返します
	 *
     * @return int mus_user_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserId() {
		return $this->_properties['userId'];
    }


    /**
     * mus_user_id の値をセットします
	 *
	 * @param int $userId mus_user_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserId($userId) {
		$this->_properties['userId'] = $userId;
    }


    /**
     * mus_server_name の値を返します
	 *
     * @return varchar mus_server_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServerName() {
		return $this->_properties['serverName'];
    }


    /**
     * mus_server_name の値をセットします
	 *
	 * @param varchar $serverName mus_server_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServerName($serverName) {
		$this->_properties['serverName'] = $serverName;
    }


    /**
     * mus_id_hash の値を返します
	 *
     * @return varchar mus_id_hash
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIdHash() {
		return $this->_properties['idHash'];
    }


    /**
     * mus_id_hash の値をセットします
	 *
	 * @param varchar $idHash mus_id_hash
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIdHash($idHash) {
		$this->_properties['idHash'] = $idHash;
    }


    /**
     * mus_password_hash の値を返します
	 *
     * @return varchar mus_password_hash
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPasswordHash() {
		return $this->_properties['passwordHash'];
    }


    /**
     * mus_password_hash の値をセットします
	 *
	 * @param varchar $passwordHash mus_password_hash
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPasswordHash($passwordHash) {
		$this->_properties['passwordHash'] = $passwordHash;
    }


    /**
     * mus_delete_flag の値を返します
	 *
     * @return char mus_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mus_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mus_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mus_deletion_datetime の値を返します
	 *
     * @return datetime mus_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mus_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mus_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mus_registration_datetime の値を返します
	 *
     * @return datetime mus_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mus_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mus_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mus_update_datetime の値を返します
	 *
     * @return datetime mus_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mus_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mus_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mus_update_timestamp の値を返します
	 *
     * @return timestamp mus_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mus_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mus_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























