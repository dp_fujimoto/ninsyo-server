<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">バッチテスト実行</h2>

            <div id="divBefore">

                <!-- // エラーメッセージ -->
                {if $message|count > 0}
                    <p class="COM_errorMsg">
                        {imploader pieces=$message glue='<br>'}
                    </p>
                {/if}

                ■結果<br>
                <textarea cols="60" rows="3" class="COM_fontRed">{$complete}</textarea>
                <br>

                {$formNo = 0}

                ボタンを１つ押したあとは完了のメッセージが出るまで他のボタンは押さないでください。

                <!-- // フォーム開始 -->
                <table border="2">
                    <tr>
                        <td>
                            {currentTabButton name='ユーザーコンテンツ登録タスク処理' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/batch-test/complete1/mode/1' tabId=$tabId}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {$formNo = $formNo + 1}
                            <form name="inputForm{$formNo}" id="inputForm{$formNo}" method="post" onSubmit="return false;">
                                <input type="button" value='エッジトレーダーアクセス解析' onClick="TOL_execute{$tabId}({$formNo},2);">2
                                <br>
                                <input type="text" name="date" value="{date('Y-m-d')}" maxlength="10" size="10">年月日　
                            </form>
                        </td>
                    </tr>
                </table>
                <!-- // フォーム終了 -->

            </div>
            <div id="divAfter" class="COM_displayNone">
                <span class="COM_fontRed COM_bold">処理実行中。しばらくお待ち下さい</span>
            </div>

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<!-- // メインテーブル終了 -->
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->

<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 検索
    //-----------------------------------------------
    function TOL_execute{$tabId}(formNo,no){
        document.getElementById('divBefore').className = 'COM_displayNone';
        document.getElementById('divAfter').className = '';
        KO_currentTab('{$smarty.const.SUB_FOLDER_PATH}/tool/batch-test/complete' + no + '/mode/' + no, '{$tabId}', 'inputForm' + formNo, []);
    }

</script>