<?php


/**
 * 決済クラスのインスタンスを生成します
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/08
 * @version SVN:$Id: Factory.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Order_Factory {


    /**
     * 決済方法 (paymentType) を元に決済クラスのインスタンスを返します
     *
     * @param string                                $paymentType 支払い方法
     * @param Mikoshiva_Settlement_Popo_Input_Order $inputPopo
     * @return Mikoshiva_Settlement_Execution_Order_Abstract
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/09
     * @version SVN:$Id: Factory.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function getInstance(Mikoshiva_Settlement_Popo_Input_Order $inputPopo) {


        //-------------------------------------------------
        // ◆paymentType を元に該当 instance を生成
        //-------------------------------------------------
        $class = null;
        switch ($inputPopo->getChargeType()) {

            // ▼GMO ----------------------------------------------------------
            case 'CHARGE_TYPE_CARD':         // カードPG
            //case 'CHARGE_TYPE_CARD_CONTINUITY': // カード継続
                include_once 'Mikoshiva/Settlement/Execution/Order/Gmo.php';
                $class = new Mikoshiva_Settlement_Execution_Order_Gmo($inputPopo);
                break;

            // ▼代金引換 ----------------------------------------------------------
            case 'CHARGE_TYPE_CASH_ON_DELIVERY':
                include_once 'Mikoshiva/Settlement/Execution/Order/Delivery.php';
                $class = new Mikoshiva_Settlement_Execution_Order_Delivery($inputPopo);
                break;

            // ▼銀行振込 ----------------------------------------------------------
            case 'CHARGE_TYPE_BANK_TRANSFER':
                include_once 'Mikoshiva/Settlement/Execution/Order/Bank.php';
                $class = new Mikoshiva_Settlement_Execution_Order_Bank($inputPopo);
                break;

            // ▼PayPal ----------------------------------------------------------
            case 'CHARGE_TYPE_PAYPAL':
                include_once 'Mikoshiva/Settlement/Execution/Order/Paypal.php';
                $class = new Mikoshiva_Settlement_Execution_Order_Paypal($inputPopo);
                break;

            // ▼請求無し ----------------------------------------------------------
            case 'CHARGE_TYPE_NONE':
                include_once 'Mikoshiva/Settlement/Execution/Order/None.php';
                $class = new Mikoshiva_Settlement_Execution_Order_None($inputPopo);
                break;

            // ▼どれにも当てはまらなければ例外を投げる
            default:
                $message = "【" . __CLASS__ . "::" . __FUNCTION__ . " （" . __LINE__ . "）】決済方法に合致する実行クラスを生成出来ませんでした。" . $inputPopo->getChargeType();
                logDebug($message);
                include_once 'Mikoshiva/Settlement/Execution/Order/Exception.php';
                throw new Mikoshiva_Settlement_Execution_Order_Exception($message);
        }



        return $class;
    }


}



























