<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">
        
            <h2 id="page_title_{$tabId}">select SQL 実行</h2>
            
            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}
            {if count($search) > 0}
                <table class="COM_table">
                    {foreach from=$search key="k" item="v"}
                    <tr>
                        {foreach from=$v key="k2" item="v2"}
                        <td class="COM_nowrap">
                            {$v2}
                        </td>
                        {/foreach}
                    </tr>
                    {/foreach}
                </table>
                <hr>
                {$str1 = "\n"}
                {$str2 = "\t"}
                <textarea cols="120" rows="10">{foreach from=$search key="k" item="v"}{foreach from=$v key="k2" item="v2"}{$v2}{$str2}{/foreach}{$str1}{/foreach}</textarea>
            {elseif !isBlankOrNull($form->getSql())}
                検索結果なし
            {else}
                <form name="searchForm" id="searchForm" method="post" onSubmit="return false;">
                    selectSql:{newTabButton name="　　実行　　" url="/tool/search-special-data/execute-select-sql" formId="searchForm" protected=true}<br>
                    <textarea name="sql" cols="100" rows="10"></textarea>
                </form>
            {/if}
        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
