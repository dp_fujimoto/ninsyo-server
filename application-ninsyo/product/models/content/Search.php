<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstProduct.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Product_Models_Content_Search extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 出力
        $output = array();

        if ($actionForm->getMode() !== 'search') {
            // 部署が未入力であれば、初期値を代入
            if( isBlankOrNull($actionForm->getDivisionId()) ) {
                $actionForm->setDivisionId($context->getStaff()->changeDivisionId);
            }
        }

        //-----------------------------------------------
        // ブランドリスト取得
        //-----------------------------------------------
        $brandList = array();
        $productList = array();

        // SQL作成
        $sql = "";
        $sql .= "SELECT *";
        $sql .= " FROM mst_brand";
        $sql .= " INNER JOIN mst_product ON mbr_brand_id = mpr_brand_id";
        $sql .= " WHERE mbr_delete_flag = '0'";
        $sql .= " AND mpr_delete_flag = '0'";

        if (!isBlankOrNull($actionForm->getDivisionId())) {
            $sql .= " AND mbr_division_id = " . $actionForm->getDivisionId();
        }
        $sql .= " ORDER BY mbr_brand_name, mpr_product_name";

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $brandList[$v['mbr_brand_id']] = $v['mbr_brand_name'];
            $productList[$v['mpr_product_id']] = $v['mpr_product_name'];
        }

        $actionForm->setBrandList($brandList);
        $actionForm->setProductList($productList);

        //-----------------------------------------------
        //
        // コンテンツ検索
        //
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT";
        $sql .= " *";
        $sql .= " FROM mst_content";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1 ";
        $sql .= " AND mbr_delete_flag = '0'";
        $sql .= " AND mpr_delete_flag = '0'";

        // パラメータセット
        $params = array();

        // 部署ID
        if (!isBlankOrNull($actionForm->getDivisionId())) {
            $sql .= " AND mbr_division_id = ?";
            $params[] = $actionForm->getDivisionId();
        }

        // ブランドID
        if (!isBlankOrNull($actionForm->getBrandId())) {
            $sql .= " AND mbr_brand_id = ?";
            $params[] = $actionForm->getBrandId();
        }

        // 商品ID
        if (!isBlankOrNull($actionForm->getProductId())) {
            $sql .= " AND mpr_product_id = ?";
            $params[] = $actionForm->getProductId();
        }

        // 用途
        if (!isBlankOrNull( $actionForm->getApplicationType())) {
            $sql .= " AND mct_application_type = ?";
            $params[] = $actionForm->getApplicationType();
        }

        // コンテンツ名
        if (!isBlankOrNull( $actionForm->getName() )) {
            $sql .= " AND (mct_content_name like ? OR mct_confirm_name like ?)";
            $params[] = '%' . $actionForm->getName() . '%';
            $params[] = '%' . $actionForm->getName() . '%';
        }

        // 状態
        if (!isBlankOrNull($actionForm->getDeleteFlag())) {
            $sql .= " AND mct_delete_flag = ?";
            $params[] = $actionForm->getDeleteFlag();
        } else {
            $sql .= " AND mct_delete_flag = '0'";
        }

        $sql .= " ORDER BY mbr_brand_name, mpr_product_name, mct_content_name";

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $output[] = $v;
        }

        // 自然順ソート
        uksort($output, 'strnatcmp');

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;

    }
}
