<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ユーザーコンテンツ一覧</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // 新規作成ボタン -->
            {newTabButton name="新規登録" url=$smarty.const.SUB_FOLDER_PATH|cat:"/user/content/input"}

            <!-- // フォーム開始 -->
            <form name="searchForm" id="searchForm" method="post" onSubmit="return false;">

                <span class="COM_fontRed">メールアドレス・コンテンツIDのいずれかは入力必須です</span>

                <!-- // hidden -->
                <input type="hidden" name="mode" value="search">

                <!-- 検索条件テーブル 開始-->
                <table class="COM_tableBorderZero" >
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            メールアドレス
                        </td>
                        <td class="COM_nowrap" colspan="3">
                            <input type="text" name="email" value="{$form->getEmail()}" size="50" maxlength="200">
                        </td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            パスワード
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="password" value="{$form->getPassword()}" size="20" maxlength="20">
                        </td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            コンテンツID
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="contentId" value="{$form->getContentId()}" size="6" maxlength="6">
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            サーバー名
                        </td>
                        <td class="COM_nowrap">
                            <select name="serverName">
                                <option value=""></option>
                                <option value="mikoshiva" {if $form->getServerName() === 'mikoshiva'}selected{/if}>mikoshiva</option>
                                <option value="shimant" {if $form->getServerName() === 'shimant'}selected{/if}>shimant</option>
                                <option value="gusiken" {if $form->getServerName() === 'gusiken'}selected{/if}>gusiken</option>
                                <option value="test.mikoshiva" {if $form->getServerName() === 'test.mikoshiva'}selected{/if}>test.mikoshiva</option>
                                <option value="free" {if $form->getServerName() === 'free'}selected{/if}>free（無料ＷＥＢ認証アカウント登録用）</option>
                            </select>
                        </td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            用途
                        </td>
                        <td class="COM_nowrap">
                            {statusLister name="applicationType" selected=$form->getApplicationType() status="application_type" addEmpty=true}
                        </td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">閲覧権限</td>
                        <td class="COM_nowrap">
                            <select name="authFlag">
                                <option value=""></option>
                                <option value="0" {if $form->getAuthFlag() === '0'}selected{/if}>なし</option>
                                <option value="1" {if $form->getAuthFlag() === '1'}selected{/if}>あり</option>
                            </select>
                        </td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">削除</td>
                        <td class="COM_nowrap">
                            <select name="deleteFlag">
                                <option value=""></option>
                                <option value="0" {if $form->getDeleteFlag() === '0'}selected{/if}>未</option>
                                <option value="1" {if $form->getDeleteFlag() === '1'}selected{/if}>済み</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <!-- 検索条件テーブル 終了-->

                <br>

                {currentTabButton name="　　　検索　　　" url=$smarty.const.SUB_FOLDER_PATH|cat:"/user/content/search" tabId=$tabId formId="searchForm"}　　　　　　
                {currentTabButton name="リセット" url=$smarty.const.SUB_FOLDER_PATH|cat:"/user/content/search" tabId=$tabId}

            </form>
            <!-- // 検索フォーム終了 -->

            <hr>

            {* 検索結果あり *}
            {if count($search) > 0}

                {$form->getPagerBar()|hd}

                　　　　　　　　　　　　　　　　　　　　　　サービス開始日（強制）・サービス終了日（強制）は、クリックすると変更することができます。強制の日付がある場合、データ更新時に強制の日付に修正します。

                <br>

                <!-- // 検索結果リスト開始 -->
                <table class="COM_table PDT_tableWidthBasic">
                    <tr>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">ユーザー<br>ID</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">コンテンツ<br>ID</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">ブランド名／商品名</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">コンテンツ名</td>
                        {*<td class="COM_bgColorLight COM_bold COM_center COM_nowrap">管理名</td>*}
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">用途</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">サービス<br>開始日</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">サービス<br>終了日</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">ブランク期間</td>
                        {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap COM_fontGrey">サービス<br>開始日強制</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap COM_fontGrey">サービス<br>終了日強制</td>
                        {/if}
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">登録日<br>更新日</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">削除日</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">ボタン</td>
                    </tr>

                    {assign var="lineCount" value='0'}
                    {foreach from=$search key="k" item="v"}
                    {if $lineCount === '1'}
                        {assign var="lineColor" value="PDT_rowBackgoundColor"}
                        {assign var="lineCount" value='0'}
                    {else}
                        {assign var="lineColor" value=''}
                        {assign var="lineCount" value='1'}
                    {/if}

                    <tr>
                        <td class="COM_nowrap {$lineColor}">{$v['mus_user_id']}</td>
                        <td class="COM_nowrap {$lineColor} {if $v['muc_delete_flag'] === '1'}COM_fontRed{/if}">
                            {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                            <span onClick="{newTab url=$smarty.const.SUB_FOLDER_PATH|cat:'/product/management/search-table-data/mode/search/mucUserContentId/'|cat:$v['muc_user_content_id']}">
                                {$v['mct_content_id']}
                            </span>
                            {else}
                                {$v['mct_content_id']}
                            {/if}
                        </td>
                        <td class="COM_nowrap {$lineColor}">{$v['mbr_brand_name']}<br><span class="COM_fontGrey">{$v['mpr_product_name']}</span></td>
                        <td class="COM_nowrap {$lineColor}">{$v['mct_content_name']}</td>
                        {*<td class="COM_nowrap {$lineColor}">{$v['mct_confirm_name']}</td>*}
                        <td class="COM_nowrap {$lineColor}">{statusMapping code=$v['mct_application_type']}</td>
                        <td class="COM_nowrap {$lineColor}" onClick="document.getElementById('startDateDiv_{$v['muc_user_content_id']}').className='COM_displayNone';document.getElementById('startDateEditDiv_{$v['muc_user_content_id']}').className='';">
                            <div id="startDateDiv_{$v['muc_user_content_id']}">{$v['muc_service_start_date']}</div>
                            <div id="startDateEditDiv_{$v['muc_user_content_id']}" class="COM_displayNone">
                                <form id="startDateForm_{$v['muc_user_content_id']}">
                                    <input type="text" name="startDate" value="{$v['muc_service_start_date']}" maxlength="10" size="10">
                                    {requestToIdButton name='変更' url='/user/content/update/mode/startDate/userContentId/'|cat:$v['muc_user_content_id'] formId='startDateForm_'|cat:$v['muc_user_content_id'] targetId='startDateSpan_'|cat:$v['muc_user_content_id']}
                                    <span id="startDateSpan_{$v['muc_user_content_id']}"></span>
                                </form>
                            </div>
                        </td>
                        <td class="COM_nowrap {$lineColor}" onClick="document.getElementById('endDateDiv_{$v['muc_user_content_id']}').className='COM_displayNone';document.getElementById('endDateEditDiv_{$v['muc_user_content_id']}').className='';">
                            <div id="endDateDiv_{$v['muc_user_content_id']}">{$v['muc_service_end_date']}</div>
                            <div id="endDateEditDiv_{$v['muc_user_content_id']}" class="COM_displayNone">
                                <form id="endDateForm_{$v['muc_user_content_id']}">
                                    <input type="text" name="endDate" value="{$v['muc_service_end_date']}" maxlength="10" size="10">
                                    {requestToIdButton name='変更' url='/user/content/update/mode/endDate/userContentId/'|cat:$v['muc_user_content_id'] formId='endDateForm_'|cat:$v['muc_user_content_id'] targetId='endDateSpan_'|cat:$v['muc_user_content_id']}
                                    <span id="endDateSpan_{$v['muc_user_content_id']}"></span>
                                </form>
                            </div>
                        </td>
                        <td class="COM_nowrap {$lineColor}">{$v['blank']|hd}</td>
                        {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                        <td class="COM_nowrap {$lineColor}" onClick="document.getElementById('forceStartDateDiv_{$v['muc_user_content_id']}').className='COM_displayNone';document.getElementById('forceStartDateEditDiv_{$v['muc_user_content_id']}').className='';">
                            <div id="forceStartDateDiv_{$v['muc_user_content_id']}">{$v['muf_start_date']}</div>
                            <div id="forceStartDateEditDiv_{$v['muc_user_content_id']}" class="COM_displayNone">
                                <form id="forceStartDateForm_{$v['muc_user_content_id']}">
                                    <input type="text" name="startDate" value="{$v['muf_start_date']}" maxlength="10" size="10">
                                    {requestToIdButton name='変更' url='/user/content/update/mode/forceStartDate/userContentId/'|cat:$v['muc_user_content_id'] formId='forceStartDateForm_'|cat:$v['muc_user_content_id'] targetId='forceStartDateSpan_'|cat:$v['muc_user_content_id']}
                                    <span id="forceStartDateSpan_{$v['muc_user_content_id']}"></span>
                                </form>
                            </div>
                        </td>
                        <td class="COM_nowrap {$lineColor}" onClick="document.getElementById('forceEndDateDiv_{$v['muc_user_content_id']}').className='COM_displayNone';document.getElementById('forceEndDateEditDiv_{$v['muc_user_content_id']}').className='';">
                            <div id="forceEndDateDiv_{$v['muc_user_content_id']}">{$v['muf_end_date']}</div>
                            <div id="forceEndDateEditDiv_{$v['muc_user_content_id']}" class="COM_displayNone">
                                <form id="forceEndDateForm_{$v['muc_user_content_id']}">
                                    <input type="text" name="endDate" value="{$v['muf_end_date']}" maxlength="10" size="10">
                                    {requestToIdButton name='変更' url='/user/content/update/mode/forceEndDate/userContentId/'|cat:$v['muc_user_content_id'] formId='forceEndDateForm_'|cat:$v['muc_user_content_id'] targetId='forceEndDateSpan_'|cat:$v['muc_user_content_id']}
                                    <span id="forceEndDateSpan_{$v['muc_user_content_id']}"></span>
                                </form>
                            </div>
                        </td>
                        {/if}
                        <td class="COM_nowrap {$lineColor}">{$v['muc_registration_datetime']}<br>{$v['muc_update_timestamp']}</td>
                        <td class="COM_nowrap {$lineColor}">{$v['muc_deletion_datetime']}</td>
                        <td id="contentTd_{$v['muc_user_content_id']}" class="COM_nowrap {$lineColor}">
                            {requestToIdButton name='削除' url='/user/content/delete/userContentId/'|cat:$v['muc_user_content_id'] targetId='contentTd_'|cat:$v['muc_user_content_id']}
                        </td>
                    </tr>
                    {/foreach}
                </table>
                <!-- // 検索結果リスト終了 -->

            {elseif $form->getMode() === 'search'}

                <p>該当する検索結果はありません。</p>

            {else}

                <p>正しく条件を選択し、検索ボタンを押してください</p>

            {/if}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->