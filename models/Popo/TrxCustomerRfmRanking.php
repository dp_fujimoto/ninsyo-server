<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_customer_rfm_ranking
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxCustomerRfmRanking extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tcr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'customerRfmRankingId' => null,
        'customerId' => null,
        'divisionId' => null,
        'recencyRank' => null,
        'recency' => null,
        'frequencyRank' => null,
        'frequency' => null,
        'moneyAverageRank' => null,
        'moneyAverage' => null,
        'moneyMaxRank' => null,
        'moneyMax' => null,
        'moneyTotalRank' => null,
        'moneyTotal' => null,
        'moneyTotalDataNumber' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 顧客RFMランキングID の値を返します
	 *
     * @return int 顧客RFMランキングID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerRfmRankingId() {
		return $this->_properties['customerRfmRankingId'];
    }


    /**
     * 顧客RFMランキングID の値をセットします
	 *
	 * @param int $customerRfmRankingId 顧客RFMランキングID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerRfmRankingId($customerRfmRankingId) {
		$this->_properties['customerRfmRankingId'] = $customerRfmRankingId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 最終購入日日数ランク の値を返します
	 *
     * @return int 最終購入日日数ランク
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRecencyRank() {
		return $this->_properties['recencyRank'];
    }


    /**
     * 最終購入日日数ランク の値をセットします
	 *
	 * @param int $recencyRank 最終購入日日数ランク
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRecencyRank($recencyRank) {
		$this->_properties['recencyRank'] = $recencyRank;
    }


    /**
     * 最終購入日日数 の値を返します
	 *
     * @return int 最終購入日日数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRecency() {
		return $this->_properties['recency'];
    }


    /**
     * 最終購入日日数 の値をセットします
	 *
	 * @param int $recency 最終購入日日数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRecency($recency) {
		$this->_properties['recency'] = $recency;
    }


    /**
     * 購入頻度ランク の値を返します
	 *
     * @return int 購入頻度ランク
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFrequencyRank() {
		return $this->_properties['frequencyRank'];
    }


    /**
     * 購入頻度ランク の値をセットします
	 *
	 * @param int $frequencyRank 購入頻度ランク
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFrequencyRank($frequencyRank) {
		$this->_properties['frequencyRank'] = $frequencyRank;
    }


    /**
     * 購入頻度 の値を返します
	 *
     * @return int 購入頻度
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFrequency() {
		return $this->_properties['frequency'];
    }


    /**
     * 購入頻度 の値をセットします
	 *
	 * @param int $frequency 購入頻度
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFrequency($frequency) {
		$this->_properties['frequency'] = $frequency;
    }


    /**
     * 平均購入単価ランク の値を返します
	 *
     * @return int 平均購入単価ランク
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyAverageRank() {
		return $this->_properties['moneyAverageRank'];
    }


    /**
     * 平均購入単価ランク の値をセットします
	 *
	 * @param int $moneyAverageRank 平均購入単価ランク
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyAverageRank($moneyAverageRank) {
		$this->_properties['moneyAverageRank'] = $moneyAverageRank;
    }


    /**
     * 平均購入単価 の値を返します
	 *
     * @return int 平均購入単価
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyAverage() {
		return $this->_properties['moneyAverage'];
    }


    /**
     * 平均購入単価 の値をセットします
	 *
	 * @param int $moneyAverage 平均購入単価
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyAverage($moneyAverage) {
		$this->_properties['moneyAverage'] = $moneyAverage;
    }


    /**
     * 最大購入単価ランク の値を返します
	 *
     * @return int 最大購入単価ランク
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyMaxRank() {
		return $this->_properties['moneyMaxRank'];
    }


    /**
     * 最大購入単価ランク の値をセットします
	 *
	 * @param int $moneyMaxRank 最大購入単価ランク
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyMaxRank($moneyMaxRank) {
		$this->_properties['moneyMaxRank'] = $moneyMaxRank;
    }


    /**
     * 最大購入単価 の値を返します
	 *
     * @return int 最大購入単価
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyMax() {
		return $this->_properties['moneyMax'];
    }


    /**
     * 最大購入単価 の値をセットします
	 *
	 * @param int $moneyMax 最大購入単価
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyMax($moneyMax) {
		$this->_properties['moneyMax'] = $moneyMax;
    }


    /**
     * 合計購入金額ランク の値を返します
	 *
     * @return int 合計購入金額ランク
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyTotalRank() {
		return $this->_properties['moneyTotalRank'];
    }


    /**
     * 合計購入金額ランク の値をセットします
	 *
	 * @param int $moneyTotalRank 合計購入金額ランク
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyTotalRank($moneyTotalRank) {
		$this->_properties['moneyTotalRank'] = $moneyTotalRank;
    }


    /**
     * 合計購入金額 の値を返します
	 *
     * @return int 合計購入金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyTotal() {
		return $this->_properties['moneyTotal'];
    }


    /**
     * 合計購入金額 の値をセットします
	 *
	 * @param int $moneyTotal 合計購入金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyTotal($moneyTotal) {
		$this->_properties['moneyTotal'] = $moneyTotal;
    }


    /**
     * tcr_money_total_data_number の値を返します
	 *
     * @return int tcr_money_total_data_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyTotalDataNumber() {
		return $this->_properties['moneyTotalDataNumber'];
    }


    /**
     * tcr_money_total_data_number の値をセットします
	 *
	 * @param int $moneyTotalDataNumber tcr_money_total_data_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyTotalDataNumber($moneyTotalDataNumber) {
		$this->_properties['moneyTotalDataNumber'] = $moneyTotalDataNumber;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























