<?php
/** バッチ関連設定 **/
//バッチ側のCSVファイル格納パス
//define(AUTH_FILE_DIR,'./bat');
define("AUTH_FILE_DIR",SYSTEMS_DIR.'/member-ship-site-files');
// サーバ側のCSVファイル格納パス
define("AUTH_FILE_SERVER_DIR",'/var/www/members/success_group');
define("AUTH_FILE_SERVER_SAVE_DIR",'/var/www/members/success_group/save');
// サーバ側
define("MERGE_KEY",'64561af8fe4afd4de4b67317077d532a');

// 商品コード別マージURL MERGE_URL_に続き商品コードを大文字で定義してください。
// 例：新しい商品コードがABCとし、転送先をhttp://example.jp/dpub/htdocs/merge.phpとした場合
//     define(MERGE_URL_ABC,'http://example.jp/dpub/htdocs/merge.php');
define("MERGE_URL_MLT",'http://www.drmaltz.jp/success_group/members/merge.php?url=mlt');
define("MERGE_URL_MSC",'http://www.milteer.jp/msc/members/merge.php?url=msc');

/** 認証系設定 **/
// ユーザがログイン画面で入力するパスワードと照合するキー
define("LOGIN_KEY",  '2008lee');
// 以下は、ログインパラメータ名。
// 通常は変更なし
define("KEY_EMAIL",  'email');
define("KEY_PASSWD", 'passwd');
define("KEY_ACTION", 'action');
define("KEY_MESSAGE",'message');

//メニューおよびログインページのURL設定
define("URL_MENU", 'menu.php');
define("URL_LOGIN",'login.php');
// セッションでエラーがあった場合の転送先（エラー画面）
define("URL_SESSION_ERROR", 'error.php');

// メッセージ
define("MSG_LOGIN_FAIL",'メールアドレスまたは、パスワードが異なります。ご確認いただき再度ご入力ください。');
define("MSG_AUTHZ_FAIL",'本メニューの閲覧権限をお持ちでありません。');
define("MSG_SESSION_FAIL",'セッション情報がございません。もう一度ログインしてください。');

// 認証データの登録年月開始日
// 例：AUTHZ_START_DAY=11の場合、2011/02/10は、AUTHZ_START_DAYよりも過去なので
//     先月の認証ファイル（2011/01）に登録する。2011/02/11は２月として扱う
define("AUTHZ_START_DAY",11);
?>
