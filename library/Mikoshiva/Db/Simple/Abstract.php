<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Date.php';
require_once 'Mikoshiva/Utility/Copy.php';
require_once 'Mikoshiva/Db/Simple/Exception.php';
require_once 'Mikoshiva/Utility/Array.php';


/**
 * SQL テーブルクラス
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Db_Simple_Abstract {

    /**
     * 強制条件
     *
     * 連想配列のキーに接頭辞を除いたカラム名
     * 連想配列の値に条件となる値
     *
     * array(delete_flag => 0)
     *
     * @var array
     */
    protected $_addWwhere = array();


    /**
     * DB から 1 件データを取得します
     *
     * 1件以上取得出来た場合、Mikoshiva_Db_Simple_Exception 投げます
     * 取得出来なかった場合空の POPO オブジェクトを返します
     *
     * @param string $arName
     * @param mixed $condition
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneSelect($arName, $condition) {


        // ログ書き出し
        //logInfo(LOG_START);



        // Ar のインスタンスを作成
        $ar = null;
        $ar = $this->_getArInstance($arName);


        // プレフィックスを取得
        $primaryList = $ar->info('primary');
        $prefix      = '';
        $prefix      = preg_replace('/^(.*?)_.*$/', '${1}', $primaryList[1]);


        // Popoを通すことでDBのカラムに沿った配列にする。
        // tel1, tel2, tel3などの値も、DB格納の形である tel(xxx-yyy-zzzz等)になる。
        $popo = null;
        $popo = $this->_getPopoInstance($arName);
        $popo = Mikoshiva_Utility_Copy::copyToPopo($popo, $condition);
        $sourceArray = $popo->getProperties();


        // WHERE 句 を生成
        $select = $ar->select();
        foreach ($sourceArray as $k => $v) {

            // 値が NULL と空文字以外を where 句の対象とする
            if (! isBlankOrNull($v)) {

                // カラム名を生成 lastName → mcu_last_name
                $columnName = '';
                $columnName = Mikoshiva_Utility_Convert::variableNameToColumnName($k, $prefix);

                // WHERE
                $select->where($columnName . ' = ?', $v);
            }
        }


        // WHERE の条件に強制条件を追加
        foreach ($this->_addWwhere as $k => $v) {

            // カラム名を生成 lastName → mcu_last_name
            $columnName = '';
            $columnName = "{$prefix}_{$k}";

            // WHERE
            $select->where($columnName . ' = ?', $v);
        }



        // 取得
        $rows = null;
        $rows = $ar->fetchAll($select);


        //------------------------------------------------------------------------------------
        //■取得したデータの処理
        //------------------------------------------------------------------------------------
        $retPopo = null;
        $retPopo = $this->_getPopoInstance($arName);

        //データが1件かをチェック。
        if($this->_checkDataIsOne($rows, __FUNCTION__) === false) {
            //logInfo(LOG_END);

            return $retPopo;
        }

        $work = array();
        $work = $rows->toArray();
        $row  = array();
        $row  = $work[0];

        $retPopo = Mikoshiva_Utility_Copy::copyDbProperties($retPopo, $row);

        // ログ終了
        //logInfo(LOG_END);

        return $retPopo;
    }


    /**
     * 指定された ID でにヒットしたデータを返します
     *
     * @param string $arName
     * @param int $id
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneSelectById($arName, $id) {


        // ログ書き出し
        //logInfo(LOG_START);



        // Ar のインスタンスを生成
        $ar = null;
        $ar = $this->_getArInstance($arName);


        // プライマリキーを取得
        $primaryList = $ar->info('primary');


        // Prefix の取得
        $prefix      = '';
        $prefix      = preg_replace('/^(.*?)_.*$/', '${1}', $primaryList[1]);


        // SELECT 文を生成
        $select = $ar->select();
        $select->where($primaryList[1] . ' = ?', $id);


        // WHERE の条件に強制条件を追加
        foreach ($this->_addWwhere as $k => $v) {

            // カラム名を生成 lastName → mcu_last_name
            $columnName = '';
            $columnName = "{$prefix}_{$k}";

            // WHERE
            $select->where($columnName . ' = ?', $v);
        }


        // 実行＆取得
        $rows = null;
        $rows = $ar->fetchAll($select);


        //------------------------------------------------------------------------------------
        //■取得したデータの処理
        //------------------------------------------------------------------------------------
        $popo = null;
        $popo = $this->_getPopoInstance($arName);

        //データが1件かをチェック。
        if($this->_checkDataIsOne($rows, __FUNCTION__) === false) {
            //logInfo(LOG_END);

            return $popo;
        }

        $work = array();
        $work = $rows->toArray();
        $row  = array();
        $row  = $work[0];


        $popo = Mikoshiva_Utility_Copy::copyDbProperties($popo, $row);

        // ログ終了
        //logInfo(LOG_END);

        return $popo;
    }


    /**
     * 引数に与えられた actionForm からプライマリーキーを取得し、ヒットしたデータを返します
     *
     * @param string $arName
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneSelectByIdFromForm($arName, $actionForm) {


        // ログ書き出し
        //logInfo(LOG_START);



        // Ar オブジェクトを生成
        $ar = null;
        $ar = $this->_getArInstance($arName);


        // primary 情報を取得
        $primaryList = $ar->info('primary');
        $primaryName = $primaryList[1];                                                   // mcu_customer_id
        $copyToArray = Mikoshiva_Utility_Copy::copyToArray($actionForm);                  // ActinForm / POPO / Array を配列化
        $condition   = Mikoshiva_Utility_Convert::columnNameToVariableName($primaryName); // customerId


        // Prefix の取得
        $prefix      = '';
        $prefix      = preg_replace('/^(.*?)_.*$/', '${1}', $primaryList[1]);


        // キーが設定されていなければエラー
        if (! isset($copyToArray[$condition])) {
            $message = "【{__CLASS__}#{__FUNCTION__} （__LINE__）】与えられた配列にプライマリーキー {$primaryList[1]} が設定されていません。";
            logDebug($message);
            throw new Mikoshiva_Db_Simple_Exception($message);
        }


        // WHERE 句を生成
        $select = $ar->select();
        $select->where($primaryList[1] . ' = ?', $copyToArray[$condition]);



        // WHERE の条件に強制条件を追加
        foreach ($this->_addWwhere as $k => $v) {

            // カラム名を生成 lastName → mcu_last_name
            $columnName = '';
            $columnName = "{$prefix}_{$k}";

            // WHERE
            $select->where($columnName . ' = ?', $v);
        }


        // 実行・取得
        $rows = null;
        $rows = $ar->fetchAll($select);


        //----------------------------------------------
        //■取得したデータの処理
        //----------------------------------------------
        // POPO を生成
        $popo = null;
        $popo = $this->_getPopoInstance($arName);


        // データが 1 件かをチェック。
        if($this->_checkDataIsOne($rows, __FUNCTION__) === false) {
            //logInfo(LOG_END);

            return $popo;
        }


        // POPO に詰める
        $work = array();
        $work = $rows->toArray();
        $row = array();
        $row = $work[0];
        $popo = Mikoshiva_Utility_Copy::copyDbProperties($popo, $row);


        // ログ終了
        //logInfo(LOG_END);

        return $popo;
    }



    /**
     * 指定された条件にマッチしたデータを返します
     *
     * @param string $arName
     * @param mixed $condition
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleManySelect($arName, $condition) {


        // ログ書き出し
        //logInfo(LOG_START);



        // Ar オブジェクトを生成
        $ar = null;
        $ar = $this->_getArInstance($arName);


        // プレフィックスを取得
        $primaryList = $ar->info('primary');
        $prefix      = '';
        $prefix      = preg_replace('/^(.*?)_.*$/', '${1}', $primaryList[1]);


        // Popoを通すことでDBのカラムに沿った配列にする。
        // tel1, tel2, tel3などの値も、DB格納の形である tel(xxx-yyy-zzzz等)になる。
        $popo = null;
        $popo = $this->_getPopoInstance($arName);
        $popo = Mikoshiva_Utility_Copy::copyToPopo($popo, $condition);
        $sourceArray = $popo->getProperties();


        // WHERE 句 を生成
        $select = $ar->select();
        foreach ($sourceArray as $k => $v) {

            // NULL じゃないものだけ select の対象にする
            if (! isBlankOrNull($v)) {

                // カラム名を生成 lastName → mcu_last_name
                $columnName = '';
                $columnName = Mikoshiva_Utility_Convert::variableNameToColumnName($k, $prefix);

                // WHERE
                $select->where($columnName . ' = ?', $v);
            }
        }


        // WHERE の条件に強制条件を追加
        foreach ($this->_addWwhere as $k => $v) {

            // カラム名を生成 lastName → mcu_last_name
            $columnName = '';
            $columnName = "{$prefix}_{$k}";

            // WHERE
            $select->where($columnName . ' = ?', $v);
        }


        // 取得
        $rows = null;
        $rows = $ar->fetchAll($select);


        // 1件も取得出来なければ空の配列を返す
        if ($rows->count() === 0) {
            //logInfo(LOG_END);

            return array();
        }

        //------------------------------------------------------------------------------------
        //■取得したデータの処理
        //------------------------------------------------------------------------------------
        $result = array();
        foreach($rows->toArray() as $row) {
            $popo = null;
            $popo = $this->_getPopoInstance($arName);

            $popo = Mikoshiva_Utility_Copy::copyDbProperties($popo, $row);
            $result[] = $popo;

        }

        // ログ終了
        //logInfo(LOG_END);

        return $result;

    }


    /**
     * プライマリキーを条件に登録・更新処理を行います。
     * プライマリキーが引数の値に存在しなければ登録処理を行います。
     *
     * @param string $arName
     * @param Popo_Abstract|Mikoshiva_Controller_Mapping_Form_Interface|array $source Popo、アクションフォーム、または配列
     * @return Popo_Abstract $result 挿入したデータ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/05
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneCreateOrUpdateById($arName, $source) {

        // ログ書き出し
        //logInfo(LOG_START);



        $ar = null;
        $ar = $this->_getArInstance($arName);

        // プライマリキーを取得
        $primaryList = $ar->info('primary');
        $prefix = '';
        $prefix = preg_replace('/^(.*?)_.*$/', '${1}', $primaryList[1]);


        // Popoを通すことでDBのカラムに沿った配列にする。
        // tel1, tel2, tel3などの値も、DB格納の形である tel(xxx-yyy-zzzz等)になる。
        $popo        = null;
        $popo        = $this->_getPopoInstance($arName);
        $popo        = Mikoshiva_Utility_Copy::copyToPopo($popo, $source);
        $popoArray   = array();
        $popoArray   = $popo->getProperties();

//dumper($popoArray);//exit;
        //-----------------------------------------------
        // ■$popoArray と $source の共通項目リストを生成
        //   （キーと値がマッチするもののみ）
        //-----------------------------------------------
        $sourceArray = array();
        $sourceArray = Mikoshiva_Utility_Array::intersectAssoc($popoArray,
                                                               Mikoshiva_Utility_Copy::copyToArray($source),
                                                               array('zip' => array('zip1', 'zip2'),
                                                                     'tel' => array('tel1', 'tel2', 'tel3'),
                                                                     'fax' => array('fax1', 'fax2', 'fax3'))
                                                               );

//dumper($sourceArray);exit;
        //-----------------------------------------------
        // ■プライマリキー名を取得
        //-----------------------------------------------
        $primaryKeyName = '';
        $primaryKeyName = $primaryList[1];


        //-----------------------------------------------
        // ■共通項目リストのキー名を DB のカラム名にして再生成する
        //-----------------------------------------------
        $data = array();
        foreach($sourceArray as $key => $value) {

            // キー名をカラム名に変換
            $columnName = '';
            $columnName = Mikoshiva_Utility_Convert::variableNameToColumnName($key, $prefix);

            // 値を詰める
            $data[$columnName] = $value;
        }
        $data[$prefix . '_update_datetime'] = Mikoshiva_Date::mikoshivaNow();
        $data[$prefix . '_update_timestamp'] = null;


        // 処理結果
        $result = null;

// dumper($data);exit;

        //-------------------------------------------------------------------
        // プライマリキーの有無で処理を振り分ける
        // $data にプライマリキーが存在していれば     UPDATE
        // $data にプライマリキーが存在していなければ INSERT
        //-------------------------------------------------------------------
        if (isset($data[$primaryKeyName])) {


            //-----------------------------------------------
            // 更新
            //-----------------------------------------------
            logDebug('-- UPDATE START --');


            // 不正な値であれば例外を投げる
            if (! numberCheck($data[$primaryKeyName]) || (string)$data[$primaryKeyName] === '0') {
                throw new Mikoshiva_Exception('【Mikoshiva_Db_Table_Abstract#simpleOneCreateOrUpdateById】プライマリキーの値は 0 以上の数字文字列で入力して下さい・');
            }


            // WHERE
            $where = array();
            $where[$primaryKeyName . ' = ?'] = $data[$primaryKeyName];



// dumper($data);exit;

            // 実行
            //updateは変更した行数を返すので、プライマリキーが取れない
            $ar->update($data, $where);
            $id = $data[$primaryKeyName];


            logDebug('-- UPDATE END --');

        } else {
            //-----------------------------------------------
            // 新規登録
            //-----------------------------------------------
            logDebug('-- INSERT START --');

            unset ($data[$primaryList[1]]);
            /////////$data[$prefix . '_update_datetime'] = Mikoshiva_Date::mikoshivaNow();

           $data[$prefix . '_delete_flag'] = '0';
           $data[$prefix . '_registration_datetime'] = Mikoshiva_Date::mikoshivaNow();
// dumper($data);exit;
            // 実行
            $id = $ar->insert($data);
            logDebug('-- INSERT END --');
        }

        $result = $this->simpleOneSelectById($arName, $id);
        // ログ終了
        //logInfo(LOG_END);


        return $result;
    }


    /**
     * カラム情報の一覧を返します
     *
     * @param string $arName
     * @return array カラム情報の一覧
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getColumnList($arName) {
        $ar = null;
        $ar = $this->_getArInstance($arName);
        return $ar->info('cols');
    }


    /**
     * カラム名一覧を取得し、それをキーとした配列をかえします
     *
     * @param string $arName
     * @param $initData 配列の初期値
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function createArrayColumnNames($arName, $initData = null) {

        // ログスタート
        //logInfo(LOG_START);

        $ar = null;
        $ar = $this->_getArInstance($arName);


        // 配列の値を取得
        $cols = $ar->getColumnList();


        // 配列の値をキーにした配列を生成
        $newArray = array();
        foreach ($cols as $k => $v) {
            $newArray[ $v ] = $initData;
        }


        // ログエンド
        //logInfo(LOG_START);


        return $newArray;
    }

    /**
     * アクティブレコード名を元に、それに対応したArのインスタンスを取得して返します
     *
     * @param  string                      $arName アクティブレコード名
     * @return Mikoshiva_Db_Table_Abstract $object Arクラスのインスタンス
     *
     * @author ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    function _getArInstance($arName) {
        //logInfo(LOG_START);

        $path = '';
        $path = str_replace('_', '/', $arName) . '.php';

        require_once $path;

        $object = new $arName();
        //logInfo(LOG_END);

        return $object;
    }

    /**
     * アクティブレコード名を元に、それに対応したPopoのインスタンスを取得して返します
     *
     * @param  string        $arName アクティブレコード名
     * @return Popo_Abstract $object アクティブレコードに対応したPopoのインスタンス
     *
     * @author ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    function _getPopoInstance($arName) {
        //logInfo(LOG_START);

        $popoName = '';
        $popoName = preg_replace('/^.*Ar_/', 'Popo_', $arName);
        $path     = '';
        $path     = str_replace('_', '/', $popoName) . '.php';
        require_once $path;

        $object = new $popoName();
        //logInfo(LOG_END);

        return $object;
    }

    /**
     * 与えられた配列の長さが1か否かをチェックする
     *
     * @param  array   $rows
     * @return boolean boolean
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    function _checkDataIsOne(Mikoshiva_Db_Table_Rowset $rows, $functionName) {
        //logInfo(LOG_START);

        // 1件も取得出来なければ空の配列を返す
        if ($rows->count() === 0) {
            // ログ終了
            //logInfo(LOG_END);

            return false;
        }

        // 2件以上取得出来たらエラー
        if ($rows->count() > 1) {
            throw new Mikoshiva_Db_Simple_Exception('【' . $functionName . '】2件以上の値が取得されました。');
        }
        return true;
    }
}






