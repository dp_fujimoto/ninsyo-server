<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Action/Abstract.php';
/**
 * Mikoshiva_Controller_Action_Admin_Abstract
 *
 * ログインチェックを行っているので管理画面を扱うアクションコントローラーでは
 * このクラスを継承すること
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/03
 * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Action_Admin_Abstract extends Mikoshiva_Controller_Action_Abstract {


    /**
     * Action メソッドの処理の前に認証チェックを行います
     * 認証に失敗した場合はログイン画面へ強制的に遷移します
     *
     * init() の後に処理されます
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see library/Zend/Controller/Zend_Controller_Action#preDispatch()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/04
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function preDispatch() {


        // デフォルトページ以外レイアウトは非表示にする
        if( $this->_request->getModuleName() !== 'default' ) {
            $this->_helper->layout->disableLayout(); // layout
        }
        // セッションスタート
        //Zend_Session::start();

        // staff が存在しなければエラー
        if ( ! Zend_Session::namespaceIsset('staff') ) {

            $this->_redirect('/login');
        }

        // ◆--- 本番サーバー ---◆
        if (preg_match('#\.[a-z]{2,3}$#', $this->_request->getServer('HTTP_HOST')) === 1) {
            // ◆--- https で無ければ再度ログインさせる ---◆
            if ($this->_request->getServer('HTTPS') !== 'on' && isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
                if (_SERVER_MODE_ID_ !== 'test.mikoshiva') {
                    $_httpHost = $this->_request->getServer('HTTP_HOST');
                    $this->_redirect("https://{$_httpHost}/login");
                }
            };
        }

        // 親メソッドを処理
        parent::preDispatch();


        // 親メソッドを処理
        /***
        parent::preDispatch();
        Zend_Debug::dump('111111111111111111111111111111');
        $this->_forward('list');
        Zend_Debug::dump('222222222222222222222222222222');
        $this->_forward('testpp');
        $this->getRequest()->setDispatched(true);
        ***/
    }

}





















