<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstStaff.php';


/**
 * 指定した処理ごとに初期化を行う
 *
 * @author         K.sekiya <kurachi0223@gmail.com>
 * @since        2010/02/15
 * @version        SVN: $Id:
 *
 */
class Tool_Models_BatchTest_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定した処理ごとに初期化を行う
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $sql = '';

        switch($actionForm->getMode()) {
            case '1':
                    set_time_limit(0);
                    break;
            default:
                    set_time_limit(600);
                    break;
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return true;
    }

}
