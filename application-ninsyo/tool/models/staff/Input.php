<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstStaff.php';

/**
 * 指定したスタッフIDの情報を入力画面にプリセットする。
 *
 * @author         K.Fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/22
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Staff_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したスタッフIDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        if ($actionForm->getLvFlag() === '1') {

            $staffId = $actionForm->getStaffId();
        } else {
            $staffId = $context->getStaff()->mstStaff->getStaffId();
        }
        $staff   = null;

        if (!isBlankOrNull($staffId)) {
            logInfo('(' . __LINE__ . ')', 'if：スタッフIDあり');

            // SQL作成
            $sql = '';
            $sql .= 'SELECT';
            $sql .= ' *';
            $sql .= ' FROM mst_staff ';
            $sql .= ' WHERE 1 = 1 ';

            // パラメータセット
            $params = array();

            $sql .= ' AND mst_staff_id =  ?';
            $params[] = $staffId;


            // SQL実行＋全件取得
            $res = array();
            $res = $this->_db->fetchAll($sql,$params);

            $row = Mikoshiva_Utility_Array::extractBeginningOfPrefix( 'mst_' , $res[0] );

            // Popoに一行ずつ入れていく
            $rowPopo = Mikoshiva_Utility_Copy::copyDbProperties( new Popo_MstStaff() , $row );

            $actionForm = Mikoshiva_Utility_Copy::copyPopoToActionFormProperties($actionForm, $rowPopo);

        } else {
            logInfo('(' . __LINE__ . ')', 'else：スタッフIDなし');

        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $staff;
    }

}
