<?php



/**
 * 発送リスト作成時の発送グループラベルを取得する
 *
 * @author 		K.sekiya <kurachi0223@gmail.com>
 * @since		2010/02/15
 * @version		SVN: $Id:
 *
 */
class Mikoshiva_Plugin_Shipping_ShippingGroupLabelSelect {

	/**
	 * 発送リスト作成時の発送グループラベルを取得する
	 *
	 * @param Popo_MstDirection $direction 指示
	 *
	 * @return string 発送グループラベル
	 *
	 * @author k.sekiya
	 * @since version - 2010/03/18
	 */
	public function select( $direction) {


		try{

			//-----------------------------------------------
			// 発送グループラベルを取得する
			//-----------------------------------------------
			// シンプルクラス インスタンス作成
			$db = null;
			$db = new Mikoshiva_Db_Simple();

			// 検索条件
			$where = array();
			$where['targetProductId'] = $direction->getProductId(); // 対象商品ID

			// SQL実行＋１件取得
			$dmEnclosing = null;
			$dmEnclosing = $db->simpleManySelect('Mikoshiva_Db_Ar_MstDmEnclosing',$where);

			// 発送グループラベル
			$shippingGroupLabel = '';
			$shippingGroupLabel = $dmEnclosing[0]->getShippingGroupLabel();

			// 除外商品ID配列
			$exclusionProductIdArr = array();
			foreach($dmEnclosing as $k => $v){
				$exclusionProductIdArr[] = $v->getExclusionProductId();
			}


			//-----------------------------------------------
			// 購入商品を全て取得
			//-----------------------------------------------
			// シンプルクラス インスタンス作成
			$db = null;
			$db = new Mikoshiva_Db_Simple();

			// 検索条件
			$where = array();
			$where['customerId'] = $direction->getCustomerId(); // 顧客ID単位で商品IDを取得する

			// SQL実行＋１件取得
			$direction = null;
			$direction = $db->simpleManySelect('Mikoshiva_Db_Ar_MstDirection',$where);


			//-----------------------------------------------
			// DMを送っていい顧客かチェック
			//-----------------------------------------------
			foreach($direction as $k => $v){

				// 購入商品ID
				$productId = $v->getProductId();

				// 購入商品が一つでも除外商品に含まれていたら、DMは送らないのでnullを返す
				if( in_array( $productId , $exclusionProductIdArr ) ) {
					return null;
				}
			}

			// 発送グループラベルを返す
			return $shippingGroupLabel;

		// エラーがおこれば、ログ記録→nullを返す
		} catch (Exception $e) {

			return null;

		}
	}
}