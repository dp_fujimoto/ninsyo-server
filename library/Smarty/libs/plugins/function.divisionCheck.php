<?php

require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/Utility/Session.php';
require_once 'Mikoshiva/Logger.php';


/**
 * セッションにセットされた部門IDを返します。
 * ただし、部門変更が行われていて、かつフラグとしてtrueが渡された場合、
 * 変更後の部門IDを返します。
 *
 * @param  array  $params
 *                      boolean flagChangeDivision false|フラグ
 * @param  Smarty &$smarty
 * @return string                      部門ID
 *
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/03
 * @version SVN:$Id: function.divisionCheck.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_divisionCheck($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);

    $result = Mikoshiva_Utility_Session::getNowDivisionId();

    Mikoshiva_Logger::info(LOG_END);
    return $result;
}


