<?php

// サーバーモードを取得（必ず一番はじめに読み込む）
require_once dirname(__FILE__) . '/../../../configs/server-mode-config.php';

$domain = preg_match('#\.#', _SERVER_MODE_ID_) === 0
        ? 'www.' . _SERVER_MODE_ID_
        : _SERVER_MODE_ID_;

$s = ''
   . 'document.write(\'<script type="text/javascript" src="https://' . $domain . '.com/outside/js/jquery.js"             charset="utf-8"><\/script>\');' . "\n"
   . 'document.write(\'<script type="text/javascript" src="https://' . $domain . '.com/outside/js/jquery.upload.tani.js" charset="utf-8"><\/script>\');' . "\n"
   . 'document.write(\'<script type="text/javascript" src="https://' . $domain . '.com/outside/js/jquery.cookie.js"      charset="utf-8"><\/script>\');' . "\n"
   . 'document.write(\'<script type="text/javascript" src="https://' . $domain . '.com/outside/js/common.js"             charset="utf-8"><\/script>\');' . "\n"
   . '';

echo $s;
