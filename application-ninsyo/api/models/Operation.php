<?php

/**
 * 業務クラス
 * 他の業務から呼ばれるクラス
 *
 * 【ファンクション集】
 *
 * @author kawakami
 *
 */
class Api_Models_Operation {

    /**
    * ハッシュを作成する
    *
    */
    function createUserHash($string){

        require_once 'api/models/classes/CreateUserHash.php';

        $class = null;
        $class = new Api_Models_Classes_CreateUserHash();

        $res = $class->execute($string);

        return $res;

    }

    /**
    * セッションIDをチェックする
    *
    */
    function checkSessionId($sessionId){

        require_once 'api/models/classes/CheckSessionId.php';

        $class = null;
        $class = new Api_Models_Classes_CheckSessionId();

        $res = $class->execute($sessionId);

        return $res;

    }

    /**
    * 指定したhash値とコンテンツIDからユーザーコンテンツを登録する
    *
    */
    function insertUserContentDetail($list = array(), $deleteFlag = '0', $serverNameList){

        require_once 'api/models/classes/user-content/InsertUserContentDetail.php';

        $class = null;
        $class = new Api_Models_Classes_UserContent_InsertUserContentDetail();

        $res = $class->execute($list,$deleteFlag, $serverNameList);

        return $res;

    }

    /**
    * 条件キャッシュファイルを削除する
    *
    */
    function deleteConditionCacheFile(Mikoshiva_Controller_Mapping_Form_Interface $actionForm, Mikoshiva_Controller_Mapping_ActionContext $context){

        require_once 'api/models/content/DeleteConditionCacheFile.php';

        $class = null;
        $class = new Api_Models_Content_DeleteConditionCacheFile();

        $res = $class->execute($actionForm,$context);

        return null;

    }

    /**
    * ユーザーキャッシュファイルを削除する
    *
    */
    function deleteUserCacheFile(Mikoshiva_Controller_Mapping_Form_Interface $actionForm, Mikoshiva_Controller_Mapping_ActionContext $context){

        require_once 'api/models/user-content/DeleteUserCacheFile.php';

        $class = null;
        $class = new Api_Models_UserContent_DeleteUserCacheFile();

        $res = $class->execute($actionForm,$context);

        return null;

    }

}