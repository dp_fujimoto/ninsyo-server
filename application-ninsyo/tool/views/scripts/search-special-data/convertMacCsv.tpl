{if $form->getFlag() !== '1'}
<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">windows用csvファイルをmac用に変換</h2>

            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" enctype="multipart/form-data" method="post" onSubmit="return false;">

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            CSVファイル<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            <input type="file" name="addressFile" size="80">
                            <br>※mikoshivaなどでダウンロードしたcsvファイルで、未加工のもの。macで文字化けがある場合
                        </td>
                    </tr>
                </table>

                <!-- // ページ下部のボタン開始 -->
                {currentTabButton name='　　　変　換　　　' url='/tool/search-special-data/convert-mac-csv' tabId=$tabId  formId='inputForm'}
                <!-- // ページ下部のボタン終了 -->

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->
    </tr>

</table>
<!-- // メインテーブル終了 -->
{/if}