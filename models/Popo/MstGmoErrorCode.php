<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_gmo_error_code
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstGmoErrorCode extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mge';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'gmoErrorCodeId' => null,
        'errorCode' => null,
        'errorCodeDetail' => null,
        'settlementStatus' => null,
        'settlementTarget' => null,
        'errorContent' => null,
        'workaround' => null,
        'errorMessage' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * GMOエラーコードID の値を返します
	 *
     * @return int GMOエラーコードID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGmoErrorCodeId() {
		return $this->_properties['gmoErrorCodeId'];
    }


    /**
     * GMOエラーコードID の値をセットします
	 *
	 * @param int $gmoErrorCodeId GMOエラーコードID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGmoErrorCodeId($gmoErrorCodeId) {
		$this->_properties['gmoErrorCodeId'] = $gmoErrorCodeId;
    }


    /**
     * エラーコード の値を返します
	 *
     * @return char エラーコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorCode() {
		return $this->_properties['errorCode'];
    }


    /**
     * エラーコード の値をセットします
	 *
	 * @param char $errorCode エラーコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorCode($errorCode) {
		$this->_properties['errorCode'] = $errorCode;
    }


    /**
     * エラー詳細コード の値を返します
	 *
     * @return char エラー詳細コード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorCodeDetail() {
		return $this->_properties['errorCodeDetail'];
    }


    /**
     * エラー詳細コード の値をセットします
	 *
	 * @param char $errorCodeDetail エラー詳細コード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorCodeDetail($errorCodeDetail) {
		$this->_properties['errorCodeDetail'] = $errorCodeDetail;
    }


    /**
     * 決済状態 の値を返します
	 *
     * @return char 決済状態
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSettlementStatus() {
		return $this->_properties['settlementStatus'];
    }


    /**
     * 決済状態 の値をセットします
	 *
	 * @param char $settlementStatus 決済状態
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSettlementStatus($settlementStatus) {
		$this->_properties['settlementStatus'] = $settlementStatus;
    }


    /**
     * 課金対象 の値を返します
	 *
     * @return char 課金対象
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSettlementTarget() {
		return $this->_properties['settlementTarget'];
    }


    /**
     * 課金対象 の値をセットします
	 *
	 * @param char $settlementTarget 課金対象
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSettlementTarget($settlementTarget) {
		$this->_properties['settlementTarget'] = $settlementTarget;
    }


    /**
     * エラー内容 の値を返します
	 *
     * @return varchar エラー内容
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorContent() {
		return $this->_properties['errorContent'];
    }


    /**
     * エラー内容 の値をセットします
	 *
	 * @param varchar $errorContent エラー内容
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorContent($errorContent) {
		$this->_properties['errorContent'] = $errorContent;
    }


    /**
     * 対処方法 の値を返します
	 *
     * @return varchar 対処方法
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getWorkaround() {
		return $this->_properties['workaround'];
    }


    /**
     * 対処方法 の値をセットします
	 *
	 * @param varchar $workaround 対処方法
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setWorkaround($workaround) {
		$this->_properties['workaround'] = $workaround;
    }


    /**
     * 顧客メッセージ の値を返します
	 *
     * @return varchar 顧客メッセージ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorMessage() {
		return $this->_properties['errorMessage'];
    }


    /**
     * 顧客メッセージ の値をセットします
	 *
	 * @param varchar $errorMessage 顧客メッセージ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorMessage($errorMessage) {
		$this->_properties['errorMessage'] = $errorMessage;
    }


}

























