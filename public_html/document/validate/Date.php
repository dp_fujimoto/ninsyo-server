<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Validate - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/validate">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> バリデータ"><?php echo basename(__FILE__, '.php') ?> バリデータ</h1>

            <p>
              入力値が、指定したDate形式の文字列かを検証。フォーマットは以下の4形式のみ指定可能。<br>
            </p>
            <ul>
              <li>'yyyy/MM/dd'</li>
              <li> 'yyyy/MM/dd HH:mm:ss'</li>
              <li>'yyyy-MM-dd HH:mm:ss'</li>
              <li>'yyyy-MM-dd'</li>
            </ul>



            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <h3>通常の検証方法</h3>
            <p>
              date=2010/02/25 と言う形でリクエストが送信された場合を想定しています。
            </p>
            <pre class="wiki">
            date:
              0: [Date, 'yyyy/MM/dd HH:mm:ss']
            </pre>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>