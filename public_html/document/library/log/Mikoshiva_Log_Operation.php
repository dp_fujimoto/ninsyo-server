<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#staffLogRegister">Mikoshiva_Log_Operation#staffLogRegister - オペレーション内容を記録する</a></li>
              <li><a href="#selectOperationLogByCustomerId">Mikoshiva_Log_Operation#selectOperationLogByCustomerId - カスタマー ID を元にオペレーション内容を取得する</a></li>
              <li><a href="#selectOperationLogByProductCode">Mikoshiva_Log_Operation#selectOperationLogByProductCode - 商品コードを元にオペレーション内容を取得する</a></li>
            </ol>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple#simpleOneSelect
            ============================================== -->
            <h2 id="Mikoshiva_Log_Operation#staffLogRegister"><a name="simpleOneSelect">Mikoshiva_Db_Simple#simpleOneSelect</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              オペレーション内容を記録します
            </p>

            <h3>クラス定数（オペレーション名）</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_LOGIN</td>
                <td>string</td>
                <td>OPERATION_NAME_LOGIN</td>
                <td>ログイン</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CANCEL</td>
                <td>string</td>
                <td>OPERATION_NAME_CANCEL</td>
                <td>キャンセル</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CANCEL_STOP</td>
                <td>string</td>
                <td>OPERATION_NAME_CANCEL_STOP</td>
                <td>解約（停止処理）</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CANCEL_ANNULMENT</td>
                <td>string</td>
                <td>OPERATION_NAME_CANCEL_ANNULMENT</td>
                <td> 解約（取消処理）</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CHANGE_PAYMENT</td>
                <td>string</td>
                <td>OPERATION_NAME_CHANGE_PAYMENT</td>
                <td>決済方法変更</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CHANGE_CARD_NO</td>
                <td>string</td>
                <td>OPERATION_NAME_CHANGE_CARD_NO</td>
                <td>カード番号変更</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CHANGE_SHIPPING_ADDRESS</td>
                <td>string</td>
                <td>OPERATION_NAME_CHANGE_SHIPPING_ADDRESS</td>
                <td>配送先変更</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CREATE_SHIPPING_ADDRESS</td>
                <td>string</td>
                <td>OPERATION_NAME_CREATE_SHIPPING_ADDRESS</td>
                <td>配送先新規登録</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_STOP_SHIPPING</td>
                <td>string</td>
                <td>OPERATION_NAME_STOP_SHIPPING</td>
                <td>発送停止</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_STOP_SETTLEMENT</td>
                <td>string</td>
                <td>OPERATION_NAME_STOP_SETTLEMENT</td>
                <td>支払方法変更</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_EDIT_CUSTOMER</td>
                <td>string</td>
                <td>OPERATION_NAME_EDIT_CUSTOMER</td>
                <td>顧客情報変更</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_SETTLEMENT_RECEIVABLE</td>
                <td>string</td>
                <td>OPERATION_NAME_SETTLEMENT_RECEIVABLE</td>
                <td>未収請求</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_FAX_ORDER</td>
                <td>string</td>
                <td>OPERATION_NAME_FAX_ORDER</td>
                <td>FAX オーダー</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_UPGRADE_ANNUAL_SUBSCRIPTION</td>
                <td>string</td>
                <td>OPERATION_NAME_UPGRADE_ANNUAL_SUBSCRIPTION</td>
                <td>年間商品アップグレード</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_UPDATE_ANNUAL_SUBSCRIPTION</td>
                <td>string</td>
                <td>OPERATION_NAME_UPDATE_ANNUAL_SUBSCRIPTION</td>
                <td>年間更新</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_RE_SUBSCRIPTION</td>
                <td>string</td>
                <td>OPERATION_NAME_RE_SUBSCRIPTION</td>
                <td>再購読</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_PAY_BACK</td>
                <td>string</td>
                <td>OPERATION_NAME_PAY_BACK</td>
                <td>返金</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_RETURN_PRODUCT</td>
                <td>string</td>
                <td>OPERATION_NAME_RETURN_PRODUCT</td>
                <td>返品</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_CREATE_SAME_CUSTOMER</td>
                <td>string</td>
                <td>OPERATION_NAME_CREATE_SAME_CUSTOMER</td>
                <td>同一顧客登録</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_SIMILAR_TO_SAME_CUSTOMER</td>
                <td>string</td>
                <td>OPERATION_NAME_SIMILAR_TO_SAME_CUSTOMER</td>
                <td>類似顧客から同一顧客登録</td>
                <td>オペレーション名</td>
              </tr>
              <tr>
                <td>NAME_OPERATION_NAME_UPDATE_SHIPPING_ADDRESS</td>
                <td>string</td>
                <td>OPERATION_NAME_UPDATE_SHIPPING_ADDRESS</td>
                <td>配送先更新</td>
                <td>オペレーション名</td>
              </tr>
            </table>

              <!--  -->

            <h3>クラス定数（オペレーション内容（詳細））</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_CARD_TO_PAYMENT_DELIVERY</td>
                <td>string</td>
                <td>OPERATION_CONTENT_CARD_TO_PAYMENT_DELIVERY</td>
                <td>カードから代引き</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_CARD_TO_PAYMENT_DELIVERY</td>
                <td>string</td>
                <td>OPERATION_CONTENT_CARD_TO_PAYMENT_DELIVERY</td>
                <td>カードから銀行振込</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_BOPERATION_CONTENT_ANK_TRANSFER_TO_CARD</td>
                <td>string</td>
                <td>OPERATION_CONTENT_BANK_TRANSFER_TO_CARD</td>
                <td>銀行振込からカード</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_BANK_TRANSFER_TO_PAYMENT_DELIVERY</td>
                <td>string</td>
                <td>OPERATION_CONTENT_BANK_TRANSFER_TO_PAYMENT_DELIVERY</td>
                <td>銀行振込から代引き</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_PAYMENT_DELIVERY_TO_CARD</td>
                <td>string</td>
                <td>OPERATION_CONTENT_PAYMENT_DELIVERY_TO_CARD</td>
                <td>代引きからカード</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_PAYMENT_DELIVERY_TO_CARD</td>
                <td>string</td>
                <td>OPERATION_CONTENT_PAYMENT_DELIVERY_TO_CARD</td>
                <td>代引きから銀行振込</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_PAY_BACK_UNPROCESSED</td>
                <td>string</td>
                <td>OPERATION_CONTENT_PAY_BACK_UNPROCESSED</td>
                <td>返金（未処理）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_PAY_BACK_REQUEST</td>
                <td>string</td>
                <td>OPERATION_CONTENT_PAY_BACK_REQUEST</td>
                <td>返金（返金依頼中）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_PAY_BACK_COMPLETE</td>
                <td>string</td>
                <td>OPERATION_CONTENT_PAY_BACK_COMPLETE</td>
                <td>返金（返金処理済）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_RETURN_PRODUCT_UNPROCESSED</td>
                <td>string</td>
                <td>OPERATION_CONTENT_RETURN_PRODUCT_UNPROCESSED</td>
                <td>返品（未処理ち）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_RETURN_PRODUCT_WAIT</td>
                <td>string</td>
                <td>OPERATION_CONTENT_RETURN_PRODUCT_WAIT</td>
                <td>返品（返品待ち）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_RETURN_PRODUCT_MISSING</td>
                <td>string</td>
                <td>OPERATION_CONTENT_RETURN_PRODUCT_MISSING</td>
                <td>返品（欠品）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_RETURN_PRODUCT_MISSING_COMPLETE</td>
                <td>string</td>
                <td>OPERATION_CONTENT_RETURN_PRODUCT_MISSING_COMPLETE</td>
                <td>返品（返品済（欠品））</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_RETURN_PRODUCT_COMPLETE</td>
                <td>string</td>
                <td>OPERATION_CONTENT_RETURN_PRODUCT_COMPLETE</td>
                <td>返品（返品済）</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
              <tr>
                <td>CONTENT_OPERATION_CONTENT_SHIPPING_ADDRESS_MY_HOME</td>
                <td>string</td>
                <td>OPERATION_CONTENT_SHIPPING_ADDRESS_MY_HOME</td>
                <td>配送先（変更・新規登録）が自宅の場合</td>
                <td>オペレーション内容（詳細）</td>
              </tr>
            </table>




            <!-- ==============================================
            ■Mikoshiva_Log_Operation#staffLogRegister
            ============================================== -->
            <h2 id="Mikoshiva_Log_Operation#staffLogRegister"><a name="staffLogRegister">Mikoshiva_Log_Operation#staffLogRegister</a></h2>

            <p>
              オペレーション内容を記録する
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$oprationName</td>
                <td>string</td>
                <td><br></td>
                <td>オペレーション名</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$operationContent</td>
                <td>string</td>
                <td>NULL</td>
                <td>オペレーション内容</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$customerId</td>
                <td>int</td>
                <td>NULL</td>
                <td>顧客 ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>4</td>
                <td>$productCode</td>
                <td>string</td>
                <td>NULL</td>
                <td>商品コード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>int</td>
                <td><br></td>
                <td>LAST_INSERT_ID</td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php


// 登録
include_once \'Mikoshiva/Log/Operation.php\';
$opeLog = new Mikoshiva_Log_Operation();
$lastPrimaryLey = $opeLog->staffLogRegister(Mikoshiva_Log_Operation::NAME_OPERATION_NAME_CHANGE_CARD_NO,              // カード番号変更
                                            Mikoshiva_Log_Operation::CONTENT_OPERATION_CONTENT_BANK_TRANSFER_TO_CARD, // 銀行からカード
                                            123,                                                                      // 顧客 ID
                                            \'DRM\'                                                                     // 商品コード
);



// 登録結果を表示
$ar = new Mikoshiva_Db_Ar_MstOperationLog();
dumper($ar->fetchAll($ar->select()->where(\'mol_operation_log_id = ?\', $lastPrimaryLey))->toArray());
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
<?php echo htmlspecialchars('
array(11) {
  ["mol_operation_log_id"] => string(2) "11"
  ["mol_login_id"] => string(1) "1"
  ["mol_operation_name"] => string(14) "OPERATION_NAME_CHANGE_CARD_NO"
  ["mol_customer_id"] => string(3) "123"
  ["mol_product_code"] => string(3) "DRM"
  ["mol_operation_content"] => string(21) "OPERATION_CONTENT_BANK_TRANSFER_TO_CARD"
  ["mol_delete_flag"] => string(1) "0"
  ["mol_deletion_datetime"] => NULL
  ["mol_update_datetime"] => string(19) "2010-03-15 14:49:55"
  ["mol_registration_datetime"] => string(19) "2010-03-15 14:49:55"
  ["mol_update_timestamp"] => string(19) "2010-03-15 14:49:55"
}
', ENT_NOQUOTES); ?>
</pre>




            <!-- ==============================================
            ■Mikoshiva_Log_Operation#selectOperationLogByCustomerId
            ============================================== -->
            <h2 id="Mikoshiva_Log_Operation#selectOperationLogByCustomerId"><a name="selectOperationLogByCustomerId">Mikoshiva_Log_Operation#selectOperationLogByCustomerId</a></h2>

            <p>
              顧客 ID でオペレーションログを検索します
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$customerId</td>
                <td>int</td>
                <td><br></td>
                <td>顧客 ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>array</td>
                <td>検索結果</td>
                <td>取得出来なかった場合は空の配列</td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Log/Operation.php\';


// 顧客 ID で取得
$opeLog = new Mikoshiva_Log_Operation();
dumper($opeLog->selectOperationLogByCustomerId(123)); // 結果を出力
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
<?php echo htmlspecialchars('
array(7) {
  [0] => array(11) {
    ["mol_operation_log_id"] => string(1) "6"
    ["mol_login_id"] => string(1) "1"
    ["mol_operation_name"] => string(14) "CHANGE_CARD_NO"
    ["mol_customer_id"] => string(3) "123"
    ["mol_product_code"] => string(3) "DRM"
    ["mol_operation_content"] => string(21) "BANK_TRANSFER_TO_CARD"
    ["mol_delete_flag"] => string(1) "0"
    ["mol_deletion_datetime"] => NULL
    ["mol_update_datetime"] => string(19) "2010-03-15 14:46:04"
    ["mol_registration_datetime"] => string(19) "2010-03-15 14:46:04"
    ["mol_update_timestamp"] => string(19) "2010-03-15 14:46:05"
  }
  [1] => array(11) {
    ["mol_operation_log_id"] => string(1) "7"
    ["mol_login_id"] => string(1) "1"
    ["mol_operation_name"] => string(14) "CHANGE_CARD_NO"
    ["mol_customer_id"] => string(3) "123"
    ["mol_product_code"] => string(3) "DRM"
    ["mol_operation_content"] => string(21) "BANK_TRANSFER_TO_CARD"
    ["mol_delete_flag"] => string(1) "0"
    ["mol_deletion_datetime"] => NULL
    ["mol_update_datetime"] => string(19) "2010-03-15 14:46:59"
    ["mol_registration_datetime"] => string(19) "2010-03-15 14:46:59"
    ["mol_update_timestamp"] => string(19) "2010-03-15 14:47:00"
  }
}
', ENT_NOQUOTES); ?>
</pre>








            <!-- ==============================================
            ■Mikoshiva_Log_Operation#selectOperationLogByProductCode
            ============================================== -->
            <h2 id="Mikoshiva_Log_Operation#selectOperationLogByProductCode"><a name="selectOperationLogByProductCode">Mikoshiva_Log_Operation#selectOperationLogByProductCode</a></h2>

            <p>
              商品コードを元にオペレーション内容を取得する
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$productCode</td>
                <td>string</td>
                <td><br></td>
                <td>商品コード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>array</td>
                <td>検索結果</td>
                <td>取得出来なかった場合は空の配列</td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Log/Operation.php\';


// 商品コードで取得
$opeLog = new Mikoshiva_Log_Operation();
dumper($opeLog->selectOperationLogByProductCode(\'DRM\')); // 結果を出力
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
<?php echo htmlspecialchars('
array(7) {
  [0] => array(11) {
    ["mol_operation_log_id"] => string(1) "6"
    ["mol_login_id"] => string(1) "1"
    ["mol_operation_name"] => string(14) "CHANGE_CARD_NO"
    ["mol_customer_id"] => string(3) "123"
    ["mol_product_code"] => string(3) "DRM"
    ["mol_operation_content"] => string(21) "BANK_TRANSFER_TO_CARD"
    ["mol_delete_flag"] => string(1) "0"
    ["mol_deletion_datetime"] => NULL
    ["mol_update_datetime"] => string(19) "2010-03-15 14:46:04"
    ["mol_registration_datetime"] => string(19) "2010-03-15 14:46:04"
    ["mol_update_timestamp"] => string(19) "2010-03-15 14:46:05"
  }
  [1] => array(11) {
    ["mol_operation_log_id"] => string(1) "7"
    ["mol_login_id"] => string(1) "1"
    ["mol_operation_name"] => string(14) "CHANGE_CARD_NO"
    ["mol_customer_id"] => string(3) "123"
    ["mol_product_code"] => string(3) "DRM"
    ["mol_operation_content"] => string(21) "BANK_TRANSFER_TO_CARD"
    ["mol_delete_flag"] => string(1) "0"
    ["mol_deletion_datetime"] => NULL
    ["mol_update_datetime"] => string(19) "2010-03-15 14:46:59"
    ["mol_registration_datetime"] => string(19) "2010-03-15 14:46:59"
    ["mol_update_timestamp"] => string(19) "2010-03-15 14:47:00"
  }
}
', ENT_NOQUOTES); ?>
</pre>




  </div>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






























