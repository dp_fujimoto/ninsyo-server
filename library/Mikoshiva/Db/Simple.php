<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Simple/Abstract.php';

/**
 *
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/04/07
 * @version SVN:$Id: Simple.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Simple extends Mikoshiva_Db_Simple_Abstract {

    /**
     * 強制条件
     *
     * 連想配列のキーに接頭辞を除いたカラム名
     * 連想配列の値に条件となる値
     *
     * array(delete_flag => 0)
     *
     * @var array
     */
    protected $_addWwhere = array();

}