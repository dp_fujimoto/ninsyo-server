<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/NewTab.php';

/**
 * 新しいタブを開くjavascriptをボタンタグに実装した形で返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: NewTabButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_NewTabButton extends Mikoshiva_View_Helper_Abstract {

    /**
     * 新しいタブを開くjavascriptをボタンタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: NewTabButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $name ボタンの vlue 値
     * @param string $url 遷移先 URL
     * @param boolean $storage true|保存flase|保存しない
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function newTabButton($name, $url, $storage = false, $formId = null) {
        $a = new Mikoshiva_View_Helper_NewTab();
        return '<input type="button" value="' . $name . '" onclick="' . $a->newTab($url, $storage, $formId) . '">';
    }
}
