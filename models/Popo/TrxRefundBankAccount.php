<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_refund_bank_account
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxRefundBankAccount extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'trb';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'refundBankAccountId' => null,
        'customerId' => null,
        'chargeId' => null,
        'directInstallmentPaymentId' => null,
        'shippingId' => null,
        'returnRefundId' => null,
        'bankName' => null,
        'branchName' => null,
        'depositType' => null,
        'accountNumber' => null,
        'accountHolder' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * trb_refund_bank_account_id の値を返します
	 *
     * @return int trb_refund_bank_account_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundBankAccountId() {
		return $this->_properties['refundBankAccountId'];
    }


    /**
     * trb_refund_bank_account_id の値をセットします
	 *
	 * @param int $refundBankAccountId trb_refund_bank_account_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundBankAccountId($refundBankAccountId) {
		$this->_properties['refundBankAccountId'] = $refundBankAccountId;
    }


    /**
     * trb_customer_id の値を返します
	 *
     * @return int trb_customer_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * trb_customer_id の値をセットします
	 *
	 * @param int $customerId trb_customer_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * trb_charge_id の値を返します
	 *
     * @return int trb_charge_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * trb_charge_id の値をセットします
	 *
	 * @param int $chargeId trb_charge_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * trb_direct_installment_payment_id の値を返します
	 *
     * @return int trb_direct_installment_payment_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPaymentId() {
		return $this->_properties['directInstallmentPaymentId'];
    }


    /**
     * trb_direct_installment_payment_id の値をセットします
	 *
	 * @param int $directInstallmentPaymentId trb_direct_installment_payment_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPaymentId($directInstallmentPaymentId) {
		$this->_properties['directInstallmentPaymentId'] = $directInstallmentPaymentId;
    }


    /**
     * trb_shipping_id の値を返します
	 *
     * @return int trb_shipping_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * trb_shipping_id の値をセットします
	 *
	 * @param int $shippingId trb_shipping_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * trb_return_refund_id の値を返します
	 *
     * @return int trb_return_refund_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnRefundId() {
		return $this->_properties['returnRefundId'];
    }


    /**
     * trb_return_refund_id の値をセットします
	 *
	 * @param int $returnRefundId trb_return_refund_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnRefundId($returnRefundId) {
		$this->_properties['returnRefundId'] = $returnRefundId;
    }


    /**
     * trb_bank_name の値を返します
	 *
     * @return varchar trb_bank_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBankName() {
		return $this->_properties['bankName'];
    }


    /**
     * trb_bank_name の値をセットします
	 *
	 * @param varchar $bankName trb_bank_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBankName($bankName) {
		$this->_properties['bankName'] = $bankName;
    }


    /**
     * trb_branch_name の値を返します
	 *
     * @return varchar trb_branch_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBranchName() {
		return $this->_properties['branchName'];
    }


    /**
     * trb_branch_name の値をセットします
	 *
	 * @param varchar $branchName trb_branch_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBranchName($branchName) {
		$this->_properties['branchName'] = $branchName;
    }


    /**
     * trb_deposit_type の値を返します
	 *
     * @return varchar trb_deposit_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDepositType() {
		return $this->_properties['depositType'];
    }


    /**
     * trb_deposit_type の値をセットします
	 *
	 * @param varchar $depositType trb_deposit_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDepositType($depositType) {
		$this->_properties['depositType'] = $depositType;
    }


    /**
     * trb_account_number の値を返します
	 *
     * @return char trb_account_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccountNumber() {
		return $this->_properties['accountNumber'];
    }


    /**
     * trb_account_number の値をセットします
	 *
	 * @param char $accountNumber trb_account_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccountNumber($accountNumber) {
		$this->_properties['accountNumber'] = $accountNumber;
    }


    /**
     * trb_account_holder の値を返します
	 *
     * @return varchar trb_account_holder
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccountHolder() {
		return $this->_properties['accountHolder'];
    }


    /**
     * trb_account_holder の値をセットします
	 *
	 * @param varchar $accountHolder trb_account_holder
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccountHolder($accountHolder) {
		$this->_properties['accountHolder'] = $accountHolder;
    }


    /**
     * trb_delete_flag の値を返します
	 *
     * @return char trb_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * trb_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag trb_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * trb_deletion_datetime の値を返します
	 *
     * @return datetime trb_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * trb_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime trb_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * trb_registration_datetime の値を返します
	 *
     * @return datetime trb_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * trb_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime trb_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * trb_update_datetime の値を返します
	 *
     * @return datetime trb_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * trb_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime trb_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * trb_update_timestamp の値を返します
	 *
     * @return timestamp trb_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * trb_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp trb_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























