<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{$smarty.const._SERVER_MODE_ID_|upper}</title>

<meta http-equiv="pragma"        content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires"       content="0">

<meta http-equiv="content-style-type"  content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">

<link rel="stylesheet" type="text/css" href="{$smarty.const.SUB_FOLDER_PATH}/css/common.css"          charset="utf-8">
<link rel="stylesheet" type="text/css" href="{$smarty.const.SUB_FOLDER_PATH}/css/product.css"          charset="utf-8">
<link rel="stylesheet" type="text/css" href="{$smarty.const.SUB_FOLDER_PATH}/css/tabNinsyo.css"             charset="utf-8">
<link rel="stylesheet" type="text/css" href="{$smarty.const.SUB_FOLDER_PATH}/css/menu.css"            charset="utf-8">
<link rel="stylesheet" type="text/css" href="{$smarty.const.SUB_FOLDER_PATH}/css/jquery.megamenu.css" charset="utf-8">

<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/jquery.js"             charset="utf-8"></script>
<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/jquery.blockUI.js"     charset="utf-8"></script>
<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/droppy.js"             charset="utf-8"></script>
<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/jquery.upload.tani.js" charset="utf-8"></script>
<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/jquery.megamenu.js"    charset="utf-8"></script>
<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/mikoshivaNinsyo.php"         charset="utf-8"></script>
<script type="text/javascript" src="{$smarty.const.SUB_FOLDER_PATH}/js/common_ninsyo.js"             charset="utf-8"></script>


<script type="text/javascript">
$(document).ready(function(){
    $(".MegaMenuLink").megamenu(
        ".MegaMenuContent",{
        //width: "850px"
    });

    $.post('{$smarty.const.SUB_FOLDER_PATH}/systemMessage.php', '', function(data, status){
        $('#systemMessage').html(data);
    });

    OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/information/search');
});





</script>


</head>
<body>

  <div id="header">
    <span style="font-size: 0.5em" id="systemMessage"></span>
    　　
    <span style="display: none" id="systemInfoLastOyaUrl"></span>
    　　
    {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
    <span onclick="KO_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/search-special-data/special-menu');">　</span>
    {/if}
    部署初期値：
    {*****************************************************************
    部門（権限）変更
    ******************************************************************}
    {php}
        $changeDivisionId = $_SESSION['staff']['changeDivisionId'];
        $smarty->assign('attribs', array('onchange' => 'COM_divisionChanger(' . $changeDivisionId . ', this.value);'));
    {/php}
    {divisionLister id="_divisionChanger" name="_divisionChanger" selected=$smarty.session.staff.changeDivisionId attribs=$attribs}
    　ログイン時の部署変更はsystemメニューのログイン情報変更から。
  </div>

    {include file='menu.tpl'}

<hr style="visibility: hidden">

  <div id="content">
  {if !isset($smarty.session.staff)}
        {$layout->content}
  {/if}
    <div id="con0"  style="display: block;"></div>

    <div id="tab" style="clear: both"></div>

  </div>

  <div style="visibility: hidden; font-size: 5em;">a</div>

    {if ($smarty.const.DEBUG_VIEW_HISTORY === 1 || $smarty.const.DEBUG_VIEW_REQUEST === 1 || $smarty.const.DEBUG_VIEW_HTML === 1)}
    <div id="debug"></div>
    {/if}


</body>
</html>