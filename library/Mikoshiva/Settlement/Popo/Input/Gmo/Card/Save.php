<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Card_Save extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'SiteID' => null,
        'SitePass' => null,
        'MemberID' => null,
        'SeqMode' => null,
        'CardSeq' => null,
        'DefaultFlag' => null,
        'CardName' => null,
        'CardNo' => null,
        'CardPass' => null,
        'Expire' => null,
        'HolderName' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * SiteID
	 *
     * @return  SiteID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSiteID() {
		return $this->_properties['SiteID'];
    }


    /**
     * SiteID
	 *
	 * @param  $SiteID SiteID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSiteID($SiteID) {
		$this->_properties['SiteID'] = $SiteID;
    }


    /**
     * SitePass
	 *
     * @return  SitePass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSitePass() {
		return $this->_properties['SitePass'];
    }


    /**
     * SitePass
	 *
	 * @param  $SitePass SitePass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSitePass($SitePass) {
		$this->_properties['SitePass'] = $SitePass;
    }


    /**
     * MemberID
	 *
     * @return  MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMemberID() {
		return $this->_properties['MemberID'];
    }


    /**
     * MemberID
	 *
	 * @param  $MemberID MemberID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMemberID($MemberID) {
		$this->_properties['MemberID'] = $MemberID;
    }


    /**
     * SeqMode
	 *
     * @return  SeqMode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSeqMode() {
		return $this->_properties['SeqMode'];
    }


    /**
     * SeqMode
	 *
	 * @param  $SeqMode SeqMode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSeqMode($SeqMode) {
		$this->_properties['SeqMode'] = $SeqMode;
    }


    /**
     * CardSeq
	 *
     * @return  CardSeq
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardSeq() {
		return $this->_properties['CardSeq'];
    }


    /**
     * CardSeq
	 *
	 * @param  $CardSeq CardSeq
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardSeq($CardSeq) {
		$this->_properties['CardSeq'] = $CardSeq;
    }


    /**
     * DefaultFlag
	 *
     * @return  DefaultFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getDefaultFlag() {
		return $this->_properties['DefaultFlag'];
    }


    /**
     * DefaultFlag
	 *
	 * @param  $DefaultFlag DefaultFlag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setDefaultFlag($DefaultFlag) {
		$this->_properties['DefaultFlag'] = $DefaultFlag;
    }


    /**
     * CardName
	 *
     * @return  CardName
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardName() {
		return $this->_properties['CardName'];
    }


    /**
     * CardName
	 *
	 * @param  $CardName CardName
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardName($CardName) {
		$this->_properties['CardName'] = $CardName;
    }


    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * CardPass
	 *
     * @return  CardPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardPass() {
		return $this->_properties['CardPass'];
    }


    /**
     * CardPass
	 *
	 * @param  $CardPass CardPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardPass($CardPass) {
		$this->_properties['CardPass'] = $CardPass;
    }


    /**
     * Expire
	 *
     * @return  Expire
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getExpire() {
		return $this->_properties['Expire'];
    }


    /**
     * Expire
	 *
	 * @param  $Expire Expire
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setExpire($Expire) {
		$this->_properties['Expire'] = $Expire;
    }


    /**
     * HolderName
	 *
     * @return  HolderName
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getHolderName() {
		return $this->_properties['HolderName'];
    }


    /**
     * HolderName
	 *
	 * @param  $HolderName HolderName
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setHolderName($HolderName) {
		$this->_properties['HolderName'] = $HolderName;
    }


}

























