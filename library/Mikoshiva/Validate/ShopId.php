<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * ショップ ID の検証を行います
 * 
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: ShopId.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_ShopId extends Zend_Validate_Abstract
{
    const STRING_EMPTY = 'stringEmpty';
    const INVALID      = 'noKana';
    const noExist      = 'noExist';

    /**
     * Digits filter used for validation
     *
     * @var Zend_Filter_Digits
     */
    protected static $_filter = null;

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::STRING_EMPTY => "%COLUMN_NAME% は必須項目です。",
        self::INVALID      => "%COLUMN_NAME% の形式が正しくありません。",
        self::noExist      => "入力された %COLUMN_NAME% は存在しません",
    );



    /**
     * ショップ ID の検証を行います
     *
     * @param unknown_type $value
     * @return string|string|string
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Interface#isValid($value)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/09
     * @version SVN:$Id: ShopId.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function isValid($value)
    {
        // 形式チェック
        if (preg_match('/^\d{13}$/', $value) !== 1) {
            $this->_error(self::INVALID);
            return false;
        }
        
        // ショップ ID の存在チェック
        $shopIdList = Zend_Registry::get('GMO_CONFIG');
        if (!array_key_exists($value, $shopIdList['REAL']['SHOP']) &&
            !array_key_exists($value, $shopIdList['TEST']['SHOP'])) {
            $this->_error(self::noExist);
            return false;
        }
        return true;
    }

}
