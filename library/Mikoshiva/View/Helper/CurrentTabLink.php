<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/CurrentTab.php';
require_once 'Mikoshiva/Logger.php';

/**
 *
 *
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: CurrentTabLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_CurrentTabLink extends Mikoshiva_View_Helper_Abstract {

    /**
     * 画面遷移を現在開いているタブに対して行うjavascriptをリンクタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: CurrentTabLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $name
     * @param string $url
     * @param string $tabId
     * @return string
     */
    public function currentTabLink( $name, $url, $tabId = null) {
        logInfo(LOG_START);
        $a = new Mikoshiva_View_Helper_CurrentTab();
        logInfo(LOG_END);
        return '<a href="#" onclick="' . $a->currentTab($url, $tabId) . '">' . $name . '</a>';
    }
}
