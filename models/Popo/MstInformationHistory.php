<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_information_history
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstInformationHistory extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mih';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'informationHistoryId' => null,
        'staffId' => null,
        'subject' => null,
        'body' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mih_information_history_id の値を返します
	 *
     * @return int mih_information_history_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInformationHistoryId() {
		return $this->_properties['informationHistoryId'];
    }


    /**
     * mih_information_history_id の値をセットします
	 *
	 * @param int $informationHistoryId mih_information_history_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInformationHistoryId($informationHistoryId) {
		$this->_properties['informationHistoryId'] = $informationHistoryId;
    }


    /**
     * mih_staff_id の値を返します
	 *
     * @return int mih_staff_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * mih_staff_id の値をセットします
	 *
	 * @param int $staffId mih_staff_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * mih_subject の値を返します
	 *
     * @return varchar mih_subject
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubject() {
		return $this->_properties['subject'];
    }


    /**
     * mih_subject の値をセットします
	 *
	 * @param varchar $subject mih_subject
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubject($subject) {
		$this->_properties['subject'] = $subject;
    }


    /**
     * mih_body の値を返します
	 *
     * @return text mih_body
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBody() {
		return $this->_properties['body'];
    }


    /**
     * mih_body の値をセットします
	 *
	 * @param text $body mih_body
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBody($body) {
		$this->_properties['body'] = $body;
    }


    /**
     * mih_delete_flag の値を返します
	 *
     * @return char mih_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mih_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mih_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mih_deletion_datetime の値を返します
	 *
     * @return datetime mih_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mih_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mih_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mih_registration_datetime の値を返します
	 *
     * @return datetime mih_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mih_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mih_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mih_update_datetime の値を返します
	 *
     * @return datetime mih_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mih_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mih_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mih_update_timestamp の値を返します
	 *
     * @return timestamp mih_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mih_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mih_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























