<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CheckSessionId.php';
require_once 'api/models/classes/user-content/InsertUserContentDetail.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_InsertUserContent extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        // 初期化
        //-----------------------------------------------
        $output = array();
        $nowDatetime = Mikoshiva_date::mikoshivaNow();

        $class = new Api_Models_Classes_CheckSessionId();
        $class->execute($actionForm->getSessionId());

        if (!is_array($actionForm->getList())) {
            $list = array();
        } else {
            $list = $actionForm->getList();
        }
        
        $deleteFlag = $actionForm->getDeleteFlag();

        $config = $context->getConfig();

        $class = new Api_Models_Classes_UserContent_InsertUserContentDetail();
        $output = $class->execute($list, $deleteFlag, $config['NINSYO_SERVER_NAME_LIST']);
        
        // logTemporary(count($list) . ':' . dumper($list,null,false) . ':' . dumper($output,null,false),'insertUserContent');

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }
}
