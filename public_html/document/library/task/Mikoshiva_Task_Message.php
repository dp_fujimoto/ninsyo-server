
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; Task 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#sendAlert">Mikoshiva_Task_Message#sendAlert - オペレーターへアラートを発行します。</a></li>
              <li><a href="#sendTask">Mikoshiva_Task_Message#sendTask - オペレーターへタスクを発行します。</a></li>
              <li><a href="#sendAlertTemplate">Mikoshiva_Task_Message#sendAlertTemplate - メールテンプレートの情報を使用してオペレーターへアラートを発行します。</a></li>
              <li><a href="#sendTaskTemplate">Mikoshiva_Task_Message#sendTaskTemplate - メールテンプレートの情報を使用してオペレーターへタスクを発行します。</a></li>
            </ol>




            <!-- ==============================================
            ■Mikoshiva_Task_Message#sendAlert
            ============================================== -->
            <h2 id="Mikoshiva_Task_Message#sendAlert"><a name="sendAlert">Mikoshiva_Task_Message#sendAlert</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
			  オペレーターへアラートを発行します。<br>
              発行したアラートは「問い合わせ一覧」に「アラート」として表示されます。
            </p>
			<p>
              メソッドを実行時「trx_inquiry_detail」テーブルにデータを作成されます。<br>
			  Mikoshiva_Task_Messageクラスでは、トランザクション制御を行っていないため、<br>
              呼びだし元にて管理する必要があります。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$subject</td>
                <td>string</td>
                <td><br></td>
                <td>メール件名</td>
                <td>
				  問い合わせのメール件名となる値です。<br>
				  改行やタブが含まれていた場合は自動的に除去されます。<br>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>$body</td>
                <td>string</td>
                <td><br></td>
                <td>メール本文</td>
                <td>
				  問い合わせのメール本文となる値です。<br>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>$divisionValues</td>
                <td>array|int</td>
                <td>array()</td>
                <td>アラートを表示する部署ID</td>
                <td>
				  アラートを表示する部署IDを配列に代入して指定します。<br>
                  1データの場合、配列ではなく単一の値として引数に指定することも可能です。<br>
				  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$staffId</td>
                <td>int</td>
                <td>null</td>
                <td>アラートを表示するスタッフID</td>
                <td>
				  アラートを表示するスタッフIDを指定します。<br>
                  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>boolean</td>
                <td><br></td>
				<td>true</td>
                <td>登録失敗時は例外がスローされます。</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Task/Message.php\';

$subject          = \'サブジェクト\';
$body             = \'ボディ\';
$divisionValues   = array();
$divisionValues[] = 1;
$divisionValues[] = 2;
$staffId          = null;

$bool = false;
$tsk = null;
$tsk = new Mikoshiva_Task_Message();
$bool = $tsk->sendAlert($subject, $body, $divisionValues, $staffId);

dumper($bool);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
bool(true)
</pre>




            <!-- ==============================================
            ■Mikoshiva_Task_Message#sendTask
            ============================================== -->
            <h2 id="Mikoshiva_Task_Message#sendTask"><a name="sendTask">Mikoshiva_Task_Message#sendTask</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              オペレーターへタスクを発行します。<br>
              発行したタスクは「問い合わせ一覧」に「タスク」として表示されます。<br>
            </p>
			<p>
              メソッドを実行時「trx_inquiry_detail」テーブルにデータが作成されます。<br>
			  Mikoshiva_Task_Messageクラスでは、トランザクション制御を行っていないため、<br>
              呼びだし元にて管理する必要があります。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$subject</td>
                <td>string</td>
                <td><br></td>
                <td>メール件名</td>
                <td>
				  問い合わせのメール件名となる値です。<br>
				  改行やタブが含まれていた場合は自動的に除去されます。
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>$body</td>
                <td>string</td>
                <td><br></td>
                <td>メール本文</td>
                <td>
				  問い合わせのメール本文となる値です。<br>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>$divisionValues</td>
                <td>array|int</td>
                <td>array()</td>
                <td>アラートを表示する部署ID</td>
                <td>
                  1データの場合、配列ではなく単一の値として引数に指定することも可能です。<br>
				  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$staffId</td>
                <td>int</td>
                <td>null</td>
                <td>アラートを表示するスタッフID</td>
                <td>
				  アラートを表示するスタッフIDを指定します。<br>
                  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>boolean</td>
                <td><br></td>
				<td>true</td>
                <td>登録失敗時は例外がスローされます。</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Task/Message.php\';

$subject          = \'サブジェクト\';
$body             = \'ボディ\';
$divisionValues   = array();
$divisionValues[] = 1;
$divisionValues[] = 2;
$staffId          = null;

$bool = false;
$tsk = null;
$tsk = new Mikoshiva_Task_Message();
$bool = $tsk->sendTask($subject, $body, $divisionValues, $staffId);

dumper($bool);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
bool(true)
</pre>








            <!-- ==============================================
            ■Mikoshiva_Task_Message#sendAlertTemplate
            ============================================== -->
            <h2 id="Mikoshiva_Task_Message#sendAlertTemplate"><a name="sendAlertTemplate">Mikoshiva_Task_Message#sendAlertTemplate</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              メールテンプレートの情報を使用してオペレーターへアラートを発行します。<br>
              発行したアラートは「問い合わせ一覧」に「アラート」として表示されます。
            </p>
            <p>
              メソッドを実行時「trx_inquiry_detail」テーブルにデータを作成されます。<br>
              Mikoshiva_Task_Messageクラスでは、トランザクション制御を行っていないため、<br>
              呼びだし元にて管理する必要があります。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$templateKey</td>
                <td>string</td>
                <td><br></td>
                <td>メールテンプレートキー</td>
                <td>
                  mst_mail_template テーブルの mmt_template_key の値です。
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>$replaceList</td>
                <td>array</td>
                <td>array()</td>
                <td>メール本文</td>
                <td>
                  置換リスト
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>$divisionValues</td>
                <td>array|int</td>
                <td>array()</td>
                <td>アラートを表示する部署ID</td>
                <td>
                  アラートを表示する部署IDを配列に代入して指定します。<br>
                  1データの場合、配列ではなく単一の値として引数に指定することも可能です。<br>
                  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$staffId</td>
                <td>int</td>
                <td>null</td>
                <td>アラートを表示するスタッフID</td>
                <td>
                  アラートを表示するスタッフIDを指定します。<br>
                  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>boolean</td>
                <td><br></td>
                <td>true</td>
                <td>登録失敗時は例外がスローされます。</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Task/Message.php\';
include_once \'Mikoshiva/Db/Simple.php\';

// templateKey
$templateKey = \'PASSWORD_REMINDER\';

// replaceList
$simple      = new Mikoshiva_Db_Simple();
$popo        = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);
$replaceList = array(\'HOGE_HOGE\' => \'☆★☆\', $popo);

// divisionValues
$divisionValues   = array();
$divisionValues[] = 1;
$divisionValues[] = 2;

// staffId
$staffId = null;

$tsk = null;
$tsk = new Mikoshiva_Task_Message();
$bool = false;
$bool = $tsk->sendAlertTemplate($templateKey, $replaceList, $divisionValues, $staffId);

dumper($bool);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
bool(true)
</pre>












            <!-- ==============================================
            ■Mikoshiva_Task_Message#sendTaskTemplate
            ============================================== -->
            <h2 id="Mikoshiva_Task_Message#sendTaskTemplate"><a name="sendTaskTemplate">Mikoshiva_Task_Message#sendTaskTemplate</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              メールテンプレートの情報を使用してオペレーターへタスクを発行します。<br>
              発行したタスクは「問い合わせ一覧」に「タスク」として表示されます。<br>
            </p>
            <p>
              メソッドを実行時「trx_inquiry_detail」テーブルにデータが作成されます。<br>
              Mikoshiva_Task_Messageクラスでは、トランザクション制御を行っていないため、<br>
              呼びだし元にて管理する必要があります。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$templateKey</td>
                <td>string</td>
                <td><br></td>
                <td>メールテンプレートキー</td>
                <td>
                  mst_mail_template テーブルの mmt_template_key の値です。
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>$replaceList</td>
                <td>array</td>
                <td>array()</td>
                <td>メール本文</td>
                <td>
                  置換リスト
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>$divisionValues</td>
                <td>array|int</td>
                <td>array()</td>
                <td>アラートを表示する部署ID</td>
                <td>
                  1データの場合、配列ではなく単一の値として引数に指定することも可能です。<br>
                  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$staffId</td>
                <td>int</td>
                <td>null</td>
                <td>アラートを表示するスタッフID</td>
                <td>
                  アラートを表示するスタッフIDを指定します。<br>
                  (※省略可能)
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>boolean</td>
                <td><br></td>
                <td>true</td>
                <td>登録失敗時は例外がスローされます。</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Task/Message.php\';
include_once \'Mikoshiva/Db/Simple.php\';

// templateKey
$templateKey = \'PASSWORD_REMINDER\';

// replaceList
$simple      = new Mikoshiva_Db_Simple();
$popo        = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);
$replaceList = array(\'HOGE_HOGE\' => \'☆★☆\', $popo);

// divisionValues
$divisionValues   = array();
$divisionValues[] = 1;
$divisionValues[] = 2;

// staffId
$staffId = null;

$tsk = null;
$tsk = new Mikoshiva_Task_Message();
$bool = false;
$bool = $tsk->sendTaskTemplate($templateKey, $replaceList, $divisionValues, $staffId);

dumper($bool);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
bool(true)
</pre>




































  </div>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





