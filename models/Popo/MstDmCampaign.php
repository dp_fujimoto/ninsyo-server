<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_dm_campaign
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstDmCampaign extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mdc';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'dmCampaignId' => null,
        'divisionId' => null,
        'dmCampaignName' => null,
        'listType' => null,
        'listSplitType' => null,
        'totalListNumber' => null,
        'maxListNumber' => null,
        'campaignStartDate' => null,
        'evaluation' => null,
        'memo' => null,
        'masterDirectCampaignId' => null,
        'batchLastExecuteDate' => null,
        'staffId' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * ダイレクトメールキャンペーンID の値を返します
	 *
     * @return int ダイレクトメールキャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignId() {
		return $this->_properties['dmCampaignId'];
    }


    /**
     * ダイレクトメールキャンペーンID の値をセットします
	 *
	 * @param int $dmCampaignId ダイレクトメールキャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignId($dmCampaignId) {
		$this->_properties['dmCampaignId'] = $dmCampaignId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * ダイレクトメールキャンペーン名 の値を返します
	 *
     * @return varchar ダイレクトメールキャンペーン名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignName() {
		return $this->_properties['dmCampaignName'];
    }


    /**
     * ダイレクトメールキャンペーン名 の値をセットします
	 *
	 * @param varchar $dmCampaignName ダイレクトメールキャンペーン名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignName($dmCampaignName) {
		$this->_properties['dmCampaignName'] = $dmCampaignName;
    }


    /**
     * リストタイプ の値を返します
	 *
     * @return varchar リストタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getListType() {
		return $this->_properties['listType'];
    }


    /**
     * リストタイプ の値をセットします
	 *
	 * @param varchar $listType リストタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setListType($listType) {
		$this->_properties['listType'] = $listType;
    }


    /**
     * リスト分割タイプ の値を返します
	 *
     * @return varchar リスト分割タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getListSplitType() {
		return $this->_properties['listSplitType'];
    }


    /**
     * リスト分割タイプ の値をセットします
	 *
	 * @param varchar $listSplitType リスト分割タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setListSplitType($listSplitType) {
		$this->_properties['listSplitType'] = $listSplitType;
    }


    /**
     * 全対象リスト数 の値を返します
	 *
     * @return int 全対象リスト数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTotalListNumber() {
		return $this->_properties['totalListNumber'];
    }


    /**
     * 全対象リスト数 の値をセットします
	 *
	 * @param int $totalListNumber 全対象リスト数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTotalListNumber($totalListNumber) {
		$this->_properties['totalListNumber'] = $totalListNumber;
    }


    /**
     * 最大対象リスト数 の値を返します
	 *
     * @return int 最大対象リスト数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMaxListNumber() {
		return $this->_properties['maxListNumber'];
    }


    /**
     * 最大対象リスト数 の値をセットします
	 *
	 * @param int $maxListNumber 最大対象リスト数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMaxListNumber($maxListNumber) {
		$this->_properties['maxListNumber'] = $maxListNumber;
    }


    /**
     * キャンペーン開始日 の値を返します
	 *
     * @return date キャンペーン開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignStartDate() {
		return $this->_properties['campaignStartDate'];
    }


    /**
     * キャンペーン開始日 の値をセットします
	 *
	 * @param date $campaignStartDate キャンペーン開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignStartDate($campaignStartDate) {
		$this->_properties['campaignStartDate'] = $campaignStartDate;
    }


    /**
     * 評価 の値を返します
	 *
     * @return int 評価
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEvaluation() {
		return $this->_properties['evaluation'];
    }


    /**
     * 評価 の値をセットします
	 *
	 * @param int $evaluation 評価
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEvaluation($evaluation) {
		$this->_properties['evaluation'] = $evaluation;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * コピー元ダイレクトキャンペーンID の値を返します
	 *
     * @return int コピー元ダイレクトキャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMasterDirectCampaignId() {
		return $this->_properties['masterDirectCampaignId'];
    }


    /**
     * コピー元ダイレクトキャンペーンID の値をセットします
	 *
	 * @param int $masterDirectCampaignId コピー元ダイレクトキャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMasterDirectCampaignId($masterDirectCampaignId) {
		$this->_properties['masterDirectCampaignId'] = $masterDirectCampaignId;
    }


    /**
     * mdc_batch_last_execute_date の値を返します
	 *
     * @return date mdc_batch_last_execute_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBatchLastExecuteDate() {
		return $this->_properties['batchLastExecuteDate'];
    }


    /**
     * mdc_batch_last_execute_date の値をセットします
	 *
	 * @param date $batchLastExecuteDate mdc_batch_last_execute_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBatchLastExecuteDate($batchLastExecuteDate) {
		$this->_properties['batchLastExecuteDate'] = $batchLastExecuteDate;
    }


    /**
     * mdc_staff_id の値を返します
	 *
     * @return int mdc_staff_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * mdc_staff_id の値をセットします
	 *
	 * @param int $staffId mdc_staff_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























