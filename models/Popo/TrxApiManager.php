<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_api_manager
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxApiManager extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tam';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'apiManagerId' => null,
        'directionId' => null,
        'directionStatusId' => null,
        'divisionId' => null,
        'customerId' => null,
        'apiEventStatus' => null,
        'cookies' => null,
        'parameters' => null,
        'executionScheduleDatetime' => null,
        'readyFlag' => null,
        'readyDatetime' => null,
        'completeFlag' => null,
        'completeDatetime' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * APIマネージャーID の値を返します
	 *
     * @return int APIマネージャーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApiManagerId() {
		return $this->_properties['apiManagerId'];
    }


    /**
     * APIマネージャーID の値をセットします
	 *
	 * @param int $apiManagerId APIマネージャーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApiManagerId($apiManagerId) {
		$this->_properties['apiManagerId'] = $apiManagerId;
    }


    /**
     * 指示ID の値を返します
	 *
     * @return int 指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * 指示ID の値をセットします
	 *
	 * @param int $directionId 指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * APIイベントステータス の値を返します
	 *
     * @return varchar APIイベントステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApiEventStatus() {
		return $this->_properties['apiEventStatus'];
    }


    /**
     * APIイベントステータス の値をセットします
	 *
	 * @param varchar $apiEventStatus APIイベントステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApiEventStatus($apiEventStatus) {
		$this->_properties['apiEventStatus'] = $apiEventStatus;
    }


    /**
     * クッキー の値を返します
	 *
     * @return mediumtext クッキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCookies() {
		return $this->_properties['cookies'];
    }


    /**
     * クッキー の値をセットします
	 *
	 * @param mediumtext $cookies クッキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCookies($cookies) {
		$this->_properties['cookies'] = $cookies;
    }


    /**
     * パラメーター の値を返します
	 *
     * @return mediumtext パラメーター
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getParameters() {
		return $this->_properties['parameters'];
    }


    /**
     * パラメーター の値をセットします
	 *
	 * @param mediumtext $parameters パラメーター
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setParameters($parameters) {
		$this->_properties['parameters'] = $parameters;
    }


    /**
     * 実行予定日時 の値を返します
	 *
     * @return datetime 実行予定日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionScheduleDatetime() {
		return $this->_properties['executionScheduleDatetime'];
    }


    /**
     * 実行予定日時 の値をセットします
	 *
	 * @param datetime $executionScheduleDatetime 実行予定日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionScheduleDatetime($executionScheduleDatetime) {
		$this->_properties['executionScheduleDatetime'] = $executionScheduleDatetime;
    }


    /**
     * 準備完了フラグ の値を返します
	 *
     * @return char 準備完了フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReadyFlag() {
		return $this->_properties['readyFlag'];
    }


    /**
     * 準備完了フラグ の値をセットします
	 *
	 * @param char $readyFlag 準備完了フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReadyFlag($readyFlag) {
		$this->_properties['readyFlag'] = $readyFlag;
    }


    /**
     * 準備完了日時 の値を返します
	 *
     * @return datetime 準備完了日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReadyDatetime() {
		return $this->_properties['readyDatetime'];
    }


    /**
     * 準備完了日時 の値をセットします
	 *
	 * @param datetime $readyDatetime 準備完了日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReadyDatetime($readyDatetime) {
		$this->_properties['readyDatetime'] = $readyDatetime;
    }


    /**
     * 完了フラグ の値を返します
	 *
     * @return char 完了フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompleteFlag() {
		return $this->_properties['completeFlag'];
    }


    /**
     * 完了フラグ の値をセットします
	 *
	 * @param char $completeFlag 完了フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompleteFlag($completeFlag) {
		$this->_properties['completeFlag'] = $completeFlag;
    }


    /**
     * 完了日時 の値を返します
	 *
     * @return datetime 完了日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompleteDatetime() {
		return $this->_properties['completeDatetime'];
    }


    /**
     * 完了日時 の値をセットします
	 *
	 * @param datetime $completeDatetime 完了日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompleteDatetime($completeDatetime) {
		$this->_properties['completeDatetime'] = $completeDatetime;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























