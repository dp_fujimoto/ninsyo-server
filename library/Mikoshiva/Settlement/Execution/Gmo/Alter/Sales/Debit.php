<?php


require_once 'Mikoshiva/Utility/Copy.php';
require_once 'Mikoshiva/Utility/Gmo.php';
require_once 'Mikoshiva/Date.php';
require_once 'Mikoshiva/Db/Simple.php';

require_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Alter/Debit.php';
require_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Entry.php';
require_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Exec/Run.php';

require_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Capture.php';
require_once 'Mikoshiva/Settlement/Execution/Gmo/Exec/Run.php';

require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Alter/Debit.php';




/**
 * デビットカードで仮売上商品を購入された際の次決済を行う実行処理クラス
 *
 * 概要：
 *       デビットカードで仮売上商品を購入された場合、その際に与信枠を押さえません。
 *       このため、本来実売上にする期日にこのクラスで実売上処理を行います（実際は即時売上を行っています）
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/05/24
 * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Alter_Sales_Debit {


    /**
     * 入力 POPO
     * @var Mikoshiva_Settlement_Popo_Input_Gmo_Alter_Debit 入力 POPO
     */
    protected $_inputPopo;


    /**
     * 出力 POPO
     * @var Mikoshiva_Settlement_Popo_Output_Gmo_Alter_Debit 入力 POPO
     */
    protected $_outputPopo;


    /**
     * 処理区分
     * @var string 処理区分
     */
    protected $_JobCd = 'CAPTURE';


    /**
     * 支払方法
     * @var string 支払方法
     */
    protected $_entryStatus = 'ENTRY_STATUS_CAPTURE'; // 一括払い



    /**
     * 支払方法
     * @var string 支払方法
     */
    protected $_method = '1'; // 一括払い


    /**
     * コンストラクタ
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/05/24
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(Mikoshiva_Settlement_Popo_Input_Gmo_Alter_Debit $_inputPopo) {

        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");

        $this->_inputPopo  = $_inputPopo;
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Alter_Debit();

        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
    }


    /**
     * 実行メソッド
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/05/24
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function execute() {

        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");


        // ▼オーダー ID
        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Alter_Debit */
        $inputPopo = null;
        $inputPopo = $this->_inputPopo;

        // ▼ショップ情報を取得
        $shopInfo = array();
        $shopInfo = Mikoshiva_Utility_Gmo::getShopInfo($inputPopo->getShopID());
//dumper($shopInfo);
//exit;

        // ▼オーダー ID から会員 ID を取得
        $memberId = '';
        $memberId = Mikoshiva_Utility_Gmo::getFromOrderIdToMemberId($inputPopo->getOrderID());
//dumper($inputPopo->getOrderID());
//dumper($memberId);
//exit;

        //-------------------------------------------
        //
        // ■取引登録
        //
        //-------------------------------------------
        // ▼入力 POPO を登録
        $entryInputPopo = null;
        $entryInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Entry();
        $entryInputPopo->setShopID($inputPopo->getShopID());   // ショップ ID
        $entryInputPopo->setShopPass($shopInfo['SHOP_PASS']);  // ショップパスワード
        $entryInputPopo->setOrderID($inputPopo->getOrderID()); // オーダー ID
        $entryInputPopo->setJobCd($this->_JobCd);              // 処理区分
        $entryInputPopo->setAmount($inputPopo->getAmount());   // 処理区分

        // ▼実行
        $entryExec = null;
        $entryExec = new Mikoshiva_Settlement_Execution_Gmo_Entry_Capture($entryInputPopo);
        $ret       = null;
        $ret       = $entryExec->execute();

        // ▼処理結果を取得
        /* @var $entryOutputPopo Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register */
        $entryOutputPopo = null;
        $entryOutputPopo = $entryExec->getOutput();
        Mikoshiva_Utility_Copy::copyToPopo($this->_outputPopo, $entryOutputPopo);
//dumper($ret);
//dumper($entryOutputPopo);
//exit;
        // ▼処理に失敗していたらリターン
        if (! $ret) {
            return false;
        }


        //--------------------------------------------------------------------
        //
        // ■決済実行
        //
        //--------------------------------------------------------------------
        // ▼inputPopo から会員登録 POPO にコピー
        $execInputPopo = null;
        $execInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Run();
        $execInputPopo->setAccessID($entryOutputPopo->getAccessID());        // 取引 ID
        $execInputPopo->setAccessPass($entryOutputPopo->getAccessPass());    // 取引パスワード
        $execInputPopo->setOrderID($inputPopo->getOrderID());                // オーダー ID
        $execInputPopo->setMethod($this->_method);                           // 支払方法
////////////$execInputPopo->setPayTimes();                                       // 支払回数
        $execInputPopo->setSiteID($shopInfo['SITE_ID']);                     // サイト ID
        $execInputPopo->setSitePass($shopInfo['SITE_PASS']);                 // サイトパスワード
        $execInputPopo->setMemberID($memberId);                              // 会員 ID
        $execInputPopo->setCardSeq('1');                                     // カード登録連番

        // ▼決済実行クラス
        include_once 'Mikoshiva/Settlement/Execution/Gmo/Exec/Run.php';
        $execExec = null;
        $execExec = new Mikoshiva_Settlement_Execution_Gmo_Exec_Run($execInputPopo);
        $ret      = $execExec->execute();

        // ▼結果を取得
        $execOutputPopo = null;
        $execOutputPopo = $execExec->getOutput();
//dumper($ret);
//dumper($execOutputPopo);
//exit;
        // ▼エラー処理
        if (! $ret)  {
            Mikoshiva_Utility_Copy::copyToPopo($this->_outputPopo, $execOutputPopo);
            return false;
        }


        //--------------------------------------------------------------------
        //
        // ■GMO 取引履歴テーブル登録処理
        //
        //--------------------------------------------------------------------
        // ▼登録データを作成
        $data = array();
        $data = array(
//'mgh_gmo_transaction_history_id' => null,
            'orderId'                 => $inputPopo->getOrderID(),
            'memberId'                => $memberId,
            'shopId'                  => $inputPopo->getShopID(),
            'accessId'                => $entryOutputPopo->getAccessID(),
            'accessPass'              => $entryOutputPopo->getAccessPass(),
            //'cardNo'                  => (isBlankOrNull($orderInputPopo->getCardNo()) ? null : 'XXXXXXXXXXXX' . substr($orderInputPopo->getCardNo(), -4)),
            'method'                  => $this->_method,
//            'payTimes'                => $orderInputPopo->getPayTimes(),
            'entryStatus'             => $this->_entryStatus,
            'amount'                  => $inputPopo->getAmount(),
//    mgh_tran_date datetime,
//    mgh_delete_flag char(1) not null default 0,
//    mgh_deletion_datetime datetime,
            'registrationDatetime'    => Mikoshiva_Date::mikoshivaNow(),
            'updateDatetime'          => Mikoshiva_Date::mikoshivaNow(),
            //'updateTimestamp'         => null,
        );

        // ▼登録
        $simple = null;
        $simple = new Mikoshiva_Db_Simple();

        $ret = null;
        $ret = $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);



        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
        return true;
    }


    /**
     * 処理結果を返します
     *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Alter_Debit
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/05/31
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_outputPopo;
    }



}









































