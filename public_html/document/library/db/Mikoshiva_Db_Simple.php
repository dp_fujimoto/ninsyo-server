<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#simpleOneSelect">Mikoshiva_Db_Simple#simpleOneSelect - 第二引数に与えられた条件を元に DB から値を 1 件取得します</a></li>
              <li><a href="#simpleOneSelectById">Mikoshiva_Db_Simple#simpleOneSelectById - 指定された ID にヒットしたデータを返します</a></li>
              <li><a href="#simpleOneSelectByIdFromForm">Mikoshiva_Db_Simple#simpleOneSelectByIdFromForm - 第二引数に与えられた配列・オブジェクトからプライマリーキーを見つけ出し、ヒットしたデータを POPO に詰めて返します</a></li>
              <li><a href="#simpleManySelect">Mikoshiva_Db_Simple#simpleManySelect - 指定された条件にマッチしたデータを返します</a></li>
              <li><a href="#simpleOneCreateOrUpdateById">Mikoshiva_Db_Simple#simpleOneCreateOrUpdateById - 第二引数の値を条件に登録・更新処理を行います。</a></li>
            </ol>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple#simpleOneSelect
            ============================================== -->
            <h2 id="Mikoshiva_Db_Simple＃simpleOneSelect"><a name="simpleOneSelect">Mikoshiva_Db_Simple#simpleOneSelect</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              第二引数に与えられた条件を元に DB から値を 1 件取得します
            </p>
            <p>
              1件以上取得出来た場合、Mikoshiva_Db_Simple_Exception 投げます<br>
              取得出来なかった場合空の POPO オブジェクトを返します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$arName</td>
                <td>string</td>
                <td><br></td>
                <td>使用する Mikoshiva_Db_Ar クラス名</td>
                <td>ここで指定されたクラス名に紐付くテーブルに対して SELECT を行います</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$condition</td>
                <td>
                  array<br>
                  Popo_Abstract<br>
                  Mikoshiva_Controller_Mapping_Form_Interface<br>
                </td>
                <td><br></td>
                <td>条件に使用する配列及びクラス</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>Popo_Abstract</td>
                <td><br></td>
                <td>テーブルに紐付く POPO クラスのインスタンスに値を詰めて返します</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';

// 値をセット
$mm                 =  array();
$mm[\'customerId\'] = 2;

// 実行
$simple = new Mikoshiva_Db_Simple();
dumper($simple->simpleOneSelect(\'Mikoshiva_Db_Ar_MstCustomer\', $mm));
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
object(Popo_MstCustomer)#65 (1) {
  ["_properties:protected"] => array(25) {
    ["customerId"] => string(1) "1"
    ["email"] => string(15) "test01@test.com"
    ["password"] => string(14) "testpassword01"
    ["lastName"] => string(6) "山田"
    ["firstName"] => string(6) "太郎"
    ["lastNameKana"] => string(9) "ヤマダ"
    ["firstNameKana"] => string(9) "タロウ"
    ["customerType"] => string(24) "CUSTOMER_TYPE_INDIVIDUAL"
    ["companyName"] => NULL
    ["zip"] => string(8) "541-0059"
    ["prefecture"] => string(9) "大阪府"
    ["address1"] => string(27) "大阪市中央区博労町"
    ["address2"] => string(45) "１竏窒V竏窒V　中央博労町ビル９Ｆ"
    ["tel"] => string(12) "06-6268-0850"
    ["fax"] => string(12) "06-6268-0851"
    ["totalPurchaseNumber"] => string(1) "1"
    ["point"] => string(1) "0"
    ["accountFlag"] => string(1) "0"
    ["blackFlag"] => string(1) "0"
    ["unpaidFlag"] => string(1) "0"
    ["deleteFlag"] => string(1) "0"
    ["deletionDatetime"] => NULL
    ["updateDatetime"] => string(19) "2010-03-09 21:18:17"
    ["registrationDatetime"] => string(19) "2010-03-09 21:18:17"
    ["updateTimestamp"] => string(19) "2010-03-09 21:18:17"
  }
}
</pre>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple#simpleOneSelectById
            ============================================== -->
            <h2 id="Mikoshiva_Db_Simple#simpleOneSelectById"><a name="simpleOneSelectById">Mikoshiva_Db_Simple#simpleOneSelectById</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              指定された ID にヒットしたデータを返します
            </p>
            <p>
              取得出来なかった場合空の POPO オブジェクトを返します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$arName</td>
                <td>string</td>
                <td><br></td>
                <td>使用する Mikoshiva_Db_Ar クラス名</td>
                <td>ここで指定されたクラス名に紐付くテーブルに対して SELECT を行います</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$id</td>
                <td>int</td>
                <td><br></td>
                <td>プライマリキーの ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>Popo_Abstract</td>
                <td><br></td>
                <td>テーブルに紐付く POPO クラスのインスタンスに値を詰めて返します</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';

// 実行
$simple = new Mikoshiva_Db_Simple();
dumper($simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 2));
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
object(Popo_MstCustomer)#65 (1) {
  ["_properties:protected"] => array(25) {
    ["customerId"] => string(1) "1"
    ["email"] => string(15) "test01@test.com"
    ["password"] => string(14) "testpassword01"
    ["lastName"] => string(6) "山田"
    ["firstName"] => string(6) "太郎"
    ["lastNameKana"] => string(9) "ヤマダ"
    ["firstNameKana"] => string(9) "タロウ"
    ["customerType"] => string(24) "CUSTOMER_TYPE_INDIVIDUAL"
    ["companyName"] => NULL
    ["zip"] => string(8) "541-0059"
    ["prefecture"] => string(9) "大阪府"
    ["address1"] => string(27) "大阪市中央区博労町"
    ["address2"] => string(45) "１竏窒V竏窒V　中央博労町ビル９Ｆ"
    ["tel"] => string(12) "06-6268-0850"
    ["fax"] => string(12) "06-6268-0851"
    ["totalPurchaseNumber"] => string(1) "1"
    ["point"] => string(1) "0"
    ["accountFlag"] => string(1) "0"
    ["blackFlag"] => string(1) "0"
    ["unpaidFlag"] => string(1) "0"
    ["deleteFlag"] => string(1) "0"
    ["deletionDatetime"] => NULL
    ["updateDatetime"] => string(19) "2010-03-09 21:18:17"
    ["registrationDatetime"] => string(19) "2010-03-09 21:18:17"
    ["updateTimestamp"] => string(19) "2010-03-09 21:18:17"
  }
}
</pre>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple#simpleOneSelectByIdFromForm
            ============================================== -->
            <h2 id="Mikoshiva_Db_Simple#simpleOneSelectByIdFromForm"><a name="simpleOneSelectByIdFromForm">Mikoshiva_Db_Simple#simpleOneSelectByIdFromForm</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              第二引数に与えられた配列・オブジェクトからプライマリーキーを見つけ出し、ヒットしたデータを POPO に詰めて返します
            </p>
            <p>
              1件以上取得出来た場合、Mikoshiva_Db_Simple_Exception 投げます<br>
              取得出来なかった場合空の POPO オブジェクトを返します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$arName</td>
                <td>string</td>
                <td><br></td>
                <td>使用する Mikoshiva_Db_Ar クラス名</td>
                <td>ここで指定されたクラス名に紐付くテーブルに対して SELECT を行います</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$actionForm</td>
                <td>
                  array<br>
                  Popo_Abstract<br>
                  Mikoshiva_Controller_Mapping_Form_Interface<br>
                </td>
                <td><br></td>
                <td>条件に使用する配列及びクラス</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>Popo_Abstract</td>
                <td><br></td>
                <td>テーブルに紐付く POPO クラスのインスタンスに値を詰めて返します</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';

// 値をセット
$mm                 =  array();
$mm[\'customerId\'] = 2;
$mm[\'lastName\']   = \'谷口さん☆\';

// 実行
$simple = new Mikoshiva_Db_Simple();
dumper($simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', $mm));
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
object(Popo_MstCustomer)#65 (1) {
  ["_properties:protected"] => array(25) {
    ["customerId"] => string(1) "1"
    ["email"] => string(15) "test01@test.com"
    ["password"] => string(14) "testpassword01"
    ["lastName"] => string(6) "山田"
    ["firstName"] => string(6) "太郎"
    ["lastNameKana"] => string(9) "ヤマダ"
    ["firstNameKana"] => string(9) "タロウ"
    ["customerType"] => string(24) "CUSTOMER_TYPE_INDIVIDUAL"
    ["companyName"] => NULL
    ["zip"] => string(8) "541-0059"
    ["prefecture"] => string(9) "大阪府"
    ["address1"] => string(27) "大阪市中央区博労町"
    ["address2"] => string(45) "１竏窒V竏窒V　中央博労町ビル９Ｆ"
    ["tel"] => string(12) "06-6268-0850"
    ["fax"] => string(12) "06-6268-0851"
    ["totalPurchaseNumber"] => string(1) "1"
    ["point"] => string(1) "0"
    ["accountFlag"] => string(1) "0"
    ["blackFlag"] => string(1) "0"
    ["unpaidFlag"] => string(1) "0"
    ["deleteFlag"] => string(1) "0"
    ["deletionDatetime"] => NULL
    ["updateDatetime"] => string(19) "2010-03-09 21:18:17"
    ["registrationDatetime"] => string(19) "2010-03-09 21:18:17"
    ["updateTimestamp"] => string(19) "2010-03-09 21:18:17"
  }
}
</pre>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple#simpleManySelect
            ============================================== -->
            <h2 id="Mikoshiva_Db_Simple#simpleManySelect"><a name="simpleManySelect">Mikoshiva_Db_Simple#simpleManySelect</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              指定された条件にマッチしたデータを返します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$arName</td>
                <td>string</td>
                <td><br></td>
                <td>使用する Mikoshiva_Db_Ar クラス名</td>
                <td>ここで指定されたクラス名に紐付くテーブルに対して SELECT を行います</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$actionForm</td>
                <td>
                  array<br>
                  Popo_Abstract<br>
                  Mikoshiva_Controller_Mapping_Form_Interface<br>
                </td>
                <td><br></td>
                <td>条件に使用する配列及びクラス</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>Popo_Abstract</td>
                <td><br></td>
                <td>テーブルに紐付く POPO クラスのインスタンスに値を詰めて返します</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';

// 値をセット
$mm          =  array();
$mm[\'zip\'] = \'541-0059\';

// 実行
$simple = new Mikoshiva_Db_Simple();
dumper($simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', $mm));
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
array(2) {
  [0] => object(Popo_MstCustomer)#58 (1) {
    ["_properties:protected"] => array(25) {
      ["customerId"] => string(1) "1"
      ["email"] => string(15) "test01@test.com"
      ["password"] => string(14) "testpassword01"
      ["lastName"] => string(6) "山田"
      ["firstName"] => string(6) "太郎"
      ["lastNameKana"] => string(9) "ヤマダ"
      ["firstNameKana"] => string(9) "タロウ"
      ["customerType"] => string(24) "CUSTOMER_TYPE_INDIVIDUAL"
      ["companyName"] => NULL
      ["zip"] => string(8) "541-0059"
      ["prefecture"] => string(9) "大阪府"
      ["address1"] => string(27) "大阪市中央区博労町"
      ["address2"] => string(45) "１竏窒V竏窒V　中央博労町ビル９Ｆ"
      ["tel"] => string(12) "06-6268-0850"
      ["fax"] => string(12) "06-6268-0851"
      ["totalPurchaseNumber"] => string(1) "1"
      ["point"] => string(1) "0"
      ["accountFlag"] => string(1) "0"
      ["blackFlag"] => string(1) "0"
      ["unpaidFlag"] => string(1) "0"
      ["deleteFlag"] => string(1) "0"
      ["deletionDatetime"] => NULL
      ["updateDatetime"] => string(19) "2010-03-09 21:18:17"
      ["registrationDatetime"] => string(19) "2010-03-09 21:18:17"
      ["updateTimestamp"] => string(19) "2010-03-09 21:18:17"
    }
  }
  [1] => object(Popo_MstCustomer)#65 (1) {
    ["_properties:protected"] => array(25) {
      ["customerId"] => string(1) "3"
      ["email"] => string(25) "elfaris@taupe.plala.or.jp"
      ["password"] => string(8) "19810303"
      ["lastName"] => string(15) "谷口さん☆"
      ["firstName"] => string(6) "司☆"
      ["lastNameKana"] => string(12) "タニグチ"
      ["firstNameKana"] => string(9) "ツカサ"
      ["customerType"] => string(24) "CUSTOMER_TYPE_INDIVIDUAL"
      ["companyName"] => string(0) ""
      ["zip"] => string(8) "541-0059"
      ["prefecture"] => string(9) "大阪府"
      ["address1"] => string(39) "大阪市中央区高津１－３－８"
      ["address2"] => string(52) "エステムコート難波EASTレオルガ９０５"
      ["tel"] => string(13) "090-9861-0023"
      ["fax"] => NULL
      ["totalPurchaseNumber"] => string(1) "0"
      ["point"] => string(1) "0"
      ["accountFlag"] => string(1) "0"
      ["blackFlag"] => string(1) "0"
      ["unpaidFlag"] => string(1) "0"
      ["deleteFlag"] => string(1) "0"
      ["deletionDatetime"] => NULL
      ["updateDatetime"] => string(19) "2010-03-10 18:13:50"
      ["registrationDatetime"] => string(19) "2010-03-10 18:13:50"
      ["updateTimestamp"] => string(19) "2010-03-11 12:38:22"
    }
  }
}
</pre>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple#simpleOneCreateOrUpdateById
            ============================================== -->
            <h2 id="Mikoshiva_Db_Simple#simpleOneCreateOrUpdateById"><a name="simpleOneCreateOrUpdateById">Mikoshiva_Db_Simple#simpleOneCreateOrUpdateById</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              第二引数の値を条件に登録・更新処理を行います。
            </p>
            <p>
              第二引数の値にプライマリキーが存在しなければ登録（INSERT）処理を行います。<br>
              第二引数の値にプライマリキーが存在していれば更新（UPDATE）処理を行います。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$arName</td>
                <td>string</td>
                <td><br></td>
                <td>使用する Mikoshiva_Db_Ar クラス名</td>
                <td>ここで指定されたクラス名に紐付くテーブルに対して SELECT を行います</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$source</td>
                <td>
                  array<br>
                  Popo_Abstract<br>
                  Mikoshiva_Controller_Mapping_Form_Interface<br>
                </td>
                <td><br></td>
                <td>条件に使用する配列及びクラス</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>Popo_Abstract</td>
                <td><br></td>
                <td>テーブルに紐付く POPO クラスのインスタンスに値を詰めて返します</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';


//----------------------------------------------------------
// INSERT の場合
//----------------------------------------------------------

// 値をセット
// $mm[\'email\']              = \'elfaris@taupe.plala.or.jp\';
// $mm[\'password\']           = 19810303;
// $mm[\'lastName\']           = \'山田eeeeee3333\';
// $mm[\'firstName\']          = \'太郎\';
// $mm[\'lastNameKana\']       = \'ヤマダ\';
// $mm[\'firstNameKana\']      = \'タロウ\';
// $mm[\'customerType\']       = \'CUSTOMER_TYPE_INDIVIDUAL\';
// $mm[\'companyName\']        = null;
// $mm[\'zip1\']               = \'111\';
// $mm[\'zip2\']               = \'2222\';
// $mm[\'prefecture\']         = \'大阪府\';
// $mm[\'address1\']           = \'大阪市中央区博労町\';
// $mm[\'address2\']           = \'１－７－７　中央博労町ビル９Ｆ\';
// $mm[\'tel\']                = \'06-6268-0850\';
// $mm[\'fax\']                = null;
// $mm[\'hrighthgiothogjtio\'] = \'hfiorhgiohrtghtiohgoit\';

// 実行
$simple = new Mikoshiva_Db_Simple();
dumper($simple->simpleOneCreateOrUpdateById(\'Mikoshiva_Db_Ar_MstCustomer\', $mm));


//----------------------------------------------------------
// UPDATE の場合
//----------------------------------------------------------

// 値をセット
// $mm[\'customerId\']         = 2;                             // プライマリキー
// $mm[\'email\']              = \'elfaris@taupe.plala.or.jp\';
// $mm[\'password\']           = 19810303;
// $mm[\'lastName\']           = \'山田eeeeee3333\';
// $mm[\'firstName\']          = \'太郎\';
// $mm[\'lastNameKana\']       = \'ヤマダ\';
// $mm[\'firstNameKana\']      = \'タロウ\';
// $mm[\'customerType\']       = \'CUSTOMER_TYPE_INDIVIDUAL\';
// $mm[\'companyName\']        = null;
// $mm[\'zip1\']               = \'111\';
// $mm[\'zip2\']               = \'2222\';
// $mm[\'prefecture\']         = \'大阪府\';
// $mm[\'address1\']           = \'大阪市中央区博労町\';
// $mm[\'address2\']           = \'１－７－７　中央博労町ビル９Ｆ\';
// $mm[\'tel\']                = \'06-6268-0850\';
// $mm[\'fax\']                = null;
// $mm[\'hrighthgiothogjtio\'] = \'hfiorhgiohrtghtiohgoit\';

// 実行
$simple = new Mikoshiva_Db_Simple();
dumper($simple->simpleOneCreateOrUpdateById(\'Mikoshiva_Db_Ar_MstCustomer\', $mm));
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
object(Popo_MstCustomer)#65 (1) {
  ["_properties:protected"] => array(25) {
    ["customerId"] => string(1) "1"
    ["email"] => string(15) "test01@test.com"
    ["password"] => string(14) "testpassword01"
    ["lastName"] => string(6) "山田"
    ["firstName"] => string(6) "太郎"
    ["lastNameKana"] => string(9) "ヤマダ"
    ["firstNameKana"] => string(9) "タロウ"
    ["customerType"] => string(24) "CUSTOMER_TYPE_INDIVIDUAL"
    ["companyName"] => NULL
    ["zip"] => string(8) "541-0059"
    ["prefecture"] => string(9) "大阪府"
    ["address1"] => string(27) "大阪市中央区博労町"
    ["address2"] => string(45) "１竏窒V竏窒V　中央博労町ビル９Ｆ"
    ["tel"] => string(12) "06-6268-0850"
    ["fax"] => string(12) "06-6268-0851"
    ["totalPurchaseNumber"] => string(1) "1"
    ["point"] => string(1) "0"
    ["accountFlag"] => string(1) "0"
    ["blackFlag"] => string(1) "0"
    ["unpaidFlag"] => string(1) "0"
    ["deleteFlag"] => string(1) "0"
    ["deletionDatetime"] => NULL
    ["updateDatetime"] => string(19) "2010-03-09 21:18:17"
    ["registrationDatetime"] => string(19) "2010-03-09 21:18:17"
    ["updateTimestamp"] => string(19) "2010-03-09 21:18:17"
  }
}
</pre>






  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





























