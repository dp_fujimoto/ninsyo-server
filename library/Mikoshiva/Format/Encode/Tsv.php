<?php

/** @package Mikoshiva_Format */

/**
 * require
 */
require_once 'Mikoshiva/Format/Encode/Abstract.php';

/**
 * 
 * 
 * @package Mikoshiva_Format
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: Tsv.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Format_Encode_Tsv extends Mikoshiva_Format_Encode_Abstract
{
	protected $_format = "\t";
	
	public function process( array $data )
	{
		return implode( $this->_format, $data ) . "\n";
	}
}