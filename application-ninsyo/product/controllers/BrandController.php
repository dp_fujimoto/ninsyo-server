<?php
require_once 'Mikoshiva/Controller/Action/Admin/Abstract.php';

/**
 * ブランドを管理するコントローラークラスです。
 *
 * @author      K.fujimoto <fujimoto@d-publishing.jp>
 * @since       2012/06/25
 * @version     SVN: $Id: BrandController.php,v 1.1 2012/09/21 07:08:31 fujimoto Exp $
 */
class Product_BrandController
    extends Mikoshiva_Controller_Action_Admin_Abstract {

}

