




<!-- ▼メインテーブル開始▼ -->
<table class="COM_layautTable">
  <tr>
    <!-- ▼ペイン開始▼ -->
    <td id="COM_painSingle">

      <!-- // メインタイトル -->
      <h2>システムエラー</h2>


      <!-- ▼エラーメッセージ▼ -->
      <div style="
          background:none repeat scroll 0 0 #FFDDCC;
          border:2px solid #DD0000;
          color:#550000;
          margin:1em 0;
          padding:0.5em;
          ">
        <strong style="font-size: 1.5em">内部エラーが発生しました。<br>
        <u>システム管理者にお問い合わせ下さい。</u></strong>
      </div>
      <!-- ▲エラーメッセージ▲ -->

      <!-- ▼閉じるボタン▼ -->
      <p>
      </p>
      <!-- ▲閉じるボタン▲ -->


    </td>
    <!-- ▲ペイン終了▲ -->

  </tr>

</table>
<!-- ▲メインテーブル終了▲ -->




