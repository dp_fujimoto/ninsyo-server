<?php
/** @package Mikoshiva_View */

/**
 * 
 * @version $Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 * @author admin
 * 
 */
require_once 'Zend/View/Helper/Abstract.php';

/**
 * 部署リストを表示します
 * 
 * @package Mikoshiva_View
 * @author Tsukasa Taniguchi 2009-11-19
 *
 */
abstract class Mikoshiva_View_Helper_Abstract extends Zend_View_Helper_Abstract
{
}
