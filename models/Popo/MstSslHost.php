<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_ssl_host
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstSslHost extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'msh';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'sslHostId' => null,
        'sslHostName' => null,
        'sslType' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * msh_ssl_host_id の値を返します
	 *
     * @return int msh_ssl_host_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSslHostId() {
		return $this->_properties['sslHostId'];
    }


    /**
     * msh_ssl_host_id の値をセットします
	 *
	 * @param int $sslHostId msh_ssl_host_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSslHostId($sslHostId) {
		$this->_properties['sslHostId'] = $sslHostId;
    }


    /**
     * msh_ssl_host_name の値を返します
	 *
     * @return varchar msh_ssl_host_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSslHostName() {
		return $this->_properties['sslHostName'];
    }


    /**
     * msh_ssl_host_name の値をセットします
	 *
	 * @param varchar $sslHostName msh_ssl_host_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSslHostName($sslHostName) {
		$this->_properties['sslHostName'] = $sslHostName;
    }


    /**
     * msh_ssl_type の値を返します
	 *
     * @return varchar msh_ssl_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSslType() {
		return $this->_properties['sslType'];
    }


    /**
     * msh_ssl_type の値をセットします
	 *
	 * @param varchar $sslType msh_ssl_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSslType($sslType) {
		$this->_properties['sslType'] = $sslType;
    }


    /**
     * msh_delete_flag の値を返します
	 *
     * @return char msh_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * msh_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag msh_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * msh_deletion_datetime の値を返します
	 *
     * @return datetime msh_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * msh_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime msh_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * msh_registration_datetime の値を返します
	 *
     * @return datetime msh_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * msh_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime msh_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * msh_update_datetime の値を返します
	 *
     * @return datetime msh_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * msh_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime msh_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * msh_update_timestamp の値を返します
	 *
     * @return timestamp msh_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * msh_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp msh_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























