<?php

class Mikoshiva_Error_Exception extends Exception
{
    /**
     * �
     *
     * @var array
     */
    private $_errlev = array(
        E_USER_ERROR   => 'FATAL',
        E_ERROR        => 'FATAL',
        E_USER_WARNING => 'WARNING',
        E_WARNING      => 'WARNING',
        E_USER_NOTICE  => 'NOTICE',
        E_NOTICE       => 'NOTICE',
        E_STRICT       => 'E_STRICT'
    );
    
	public function __construct( $errno, $errstr, $errfile, $errline )
	{
		$addMsg = (string)$errno;
		if( isset( $this->_errlev[$errno] ) )
	    {
			$addMsg = $this->_errlev[$errno] . ' : ';
		}
		parent::__construct( $addMsg . $errstr, $errno );
		$this->file = $errfile;
		$this->line = $errline;
	}
}
