<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー存在チェックAPIチェックフォーム</title>
</head>

<body>
<form name="form" method="post" action="../api/user/check">
    <input type="hidden" name="flag" value="1">
    <table>
        <tr>
            <td>セッションID:</td>
            <td><input type="text" name="sessionId"></td>
        </tr>
        <tr>
            <td>メールアドレス:</td>
            <td><input type="text" name="id"></td>
        </tr>
        <tr>
            <td>パスワード:</td>
            <td><input type="text" name="password"></td>
        </tr>
        <tr>
            <td>アプリナンバー:</td>
            <td><input type="text" name="appNum" value="1"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="送信"></td>
        </tr>
    </table>
</form>
</body>
</html>