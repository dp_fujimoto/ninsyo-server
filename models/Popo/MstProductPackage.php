<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product_package
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProductPackage extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mpp';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productPackageId' => null,
        'divisionId' => null,
        'productShippingId' => null,
        'packageCode' => null,
        'packageNumber' => null,
        'packageName' => null,
        'shippingAtPaymentDeliveryFlag' => null,
        'packageType' => null,
        'packageSize' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 商品パッケージID の値を返します
	 *
     * @return int 商品パッケージID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductPackageId() {
		return $this->_properties['productPackageId'];
    }


    /**
     * 商品パッケージID の値をセットします
	 *
	 * @param int $productPackageId 商品パッケージID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductPackageId($productPackageId) {
		$this->_properties['productPackageId'] = $productPackageId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 商品発送ID の値を返します
	 *
     * @return int 商品発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingId() {
		return $this->_properties['productShippingId'];
    }


    /**
     * 商品発送ID の値をセットします
	 *
	 * @param int $productShippingId 商品発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingId($productShippingId) {
		$this->_properties['productShippingId'] = $productShippingId;
    }


    /**
     * パッケージコード の値を返します
	 *
     * @return varchar パッケージコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageCode() {
		return $this->_properties['packageCode'];
    }


    /**
     * パッケージコード の値をセットします
	 *
	 * @param varchar $packageCode パッケージコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageCode($packageCode) {
		$this->_properties['packageCode'] = $packageCode;
    }


    /**
     * パッケージナンバー の値を返します
	 *
     * @return int パッケージナンバー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageNumber() {
		return $this->_properties['packageNumber'];
    }


    /**
     * パッケージナンバー の値をセットします
	 *
	 * @param int $packageNumber パッケージナンバー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageNumber($packageNumber) {
		$this->_properties['packageNumber'] = $packageNumber;
    }


    /**
     * パッケージ名 の値を返します
	 *
     * @return varchar パッケージ名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageName() {
		return $this->_properties['packageName'];
    }


    /**
     * パッケージ名 の値をセットします
	 *
	 * @param varchar $packageName パッケージ名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageName($packageName) {
		$this->_properties['packageName'] = $packageName;
    }


    /**
     * 代引き時発送フラグ の値を返します
	 *
     * @return char 代引き時発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAtPaymentDeliveryFlag() {
		return $this->_properties['shippingAtPaymentDeliveryFlag'];
    }


    /**
     * 代引き時発送フラグ の値をセットします
	 *
	 * @param char $shippingAtPaymentDeliveryFlag 代引き時発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAtPaymentDeliveryFlag($shippingAtPaymentDeliveryFlag) {
		$this->_properties['shippingAtPaymentDeliveryFlag'] = $shippingAtPaymentDeliveryFlag;
    }


    /**
     * パッケージタイプ の値を返します
	 *
     * @return varchar パッケージタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageType() {
		return $this->_properties['packageType'];
    }


    /**
     * パッケージタイプ の値をセットします
	 *
	 * @param varchar $packageType パッケージタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageType($packageType) {
		$this->_properties['packageType'] = $packageType;
    }


    /**
     * サイズ の値を返します
	 *
     * @return int サイズ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageSize() {
		return $this->_properties['packageSize'];
    }


    /**
     * サイズ の値をセットします
	 *
	 * @param int $packageSize サイズ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageSize($packageSize) {
		$this->_properties['packageSize'] = $packageSize;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























