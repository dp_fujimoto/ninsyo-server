[production]
routes.default.route = ":controller/:action/*"
routes.default.defaults.module = default
routes.default.defaults.controller = index
routes.default.defaults.action = index

routes.tt.route = "tt/:controller/:action/*"
routes.tt.defaults.module = tt
routes.tt.defaults.controller = index
routes.tt.defaults.action = index

routes.login.route = "login/:controller/:action/*"
routes.login.defaults.module = login
routes.login.defaults.controller = index
routes.login.defaults.action = index