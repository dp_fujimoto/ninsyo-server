<?php

/** @package Mikoshiva_Validate */



/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';


/**
 * エラーメッセージエクセプション
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/03
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Validate_Message_Exception extends Zend_Exception
{}
