<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/Operation.php';
require_once 'tool/models/Operation.php';

/**
 * コンテンツ情報の登録を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class User_Models_Content_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * コンテンツ情報の登録を行う。
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        if (!is_array($actionForm->getList())) {
            $list = array();
        } else {
            $list = $actionForm->getList();
        }
        $deleteFlag = $actionForm->getDeleteFlag();
        $config = $context->getConfig();

        // 共通クラス
        $class = new Api_Models_Operation();

        $output = $class->insertUserContentDetail($list, $deleteFlag, $config['NINSYO_SERVER_NAME_LIST']);

        //-----------------------------------------------
        // ユーザーキャッシュファイル削除
        //-----------------------------------------------
        $operation = new Api_Models_Operation();
        $operation->deleteUserCacheFile($actionForm, $context);

        file_get_contents(DELETE_USER_CACHE_FILE_API);

        //-----------------------------------------------
        // アクションログ保存
        //-----------------------------------------------
        $logText = '';
        if (isset($output['all'])) {
            $logText .= $output['all'] . "\r\n";
        }
        if (isset($output['error']) && !isBlankOrNull($output['error'])) {
            $logText .= $output['error'] . "\r\n";
        }

        $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

        $operation = new Tool_Models_Operation();
        $operation->insertActionLog($logText);

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }
}
