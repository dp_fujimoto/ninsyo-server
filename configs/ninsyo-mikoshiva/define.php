<?php


/**
 * 定数設定ファイル
 *
 * 【説明】
 *
 *
 * 【概要】
 *
 * 【変更履歴】
 * 2011-03-23 谷口司 一時ログにsalesを追加
 */
include_once dirname(__FILE__) . '/test-server-flag.php';
include_once dirname(__FILE__) . '/test-define.php';

//--------------------------------------------
//
// ■本番環境設定
//
//---------------------------------------------
if (strncmp(PHP_OS, 'WIN', 3) === 0) define('REAL_FLAG', 0); // LOCAL
elseif (TEST_SERVER_FLAG === 1)      define('REAL_FLAG', 0); // テストサーバー
else                                 define('REAL_FLAG', 1); // 本番


//--------------------------------------------
//
// ■各ディレクトパス
//
//---------------------------------------------
define('PROJECT_PATH',          dirname(__FILE__) . '/../..');

// システムでメインで使うアプリケーション
define('APPLICATION_PATH',           PROJECT_PATH      . '/application-ninsyo');
define('MODELS_DIR',                 PROJECT_PATH      . '/models/ninsyo');
define('SUB_FOLDER_PATH',            '');

define('RESOURCE_PATH',         PROJECT_PATH      . '/resource');
define('LIBRARY_PATH',          PROJECT_PATH      . '/library');
define('AR_PATH',               LIBRARY_PATH      . '/Mikoshiva/Db/NinsyoAr');
define('CONFIGS_DIR',           PROJECT_PATH      . '/configs');
define('SYSTEMS_DIR',           PROJECT_PATH      . '/systems');
define('LOGS_DIR',              SYSTEMS_DIR       . '/logs');
define('CACHES_DIR',            SYSTEMS_DIR       . '/caches');
define('UPLOADS_DIR',           SYSTEMS_DIR       . '/uploads');
define('UPLOADS_TEMP_DIR',      UPLOADS_DIR       . '/temp');
define('SESSION_DIR',           SYSTEMS_DIR       . '/session');
define('LOGS_DIR_BATCH',        SYSTEMS_DIR       . '/bin_logs');
define('DOCUMENT_ROOT',         $_SERVER['DOCUMENT_ROOT']);
define('ORDER_TEMPLATE_PATH',   PROJECT_PATH      . '/web_template');
define('MAIL_ATTACHMENT_FILES', SYSTEMS_DIR       . '/mail-attachment-files');
define('USER_CONTENT_XML_FILES', SYSTEMS_DIR       . '/user-content-xml-files');
define('CONDITION_JSON_FILES', SYSTEMS_DIR       . '/condition-json-files');

//--------------------------------------------
//
// ■その他
//
//---------------------------------------------
// ◆https
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') define('_HTTP_', 'https://'); // https
else                                                       define('_HTTP_', 'http://');  // http

// ◆MIKOSHIVA ドメイン
if(isset($_SERVER['HTTP_HOST'])) define('MIKOSHIVA_DOMAIN', $_SERVER['HTTP_HOST']);
else                             define('MIKOSHIVA_DOMAIN', 'mikoshiva');


//------------------------------------------------------------------
//
// ■会社関連
//
//------------------------------------------------------------------
// ▼エラー送信先メールアドレス
define('MIKOSHIVA_ERROR_MAIL',    'direct.errors@gmail.com'); // エラーメール送信先メールアドレス
define('ERROR_MAIL_FROM_ADDRESS', 'admin@ninsyo-mikoshiva.com');     // エラーメール送信者アドレス
define('ERROR_MAIL_FROM_NAME',    'Ninsyo System');        // エラーメール送信者名
define('ERROR_MAIL_TO_ADDRESS',   'direct.errors@gmail.com'); // エラーメール宛先アドレス
define('ERROR_MAIL_TO_NAME',      'ios認証サーバーエラー受信');     // エラーメール宛先名

// ▼自動配信メール
// ダイレクト出版株式会社
define('SYSTEM_EMAIL_ORDER_D_PUBLISHING', 'order@d-publishing.jp');
//その他の自動配信メールは
//ダイレクト出版株式会社
define('SYSTEM_EMAIL_SUPPORT_D_PUBLISHING', 'support@d-publishing.jp');

// ▼会社名
define('SYSTEM_COMPANY_NAME_DIRECT_PUBLISHING', 'ダイレクト出版株式会社'); // 会社名
define('SYSTEM_COMPANY_NAME_PREFIX_WORD',       'D');                      // 会社接頭辞
define('SYSTEM_COMPANY_ZIP',                    '541-0059');
define('SYSTEM_COMPANY_ADDRESS',                '大阪市中央区博労町1-7-7　中央博労町ビル9F');
define('SYSTEM_COMPANY_SUPPORT_URL',            'http://www.drmaltz.jp/support/');
define('SYSTEM_COMPANY_FAX',                    '06-6268-0851');
define('SYSTEM_COMPANY_TEL',                    '06-6268-0850');
define('SYSTEM_COMPANY_RECEPTION',              '(電話受付時間　平日10:00-17:00)');

// ▼サポートメール
define('TO_AUTO_SUPPORT_EMAIL', 'auto-mail@d-publishing.jp');
define('TO_AUTO_SUPPORT_NAME'  , 'mikoshiva発送自動メール');

// ▼メンバーシップ登録API実行フラグ 1:実行する 0:実行しない
if (REAL_FLAG === 0) define('MEMBER_SHIP_API_EXECUTE_FLAG', '0'); // ローカル
else                 define('MEMBER_SHIP_API_EXECUTE_FLAG', '1'); // 本番

// ▼特商法URL
define('OD_TOKUSHOHO_URL', 'http://www.d-publishing.jp/law/');
// ▼プライバシーポリシーURL
define('OD_PRIVACY_URL', 'http://www.d-publishing.jp/privacy/');
// ▼サポートURL
define('OD_SUPPORT_URL', 'http://www.d-publishing.jp/contact/');
// ▼個人情報取り扱いURL
define('OD_HANDLING_PERSONAL_INFO_URL', 'https://www.d-publishing.jp/privacy/MKprivacy.php');
// ▼商品が更新された時にお知らせメールを飛ばす先
define('PRODUCT_UPDATE_INFO_SEND_ADDRESS', 'operation_u@d-publishing.jp');

// ▼パスワード
define('SALES_DM_CAMPAIGN_ZIP_PASSWORD', 'tada1125');

// ▼ダイレクサーポートURL（保証）
define('SYSTEM_SUPPORT_URL_HOSYOU', 'http://www.directsupport.jp/hosyou.html');


//------------------------------------------------------------------
//
// ■認証処理
//
//------------------------------------------------------------------
if (REAL_FLAG === 0) define('AUTHENTICATION_SERVER_API_BASE_URL', 'http://ios_auth'); // ローカル
else                 define('AUTHENTICATION_SERVER_API_BASE_URL', 'https://www.dp-auth.jp'); // 本番
define('NINSYO_API', AUTHENTICATION_SERVER_API_BASE_URL . '/api/authentication/check');
define('INSERT_USER_CONTENT_API', AUTHENTICATION_SERVER_API_BASE_URL . '/api/user-content/insert-user-content');
define('DELETE_CONDITION_CACHE_FILE_API', AUTHENTICATION_SERVER_API_BASE_URL . '/api/content/delete-condition-cache-file');
define('DELETE_USER_CACHE_FILE_API', AUTHENTICATION_SERVER_API_BASE_URL . '/api/user-content/delete-user-cache-file');

define('NINSYO_APP_ID', '3443b5d01263d73ca8c734bc917d6450');
define('NINSYO_APP_KEY', 'f4c6be0b8ebfafecf56a8d223fb1bcc8');

// ▼パスワード
define('NINSYO_ADMIN_PASSWORD', 'dk101513');
define('NINSYO_TRAINING_PASSWORD', 'tr1007');

// ▼暗号化ベース
define('HASH_ALGO', 'sha256');
define('HASH_LENGTH', 64);

// ▼テスト用アカウント
define('TEST_USER_EMAIL', 'testtesttest@test.com');
define('TEST_USER_PASS', 'testtesttest');

// ▼無料用アカウント
define('FREE_USER_EMAIL', 'freefreefree@free.com');
define('FREE_USER_PASS', 'freefreefree');

// ▼認証アプリIDリスト
// 認証プロトコルバージョン、アプリID、アプリキーの順
$NINSYO_APP_ID_LIST = array();
$NINSYO_APP_ID_LIST['1.0'] = array();
$NINSYO_APP_ID_LIST['1.0']['3443b5d01263d73ca8c734bc917d6450'] = 'f4c6be0b8ebfafecf56a8d223fb1bcc8'; // dp-library
$NINSYO_APP_ID_LIST['1.0']['G8jG8Bi4GQeYjVjVEr3BU2aTLS7STurFBAkZiGCH'] = 'jtbudphHzDRpxBTMFCHgDiyAaXU3kAgPfKirpeaD'; // ios
$NINSYO_APP_ID_LIST['1.0']['d041924c15885af6d06530a425c6dbff'] = 'c80520150c4dd264f40b4364b12421a8'; // signal-trade
$NINSYO_APP_ID_LIST['1.0']['mikoshiva'] = 'mikoshiva';
$NINSYO_APP_ID_LIST['1.0']['shimant'] = 'shimant';
$NINSYO_APP_ID_LIST['1.0']['gusiken'] = 'gusiken';
$NINSYO_APP_ID_LIST['1.0']['test.mikoshiva'] = 'test.mikoshiva';
$NINSYO_APP_ID_LIST['1.0']['mypage'] = 'mypage';

// 許可サーバー名リスト
$NINSYO_SERVER_NAME_LIST = array();
$NINSYO_SERVER_NAME_LIST['mikoshiva'] = 'mikoshiva';
$NINSYO_SERVER_NAME_LIST['shimant'] = 'shimant';
$NINSYO_SERVER_NAME_LIST['gusiken'] = 'gusiken';
$NINSYO_SERVER_NAME_LIST['test.mikoshiva'] = 'test.mikoshiva';
$NINSYO_SERVER_NAME_LIST['mypage'] = 'mypage';
$NINSYO_SERVER_NAME_LIST['free'] = 'free';

// test
//$NINSYO_APP_ID_LIST['1.0']['ios'] = 'test';
//$NINSYO_APP_ID_LIST['1.0']['kawa'] = 'kami';

// 画像サイズ
define('BLAND_LOGO_W', 110);
define('BLAND_LOGO_H', 105);
define('BLAND_BANNER_V_W', 728);
define('BLAND_BANNER_V_H', 128);
define('BLAND_BANNER_S_W', 984);
define('BLAND_BANNER_S_H', 128);
define('PRODUCT_IMAGE_W', 220);
define('PRODUCT_IMAGE_H', 210);
define('CONTENT_IMAGE_W', 110);
define('CONTENT_IMAGE_H', 105);

//------------------------------------------------------------------
//
// ■Google Analytics
//
//------------------------------------------------------------------
define('GOOGLE_ANALYTICS_LOGIN_ID',   'mikoshiva@gmail.com');
define('GOOGLE_ANALYTICS_LOGIN_PASS', 'tada1125');


//------------------------------------------------
//
// ■DB 接続情報
//
//------------------------------------------------
if (REAL_FLAG === 0) {
    define('DB_PAYMENT_HOST',         'localhost');
    define('DB_PAYMENT_USER_NAME',    'mk_ninsyo');
    define('DB_PAYMENT_PASSWORD',     'mk_ninsyo');
    define('DB_PAYMENT_DB_NAME',      'mk_ninsyo');
    //define('CLIENT_ENCODING', 'UTF-8');
} else {
    define('DB_PAYMENT_HOST',         'localhost');
    define('DB_PAYMENT_USER_NAME',    'mk_ninsyo');
    define('DB_PAYMENT_PASSWORD',     'mk_ninsyo');
    define('DB_PAYMENT_DB_NAME',      'mk_ninsyo');
    //define('CLIENT_ENCODING', 'UTF-8');
}


//------------------------------------------------
//
// ■ログレベルの設定
//
// EMERG   = 0;  // 緊急事態 (Emergency):     システムが使用不可能です
// ALERT   = 1;  // 警報     (Alert):         至急対応が必要です
// CRIT    = 2;  // 危機     (Critical):      危機的な状況です
// ERR     = 3;  // エラー   (Error):         エラーが発生しました
// WARN    = 4;  // 警告     (Warning):       警告が発生しました
// NOTICE  = 5;  // 注意     (Notice):        通常動作ですが、注意すべき状況です
// INFO    = 6;  // 情報     (Informational): 情報メッセージ
// DEBUG   = 7;  // デバッグ (Debug):         デバッグメッセージ
//
//------------------------------------------------
define('OPERATION_LOG_FLAG', 1);  // オペレーションログフラグ。(0:開発 1:本番)
$LOG_TEMPORARY = array();
if (REAL_FLAG === 0) {

    // ▼ローカル▼
    define('LOG_LEVEL_SQL',     5);   // SQL ログ
    define('LOG_LEVEL_DEVEL',   5);   // 開発 ログ
    define('LOG_LEVEL_FILE',    5);   // 通常ログ
    define('LOG_LEVEL_MAIL',    99);  // メールログ
    define('LOG_LEVEL_FIREBUG', 99);   // firebug
    define('LOG_MIKOSHIVA_CLASS', true); // MIKOSHIVA クラスのログ出力を停止します true:出力|false:出力しない

    define('LOG_LEVEL_SQL_FILENAME',           date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');   // SQL ログファイル名
    define('LOG_LEVEL_DEVEL_FILENAME',         date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');   // DEVELログファイル名
    define('LOG_LEVEL_FILE_FILE_NAME',         date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');   // 通常ログファイル名


    //------------------------------------------------
    // ■一時ログ
    //------------------------------------------------
    $LOG_TEMPORARY = array(
        'batch'      => true,
        'login'      => true,
        'product'    => true,
        'api'        => true,
        'tool'       => true,
    );

    //------------------------------------------------
    // ■デバッグモード 1:表示 0:非表示
    //------------------------------------------------
    define('DEBUG_VIEW_HISTORY', 0);  // デバッグ履歴
    define('DEBUG_VIEW_REQUEST', 0);  // デバッグリクエスト
    define('DEBUG_VIEW_HTML',    0);  // デバッグHTMLソース
} else {

    // ▼本番▼
    define('LOG_LEVEL_SQL',     5);  // SQL ログ
    define('LOG_LEVEL_DEVEL',   5);  // 開発 ログ
    define('LOG_LEVEL_FILE',    5);  // 通常ログ
    define('LOG_LEVEL_MAIL',    99);  // メールログ
    define('LOG_LEVEL_FIREBUG', 99);  // firebug
    define('LOG_MIKOSHIVA_CLASS', true); // MIKOSHIVA クラスのログ出力を停止します true:出力|false:出力しない

    define('LOG_LEVEL_SQL_FILENAME',     date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');   // SQL ログファイル名
    define('LOG_LEVEL_DEVEL_FILENAME',   date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');   // DEVELログファイル名
    define('LOG_LEVEL_FILE_FILE_NAME',   date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');   // 通常ログファイル名


    //------------------------------------------------
    // ■一時ログ
    //------------------------------------------------
    $LOG_TEMPORARY = array(
        'api'        => true,
        'batch'      => true,
        'login'      => true,
        'product'    => true,
        'tool'       => true,
        'user'    => true,
    );

    //------------------------------------------------
    // ■デバッグモード 1:表示 0:非表示
    //------------------------------------------------
    define('DEBUG_VIEW_HISTORY', 0);  // デバッグ履歴
    define('DEBUG_VIEW_REQUEST', 0);  // デバッグリクエスト
    define('DEBUG_VIEW_HTML',    0);  // デバッグHTMLソース
}


//------------------------------------------------
//
// ■ログ関連定数
//
//------------------------------------------------
define('LOG_START',      '-- START --');
define('LOG_END',        '-- END --');
define('LOG_LOOP_START', '-- LOOP_START --');
define('LOG_LOOP_END',   '-- LOG_LOOP_END --');
define('LOG_IF_START',   '-- IF_START --');
define('LOG_IF_END',     '-- IF_END --');


//------------------------------------------------
//
// ■デビットカード定義
//
//------------------------------------------------
$DEBIT_CARD_LIST = array(
);

//------------------------------------------------
//
// ■ワンタイムデビットカード定義
//
//------------------------------------------------
$ONE_TIME_DEBIT_CARD_LIST = array(
);



//---------------------------------------------------
//
// ■テスト用クレジットカード番号
//
//---------------------------------------------------
$GMO_TEST_CARD_LIST = array();
$GMO_TEST_CARD_LIST['4111111111111111'] = ''; // － － 正常カード



//---------------------------------------------------
//
// ■テスト用クレジットカード番号の使用を許可するネットワークのリスト
//   ノード単体のIPアドレスを指定する場合は、'xxx.xxx.xxx.xxx/32'を指定
//
//---------------------------------------------------
$GMO_TEST_CARD_ALLOW_LIST = array();
$GMO_TEST_CARD_ALLOW_LIST[] = '110.3.193.16';   // インベストメントカレッジ（沖縄）
$GMO_TEST_CARD_ALLOW_LIST[] = '110.3.185.110';  // 大阪オフィスグローバルIP（新2012-01-31）
$GMO_TEST_CARD_ALLOW_LIST[] = '110.3.254.45';   // 大阪オフィスグローバルIP（新）
$GMO_TEST_CARD_ALLOW_LIST[] = '61.44.64.34';    // 大阪オフィスグローバルIP（旧）
$GMO_TEST_CARD_ALLOW_LIST[] = '61.45.196.53';   // ○大阪８階 IP
$GMO_TEST_CARD_ALLOW_LIST[] = '43.244.133.223'; // 東京オフィスグローバルIP
$GMO_TEST_CARD_ALLOW_LIST[] = '36.52.201.73';   // 東京オフィスグローバルIP
$GMO_TEST_CARD_ALLOW_LIST[] = '127.0.0.1';      // Localhost

//------------------------------------------------
//
// ■GMO 設定
//
//------------------------------------------------
// ▼オーダー ID プレフィックス
define('ORDER_ID_PREFIX', 'DP');

if (isset($_REQUEST['cardNo']) && $_REQUEST['cardNo'] === '4111111111111111') {
    define('GMO_FLAG', 'TEST');
} else {
    if (REAL_FLAG === 0) define('GMO_FLAG', 'TEST');
    else                 define('GMO_FLAG', 'REAL');
}
$GMO_CONFIG         = array();

//------------------------------------------------
//
// ■メンテナンス関連設定ファイル
//
//                    2011-04-11 谷口司
//
//------------------------------------------------
include_once dirname(__FILE__) . '/operation-log-flag.php';



//------------------------------------------------
//
// ■メンテナンス関連設定ファイル
//
//                    T.Taniguchi 2010/07/06
//
//------------------------------------------------
include_once dirname(__FILE__) . '/MENTE.php';



//------------------------------------------------
//
// ■項目名マッピング（Validate で使用します）
//   別ファイルに移動しました。
//
//                    T.Taniguchi 2010/07/06
//
//------------------------------------------------
include_once dirname(__FILE__) . '/column_map_list.php';



//-----------------------------------------------
//
// ■テーブル情報
//
//-----------------------------------------------
include_once dirname(__FILE__) . '/table-info.php';

//-----------------------------------------------
//
// ステータス一覧
//
//-----------------------------------------------
include_once dirname(__FILE__) . '/status-mapping.php';


//-----------------------------------------------
//
// FTPサーバ情報
//
//-----------------------------------------------
include_once dirname(__FILE__) . '/ftp-config.php';


//-----------------------------------------------
//
// メールサーバー情報
//
//-----------------------------------------------
include_once dirname(__FILE__) . '/mail-config.php';


//-----------------------------------------------
//
// 外部API情報
//
//-----------------------------------------------
include_once dirname(__FILE__) . '/api-config.php';


//-----------------------------------------------
//
// ACD/Mikoshiva 対応リスト
//
//-----------------------------------------------
include_once dirname(__FILE__) . '/acd-mapping.php';



