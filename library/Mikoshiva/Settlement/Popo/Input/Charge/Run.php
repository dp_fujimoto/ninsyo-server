<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Charge_Run
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Charge_Run extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'CardNo' => null,
        'Expire' => null,
        'ShopID' => null,
        'OrderID' => null,
        'Amount' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * Expire
	 *
     * @return  Expire
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getExpire() {
		return $this->_properties['Expire'];
    }


    /**
     * Expire
	 *
	 * @param  $Expire Expire
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setExpire($Expire) {
		$this->_properties['Expire'] = $Expire;
    }


    /**
     * ShopID
	 *
     * @return  ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getShopID() {
		return $this->_properties['ShopID'];
    }


    /**
     * ShopID
	 *
	 * @param  $ShopID ShopID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setShopID($ShopID) {
		$this->_properties['ShopID'] = $ShopID;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * Amount
	 *
     * @return  Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAmount() {
		return $this->_properties['Amount'];
    }


    /**
     * Amount
	 *
	 * @param  $Amount Amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAmount($Amount) {
		$this->_properties['Amount'] = $Amount;
    }


}

























