<?php
/** @package Mikoshiva_Filter */


/**
 * 第一引数に渡された値に対してフィルターを掛けます。
 *
 * フィルターの詳細
 * ・全角英数字を半角
 * ・半角カナを全角カナに
 * ・全角空白削除
 *
 * @package Mikoshiva_Filter
 * @author admin
 * @since 2010/02/17
 * @version SVN:$Id: Request.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Filter_Input_Request
{
    /**
     * 処理メソッド
     *
     * @param $data String|Array フィルターを掛ける値
     * @param $exclusionList 除外リスト
     * @return フィルターを掛けた値
     */
    public static function exec( $data, $exclusionList = array() )
    {
        switch( true )
        {
            case is_array( $data ):
                array_walk( $data, array( 'Mikoshiva_Filter_Input_Request', '_arrayFilter'), $exclusionList );
                break;

            case is_string( $data ):
                self::_stringFilter( $data );
                break;
        }

        return $data;
    }

    /**
     * 配列の値すべてに対しフィルターを掛けます
     *
     * @param &$data
     * @param $key
     * @param $exclusionList
     * @return void
     */
    private static function _arrayFilter( &$data, $key, $exclusionList = array() )
    {
        // ◆$key が除外リストのキー名で有ればリターン
        if( in_array( $key, $exclusionList, true ) ) return $data;

        if( is_object($data) ) return;

        // ◆配列で有れば再度allBlankerに掛ける
        if( is_array( $data ) ) {
            return array_walk( $data, array( 'Mikoshiva_Filter_Input_Request', '_arrayFilter'), $exclusionList );
        }

        // ◆null か空文字で有ればリターン
        if( ( $data === null ) || ( $data === '' ) ) return $data;

        // ◆全角英数字を半角・半角カナを全角カナに置換・空白削除
        self::_stringFilter( $data );
    }

    /**
     * 文字列に対しフィルターを掛けます
     *
     * @param &$data
     * @return void
     */
    private static function _stringFilter( &$data )
    {
        // 「全角」英字を「半角」に変換します。
        // 「全角」数字を「半角」に変換します。
        // 「半角カタカナ」を「全角カタカナ」に変換します。
        $data = mb_convert_kana( $data, 'rnK' );  // 片仮名
        $data = str_replace("\r\n", "\n", $data ); // 改行コード
        $data = str_replace("\r",   "\n", $data ); // 改行コード
        $data = str_replace("\t",   '', $data );   // タブ
        $data = str_replace("＠",   '@', $data );   // アットマーク
        $data = str_replace("‐",   '-', $data );   // 全角ハイフン
        $data = str_replace("－",   '-', $data );   // 全角ハイフン
        $data = str_replace("―",   '-', $data );   // 全角ダッシュ
        $data = str_replace("＿",   '_', $data );   // 全角アンダーライン
        $data = str_replace("．",   '.', $data );   // 全角ピリオド
        $data = trim($data, ' ');
//        $data = trim($data, "　");

        //$data = preg_replace('#^[ 　]#u', '', $data);
        //$data = preg_replace('#[ 　]$#u', '', $data);
    }
}

























