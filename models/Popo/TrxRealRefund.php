<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_real_refund
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxRealRefund extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tre';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'realRefundId' => null,
        'chargeId' => null,
        'claimId' => null,
        'returnRefundId' => null,
        'realRefundAmount' => null,
        'realRefundDate' => null,
        'realRefundType' => null,
        'realRefundCompleteFlag' => null,
        'resultFileType' => null,
        'resultFileName' => null,
        'resultFileRowNumber' => null,
        'resultFileId' => null,
        'resultFileUploadDatetime' => null,
        'staffId' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tre_real_refund_id の値を返します
	 *
     * @return int tre_real_refund_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealRefundId() {
		return $this->_properties['realRefundId'];
    }


    /**
     * tre_real_refund_id の値をセットします
	 *
	 * @param int $realRefundId tre_real_refund_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealRefundId($realRefundId) {
		$this->_properties['realRefundId'] = $realRefundId;
    }


    /**
     * tre_charge_id の値を返します
	 *
     * @return int tre_charge_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * tre_charge_id の値をセットします
	 *
	 * @param int $chargeId tre_charge_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * tre_claim_id の値を返します
	 *
     * @return int tre_claim_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClaimId() {
		return $this->_properties['claimId'];
    }


    /**
     * tre_claim_id の値をセットします
	 *
	 * @param int $claimId tre_claim_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClaimId($claimId) {
		$this->_properties['claimId'] = $claimId;
    }


    /**
     * tre_return_refund_id の値を返します
	 *
     * @return int tre_return_refund_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnRefundId() {
		return $this->_properties['returnRefundId'];
    }


    /**
     * tre_return_refund_id の値をセットします
	 *
	 * @param int $returnRefundId tre_return_refund_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnRefundId($returnRefundId) {
		$this->_properties['returnRefundId'] = $returnRefundId;
    }


    /**
     * tre_real_refund_amount の値を返します
	 *
     * @return int tre_real_refund_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealRefundAmount() {
		return $this->_properties['realRefundAmount'];
    }


    /**
     * tre_real_refund_amount の値をセットします
	 *
	 * @param int $realRefundAmount tre_real_refund_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealRefundAmount($realRefundAmount) {
		$this->_properties['realRefundAmount'] = $realRefundAmount;
    }


    /**
     * tre_real_refund_date の値を返します
	 *
     * @return date tre_real_refund_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealRefundDate() {
		return $this->_properties['realRefundDate'];
    }


    /**
     * tre_real_refund_date の値をセットします
	 *
	 * @param date $realRefundDate tre_real_refund_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealRefundDate($realRefundDate) {
		$this->_properties['realRefundDate'] = $realRefundDate;
    }


    /**
     * tre_real_refund_type の値を返します
	 *
     * @return varchar tre_real_refund_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealRefundType() {
		return $this->_properties['realRefundType'];
    }


    /**
     * tre_real_refund_type の値をセットします
	 *
	 * @param varchar $realRefundType tre_real_refund_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealRefundType($realRefundType) {
		$this->_properties['realRefundType'] = $realRefundType;
    }


    /**
     * tre_real_refund_complete_flag の値を返します
	 *
     * @return char tre_real_refund_complete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealRefundCompleteFlag() {
		return $this->_properties['realRefundCompleteFlag'];
    }


    /**
     * tre_real_refund_complete_flag の値をセットします
	 *
	 * @param char $realRefundCompleteFlag tre_real_refund_complete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealRefundCompleteFlag($realRefundCompleteFlag) {
		$this->_properties['realRefundCompleteFlag'] = $realRefundCompleteFlag;
    }


    /**
     * tre_result_file_type の値を返します
	 *
     * @return varchar tre_result_file_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileType() {
		return $this->_properties['resultFileType'];
    }


    /**
     * tre_result_file_type の値をセットします
	 *
	 * @param varchar $resultFileType tre_result_file_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileType($resultFileType) {
		$this->_properties['resultFileType'] = $resultFileType;
    }


    /**
     * tre_result_file_name の値を返します
	 *
     * @return varchar tre_result_file_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileName() {
		return $this->_properties['resultFileName'];
    }


    /**
     * tre_result_file_name の値をセットします
	 *
	 * @param varchar $resultFileName tre_result_file_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileName($resultFileName) {
		$this->_properties['resultFileName'] = $resultFileName;
    }


    /**
     * tre_result_file_row_number の値を返します
	 *
     * @return int tre_result_file_row_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileRowNumber() {
		return $this->_properties['resultFileRowNumber'];
    }


    /**
     * tre_result_file_row_number の値をセットします
	 *
	 * @param int $resultFileRowNumber tre_result_file_row_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileRowNumber($resultFileRowNumber) {
		$this->_properties['resultFileRowNumber'] = $resultFileRowNumber;
    }


    /**
     * tre_result_file_id の値を返します
	 *
     * @return varchar tre_result_file_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileId() {
		return $this->_properties['resultFileId'];
    }


    /**
     * tre_result_file_id の値をセットします
	 *
	 * @param varchar $resultFileId tre_result_file_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileId($resultFileId) {
		$this->_properties['resultFileId'] = $resultFileId;
    }


    /**
     * tre_result_file_upload_datetime の値を返します
	 *
     * @return datetime tre_result_file_upload_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileUploadDatetime() {
		return $this->_properties['resultFileUploadDatetime'];
    }


    /**
     * tre_result_file_upload_datetime の値をセットします
	 *
	 * @param datetime $resultFileUploadDatetime tre_result_file_upload_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileUploadDatetime($resultFileUploadDatetime) {
		$this->_properties['resultFileUploadDatetime'] = $resultFileUploadDatetime;
    }


    /**
     * tre_staff_id の値を返します
	 *
     * @return int tre_staff_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * tre_staff_id の値をセットします
	 *
	 * @param int $staffId tre_staff_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * tre_imperfection_flag の値を返します
	 *
     * @return char tre_imperfection_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * tre_imperfection_flag の値をセットします
	 *
	 * @param char $imperfectionFlag tre_imperfection_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * tre_delete_flag の値を返します
	 *
     * @return char tre_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tre_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tre_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tre_deletion_datetime の値を返します
	 *
     * @return datetime tre_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tre_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tre_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tre_update_datetime の値を返します
	 *
     * @return datetime tre_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tre_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tre_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tre_registration_datetime の値を返します
	 *
     * @return datetime tre_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tre_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tre_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tre_update_timestamp の値を返します
	 *
     * @return timestamp tre_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tre_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tre_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























