<?php
/**
 * @package Mikoshiva_Ftp
 */


/**
 * require
 */
require_once 'Net/FTP.php';


/**
 * FTP クラス
 *
 * Net_FTP のラップクラスです
 *
 * @package Mikoshiva_Ftp
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/18
 * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Ftp {


    /**
     * Net_FTP
     * @var Net_FTP
     */
    protected $_ftp;


    /**
     * 再接続回数
     * @var int 再接続回数
     */
    const RE_CONNECT_COUNT = 3;


    /**
     * コンストラクタ
     *
     * Net_FTP のラップクラスです
     *
     * @param string $host    ホスト名
     * @param int    $port    ポート番号
     * @param int    $timeout タイムリミット
     *
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct($host = null, $port = null, $timeout = 90) {


        // ログ開始
        logInfo(LOG_START);

        // インスタンスを生成
        $this->_ftp = new Net_FTP($host, $port, $timeout);


        // ログ終了
        logInfo(LOG_END);
    }


    /**
     * FTP サーバーへの接続を試みます
     *
     * @param string $host ホスト名
     * @param int    $port ポート番号
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function connect($host = null, $port = null) {


        // ログ開始
        logInfo(LOG_START);


        // 接続
        // true で無ければ規定回数分接続を試みる
        $ret = null;
        for ($i = 0; $i < self::RE_CONNECT_COUNT; $i++) {

            // 接続
            $ret = null;
            $ret = $this->_ftp->connect($host, $port);

            // 接続に成功したらループを抜ける
            if ($ret === true) break;
        }


        // true で無ければ例外を投げる
        if ($ret !== true) {

            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】FTP サーバーへの接続に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * FTP サーバーにログインを試みます
     *
     * @param string $username ログインユーザー名
     * @param string $password ログインパスワード
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function login($username, $password) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = null;
        $_reConnectCount = self::RE_CONNECT_COUNT;
        for ($i = 1; $i <= $_reConnectCount; $i++) {
            //dumper("//-----------------------■{$i}回目■-------------------------------------------");

            $ret = null;
            //------------------------------------------------------------------
            // ▼２回目以降の接続で有れば再接続
            //------------------------------------------------------------------
            if ($i > 1) {
                try {
                    $this->_ftp->connect($this->_ftp->getHostname(), $this->_ftp->getPort());
                } catch (Exception $e) {
                    $this->_ftp->disconnect();
                    throw $e;
                }
            }
            try {
                //------------------------------------------------------------------
                // ▼ログイン
                //------------------------------------------------------------------
                $ret = $this->_ftp->login($username, $password);

                //------------------------------------------------------------------
                // ▼接続に成功したらループを抜ける
                //------------------------------------------------------------------
                if ($ret === true) {
                    if ($i > 1) {
                        $_mailMessage = "（{$i}/{$_reConnectCount}回目）FTPログインに成功しました。";
                        reportMail($_mailMessage, $_mailMessage);
                    }
                    break;
                }
            } catch (Exception $e) {

                //------------------------------------------------------------------
                // ▼接続をクローズ
                //------------------------------------------------------------------
                $this->_ftp->disconnect();

                //------------------------------------------------------------------
                // ▼再接続回数を超えていたら例外を投げる
                //------------------------------------------------------------------
                if ($i === $_reConnectCount) {
                    throw $e;
                }

                //------------------------------------------------------------------
                // ▼リポートメールを送信する
                //------------------------------------------------------------------
                $_mailMessage = "（{$i}/{$_reConnectCount}回目）FTPログインに失敗したため再ログインを試みます";
                errorMail($e, $_mailMessage);
            }
        }

        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * リモートのカレント作業ディレクトリを移動します
     *
     * @param string $dir ディレクトリパス
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function cd($dir) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->cd($dir);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】作業ディレクトリの移動に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * 	リモートのカレントディレクトリのパスを表示します
     *
     * @return string モートのカレントディレクトリのパス
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function pwd() {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->pwd();


        // true で無ければ例外を投げる
        if (is_string($ret) !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】カレントディレクトリの取得に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * 	リモートにディレクトリ作成します
     *
     * @param string $dir       作成したいディレクトリ名
     *                          絶対パス（例. '/home/mydir'）
     *                          相対パス（例. 'mydir'）
     * @param bool   $recursive 指定したディレクトリがない場合に
     *                          再帰的にディレクトリを作成するか
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function mkdir($dir, $recursive = false) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->mkdir($dir, $recursive);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ディレクトリの作成に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * 任意の FTP コマンドを実行します
     *
     * @param string $command コマンド
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function execute($command) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->execute($command);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】”{$command} ”の実行に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * リモートのファイル or ディレクトリの名前を変更します
     *
     *
     * @param string $remote_from リモートのファイル名
     * @param string $remote_to   変更後のファイル名
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function rename($remote_from, $remote_to) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->rename($remote_from, $remote_to);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ファイル名の変更に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * 	リモートのファイルの一覧を表示
     *
     * @param string $dir  一覧を取得したいディレクトリのパス.
     * @param int    $mode 表示モード
     *
     * @return mixed The directory list as described above or PEAR::Error on failure
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function ls($dir = null, $mode = NET_FTP_DIRS_FILES) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->ls($dir, $mode);


        // true で無ければ例外を投げる
        if (! is_array($ret)) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ファイルの一覧の取得に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }

    /**
     * リモートのファイル or ディレクトリの削除を行います
     *
     * @param string $path      削除したいファイル・ディレクトリ
     * @param bool   $recursive 再帰的に削除するか
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function rm($path, $recursive = false) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->rm($path, $recursive);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ファイルの削除に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * ファイルのダウンロードを行います
     *
     * @param string $remote_file ローカルのファイル名
     * @param string $local_file  リモートのファイル名
     * @param bool   $overwrite   上書き
     * @param int    $mode        FTP_ASCII：アスキー|FTP_BINARY：バイナリ
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function get($remote_file, $local_file, $overwrite = false, $mode = null) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->get($remote_file, $local_file, $overwrite, $mode);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ファイルのダウンロードに失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * リモートにファイルをアップロードします
     *
     * @param string $local_file  ローカルのファイル名
     * @param string $remote_file リモートのファイル名
     * @param bool   $overwrite   上書き
     * @param int    $mode        FTP_ASCII：アスキー|FTP_BINARY：バイナリ
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function put($local_file, $remote_file, $overwrite = false, $mode = FTP_BINARY) {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->put($local_file, $remote_file, $overwrite, $mode);


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ファイルのアップロードに失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }


    /**
     * FTP サーバーとの接続を切断します
     *
     * @return boolean true
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/17
     * @version SVN:$Id: Ftp.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function disconnect() {


        // ログ開始
        logInfo(LOG_START);


        // ログイン
        $ret = $this->_ftp->disconnect();


        // true で無ければ例外を投げる
        if ($ret !== true) {
            // dumper($ret);exit;
            include_once 'Mikoshiva/Ftp/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】FTP サーバーの切断に失敗しました。"
                     . $ret->getMessage();
            logDebug($message);
            throw new Mikoshiva_Ftp_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }
}









































