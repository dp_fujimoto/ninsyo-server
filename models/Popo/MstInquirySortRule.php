<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_inquiry_sort_rule
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstInquirySortRule extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mis';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'inquirySortRuleId' => null,
        'sortRuleName' => null,
        'systemFlag' => null,
        'priority' => null,
        'sortType' => null,
        'conditionType' => null,
        'duplicateApplicationFlag' => null,
        'autoDeleteFlag' => null,
        'trashFlag' => null,
        'divisionValue' => null,
        'inquiryType' => null,
        'inquiryLabelId' => null,
        'inquiryStatus' => null,
        'staffId' => null,
        'inquiryAddressId' => null,
        'inquiryTemplateId' => null,
        'inquirySignatureId' => null,
        'inquiryGroupingId1' => null,
        'inquiryGroupingId2' => null,
        'inquiryGroupingId3' => null,
        'transferAddress' => null,
        'autoReturnFlag' => null,
        'autoReturnTemplateId' => null,
        'validFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 振分ルールテーブルID の値を返します
	 *
     * @return int 振分ルールテーブルID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquirySortRuleId() {
		return $this->_properties['inquirySortRuleId'];
    }


    /**
     * 振分ルールテーブルID の値をセットします
	 *
	 * @param int $inquirySortRuleId 振分ルールテーブルID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquirySortRuleId($inquirySortRuleId) {
		$this->_properties['inquirySortRuleId'] = $inquirySortRuleId;
    }


    /**
     * 振分ルール名 の値を返します
	 *
     * @return varchar 振分ルール名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSortRuleName() {
		return $this->_properties['sortRuleName'];
    }


    /**
     * 振分ルール名 の値をセットします
	 *
	 * @param varchar $sortRuleName 振分ルール名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSortRuleName($sortRuleName) {
		$this->_properties['sortRuleName'] = $sortRuleName;
    }


    /**
     * システムフラグ の値を返します
	 *
     * @return char システムフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSystemFlag() {
		return $this->_properties['systemFlag'];
    }


    /**
     * システムフラグ の値をセットします
	 *
	 * @param char $systemFlag システムフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSystemFlag($systemFlag) {
		$this->_properties['systemFlag'] = $systemFlag;
    }


    /**
     * 優先順位 の値を返します
	 *
     * @return int 優先順位
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPriority() {
		return $this->_properties['priority'];
    }


    /**
     * 優先順位 の値をセットします
	 *
	 * @param int $priority 優先順位
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPriority($priority) {
		$this->_properties['priority'] = $priority;
    }


    /**
     * 振分けタイプ の値を返します
	 *
     * @return varchar 振分けタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSortType() {
		return $this->_properties['sortType'];
    }


    /**
     * 振分けタイプ の値をセットします
	 *
	 * @param varchar $sortType 振分けタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSortType($sortType) {
		$this->_properties['sortType'] = $sortType;
    }


    /**
     * 条件タイプ の値を返します
	 *
     * @return varchar 条件タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getConditionType() {
		return $this->_properties['conditionType'];
    }


    /**
     * 条件タイプ の値をセットします
	 *
	 * @param varchar $conditionType 条件タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setConditionType($conditionType) {
		$this->_properties['conditionType'] = $conditionType;
    }


    /**
     * 重複適用ﾌﾗｸﾞ の値を返します
	 *
     * @return char 重複適用ﾌﾗｸﾞ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDuplicateApplicationFlag() {
		return $this->_properties['duplicateApplicationFlag'];
    }


    /**
     * 重複適用ﾌﾗｸﾞ の値をセットします
	 *
	 * @param char $duplicateApplicationFlag 重複適用ﾌﾗｸﾞ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDuplicateApplicationFlag($duplicateApplicationFlag) {
		$this->_properties['duplicateApplicationFlag'] = $duplicateApplicationFlag;
    }


    /**
     * 自動削除ﾌﾗｸﾞ の値を返します
	 *
     * @return char 自動削除ﾌﾗｸﾞ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAutoDeleteFlag() {
		return $this->_properties['autoDeleteFlag'];
    }


    /**
     * 自動削除ﾌﾗｸﾞ の値をセットします
	 *
	 * @param char $autoDeleteFlag 自動削除ﾌﾗｸﾞ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAutoDeleteFlag($autoDeleteFlag) {
		$this->_properties['autoDeleteFlag'] = $autoDeleteFlag;
    }


    /**
     * ゴミ箱フラグ の値を返します
	 *
     * @return char ゴミ箱フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTrashFlag() {
		return $this->_properties['trashFlag'];
    }


    /**
     * ゴミ箱フラグ の値をセットします
	 *
	 * @param char $trashFlag ゴミ箱フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTrashFlag($trashFlag) {
		$this->_properties['trashFlag'] = $trashFlag;
    }


    /**
     * 部署値 の値を返します
	 *
     * @return int 部署値
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionValue() {
		return $this->_properties['divisionValue'];
    }


    /**
     * 部署値 の値をセットします
	 *
	 * @param int $divisionValue 部署値
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionValue($divisionValue) {
		$this->_properties['divisionValue'] = $divisionValue;
    }


    /**
     * 問合せタイプ の値を返します
	 *
     * @return varchar 問合せタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryType() {
		return $this->_properties['inquiryType'];
    }


    /**
     * 問合せタイプ の値をセットします
	 *
	 * @param varchar $inquiryType 問合せタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryType($inquiryType) {
		$this->_properties['inquiryType'] = $inquiryType;
    }


    /**
     * 問合せラベルID の値を返します
	 *
     * @return int 問合せラベルID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryLabelId() {
		return $this->_properties['inquiryLabelId'];
    }


    /**
     * 問合せラベルID の値をセットします
	 *
	 * @param int $inquiryLabelId 問合せラベルID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryLabelId($inquiryLabelId) {
		$this->_properties['inquiryLabelId'] = $inquiryLabelId;
    }


    /**
     * 問合せステータス の値を返します
	 *
     * @return varchar 問合せステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryStatus() {
		return $this->_properties['inquiryStatus'];
    }


    /**
     * 問合せステータス の値をセットします
	 *
	 * @param varchar $inquiryStatus 問合せステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryStatus($inquiryStatus) {
		$this->_properties['inquiryStatus'] = $inquiryStatus;
    }


    /**
     * 担当者ID の値を返します
	 *
     * @return int 担当者ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * 担当者ID の値をセットします
	 *
	 * @param int $staffId 担当者ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * 宛先ID の値を返します
	 *
     * @return int 宛先ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryAddressId() {
		return $this->_properties['inquiryAddressId'];
    }


    /**
     * 宛先ID の値をセットします
	 *
	 * @param int $inquiryAddressId 宛先ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryAddressId($inquiryAddressId) {
		$this->_properties['inquiryAddressId'] = $inquiryAddressId;
    }


    /**
     * 問合せテンプレートID の値を返します
	 *
     * @return int 問合せテンプレートID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryTemplateId() {
		return $this->_properties['inquiryTemplateId'];
    }


    /**
     * 問合せテンプレートID の値をセットします
	 *
	 * @param int $inquiryTemplateId 問合せテンプレートID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryTemplateId($inquiryTemplateId) {
		$this->_properties['inquiryTemplateId'] = $inquiryTemplateId;
    }


    /**
     * 問合せ署名ID の値を返します
	 *
     * @return int 問合せ署名ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquirySignatureId() {
		return $this->_properties['inquirySignatureId'];
    }


    /**
     * 問合せ署名ID の値をセットします
	 *
	 * @param int $inquirySignatureId 問合せ署名ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquirySignatureId($inquirySignatureId) {
		$this->_properties['inquirySignatureId'] = $inquirySignatureId;
    }


    /**
     * 問合せ分類ID１ の値を返します
	 *
     * @return int 問合せ分類ID１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryGroupingId1() {
		return $this->_properties['inquiryGroupingId1'];
    }


    /**
     * 問合せ分類ID１ の値をセットします
	 *
	 * @param int $inquiryGroupingId1 問合せ分類ID１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryGroupingId1($inquiryGroupingId1) {
		$this->_properties['inquiryGroupingId1'] = $inquiryGroupingId1;
    }


    /**
     * 問合せ分類ID２ の値を返します
	 *
     * @return int 問合せ分類ID２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryGroupingId2() {
		return $this->_properties['inquiryGroupingId2'];
    }


    /**
     * 問合せ分類ID２ の値をセットします
	 *
	 * @param int $inquiryGroupingId2 問合せ分類ID２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryGroupingId2($inquiryGroupingId2) {
		$this->_properties['inquiryGroupingId2'] = $inquiryGroupingId2;
    }


    /**
     * 問合せ分類ID３ の値を返します
	 *
     * @return int 問合せ分類ID３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryGroupingId3() {
		return $this->_properties['inquiryGroupingId3'];
    }


    /**
     * 問合せ分類ID３ の値をセットします
	 *
	 * @param int $inquiryGroupingId3 問合せ分類ID３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryGroupingId3($inquiryGroupingId3) {
		$this->_properties['inquiryGroupingId3'] = $inquiryGroupingId3;
    }


    /**
     * 自動転送アドレス の値を返します
	 *
     * @return text 自動転送アドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransferAddress() {
		return $this->_properties['transferAddress'];
    }


    /**
     * 自動転送アドレス の値をセットします
	 *
	 * @param text $transferAddress 自動転送アドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransferAddress($transferAddress) {
		$this->_properties['transferAddress'] = $transferAddress;
    }


    /**
     * 自動返信ﾌﾗｸﾞ の値を返します
	 *
     * @return char 自動返信ﾌﾗｸﾞ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAutoReturnFlag() {
		return $this->_properties['autoReturnFlag'];
    }


    /**
     * 自動返信ﾌﾗｸﾞ の値をセットします
	 *
	 * @param char $autoReturnFlag 自動返信ﾌﾗｸﾞ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAutoReturnFlag($autoReturnFlag) {
		$this->_properties['autoReturnFlag'] = $autoReturnFlag;
    }


    /**
     * 自動返信用問合せテンプレートID の値を返します
	 *
     * @return int 自動返信用問合せテンプレートID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAutoReturnTemplateId() {
		return $this->_properties['autoReturnTemplateId'];
    }


    /**
     * 自動返信用問合せテンプレートID の値をセットします
	 *
	 * @param int $autoReturnTemplateId 自動返信用問合せテンプレートID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAutoReturnTemplateId($autoReturnTemplateId) {
		$this->_properties['autoReturnTemplateId'] = $autoReturnTemplateId;
    }


    /**
     * 有効フラグ の値を返します
	 *
     * @return char 有効フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValidFlag() {
		return $this->_properties['validFlag'];
    }


    /**
     * 有効フラグ の値をセットします
	 *
	 * @param char $validFlag 有効フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValidFlag($validFlag) {
		$this->_properties['validFlag'] = $validFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























