<?php
require_once('DPConfig.php');
class DPLogin{
	
	function DPLogin(){
		
		if($_POST[KEY_ACTION] != 'login'){
			session_start();
			$_SESSION = array();
			if (isset($_COOKIE[session_name()])) {
			    setcookie(session_name(), '', time()-42000, '/');
			}
			session_destroy();
		}
	}
	
	function auth(){
		if($_GET[KEY_MESSAGE]) return $_GET[KEY_MESSAGE];
		if($_POST[KEY_ACTION] != 'login') return;
		
		$email  = $_POST[KEY_EMAIL];
		$passwd = $_POST[KEY_PASSWD];
		
		if(!$email || !$passwd){
			return MSG_LOGIN_FAIL;
		}
		if($passwd === LOGIN_KEY){
			session_start();
			$_SESSION[KEY_EMAIL] = $email;
			header("Location: ".URL_MENU);
		}else{
			return MSG_LOGIN_FAIL;
		}
		return;
	}
}
?>