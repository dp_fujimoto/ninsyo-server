<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Zend/View/Interface.php';
require_once 'Zend/Registry.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';
require_once 'Mikoshiva/View/Helper/FormRadio.php';

 /**
  * ショップ ID のリストを select or radio で表示します
  *
  * @package Mikoshiva_View
  * @author T.Tsukasa <taniguchi@kaihatsu.com>
  * @since 2009/12/16
  * @version SVN:$Id: GmoShopIdLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
  */
class Mikoshiva_View_Helper_GmoShopIdLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;


    /**
     * ショップ ID のリストをselect or radio で表示します
     *
     * @param  string  $name name       属性
     * @param  string  $selected        selected
     * @param  string  $mode            select|radio
     * @param  array   $attribs         追加属性
     * @param  boolean $addShopNameFlag ture|ショップ名を追記
     * @param  boolean $addEmpty        true:先頭行に空項目を作成
     * @param  boolean $addAllItem      true:【全て】の項目を追加
     * @param  string  $listsep         区切文字
     * @return string select or radio
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: GmoShopIdLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function gmoShopIdLister($name,
                                    $selected        = null,
                                    $mode            = 'select',
                                    $attribs         = array(),
                                    $addShopNameFlag = true,
                                    $addEmpty        = false,
                                    $addAllItem      = false,
                                    $listsep         = "<br>\n") {

        // ログスタート
        logInfo(LOG_START);


        // GMO のマッピングデータを取得
        $GMO_CONFIG = array();
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG'); //GMOのマッピングデータを取得


        $shopIdList = array();
        foreach ($GMO_CONFIG as $k => $v) {

            foreach ($v['SHOP'] as $k2 => $v2) {

                // ショップ名を追加する or しない
                if ($addShopNameFlag) $shopIdList[ (string)$k2 ] = $k2 . ' ' . $v2['NAME'];
                else                  $shopIdList[ (string)$k2 ] = $k2;
            }
        }


        //-------------------------------------------
        // mode によって表示を切り替える
        // 不明な mode であれば空文字が返ります
        //-------------------------------------------

        if ( $mode === 'select' ) {


            // ログエンド
            logInfo(LOG_END);
            return $this->view->formSelect($name,
                                           $selected,
                                           $attribs,
                                           $shopIdList,
                                           $addEmpty,
                                           $addAllItem,
                                           $listsep);
        }

        if ( $mode === 'radio' ) {


            // ログエンド
            logInfo(LOG_END);
            return $this->view->formRadio($name,
                                          $selected,
                                          $attribs,
                                          $shopIdList,
                                          $addAllItem,
                                          $listsep);
        }

        logInfo(LOG_END);
        return '';
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
