<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_GreaterThan
 */
require_once 'Zend/Validate/GreaterThan.php';


/**
 *
 *
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: GreaterThan.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_GreaterThan extends Zend_Validate_GreaterThan
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        //self::NOT_GREATER => "【%COLUMN_NAME%】は %min% より大きくありません",
        self::NOT_GREATER => "【%COLUMN_NAME%】は %min% 以上で入力して下さい。",
    );
}
