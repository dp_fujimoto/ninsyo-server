<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_direction_status
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxDirectionStatus extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tds';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'directionStatusId' => null,
        'directionId' => null,
        'divisionId' => null,
        'customerId' => null,
        'statusNumber' => null,
        'campaignProductId' => null,
        'campaignSalesId' => null,
        'purchaseDetailId' => null,
        'memberId' => null,
        'addressNumber' => null,
        'chargeType' => null,
        'proposalDatetime' => null,
        'freePeriod' => null,
        'freePeriodType' => null,
        'freePeriodEndDate' => null,
        'guaranteePeriod' => null,
        'guaranteePeriodType' => null,
        'masterOrderId' => null,
        'masterEntryStatus' => null,
        'firstOrderId' => null,
        'firstAmount' => null,
        'firstShippingAmount' => null,
        'firstDeliveryFee' => null,
        'provisorySalesOrderId' => null,
        'continuityOrderIdLastNumber' => null,
        'continuityAmount' => null,
        'continuityShippingAmount' => null,
        'continuityDeliveryFee' => null,
        'cardDivideNumber' => null,
        'cardDivideType' => null,
        'subscriptionPackageFlag' => null,
        'shippingStartDate' => null,
        'packageShippingNumber' => null,
        'packageShippingPlanNumber' => null,
        'shippingNumber' => null,
        'chargeNumber' => null,
        'directInstallmentFlag' => null,
        'directInstallmentChargeNumber' => null,
        'directInstallmentPrice' => null,
        'debitFlag' => null,
        'activeFlag' => null,
        'midtermCancellationFlag' => null,
        'applicationType' => null,
        'shippingAtOrderFlag' => null,
        'stopFlag' => null,
        'stopDatetime' => null,
        'stopReasonType' => null,
        'subscriptionRestartDate' => null,
        'subscriptionCancelFlag' => null,
        'subscriptionCancelDatetime' => null,
        'singleCancelFlag' => null,
        'singleCancelDatetime' => null,
        'orderCancelFlag' => null,
        'orderCancelDatetime' => null,
        'dummyFlag' => null,
        'suddenlyYearFlag' => null,
        'endlessFlag' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * 指示ID の値を返します
	 *
     * @return int 指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * 指示ID の値をセットします
	 *
	 * @param int $directionId 指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * ステータスNo の値を返します
	 *
     * @return int ステータスNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStatusNumber() {
		return $this->_properties['statusNumber'];
    }


    /**
     * ステータスNo の値をセットします
	 *
	 * @param int $statusNumber ステータスNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStatusNumber($statusNumber) {
		$this->_properties['statusNumber'] = $statusNumber;
    }


    /**
     * キャンペーン商品ID の値を返します
	 *
     * @return int キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductId() {
		return $this->_properties['campaignProductId'];
    }


    /**
     * キャンペーン商品ID の値をセットします
	 *
	 * @param int $campaignProductId キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductId($campaignProductId) {
		$this->_properties['campaignProductId'] = $campaignProductId;
    }


    /**
     * キャンペーンセールスID の値を返します
	 *
     * @return int キャンペーンセールスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesId() {
		return $this->_properties['campaignSalesId'];
    }


    /**
     * キャンペーンセールスID の値をセットします
	 *
	 * @param int $campaignSalesId キャンペーンセールスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesId($campaignSalesId) {
		$this->_properties['campaignSalesId'] = $campaignSalesId;
    }


    /**
     * 購入履歴詳細ID の値を返します
	 *
     * @return int 購入履歴詳細ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseDetailId() {
		return $this->_properties['purchaseDetailId'];
    }


    /**
     * 購入履歴詳細ID の値をセットします
	 *
	 * @param int $purchaseDetailId 購入履歴詳細ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseDetailId($purchaseDetailId) {
		$this->_properties['purchaseDetailId'] = $purchaseDetailId;
    }


    /**
     * 会員ID の値を返します
	 *
     * @return char 会員ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemberId() {
		return $this->_properties['memberId'];
    }


    /**
     * 会員ID の値をセットします
	 *
	 * @param char $memberId 会員ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemberId($memberId) {
		$this->_properties['memberId'] = $memberId;
    }


    /**
     * 発送先No の値を返します
	 *
     * @return int 発送先No
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddressNumber() {
		return $this->_properties['addressNumber'];
    }


    /**
     * 発送先No の値をセットします
	 *
	 * @param int $addressNumber 発送先No
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddressNumber($addressNumber) {
		$this->_properties['addressNumber'] = $addressNumber;
    }


    /**
     * 請求タイプ の値を返します
	 *
     * @return varchar 請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * 請求タイプ の値をセットします
	 *
	 * @param varchar $chargeType 請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * 申込日時 の値を返します
	 *
     * @return datetime 申込日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProposalDatetime() {
		return $this->_properties['proposalDatetime'];
    }


    /**
     * 申込日時 の値をセットします
	 *
	 * @param datetime $proposalDatetime 申込日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProposalDatetime($proposalDatetime) {
		$this->_properties['proposalDatetime'] = $proposalDatetime;
    }


    /**
     * お試し期間 の値を返します
	 *
     * @return int お試し期間
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriod() {
		return $this->_properties['freePeriod'];
    }


    /**
     * お試し期間 の値をセットします
	 *
	 * @param int $freePeriod お試し期間
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriod($freePeriod) {
		$this->_properties['freePeriod'] = $freePeriod;
    }


    /**
     * お試し期間タイプ の値を返します
	 *
     * @return varchar お試し期間タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodType() {
		return $this->_properties['freePeriodType'];
    }


    /**
     * お試し期間タイプ の値をセットします
	 *
	 * @param varchar $freePeriodType お試し期間タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodType($freePeriodType) {
		$this->_properties['freePeriodType'] = $freePeriodType;
    }


    /**
     * お試し期間終了日 の値を返します
	 *
     * @return date お試し期間終了日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodEndDate() {
		return $this->_properties['freePeriodEndDate'];
    }


    /**
     * お試し期間終了日 の値をセットします
	 *
	 * @param date $freePeriodEndDate お試し期間終了日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodEndDate($freePeriodEndDate) {
		$this->_properties['freePeriodEndDate'] = $freePeriodEndDate;
    }


    /**
     * 返金保証期間 の値を返します
	 *
     * @return int 返金保証期間
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriod() {
		return $this->_properties['guaranteePeriod'];
    }


    /**
     * 返金保証期間 の値をセットします
	 *
	 * @param int $guaranteePeriod 返金保証期間
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriod($guaranteePeriod) {
		$this->_properties['guaranteePeriod'] = $guaranteePeriod;
    }


    /**
     * 返金保証期間タイプ の値を返します
	 *
     * @return varchar 返金保証期間タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriodType() {
		return $this->_properties['guaranteePeriodType'];
    }


    /**
     * 返金保証期間タイプ の値をセットします
	 *
	 * @param varchar $guaranteePeriodType 返金保証期間タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriodType($guaranteePeriodType) {
		$this->_properties['guaranteePeriodType'] = $guaranteePeriodType;
    }


    /**
     * マスターオーダーID の値を返します
	 *
     * @return char マスターオーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMasterOrderId() {
		return $this->_properties['masterOrderId'];
    }


    /**
     * マスターオーダーID の値をセットします
	 *
	 * @param char $masterOrderId マスターオーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMasterOrderId($masterOrderId) {
		$this->_properties['masterOrderId'] = $masterOrderId;
    }


    /**
     * マスター取引ステータス の値を返します
	 *
     * @return varchar マスター取引ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMasterEntryStatus() {
		return $this->_properties['masterEntryStatus'];
    }


    /**
     * マスター取引ステータス の値をセットします
	 *
	 * @param varchar $masterEntryStatus マスター取引ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMasterEntryStatus($masterEntryStatus) {
		$this->_properties['masterEntryStatus'] = $masterEntryStatus;
    }


    /**
     * 初回オーダーID の値を返します
	 *
     * @return char 初回オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstOrderId() {
		return $this->_properties['firstOrderId'];
    }


    /**
     * 初回オーダーID の値をセットします
	 *
	 * @param char $firstOrderId 初回オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstOrderId($firstOrderId) {
		$this->_properties['firstOrderId'] = $firstOrderId;
    }


    /**
     * 初回商品金額 の値を返します
	 *
     * @return int 初回商品金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstAmount() {
		return $this->_properties['firstAmount'];
    }


    /**
     * 初回商品金額 の値をセットします
	 *
	 * @param int $firstAmount 初回商品金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstAmount($firstAmount) {
		$this->_properties['firstAmount'] = $firstAmount;
    }


    /**
     * 初回発送金額 の値を返します
	 *
     * @return int 初回発送金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstShippingAmount() {
		return $this->_properties['firstShippingAmount'];
    }


    /**
     * 初回発送金額 の値をセットします
	 *
	 * @param int $firstShippingAmount 初回発送金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstShippingAmount($firstShippingAmount) {
		$this->_properties['firstShippingAmount'] = $firstShippingAmount;
    }


    /**
     * 初回代引手数料 の値を返します
	 *
     * @return int 初回代引手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstDeliveryFee() {
		return $this->_properties['firstDeliveryFee'];
    }


    /**
     * 初回代引手数料 の値をセットします
	 *
	 * @param int $firstDeliveryFee 初回代引手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstDeliveryFee($firstDeliveryFee) {
		$this->_properties['firstDeliveryFee'] = $firstDeliveryFee;
    }


    /**
     * 仮売上オーダーID の値を返します
	 *
     * @return char 仮売上オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesOrderId() {
		return $this->_properties['provisorySalesOrderId'];
    }


    /**
     * 仮売上オーダーID の値をセットします
	 *
	 * @param char $provisorySalesOrderId 仮売上オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesOrderId($provisorySalesOrderId) {
		$this->_properties['provisorySalesOrderId'] = $provisorySalesOrderId;
    }


    /**
     * 継続オーダーID末尾番号 の値を返します
	 *
     * @return varchar 継続オーダーID末尾番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityOrderIdLastNumber() {
		return $this->_properties['continuityOrderIdLastNumber'];
    }


    /**
     * 継続オーダーID末尾番号 の値をセットします
	 *
	 * @param varchar $continuityOrderIdLastNumber 継続オーダーID末尾番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityOrderIdLastNumber($continuityOrderIdLastNumber) {
		$this->_properties['continuityOrderIdLastNumber'] = $continuityOrderIdLastNumber;
    }


    /**
     * 継続金額 の値を返します
	 *
     * @return int 継続金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityAmount() {
		return $this->_properties['continuityAmount'];
    }


    /**
     * 継続金額 の値をセットします
	 *
	 * @param int $continuityAmount 継続金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityAmount($continuityAmount) {
		$this->_properties['continuityAmount'] = $continuityAmount;
    }


    /**
     * 継続発送金額 の値を返します
	 *
     * @return int 継続発送金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityShippingAmount() {
		return $this->_properties['continuityShippingAmount'];
    }


    /**
     * 継続発送金額 の値をセットします
	 *
	 * @param int $continuityShippingAmount 継続発送金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityShippingAmount($continuityShippingAmount) {
		$this->_properties['continuityShippingAmount'] = $continuityShippingAmount;
    }


    /**
     * 継続代引手数料 の値を返します
	 *
     * @return int 継続代引手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityDeliveryFee() {
		return $this->_properties['continuityDeliveryFee'];
    }


    /**
     * 継続代引手数料 の値をセットします
	 *
	 * @param int $continuityDeliveryFee 継続代引手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityDeliveryFee($continuityDeliveryFee) {
		$this->_properties['continuityDeliveryFee'] = $continuityDeliveryFee;
    }


    /**
     * カード分割回数 の値を返します
	 *
     * @return int カード分割回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCardDivideNumber() {
		return $this->_properties['cardDivideNumber'];
    }


    /**
     * カード分割回数 の値をセットします
	 *
	 * @param int $cardDivideNumber カード分割回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCardDivideNumber($cardDivideNumber) {
		$this->_properties['cardDivideNumber'] = $cardDivideNumber;
    }


    /**
     * カード分割タイプ の値を返します
	 *
     * @return varchar カード分割タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCardDivideType() {
		return $this->_properties['cardDivideType'];
    }


    /**
     * カード分割タイプ の値をセットします
	 *
	 * @param varchar $cardDivideType カード分割タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCardDivideType($cardDivideType) {
		$this->_properties['cardDivideType'] = $cardDivideType;
    }


    /**
     * 一括購読フラグ の値を返します
	 *
     * @return char 一括購読フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionPackageFlag() {
		return $this->_properties['subscriptionPackageFlag'];
    }


    /**
     * 一括購読フラグ の値をセットします
	 *
	 * @param char $subscriptionPackageFlag 一括購読フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionPackageFlag($subscriptionPackageFlag) {
		$this->_properties['subscriptionPackageFlag'] = $subscriptionPackageFlag;
    }


    /**
     * 発送開始日 の値を返します
	 *
     * @return date 発送開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStartDate() {
		return $this->_properties['shippingStartDate'];
    }


    /**
     * 発送開始日 の値をセットします
	 *
	 * @param date $shippingStartDate 発送開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStartDate($shippingStartDate) {
		$this->_properties['shippingStartDate'] = $shippingStartDate;
    }


    /**
     * 購読発送回数 の値を返します
	 *
     * @return int 購読発送回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageShippingNumber() {
		return $this->_properties['packageShippingNumber'];
    }


    /**
     * 購読発送回数 の値をセットします
	 *
	 * @param int $packageShippingNumber 購読発送回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageShippingNumber($packageShippingNumber) {
		$this->_properties['packageShippingNumber'] = $packageShippingNumber;
    }


    /**
     * 購読発送予定回数 の値を返します
	 *
     * @return int 購読発送予定回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageShippingPlanNumber() {
		return $this->_properties['packageShippingPlanNumber'];
    }


    /**
     * 購読発送予定回数 の値をセットします
	 *
	 * @param int $packageShippingPlanNumber 購読発送予定回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageShippingPlanNumber($packageShippingPlanNumber) {
		$this->_properties['packageShippingPlanNumber'] = $packageShippingPlanNumber;
    }


    /**
     * 発送回数 の値を返します
	 *
     * @return int 発送回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingNumber() {
		return $this->_properties['shippingNumber'];
    }


    /**
     * 発送回数 の値をセットします
	 *
	 * @param int $shippingNumber 発送回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingNumber($shippingNumber) {
		$this->_properties['shippingNumber'] = $shippingNumber;
    }


    /**
     * 請求回数 の値を返します
	 *
     * @return int 請求回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeNumber() {
		return $this->_properties['chargeNumber'];
    }


    /**
     * 請求回数 の値をセットします
	 *
	 * @param int $chargeNumber 請求回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeNumber($chargeNumber) {
		$this->_properties['chargeNumber'] = $chargeNumber;
    }


    /**
     * ダイレクト分割フラグ の値を返します
	 *
     * @return char ダイレクト分割フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentFlag() {
		return $this->_properties['directInstallmentFlag'];
    }


    /**
     * ダイレクト分割フラグ の値をセットします
	 *
	 * @param char $directInstallmentFlag ダイレクト分割フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentFlag($directInstallmentFlag) {
		$this->_properties['directInstallmentFlag'] = $directInstallmentFlag;
    }


    /**
     * ダイレクト分割請求回数 の値を返します
	 *
     * @return int ダイレクト分割請求回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentChargeNumber() {
		return $this->_properties['directInstallmentChargeNumber'];
    }


    /**
     * ダイレクト分割請求回数 の値をセットします
	 *
	 * @param int $directInstallmentChargeNumber ダイレクト分割請求回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentChargeNumber($directInstallmentChargeNumber) {
		$this->_properties['directInstallmentChargeNumber'] = $directInstallmentChargeNumber;
    }


    /**
     * ダイレクト分割初回金額 の値を返します
	 *
     * @return int ダイレクト分割初回金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPrice() {
		return $this->_properties['directInstallmentPrice'];
    }


    /**
     * ダイレクト分割初回金額 の値をセットします
	 *
	 * @param int $directInstallmentPrice ダイレクト分割初回金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPrice($directInstallmentPrice) {
		$this->_properties['directInstallmentPrice'] = $directInstallmentPrice;
    }


    /**
     * デビットフラグ の値を返します
	 *
     * @return char デビットフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDebitFlag() {
		return $this->_properties['debitFlag'];
    }


    /**
     * デビットフラグ の値をセットします
	 *
	 * @param char $debitFlag デビットフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDebitFlag($debitFlag) {
		$this->_properties['debitFlag'] = $debitFlag;
    }


    /**
     * アクティブフラグ の値を返します
	 *
     * @return char アクティブフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getActiveFlag() {
		return $this->_properties['activeFlag'];
    }


    /**
     * アクティブフラグ の値をセットします
	 *
	 * @param char $activeFlag アクティブフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setActiveFlag($activeFlag) {
		$this->_properties['activeFlag'] = $activeFlag;
    }


    /**
     * 途中解約フラグ の値を返します
	 *
     * @return char 途中解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMidtermCancellationFlag() {
		return $this->_properties['midtermCancellationFlag'];
    }


    /**
     * 途中解約フラグ の値をセットします
	 *
	 * @param char $midtermCancellationFlag 途中解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMidtermCancellationFlag($midtermCancellationFlag) {
		$this->_properties['midtermCancellationFlag'] = $midtermCancellationFlag;
    }


    /**
     * 申込方法 の値を返します
	 *
     * @return varchar 申込方法
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApplicationType() {
		return $this->_properties['applicationType'];
    }


    /**
     * 申込方法 の値をセットします
	 *
	 * @param varchar $applicationType 申込方法
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApplicationType($applicationType) {
		$this->_properties['applicationType'] = $applicationType;
    }


    /**
     * 購入時発送フラグ の値を返します
	 *
     * @return char 購入時発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAtOrderFlag() {
		return $this->_properties['shippingAtOrderFlag'];
    }


    /**
     * 購入時発送フラグ の値をセットします
	 *
	 * @param char $shippingAtOrderFlag 購入時発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAtOrderFlag($shippingAtOrderFlag) {
		$this->_properties['shippingAtOrderFlag'] = $shippingAtOrderFlag;
    }


    /**
     * 停止フラグ の値を返します
	 *
     * @return char 停止フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopFlag() {
		return $this->_properties['stopFlag'];
    }


    /**
     * 停止フラグ の値をセットします
	 *
	 * @param char $stopFlag 停止フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopFlag($stopFlag) {
		$this->_properties['stopFlag'] = $stopFlag;
    }


    /**
     * 停止日時 の値を返します
	 *
     * @return datetime 停止日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopDatetime() {
		return $this->_properties['stopDatetime'];
    }


    /**
     * 停止日時 の値をセットします
	 *
	 * @param datetime $stopDatetime 停止日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopDatetime($stopDatetime) {
		$this->_properties['stopDatetime'] = $stopDatetime;
    }


    /**
     * 停止理由 の値を返します
	 *
     * @return varchar 停止理由
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopReasonType() {
		return $this->_properties['stopReasonType'];
    }


    /**
     * 停止理由 の値をセットします
	 *
	 * @param varchar $stopReasonType 停止理由
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopReasonType($stopReasonType) {
		$this->_properties['stopReasonType'] = $stopReasonType;
    }


    /**
     * 購読再開予定日 の値を返します
	 *
     * @return date 購読再開予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionRestartDate() {
		return $this->_properties['subscriptionRestartDate'];
    }


    /**
     * 購読再開予定日 の値をセットします
	 *
	 * @param date $subscriptionRestartDate 購読再開予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionRestartDate($subscriptionRestartDate) {
		$this->_properties['subscriptionRestartDate'] = $subscriptionRestartDate;
    }


    /**
     * 購読解約フラグ の値を返します
	 *
     * @return char 購読解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionCancelFlag() {
		return $this->_properties['subscriptionCancelFlag'];
    }


    /**
     * 購読解約フラグ の値をセットします
	 *
	 * @param char $subscriptionCancelFlag 購読解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionCancelFlag($subscriptionCancelFlag) {
		$this->_properties['subscriptionCancelFlag'] = $subscriptionCancelFlag;
    }


    /**
     * 購読解約日時 の値を返します
	 *
     * @return datetime 購読解約日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionCancelDatetime() {
		return $this->_properties['subscriptionCancelDatetime'];
    }


    /**
     * 購読解約日時 の値をセットします
	 *
	 * @param datetime $subscriptionCancelDatetime 購読解約日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionCancelDatetime($subscriptionCancelDatetime) {
		$this->_properties['subscriptionCancelDatetime'] = $subscriptionCancelDatetime;
    }


    /**
     * 単発解約フラグ の値を返します
	 *
     * @return char 単発解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSingleCancelFlag() {
		return $this->_properties['singleCancelFlag'];
    }


    /**
     * 単発解約フラグ の値をセットします
	 *
	 * @param char $singleCancelFlag 単発解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSingleCancelFlag($singleCancelFlag) {
		$this->_properties['singleCancelFlag'] = $singleCancelFlag;
    }


    /**
     * 単発解約日時 の値を返します
	 *
     * @return datetime 単発解約日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSingleCancelDatetime() {
		return $this->_properties['singleCancelDatetime'];
    }


    /**
     * 単発解約日時 の値をセットします
	 *
	 * @param datetime $singleCancelDatetime 単発解約日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSingleCancelDatetime($singleCancelDatetime) {
		$this->_properties['singleCancelDatetime'] = $singleCancelDatetime;
    }


    /**
     * 注文取消フラグ の値を返します
	 *
     * @return char 注文取消フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderCancelFlag() {
		return $this->_properties['orderCancelFlag'];
    }


    /**
     * 注文取消フラグ の値をセットします
	 *
	 * @param char $orderCancelFlag 注文取消フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderCancelFlag($orderCancelFlag) {
		$this->_properties['orderCancelFlag'] = $orderCancelFlag;
    }


    /**
     * 注文取消日時 の値を返します
	 *
     * @return datetime 注文取消日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderCancelDatetime() {
		return $this->_properties['orderCancelDatetime'];
    }


    /**
     * 注文取消日時 の値をセットします
	 *
	 * @param datetime $orderCancelDatetime 注文取消日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderCancelDatetime($orderCancelDatetime) {
		$this->_properties['orderCancelDatetime'] = $orderCancelDatetime;
    }


    /**
     * tds_dummy_flag の値を返します
	 *
     * @return char tds_dummy_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDummyFlag() {
		return $this->_properties['dummyFlag'];
    }


    /**
     * tds_dummy_flag の値をセットします
	 *
	 * @param char $dummyFlag tds_dummy_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDummyFlag($dummyFlag) {
		$this->_properties['dummyFlag'] = $dummyFlag;
    }


    /**
     * tds_suddenly_year_flag の値を返します
	 *
     * @return char tds_suddenly_year_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSuddenlyYearFlag() {
		return $this->_properties['suddenlyYearFlag'];
    }


    /**
     * tds_suddenly_year_flag の値をセットします
	 *
	 * @param char $suddenlyYearFlag tds_suddenly_year_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSuddenlyYearFlag($suddenlyYearFlag) {
		$this->_properties['suddenlyYearFlag'] = $suddenlyYearFlag;
    }


    /**
     * tds_endless_flag の値を返します
	 *
     * @return char tds_endless_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEndlessFlag() {
		return $this->_properties['endlessFlag'];
    }


    /**
     * tds_endless_flag の値をセットします
	 *
	 * @param char $endlessFlag tds_endless_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEndlessFlag($endlessFlag) {
		$this->_properties['endlessFlag'] = $endlessFlag;
    }


    /**
     * 欠陥フラグ の値を返します
	 *
     * @return char 欠陥フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * 欠陥フラグ の値をセットします
	 *
	 * @param char $imperfectionFlag 欠陥フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























