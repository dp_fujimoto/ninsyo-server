<?php
/** @package Mikoshiva_Settlement */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Input_Exec_Run
 *
 * @package Mikoshiva_Settlement
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Run extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'AccessID' => null,
        'AccessPass' => null,
        'OrderID' => null,
        'Method' => null,
        'PayTimes' => null,
        'SiteID' => null,
        'SitePass' => null,
        'MemberID' => null,
        'SeqMode' => null,
        'CardSeq' => null,
        'CardPass' => null,
        'ClientField1' => null,
        'ClientField2' => null,
        'ClientField3' => null,
        'ClientFieldFlag' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * AccessID
	 *
     * @return  AccessID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessID() {
		return $this->_properties['AccessID'];
    }


    /**
     * AccessID
	 *
	 * @param  $AccessID AccessID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessID($AccessID) {
		$this->_properties['AccessID'] = $AccessID;
    }


    /**
     * AccessPass
	 *
     * @return  AccessPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessPass() {
		return $this->_properties['AccessPass'];
    }


    /**
     * AccessPass
	 *
	 * @param  $AccessPass AccessPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessPass($AccessPass) {
		$this->_properties['AccessPass'] = $AccessPass;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * Method
	 *
     * @return  Method
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMethod() {
		return $this->_properties['Method'];
    }


    /**
     * Method
	 *
	 * @param  $Method Method
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMethod($Method) {
		$this->_properties['Method'] = $Method;
    }


    /**
     * PayTimes
	 *
     * @return  PayTimes
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getPayTimes() {
		return $this->_properties['PayTimes'];
    }


    /**
     * PayTimes
	 *
	 * @param  $PayTimes PayTimes
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setPayTimes($PayTimes) {
		$this->_properties['PayTimes'] = $PayTimes;
    }


    /**
     * SiteID
	 *
     * @return  SiteID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSiteID() {
		return $this->_properties['SiteID'];
    }


    /**
     * SiteID
	 *
	 * @param  $SiteID SiteID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSiteID($SiteID) {
		$this->_properties['SiteID'] = $SiteID;
    }


    /**
     * SitePass
	 *
     * @return  SitePass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSitePass() {
		return $this->_properties['SitePass'];
    }


    /**
     * SitePass
	 *
	 * @param  $SitePass SitePass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSitePass($SitePass) {
		$this->_properties['SitePass'] = $SitePass;
    }


    /**
     * MemberID
	 *
     * @return  MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMemberID() {
		return $this->_properties['MemberID'];
    }


    /**
     * MemberID
	 *
	 * @param  $MemberID MemberID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMemberID($MemberID) {
		$this->_properties['MemberID'] = $MemberID;
    }


    /**
     * SeqMode
	 *
     * @return  SeqMode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSeqMode() {
		return $this->_properties['SeqMode'];
    }


    /**
     * SeqMode
	 *
	 * @param  $SeqMode SeqMode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSeqMode($SeqMode) {
		$this->_properties['SeqMode'] = $SeqMode;
    }


    /**
     * CardSeq
	 *
     * @return  CardSeq
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardSeq() {
		return $this->_properties['CardSeq'];
    }


    /**
     * CardSeq
	 *
	 * @param  $CardSeq CardSeq
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardSeq($CardSeq) {
		$this->_properties['CardSeq'] = $CardSeq;
    }


    /**
     * CardPass
	 *
     * @return  CardPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardPass() {
		return $this->_properties['CardPass'];
    }


    /**
     * CardPass
	 *
	 * @param  $CardPass CardPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardPass($CardPass) {
		$this->_properties['CardPass'] = $CardPass;
    }


    /**
     * ClientField1
	 *
     * @return  ClientField1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientField1() {
		return $this->_properties['ClientField1'];
    }


    /**
     * ClientField1
	 *
	 * @param  $ClientField1 ClientField1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientField1($ClientField1) {
		$this->_properties['ClientField1'] = $ClientField1;
    }


    /**
     * ClientField2
	 *
     * @return  ClientField2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientField2() {
		return $this->_properties['ClientField2'];
    }


    /**
     * ClientField2
	 *
	 * @param  $ClientField2 ClientField2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientField2($ClientField2) {
		$this->_properties['ClientField2'] = $ClientField2;
    }


    /**
     * ClientField3
	 *
     * @return  ClientField3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientField3() {
		return $this->_properties['ClientField3'];
    }


    /**
     * ClientField3
	 *
	 * @param  $ClientField3 ClientField3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientField3($ClientField3) {
		$this->_properties['ClientField3'] = $ClientField3;
    }


    /**
     * ClientFieldFlag
	 *
     * @return  ClientFieldFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientFieldFlag() {
		return $this->_properties['ClientFieldFlag'];
    }


    /**
     * ClientFieldFlag
	 *
	 * @param  $ClientFieldFlag ClientFieldFlag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientFieldFlag($ClientFieldFlag) {
		$this->_properties['ClientFieldFlag'] = $ClientFieldFlag;
    }


}

























