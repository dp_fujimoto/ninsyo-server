<?php
/**
 * @package Mikoshiva_Utility
 */


/**
 * POPO 関連ユーティリティ
 *
 * @package Mikoshiva_Utility
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/08
 * @version SVN:$Id: Popo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Popo {


    /**
     *
     *
     * @param $number
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/10
     * @version SVN:$Id: Popo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function createPopoList(array $dataList) {

        include_once 'Zend/Registry.php';
        include_once 'Mikoshiva/Utility/String.php';
        include_once 'Mikoshiva/Utility/Array.php';
        include_once 'Mikoshiva/Utility/Copy.php';

        $popoList = array();
        foreach ($dataList as $columnName => $value) {

            $prefixName = '';
            $prefixName = Mikoshiva_Utility_String::getColumnPrefix($columnName);

            $popoList[$prefixName] = null;
        }


        $COLUMN_MAPPING_POPO_NAME_LIST = Zend_Registry::get('COLUMN_MAPPING_POPO_NAME_LIST');
        foreach ($popoList as $k => $v) {

            $tableDataList = array();
            $tableDataList = Mikoshiva_Utility_Array::extractBeginningOfPrefix($k, $dataList);

            $popoClassName = '';
            $popoClassName = $COLUMN_MAPPING_POPO_NAME_LIST[$k];

            $popoClassFilePath = '';
            $popoClassFilePath = str_replace('_', '/', $popoClassName) . '.php';
            include_once $popoClassFilePath;

            $popoList[$k] = Mikoshiva_Utility_Copy::copyDbProperties(new $popoClassName(), $tableDataList);
        }

        return $popoList;
    }
}










