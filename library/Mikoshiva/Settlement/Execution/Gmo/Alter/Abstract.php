<?php
/** @package Mikoshiva_Settlement */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';

/**
 * Mikoshiva_Settlement_Gmo_Execution_Alter_Abstract
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Settlement_Execution_Gmo_Alter_Abstract extends Mikoshiva_Settlement_Execution_Gmo_Abstract {


    /**
     * 処理区分 を保持します（必須）
     * @var string 処理区分 を保持します（必須）
     */
    protected $_jobCd;


    /**
     * 初期化処理
     *
     * @param Popo_Abstract $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Alter */
        $inputPopo;


        // 固定値を POPO に詰める
        $inputPopo->setShopPass($gmoConfig['SHOP'][$inputPopo->getShopID()]['SHOP_PASS']); // ショップパスワード
        $inputPopo->setJobCd($this->_jobCd);                                               // ジョブコード


        // GMO 処理結果 POPO クラス
        include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Alter.php';
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Alter();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['SHOP'][$inputPopo->getShopID()]['ALTER_PGM'];

        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }




    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Execution/Mikoshiva_Settlement_Gmo_Execution_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Exception.php';


        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Alter */
        $inputPopo;


        // ショップ ID
        if (isBlankOrNull($inputPopo->getShopID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ショップ ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }


        // ショップパスワード
        if (isBlankOrNull($inputPopo->getShopPass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ショップパスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }


        // 決済データへのアクセス ID
        if (isBlankOrNull($inputPopo->getAccessID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }


        // 決済データへのアクセスパスワード
        if (isBlankOrNull($inputPopo->getAccessPass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引パスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }


        // 処理区分
        if (isBlankOrNull($inputPopo->getJobCd())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】処理区分 は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }


        // 利用金額（仮売上 → 実売上の場合のみチェック）
        if( $this->_jobCd === 'SALES') {
            if (isBlankOrNull($inputPopo->getAmount())) {
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】仮売上　→　実売上処理に利用金額は必須項目です。";
                logDebug($message);
                throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
            }
        }

        return true;
    }


    /**
     * 決済後に GMO 取引履歴テーブルに決済結果のデータの登録を行います
     *
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#execute()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/05/12
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    /*
    public function execute() {


        // 処理結果が成功で有れば DB に登録を行う
        $ret = parent::execute();


        // 処理失敗で有れば return
        if($ret === false) return $ret;


        //------------------------------------------------------------
        // ■GMO 取引履歴テーブル（mst_gmo_transaction_history）から該当の返金対象の決済データを取得する
        //------------------------------------------------------------
        // table
        include_once 'Mikoshiva/Db/Ar/MstGmoTransactionHistory.php';
        $tableClass = null;
        $tableClass = new Mikoshiva_Db_Ar_MstGmoTransactionHistory();

        // select
        $selectClass = null;
        $selectClass = $tableClass->select();

        // where
        $selectClass->where('mgh_shop_id = ?', $this->_inputPopo->getShopId());
        $selectClass->where('mgh_access_id = ?', $this->_inputPopo->getAccessId());
        $selectClass->where('mgh_access_pass = ?', $this->_inputPopo->getAccessPass());
        $selectClass->order('mgh_tran_date DESC');
        $selectClass->limit(1);
echo $selectClass;exit;
        // 実行
        $rows = $tableClass->fetchAll($selectClass)->toArray();
        $row = $rows[0];


        //------------------------------------------------------------
        // ■ 上で取得したデータを元に GMO 取引履歴マスタに履歴を登録
        //------------------------------------------------------------
        // 新規登録のため、IDを削除
        unset($row['mgh_gmo_transaction_history_id']);

        // 登録データ作成
        include_once 'Popo/MstGmoTransactionHistory.php';
        $popo = new Popo_MstGmoTransactionHistory();
        $popo = Mikoshiva_Utility_Copy::copyDbProperties($popo, $row);
        $popo->setTranDate($this->_outputPopo->getTranDate());
        $popo->setEntryStatus('ENTRY_STATUS_' . $this->_jobCd);

        // 実行
        include_once 'Mikoshiva/Db/Simple.php';
        $simple = new Mikoshiva_Db_Simple();
        $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $popo);


        // 決済結果を返却
        return $ret;

    }        */
}












