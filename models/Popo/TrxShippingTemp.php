<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_shipping_temp
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxShippingTemp extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tst';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'shippingTempId' => null,
        'shippingId' => null,
        'customerId' => null,
        'directionId' => null,
        'directionStatusId' => null,
        'chargeId' => null,
        'divisionId' => null,
        'relativeShippingId' => null,
        'campaignSalesId' => null,
        'campaignId' => null,
        'productShippingId' => null,
        'productPackageId' => null,
        'packageNumber' => null,
        'packageType' => null,
        'chargeType' => null,
        'stepType' => null,
        'stepCount' => null,
        'shippingGroupLabel' => null,
        'entityShippingFlag' => null,
        'companyName' => null,
        'lastName' => null,
        'firstName' => null,
        'lastNameKana' => null,
        'firstNameKana' => null,
        'zip' => null,
        'prefecture' => null,
        'address1' => null,
        'address2' => null,
        'tel' => null,
        'shippingType' => null,
        'expectedShippingDate' => null,
        'paymentDeliveryPrice' => null,
        'shippingAssign' => null,
        'builderId1' => null,
        'builderId2' => null,
        'builderId3' => null,
        'builderId4' => null,
        'builderId5' => null,
        'shippingStatus' => null,
        'shippingStopStatus' => null,
        'supportStatus' => null,
        'returnStatus' => null,
        'returnSupportStatus' => null,
        'returnReasonType' => null,
        'returnRequestDate' => null,
        'returnCompleteDate' => null,
        'returnLimitDate' => null,
        'responaseLimitDate' => null,
        'cancelAfterShippingDate' => null,
        'realShippingDate' => null,
        'inquiryNumber' => null,
        'refundPossibilityDate' => null,
        'shippingCompleteFlag' => null,
        'imperfectionFlag' => null,
        'temporaryInvoiceId' => null,
        'invoiceId' => null,
        'packagingDirectionCode' => null,
        'packagingDirectionNumber' => null,
        'realShippingType' => null,
        'reshippingFlag' => null,
        'shippingSlipCreateFlag' => null,
        'shippingStopFlag' => null,
        'proposalDatetime' => null,
        'campaignSalesCode' => null,
        'campaignCode' => null,
        'productShippingCode' => null,
        'productPackageCode' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tst_shipping_temp_id の値を返します
	 *
     * @return int tst_shipping_temp_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingTempId() {
		return $this->_properties['shippingTempId'];
    }


    /**
     * tst_shipping_temp_id の値をセットします
	 *
	 * @param int $shippingTempId tst_shipping_temp_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingTempId($shippingTempId) {
		$this->_properties['shippingTempId'] = $shippingTempId;
    }


    /**
     * tst_shipping_id の値を返します
	 *
     * @return int tst_shipping_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * tst_shipping_id の値をセットします
	 *
	 * @param int $shippingId tst_shipping_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * tst_customer_id の値を返します
	 *
     * @return int tst_customer_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * tst_customer_id の値をセットします
	 *
	 * @param int $customerId tst_customer_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * tst_direction_id の値を返します
	 *
     * @return int tst_direction_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * tst_direction_id の値をセットします
	 *
	 * @param int $directionId tst_direction_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * tst_direction_status_id の値を返します
	 *
     * @return int tst_direction_status_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * tst_direction_status_id の値をセットします
	 *
	 * @param int $directionStatusId tst_direction_status_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * tst_charge_id の値を返します
	 *
     * @return int tst_charge_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * tst_charge_id の値をセットします
	 *
	 * @param int $chargeId tst_charge_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * tst_division_id の値を返します
	 *
     * @return int tst_division_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * tst_division_id の値をセットします
	 *
	 * @param int $divisionId tst_division_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * tst_relative_shipping_id の値を返します
	 *
     * @return int tst_relative_shipping_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRelativeShippingId() {
		return $this->_properties['relativeShippingId'];
    }


    /**
     * tst_relative_shipping_id の値をセットします
	 *
	 * @param int $relativeShippingId tst_relative_shipping_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRelativeShippingId($relativeShippingId) {
		$this->_properties['relativeShippingId'] = $relativeShippingId;
    }


    /**
     * tst_campaign_sales_id の値を返します
	 *
     * @return int tst_campaign_sales_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesId() {
		return $this->_properties['campaignSalesId'];
    }


    /**
     * tst_campaign_sales_id の値をセットします
	 *
	 * @param int $campaignSalesId tst_campaign_sales_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesId($campaignSalesId) {
		$this->_properties['campaignSalesId'] = $campaignSalesId;
    }


    /**
     * tst_campaign_id の値を返します
	 *
     * @return int tst_campaign_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignId() {
		return $this->_properties['campaignId'];
    }


    /**
     * tst_campaign_id の値をセットします
	 *
	 * @param int $campaignId tst_campaign_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignId($campaignId) {
		$this->_properties['campaignId'] = $campaignId;
    }


    /**
     * tst_product_shipping_id の値を返します
	 *
     * @return int tst_product_shipping_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingId() {
		return $this->_properties['productShippingId'];
    }


    /**
     * tst_product_shipping_id の値をセットします
	 *
	 * @param int $productShippingId tst_product_shipping_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingId($productShippingId) {
		$this->_properties['productShippingId'] = $productShippingId;
    }


    /**
     * tst_product_package_id の値を返します
	 *
     * @return int tst_product_package_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductPackageId() {
		return $this->_properties['productPackageId'];
    }


    /**
     * tst_product_package_id の値をセットします
	 *
	 * @param int $productPackageId tst_product_package_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductPackageId($productPackageId) {
		$this->_properties['productPackageId'] = $productPackageId;
    }


    /**
     * tst_package_number の値を返します
	 *
     * @return int tst_package_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageNumber() {
		return $this->_properties['packageNumber'];
    }


    /**
     * tst_package_number の値をセットします
	 *
	 * @param int $packageNumber tst_package_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageNumber($packageNumber) {
		$this->_properties['packageNumber'] = $packageNumber;
    }


    /**
     * tst_package_type の値を返します
	 *
     * @return varchar tst_package_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageType() {
		return $this->_properties['packageType'];
    }


    /**
     * tst_package_type の値をセットします
	 *
	 * @param varchar $packageType tst_package_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageType($packageType) {
		$this->_properties['packageType'] = $packageType;
    }


    /**
     * tst_charge_type の値を返します
	 *
     * @return varchar tst_charge_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * tst_charge_type の値をセットします
	 *
	 * @param varchar $chargeType tst_charge_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * tst_step_type の値を返します
	 *
     * @return varchar tst_step_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepType() {
		return $this->_properties['stepType'];
    }


    /**
     * tst_step_type の値をセットします
	 *
	 * @param varchar $stepType tst_step_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepType($stepType) {
		$this->_properties['stepType'] = $stepType;
    }


    /**
     * tst_step_count の値を返します
	 *
     * @return int tst_step_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepCount() {
		return $this->_properties['stepCount'];
    }


    /**
     * tst_step_count の値をセットします
	 *
	 * @param int $stepCount tst_step_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepCount($stepCount) {
		$this->_properties['stepCount'] = $stepCount;
    }


    /**
     * tst_shipping_group_label の値を返します
	 *
     * @return varchar tst_shipping_group_label
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingGroupLabel() {
		return $this->_properties['shippingGroupLabel'];
    }


    /**
     * tst_shipping_group_label の値をセットします
	 *
	 * @param varchar $shippingGroupLabel tst_shipping_group_label
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingGroupLabel($shippingGroupLabel) {
		$this->_properties['shippingGroupLabel'] = $shippingGroupLabel;
    }


    /**
     * tst_entity_shipping_flag の値を返します
	 *
     * @return char tst_entity_shipping_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEntityShippingFlag() {
		return $this->_properties['entityShippingFlag'];
    }


    /**
     * tst_entity_shipping_flag の値をセットします
	 *
	 * @param char $entityShippingFlag tst_entity_shipping_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEntityShippingFlag($entityShippingFlag) {
		$this->_properties['entityShippingFlag'] = $entityShippingFlag;
    }


    /**
     * tst_company_name の値を返します
	 *
     * @return varchar tst_company_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompanyName() {
		return $this->_properties['companyName'];
    }


    /**
     * tst_company_name の値をセットします
	 *
	 * @param varchar $companyName tst_company_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompanyName($companyName) {
		$this->_properties['companyName'] = $companyName;
    }


    /**
     * tst_last_name の値を返します
	 *
     * @return varchar tst_last_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastName() {
		return $this->_properties['lastName'];
    }


    /**
     * tst_last_name の値をセットします
	 *
	 * @param varchar $lastName tst_last_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastName($lastName) {
		$this->_properties['lastName'] = $lastName;
    }


    /**
     * tst_first_name の値を返します
	 *
     * @return varchar tst_first_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstName() {
		return $this->_properties['firstName'];
    }


    /**
     * tst_first_name の値をセットします
	 *
	 * @param varchar $firstName tst_first_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstName($firstName) {
		$this->_properties['firstName'] = $firstName;
    }


    /**
     * tst_last_name_kana の値を返します
	 *
     * @return varchar tst_last_name_kana
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastNameKana() {
		return $this->_properties['lastNameKana'];
    }


    /**
     * tst_last_name_kana の値をセットします
	 *
	 * @param varchar $lastNameKana tst_last_name_kana
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastNameKana($lastNameKana) {
		$this->_properties['lastNameKana'] = $lastNameKana;
    }


    /**
     * tst_first_name_kana の値を返します
	 *
     * @return varchar tst_first_name_kana
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstNameKana() {
		return $this->_properties['firstNameKana'];
    }


    /**
     * tst_first_name_kana の値をセットします
	 *
	 * @param varchar $firstNameKana tst_first_name_kana
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstNameKana($firstNameKana) {
		$this->_properties['firstNameKana'] = $firstNameKana;
    }


    /**
     * tst_zip の値を返します
	 *
     * @return char tst_zip
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZip() {
		return $this->_properties['zip'];
    }


    /**
     * tst_zip の値をセットします
	 *
	 * @param char $zip tst_zip
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZip($zip) {
		$this->_properties['zip'] = $zip;
    }


    /**
     * tst_prefecture の値を返します
	 *
     * @return varchar tst_prefecture
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefecture() {
		return $this->_properties['prefecture'];
    }


    /**
     * tst_prefecture の値をセットします
	 *
	 * @param varchar $prefecture tst_prefecture
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefecture($prefecture) {
		$this->_properties['prefecture'] = $prefecture;
    }


    /**
     * tst_address1 の値を返します
	 *
     * @return varchar tst_address1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * tst_address1 の値をセットします
	 *
	 * @param varchar $address1 tst_address1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * tst_address2 の値を返します
	 *
     * @return varchar tst_address2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * tst_address2 の値をセットします
	 *
	 * @param varchar $address2 tst_address2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


    /**
     * tst_tel の値を返します
	 *
     * @return varchar tst_tel
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTel() {
		return $this->_properties['tel'];
    }


    /**
     * tst_tel の値をセットします
	 *
	 * @param varchar $tel tst_tel
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTel($tel) {
		$this->_properties['tel'] = $tel;
    }


    /**
     * tst_shipping_type の値を返します
	 *
     * @return varchar tst_shipping_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingType() {
		return $this->_properties['shippingType'];
    }


    /**
     * tst_shipping_type の値をセットします
	 *
	 * @param varchar $shippingType tst_shipping_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingType($shippingType) {
		$this->_properties['shippingType'] = $shippingType;
    }


    /**
     * tst_expected_shipping_date の値を返します
	 *
     * @return date tst_expected_shipping_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExpectedShippingDate() {
		return $this->_properties['expectedShippingDate'];
    }


    /**
     * tst_expected_shipping_date の値をセットします
	 *
	 * @param date $expectedShippingDate tst_expected_shipping_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExpectedShippingDate($expectedShippingDate) {
		$this->_properties['expectedShippingDate'] = $expectedShippingDate;
    }


    /**
     * tst_payment_delivery_price の値を返します
	 *
     * @return int tst_payment_delivery_price
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryPrice() {
		return $this->_properties['paymentDeliveryPrice'];
    }


    /**
     * tst_payment_delivery_price の値をセットします
	 *
	 * @param int $paymentDeliveryPrice tst_payment_delivery_price
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryPrice($paymentDeliveryPrice) {
		$this->_properties['paymentDeliveryPrice'] = $paymentDeliveryPrice;
    }


    /**
     * tst_shipping_assign の値を返します
	 *
     * @return varchar tst_shipping_assign
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAssign() {
		return $this->_properties['shippingAssign'];
    }


    /**
     * tst_shipping_assign の値をセットします
	 *
	 * @param varchar $shippingAssign tst_shipping_assign
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAssign($shippingAssign) {
		$this->_properties['shippingAssign'] = $shippingAssign;
    }


    /**
     * tst_builder_id1 の値を返します
	 *
     * @return int tst_builder_id1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId1() {
		return $this->_properties['builderId1'];
    }


    /**
     * tst_builder_id1 の値をセットします
	 *
	 * @param int $builderId1 tst_builder_id1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId1($builderId1) {
		$this->_properties['builderId1'] = $builderId1;
    }


    /**
     * tst_builder_id2 の値を返します
	 *
     * @return int tst_builder_id2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId2() {
		return $this->_properties['builderId2'];
    }


    /**
     * tst_builder_id2 の値をセットします
	 *
	 * @param int $builderId2 tst_builder_id2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId2($builderId2) {
		$this->_properties['builderId2'] = $builderId2;
    }


    /**
     * tst_builder_id3 の値を返します
	 *
     * @return int tst_builder_id3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId3() {
		return $this->_properties['builderId3'];
    }


    /**
     * tst_builder_id3 の値をセットします
	 *
	 * @param int $builderId3 tst_builder_id3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId3($builderId3) {
		$this->_properties['builderId3'] = $builderId3;
    }


    /**
     * tst_builder_id4 の値を返します
	 *
     * @return int tst_builder_id4
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId4() {
		return $this->_properties['builderId4'];
    }


    /**
     * tst_builder_id4 の値をセットします
	 *
	 * @param int $builderId4 tst_builder_id4
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId4($builderId4) {
		$this->_properties['builderId4'] = $builderId4;
    }


    /**
     * tst_builder_id5 の値を返します
	 *
     * @return int tst_builder_id5
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId5() {
		return $this->_properties['builderId5'];
    }


    /**
     * tst_builder_id5 の値をセットします
	 *
	 * @param int $builderId5 tst_builder_id5
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId5($builderId5) {
		$this->_properties['builderId5'] = $builderId5;
    }


    /**
     * tst_shipping_status の値を返します
	 *
     * @return varchar tst_shipping_status
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStatus() {
		return $this->_properties['shippingStatus'];
    }


    /**
     * tst_shipping_status の値をセットします
	 *
	 * @param varchar $shippingStatus tst_shipping_status
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStatus($shippingStatus) {
		$this->_properties['shippingStatus'] = $shippingStatus;
    }


    /**
     * tst_shipping_stop_status の値を返します
	 *
     * @return varchar tst_shipping_stop_status
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStopStatus() {
		return $this->_properties['shippingStopStatus'];
    }


    /**
     * tst_shipping_stop_status の値をセットします
	 *
	 * @param varchar $shippingStopStatus tst_shipping_stop_status
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStopStatus($shippingStopStatus) {
		$this->_properties['shippingStopStatus'] = $shippingStopStatus;
    }


    /**
     * tst_support_status の値を返します
	 *
     * @return varchar tst_support_status
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSupportStatus() {
		return $this->_properties['supportStatus'];
    }


    /**
     * tst_support_status の値をセットします
	 *
	 * @param varchar $supportStatus tst_support_status
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSupportStatus($supportStatus) {
		$this->_properties['supportStatus'] = $supportStatus;
    }


    /**
     * tst_return_status の値を返します
	 *
     * @return varchar tst_return_status
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnStatus() {
		return $this->_properties['returnStatus'];
    }


    /**
     * tst_return_status の値をセットします
	 *
	 * @param varchar $returnStatus tst_return_status
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnStatus($returnStatus) {
		$this->_properties['returnStatus'] = $returnStatus;
    }


    /**
     * tst_return_support_status の値を返します
	 *
     * @return varchar tst_return_support_status
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnSupportStatus() {
		return $this->_properties['returnSupportStatus'];
    }


    /**
     * tst_return_support_status の値をセットします
	 *
	 * @param varchar $returnSupportStatus tst_return_support_status
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnSupportStatus($returnSupportStatus) {
		$this->_properties['returnSupportStatus'] = $returnSupportStatus;
    }


    /**
     * tst_return_reason_type の値を返します
	 *
     * @return varchar tst_return_reason_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnReasonType() {
		return $this->_properties['returnReasonType'];
    }


    /**
     * tst_return_reason_type の値をセットします
	 *
	 * @param varchar $returnReasonType tst_return_reason_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnReasonType($returnReasonType) {
		$this->_properties['returnReasonType'] = $returnReasonType;
    }


    /**
     * tst_return_request_date の値を返します
	 *
     * @return date tst_return_request_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnRequestDate() {
		return $this->_properties['returnRequestDate'];
    }


    /**
     * tst_return_request_date の値をセットします
	 *
	 * @param date $returnRequestDate tst_return_request_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnRequestDate($returnRequestDate) {
		$this->_properties['returnRequestDate'] = $returnRequestDate;
    }


    /**
     * tst_return_complete_date の値を返します
	 *
     * @return date tst_return_complete_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnCompleteDate() {
		return $this->_properties['returnCompleteDate'];
    }


    /**
     * tst_return_complete_date の値をセットします
	 *
	 * @param date $returnCompleteDate tst_return_complete_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnCompleteDate($returnCompleteDate) {
		$this->_properties['returnCompleteDate'] = $returnCompleteDate;
    }


    /**
     * tst_return_limit_date の値を返します
	 *
     * @return date tst_return_limit_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnLimitDate() {
		return $this->_properties['returnLimitDate'];
    }


    /**
     * tst_return_limit_date の値をセットします
	 *
	 * @param date $returnLimitDate tst_return_limit_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnLimitDate($returnLimitDate) {
		$this->_properties['returnLimitDate'] = $returnLimitDate;
    }


    /**
     * tst_responase_limit_date の値を返します
	 *
     * @return date tst_responase_limit_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResponaseLimitDate() {
		return $this->_properties['responaseLimitDate'];
    }


    /**
     * tst_responase_limit_date の値をセットします
	 *
	 * @param date $responaseLimitDate tst_responase_limit_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResponaseLimitDate($responaseLimitDate) {
		$this->_properties['responaseLimitDate'] = $responaseLimitDate;
    }


    /**
     * tst_cancel_after_shipping_date の値を返します
	 *
     * @return datetime tst_cancel_after_shipping_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelAfterShippingDate() {
		return $this->_properties['cancelAfterShippingDate'];
    }


    /**
     * tst_cancel_after_shipping_date の値をセットします
	 *
	 * @param datetime $cancelAfterShippingDate tst_cancel_after_shipping_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelAfterShippingDate($cancelAfterShippingDate) {
		$this->_properties['cancelAfterShippingDate'] = $cancelAfterShippingDate;
    }


    /**
     * tst_real_shipping_date の値を返します
	 *
     * @return date tst_real_shipping_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealShippingDate() {
		return $this->_properties['realShippingDate'];
    }


    /**
     * tst_real_shipping_date の値をセットします
	 *
	 * @param date $realShippingDate tst_real_shipping_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealShippingDate($realShippingDate) {
		$this->_properties['realShippingDate'] = $realShippingDate;
    }


    /**
     * tst_inquiry_number の値を返します
	 *
     * @return varchar tst_inquiry_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryNumber() {
		return $this->_properties['inquiryNumber'];
    }


    /**
     * tst_inquiry_number の値をセットします
	 *
	 * @param varchar $inquiryNumber tst_inquiry_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryNumber($inquiryNumber) {
		$this->_properties['inquiryNumber'] = $inquiryNumber;
    }


    /**
     * tst_refund_possibility_date の値を返します
	 *
     * @return date tst_refund_possibility_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundPossibilityDate() {
		return $this->_properties['refundPossibilityDate'];
    }


    /**
     * tst_refund_possibility_date の値をセットします
	 *
	 * @param date $refundPossibilityDate tst_refund_possibility_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundPossibilityDate($refundPossibilityDate) {
		$this->_properties['refundPossibilityDate'] = $refundPossibilityDate;
    }


    /**
     * tst_shipping_complete_flag の値を返します
	 *
     * @return char tst_shipping_complete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingCompleteFlag() {
		return $this->_properties['shippingCompleteFlag'];
    }


    /**
     * tst_shipping_complete_flag の値をセットします
	 *
	 * @param char $shippingCompleteFlag tst_shipping_complete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingCompleteFlag($shippingCompleteFlag) {
		$this->_properties['shippingCompleteFlag'] = $shippingCompleteFlag;
    }


    /**
     * tst_imperfection_flag の値を返します
	 *
     * @return char tst_imperfection_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * tst_imperfection_flag の値をセットします
	 *
	 * @param char $imperfectionFlag tst_imperfection_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * tst_temporary_invoice_id の値を返します
	 *
     * @return int tst_temporary_invoice_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemporaryInvoiceId() {
		return $this->_properties['temporaryInvoiceId'];
    }


    /**
     * tst_temporary_invoice_id の値をセットします
	 *
	 * @param int $temporaryInvoiceId tst_temporary_invoice_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemporaryInvoiceId($temporaryInvoiceId) {
		$this->_properties['temporaryInvoiceId'] = $temporaryInvoiceId;
    }


    /**
     * tst_invoice_id の値を返します
	 *
     * @return int tst_invoice_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInvoiceId() {
		return $this->_properties['invoiceId'];
    }


    /**
     * tst_invoice_id の値をセットします
	 *
	 * @param int $invoiceId tst_invoice_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInvoiceId($invoiceId) {
		$this->_properties['invoiceId'] = $invoiceId;
    }


    /**
     * tst_packaging_direction_code の値を返します
	 *
     * @return char tst_packaging_direction_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionCode() {
		return $this->_properties['packagingDirectionCode'];
    }


    /**
     * tst_packaging_direction_code の値をセットします
	 *
	 * @param char $packagingDirectionCode tst_packaging_direction_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionCode($packagingDirectionCode) {
		$this->_properties['packagingDirectionCode'] = $packagingDirectionCode;
    }


    /**
     * tst_packaging_direction_number の値を返します
	 *
     * @return int tst_packaging_direction_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionNumber() {
		return $this->_properties['packagingDirectionNumber'];
    }


    /**
     * tst_packaging_direction_number の値をセットします
	 *
	 * @param int $packagingDirectionNumber tst_packaging_direction_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionNumber($packagingDirectionNumber) {
		$this->_properties['packagingDirectionNumber'] = $packagingDirectionNumber;
    }


    /**
     * tst_real_shipping_type の値を返します
	 *
     * @return varchar tst_real_shipping_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealShippingType() {
		return $this->_properties['realShippingType'];
    }


    /**
     * tst_real_shipping_type の値をセットします
	 *
	 * @param varchar $realShippingType tst_real_shipping_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealShippingType($realShippingType) {
		$this->_properties['realShippingType'] = $realShippingType;
    }


    /**
     * tst_reshipping_flag の値を返します
	 *
     * @return char tst_reshipping_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReshippingFlag() {
		return $this->_properties['reshippingFlag'];
    }


    /**
     * tst_reshipping_flag の値をセットします
	 *
	 * @param char $reshippingFlag tst_reshipping_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReshippingFlag($reshippingFlag) {
		$this->_properties['reshippingFlag'] = $reshippingFlag;
    }


    /**
     * tst_shipping_slip_create_flag の値を返します
	 *
     * @return char tst_shipping_slip_create_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingSlipCreateFlag() {
		return $this->_properties['shippingSlipCreateFlag'];
    }


    /**
     * tst_shipping_slip_create_flag の値をセットします
	 *
	 * @param char $shippingSlipCreateFlag tst_shipping_slip_create_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingSlipCreateFlag($shippingSlipCreateFlag) {
		$this->_properties['shippingSlipCreateFlag'] = $shippingSlipCreateFlag;
    }


    /**
     * tst_shipping_stop_flag の値を返します
	 *
     * @return char tst_shipping_stop_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStopFlag() {
		return $this->_properties['shippingStopFlag'];
    }


    /**
     * tst_shipping_stop_flag の値をセットします
	 *
	 * @param char $shippingStopFlag tst_shipping_stop_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStopFlag($shippingStopFlag) {
		$this->_properties['shippingStopFlag'] = $shippingStopFlag;
    }


    /**
     * tst_proposal_datetime の値を返します
	 *
     * @return datetime tst_proposal_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProposalDatetime() {
		return $this->_properties['proposalDatetime'];
    }


    /**
     * tst_proposal_datetime の値をセットします
	 *
	 * @param datetime $proposalDatetime tst_proposal_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProposalDatetime($proposalDatetime) {
		$this->_properties['proposalDatetime'] = $proposalDatetime;
    }


    /**
     * tst_campaign_sales_code の値を返します
	 *
     * @return varchar tst_campaign_sales_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesCode() {
		return $this->_properties['campaignSalesCode'];
    }


    /**
     * tst_campaign_sales_code の値をセットします
	 *
	 * @param varchar $campaignSalesCode tst_campaign_sales_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesCode($campaignSalesCode) {
		$this->_properties['campaignSalesCode'] = $campaignSalesCode;
    }


    /**
     * tst_campaign_code の値を返します
	 *
     * @return varchar tst_campaign_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignCode() {
		return $this->_properties['campaignCode'];
    }


    /**
     * tst_campaign_code の値をセットします
	 *
	 * @param varchar $campaignCode tst_campaign_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignCode($campaignCode) {
		$this->_properties['campaignCode'] = $campaignCode;
    }


    /**
     * tst_product_shipping_code の値を返します
	 *
     * @return varchar tst_product_shipping_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingCode() {
		return $this->_properties['productShippingCode'];
    }


    /**
     * tst_product_shipping_code の値をセットします
	 *
	 * @param varchar $productShippingCode tst_product_shipping_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingCode($productShippingCode) {
		$this->_properties['productShippingCode'] = $productShippingCode;
    }


    /**
     * tst_product_package_code の値を返します
	 *
     * @return varchar tst_product_package_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductPackageCode() {
		return $this->_properties['productPackageCode'];
    }


    /**
     * tst_product_package_code の値をセットします
	 *
	 * @param varchar $productPackageCode tst_product_package_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductPackageCode($productPackageCode) {
		$this->_properties['productPackageCode'] = $productPackageCode;
    }


    /**
     * tst_delete_flag の値を返します
	 *
     * @return char tst_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tst_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tst_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tst_deletion_datetime の値を返します
	 *
     * @return datetime tst_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tst_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tst_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tst_registration_datetime の値を返します
	 *
     * @return datetime tst_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tst_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tst_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tst_update_datetime の値を返します
	 *
     * @return datetime tst_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tst_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tst_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tst_update_timestamp の値を返します
	 *
     * @return timestamp tst_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tst_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tst_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























