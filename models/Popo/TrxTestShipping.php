<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_test_shipping
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxTestShipping extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tts';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'shippingId' => null,
        'customerId' => null,
        'divisionId' => null,
        'relativeShippingId' => null,
        'campaignSalesCode' => null,
        'campaignCode' => null,
        'productShippingCode' => null,
        'packageType' => null,
        'packageCode' => null,
        'packageNumber' => null,
        'chargeType' => null,
        'stepType' => null,
        'stepCount' => null,
        'shippingGroupLabel' => null,
        'companyName' => null,
        'lastName' => null,
        'firstName' => null,
        'lastNameKana' => null,
        'firstNameKana' => null,
        'zip' => null,
        'prefecture' => null,
        'address1' => null,
        'address2' => null,
        'tel' => null,
        'shippingType' => null,
        'expectedShippingDate' => null,
        'paymentDeliveryPrice' => null,
        'shippingAssign' => null,
        'builderId1' => null,
        'builderId2' => null,
        'builderId3' => null,
        'builderId4' => null,
        'builderId5' => null,
        'shippingStatus' => null,
        'realShippingDate' => null,
        'inquiryNumber' => null,
        'shippingStopStatus' => null,
        'dirtyFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 発送ID の値を返します
	 *
     * @return int 発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * 発送ID の値をセットします
	 *
	 * @param int $shippingId 発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 関連発送ID の値を返します
	 *
     * @return int 関連発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRelativeShippingId() {
		return $this->_properties['relativeShippingId'];
    }


    /**
     * 関連発送ID の値をセットします
	 *
	 * @param int $relativeShippingId 関連発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRelativeShippingId($relativeShippingId) {
		$this->_properties['relativeShippingId'] = $relativeShippingId;
    }


    /**
     * キャンペーンセールスコード の値を返します
	 *
     * @return varchar キャンペーンセールスコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesCode() {
		return $this->_properties['campaignSalesCode'];
    }


    /**
     * キャンペーンセールスコード の値をセットします
	 *
	 * @param varchar $campaignSalesCode キャンペーンセールスコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesCode($campaignSalesCode) {
		$this->_properties['campaignSalesCode'] = $campaignSalesCode;
    }


    /**
     * キャンペーンコード の値を返します
	 *
     * @return varchar キャンペーンコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignCode() {
		return $this->_properties['campaignCode'];
    }


    /**
     * キャンペーンコード の値をセットします
	 *
	 * @param varchar $campaignCode キャンペーンコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignCode($campaignCode) {
		$this->_properties['campaignCode'] = $campaignCode;
    }


    /**
     * 商品発送コード の値を返します
	 *
     * @return varchar 商品発送コード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingCode() {
		return $this->_properties['productShippingCode'];
    }


    /**
     * 商品発送コード の値をセットします
	 *
	 * @param varchar $productShippingCode 商品発送コード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingCode($productShippingCode) {
		$this->_properties['productShippingCode'] = $productShippingCode;
    }


    /**
     * パッケージタイプ の値を返します
	 *
     * @return varchar パッケージタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageType() {
		return $this->_properties['packageType'];
    }


    /**
     * パッケージタイプ の値をセットします
	 *
	 * @param varchar $packageType パッケージタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageType($packageType) {
		$this->_properties['packageType'] = $packageType;
    }


    /**
     * 商品パッケージコード の値を返します
	 *
     * @return varchar 商品パッケージコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageCode() {
		return $this->_properties['packageCode'];
    }


    /**
     * 商品パッケージコード の値をセットします
	 *
	 * @param varchar $packageCode 商品パッケージコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageCode($packageCode) {
		$this->_properties['packageCode'] = $packageCode;
    }


    /**
     * パッケージナンバー の値を返します
	 *
     * @return int パッケージナンバー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageNumber() {
		return $this->_properties['packageNumber'];
    }


    /**
     * パッケージナンバー の値をセットします
	 *
	 * @param int $packageNumber パッケージナンバー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageNumber($packageNumber) {
		$this->_properties['packageNumber'] = $packageNumber;
    }


    /**
     * 請求タイプ の値を返します
	 *
     * @return varchar 請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * 請求タイプ の値をセットします
	 *
	 * @param varchar $chargeType 請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * 適用ステップタイプ の値を返します
	 *
     * @return varchar 適用ステップタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepType() {
		return $this->_properties['stepType'];
    }


    /**
     * 適用ステップタイプ の値をセットします
	 *
	 * @param varchar $stepType 適用ステップタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepType($stepType) {
		$this->_properties['stepType'] = $stepType;
    }


    /**
     * 適用ステップカウント の値を返します
	 *
     * @return int 適用ステップカウント
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepCount() {
		return $this->_properties['stepCount'];
    }


    /**
     * 適用ステップカウント の値をセットします
	 *
	 * @param int $stepCount 適用ステップカウント
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepCount($stepCount) {
		$this->_properties['stepCount'] = $stepCount;
    }


    /**
     * 発送グループラベル の値を返します
	 *
     * @return varchar 発送グループラベル
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingGroupLabel() {
		return $this->_properties['shippingGroupLabel'];
    }


    /**
     * 発送グループラベル の値をセットします
	 *
	 * @param varchar $shippingGroupLabel 発送グループラベル
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingGroupLabel($shippingGroupLabel) {
		$this->_properties['shippingGroupLabel'] = $shippingGroupLabel;
    }


    /**
     * 発送先会社名 の値を返します
	 *
     * @return varchar 発送先会社名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompanyName() {
		return $this->_properties['companyName'];
    }


    /**
     * 発送先会社名 の値をセットします
	 *
	 * @param varchar $companyName 発送先会社名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompanyName($companyName) {
		$this->_properties['companyName'] = $companyName;
    }


    /**
     * 発送先お名前　姓 の値を返します
	 *
     * @return varchar 発送先お名前　姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastName() {
		return $this->_properties['lastName'];
    }


    /**
     * 発送先お名前　姓 の値をセットします
	 *
	 * @param varchar $lastName 発送先お名前　姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastName($lastName) {
		$this->_properties['lastName'] = $lastName;
    }


    /**
     * 発送先お名前　名 の値を返します
	 *
     * @return varchar 発送先お名前　名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstName() {
		return $this->_properties['firstName'];
    }


    /**
     * 発送先お名前　名 の値をセットします
	 *
	 * @param varchar $firstName 発送先お名前　名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstName($firstName) {
		$this->_properties['firstName'] = $firstName;
    }


    /**
     * 発送先お名前（カナ）　姓 の値を返します
	 *
     * @return varchar 発送先お名前（カナ）　姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastNameKana() {
		return $this->_properties['lastNameKana'];
    }


    /**
     * 発送先お名前（カナ）　姓 の値をセットします
	 *
	 * @param varchar $lastNameKana 発送先お名前（カナ）　姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastNameKana($lastNameKana) {
		$this->_properties['lastNameKana'] = $lastNameKana;
    }


    /**
     * 発送先お名前（カナ）　名 の値を返します
	 *
     * @return varchar 発送先お名前（カナ）　名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstNameKana() {
		return $this->_properties['firstNameKana'];
    }


    /**
     * 発送先お名前（カナ）　名 の値をセットします
	 *
	 * @param varchar $firstNameKana 発送先お名前（カナ）　名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstNameKana($firstNameKana) {
		$this->_properties['firstNameKana'] = $firstNameKana;
    }


    /**
     * 発送先郵便番号 の値を返します
	 *
     * @return char 発送先郵便番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZip() {
		return $this->_properties['zip'];
    }


    /**
     * 発送先郵便番号 の値をセットします
	 *
	 * @param char $zip 発送先郵便番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZip($zip) {
		$this->_properties['zip'] = $zip;
    }


    /**
     * 発送先都道府県 の値を返します
	 *
     * @return varchar 発送先都道府県
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefecture() {
		return $this->_properties['prefecture'];
    }


    /**
     * 発送先都道府県 の値をセットします
	 *
	 * @param varchar $prefecture 発送先都道府県
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefecture($prefecture) {
		$this->_properties['prefecture'] = $prefecture;
    }


    /**
     * 発送先住所１ の値を返します
	 *
     * @return varchar 発送先住所１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * 発送先住所１ の値をセットします
	 *
	 * @param varchar $address1 発送先住所１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * 発送先住所２ の値を返します
	 *
     * @return varchar 発送先住所２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * 発送先住所２ の値をセットします
	 *
	 * @param varchar $address2 発送先住所２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


    /**
     * 発送先電話番号 の値を返します
	 *
     * @return varchar 発送先電話番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTel() {
		return $this->_properties['tel'];
    }


    /**
     * 発送先電話番号 の値をセットします
	 *
	 * @param varchar $tel 発送先電話番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTel($tel) {
		$this->_properties['tel'] = $tel;
    }


    /**
     * 発送タイプ の値を返します
	 *
     * @return varchar 発送タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingType() {
		return $this->_properties['shippingType'];
    }


    /**
     * 発送タイプ の値をセットします
	 *
	 * @param varchar $shippingType 発送タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingType($shippingType) {
		$this->_properties['shippingType'] = $shippingType;
    }


    /**
     * 発送予定日 の値を返します
	 *
     * @return date 発送予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExpectedShippingDate() {
		return $this->_properties['expectedShippingDate'];
    }


    /**
     * 発送予定日 の値をセットします
	 *
	 * @param date $expectedShippingDate 発送予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExpectedShippingDate($expectedShippingDate) {
		$this->_properties['expectedShippingDate'] = $expectedShippingDate;
    }


    /**
     * 代引き金額 の値を返します
	 *
     * @return int 代引き金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryPrice() {
		return $this->_properties['paymentDeliveryPrice'];
    }


    /**
     * 代引き金額 の値をセットします
	 *
	 * @param int $paymentDeliveryPrice 代引き金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryPrice($paymentDeliveryPrice) {
		$this->_properties['paymentDeliveryPrice'] = $paymentDeliveryPrice;
    }


    /**
     * 発送指示 の値を返します
	 *
     * @return varchar 発送指示
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAssign() {
		return $this->_properties['shippingAssign'];
    }


    /**
     * 発送指示 の値をセットします
	 *
	 * @param varchar $shippingAssign 発送指示
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAssign($shippingAssign) {
		$this->_properties['shippingAssign'] = $shippingAssign;
    }


    /**
     * 部材１ の値を返します
	 *
     * @return int 部材１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId1() {
		return $this->_properties['builderId1'];
    }


    /**
     * 部材１ の値をセットします
	 *
	 * @param int $builderId1 部材１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId1($builderId1) {
		$this->_properties['builderId1'] = $builderId1;
    }


    /**
     * 部材２ の値を返します
	 *
     * @return int 部材２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId2() {
		return $this->_properties['builderId2'];
    }


    /**
     * 部材２ の値をセットします
	 *
	 * @param int $builderId2 部材２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId2($builderId2) {
		$this->_properties['builderId2'] = $builderId2;
    }


    /**
     * 部材３ の値を返します
	 *
     * @return int 部材３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId3() {
		return $this->_properties['builderId3'];
    }


    /**
     * 部材３ の値をセットします
	 *
	 * @param int $builderId3 部材３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId3($builderId3) {
		$this->_properties['builderId3'] = $builderId3;
    }


    /**
     * 部材４ の値を返します
	 *
     * @return int 部材４
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId4() {
		return $this->_properties['builderId4'];
    }


    /**
     * 部材４ の値をセットします
	 *
	 * @param int $builderId4 部材４
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId4($builderId4) {
		$this->_properties['builderId4'] = $builderId4;
    }


    /**
     * 部材５ の値を返します
	 *
     * @return int 部材５
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId5() {
		return $this->_properties['builderId5'];
    }


    /**
     * 部材５ の値をセットします
	 *
	 * @param int $builderId5 部材５
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId5($builderId5) {
		$this->_properties['builderId5'] = $builderId5;
    }


    /**
     * 発送ステータス の値を返します
	 *
     * @return varchar 発送ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStatus() {
		return $this->_properties['shippingStatus'];
    }


    /**
     * 発送ステータス の値をセットします
	 *
	 * @param varchar $shippingStatus 発送ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStatus($shippingStatus) {
		$this->_properties['shippingStatus'] = $shippingStatus;
    }


    /**
     * 実発送日 の値を返します
	 *
     * @return date 実発送日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealShippingDate() {
		return $this->_properties['realShippingDate'];
    }


    /**
     * 実発送日 の値をセットします
	 *
	 * @param date $realShippingDate 実発送日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealShippingDate($realShippingDate) {
		$this->_properties['realShippingDate'] = $realShippingDate;
    }


    /**
     * 問合せ番号 の値を返します
	 *
     * @return varchar 問合せ番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryNumber() {
		return $this->_properties['inquiryNumber'];
    }


    /**
     * 問合せ番号 の値をセットします
	 *
	 * @param varchar $inquiryNumber 問合せ番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryNumber($inquiryNumber) {
		$this->_properties['inquiryNumber'] = $inquiryNumber;
    }


    /**
     * 発送停止フラグ の値を返します
	 *
     * @return char 発送停止フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStopStatus() {
		return $this->_properties['shippingStopStatus'];
    }


    /**
     * 発送停止フラグ の値をセットします
	 *
	 * @param char $shippingStopStatus 発送停止フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStopStatus($shippingStopStatus) {
		$this->_properties['shippingStopStatus'] = $shippingStopStatus;
    }


    /**
     * ダーティーフラグ の値を返します
	 *
     * @return char ダーティーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirtyFlag() {
		return $this->_properties['dirtyFlag'];
    }


    /**
     * ダーティーフラグ の値をセットします
	 *
	 * @param char $dirtyFlag ダーティーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirtyFlag($dirtyFlag) {
		$this->_properties['dirtyFlag'] = $dirtyFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























