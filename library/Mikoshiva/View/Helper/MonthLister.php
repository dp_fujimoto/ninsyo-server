<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Zend/View/Interface.php';
require_once 'Zend/Validate/Date.php';

/**
 * 年のリストを作成します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/16
 * @version SVN:$Id: MonthLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_MonthLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;

    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function monthLister($name, $selected = null, $attribs = null)
    {
        logInfo(LOG_START);
        $monthList = array();
        $monthList['']  = '';
        $monthList['01'] = '01';
        $monthList['02'] = '02';
        $monthList['03'] = '03';
        $monthList['04'] = '04';
        $monthList['05'] = '05';
        $monthList['06'] = '06';
        $monthList['07'] = '07';
        $monthList['08'] = '08';
        $monthList['09'] = '09';
        $monthList['10'] = '10';
        $monthList['11'] = '11';
        $monthList['12'] = '12';


        // ◆selected 2009/12/24 00:00:00 で来た場合にも対応
        $_selected = $selected;
        if (isset($_selected)) {
            $_selected = str_replace('/', '-', $_selected );
            $validator1 = new Zend_Validate_Date('Y-m-d H:i:s');
            $validator2 = new Zend_Validate_Date('Y-m-d');
            if ($validator1->isValid($_selected) || $validator2->isValid($_selected)) {

                $matche = array();
                preg_match('#^\d{4}-(\d{2})#', $_selected, $matche);
                $_selected = $matche[1];
            }
        }
        logInfo(LOG_END);
        return $this->view->formSelect($name,
                                       $_selected,
                                       $attribs,
                                       $monthList);
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
