<?php

/**
 * Zend_Http_Response の ラップクラス
 *
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/23
 * @version SVN:$Id: Response.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Http_Response extends Zend_Http_Response {

    /**
     * コンストラクタ
     *
     * @param $code
     * @param $headers
     * @param $body
     * @param $version
     * @param $message
     * @return unknown_type
     */
    public function __construct($code, $headers, $body = null, $version = '1.1', $message = null)
    {
        parent::__construct($code, $headers, $body, $version, $message);
    }

}