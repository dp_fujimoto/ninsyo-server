<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_inquiry_template
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstInquiryTemplate extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mit';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'inquiryTemplateId' => null,
        'templateName' => null,
        'divisionValue' => null,
        'subject' => null,
        'body' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 問合せテンプレートテーブルID の値を返します
	 *
     * @return int 問合せテンプレートテーブルID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryTemplateId() {
		return $this->_properties['inquiryTemplateId'];
    }


    /**
     * 問合せテンプレートテーブルID の値をセットします
	 *
	 * @param int $inquiryTemplateId 問合せテンプレートテーブルID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryTemplateId($inquiryTemplateId) {
		$this->_properties['inquiryTemplateId'] = $inquiryTemplateId;
    }


    /**
     * テンプレート名 の値を返します
	 *
     * @return varchar テンプレート名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateName() {
		return $this->_properties['templateName'];
    }


    /**
     * テンプレート名 の値をセットします
	 *
	 * @param varchar $templateName テンプレート名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateName($templateName) {
		$this->_properties['templateName'] = $templateName;
    }


    /**
     * 部署値 の値を返します
	 *
     * @return int 部署値
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionValue() {
		return $this->_properties['divisionValue'];
    }


    /**
     * 部署値 の値をセットします
	 *
	 * @param int $divisionValue 部署値
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionValue($divisionValue) {
		$this->_properties['divisionValue'] = $divisionValue;
    }


    /**
     * 件名 の値を返します
	 *
     * @return varchar 件名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubject() {
		return $this->_properties['subject'];
    }


    /**
     * 件名 の値をセットします
	 *
	 * @param varchar $subject 件名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubject($subject) {
		$this->_properties['subject'] = $subject;
    }


    /**
     * 本文 の値を返します
	 *
     * @return text 本文
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBody() {
		return $this->_properties['body'];
    }


    /**
     * 本文 の値をセットします
	 *
	 * @param text $body 本文
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBody($body) {
		$this->_properties['body'] = $body;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























