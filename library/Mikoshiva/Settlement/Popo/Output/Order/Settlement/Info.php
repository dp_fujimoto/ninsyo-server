<?php
/** @package Mikoshiva_Settlement */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Output_Order_Settlement_Info
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Order_Settlement_Info extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'MemberID' => null,
        'ShopID' => null,
        'authFlag' => null,
        'inputProductInfo' => null,
        'settlementOutputList' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * MemberID
     *
     * @return  MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMemberID() {
        return $this->_properties['MemberID'];
    }


    /**
     * MemberID
     *
     * @param  $MemberID MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMemberID($MemberID) {
        $this->_properties['MemberID'] = $MemberID;
    }



    /**
     * ShopID
     *
     * @return  ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getShopID() {
        return $this->_properties['ShopID'];
    }


    /**
     * ShopID
     *
     * @param  $ShopID ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setShopID($ShopID) {
        $this->_properties['ShopID'] = $ShopID;
    }


    /**
     * authFlag
     *
     * @return string authFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getAuthFlag() {
        return $this->_properties['authFlag'];
    }


    /**
     * authFlag
     *
     * @param string $paymentType authFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setAuthFlag($authFlag) {
        $this->_properties['authFlag'] = $authFlag;
    }


    /**
     * inputProductInfo
     *
     * @return  inputProductInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getInputProductInfo() {
        return $this->_properties['inputProductInfo'];
    }


    /**
     * inputProductInfo
     *
     * @param  $inputProductInfo inputProductInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setInputProductInfo($inputProductInfo) {
        $this->_properties['inputProductInfo'] = $inputProductInfo;
    }


    /**
     * settlementOutputList
     *
     * @return Mikoshiva_Settlement_Popo_Output_Order_Settlement_List settlementOutputList
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getSettlementOutputList() {
        return $this->_properties['settlementOutputList'];
    }


    /**
     * settlementOutputList
     *
     * @param Mikoshiva_Settlement_Popo_Output_Order_Settlement_List $settlementOutputList settlementOutputList
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Info.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setSettlementOutputList(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List $settlementOutputList) {
        $this->_properties['settlementOutputList'] = $settlementOutputList;
    }
}

























