<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


    /**
     * データベベースから値を取得し、セレクトボックスを生成して返します。
     *
     * @param array $params
     *                      string       name                   フォームのname, id
     *                      string       selected    空文字   | selectedにする値
     *                      string       arClassName            アクティブレコード名
     *                      array        where       array()  | 検索条件
     *                      string       valueName              value に入力させたいカラム名
     *                      string|array labelName              ラベルに表示されたいカラム名
     *                      string       separator   空文字   | ラベルが複数指定された時の区切り文字
     *                      array        attribs     array()  | selectタグに指定する追加の属性 '属性名' => 値
     *                      boolean      addEmpty    false    | リストに空白を追加(デフォルト：無効)
     *                      boolean      addAllItem  false    | リストに｢全て｣を追加(デフォルト：無効)
     *                      string       listsep     "<br>\n" | 各エンティティを区切る文字列
     * @param Smarty   $smarty
     * @param Template $template
     *
     * @return string                    &lt;select&gt;DBから取得した値によるリスト&lt;/select&gt;
     *
     * @package Smarty_libs
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/02
     * @version SVN:$Id: function.formSelectDb.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
     */
function smarty_function_formSelectDb($params, &$smarty, &$template) {
    Mikoshiva_Logger::info(LOG_START);

    require_once SMARTY_PLUGINS_DIR . 'function.formSelect.php';

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['name']         = '';
    $default['selected']     = '';
    $default['attribs']      = array();
    $default['addEmpty']     = false;
    $default['addAllItem']   = false;
    $default['listsep']      = "<br>\n";
    $default['arClassName'] = null;
    $default['where']       = array();
    $default['valueName']   = null;
    $default['labelName']   = null;
    $default['separator']   = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }

    if(isBlankOrNull($data['arClassName']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 arClassName に値がありません。');
    }

    if(isBlankOrNull($data['valueName']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 valueName に値がありません。');
    }

    if(is_array($data['labelName'])) {
        if(count($data['labelName']) === 0) {
            throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 labelName に値がありません。');
        }
    } else if(isBlankOrNull($data['labelName']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 labelName に値がありません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }
    if(!is_array($data['where'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 where は配列でなければなりません。');
    }



    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $arClassName = $data['arClassName'];
    $valueName   = $data['valueName'];
    $labelName   = $data['labelName'];
    $where       = $data['where'];
    $separator   = $data['separator'];

    /* @var $arClass Zend_Db_Table_Abstract */
    require_once str_replace('_', '/', $arClassName) . '.php';
    $arClass = new $arClassName();
    $select  = $arClass->select();

    if(is_array($labelName)) {
        $select->from($arClass, array_merge((array)$valueName, $labelName));
    } else {
        $select->from($arClass, array($valueName, $labelName));
    }

    // 条件を設定
    foreach ($where as $k => $v) {
        $select->where($k . ' = ?', $v);
    }

    // 取得
    $rows = $arClass->fetchAll($select);


    // 取得件数が 0 件で有れば空文字を返す
    if ($rows->count() === 0) {
        $data['options'] = array("" => "");
        return smarty_function_formSelect($data, $smarty, $template);
    }


    // formSelectに渡すため、配列を整形する
    $options = array();
    if(is_array($labelName)) {
        // labelNameが配列の場合、ラベル名の結合を行う
        foreach ($rows->toArray() as $k2 => $v2) {
            $n = array();
            foreach($labelName as $column) {
                $n[] = $v2[$column];
            }
            $options[ $v2[$valueName] ] = implode($separator, $n);
        }
    } else {
        foreach ($rows->toArray() as $k2 => $v2) {
            $options[ $v2[$valueName] ] = $v2[ $labelName ];
        }
    }

    $data['options'] = $options;


    Mikoshiva_Logger::info(LOG_END);
    return smarty_function_formSelect($data, $smarty, $template);

}


?>
