<?php
/** @package Mikoshiva_Gmo */


/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Abstract.php';


/**
 * 月内・未来の日付（仮売上）の決済データを【返品】します
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Check.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Alter_Check extends Mikoshiva_Settlement_Execution_Gmo_Alter_Abstract {

    /**
     * 処理区分 を保持します（必須）
     * @var string 処理区分 を保持します（必須）
     */
    protected $_jobCd = 'CHECK';
    
    public function execute() {
        // GMO 処理結果 POPO クラス
        include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Alter.php';
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Alter();
        
        return true;
    }

}