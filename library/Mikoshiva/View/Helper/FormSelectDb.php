<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Zend/View/Interface.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';

/**
 * データベベースから値を取得し、セレクトボックスを生成します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/28
 * @version SVN:$Id: FormSelectDb.php,v 1.1 2012/09/21 07:08:06 fujimoto Exp $
 */
class Mikoshiva_View_Helper_FormSelectDb extends Mikoshiva_View_Helper_Abstract {

    public $view;



    /**
     * データベベースから値を取得し、セレクトボックスを生成します
     *
     * @param  string       $name        name 属性
     * @param  string       $selected    selected
     * @param  string       $arClassName アクティブレコード名
     * @param  array        $where       検索条件
     * @param  string       $valueName   value に入力させたいカラム名
     * @param  string|array $labelName   ラベルに表示されたいカラム名
     * @param  string       $separater   ラベルが複数指定された時の区切り文字
     * @param  array        $attribs     追加属性
     * @param  boolean      $addEmpty    true: 先頭項目に空項目を追加
     * @param  boolean      $addAllItem  true: 先頭項目に【全て】の項目を追加
     * @param  string       $listsep     区切文字
     * @return string                    &lt;select&gt;DBから取得した値によるリスト&lt;/select&gt;
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/22
     * @version SVN:$Id: FormSelectDb.php,v 1.1 2012/09/21 07:08:06 fujimoto Exp $
     */
    public function formSelectDb($name,
                                 $selected     = null,
                                 $arClassName,
                           array $where        = array(),
                                 $valueName,
                                 $labelName,
                                 $separater    = '',
                                 $attribs      = null,
                                 $addEmpty     = false,
                                 $addAllItem   = false,
                                 $listsep      = "<br>\n") {


        // メソッド開始ログを出力
        logInfo(LOG_START);

        if(isBlankOrNull($arClassName) || isBlankOrNull($valueName) || isBlankOrNull($labelName)) {
            // メソッド終了ログを出力
            logInfo(LOG_END);
            return '';
        }

        /* @var $arClass Zend_Db_Table_Abstract */
        require_once preg_replace('/_/', '/', $arClassName, 1) . '.php';
        $arClass = new $arClassName();
        $select  = $arClass->select();

        if(is_array($labelName)) {
            $select->from($arClass, array($valueName)+$labelName);
        } else {
            $select->from($arClass, array($valueName, $labelName));
        }

        // 条件を設定
        foreach ($where as $k => $v) {
            $select->where($k, $v);
        }


        // 取得
        $rows = $arClass->fetchAll($select);


        // 取得件数が 0 件で有れば空文字を返す
        if ($rows->count() === 0) return '';


        // formSelectに渡すため、配列を整形する
        // @authr M.Ikuma <ikumamasayuki@gmail.com>
        $d     = array();
        if(is_array($labelName)) {
            // labelNameが配列の場合、ラベル名の結合を行う
            foreach ($rows->toArray() as $k2 => $v2) {
                $n = array();
                foreach($labelName as $column) {
                    $n[] = $v2[$column];
                }
                $d[ $v2[$valueName] ] = implode($separater, $n);
            }
        } else {
            foreach ($rows->toArray() as $k2 => $v2) {
                $d[ $v2[$valueName] ] = $v2[ $labelName ];
            }
        }


        // メソッド終了ログを出力
        logInfo(LOG_END);
$helperFormSelect = new Mikoshiva_View_Helper_FormSelect();

        // セレクトボックスを返す
        return $helperFormSelect->formSelect($name,
                                       $selected,
                                       $attribs,
                                       $d,
                                       $addEmpty,
                                       $addAllItem,
                                       $listsep
                                     );
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
