<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/Registry.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';

/**
 * カラムリストからマッピングされた値を取得します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/09
 * @version SVN:$Id: ColumnName.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_ColumnName extends Mikoshiva_View_Helper_Abstract {

    /**
     * カラムリストからマッピングされた値を取得します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ColumnName.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @return string
     */
    public function columnName($column) {

        $COLUMN_MAP_LIST = Zend_Registry::get('COLUMN_MAP_LIST');

        if (!isset($COLUMN_MAP_LIST[$column])) return '項目リストに【 ' . $column . ' 】 は存在しません。 ';
        return $COLUMN_MAP_LIST[$column];
    }
}
