<?php
/** @package Mikoshiva_Db */


/**
 * require
 */
require_once 'Mikoshiva/Log/Operation.php';
/**
 *
 * @package Mikoshiva_Db
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: Mysql.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Adapter_Pdo_Mysql extends Zend_Db_Adapter_Pdo_Mysql {


    private $_opeLog;

    public function __construct($config) {
        parent::__construct($config);
        $this->_opeLog = new Mikoshiva_Log_Operation();
    }
    /**
     * 実行前に SQL のログを記録します
     *
     * @param string|Zend_Db_Select $sql SQL
     * @param array $bind バインドデータ
     * @return Zend_Db_Statement_Interface
     * (non-PHPdoc)
     * @see library/Zend/Db/Adapter/Pdo/Zend_Db_Adapter_Pdo_Abstract#query($sql, $bind)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/29
     * @version SVN:$Id: Mysql.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function query($sql, $bind = array()) {

        $sqlBinded = '';

        // Zend_Db_Select で有る場合、バインド後のSQLを取得出来るので
        // そのまま取得
        if ($sql instanceof Zend_Db_Select) {

            // DESCRIBE は除去
            if (preg_match('#^DESCRIBE#', $sql->__toString()) !== 1) {
                if(preg_match('/mst_operation|trx_action_analysis|trx_customer_rfm_ranking|trx_claim|trx_money_received|trx_real_refund|trx_balance_payments_execution|trx_balance_payments|trx_balance_payments_detail/', $sql->__toString()) === 0) {
                    $sqlBinded = str_replace("\n", '', $sql->__toString());

                    Zend_Registry::get('sqlLogger')->info($sqlBinded);
                }
            }
        } else {

            // DESCRIBE は除去
            if (preg_match('#^DESCRIBE#', $sql) !== 1) {

                // Zend_Db_Select でない場合は手動でバインドを行う
                $_sql = $sql;
                foreach ($bind as $k => $v) {
                    $_v = '';
                    $_v = (is_string($v)) ? "'" . $v . "'" : $v;
                    $_sql = preg_replace('#\?#', $_v, $_sql, 1);
                }

                if(preg_match('/mst_operation|trx_action_analysis|trx_customer_rfm_ranking|trx_claim|trx_money_received|trx_real_refund|trx_balance_payments_execution|trx_balance_payments|trx_balance_payments_detail/', $_sql) === 0) {
                    Zend_Registry::get('sqlLogger')->info($_sql);
                }
                $sqlBinded = $_sql;
            }
        }


        // オペログの書き込み
        $this->_opeLog->register($sqlBinded);

        // SQL発行
        $data = array();
        $data = parent::query($sql, $bind);


        // 最後に実行されたSQLを保存(エラーログ出力等に使う)
        Zend_Registry::set('LAST_EXECUTED_SQL', $sqlBinded);


        return $data;
    }

}
