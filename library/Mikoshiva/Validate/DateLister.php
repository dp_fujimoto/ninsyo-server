<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Date
 */
require_once 'Zend/Validate/Date.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 日付関連のメソッドを提供します。
 *
 * @category   Mikoshiva
 * @package    Mikoshiva_Validate
 */
class Mikoshiva_Validate_DateLister extends Zend_Validate_Date
{

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_YYYY_MM_DD => "【%COLUMN_NAME%】は正しい形式ではありません",
        self::FALSEFORMAT    => "【%COLUMN_NAME%】は正しい形式ではありません",
        self::INVALID        => "【%COLUMN_NAME%】は正しい日付ではないようです",
        self::INVALID_DATE   => "【%COLUMN_NAME%】は正しい日付ではないようです",
    );


    /**
     * 月
     * @var string
     */
    protected $_monthName;


    /**
     * 日
     * @var string
     */
    protected $_dayName;


    /**
     * リクエストデータ
     * @var array
     */
    protected $_data;


    /**
     * バリデーション用のフォーマットを設定します。
     *
     * @param string $dateFormat
     * @return void
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: DateLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct($month = null, $day = null) {


        // ログ開始
        logInfo(LOG_START);


        // 値をセット（月）
        $this->_monthName = $month;


        // 値をセット（日）
        $this->_dayName   = $day;


        // format
        $this->_format = 'yyy-MM-dd';


        // ログ終了
        logInfo(LOG_END);
    }


    /**
     * 入力された文字列に対し、Validator.ymlに設定されたフォーマットと一致するかのチェックを行います。
     * (Zend_Dateのチェックよりは厳密)
     * フォーマットと一致したものについては、Zend_Validate_Dateにチェックを依頼し、
     * 存在しない日付・時刻のチェックを行います。
     *
     * @param string $value
     * @return unknown_type
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Date#isValid($value)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: DateLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function isValid($value) {


        // ログ開始
        logInfo(LOG_START);


        //--------------------------------------
        //■ActionForm 値から値を取得する
        //--------------------------------------
        // ▼月 ----------------------------------------------------------
        $month = '';
        if (! isBlankOrNull($this->_monthName)) { // 月の name 属性が指定されている場合

            // ActionForm に $this->_minhName が見つけられなければ例外を投げる（定義されていないのでシステムエラー）
            if (! array_key_exists($this->_monthName, $this->_data)) {
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ActionForm に {$this->_monthName} が見つかりません";
                logDebug($message);
                throw new Mikoshiva_Validate_Exception($message);
            }


            // 値を取得
            $month = $this->_data[$this->_monthName];

        } else {  // 月の name 属性が指定されていなければ 00 を代入
            $month = '01';
        }


        // ▼日 ----------------------------------------------------------
        $day = '';
        if (! isBlankOrNull($this->_dayName)) { // 日の name 属性が指定されている場合

            // ActionForm に $this->_minhName が見つけられなければ例外を投げる（定義されていないのでシステムエラー）
            if (! array_key_exists($this->_dayName, $this->_data)) {
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ActionForm に {$this->_dayName} が見つかりません";
                logDebug($message);
                throw new Mikoshiva_Validate_Exception($message);
            }


            // 値を取得
            $day = $this->_data[$this->_dayName];

        } else {  // 日の name 属性が指定されていなければ 00 を代入
            $day = '01';
        }


        //--------------------------------------
        //■連結
        //--------------------------------------
        $date = $value
              . '-'
              . $month
              . '-'
              . $day;

//dumper($date);
        //--------------------------------------
        //■正規表現でフォーマットチェックを行う
        //--------------------------------------
        if ( preg_match('#^\d{4}-\d{2}-\d{2}$#', $date) !== 1) {
            $this->_error(parent::FALSEFORMAT);
            logInfo(LOG_END);
            return false;
        }


        // ログ終了
        logInfo(LOG_END);
        return parent::isValid($date);
    }





    /**
     * リクエストデータ をセットします
     *
     * @param array $data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: DateLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {

        logInfo(LOG_START);
        $this->_data = $data;
        logInfo(LOG_END);
    }
}
























