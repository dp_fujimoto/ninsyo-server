<?php
/**
 * @package Mikoshiva_Filter
 */

/**
 * require
 */
require_once 'Zend/Filter/Input.php';

/**
 * 
 * 
 * @package Mikoshiva_Filter
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Input.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Filter_Input extends Zend_Filter_Input {




    /**
     * @var array Default namespaces, to search after user-defined namespaces.
     */
    protected $_namespaces = array('Mikoshiva_Filter', 'Zend_Filter', 'Mikoshiva_Validate', 'Zend_Validate');

    /**
     * @var array Default values to use when processing filters and validators.
     */
    protected $_defaults = array(
    	// 空文字も検証対象とするか。true無効false有効
        self::ALLOW_EMPTY         => true,

        // バリデータチェインであった場合に検証を続行するかのフラグtrue続行しないfalse続行
        self::BREAK_CHAIN         => true,

        self::ESCAPE_FILTER       => 'HtmlEntities',
        self::MISSING_MESSAGE     => "【 %COLUMN_NAME%】 はルール「%rule%」により必須ですが項目がありません",
        self::NOT_EMPTY_MESSAGE   => "【%COLUMN_NAME%】 の入力は必須です。",
        self::PRESENCE            => parent::PRESENCE_OPTIONAL
    );



    /**
     * @param string $type
     * @param mixed $classBaseName
     * @return Zend_Filter_Interface|Zend_Validate_Interface
     * @throws Zend_Filter_Exception
     */
    protected function _getFilterOrValidator($type, $classBaseName)
    {
        //-------------------------------------
        // 親メソッド
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        /*****
        $args = array();

        if (is_array($classBaseName)) {
            $args = $classBaseName;
            $classBaseName = array_shift($args);
        }

        $interfaceName = 'Zend_' . ucfirst($type) . '_Interface';
        $className = $this->getPluginLoader($type)->load(ucfirst($classBaseName));

        $class = new ReflectionClass($className);

        if (!$class->implementsInterface($interfaceName)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("Class '$className' based on basename '$classBaseName' must implement the '$interfaceName' interface");
        }

        if ($class->hasMethod('__construct')) {
            $object = $class->newInstanceArgs($args);
        } else {
            $object = $class->newInstance();
        }
        *****/

        $object = parent::_getFilterOrValidator($type, $classBaseName);

        if (method_exists($object, 'setData')) {
            $object->setData($this->_data);
        }

        return $object;
    }
}




















