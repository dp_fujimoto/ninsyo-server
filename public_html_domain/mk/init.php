<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/_SYSTEM_/config/define.php';
require_once 'Mikoshiva/Relay/Agent.php';


// セッションスタート
session_start();
/*$conf = array();
$conf = parse_ini_file('../config.ini', true );
*/
$basePath = MIKOSHIVA_ORDER_URL;

$agent  = null;
$agent  = new Mikoshiva_Relay_Agent();

$params = array();

$errorLogTime = date('Y-m-d H:i:s');

/*function googleChecker() {
//return true;
	error_log($errorLogTime . '['  . session_id() . ']' . print_r($_SERVER,true) . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

	$ipAddress = $_SERVER['REMOTE_ADDR'];

	if ($ipAddress !== null && $ipAddress !== '') {
		$ex = explode('.',$ipAddress);
		if (count($ex) === 4) {
			if ($ex[0] === '72' && $ex[1] === '14') {
				if ($ex[2] >= '0' && $ex[2] <= '255' && $ex[3] >= '0' && $ex[3] <= '255') {
					return true;
				}
			}
		}
	}
	return false;;
}*/

/**
 * 直パス参照oneclick用
 */
function oneclick($agent,$basePath,$errorLogTime) {

	// パラメータ追加
	$params                      = array();

	$id = null;
	$pKey = null;
	$transactionNumber = 2;

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}
	if (isset($_GET['pKey'])) {
		$pKey = $_GET['pKey'];
	}
	if ($pKey !== null && $pKey !== '') {
		$ex = explode('_',$pKey);
		$transactionNumber += count($ex);
	}

	$params['campaignSalesCode'] = $id;
	$params['campaignType'] = 'CAMPAIGN_TYPE_UP_SELL';
	$params['previewKey'] = $pKey;
	$params['transactionNumber'] = $transactionNumber;
	$params['addressNumber'] = '';
	$params['customerId'] = '';

	$params['chargeType'] = 'CHARGE_TYPE_CARD';
	$params['directInstallmentFlag'] = '0';
	$params['cardNo'] = '1111111111111111';
	$params['expirationDateMonth'] = '11';
	$params['expirationDateYear'] = '11';

	// mikoshivaにpost
    $response = $agent->getResponseByPost($basePath.'optimizer', $params);
	error_log($errorLogTime . '['  . session_id() . ']' . print_r($response,true) . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
    echo $response;
}

/**
 * 直パス参照upsell用
 */
function upsell($agent,$basePath,$errorLogTime) {

	// パラメータ追加
	$params                      = array();
	$id = null;
	$pKey = null;
	$transactionNumber = 2;

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}
	if (isset($_GET['pKey'])) {
		$pKey = $_GET['pKey'];
	}
	if ($pKey !== null && $pKey !== '') {
		$ex = explode('_',$pKey);
		$transactionNumber += count($ex);
	}

	$params['campaignSalesCode'] = $id;
	$params['campaignType'] = 'CAMPAIGN_TYPE_UP_SELL';
	$params['previewKey'] = $pKey;
	$params['transactionNumber'] = $transactionNumber;
	$params['addressNumber'] = '';
	$params['customerId'] = '';

	$params['chargeType'] = 'CHARGE_TYPE_CARD';
	$params['directInstallmentFlag'] = '0';
	$params['cardNo'] = '1111111111111111';
	$params['expirationDateMonth'] = '11';
	$params['expirationDateYear'] = '11';

	// mikoshivaにpost
    $response = $agent->getResponseByPost($basePath.'optimizer', $params);
	error_log($errorLogTime . '['  . session_id() . ']' . print_r($response,true) . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
    echo $response;
}

/**
 * 直パス参照downsell用
 */
function downsell($agent,$basePath,$errorLogTime) {

	// パラメータ追加
	$params                      = array();
	$id = null;
	$pKey = null;
	$transactionNumber = 2;

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}
	if (isset($_GET['pKey'])) {
		$pKey = $_GET['pKey'];
	}
	if ($pKey !== null && $pKey !== '') {
		$ex = explode('_',$pKey);
		$transactionNumber += count($ex);
	}

	$params['campaignSalesCode'] = $id;
	$params['campaignType'] = 'CAMPAIGN_TYPE_DOWN_SELL';
	$params['previewKey'] = $pKey;
	$params['transactionNumber'] = $transactionNumber;
	$params['addressNumber'] = '';
	$params['customerId'] = '';

	$params['chargeType'] = 'CHARGE_TYPE_CARD';
	$params['directInstallmentFlag'] = '0';
	$params['cardNo'] = '1111111111111111';
	$params['expirationDateMonth'] = '11';
	$params['expirationDateYear'] = '11';

	// mikoshivaにpost
    $response = $agent->getResponseByPost($basePath.'optimizer', $params);
	error_log($errorLogTime . '['  . session_id() . ']' . print_r($response,true) . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
    echo $response;
}