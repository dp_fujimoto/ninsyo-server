<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_customer_login_cookie
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxCustomerLoginCookie extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tcl';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'customerLoginCookieId' => null,
        'customerId' => null,
        'loginCookieValue' => null,
        'loginCookieKey' => null,
        'ipAddress' => null,
        'outputFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 顧客ログインクッキーID の値を返します
	 *
     * @return int 顧客ログインクッキーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerLoginCookieId() {
		return $this->_properties['customerLoginCookieId'];
    }


    /**
     * 顧客ログインクッキーID の値をセットします
	 *
	 * @param int $customerLoginCookieId 顧客ログインクッキーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerLoginCookieId($customerLoginCookieId) {
		$this->_properties['customerLoginCookieId'] = $customerLoginCookieId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * ログインクッキー値 の値を返します
	 *
     * @return char ログインクッキー値
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLoginCookieValue() {
		return $this->_properties['loginCookieValue'];
    }


    /**
     * ログインクッキー値 の値をセットします
	 *
	 * @param char $loginCookieValue ログインクッキー値
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLoginCookieValue($loginCookieValue) {
		$this->_properties['loginCookieValue'] = $loginCookieValue;
    }


    /**
     * ログインクッキーキー の値を返します
	 *
     * @return datetime ログインクッキーキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLoginCookieKey() {
		return $this->_properties['loginCookieKey'];
    }


    /**
     * ログインクッキーキー の値をセットします
	 *
	 * @param datetime $loginCookieKey ログインクッキーキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLoginCookieKey($loginCookieKey) {
		$this->_properties['loginCookieKey'] = $loginCookieKey;
    }


    /**
     * IPアドレス の値を返します
	 *
     * @return varchar IPアドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIpAddress() {
		return $this->_properties['ipAddress'];
    }


    /**
     * IPアドレス の値をセットします
	 *
	 * @param varchar $ipAddress IPアドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIpAddress($ipAddress) {
		$this->_properties['ipAddress'] = $ipAddress;
    }


    /**
     * 出力フラグ の値を返します
	 *
     * @return char 出力フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOutputFlag() {
		return $this->_properties['outputFlag'];
    }


    /**
     * 出力フラグ の値をセットします
	 *
	 * @param char $outputFlag 出力フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOutputFlag($outputFlag) {
		$this->_properties['outputFlag'] = $outputFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























