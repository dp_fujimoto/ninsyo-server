<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_TrxPurchaseAnalysis
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_TrxPurchaseAnalysis extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'trx_purchase_analysis';
    protected $_primary  = 'tpa_purchase_analysis_id';
    protected $_metadata = array (
  'tpa_purchase_analysis_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_purchase_analysis_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'tpa_purchase_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_purchase_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_sales_letter_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_sales_letter_id',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_campaign_sales_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_campaign_sales_id',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_traffic_source1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_traffic_source1',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_traffic_source2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_traffic_source2',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_traffic_source3' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_traffic_source3',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_advertisement_code' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_advertisement_code',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_magazine_source' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_magazine_source',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_user_agent' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_user_agent',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '1024',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_delete_flag',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_deletion_datetime',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_registration_datetime',
    'COLUMN_POSITION' => 13,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_update_datetime',
    'COLUMN_POSITION' => 14,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tpa_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_purchase_analysis',
    'COLUMN_NAME' => 'tpa_update_timestamp',
    'COLUMN_POSITION' => 15,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

