<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1>GMO 決済変更関連 - 決済取キャンセル（取消・返品・月跨返品）</h1>


            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <div class="system-message">
              <p>
                実行クラスは以下の場合をシステムエラーとみなして例外を投げます。<br>
                <span style="color: #bb0000">※GMO における決済の失敗（エラー）は false を返します。</span>
              </p>
              <ol>
                <li>必須の値がセットされていない場合</li>
                <li>既にキャンセル済みの取引だった場合</li>
                <li>GMO 決済 API への接続に失敗した場合</li>
                <li>何らかの理由でレスポンスが取得出来なかった場合</li>
              </ol>
            </div>

            <pre class="wiki"><?php
highlight_string('<?php

        //----------------------------------------------
        // キャンセルに必要なデータ(オーダーIDのみ)をセットして実行
        //----------------------------------------------

        //----------------------------------------------
        // 実行
        //----------------------------------------------
        include_once \'Mikoshiva/Settlement/Execution/Gmo/Alter/Cancel/Factory.php\';
        // インスタンス生成
        $cancelExec = null;
        $cancelExec = Mikoshiva_Settlement_Execution_Gmo_Alter_Cancel_Factory::getInstance($accessId);

        // 実行
        $ret             = $cancelExec->execute();   // true/false が返る（実行結果）
        $alterOutputPopo = $cancelExec->getOutput(); // Mikoshiva_Settlement_Gmo_Popo_Output_Alter オブジェクトが返る（処理結果）
'); ?>
            </pre>


            <h2>Mikoshiva_Settlement_Popo_Input_Gmo_Alter（GMO へ送信する値を保持する POPO</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>キャンセル処理に必須</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>ShopID</td>
                <td>●</td>
                <td>string</td>
                <td>ショップ ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>ShopPass</td>
                <td><br></td>
                <td>string</td>
                <td>ショップパスワード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>AccessID</td>
                <td>●</td>
                <td>string</td>
                <td>取引 ID</td>
                <td></td>
              </tr>
              <tr>
                <td>AccessPass</td>
                <td>●</td>
                <td>string</td>
                <td>取引パスワード</td>
                <td>（YYMM）形式</td>
              </tr>
              <tr>
                <td>JobCd</td>
                <td><br></td>
                <td>string</td>
                <td>処理区分</td>
                <td><pre>
VOID:    取消
RETURN:  返品
RETURNX: 月跨り返品
SALES:   実売上
                </pre></td>
              </tr>
              <tr>
                <td>Amount</td>
                <td><br></td>
                <td>string</td>
                <td>取引登録で指定した金額</td>
                <td>金額の妥当性チェック</td>
              </tr>
            </table>


            <h2>Mikoshiva_Settlement_Execution_Gmo_Alter_Cancel_Factory::getInstance</h2>
            <h3>Method</h3>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>

              </tr>
              <tr>
                <td>1</td>
                <td>$date</td>
                <td>string</td>
                <td>-</td>
                <td>決済日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$alterInputPopo</td>
                <td>Mikoshiva_Settlement_Popo_Input_Gmo_Alter</td>
                <td>-</td>
                <td><br></td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>Mikoshiva_Settlement_Execution_Gmo_Alter_Abstract</td>
                <td>-</td>
                <td>キャンセル処理実行クラス</td>
                <td><pre>
$date の日付が今日：              Mikoshiva_Settlement_Execution_Gmo_Alter_Void
$date の日付が昨日以前であり今月：Mikoshiva_Settlement_Execution_Gmo_Alter_Return
$date の日付が未来（仮売上）：    Mikoshiva_Settlement_Execution_Gmo_Alter_Return
$date の日付が先月以前：          Mikoshiva_Settlement_Execution_Gmo_Alter_ReturnX
                </pre></td>
              </tr>
            </table>




            <h2>Mikoshiva_Settlement_Gmo_Popo_Output_Alter</h2>
            <p>処理結果を保持します</p>

            <h3>プロパティ</h3>
            <p>POPO であるためプロパティの setter/getter が存在します</p>

            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>桁</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>AccessID</td>
                <td>CHARA</td>
                <td>32</td>
                <td>取引 ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>AccessPass</td>
                <td>CHARA</td>
                <td>32</td>
                <td>取引パスワード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Forward</td>
                <td>CHARA</td>
                <td>7</td>
                <td>仕向け先コード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Approve</td>
                <td>CHARA</td>
                <td>7</td>
                <td>承認番号</td>
                <td><br></td>
              </tr>
              <tr>
                <td>TranID</td>
                <td>CHARA</td>
                <td>28</td>
                <td>トランザクション ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>TranDate</td>
                <td>CHARA</td>
                <td>14</td>
                <td>決済日付</td>
                <td>yyyyMMddHHmmss 書式</td>
              </tr>
              <tr>
                <td>ErrCode</td>
                <td>CHARA</td>
                <td>3</td>
                <td>エラーコード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続さた文字列
              </td>
              </tr>
              <tr>
                <td>ErrInfo</td>
                <td>CHARA</td>
                <td>9</td>
                <td>エラー詳細コード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続さた文字列
              </td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















