<?php
/**
 * @package Mikoshiva_Settlement
 */

/**
 * require_once
 */
require_once 'Mikoshiva/Settlement/Execution/Order/Abstract.php';
require_once 'Mikoshiva/Utility/Gmo.php';

/**
 * カード決済以外の決済処理クラス
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/08
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Settlement_Execution_Order_Cash_Abstract extends Mikoshiva_Settlement_Execution_Order_Abstract {


    /**
     * 実行メソッド
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function execute() {


        $orderInputPopo = $this->_orderInputPopo;

        // ▼ショップIDを再設定
        $this->_resetShopId($orderInputPopo);


        //--------------------------------------------------------------------
        //
        // ■処理結果を詰め直す
        //
        //--------------------------------------------------------------------
        $output = null;
        $output = $this->_createSettlementOutput($orderInputPopo);
        $this->_orderOutputPopo = $output;


        return true;
    }




    /**
     * テスト用のクレジットカード番号で購入された際に強制的にテスト用のショップ ID に切り替えます
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/07
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _resetShopId(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo) {


        // ▼本番環境で有ればリターン
        if (REAL_FLAG === 1) {
            return true;
        }

        // ▼productList
        $ProductDataList = array();
        $ProductDataList = $orderInputPopo->getProductDataList();


        // ▼ショップ ID をセットし直します
        /* @var $productData Mikoshiva_Settlement_Popo_Input_Order_ProductInfo */
        foreach ($ProductDataList as $key => $productData) {

            // ショップ ID を取り出す
            $shopId = '';
            $shopId = $productData->getMstProduct()->getShopId();

            // ショップ情報を取得
            $shopInfo = array();
            $shopInfo = Mikoshiva_Utility_Gmo::getShopInfo($shopId);

            // ショップ ID をセットし直します
            $productData->getMstProduct()->setShopId($shopInfo['TEST_SHOP_ID']);
        }
    }








    /**
     * 処理結果クラスを詰め直す
     *
     * @param array $outputIput
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/30
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _createSettlementOutput(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo) {


        // ログ開始
        logInfo(LOG_START);


        include_once 'Mikoshiva/Settlement/Popo/Output/Order.php';
        $outputOrderPopo = null;
        $outputOrderPopo = new Mikoshiva_Settlement_Popo_Output_Order();
        $outputOrderPopo->setChargeType($orderInputPopo->getChargeType());
        $outputOrderPopo->setPaymentType($orderInputPopo->getPaymentType());


        //---------------------------------------------------------------
        // ▼$orderOutputPopoList[0][$outputOrderPopo][(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)normal] = Mikoshiva_Settlement_Popo_Output_Order_Settlement
        //                                            [(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)auth]   = Mikoshiva_Settlement_Popo_Output_Order_Settlement
        //---------------------------------------------------------------
        $settlementInfoPopoList = array();


        foreach ($orderInputPopo->getProductDataList() as $productDataIndex => $productDataValueList) { // foreach:03


            /* @var $productDataValueList Mikoshiva_Settlement_Popo_Input_Order_ProductInfo */
            $productDataValueList;

            include_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement/Info.php';
            $orderSettlementInfo = null;
            $orderSettlementInfo = new Mikoshiva_Settlement_Popo_Output_Order_Settlement_Info();

            //--------------------------------------------------------------------
            //
            // ▼決済データ毎のインスタンスを詰める
            //   ※仮売上の決済はクレジットカード決済しか存在しないため
            //
            //--------------------------------------------------------------------
            include_once 'Mikoshiva/Settlement/Popo/Output/Order.php';
            $orderSettlementInfo->setInputProductInfo($productDataValueList);
            $orderSettlementInfo->setMemberID(
                Mikoshiva_Utility_Gmo::createGmoMemberId($orderInputPopo->getCustomerId(), $productDataValueList->getPaymentNumber())
            );


            include_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement/List.php';
            $outputSettlementList = null;
            $outputSettlementList = new Mikoshiva_Settlement_Popo_Output_Order_Settlement_List();


            include_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement.php';
            $orderSettlement = null;
            $orderSettlement = new Mikoshiva_Settlement_Popo_Output_Order_Settlement();



            //--------------------------------------------------------------------
            //
            // ▼Output クラスのインスタンスを生成
            //   × AccessID           アクセス ID
            //   × AccessPass         アクセスパスワード
            //   ● OrderID            オーダー ID
            //   × ErrCode            エラーコード
            //   × ErrInfo            エラー詳細コード
            //   × ErrFlag            エラーフラグ 	０：成功　１：失敗
            //   ● paymentAmount      支払い料金
            //   ● paymenShippingCost 送料
            //   ● paymentDatetime    支払い日時
            //
            //--------------------------------------------------------------------
            // ▼オーダー ID -----------------------------------------------------
            $orderSettlement->setOrderID(
                Mikoshiva_Utility_Gmo::createGmoOrderId($orderInputPopo->getCustomerId(), $productDataValueList->getPaymentNumber())
            );



            // ▼商品金額 ---------------------------------------------------------
            $price = 0; // 初回価格（本体金額）
            $price = $productDataValueList->getMstCampaignProduct()->getPrice();
            $orderSettlement->setPaymentAmount($price);



            // ▼送料 ---------------------------------------------------------
            $shippingCost = 0; // 送料
            $shippingCost = $productDataValueList->getMstCampaignProduct()->getShippingCost();
            $orderSettlement->setPaymentShippingCost($shippingCost);


             // 仮売上フラグ ---------------------------------------------------------
            $orderSettlementInfo->setAuthFlag(
                $productDataValueList->getMstCampaignProduct()->getProvisorySalesFlag()
            );


            // ▼支払い日時 ---------------------------------------------------------
            $orderSettlement->setPaymentDatetime(Mikoshiva_Date::mikoshivaNow());



            // ▼エラーフラグ ---------------------------------------------------------
            $orderSettlement->setErrFlag(0);



            // ▼決済固有のセット --------------------------------------------------------------------------------------
            $outputSettlementList->setNormal($orderSettlement);
            $outputSettlementList->setAuth(new Mikoshiva_Settlement_Popo_Output_Order_Settlement());



            // ▼決済固有のセット --------------------------------------------------------------------------------------
            $orderSettlementInfo->setSettlementOutputList($outputSettlementList);



            // ▼決済固有のセット --------------------------------------------------------------------------------------
            $settlementInfoPopoList[] = $orderSettlementInfo;
        }


        $outputOrderPopo->setSettlementInfoList($settlementInfoPopoList);


        // ログ終了
        logInfo(LOG_END);

        return $outputOrderPopo;
    }

}








































