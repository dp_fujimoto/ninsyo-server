<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Zend/Db/Table/Abstract.php';
require_once 'Mikoshiva/Controller/Model/Class/Abstract.php';
require_once 'Mikoshiva/Db/Simple.php';

/**
 * Model_Class_Db_Abstract
 *
 * Models/Classes に置かれるデータベースアクセスを必要とする
 * 共通処理クラス、業務処理クラスは、必ずこのクラスを継承して下さい。
 *
 * @package Mikoshiva_Controller
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Model_Class_Db_Abstract extends Mikoshiva_Controller_Model_Class_Abstract {

    /**
     * DB クラス
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    /**
     * リクエストクラスと DB クラスを初期化します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @return unknown_type
     */
    public function __construct() {
        // スーパークラスのメソッドを呼びます
        parent::__construct();

        // 各種コンポーネントを取得
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
    }

}










