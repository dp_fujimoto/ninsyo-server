<?php
/**
 * @package Mikoshiva_Exception
 */

/**
 * require
 */
require_once 'Mikoshiva/Exception.php';


/**
 * Mikoshiva_Batch_Exception
 *
 * @package Mikoshiva_Exception
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Batch_Exception extends Mikoshiva_Exception {

}
