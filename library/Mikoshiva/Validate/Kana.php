<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * カナチェックを行います
 * 
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Kana.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Kana extends Zend_Validate_Abstract
{
    const STRING_EMPTY = 'stringEmpty';
    const INVALID      = 'noKana';

    /**
     * Digits filter used for validation
     *
     * @var Zend_Filter_Digits
     */
    protected static $_filter = null;

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::STRING_EMPTY => "%COLUMN_NAME% は必須項目です。",
        self::INVALID      => "%COLUMN_NAME% はカタカナで入力して下さい。",
    );


    /**
     * カナチェック
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Kana.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param unknown_type $value
     * @return string|string|string|string
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Interface#isValid($value)
     */
    public function isValid($value)
    {
        if( preg_match('/^[ァ-ヶー　 ]+$/u', $value ) !== 1 ) {
            $this->_error(self::INVALID);
            return false;
        }

        return true;
    }

}
