<?php
require_once 'Mikoshiva/Db/Simple.php';
require_once 'Mikoshiva/Controller/Model/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Popo/MstCampaign.php';
require_once 'Mikoshiva/Mail.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Error_Models_Error_ErrorFinder extends Mikoshiva_Controller_Model_Abstract {

    /**
     *
     * @var Mikoshiva_Db_Simple
     */
    private $_simple;

    public function __construct() {
        parent::__construct();
        $this->_simple = new Mikoshiva_Db_Simple();
    }
    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface &$actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $contex
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        // 設定ファイルから動作モードを取得する
        //$config = new Zend_Config_Ini('../application/lib/config.ini', null);
        //$debugFlg = ($config->debug == '1') ? true : false;
        // レスポンスオブジェクトを取得する
        //$response = new Zend_Controller_Response_Http();

        // ▼ロールバック
        try {
            Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
        } catch (Exception $e) {

        }

        // エラーハンドラを取得する
        $errors = $actionForm->get('error_handler');
        if(isBlankOrNull($errors)) {
            echo 'No Exception Found'."\n";
            return;
        }


        //--------------------------------------------------------------------
        // ■ 画面出力
        //--------------------------------------------------------------------
        // エラーの種別ごとにメッセージを表示する
        switch ($errors->type) {
            // コントローラが見つからない場合
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                //$response->setRawHeader('HTTP/1.1 404 Not Found');
                $context->getResponse()->setRawHeader('HTTP/1.1 404 Not Found'); // HTTP ステータス
                $this->_result = '404';
                return;
            // その他のエラーの場合
            default:
                $exception = $errors->exception;


                // メッセージがセットされていればそれを表示する
                if ($exception !== null) {


                    if (preg_match('#^WARNING : fopen\(.*?application\/default\/controllers#', $exception->getMessage()) === 1) {
                        $context->getResponse()->setRawHeader('HTTP/1.1 404 Not Found'); // HTTP ステータス
                        $this->_result = '404';
                        return;
                    }

                    // デバッグモードならエラーメッセージをセットする
                    $requestList = Zend_Controller_Front::getInstance()->getRequest()->getParams();
                    if (($requestList['module'] === 'order' && $requestList['controller'] === 'order' && REAL_FLAG === 1) === false) {
                        $debug  = nl2br(print_r($exception->getMessage(), true));
                        $debug .= '<br><br>';
                        $debug .= nl2br(print_r($exception->getTraceAsString() , true));

                        Zend_Debug::dump($exception->getMessage());
                        Zend_Debug::dump($exception->getTraceAsString());
                    }
                }
                break;
        }

        //------------------------------------------------------------------
        // 後は全て内部エラーの HTTP ステータスを返す
        //------------------------------------------------------------------
        $context->getResponse()->setRawHeader('HTTP/1.1 500 Internal Server Error'); // HTTP ステータス

        //--------------------------------------------------------------------
        // ■ ログ出力、DB更新、メール送信
        //--------------------------------------------------------------------
        // ErrorFinderのログを更新
        $this->_writeErrorLog($errors->exception, $context->getRequest());

        // エラーログテーブルを更新
        $this->_updateErrorLogTable($errors->exception, $context->getRequest());

        // エラーメールを送信
        $this->_sendMail($errors->exception, $context->getRequest());
    }

    /**
     * エラーログを更新します
     *
     * @param  Exception $exception
     * @param  Zend_Controller_Request_Http $request
     * @return
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/05/07
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    private function _writeErrorLog($exception, $request) {
        $errorLevel     = '';
        $errorMessage   = '';
        $fullPath       = '';
        $debugTrace     = '';
        $message        = '';
        $match = array();


        if(preg_match('/^(.*?):(.*)$/', $exception->getMessage(), $match) > 0) {
            $errorLevel    = trim($match[1]);
            $errorMessage  = trim($match[2]);
        } else {
            $errorLevel    = 'EMERG';
            $errorMessage  = $exception->getMessage();
        }

        $fullPath       = trim($exception->getFile());
        $debugTrace     = $exception->getTraceAsString();

        $message .= "\n";
        $message .= '**************************************************************' . "\n";
        $message .= 'エラーファイル:' . $fullPath . "\n";
        $message .= '**************************************************************' . "\n";
        $message .= 'メッセージ:' . $errorMessage . "\n";
        $message .= "\n";
        $message .= 'Debug Trace:' . "\n";
        $message .= $debugTrace . "\n";

        Mikoshiva_Logger::emerg($message);
    }



    /**
     * エラーログテーブルを更新します
     *
     * @param  Exception $exception
     * @param  Zend_Controller_Request_Http $request
     * @return
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/05/07
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    private function _updateErrorLogTable($exception, $request) {
        $staff = new Zend_Session_Namespace('staff');

        $staffId  = null;
        if($staff->mstStaff !== null) {
            /* @var $mstStaff Popo_MstStaff */
            $staffId  = $staff->mstStaff->getStaffId();
        }

        $match = array();
        if(preg_match('/^(.*?):(.*)$/', $exception->getMessage(), $match) > 0) {
            $errorLevel = trim($match[1]);
            $errorMsg   = trim($match[2]);
        } else {
            $errorLevel     = 'EMERG';
            $errorMsg   = $exception->getMessage();
        }


        $lastSql = null;
        if(Zend_Registry::getInstance()->isRegistered('LAST_EXECUTED_SQL') === true){
            $lastSql = Zend_Registry::get('LAST_EXECUTED_SQL');
        }

        $fullPath   = trim($exception->getFile());
        $fileName = basename($fullPath);

        //$data['errorLogId']       = ;                                  // エラーログＩＤ            Auto
        $data['errorLevel']       = $errorLevel;                         // エラーレベル
        $data['errorMsg']         = $errorMsg;                           // エラーメッセージ
        $data['errorSql']         = $lastSql;                            // エラーＳＱＬ
        $data['errorInfo']        = null;                                // エラー情報
        $data['executeFile']      = $fileName;                           // エラーファイル
        $data['executePath']      = $fullPath;                           // エラーファイル（フルパス）
        $data['executeAllPath']   = $exception->getTraceAsString();      // エラーファイル（デバッグトレース）
        $data['referrer']         = $request->getHeader('REFERER');      // リファラーパス
        $data['ipAddress']        = $request->getClientIp();             // IPアドレス
        $data['userId']           = $staffId;                            // オペレーター
        $data['deleteFlag']       = '0';

        try {
            $this->_simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstErrorLog', $data);
        } catch (Exception $e) {
            Mikoshiva_Logger::debug('{'.__CLASS__.'}#{'.__FUNCTION__.'} （'.__LINE__.'）】エラーログのDB登録に失敗しました');
        }
    }

    /**
     * エラーメールを送信します
     *
     * @param Exception $exception
     * @param Zend_Controller_Request_Http $request
     * @return
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/23
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    private function _sendMail($exception, $request) {
        $mail = new Mikoshiva_Mail();
//                $options = array(
//                        'auth'     => 'login',
//                        'ssl'      => 'ssl',
//                        'port'     => '465',
//                        'username' => 'mikoshivatest@gmail.com',
//                        'password' => 'miko11112222'
//                        );

        // メールサーバ情報
//        $tr = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $options);
//        $mail->setDefaultTransport($tr);

        $match = array();
        if(preg_match('/^(.*?):(.*)$/', $exception->getMessage(), $match) > 0) {
            $errorLevel    = trim($match[1]);
            $errorMessage  = trim($match[2]);
        } else {
            $errorLevel    = 'EMERG';
            $errorMessage  = $exception->getMessage();
        }

        $fullPath       = trim($exception->getFile());

        $replacements = array();
        $replacements['FULL_PATH']      = $fullPath;
        $replacements['ERROR_MESSAGE']  = $errorMessage;
        $replacements['DEBUG_TRACE']    = $exception->getTraceAsString();
        $replacements['REQUEST_URI']    = $request->getRequestUri();


        $template = $this->getTemplate(1);

        $mail->setSubject($template['subject']);
        $mail->setBodyTextReplace($template['body'], $replacements);
        //$mail->setBodyText($template['body'], $replacements);

        //$mail->setHtmlTextReplace('#DOKI_DOKI#してみる？', array('ドキドキ'));
        $mail->setFrom('admin@mikoshiva.com', 'Mikoshiva System');

        //$mail->addTo('kaihatsuteam@googlegroups.com', '開発チーム');
        $mail->addTo('direct.errors@gmail.com', 'Mikoshivaエラー受信');
        //$mail->addCc('elfaris@taupe.plala.or.jp', '谷口さん☆addCc');
        //$mail->addBcc('elfaris.iphone@gmail.com');
        try {
            if (method_exists($exception, 'getDisplayTrace') === true) {
                /* @var $exception Mikoshiva_Exception */
                errorMail($exception, 'Mikoshivaでエラーが起きました', $exception->getDisplayTrace());
            } else {
                /* @var $exception Exception */
                errorMail($exception, 'Mikoshivaでエラーが起きました');
            }
        } catch (Exception $e) {
            Mikoshiva_Logger::debug('{'.__CLASS__.'}#{'.__FUNCTION__.'} （'.__LINE__.'）】エラーメールの送信に失敗しました');
        }
    }

    /**
     * エラーメールのテンプレートを返します
     *
     * @param  $code メールテンプレート番号
     * @return array
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/23
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    public function getTemplate($code) {
        $template  = array();

        switch((string)$code) {
            case '1':
                $template['subject'] = '';
                $template['subject'] = 'Mikoshivaでエラーが起きました' . "\n";

                $template['body']  = '';
                $template['body'] .= 'Mikoshivaでエラーが起きました' . "\n";
                $template['body'] .= "\n";
                $template['body'] .= '**************************************************************' . "\n";
                $template['body'] .= 'エラーファイル: %FULL_PATH%' . "\n";
                $template['body'] .= '**************************************************************' . "\n";
                $template['body'] .= 'メッセージ: %ERROR_MESSAGE%' . "\n";
                $template['body'] .= 'リクエストURL: %REQUEST_URI%' . "\n";
                $template['body'] .= "\n";
                $template['body'] .= 'Debug Trace:' . "\n";
                $template['body'] .= '%DEBUG_TRACE%' . "\n";
                break;
            default:
        }
        return $template;
    }

}
