<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_CreateXml extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 出力内容
        $output = '';

        // リスト検索結果
        $contentList = $context->getResult('contentList');
        $context->setResult('contentList',null);

        if (is_array($contentList)) {

            if (count($contentList) > 0) {
                $output .= '<?xml version="1.0"?>';
                $output .= '<ios_project xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                $output .= '  <version>1.00</version>';
                $output .= '  <brands count="' . count($contentList) . '">';

                $brandIndex = 0;
                foreach ($contentList as $brandName => $brandData) {
                    $brandIndex++;
                    $output .= '    <brand index="' . $brandIndex . '">';
                    $output .= '      <title>' . htmlspecialchars($brandName) . '</title>';
                    $output .= '      <brand-code>' . htmlspecialchars($brandData['code']) . '</brand-code>';
                    $output .= '      <description>' . $brandData['des'] . '</description>';
                    $output .= '      <brand-logo src="' . $brandData['logo'] . '" key="' . $brandData['logoKey'] . '"/>';
                    $output .= '      <brand-banner-vertical src="' . $brandData['vertical'] . '" key="' . $brandData['verticalKey'] . '"/>';
                    $output .= '      <brand-banner-sideways src="' . $brandData['sideways'] . '" key="' . $brandData['sidewaysKey'] . '"/>';

                    $output .= '      <products count="' . count($brandData['data']) . '">';

                    $productIndex = 0;
                    foreach ($brandData['data'] as $productName => $productData) {
                        $productIndex++;
                        $output .= '        <product index="' . $productIndex . '">';
                        $output .= '          <title>' . htmlspecialchars($productName) . '</title>';
                        $output .= '          <description>' . htmlspecialchars($productData['des']) . '</description>';
                        $output .= '          <product-image src="' . $productData['image'] . '" key="' . $productData['imageKey'] . '"/>';


                        $output .= '          <contents count="' . count($productData['data']) . '">';

                        $contentIndex = 0;
                        foreach ($productData['data'] as $contentName => $contentData) {
                            $contentIndex++;
                            $output .= '            <content index="' . $contentIndex . '">';
                            $output .= '              <title>' . htmlspecialchars($contentName) . '</title>';
                            $output .= '              <description>' . htmlspecialchars($contentData['des']) . '</description>';
                            $output .= '              <content-image src="' . $contentData['image'] . '" key="' . $contentData['imageKey'] . '"/>';
                            $output .= '              <media type="' .$contentData['mediaType']  . '" protocol="download,streaming" src="' . $contentData['media'] . '" key="' . $contentData['mediaKey'] . '"/>';
                            $output .= '              <update format="Y/m/d">' . $contentData['date'] . '</update>';
                            $output .= '            </content>';
                        }

                        $output .= '          </contents>';
                        $output .= '        </product>';
                    }

                    $output .= '      </products>';
                    $output .= '    </brand>';

                    unset($contentList[$brandName]);
                }

                $output .= '  </brands>';
                $output .= '</ios_project>';

                file_put_contents(USER_CONTENT_XML_FILES . '/' . $actionForm->getHash(),$output);
            }
        } else if (!isBlankOrNull($contentList)) {
            $output = $contentList;
        }

        $response = null;
        $response = $context->getResponse();

        // レスポンスをセット
        $response->setHeader('Content-type', 'text/xml');
        $response->appendBody($output);

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
