<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_inquiry_detail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxInquiryDetail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tid';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'inquiryDetailId' => null,
        'inquiryId' => null,
        'inquiryBranchNumber' => null,
        'inquiryCustomerId' => null,
        'customerId' => null,
        'formerInquiryDetailId' => null,
        'transceivingType' => null,
        'subject' => null,
        'body' => null,
        'inquiryWay' => null,
        'header' => null,
        'source' => null,
        'destinationTo' => null,
        'destinationCc' => null,
        'destinationBcc' => null,
        'destinationReplyTo' => null,
        'divisionValue' => null,
        'inquiryType' => null,
        'inquiryLabelId' => null,
        'inquiryStatus' => null,
        'staffId' => null,
        'inquiryGroupingId1' => null,
        'inquiryGroupingId2' => null,
        'inquiryGroupingId3' => null,
        'appliedSortRuleId' => null,
        'appliedTemplateId' => null,
        'appliedAddressId' => null,
        'appliedSignatureId' => null,
        'updateAllowingFlag' => null,
        'trashFlag' => null,
        'receptionDatetime' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 問合せ詳細ID の値を返します
	 *
     * @return int 問合せ詳細ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryDetailId() {
		return $this->_properties['inquiryDetailId'];
    }


    /**
     * 問合せ詳細ID の値をセットします
	 *
	 * @param int $inquiryDetailId 問合せ詳細ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryDetailId($inquiryDetailId) {
		$this->_properties['inquiryDetailId'] = $inquiryDetailId;
    }


    /**
     * 問合せID の値を返します
	 *
     * @return int 問合せID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryId() {
		return $this->_properties['inquiryId'];
    }


    /**
     * 問合せID の値をセットします
	 *
	 * @param int $inquiryId 問合せID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryId($inquiryId) {
		$this->_properties['inquiryId'] = $inquiryId;
    }


    /**
     * 枝番 の値を返します
	 *
     * @return int 枝番
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryBranchNumber() {
		return $this->_properties['inquiryBranchNumber'];
    }


    /**
     * 枝番 の値をセットします
	 *
	 * @param int $inquiryBranchNumber 枝番
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryBranchNumber($inquiryBranchNumber) {
		$this->_properties['inquiryBranchNumber'] = $inquiryBranchNumber;
    }


    /**
     * 問合せ顧客ID の値を返します
	 *
     * @return int 問合せ顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryCustomerId() {
		return $this->_properties['inquiryCustomerId'];
    }


    /**
     * 問合せ顧客ID の値をセットします
	 *
	 * @param int $inquiryCustomerId 問合せ顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryCustomerId($inquiryCustomerId) {
		$this->_properties['inquiryCustomerId'] = $inquiryCustomerId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 対応元問合せ詳細ID の値を返します
	 *
     * @return int 対応元問合せ詳細ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFormerInquiryDetailId() {
		return $this->_properties['formerInquiryDetailId'];
    }


    /**
     * 対応元問合せ詳細ID の値をセットします
	 *
	 * @param int $formerInquiryDetailId 対応元問合せ詳細ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFormerInquiryDetailId($formerInquiryDetailId) {
		$this->_properties['formerInquiryDetailId'] = $formerInquiryDetailId;
    }


    /**
     * 送受信タイプ の値を返します
	 *
     * @return varchar 送受信タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransceivingType() {
		return $this->_properties['transceivingType'];
    }


    /**
     * 送受信タイプ の値をセットします
	 *
	 * @param varchar $transceivingType 送受信タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransceivingType($transceivingType) {
		$this->_properties['transceivingType'] = $transceivingType;
    }


    /**
     * 件名 の値を返します
	 *
     * @return varchar 件名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubject() {
		return $this->_properties['subject'];
    }


    /**
     * 件名 の値をセットします
	 *
	 * @param varchar $subject 件名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubject($subject) {
		$this->_properties['subject'] = $subject;
    }


    /**
     * 本文 の値を返します
	 *
     * @return mediumtext 本文
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBody() {
		return $this->_properties['body'];
    }


    /**
     * 本文 の値をセットします
	 *
	 * @param mediumtext $body 本文
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBody($body) {
		$this->_properties['body'] = $body;
    }


    /**
     * 問合せ方法 の値を返します
	 *
     * @return varchar 問合せ方法
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryWay() {
		return $this->_properties['inquiryWay'];
    }


    /**
     * 問合せ方法 の値をセットします
	 *
	 * @param varchar $inquiryWay 問合せ方法
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryWay($inquiryWay) {
		$this->_properties['inquiryWay'] = $inquiryWay;
    }


    /**
     * メールヘッダ の値を返します
	 *
     * @return text メールヘッダ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHeader() {
		return $this->_properties['header'];
    }


    /**
     * メールヘッダ の値をセットします
	 *
	 * @param text $header メールヘッダ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHeader($header) {
		$this->_properties['header'] = $header;
    }


    /**
     * 送信元(From) の値を返します
	 *
     * @return varchar 送信元(From)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSource() {
		return $this->_properties['source'];
    }


    /**
     * 送信元(From) の値をセットします
	 *
	 * @param varchar $source 送信元(From)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSource($source) {
		$this->_properties['source'] = $source;
    }


    /**
     * 送信先(to) の値を返します
	 *
     * @return text 送信先(to)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDestinationTo() {
		return $this->_properties['destinationTo'];
    }


    /**
     * 送信先(to) の値をセットします
	 *
	 * @param text $destinationTo 送信先(to)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDestinationTo($destinationTo) {
		$this->_properties['destinationTo'] = $destinationTo;
    }


    /**
     * 送信先(Cc) の値を返します
	 *
     * @return text 送信先(Cc)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDestinationCc() {
		return $this->_properties['destinationCc'];
    }


    /**
     * 送信先(Cc) の値をセットします
	 *
	 * @param text $destinationCc 送信先(Cc)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDestinationCc($destinationCc) {
		$this->_properties['destinationCc'] = $destinationCc;
    }


    /**
     * 送信先(Bcc) の値を返します
	 *
     * @return text 送信先(Bcc)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDestinationBcc() {
		return $this->_properties['destinationBcc'];
    }


    /**
     * 送信先(Bcc) の値をセットします
	 *
	 * @param text $destinationBcc 送信先(Bcc)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDestinationBcc($destinationBcc) {
		$this->_properties['destinationBcc'] = $destinationBcc;
    }


    /**
     * 送信先(Reply-to) の値を返します
	 *
     * @return text 送信先(Reply-to)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDestinationReplyTo() {
		return $this->_properties['destinationReplyTo'];
    }


    /**
     * 送信先(Reply-to) の値をセットします
	 *
	 * @param text $destinationReplyTo 送信先(Reply-to)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDestinationReplyTo($destinationReplyTo) {
		$this->_properties['destinationReplyTo'] = $destinationReplyTo;
    }


    /**
     * 部署値 の値を返します
	 *
     * @return int 部署値
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionValue() {
		return $this->_properties['divisionValue'];
    }


    /**
     * 部署値 の値をセットします
	 *
	 * @param int $divisionValue 部署値
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionValue($divisionValue) {
		$this->_properties['divisionValue'] = $divisionValue;
    }


    /**
     * 問合せタイプ の値を返します
	 *
     * @return varchar 問合せタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryType() {
		return $this->_properties['inquiryType'];
    }


    /**
     * 問合せタイプ の値をセットします
	 *
	 * @param varchar $inquiryType 問合せタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryType($inquiryType) {
		$this->_properties['inquiryType'] = $inquiryType;
    }


    /**
     * 問合せラベルID の値を返します
	 *
     * @return int 問合せラベルID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryLabelId() {
		return $this->_properties['inquiryLabelId'];
    }


    /**
     * 問合せラベルID の値をセットします
	 *
	 * @param int $inquiryLabelId 問合せラベルID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryLabelId($inquiryLabelId) {
		$this->_properties['inquiryLabelId'] = $inquiryLabelId;
    }


    /**
     * 問合せステータス の値を返します
	 *
     * @return varchar 問合せステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryStatus() {
		return $this->_properties['inquiryStatus'];
    }


    /**
     * 問合せステータス の値をセットします
	 *
	 * @param varchar $inquiryStatus 問合せステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryStatus($inquiryStatus) {
		$this->_properties['inquiryStatus'] = $inquiryStatus;
    }


    /**
     * 担当者ID の値を返します
	 *
     * @return int 担当者ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * 担当者ID の値をセットします
	 *
	 * @param int $staffId 担当者ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * 問合せ分類ID１ の値を返します
	 *
     * @return int 問合せ分類ID１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryGroupingId1() {
		return $this->_properties['inquiryGroupingId1'];
    }


    /**
     * 問合せ分類ID１ の値をセットします
	 *
	 * @param int $inquiryGroupingId1 問合せ分類ID１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryGroupingId1($inquiryGroupingId1) {
		$this->_properties['inquiryGroupingId1'] = $inquiryGroupingId1;
    }


    /**
     * 問合せ分類ID２ の値を返します
	 *
     * @return int 問合せ分類ID２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryGroupingId2() {
		return $this->_properties['inquiryGroupingId2'];
    }


    /**
     * 問合せ分類ID２ の値をセットします
	 *
	 * @param int $inquiryGroupingId2 問合せ分類ID２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryGroupingId2($inquiryGroupingId2) {
		$this->_properties['inquiryGroupingId2'] = $inquiryGroupingId2;
    }


    /**
     * 問合せ分類ID３ の値を返します
	 *
     * @return int 問合せ分類ID３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryGroupingId3() {
		return $this->_properties['inquiryGroupingId3'];
    }


    /**
     * 問合せ分類ID３ の値をセットします
	 *
	 * @param int $inquiryGroupingId3 問合せ分類ID３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryGroupingId3($inquiryGroupingId3) {
		$this->_properties['inquiryGroupingId3'] = $inquiryGroupingId3;
    }


    /**
     * 適用振分ルールＩＤ の値を返します
	 *
     * @return varchar 適用振分ルールＩＤ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAppliedSortRuleId() {
		return $this->_properties['appliedSortRuleId'];
    }


    /**
     * 適用振分ルールＩＤ の値をセットします
	 *
	 * @param varchar $appliedSortRuleId 適用振分ルールＩＤ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAppliedSortRuleId($appliedSortRuleId) {
		$this->_properties['appliedSortRuleId'] = $appliedSortRuleId;
    }


    /**
     * 適用テンプレートＩＤ の値を返します
	 *
     * @return int 適用テンプレートＩＤ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAppliedTemplateId() {
		return $this->_properties['appliedTemplateId'];
    }


    /**
     * 適用テンプレートＩＤ の値をセットします
	 *
	 * @param int $appliedTemplateId 適用テンプレートＩＤ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAppliedTemplateId($appliedTemplateId) {
		$this->_properties['appliedTemplateId'] = $appliedTemplateId;
    }


    /**
     * 適用宛先ＩＤ の値を返します
	 *
     * @return int 適用宛先ＩＤ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAppliedAddressId() {
		return $this->_properties['appliedAddressId'];
    }


    /**
     * 適用宛先ＩＤ の値をセットします
	 *
	 * @param int $appliedAddressId 適用宛先ＩＤ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAppliedAddressId($appliedAddressId) {
		$this->_properties['appliedAddressId'] = $appliedAddressId;
    }


    /**
     * 適用署名ＩＤ の値を返します
	 *
     * @return int 適用署名ＩＤ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAppliedSignatureId() {
		return $this->_properties['appliedSignatureId'];
    }


    /**
     * 適用署名ＩＤ の値をセットします
	 *
	 * @param int $appliedSignatureId 適用署名ＩＤ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAppliedSignatureId($appliedSignatureId) {
		$this->_properties['appliedSignatureId'] = $appliedSignatureId;
    }


    /**
     * 更新許可ﾌﾗｸﾞ の値を返します
	 *
     * @return char 更新許可ﾌﾗｸﾞ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateAllowingFlag() {
		return $this->_properties['updateAllowingFlag'];
    }


    /**
     * 更新許可ﾌﾗｸﾞ の値をセットします
	 *
	 * @param char $updateAllowingFlag 更新許可ﾌﾗｸﾞ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateAllowingFlag($updateAllowingFlag) {
		$this->_properties['updateAllowingFlag'] = $updateAllowingFlag;
    }


    /**
     * ゴミ箱ﾌﾗｸﾞ の値を返します
	 *
     * @return char ゴミ箱ﾌﾗｸﾞ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTrashFlag() {
		return $this->_properties['trashFlag'];
    }


    /**
     * ゴミ箱ﾌﾗｸﾞ の値をセットします
	 *
	 * @param char $trashFlag ゴミ箱ﾌﾗｸﾞ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTrashFlag($trashFlag) {
		$this->_properties['trashFlag'] = $trashFlag;
    }


    /**
     * 受付日 の値を返します
	 *
     * @return datetime 受付日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReceptionDatetime() {
		return $this->_properties['receptionDatetime'];
    }


    /**
     * 受付日 の値をセットします
	 *
	 * @param datetime $receptionDatetime 受付日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReceptionDatetime($receptionDatetime) {
		$this->_properties['receptionDatetime'] = $receptionDatetime;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























