<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_dm_campaign_planning
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstDmCampaignPlanning extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mdp';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'dmCampaignPlanningId' => null,
        'dmCampaignId' => null,
        'planKey' => null,
        'planType' => null,
        'advertisementId' => null,
        'shippingDate' => null,
        'listNumber' => null,
        'purchaseNumber' => null,
        'conversion' => null,
        'conversionEvaluation' => null,
        'totalSaleAmount' => null,
        'salesEvaluation' => null,
        'returnOnInvestment' => null,
        'roiEvaluation' => null,
        'totalCost' => null,
        'perCopyCost' => null,
        'memo' => null,
        'downloadFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mdp_dm_campaign_planning_id の値を返します
	 *
     * @return int mdp_dm_campaign_planning_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignPlanningId() {
		return $this->_properties['dmCampaignPlanningId'];
    }


    /**
     * mdp_dm_campaign_planning_id の値をセットします
	 *
	 * @param int $dmCampaignPlanningId mdp_dm_campaign_planning_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignPlanningId($dmCampaignPlanningId) {
		$this->_properties['dmCampaignPlanningId'] = $dmCampaignPlanningId;
    }


    /**
     * mdp_dm_campaign_id の値を返します
	 *
     * @return int mdp_dm_campaign_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignId() {
		return $this->_properties['dmCampaignId'];
    }


    /**
     * mdp_dm_campaign_id の値をセットします
	 *
	 * @param int $dmCampaignId mdp_dm_campaign_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignId($dmCampaignId) {
		$this->_properties['dmCampaignId'] = $dmCampaignId;
    }


    /**
     * mdp_plan_key の値を返します
	 *
     * @return char mdp_plan_key
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPlanKey() {
		return $this->_properties['planKey'];
    }


    /**
     * mdp_plan_key の値をセットします
	 *
	 * @param char $planKey mdp_plan_key
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPlanKey($planKey) {
		$this->_properties['planKey'] = $planKey;
    }


    /**
     * mdp_plan_type の値を返します
	 *
     * @return varchar mdp_plan_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPlanType() {
		return $this->_properties['planType'];
    }


    /**
     * mdp_plan_type の値をセットします
	 *
	 * @param varchar $planType mdp_plan_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPlanType($planType) {
		$this->_properties['planType'] = $planType;
    }


    /**
     * mdp_advertisement_id の値を返します
	 *
     * @return int mdp_advertisement_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAdvertisementId() {
		return $this->_properties['advertisementId'];
    }


    /**
     * mdp_advertisement_id の値をセットします
	 *
	 * @param int $advertisementId mdp_advertisement_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAdvertisementId($advertisementId) {
		$this->_properties['advertisementId'] = $advertisementId;
    }


    /**
     * mdp_shipping_date の値を返します
	 *
     * @return date mdp_shipping_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingDate() {
		return $this->_properties['shippingDate'];
    }


    /**
     * mdp_shipping_date の値をセットします
	 *
	 * @param date $shippingDate mdp_shipping_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingDate($shippingDate) {
		$this->_properties['shippingDate'] = $shippingDate;
    }


    /**
     * mdp_list_number の値を返します
	 *
     * @return int mdp_list_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getListNumber() {
		return $this->_properties['listNumber'];
    }


    /**
     * mdp_list_number の値をセットします
	 *
	 * @param int $listNumber mdp_list_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setListNumber($listNumber) {
		$this->_properties['listNumber'] = $listNumber;
    }


    /**
     * mdp_purchase_number の値を返します
	 *
     * @return int mdp_purchase_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseNumber() {
		return $this->_properties['purchaseNumber'];
    }


    /**
     * mdp_purchase_number の値をセットします
	 *
	 * @param int $purchaseNumber mdp_purchase_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseNumber($purchaseNumber) {
		$this->_properties['purchaseNumber'] = $purchaseNumber;
    }


    /**
     * mdp_conversion の値を返します
	 *
     * @return float mdp_conversion
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getConversion() {
		return $this->_properties['conversion'];
    }


    /**
     * mdp_conversion の値をセットします
	 *
	 * @param float $conversion mdp_conversion
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setConversion($conversion) {
		$this->_properties['conversion'] = $conversion;
    }


    /**
     * mdp_conversion_evaluation の値を返します
	 *
     * @return int mdp_conversion_evaluation
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getConversionEvaluation() {
		return $this->_properties['conversionEvaluation'];
    }


    /**
     * mdp_conversion_evaluation の値をセットします
	 *
	 * @param int $conversionEvaluation mdp_conversion_evaluation
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setConversionEvaluation($conversionEvaluation) {
		$this->_properties['conversionEvaluation'] = $conversionEvaluation;
    }


    /**
     * mdp_total_sale_amount の値を返します
	 *
     * @return int mdp_total_sale_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTotalSaleAmount() {
		return $this->_properties['totalSaleAmount'];
    }


    /**
     * mdp_total_sale_amount の値をセットします
	 *
	 * @param int $totalSaleAmount mdp_total_sale_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTotalSaleAmount($totalSaleAmount) {
		$this->_properties['totalSaleAmount'] = $totalSaleAmount;
    }


    /**
     * mdp_sales_evaluation の値を返します
	 *
     * @return int mdp_sales_evaluation
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesEvaluation() {
		return $this->_properties['salesEvaluation'];
    }


    /**
     * mdp_sales_evaluation の値をセットします
	 *
	 * @param int $salesEvaluation mdp_sales_evaluation
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesEvaluation($salesEvaluation) {
		$this->_properties['salesEvaluation'] = $salesEvaluation;
    }


    /**
     * mdp_return_on_investment の値を返します
	 *
     * @return float mdp_return_on_investment
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnOnInvestment() {
		return $this->_properties['returnOnInvestment'];
    }


    /**
     * mdp_return_on_investment の値をセットします
	 *
	 * @param float $returnOnInvestment mdp_return_on_investment
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnOnInvestment($returnOnInvestment) {
		$this->_properties['returnOnInvestment'] = $returnOnInvestment;
    }


    /**
     * mdp_roi_evaluation の値を返します
	 *
     * @return int mdp_roi_evaluation
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRoiEvaluation() {
		return $this->_properties['roiEvaluation'];
    }


    /**
     * mdp_roi_evaluation の値をセットします
	 *
	 * @param int $roiEvaluation mdp_roi_evaluation
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRoiEvaluation($roiEvaluation) {
		$this->_properties['roiEvaluation'] = $roiEvaluation;
    }


    /**
     * mdp_total_cost の値を返します
	 *
     * @return int mdp_total_cost
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTotalCost() {
		return $this->_properties['totalCost'];
    }


    /**
     * mdp_total_cost の値をセットします
	 *
	 * @param int $totalCost mdp_total_cost
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTotalCost($totalCost) {
		$this->_properties['totalCost'] = $totalCost;
    }


    /**
     * mdp_per_copy_cost の値を返します
	 *
     * @return float mdp_per_copy_cost
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPerCopyCost() {
		return $this->_properties['perCopyCost'];
    }


    /**
     * mdp_per_copy_cost の値をセットします
	 *
	 * @param float $perCopyCost mdp_per_copy_cost
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPerCopyCost($perCopyCost) {
		$this->_properties['perCopyCost'] = $perCopyCost;
    }


    /**
     * mdp_memo の値を返します
	 *
     * @return varchar mdp_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * mdp_memo の値をセットします
	 *
	 * @param varchar $memo mdp_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * mdp_download_flag の値を返します
	 *
     * @return char mdp_download_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDownloadFlag() {
		return $this->_properties['downloadFlag'];
    }


    /**
     * mdp_download_flag の値をセットします
	 *
	 * @param char $downloadFlag mdp_download_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDownloadFlag($downloadFlag) {
		$this->_properties['downloadFlag'] = $downloadFlag;
    }


    /**
     * mdp_delete_flag の値を返します
	 *
     * @return char mdp_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mdp_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mdp_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mdp_deletion_datetime の値を返します
	 *
     * @return datetime mdp_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mdp_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mdp_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mdp_registration_datetime の値を返します
	 *
     * @return datetime mdp_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mdp_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mdp_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mdp_update_datetime の値を返します
	 *
     * @return datetime mdp_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mdp_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mdp_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mdp_update_timestamp の値を返します
	 *
     * @return timestamp mdp_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mdp_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mdp_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























