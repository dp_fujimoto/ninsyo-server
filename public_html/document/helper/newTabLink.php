<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/helper">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> ヘルパー</h1>


            <p>新しいタブを開く javascript をリンク (a タグ ) に実装した形で返します。 </p>
<!-- =================================================================
    ■<?php echo basename(__FILE__, '.php') ?>■
====================================================================== -->
          <table class="wiki">
            <tr>
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>name</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>リンク名</td>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>saveName</td>
              <td>string</td>
              <td>-</td>
              <td>空文字</td>
              <td>closeTabLocation ヘルパーでこの画面に戻る際の目印となる名前をセットします</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{newTabLink name="新しいタブボタン 3" formId="tab3From" saveName="test_AAA" url="/test1/test2/test3"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
<a href="#" onclick="KO_newTab(\'/test1/test2/test3\', \'tab3From\', \'test_AAA\');">新しいタブボタン 3</a>
'); ?>
            </pre>


  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>


