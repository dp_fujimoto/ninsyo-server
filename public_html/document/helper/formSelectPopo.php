<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（ヘルパー） ドキュメント &gt; ヘルパー 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/helper">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> ヘルパー</h1>


            <p>同種のPopoの配列を元に、セレクトボックスを生成して返します。  </p>
<!-- =================================================================
    ■<?php echo basename(__FILE__, '.php') ?>■
====================================================================== -->
          <table class="wiki">
            <tr>
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">name
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">○</td>
            <td id="html_select_timeLister" class="default">-</td>
            <td id="html_select_timeLister" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">selected
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> selectedにする値
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">options
</td>
            <td id="html_select_timeLister" class="type">array
</td>
            <td id="html_select_timeLister" class="indispensable">○</td>
            <td id="html_select_timeLister" class="default">-</td>
            <td id="html_select_timeLister" class="summary">同種のpopoの配列
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">valueName
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">○</td>
            <td id="html_select_timeLister" class="default">-</td>
            <td id="html_select_timeLister" class="summary"> value に入力させたいプロパティ名
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">labelName
</td>
            <td id="html_select_timeLister" class="type">string|array
</td>
            <td id="html_select_timeLister" class="indispensable">○</td>
            <td id="html_select_timeLister" class="default">-</td>
            <td id="html_select_timeLister" class="summary"> ラベルに表示されたいプロパティ名またはそのリスト
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">separator
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> ラベルが複数指定された時の区切り文字
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">attribs
</td>
            <td id="html_select_timeLister" class="type">array
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">array()</td>
            <td id="html_select_timeLister" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">addEmpty
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">false</td>
            <td id="html_select_timeLister" class="summary"> リストに空白を追加(デフォルト：無効)
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">addAllItem
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">false</td>
            <td id="html_select_timeLister" class="summary"> リストに｢全て｣を追加(デフォルト：無効)
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">listsep
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">"<br>\n"</td>
            <td id="html_select_timeLister" class="summary"> 各エンティティを区切る文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('
{formSelectPopo name="shippingIdForm" options=$shippingList selected="38"  valueName="shippingId" labelName="lastName" listsep="<br>\n"}
 ')?>
            </pre>

            <h2 id="使用例">結果</h2>

            <span class="wiki"><?php
highlight_string('
<select name="shippingIdForm" id="shippingIdForm">
<option label="関屋7002" value="38" selected="selected">関屋7002</option>
<option label="関屋7000" value="36">関屋7000</option>

<option label="関屋7001" value="37">関屋7001</option>
</select>



'); ?>
</span>
            <h2 id="使用例">結果(表示)</h2>
<select name="shippingIdForm" id="shippingIdForm">
<option label="関屋7002" value="38" selected="selected">関屋7002</option>
<option label="関屋7000" value="36">関屋7000</option>

<option label="関屋7001" value="37">関屋7001</option>
</select>

            <h2 id="使用例">サンプルのデータ</h2>
            <pre class="wiki">
array(3) {
  [0] =&gt; object(Popo_TrxTestShipping)#99 (2) {
    ["_columnPrefix:protected"] =&gt; string(3) "tts"
    ["_properties:protected"] =&gt; array(43) {
      ["shippingId"] =&gt; string(2) "38"
      ["customerId"] =&gt; string(4) "7002"
      ["divisionId"] =&gt; string(1) "1"
      ["relativeShippingId"] =&gt; NULL
      ["campaignSalesCode"] =&gt; NULL
      ["campaignCode"] =&gt; string(6) "OF_PBR"
      ["productShippingCode"] =&gt; string(3) "PBR"
      ["packageType"] =&gt; string(21) "PACKAGE_TYPE_DELIVERY"
      ["packageCode"] =&gt; string(5) "PBR-1"
      ["packageNumber"] =&gt; string(1) "1"
      ["chargeType"] =&gt; string(28) "CHARGE_TYPE_CASH_ON_DELIVERY"
      ["stepType"] =&gt; string(14) "STEP_TYPE_NONE"
      ["stepCount"] =&gt; string(1) "0"
      ["shippingGroupLabel"] =&gt; NULL
      ["companyName"] =&gt; NULL
      ["lastName"] =&gt; string(10) "関屋7002"
      ["firstName"] =&gt; string(9) "テスト"
      ["lastNameKana"] =&gt; string(9) "ヤマダ"
      ["firstNameKana"] =&gt; string(9) "タロウ"
      ["zip"] =&gt; string(8) "541-0059"
      ["prefecture"] =&gt; string(9) "大阪府"
      ["address1"] =&gt; string(36) "大阪市中央区博労町1－7－7"
      ["address2"] =&gt; string(23) "中央博労町ビル9F"
      ["tel"] =&gt; string(12) "06-6268-0850"
      ["shippingType"] =&gt; string(22) "SHIPPING_TYPE_TOGETHER"
      ["expectedShippingDate"] =&gt; string(10) "2010-08-21"
      ["paymentDeliveryPrice"] =&gt; string(4) "1920"
      ["shippingAssign"] =&gt; NULL
      ["builderId1"] =&gt; NULL
      ["builderId2"] =&gt; NULL
      ["builderId3"] =&gt; NULL
      ["builderId4"] =&gt; NULL
      ["builderId5"] =&gt; NULL
      ["shippingStatus"] =&gt; string(24) "SHIPPING_STATUS_COMPLETE"
      ["realShippingDate"] =&gt; string(10) "2010-08-25"
      ["inquiryNumber"] =&gt; string(2) "38"
      ["shippingStopStatus"] =&gt; string(1) "0"
      ["dirtyFlag"] =&gt; string(1) "0"
      ["deleteFlag"] =&gt; string(1) "0"
      ["deletionDatetime"] =&gt; NULL
      ["registrationDatetime"] =&gt; string(19) "2010-08-20 15:53:35"
      ["updateDatetime"] =&gt; string(19) "2010-08-24 15:33:36"
      ["updateTimestamp"] =&gt; string(19) "2010-08-30 18:33:18"
    }
  }
  [1] =&gt; object(Popo_TrxTestShipping)#90 (2) {
    ["_columnPrefix:protected"] =&gt; string(3) "tts"
    ["_properties:protected"] =&gt; array(43) {
      ["shippingId"] =&gt; string(2) "36"
      ["customerId"] =&gt; string(4) "7001"
      ["divisionId"] =&gt; string(1) "1"
      ["relativeShippingId"] =&gt; NULL
      ["campaignSalesCode"] =&gt; NULL
      ["campaignCode"] =&gt; string(10) "OF_PBR_MLT"
      ["productShippingCode"] =&gt; string(3) "PBR"
      ["packageType"] =&gt; string(21) "PACKAGE_TYPE_DELIVERY"
      ["packageCode"] =&gt; string(5) "PBR-1"
      ["packageNumber"] =&gt; string(1) "1"
      ["chargeType"] =&gt; string(16) "CHARGE_TYPE_CARD"
      ["stepType"] =&gt; string(14) "STEP_TYPE_NONE"
      ["stepCount"] =&gt; string(1) "0"
      ["shippingGroupLabel"] =&gt; NULL
      ["companyName"] =&gt; NULL
      ["lastName"] =&gt; string(10) "関屋7000"
      ["firstName"] =&gt; string(9) "テスト"
      ["lastNameKana"] =&gt; string(9) "ヤマダ"
      ["firstNameKana"] =&gt; string(9) "タロウ"
      ["zip"] =&gt; string(8) "541-0059"
      ["prefecture"] =&gt; string(9) "大阪府"
      ["address1"] =&gt; string(36) "大阪市中央区博労町1－7－7"
      ["address2"] =&gt; string(23) "中央博労町ビル9F"
      ["tel"] =&gt; string(12) "06-6268-0850"
      ["shippingType"] =&gt; string(22) "SHIPPING_TYPE_TOGETHER"
      ["expectedShippingDate"] =&gt; string(10) "2010-08-21"
      ["paymentDeliveryPrice"] =&gt; string(1) "0"
      ["shippingAssign"] =&gt; NULL
      ["builderId1"] =&gt; NULL
      ["builderId2"] =&gt; NULL
      ["builderId3"] =&gt; NULL
      ["builderId4"] =&gt; NULL
      ["builderId5"] =&gt; NULL
      ["shippingStatus"] =&gt; string(23) "SHIPPING_STATUS_EXPIRED"
      ["realShippingDate"] =&gt; string(10) "2010-08-24"
      ["inquiryNumber"] =&gt; string(2) "36"
      ["shippingStopStatus"] =&gt; string(1) "0"
      ["dirtyFlag"] =&gt; string(1) "1"
      ["deleteFlag"] =&gt; string(1) "0"
      ["deletionDatetime"] =&gt; NULL
      ["registrationDatetime"] =&gt; string(19) "2010-08-20 15:53:35"
      ["updateDatetime"] =&gt; string(19) "2010-08-23 16:15:23"
      ["updateTimestamp"] =&gt; string(19) "2010-08-30 18:32:04"
    }
  }
  [2] =&gt; object(Popo_TrxTestShipping)#73 (2) {
    ["_columnPrefix:protected"] =&gt; string(3) "tts"
    ["_properties:protected"] =&gt; array(43) {
      ["shippingId"] =&gt; string(2) "37"
      ["customerId"] =&gt; string(4) "7001"
      ["divisionId"] =&gt; string(1) "1"
      ["relativeShippingId"] =&gt; NULL
      ["campaignSalesCode"] =&gt; NULL
      ["campaignCode"] =&gt; string(10) "OF_PBR_MLT"
      ["productShippingCode"] =&gt; string(3) "MLY"
      ["packageType"] =&gt; string(21) "PACKAGE_TYPE_DELIVERY"
      ["packageCode"] =&gt; string(5) "MLY-1"
      ["packageNumber"] =&gt; string(1) "1"
      ["chargeType"] =&gt; string(16) "CHARGE_TYPE_CARD"
      ["stepType"] =&gt; string(14) "STEP_TYPE_NONE"
      ["stepCount"] =&gt; string(1) "0"
      ["shippingGroupLabel"] =&gt; NULL
      ["companyName"] =&gt; NULL
      ["lastName"] =&gt; string(10) "関屋7001"
      ["firstName"] =&gt; string(9) "テスト"
      ["lastNameKana"] =&gt; string(9) "ヤマダ"
      ["firstNameKana"] =&gt; string(9) "タロウ"
      ["zip"] =&gt; string(8) "541-0059"
      ["prefecture"] =&gt; string(9) "大阪府"
      ["address1"] =&gt; string(36) "大阪市中央区博労町1－7－7"
      ["address2"] =&gt; string(23) "中央博労町ビル9F"
      ["tel"] =&gt; string(12) "06-6268-0850"
      ["shippingType"] =&gt; string(22) "SHIPPING_TYPE_TOGETHER"
      ["expectedShippingDate"] =&gt; string(10) "2010-08-21"
      ["paymentDeliveryPrice"] =&gt; string(1) "0"
      ["shippingAssign"] =&gt; NULL
      ["builderId1"] =&gt; NULL
      ["builderId2"] =&gt; NULL
      ["builderId3"] =&gt; NULL
      ["builderId4"] =&gt; NULL
      ["builderId5"] =&gt; NULL
      ["shippingStatus"] =&gt; string(24) "SHIPPING_STATUS_COMPLETE"
      ["realShippingDate"] =&gt; string(10) "2010-08-21"
      ["inquiryNumber"] =&gt; string(2) "37"
      ["shippingStopStatus"] =&gt; string(1) "0"
      ["dirtyFlag"] =&gt; string(1) "1"
      ["deleteFlag"] =&gt; string(1) "0"
      ["deletionDatetime"] =&gt; NULL
      ["registrationDatetime"] =&gt; string(19) "2010-08-20 15:53:35"
      ["updateDatetime"] =&gt; string(19) "2010-08-20 16:01:19"
      ["updateTimestamp"] =&gt; string(19) "2010-08-20 16:01:20"
    }
  }
}


</pre>
  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>


