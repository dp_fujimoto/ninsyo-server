<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CheckSessionId.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Content_ConditionList extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 検索結果
        $output = array();

        $class = new Api_Models_Classes_CheckSessionId();
        $class->execute($actionForm->getSessionId());

        if (file_exists(CONDITION_JSON_FILES . '/' . $actionForm->getServerName() . '-' . $actionForm->getProductCode())) {
            return $output;
        }

        //-----------------------------------------------
        // ユーザー確認
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT ";
        $sql .= "  mcd_content_id";
        $sql .= " ,mcd_product_code";
        $sql .= " ,mcd_server_name";
        $sql .= " ,mcd_having_flag";
        $sql .= " ,mcd_servise_in_flag";
        $sql .= " ,mcd_designated_date";
        $sql .= " ,mcd_subscription_flag";
        $sql .= " ,mcd_bundle_flag";
        $sql .= " ,mcd_purchase_flag";
        $sql .= " ,mcd_shipping_data_flag";
        $sql .= " ,mcd_none_condition_flag";
        $sql .= " ,mcd_package_number";
        $sql .= " ,mcd_product_shipping_code";
        $sql .= " ,mcd_cancel_ok_flag";
        $sql .= " ,mcd_permission_month";
        $sql .= " ,mcd_permission_day";
        $sql .= " ,mcd_need_period_month";
        $sql .= " ,mcd_need_period_day";
        $sql .= " ,mcd_campaign_code";
        $sql .= " ,mcd_campaign_sales_code";
        $sql .= " ,mcd_subscription_type";
        $sql .= " ,mcd_product_type";
        $sql .= " ,mcd_charge_type";
        $sql .= " ,mct_publication_date";
        $sql .= " ,mct_available_check_flag";
        $sql .= " ,mct_all_condition_flag";
        $sql .= " ,mct_test_flag";
        $sql .= " FROM mst_condition";
        $sql .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mcd_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND (mcd_none_condition_flag = '0' OR mct_test_flag = '1')";
        $sql .= "   AND (mcd_server_name = ? OR mcd_server_name is null)";
        $sql .= "   AND mct_publication_date <= ?";

        $params   = array();
        $params[] = $actionForm->getServerName();
        $params[] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');

        if (!isBlankOrNull($actionForm->getProductCode())) {
            $sql .= "   AND (";
            $sql .= "       mct_all_condition_flag = '1'";
            $sql .= "       OR (mct_all_condition_flag = '0'";
            $sql .= "           AND mcd_product_code = ?";
            $sql .= "       )";
            $sql .= "   )";
            $params[] = $actionForm->getProductCode();
        }
        $sql .= " ORDER BY mcd_content_id";

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $output[] = $v;
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }
}
