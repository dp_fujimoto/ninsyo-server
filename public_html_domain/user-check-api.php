<?php

    $scriptNameArr = explode('/',$_SERVER["SCRIPT_NAME"]);
    $directoryPath = $_SERVER['DOCUMENT_ROOT'];

    for ($i = 1; $i < count($scriptNameArr); $i++) {
        $directoryPath .= '/' . $scriptNameArr[$i-1];
    }
    $directoryPath = str_replace('//','/',$directoryPath);

    require_once $directoryPath . '/_SYSTEM_/config/define.php';
    require_once $directoryPath . '/_SYSTEM_/library/Mikoshiva/Relay/Agent.php';
    $agent  = null;    $agent  = new Mikoshiva_Relay_Agent();

    // パラメータ追加
    $params = array();
    $params = $_POST;

    // mikoshivaにpost
    $response = $agent->getResponseByPost(MIKOSHIVA_URL . '/api/user/check', $params);
    echo $response;
