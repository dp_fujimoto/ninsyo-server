<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_campaign
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCampaign extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mca';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'campaignId' => null,
        'divisionId' => null,
        'campaignCode' => null,
        'campaignName' => null,
        'multipleProductFlag' => null,
        'salesStartDate' => null,
        'salesEndDate' => null,
        'thanksMailKey' => null,
        'salesFlag' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * キャンペーンID の値を返します
	 *
     * @return int キャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignId() {
		return $this->_properties['campaignId'];
    }


    /**
     * キャンペーンID の値をセットします
	 *
	 * @param int $campaignId キャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignId($campaignId) {
		$this->_properties['campaignId'] = $campaignId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * キャンペーンコード の値を返します
	 *
     * @return varchar キャンペーンコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignCode() {
		return $this->_properties['campaignCode'];
    }


    /**
     * キャンペーンコード の値をセットします
	 *
	 * @param varchar $campaignCode キャンペーンコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignCode($campaignCode) {
		$this->_properties['campaignCode'] = $campaignCode;
    }


    /**
     * キャンペーン名 の値を返します
	 *
     * @return varchar キャンペーン名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignName() {
		return $this->_properties['campaignName'];
    }


    /**
     * キャンペーン名 の値をセットします
	 *
	 * @param varchar $campaignName キャンペーン名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignName($campaignName) {
		$this->_properties['campaignName'] = $campaignName;
    }


    /**
     * 複数メイン商品フラグ の値を返します
	 *
     * @return char 複数メイン商品フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMultipleProductFlag() {
		return $this->_properties['multipleProductFlag'];
    }


    /**
     * 複数メイン商品フラグ の値をセットします
	 *
	 * @param char $multipleProductFlag 複数メイン商品フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMultipleProductFlag($multipleProductFlag) {
		$this->_properties['multipleProductFlag'] = $multipleProductFlag;
    }


    /**
     * 販売開始日 の値を返します
	 *
     * @return datetime 販売開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesStartDate() {
		return $this->_properties['salesStartDate'];
    }


    /**
     * 販売開始日 の値をセットします
	 *
	 * @param datetime $salesStartDate 販売開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesStartDate($salesStartDate) {
		$this->_properties['salesStartDate'] = $salesStartDate;
    }


    /**
     * 販売終了日 の値を返します
	 *
     * @return datetime 販売終了日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesEndDate() {
		return $this->_properties['salesEndDate'];
    }


    /**
     * 販売終了日 の値をセットします
	 *
	 * @param datetime $salesEndDate 販売終了日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesEndDate($salesEndDate) {
		$this->_properties['salesEndDate'] = $salesEndDate;
    }


    /**
     * サンキューメールキー の値を返します
	 *
     * @return varchar サンキューメールキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getThanksMailKey() {
		return $this->_properties['thanksMailKey'];
    }


    /**
     * サンキューメールキー の値をセットします
	 *
	 * @param varchar $thanksMailKey サンキューメールキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setThanksMailKey($thanksMailKey) {
		$this->_properties['thanksMailKey'] = $thanksMailKey;
    }


    /**
     * 販売フラグ の値を返します
	 *
     * @return char 販売フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesFlag() {
		return $this->_properties['salesFlag'];
    }


    /**
     * 販売フラグ の値をセットします
	 *
	 * @param char $salesFlag 販売フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesFlag($salesFlag) {
		$this->_properties['salesFlag'] = $salesFlag;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























