<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ユーザーコンテンツ存在チェック</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <form id="inputForm" enctype="multipart/form-data" action="/user/check-user-content/upload"  method="post">

                <table class="COM_tableBorderZero">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ファイル
                        </td>
                        <td class="COM_nowrap">
                            <input type="file" name="fileData" size="40">
                            <br>※各行にメールアドレスが1件ずつ記載されたファイルを選択してください。
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            コンテンツID{*（カンマ区切りで複数指定可能）*}
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="contentId" value="">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit"  value="　　　アップロード　　　">
                        </td>
                    </tr>
                </table>
                <!-- // 商品マスタ編集フォーム終了 -->

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->