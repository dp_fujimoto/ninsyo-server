<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Date - 指定された月末日を取得する</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="指定された月末日を取得する">指定された月末日を取得する</h1>


            <!-- ========================================
              ■月末日を取得する
            ==========================================-->
            <h2 id="指定された月末日を取得する">指定された月末日を取得する</h2>

            <p>このメソッドは内部で保持しているタイムスタンプに影響を与えません。</p>


            <pre class="wiki">
<?php
highlight_string('<?php

//-----------------------------------------------
//
// ■月末日を取得する
//
//------------------------------------------------
include_once \'Mikoshiva/Date.php\';

//-------------------------------------------------
// ■通常の仕様例
//-------------------------------------------------
// 当月の末日を取得する
$date = new Mikoshiva_Date(\'2010-03-12\');
dumper($date->getEndMonthDate());  // 2010-03-31
dumper($date->getEndMonthDate(0)); // 2010-03-31

// 2 ヵ月後の末日を取得する
$date = new Mikoshiva_Date(\'2010-03-12\');
dumper($date->getEndMonthDate(2)); // 2010-05-31

// 2 ヵ月前の末日を取得する
$date = new Mikoshiva_Date(\'2010-03-12\');
dumper($date->getEndMonthDate(-2)); // 2010-05-31


//-------------------------------------------------
// ■フォーマットを指定する
//   ※時・分・秒取得した場合 00:00:00 でフォーマットされます
//-------------------------------------------------
// 当月の末日を取得する
$date = new Mikoshiva_Date(\'2010-03-12\');
dumper($date->getEndMonthDate(0, \'yyyy-MM-dd HH:mm:ss\')); // 2010-03-31 00:00:00

// 2 ヵ月後の末日を取得する
$date = new Mikoshiva_Date(\'2010/03/12\');
dumper($date->getEndMonthDate(2, \'yyyy-MM-dd HH:mm:ss\')); // 2010-05-31 00:00:00

// 2 ヵ月前の末日を取得する
$date = new Mikoshiva_Date(\'2010/03/12\');
dumper($date->getEndMonthDate(-2, \'yyyy-MM-dd HH:mm:ss\')); // 2010-05-31 00:00:00

');
?>
            </pre>




            <h2 id="Mikoshiva_Date#getEndMonthDate">Mikoshiva_Date#getEndMonthDate</h2>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$month</td>
                <td>int</td>
                <td>0</td>
                <td>月</td>
                <td>
                  初期値に設定された日付から $month に与えられた月数の月末日を返します。<br>
                  ※時・分・秒は 00:00:00 でフォーマットされています。
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>$formt</td>
                <td>string</td>
                <td>yyyy-MM-dd　（例：2010-03-05）</td>
                <td>出力する日付フォーマット</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></br></td>
                <td>string</td>
                <td><br></td>
                <td>日付</td>
                <td><br></td>
              </tr>
            </table>

            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















