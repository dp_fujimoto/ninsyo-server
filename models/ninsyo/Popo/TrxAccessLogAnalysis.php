<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_access_log_analysis
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxAccessLogAnalysis extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tan';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'accessLogAnalysisId' => null,
        'accessLogTotalizationId' => null,
        'accessHour' => null,
        'accessCount' => null,
        'userCount' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tan_access_log_analysis_id の値を返します
	 *
     * @return int tan_access_log_analysis_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogAnalysisId() {
		return $this->_properties['accessLogAnalysisId'];
    }


    /**
     * tan_access_log_analysis_id の値をセットします
	 *
	 * @param int $accessLogAnalysisId tan_access_log_analysis_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogAnalysisId($accessLogAnalysisId) {
		$this->_properties['accessLogAnalysisId'] = $accessLogAnalysisId;
    }


    /**
     * tan_access_log_totalization_id の値を返します
	 *
     * @return int tan_access_log_totalization_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogTotalizationId() {
		return $this->_properties['accessLogTotalizationId'];
    }


    /**
     * tan_access_log_totalization_id の値をセットします
	 *
	 * @param int $accessLogTotalizationId tan_access_log_totalization_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogTotalizationId($accessLogTotalizationId) {
		$this->_properties['accessLogTotalizationId'] = $accessLogTotalizationId;
    }


    /**
     * tan_access_hour の値を返します
	 *
     * @return int tan_access_hour
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessHour() {
		return $this->_properties['accessHour'];
    }


    /**
     * tan_access_hour の値をセットします
	 *
	 * @param int $accessHour tan_access_hour
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessHour($accessHour) {
		$this->_properties['accessHour'] = $accessHour;
    }


    /**
     * tan_access_count の値を返します
	 *
     * @return int tan_access_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessCount() {
		return $this->_properties['accessCount'];
    }


    /**
     * tan_access_count の値をセットします
	 *
	 * @param int $accessCount tan_access_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessCount($accessCount) {
		$this->_properties['accessCount'] = $accessCount;
    }


    /**
     * tan_user_count の値を返します
	 *
     * @return int tan_user_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserCount() {
		return $this->_properties['userCount'];
    }


    /**
     * tan_user_count の値をセットします
	 *
	 * @param int $userCount tan_user_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserCount($userCount) {
		$this->_properties['userCount'] = $userCount;
    }


    /**
     * tan_memo の値を返します
	 *
     * @return text tan_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * tan_memo の値をセットします
	 *
	 * @param text $memo tan_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * tan_delete_flag の値を返します
	 *
     * @return char tan_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tan_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tan_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tan_deletion_datetime の値を返します
	 *
     * @return datetime tan_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tan_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tan_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tan_registration_datetime の値を返します
	 *
     * @return datetime tan_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tan_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tan_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tan_update_datetime の値を返します
	 *
     * @return datetime tan_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tan_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tan_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tan_update_timestamp の値を返します
	 *
     * @return timestamp tan_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tan_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tan_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























