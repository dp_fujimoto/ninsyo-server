<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>フォームテスト</title>
</head>

<body>
    <table>
        <tr>
            <td><a href="./ninsyo-api-form.php" target="_blank">認証APIフォーム</a></td>
        </tr>
        <tr>
            <td><a href="./user-check-api-form.php" target="_blank">ユーザーチェックAPIフォーム</a></td>
        </tr>
        <tr>
            <td><a href="./content-list-api-form.php" target="_blank">コンテンツリストAPIフォーム</a></td>
        </tr>
    </table>
    <hr>
    <table>
        <tr>
            <td><a href="./get-hash-algo-api-form.php" target="_blank">ハッシュアルゴリズム取得APIフォーム</a></td>
        </tr>
        <tr>
            <td><a href="./condition-list-api-form.php" target="_blank">条件リストAPIフォーム</a></td>
        </tr>
        <tr>
            <td><a href="./get-redirect-url-api-form.php" target="_blank">リダイレクト先URL取得（野川さんトレード塾）</a></td>
        </tr>
    </table>
</form>
</body>
</html>