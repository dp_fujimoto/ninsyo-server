<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CheckSessionId.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_InsertUserContentTask extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 検索結果
        $output = '';

        $class = new Api_Models_Classes_CheckSessionId();
        $class->execute($actionForm->getSessionId());

        if (!isBlankOrNull($actionForm->getServerName()) && !isBlankOrNull($actionForm->getFile()) && !isBlankOrNull($actionForm->getFileNum())) {
            $fileName = '' . $actionForm->getServerName() . '_insertUserContentTask_' . Mikoshiva_date::mikoshivaNow('yyyyMMddHHmmss') . '_' . sprintf('%03d',$actionForm->getFileNum());
            file_put_contents(UPLOADS_DIR . '/api/insert-user-content-task/' . $fileName, $actionForm->getFile());
            $output = 'ok';
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }
}
