<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Db/Sequencer/Exception.php';

/**
 * シーケンステーブルのシーケンス処理を管理します。
 * 別コネクションでシーケンステーブルへアクセスするため
 * クラス実行後は必ずcloseConnection()メソッドをよんでください。
 *
 * @package Mikoshiva_Db
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/15
 * @version SVN:$Id: Sequencer.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Sequencer {

    // データベースコネクション
    private $_db = null;

    // シーケンスをインクリメントするSQL
    private $_UPDATE_NEXT_SEQUENCE_ID_SQL =
        "update mst_sequence
           set mse_sequence_number = last_insert_id(mse_sequence_number + 1)
         where mse_sequence_name = ?";

    // シーケンス取得SQL
    private $_SELECT_LAST_INSERT_ID_SQL =
        "select last_insert_id() as next_sequence_id";


    /**
     * コンストラクタ
     * データベースに接続します。
     */
    public function __construct() {
        logInfo(LOG_START);

        // データベースコネクション取得
        $this->openConnection();

        logInfo(LOG_END);
    }

    /**
     * データベースに接続します。
     */
    private function openConnection() {
        logInfo(LOG_START);

        // データベースに未接続の場合のみコネクションを取得します
        if (is_null($this->_db)
            || ((!is_null($this->_db)) && (!$this->_db->isConnected()))) {

            // データベースコネクション取得
            $this->_db =
                Zend_Db::factory('Pdo_Mysql', Array(
                    'host'             => DB_PAYMENT_HOST,
                    'username'         => DB_PAYMENT_USER_NAME,
                    'password'         => DB_PAYMENT_PASSWORD,
                    'dbname'           => DB_PAYMENT_DB_NAME,
                    'adapterNamespace' => 'Mikoshiva_Db_Adapter',
                ));
        }

        logInfo(LOG_END);
    }

    /**
     * シーケンステーブルから第一引数に指定されたシーケンスの番号を
     * インクリメントして取得します。
     *
     * @param string $sequenceName
     * @return int $sequenceId
     * @exception Mikoshiva_Db_Sequencer_Exception
     */
    public function nextSequenceId($sequenceName) {
        logInfo(LOG_START);
        Mikoshiva_Logger::debug('START nextSequenceId(' . $sequenceName . ')');

        // 初期化
        $sequenceId = 0;

        // シーケンス名チェック
        if (isBlankOrNull($sequenceName)) {
            throw new Mikoshiva_Db_Sequencer_Exception(
                '【Mikoshiva_Db_Sequencer#nextSequenceId】シーケンス名が指定されていません。');
        }

        // トランザクション開始(begin)
        $this->_db->beginTransaction();
        logInfo('【Mikoshiva_Db_Sequencer#nextSequenceId】トランザクション開始 [begin]');


        try {
            // シーケンスをインクリメントするSQLを投げます。
            $statement = null;
            $statement = $this->_db->prepare($this->_UPDATE_NEXT_SEQUENCE_ID_SQL);
            $statement->bindValue(1, $sequenceName);
            $bool = false;
            $bool = $statement->execute();

            if (! $bool) {
                // SQL発行失敗
                throw new Mikoshiva_Db_Sequencer_Exception(
                    '【Mikoshiva_Db_Sequencer#nextSequenceId】シーケンスのインクリメントに失敗しました。');
            }

            // インクリメントしたシーケンス値を取得します。
            $row = array();
            $row = $this->_db->fetchRow($this->_SELECT_LAST_INSERT_ID_SQL, array(1, $sequenceName));

            if (count($row) !== 0) {
                // シーケンスIDを取得
                $sequenceId = $row['next_sequence_id'];
            } else {
                // SQL発行失敗
                throw new Mikoshiva_Db_Sequencer_Exception(
                    '【Mikoshiva_Db_Sequencer#nextSequenceId】シーケンスの取得に失敗しました。');
            }

            // トランザクション終了(commit)
            $this->_db->commit();
            logInfo('【Mikoshiva_Db_Sequencer#nextSequenceId】トランザクション終了 [commit]');

        } catch (Exception $e) {
            // トランザクション終了(rollback)
            $this->_db->rollBack();
            logInfo('【Mikoshiva_Db_Sequencer#nextSequenceId】トランザクション終了 [rollback]');

            throw $e;
        }


        // シーケンスIDが[0]の場合、強制的に例外とします。
        if ($sequenceId === 0 || $sequenceId === '0') {
            throw new Mikoshiva_Db_Sequencer_Exception(
                '【Mikoshiva_Db_Sequencer#nextSequenceId】シーケンスIDが[0]です。');
        }


        Mikoshiva_Logger::debug('End nextSequenceId => ' . $sequenceId);
        logInfo(LOG_END);

        return (int)$sequenceId;
    }

    /**
     * データベースに接続中の場合コネクションをクローズします。
     */
    public function closeConnection() {
        logInfo(LOG_START);
        $bool = false;

        // コネクション終了
        if ((!is_null($this->_db)) && $this->_db->isConnected()) {
            $this->_db->closeConnection();
            $this->_db = null;

            $bool = true;
        }

        logInfo(LOG_END);
        return $bool;
    }

}