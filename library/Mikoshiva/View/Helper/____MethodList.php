<?php
/** @package Mikoshiva_View */


/**
 * 
 * 
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class ____Mikoshiva_View_Helper_MethodList {

    /**
     * 権限チェックを行います
     *
     * @param string $authName 権限名
     * @param string $flag フラグ
     * @return boolean true|false
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/25
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function authCheck($authName) {
    }

    /**
     * 全てのタブを閉じるjavascriptを返します
     * @return javascript
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @return string
     */
    public function closeTabAll() {
    }


    /**
     * 全てのタブを閉じるjavascriptをボタンタグに実装した形で返します
     *
     * @param $name ボタン名
     * @return ボタンタグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabAllButton($name) {
    }


    /**
     * 全てのタブを閉じるjavascriptをリンクタグに実装した形で返します
     *
     * @param $name リンク名
     * @return リンクタグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabAllLink($name) {
    }



    /**
     * 現在開いているタブを閉じるjavascript を生成します
     *
     * @param $tabId タブ
     * @return javascript
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabCurrent($tabId) {
    }

    /**
     * 現在開いているタブを閉じるボタンを生成します
     *
     * @param $name ボタン名
     * @param $tabId タブ ID
     * @return ボタンタグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabCurrentButton($name, $tabId) {
    }


    /**
     * 現在開いているタブを閉じるリンクを生成します
     *
     * @param $name リンク名
     * @param $tabId タブ ID
     * @return リンクタグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabCurrentLink($name, $tabId) {
    }


    /**
     * 保護されたタブ以外の全てのタブを閉じるjavascript を生成します
     *
     * @return javascript
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabUnprotected() {
    }


    /**
     * 保護されたタブ以外の全てのタブを閉じるボタンを生成します
     *
     * @param $name ボタン名
     * @return ボタンタグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabUnprotectedButton($name) {
    }


    /**
     * 保護されたタブ以外の全てのタブを閉じるリンクを生成します
     *
     * @param $name リンク名
     * @return リンクタグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function closeTabUnprotectedLink($name) {
    }

    /**
     * カラムリストからマッピングされた値を取得します
     *
     * @param string $column カラム名
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function columnName($column) {
    }

    /**
     * 画面遷移を現在開いているタブに対して行うjavascriptを返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param string $url 遷移先 URL
     * @param string $tabId タブ ID
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function currentTab($url, $tabId = null, $formId = null) {
    }


    /**
     * 画面遷移を現在開いているタブに対して行うjavascriptをボタンタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param unknown_type $name ボタン名
     * @param string $url 遷移先 URL
     * @param string $tabId タブ ID
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function currentTabButton($name, $url, $tabId = null, $formId = null) {
    }

    /**
     * 画面遷移を現在開いているタブに対して行うjavascriptをリンクタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param string $name リンク名
     * @param string $url
     * @param string $tabId
     * @return string
     */
    public function currentTabLink( $name, $url, $tabId = null) {
    }



    /**
     * 日を select ボックスでリスト表示します
     *
     * @param string $name name 属性
     * @param string $selected selected
     * @param array $attribs 追加属性
     * @return select ボックス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function dayLister($name, $selected, $attribs = null) {

    }



    /**
     * 部署データをプルダウンメニュー、もしくはラジオボタンのリストで返します
     *
     * @param string $name name 属性
     * @param string $selected seelcted
     * @param string $mode select|radio
     * @param array $attribs 追加属性
     * @param boolean $divisionZero true:「全て」の項目の表示
     * @param boolean $addEmpty true:セレクトボックスの際に先頭項目に空の項目を追加する
     * @return string select or radio タグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function divisionLister($name, $selected = null, $mode = 'select', $attribs = null, $divisionZero = true, $addEmpty = false) {

    }


    /**
     * formRadio のラップクラス
     *
     * @param string  $name       name 属性
     * @param string  $value      selected
     * @param string  $attribs    追加項目
     * @param array   $options    options
     * @param boolean $addAllItem true:【全て】の項目を追加
     * @param string  $listsep    区切文字
     * @return unknown_type
     * (non-PHPdoc)
     * @see Zend/View/Helper/Zend_View_Helper_FormRadio#formRadio($name, $value, $attribs, $options, $listsep)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function formRadio($name,
                              $value      = null,
                              $attribs    = null,
                              $options    = null,
                              $addAllItem = false,
                              $listsep    = "<br>\n") {

                              }

    /**
     * formSelect のラップクラス
     *
     * @param string  $name       name 属性
     * @param string  $value      selected
     * @param string  $attribs    追加項目
     * @param array   $options    options
     * @param boolean $addEmpty   true:先頭行に空項目を作成
     * @param boolean $addAllItem true:【全て】の項目を追加
     * @param string  $listsep    区切文字
     * @return unknown_type
     * (non-PHPdoc)
     * @see Zend/View/Helper/Zend_View_Helper_FormSelect#formSelect($name, $value, $attribs, $options, $listsep)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function formSelect($name,
                               $value      = null,
                               $attribs    = null,
                               $options    = null,
                               $addEmpty   = false,
                               $addAllItem = false,
                               $listsep    = "<br>\n") {

                               }

    /**
     * データベベースから値を取得し、セレクトボックスを生成します
     *
     * @param  string       $name        name 属性
     * @param  string       $selected    selected
     * @param  string       $arClassName アクティブレコード名
     * @param  array        $where       検索条件
     * @param  string       $valueName   value に入力させたいカラム名
     * @param  string|array $labelName   ラベルに表示されたいカラム名
     * @param  string       $separater   ラベルが複数指定された時の区切り文字
     * @param  array        $attribs     追加属性
     * @param  boolean      $addEmpty    true: 先頭項目に空項目を追加
     * @param  boolean      $addAllItem  true: 先頭項目に【全て】の項目を追加
     * @param  string       $listsep     区切文字
     * @return string                    &lt;select&gt;DBから取得した値によるリスト&lt;/select&gt;
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/22
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function formSelectDb($name, $selected = null, $arClassName, array $where = array(), $valueName, $labelName, $separater = '', $attribs = null, $addEmpty = false, $addAllItem = false, $listsep = "<br>\n") {
                                 }


	/**
     * セッションの値を返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $namespace 名前空間の名前
     * @param string $name 取り出したいキー名
     * @return mixed
     */
    public function getSession( $namespace = null, $name = null ) {
    }

    /**
     * ショップIDから、ショップ名を取得して返します。
     *
     * @param  string $shopId ショップID
     * @return string $result ショップ名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/15
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function gmoMapping($shopId) {

    }

    /**
     * 与えられた配列から hidden タグを書き出します
     *
     * @param string $name name 属性
     * @param string $selected selected
     * @return hidden タグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/18
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function hiddenLister($formName, $hiddenList = array(), $attribs = null) {

    }



    /**
     * 分を select ボックスでリスト表示します
     *
     * @param $name name 属性
     * @param $selected selected
     * @param $attribs 追加属性
     * @return セレクトボックス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function hourLister($name, $selected = null, $attribs = null) {

    }


    /**
     * 改行コードを改行タグに変換します
     *
     * @param unknown_type $str
     * @return string 改行コードを改行タグに変換した文字列
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/25
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function lineToBr($str) {

    }


    /**
     * 分を select ボックスでリスト表示します
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function minLister($name, $selected = null, $attribs = null) {

    }


    /**
     * 時間を selectボックスでリスト表示します
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function monthLister($name, $selected = null, $attribs = null) {

    }

	/**
     * 新しいタブを開く javascript を返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param string $url 遷移先 URL
     * @param string $storage タブ情報を記録するか
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function newTab($url, $storage = false, $formId = null ) {

    }

    /**
     * 新しいタブを開くjavascriptをボタンタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $name ボタンの vlue 値
     * @param string $url 遷移先 URL
     * @param boolean $storage true|保存flase|保存しない
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function newTabButton($name, $url, $storage = false, $formId = null) {

    }


	/**
     * 新しいタブを開くjavascriptをリンクタグに実装した形で実装します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $name ボタンの vlue 値
     * @param string $url 遷移先 URL
     * @param boolean $storage true|保存flase|保存しない
     * @return string
     */
    public function newTabLink( $name, $url, $storage = false) {

    }


    /**
     * 分を select ボックスでリスト表示します
     *
     * @return string
     */
    public function prefectureLister($name, $selected = null, $attribs = null) {

    }


    /**
     * Ajax で指定されたアクションへリクエストを行い、
     * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うjavascriptを返します
     *
     * @param string $url アクション名
     * @param string $targetId アクション URL
     * @param string $formId フォーム ID
     * @param string $append 追記モード FRONT:前方へ追記|BACK:後方へ追記

     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function requestToId($url, $targetId, $formId = '', $append = '') {

    }


    /**
     * Ajax で指定されたアクションへリクエストを行い、
     * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うボタンタグを返します
     *
     * @param string name ボタン名
     * @param string $url アクション名
     * @param string $targetId アクション URL
     * @param string $formId フォーム ID
     * @param string $append 追記モード FRONT:前方へ追記|BACK:後方へ追記

     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function requestToIdButton($name, $url, $targetId, $formId = '', $append = '') {

    }

    /**
     * Ajax で指定されたアクションへリクエストを行い、
     * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うリンクタグを返します
     *
     * @param string name リンク名名
     * @param string $url アクション名
     * @param string $targetId アクション URL
     * @param string $formId フォーム ID
     * @param string $append 追記モード FRONT:前方へ追記|BACK:後方へ追記

     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function requestToIdLink($name, $url, $targetId, $formId = '', $append = '') {
    }

    /**
     * 検索結果が取得出来なかったときのメッセージを返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/15
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function searchNotFoundMessage () {

    }


    /**
     * 分を select ボックスでリスト表示します
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function secondLister($name, $selected = null, $attribs = null) {

    }


    /**
     * 年を select ボックスでリスト表示します
     *
     * @param string $name name 属性
     * @param string $selected selected
     * @param string $start 開始年
     * @param string $end 終了年
     * @param int $figure 表示桁数
     * @return string
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/22
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function yearLister($name, $selected = null, $start = 2000, $end = 2030, $figure = 4, $attribs = null) {

    }

    /**
     * カラム名とステータス(短縮コード)から、備考/説明を取得して返します。
     *
     * @param  string $columnName カラム名
     * @param  string $status     ステータス
     * @return string $result     備考/説明
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function statusMapping($columnName, $status) {

    }





    /**
     * 短縮コードのリスト(select/radioフォーム)を作成します。
     *
     * @param string  $name name  属性
     * @param string  $column     カラム名
     * @param string  $selected   selected
     * @param string  $mode       select/radio
     * @param array   $deleteList 削除リスト
     * @param array   $attribs    追加属性
     * @param boolean $addEmpty   true:一番目の配列に空項目を追加する
     * @param boolean $addAllItem true:【全て】の項目の追加
     * @param string  $listsep    区切文字
     * @return select/radio
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/15
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function statusLister($name,
                                 $column,
                                 $selected   = null,
                                 $mode       = 'select',
                                 $deleteList = null,
                                 $attribs    = null,
                                 $addEmpty   = false,
                                 $addAllItem = false,
                                 $listsep    = "<br>\n") {

                                 }


	/**
     * ショップ ID のリストをselect or radio で表示します
     *
     * @param  string  $name name       属性
     * @param  string  $selected        selected
     * @param  string  $mode            select|radio
     * @param  array   $attribs         追加属性
     * @param  boolean $addShopNameFlag ture|ショップ名を追記
     * @param  boolean $addEmpty        true:先頭行に空項目を作成
     * @param  boolean $addAllItem      true:【全て】の項目を追加
     * @param  string  $listsep         区切文字
     * @return string select or radio
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function gmoShopIdLister($name,
                                    $selected        = null,
                                    $mode            = 'select',
                                    $attribs         = array(),
                                    $addShopNameFlag = true,
                                    $addEmpty        = false,
                                    $addAllItem      = false,
                                    $listsep         = "<br>\n") {

                                    }

    /**
     * 与えられた配列から hidden タグを書き出します
     *
     * @param string $name       フォーム名
     * @param array  $hiddenList 値のリスト
     * @param array  $deleteList 削除する値
     * @param array  $attribs    追加項目
     * @return hidden
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/18
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function hiddenLister($formName, array $hiddenList = array(), array $deleteList = array(), array $attribs = array()) {

    }

    /**
     * スタッフID及び、カラム名を元に、スタッフ情報を返します
     *
     * @param  string        $staffId    スタッフID
     * @param  string|array  $column     カラム名、またはカラム名の配列(array(column1, column2, ...)
     * @param  string        $separater  戻り値のセパレータ(column1_data[separater]column2_data[separater]...columnN_data)
     * @return string        $result
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/21
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function staffInfo($staffId, $column = 'mst_last_name', $separater = '') {

    }

    /**
     * データベースからプライマリーキーによって値を取得し、
     * 指定されたカラムの値を返します。
     * 複数のカラムを指定する場合は配列を用います。
     * 　　この時、セパレータを指定することが可能です。
     *
     * @param  string       $arClassName アクティブレコード名
     * @param  string       $pkey        プライマリーキーの値（複合プライマリーキー未対応）
     * @param  string|array $column      取得するカラム名
     * @param  string       $separater   複数のカラムを指定した際のセパレータ
     * @return string
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2009/01/22
     * @version SVN:$Id: ____MethodList.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function dbWrite($arClassName, $pkey, $column, $separater = '') {
    }

}































