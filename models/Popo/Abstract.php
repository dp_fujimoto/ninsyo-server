<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Popo/Abstract.php';


/**
 * Popo 関連クラス
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:09 fujimoto Exp $
 */
abstract class Popo_Abstract {


    /**
     * プロパティリスト
     *
     * @var array プロパティリスト
     */
    protected $_properties = array();


    /**
     * コンストラクタ
     *
     * @param $data
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:09 fujimoto Exp $
     */
    public function __constract(array $dataList = array()) {

        // プロパティリストを初期化
        $this->copyProperty($dataList);
    }


    /**
     * プロパティの一覧を返します
	 *
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:09 fujimoto Exp $
     *
     */
    public function getProperties() {
		return $this->_properties;
    }


    /**
     * 与えられた配列を基にプロパティリストを上書きします
     *
     * @param array $dataList データリスト
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:09 fujimoto Exp $
     */
    public function copyProperty(array $dataList) {

        // ログスタート
        //Mikoshiva_Logger::info(LOG_START);

        foreach ($dataList as $k => $v) {

            // キーが存在していれば値をセットします
            if (array_key_exists($k, $this->_properties) ) {
                 $this->_properties[$k] = $v;
            }
        }

        // ログエンド
        //Mikoshiva_Logger::info(LOG_START);
    }
}






















