<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_guarantee_pattern
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstGuaranteePattern extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mgp';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'guaranteePatternId' => null,
        'paymentNumberType' => null,
        'freePeriodFlag' => null,
        'guaranteePeriodFlag' => null,
        'refundWithReturnsFlag' => null,
        'chargeType' => null,
        'guaranteeTemplateId' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mgp_guarantee_pattern_id の値を返します
	 *
     * @return int mgp_guarantee_pattern_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePatternId() {
		return $this->_properties['guaranteePatternId'];
    }


    /**
     * mgp_guarantee_pattern_id の値をセットします
	 *
	 * @param int $guaranteePatternId mgp_guarantee_pattern_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePatternId($guaranteePatternId) {
		$this->_properties['guaranteePatternId'] = $guaranteePatternId;
    }


    /**
     * mgp_payment_number_type の値を返します
	 *
     * @return varchar mgp_payment_number_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentNumberType() {
		return $this->_properties['paymentNumberType'];
    }


    /**
     * mgp_payment_number_type の値をセットします
	 *
	 * @param varchar $paymentNumberType mgp_payment_number_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentNumberType($paymentNumberType) {
		$this->_properties['paymentNumberType'] = $paymentNumberType;
    }


    /**
     * mgp_free_period_flag の値を返します
	 *
     * @return char mgp_free_period_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodFlag() {
		return $this->_properties['freePeriodFlag'];
    }


    /**
     * mgp_free_period_flag の値をセットします
	 *
	 * @param char $freePeriodFlag mgp_free_period_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodFlag($freePeriodFlag) {
		$this->_properties['freePeriodFlag'] = $freePeriodFlag;
    }


    /**
     * mgp_guarantee_period_flag の値を返します
	 *
     * @return char mgp_guarantee_period_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriodFlag() {
		return $this->_properties['guaranteePeriodFlag'];
    }


    /**
     * mgp_guarantee_period_flag の値をセットします
	 *
	 * @param char $guaranteePeriodFlag mgp_guarantee_period_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriodFlag($guaranteePeriodFlag) {
		$this->_properties['guaranteePeriodFlag'] = $guaranteePeriodFlag;
    }


    /**
     * mgp_refund_with_returns_flag の値を返します
	 *
     * @return char mgp_refund_with_returns_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundWithReturnsFlag() {
		return $this->_properties['refundWithReturnsFlag'];
    }


    /**
     * mgp_refund_with_returns_flag の値をセットします
	 *
	 * @param char $refundWithReturnsFlag mgp_refund_with_returns_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundWithReturnsFlag($refundWithReturnsFlag) {
		$this->_properties['refundWithReturnsFlag'] = $refundWithReturnsFlag;
    }


    /**
     * mgp_charge_type の値を返します
	 *
     * @return varchar mgp_charge_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * mgp_charge_type の値をセットします
	 *
	 * @param varchar $chargeType mgp_charge_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * mgp_guarantee_template_id の値を返します
	 *
     * @return int mgp_guarantee_template_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteeTemplateId() {
		return $this->_properties['guaranteeTemplateId'];
    }


    /**
     * mgp_guarantee_template_id の値をセットします
	 *
	 * @param int $guaranteeTemplateId mgp_guarantee_template_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteeTemplateId($guaranteeTemplateId) {
		$this->_properties['guaranteeTemplateId'] = $guaranteeTemplateId;
    }


    /**
     * mgp_delete_flag の値を返します
	 *
     * @return char mgp_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mgp_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mgp_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mgp_deletion_datetime の値を返します
	 *
     * @return datetime mgp_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mgp_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mgp_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mgp_registration_datetime の値を返します
	 *
     * @return datetime mgp_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mgp_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mgp_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mgp_update_datetime の値を返します
	 *
     * @return datetime mgp_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mgp_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mgp_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mgp_update_timestamp の値を返します
	 *
     * @return timestamp mgp_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mgp_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mgp_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























