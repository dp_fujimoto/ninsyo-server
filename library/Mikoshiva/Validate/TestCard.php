<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * require
 */
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';
require_once 'Mikoshiva/Validate/Exception.php';

/**
 * カード番号がテストカード番号か否かを判定します
 *
 * ・テストカードでない時 true
 * ・テストカードで、許可されたアドレスからのアクセスの時 true
 * ・テストカードで、許可されたアドレス以外からのアクセスの場合 false
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: TestCard.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_TestCard extends Zend_Validate_Abstract
{
    const INVALID_NODE  = 'invalidNode';

    /**
     * リクエストデータ を保持します
     * @var array リクエストデータ を保持します
     */
    protected $data;

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID_NODE => "%COLUMN_NAME% の形式が正しくありません。",
    );

    /**
     * リクエストデータ をセットします
     *
     * @param array $data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: TestCard.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {

        logInfo(LOG_START);
        $this->data = $data;
        logInfo(LOG_END);
    }

    /**
     * リクエストデータ を返します
     *
     * @return array $this->data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: TestCard.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getData() {
        logInfo(LOG_START);
        return $this->data;
        logInfo(LOG_END);
    }

    /**
     * Digits filter used for validation
     *
     * @var Zend_Filter_Digits
     */
    protected static $_filter = null;
    /**
     *
     *
     * @param   Mikoshiva_Property_Zip $zip 郵便番号を保持するオブジェクト
     * @return boolean
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: TestCard.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Interface#isValid($value)
     */
    public function isValid($value) {
        $GMO_TEST_CARD_LIST = array();
        $GMO_TEST_CARD_LIST = Zend_Registry::get('GMO_TEST_CARD_LIST');
        // 入力されたカード番号が、テストカード番号でない場合はtrueを返す
        if(!array_key_exists($value, $GMO_TEST_CARD_LIST)) {
            return true;
        }

        //---------------------------------------------------------------------
        // ■テストカード番号の場合
        //---------------------------------------------------------------------
        $ipAddress = '';
        if(isset($_SERVER['REMOTE_ADDR'])) {
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        } else {
            // リモートアドレスがない場合、バッチ起動とみなしてチェックを終了
            return true;
        }

        $GMO_TEST_CARD_ALLOW_LIST = array();
        $GMO_TEST_CARD_ALLOW_LIST = Zend_Registry::get('GMO_TEST_CARD_ALLOW_LIST');

        if(!$this->_isAllowed($GMO_TEST_CARD_ALLOW_LIST, $ipAddress)) {
            $this->_error(self::INVALID_NODE);
            return false;
        }

        return true;
    }

    /**
     * 指定された｢許可されたネットワーク｣に、指定されたIPアドレスが含まれるかを判定します。
     *
     * @param array  $trustedList CIDR表記(xxx.xxx.xxx.xxx/yy)のネットワークアドレスの配列
     * @param string $ipAddress   判定対象ノードのIPアドレス
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/06/08
     * @version SVN:$Id: TestCard.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
//    private function _isAllowed($allowedList, $ipAddress) {
//        foreach($allowedList as $k => $v) {
//            $networkAddress                 = '';
//            $netmask                        = '';
//            $shift                          = '';
//            list($networkAddress, $netmask) = explode('/', $v);
//            $shift                          = 32 - $netmask;
//
//            // ネットマスクを元に、ネットワークアドレスを算出
//            $n = '';
//            $i = '';
//            $n = ip2long($networkAddress) >> $shift;
//            $i = ip2long($ipAddress) >> $shift;
//
//            // ネットワークアドレスが一致した場合、trueを返す
//            if($n === $i) {
//                return true;
//            }
//        }
//        return false;
//    }

    /**
     * IP チェックを行います
     *
     * @param array  $trustedList CIDR表記(xxx.xxx.xxx.xxx/yy)のネットワークアドレスの配列
     * @param string $ipAddress   判定対象ノードのIPアドレス
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/06/08
     * @version SVN:$Id: TestCard.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _isAllowed($allowedList, $ipAddress) {


        foreach($allowedList as $index => $chackIp) {

            if (preg_match('#^192\.168#', $ipAddress) === 1) {
                return true;
            }

            if ($ipAddress === $chackIp) {
                return true;
            }
        }
        return false;
    }
}

























