
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; Task 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#convertBitsToIntValue">Mikoshiva_Utility_Integer#convertBitsToIntValue - ID(bit)からintValueを作成して返します。</a></li>
             <li><a href="#convertIntToBitsValue">Mikoshiva_Utility_Integer#convertIntToBitsValue - intValueからをID(bit)を格納した配列を作成します。</a></li>
            </ol>



            <!-- ==============================================
            ■Mikoshiva_Utility_Integer#convertBitsToIntValue
            ============================================== -->
            <h2 id="Mikoshiva_Utility_Integer#convertBitsToIntValue"><a name="convertBitsToIntValue">Mikoshiva_Utility_Integer#convertBitsToIntValue</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              ID(bit)からintValueを作成して返します。<br>
            </p>
            <p>
              与えられた値(配列の値)をビット扱いとし<br>
              最大31までの複数項目をint型の値によって表します。<br>
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$bits</td>
                <td>int|array</td>
                <td><br></td>
                <td>ID(bit)</td>
                <td>
                  int変換したいID配列(0～31までの数値)
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>int</td>
                <td><br></td>
                <td>intValue|null</td>
                <td>int値</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Utility/Integer.php\';

// 初期化
$values = array();
$values[] = 1;
$values[] = 3;
$values[] = 5;

// ID(bit)からintValueを作成します。
$result = 0;
$result = Mikoshiva_Utility_Integer::convertBitsToIntValue($values);

dumper(result);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
int(21)
</pre>




            <!-- ==============================================
            ■Mikoshiva_Utility_Integer#convertIntToBitsValue
            ============================================== -->
            <h2 id="Mikoshiva_Utility_Integer#convertIntToBitsValue"><a name="convertIntToBitsValue">Mikoshiva_Utility_Integer#convertIntToBitsValue</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              intValueからをID(bit)を格納した配列を作成します。<br>
            </p>
            <p>
              引数にint値より大きい値が指定された場合、エラーを返します。<br>
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$int</td>
                <td>int</td>
                <td><br></td>
                <td>Mikoshiva_Utility_Integer#convertBitsToIntValueで作成した値</td>
                <td>
                  0-2147483647の数値
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>array()</td>
                <td><br></td>
                <td></td>
                <td>int値の配列で返ります。</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Utility/Integer.php\';

// 初期化
$int = 0;
$int = 21;

// intValueからをID(bit)を格納した配列を作成します。
$result = array();
$result = Mikoshiva_Utility_Integer::convertIntToBitsValue($int);

dumper($result);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
array(3) {
  [0] => int(1)
  [1] => int(3)
  [2] => int(5)
}
</pre>






            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>



  </div>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





























