<?php

require_once 'Mikoshiva/Logger.php';

/**
 * 配列要素を指定された文字列により連結して返します
 * 要素が配列でない場合はそのまま返します
 *
 * @param  array   $params
 *                      array  pieces              対象となる配列
 *                      string glue   &lt;br&gt; | 連結に使用する文字列
 * @param  Smarty  &$smarty
 * @return string 連結された文字列
 */
function smarty_function_imploader( $params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                = array();
    $default['pieces']      = '';
    $default['glue']        = '<br>';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------

    if ( ! is_array( $data['pieces'] ) ) {
        Mikoshiva_Logger::info(LOG_END);
        return $data['pieces'];
    }
    Mikoshiva_Logger::info(LOG_END);
    return implode( $data['glue'], $data['pieces'] );
}
?>