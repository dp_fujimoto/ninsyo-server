<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; カレンダー 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#getBeforeHolidayDay">Mikoshiva_Calendar_Open::getBeforeHolidayDay - （基底日日付＋指定日数）を含めた最短の”前”営業日を取得します</a></li>
              <li><a href="#getAfterHolidayDay">Mikoshiva_Calendar_Open::getAfterHolidayDay - （基底日日付＋指定日数）を含めた最短の”翌”営業日を取得します</a></li>
              <li><a href="#getBeforeHolidayMonth">Mikoshiva_Calendar_Open::getBeforeHolidayMonth - （基底日日付＋指定月数）を含めた最短の”前”営業日を取得します</a></li>
              <li><a href="#getAfterHolidayMonth">Mikoshiva_Calendar_Open::getAfterHolidayMonth - （基底日日付＋指定月数）を含めた最短の”月”営業日を取得します</a></li>
              <li><a href="#getBeforeManyBusinessDayDay">Mikoshiva_Calendar_Open::getBeforeManyBusinessDayDay - 基底日日付から指定営業日を”経過”した営業日(n営業日後)を取得します</a></li>
              <li><a href="#getAfterManyBusinessDayDay">Mikoshiva_Calendar_Open::getAfterManyBusinessDayDay - 基底日日付から指定営業日を”遡った”営業日(n営業日前)を取得します</a></li>
            </ol>



            <!-- ==============================================
            ■Mikoshiva_Calendar_Open::getBeforeHolidayDay - （基底日日付＋指定日数）から最短の”前”営業日を取得します
            ============================================== -->
            <h2><a name="getBeforeHolidayDay">Mikoshiva_Calendar_Open::getBeforeHolidayDay</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              （基底日日付＋指定日数）から最短の”前”営業日を取得します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$baseDate</td>
                <td>string</td>
                <td><br></td>
                <td>基底日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$addDay</td>
                <td>string</td>
                <td><br></td>
                <td>日数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd</td>
                <td>日付フォーマット</td>
                <td>
                  Zend_Date の <a href="http://framework.zend.com/manual/ja/zend.date.constants.html#zend.date.constants.selfdefinedformats">ISO 書式指定子を使用して自分で定義する出力フォーマット</a>を参照して下さい。
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$mode</td>
                <td>string</td>
                <td>Mikoshiva_Calendar_Open::ALL</td>
                <td>祝日・休日対象モード</td>
                <td><pre>
Mikoshiva_Calendar_Open::ALL             祝日・休日すべて
Mikoshiva_Calendar_Open::FUNAI           船井休日
Mikoshiva_Calendar_Open::DIRECT          ダイレクト出版休日
Mikoshiva_Calendar_Open::HOLIDAY         国民の祝日
Mikoshiva_Calendar_Open::HOLIDAY_FUNAI   国民の祝日・船井休日
Mikoshiva_Calendar_Open::HOLIDAY_DIRECT  国民の祝日・ダイレクト出版休日
Mikoshiva_Calendar_Open::FUNAI_DIRECT    船井・ダイレクト出版休日
                </pre></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■カレンダー・サンプル
//   （前営業日を返します）
//------------------------------------------------
require_once \'Mikoshiva/Calendar/Open.php\';

// 第二引数で 1 日後（2010/02/01）を指定しており、2010/02/01 は祝日・休日ではないため 2010/02/01 が返る
dumper(Mikoshiva_Calendar_Open::getBeforeHolidayDay(\'2010/01/31\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));

// 第二引数で 1 日後（2010/03/21）を指定しており、2010/03/21 は祝日（春分の日）であり、かつ、2010/03/20 が土曜日であるため 2010/03/19 が返る
dumper(Mikoshiva_Calendar_Open::getBeforeHolidayDay(\'2010/03/20\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Calendar_Open::getAfterHolidayDay - （基底日日付＋指定日数）から最短の”翌”営業日を取得します
            ============================================== -->
            <h2><a name="getAfterHolidayDay">Mikoshiva_Calendar_Open::getAfterHolidayDay</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              （基底日日付＋指定日数）から最短の”翌”営業日を取得します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$baseDate</td>
                <td>string</td>
                <td><br></td>
                <td>基底日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$addDay</td>
                <td>string</td>
                <td><br></td>
                <td>日数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd</td>
                <td>日付フォーマット</td>
                <td>
                  Zend_Date の <a href="http://framework.zend.com/manual/ja/zend.date.constants.html#zend.date.constants.selfdefinedformats">ISO 書式指定子を使用して自分で定義する出力フォーマット</a>を参照して下さい。
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$mode</td>
                <td>string</td>
                <td>Mikoshiva_Calendar_Open::ALL</td>
                <td>祝日・休日対象モード</td>
                <td><pre>
Mikoshiva_Calendar_Open::ALL             祝日・休日すべて
Mikoshiva_Calendar_Open::FUNAI           船井休日
Mikoshiva_Calendar_Open::DIRECT          ダイレクト出版休日
Mikoshiva_Calendar_Open::HOLIDAY         国民の祝日
Mikoshiva_Calendar_Open::HOLIDAY_FUNAI   国民の祝日・船井休日
Mikoshiva_Calendar_Open::HOLIDAY_DIRECT  国民の祝日・ダイレクト出版休日
Mikoshiva_Calendar_Open::FUNAI_DIRECT    船井・ダイレクト出版休日
                </pre></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■カレンダー・サンプル
//   （翌営業日を返します）
//------------------------------------------------
require_once \'Mikoshiva/Calendar/Open.php\';

// 第二引数で 1 日後（2010/02/01）を指定しており、2010/02/01 は祝日・休日ではないため 2010/02/01 が返る
dumper(Mikoshiva_Calendar_Open::getAfterHolidayDay(\'2010/01/31\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));

// 第二引数で 1 日後（2010/03/21）を指定しており、2010/03/21 は祝日（春分の日）であるため、2010/03/22 が返る
dumper(Mikoshiva_Calendar_Open::getAfterHolidayDay(\'2010/03/20\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Calendar_Open::getBeforeHolidayMonth - （基底日日付＋指定日数）から最短の”前”営業日を取得します
            ============================================== -->
            <h2><a name="getBeforeHolidayMonth">Mikoshiva_Calendar_Open::getBeforeHolidayMonth</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              （基底日日付＋指定日数）から最短の”前”営業日を取得します<br>
              <span style="color: #BB0000">※このメソッドは 2010/01/31 の１ヶ月後は 2010/02/28 と考えます（SQL 風）</span>
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$baseDate</td>
                <td>string</td>
                <td><br></td>
                <td>基底日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$addMonth</td>
                <td>string</td>
                <td><br></td>
                <td>月数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd</td>
                <td>日付フォーマット</td>
                <td>
                  Zend_Date の <a href="http://framework.zend.com/manual/ja/zend.date.constants.html#zend.date.constants.selfdefinedformats">ISO 書式指定子を使用して自分で定義する出力フォーマット</a>を参照して下さい。
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$mode</td>
                <td>string</td>
                <td>Mikoshiva_Calendar_Open::ALL</td>
                <td>祝日・休日対象モード<br>（対象とする休日・祝日）</td>
                <td><pre>
Mikoshiva_Calendar_Open::ALL             祝日・休日すべて
Mikoshiva_Calendar_Open::FUNAI           船井休日
Mikoshiva_Calendar_Open::DIRECT          ダイレクト出版休日
Mikoshiva_Calendar_Open::HOLIDAY         国民の祝日
Mikoshiva_Calendar_Open::HOLIDAY_FUNAI   国民の祝日・船井休日
Mikoshiva_Calendar_Open::HOLIDAY_DIRECT  国民の祝日・ダイレクト出版休日
Mikoshiva_Calendar_Open::FUNAI_DIRECT    船井・ダイレクト出版休日
                </pre></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■カレンダー・サンプル
//
//------------------------------------------------
require_once \'Mikoshiva/Calendar/Open.php\';

// 第二引数で 1 月後を指定（2007/02/28）しており、2007/02/28 は祝日ではないため 2007/02/28 が返る
dumper(Mikoshiva_Calendar_Open::getBeforeHolidayMonth(\'2007/01/31\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));

// 第二引数で 1 月後を指定（2010/02/28）しており、2010/02/28 は日曜日であり、前日の 2010/02/27 も土曜日であるため、2010/02/26 が返る
dumper(Mikoshiva_Calendar_Open::getBeforeHolidayMonth(\'2010/01/31\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Calendar_Open::getAfterHolidayMonth - （基底日日付＋指定日数）から最短の”月”営業日を取得します
            ============================================== -->
            <h2><a name="getAfterHolidayMonth">Mikoshiva_Calendar_Open::getAfterHolidayMonth</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              （基底日日付＋指定日数）から最短の”翌”営業日を取得します<br>
              <span style="color: #BB0000">※このメソッドは 2010/01/31 の１ヶ月後は 2010/02/28 と考えます（SQL 風）</span>
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$baseDate</td>
                <td>string</td>
                <td><br></td>
                <td>基底日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$addMonth</td>
                <td>string</td>
                <td><br></td>
                <td>月数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd</td>
                <td>日付フォーマット</td>
                <td>
                  Zend_Date の <a href="http://framework.zend.com/manual/ja/zend.date.constants.html#zend.date.constants.selfdefinedformats">ISO 書式指定子を使用して自分で定義する出力フォーマット</a>を参照して下さい。
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$mode</td>
                <td>string</td>
                <td>Mikoshiva_Calendar_Open::ALL</td>
                <td>祝日・休日対象モード<br>（対象とする休日・祝日）</td>
                <td><pre>
Mikoshiva_Calendar_Open::ALL             祝日・休日すべて
Mikoshiva_Calendar_Open::FUNAI           船井休日
Mikoshiva_Calendar_Open::DIRECT          ダイレクト出版休日
Mikoshiva_Calendar_Open::HOLIDAY         国民の祝日
Mikoshiva_Calendar_Open::HOLIDAY_FUNAI   国民の祝日・船井休日
Mikoshiva_Calendar_Open::HOLIDAY_DIRECT  国民の祝日・ダイレクト出版休日
Mikoshiva_Calendar_Open::FUNAI_DIRECT    船井・ダイレクト出版休日
                </pre></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■カレンダー・サンプル
//
//------------------------------------------------
require_once \'Mikoshiva/Calendar/Open.php\';

// 第二引数で 1 月後を指定（2007/02/28）しており、2007/02/28 は祝日ではないため 2007/02/28 が返る
dumper(Mikoshiva_Calendar_Open::getAfterHolidayMonth(\'2007/01/31\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));

// 第二引数で 1 月後を指定（2010/02/28）しており、2010/02/28 は祝日あるため 2010/03/01 が返る
dumper(Mikoshiva_Calendar_Open::getAfterHolidayMonth(\'2010/01/31\', 1, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Calendar_Open::getBeforeManyBusinessDayDay - 基底日日付から指定営業日を”経過”した営業日(n営業日後)を取得します
            ============================================== -->
            <h2><a name="getBeforeManyBusinessDayDay">Mikoshiva_Calendar_Open::getBeforeManyBusinessDayDay</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              基底日日付から指定営業日を”経過”した営業日(n営業日後)を取得します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$baseDate</td>
                <td>string</td>
                <td><br></td>
                <td>基底日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$addDay</td>
                <td>string</td>
                <td><br></td>
                <td>日数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd</td>
                <td>日付フォーマット</td>
                <td>
                  Zend_Date の <a href="http://framework.zend.com/manual/ja/zend.date.constants.html#zend.date.constants.selfdefinedformats">ISO 書式指定子を使用して自分で定義する出力フォーマット</a>を参照して下さい。
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$mode</td>
                <td>string</td>
                <td>Mikoshiva_Calendar_Open::ALL</td>
                <td>祝日・休日対象モード</td>
                <td><pre>
Mikoshiva_Calendar_Open::ALL             祝日・休日すべて
Mikoshiva_Calendar_Open::FUNAI           船井休日
Mikoshiva_Calendar_Open::DIRECT          ダイレクト出版休日
Mikoshiva_Calendar_Open::HOLIDAY         国民の祝日
Mikoshiva_Calendar_Open::HOLIDAY_FUNAI   国民の祝日・船井休日
Mikoshiva_Calendar_Open::HOLIDAY_DIRECT  国民の祝日・ダイレクト出版休日
Mikoshiva_Calendar_Open::FUNAI_DIRECT    船井・ダイレクト出版休日
                </pre></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■カレンダー・サンプル
//   （指定営業日前を返します）
//------------------------------------------------
require_once \'Mikoshiva/Calendar/Open.php\';

// 第一引数で 2010/02/10 第二引数で 2 営業日前を指定しており、2010/02/11 は祝日 2/13(土)、2/14(日)を挟むため 2/15 が返る
// 2010/02/10 指定日
// 2010/02/11 休日　　祝日
// 2010/02/12 営業日
// 2010/02/13 休日　　土曜日
// 2010/02/14 休日　　日曜日
// 2010/02/15 営業日
dumper(Mikoshiva_Calendar_Open::getAfterManyBusinessDayDay(\'2010/02/10\', 2, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));

');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Calendar_Open::getAfterManyBusinessDayDay - 基底日日付から指定営業日を”遡った”営業日(n営業日前)を取得します
            ============================================== -->
            <h2><a name="getAfterManyBusinessDayDay">Mikoshiva_Calendar_Open::getAfterManyBusinessDayDay</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
                基底日日付から指定営業日を”遡った”営業日(n営業日前)を取得します
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$baseDate</td>
                <td>string</td>
                <td><br></td>
                <td>基底日</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$addDay</td>
                <td>string</td>
                <td><br></td>
                <td>日数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>3</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd</td>
                <td>日付フォーマット</td>
                <td>
                  Zend_Date の <a href="http://framework.zend.com/manual/ja/zend.date.constants.html#zend.date.constants.selfdefinedformats">ISO 書式指定子を使用して自分で定義する出力フォーマット</a>を参照して下さい。
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>$mode</td>
                <td>string</td>
                <td>Mikoshiva_Calendar_Open::ALL</td>
                <td>祝日・休日対象モード</td>
                <td><pre>
Mikoshiva_Calendar_Open::ALL             祝日・休日すべて
Mikoshiva_Calendar_Open::FUNAI           船井休日
Mikoshiva_Calendar_Open::DIRECT          ダイレクト出版休日
Mikoshiva_Calendar_Open::HOLIDAY         国民の祝日
Mikoshiva_Calendar_Open::HOLIDAY_FUNAI   国民の祝日・船井休日
Mikoshiva_Calendar_Open::HOLIDAY_DIRECT  国民の祝日・ダイレクト出版休日
Mikoshiva_Calendar_Open::FUNAI_DIRECT    船井・ダイレクト出版休日
                </pre></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■カレンダー・サンプル
//   （指定営業日後を返します）
//------------------------------------------------
require_once \'Mikoshiva/Calendar/Open.php\';

// 第一引数で 2010/02/15 第二引数で 2 営業日後を指定しており、2010/02/11 は祝日 2/13(土)、2/14(日)を挟むため 2/10 が返る
// 2010/02/15 指定日
// 2010/02/14 休日　　日曜日
// 2010/02/13 休日　　土曜日
// 2010/02/12 営業日
// 2010/02/11 休日　　祝日
// 2010/02/10 営業日
dumper(Mikoshiva_Calendar_Open::getBeforeManyBusinessDayDay(\'2010/02/15\', 2, \'yyyy-MM-dd HH:mm:ss\', Mikoshiva_Calendar_Open::ALL));

');
?>
            </pre>







  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>











