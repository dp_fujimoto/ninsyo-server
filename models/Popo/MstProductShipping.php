<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product_shipping
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProductShipping extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mps';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productShippingId' => null,
        'divisionId' => null,
        'productId' => null,
        'productShippingCode' => null,
        'productType' => null,
        'productShippingName' => null,
        'totalShippingNumber' => null,
        'campaignProductShippingFlag' => null,
        'singleShippingFlag' => null,
        'digitalProductFlag' => null,
        'shippingFlag' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 商品発送ID の値を返します
	 *
     * @return int 商品発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingId() {
		return $this->_properties['productShippingId'];
    }


    /**
     * 商品発送ID の値をセットします
	 *
	 * @param int $productShippingId 商品発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingId($productShippingId) {
		$this->_properties['productShippingId'] = $productShippingId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 商品ID の値を返します
	 *
     * @return int 商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * 商品ID の値をセットします
	 *
	 * @param int $productId 商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * 商品発送コード の値を返します
	 *
     * @return varchar 商品発送コード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingCode() {
		return $this->_properties['productShippingCode'];
    }


    /**
     * 商品発送コード の値をセットします
	 *
	 * @param varchar $productShippingCode 商品発送コード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingCode($productShippingCode) {
		$this->_properties['productShippingCode'] = $productShippingCode;
    }


    /**
     * 商品タイプ の値を返します
	 *
     * @return varchar 商品タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductType() {
		return $this->_properties['productType'];
    }


    /**
     * 商品タイプ の値をセットします
	 *
	 * @param varchar $productType 商品タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductType($productType) {
		$this->_properties['productType'] = $productType;
    }


    /**
     * 商品配送名 の値を返します
	 *
     * @return varchar 商品配送名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingName() {
		return $this->_properties['productShippingName'];
    }


    /**
     * 商品配送名 の値をセットします
	 *
	 * @param varchar $productShippingName 商品配送名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingName($productShippingName) {
		$this->_properties['productShippingName'] = $productShippingName;
    }


    /**
     * 総発送回数 の値を返します
	 *
     * @return int 総発送回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTotalShippingNumber() {
		return $this->_properties['totalShippingNumber'];
    }


    /**
     * 総発送回数 の値をセットします
	 *
	 * @param int $totalShippingNumber 総発送回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTotalShippingNumber($totalShippingNumber) {
		$this->_properties['totalShippingNumber'] = $totalShippingNumber;
    }


    /**
     * キャンペーン商品発送フラグ の値を返します
	 *
     * @return char キャンペーン商品発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductShippingFlag() {
		return $this->_properties['campaignProductShippingFlag'];
    }


    /**
     * キャンペーン商品発送フラグ の値をセットします
	 *
	 * @param char $campaignProductShippingFlag キャンペーン商品発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductShippingFlag($campaignProductShippingFlag) {
		$this->_properties['campaignProductShippingFlag'] = $campaignProductShippingFlag;
    }


    /**
     * 一括発送フラグ の値を返します
	 *
     * @return char 一括発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSingleShippingFlag() {
		return $this->_properties['singleShippingFlag'];
    }


    /**
     * 一括発送フラグ の値をセットします
	 *
	 * @param char $singleShippingFlag 一括発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSingleShippingFlag($singleShippingFlag) {
		$this->_properties['singleShippingFlag'] = $singleShippingFlag;
    }


    /**
     * mps_digital_product_flag の値を返します
	 *
     * @return char mps_digital_product_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDigitalProductFlag() {
		return $this->_properties['digitalProductFlag'];
    }


    /**
     * mps_digital_product_flag の値をセットします
	 *
	 * @param char $digitalProductFlag mps_digital_product_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDigitalProductFlag($digitalProductFlag) {
		$this->_properties['digitalProductFlag'] = $digitalProductFlag;
    }


    /**
     * 発送フラグ の値を返します
	 *
     * @return char 発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingFlag() {
		return $this->_properties['shippingFlag'];
    }


    /**
     * 発送フラグ の値をセットします
	 *
	 * @param char $shippingFlag 発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingFlag($shippingFlag) {
		$this->_properties['shippingFlag'] = $shippingFlag;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























