<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstDmCampaignSearchProduct
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstDmCampaignSearchProduct extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_dm_campaign_search_product';
    protected $_primary  = 'msp_dm_campaign_search_product_id';
    protected $_metadata = array (
  'msp_dm_campaign_search_product_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_dm_campaign_search_product_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'msp_dm_campaign_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_dm_campaign_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_number' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_number',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_product_id1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_product_id1',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_contract_status_type1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_contract_status_type1',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_from_proposal_day_number1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_from_proposal_day_number1',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_to_proposal_day_number1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_to_proposal_day_number1',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_product_id2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_product_id2',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_contract_status_type2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_contract_status_type2',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_from_proposal_day_number2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_from_proposal_day_number2',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_to_proposal_day_number2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_to_proposal_day_number2',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_product_id3' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_product_id3',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_contract_status_type3' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_contract_status_type3',
    'COLUMN_POSITION' => 13,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_from_proposal_day_number3' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_from_proposal_day_number3',
    'COLUMN_POSITION' => 14,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_to_proposal_day_number3' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_to_proposal_day_number3',
    'COLUMN_POSITION' => 15,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_product_id4' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_product_id4',
    'COLUMN_POSITION' => 16,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_contract_status_type4' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_contract_status_type4',
    'COLUMN_POSITION' => 17,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_from_proposal_day_number4' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_from_proposal_day_number4',
    'COLUMN_POSITION' => 18,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_to_proposal_day_number4' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_to_proposal_day_number4',
    'COLUMN_POSITION' => 19,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_product_id5' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_product_id5',
    'COLUMN_POSITION' => 20,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_contract_status_type5' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_contract_status_type5',
    'COLUMN_POSITION' => 21,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_from_proposal_day_number5' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_from_proposal_day_number5',
    'COLUMN_POSITION' => 22,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_to_proposal_day_number5' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_to_proposal_day_number5',
    'COLUMN_POSITION' => 23,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_delete_flag',
    'COLUMN_POSITION' => 24,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_deletion_datetime',
    'COLUMN_POSITION' => 25,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_registration_datetime',
    'COLUMN_POSITION' => 26,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_update_datetime',
    'COLUMN_POSITION' => 27,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'msp_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_dm_campaign_search_product',
    'COLUMN_NAME' => 'msp_update_timestamp',
    'COLUMN_POSITION' => 28,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

