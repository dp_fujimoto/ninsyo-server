<?php


//require_once 'Mikoshiva/Settlement/Popo/Input/Order.php';

/**
 * 決済処理実行クラスの Interface
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/08
 * @version SVN:$Id: Interface.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
Interface Mikoshiva_Settlement_Execution_Order_Interface {


    /**
     * コンストラクタ
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/08
     * @version SVN:$Id: Interface.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo);


    /**
     * 実行クラス
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/08
     * @version SVN:$Id: Interface.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function execute();


    /**
     * 処理結果を返します
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/08
     * @version SVN:$Id: Interface.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getOutput();
}