<?php
/**
 * @package Mikoshiva_Utility
 */


/**
 * クラス関連ユーティリティ
 *
 * @package Mikoshiva_Utility
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/08
 * @version SVN:$Id: Class.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Class {


    /**
     * コールされたクラス名とメソッド名を返します
     *
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/08
     * @version SVN:$Id: Class.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function getCalledClass($number = 1) {


        $bt = debug_backtrace();
        if (!isset($bt[$number])) throw new Exception("Cannot find called class -> stack level too deep.");
        if (!isset($bt[$number]['type'])) {
            return $bt[$number]['function'];
        } else {

            switch ($bt[$number]['type']) {
                case '::':
                    $ret = $bt[$number]['class'] . '::' . $bt[$number]['function'];
                    if (isset($bt[$number - 1]['line'])) $ret .= '（' . $bt[$number - 1]['line'] . '）';
                    return $ret;
                    // won't get here.
                case '->':
                    $ret = $bt[$number]['class'] . '#' . $bt[$number]['function'];
                    if (isset($bt[$number - 1]['line'])) $ret .= '（' . $bt[$number - 1]['line'] . '）';
                    return $ret;

                default: throw new Exception ("Unknown backtrace method type");
            }
        }
    }
}