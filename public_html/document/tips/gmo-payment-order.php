<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="注文（オーダー）サンプル">注文（オーダー）サンプル</h1>

            <p>注文（オーダー）サンプル</p>



            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <div class="system-message">
              <p>
                実行クラスは以下の場合をシステムエラーとみなして例外を投げます。<br>
                <span style="color: #bb0000">※GMO における決済の失敗（エラー）は false を返します。</span>
              </p>
              <ol>
                <li>必須の値がセットされていない場合</li>
                <li>GMO 決済 API への接続に失敗した場合</li>
                <li>何らかの理由でレスポンスが取得出来なかった場合</li>
              </ol>
            </div>
            <pre class="wiki"><?php
highlight_string('<?php

//-----------------------------------------------
//
// ■注文（オーダー）サンプル
//
//------------------------------------------------


//----------------------------------------------
// 商品情報 POPO に商品情報をセットする
//----------------------------------------------
include_once \'Popo/MstProduct.php\';
include_once \'Popo/MstCampaignProduct.php\';
include_once \'Popo/MstCampaign.php\';
include_once \'Popo/MstProductShipping.php\';
include_once \'Mikoshiva/Settlement/Popo/Input/Order/ProductInfo.php\';
$productInfo = new Mikoshiva_Settlement_Popo_Input_Order_ProductInfo();
$productInfo->setMstProduct(new Popo_MstProduct());                      // mst_product
$productInfo->setMstCampaignProduct(new Popo_MstCampaignProduct());      // mst_campaign_product
$productInfo->setMstCampaign(new Popo_MstCampaign());                    // mst_campaign
$productInfo->setMstProductShipping(new Popo_MstProductShipping());      // mst_product_shipping
$productInfo->setPaymentNumber(1);                                       // 購入回数
$productInfo->setPaymentAmount(100);                                     // 支払い金額



//----------------------------------------------
// 商品事の配列にする
//----------------------------------------------
$productInfoList   = array();
$productInfoList[] = $productInfo;


//----------------------------------------------
// 決済に必要な基本情報をセット
//----------------------------------------------
include_once \'Mikoshiva/Settlement/Popo/Input/Order.php\';
$orderInputPopo = new Mikoshiva_Settlement_Popo_Input_Order();
$orderInputPopo->setCardNo($actionForm->get(\'cardNo\'));                                  // カード番号
$orderInputPopo->setExpire($actionForm->get(\'cardYear\') . $actionForm->get(\'getMonth\')); // カード有効期限
$orderInputPopo->setMethod(\'1\');                                                         // 分割払い （値の詳細は以下に記述）
$orderInputPopo->setPayTimes(\'2\');                                                       // 支払い回数
$orderInputPopo->setProductDataList($productInfoList);                                   // 商品情報



//----------------------------------------------
// 決済実行
//----------------------------------------------
include_once \'Mikoshiva/Settlement/Execution/Order/Factory.php\';
// $instance  = Mikoshiva_Settlement_Execution_Order_Factory::getInstance($actionForm->get(\'paymentType\'), $orderPopo);
$instance = Mikoshiva_Settlement_Execution_Order_Factory::getInstance($orderInputPopo);
$ret      = $instance->execute();    // 実行 true / false


//-----------------------------------------------
// 処理結果を取得
//
// Mikoshiva_Settlement_Popo_Output_Order が詰まれた配列が返ります
// $outputList[0] = new Mikoshiva_Settlement_Popo_Output_Order();
//-----------------------------------------------
$outputList = $instance->getOutput();  // GMO 結果クラスを返します

'); ?>
            </pre>

            <h2 id="使用例">サンプル入力データ</h2>
            <pre class="wiki"><?php
highlight_string('<?php

object(Mikoshiva_Settlement_Popo_Input_Order)#87 (1) {
  ["_properties:protected"] => array(8) {
    ["CardNo"] => string(16) "4111111111111111"
    ["Expire"] => string(4) "1212"
    ["Method"] => string(20) "PAYMENT_TYPE_MONTHLY"
    ["PayTimes"] => string(2) "24"
    ["customerId"] => string(10) "test152732"
    ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
    ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
    ["productDataList"] => array(4) {
      [0] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#65 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#59 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000153"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#76 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#75 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#64 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(2)
          ["paymentAmount"] => int(10001)
        }
      }
      [1] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#79 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#66 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000153"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#71 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "0"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#84 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#81 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(3)
          ["paymentAmount"] => int(10001)
        }
      }
      [2] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#72 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#78 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000591"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#61 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#70 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#67 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(4)
          ["paymentAmount"] => int(10001)
        }
      }
      [3] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#74 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#68 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000591"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#77 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#69 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#73 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(5)
          ["paymentAmount"] => int(10001)
        }
      }
    }
  }
}

'); ?>
            </pre>






            <h2 id="使用例">サンプル出力データ</h2>

            <pre class="wiki"><?php
highlight_string('<?php

array(4) {
  [0] => object(Mikoshiva_Settlement_Popo_Output_Order)#147 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-002"
      ["ShopID"] => string(13) "testpg0000153"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "1"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#65 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#59 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000153"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#76 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#75 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#64 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(2)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#133 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#145 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "08f16cf9817df9ded695a6a15e4f21bb"
              ["AccessPass"] => string(32) "b4d105602267d50e959f725e7059cca1"
              ["OrderID"] => string(27) "DP-test150003-002-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:52"
            }
          }
          ["auth"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#151 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "4f8bf6b20f8c8d67e82229704354cb08"
              ["AccessPass"] => string(32) "4165a1ea65ba03a8dd57fc253573e98d"
              ["OrderID"] => string(27) "DP-test150003-002-201005-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => int(0)
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:53"
            }
          }
        }
      }
    }
  }
  [1] => object(Mikoshiva_Settlement_Popo_Output_Order)#143 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-003"
      ["ShopID"] => string(13) "testpg0000153"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "0"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#79 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#66 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000153"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#71 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "0"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#84 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#81 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(3)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#125 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#137 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "08f16cf9817df9ded695a6a15e4f21bb"
              ["AccessPass"] => string(32) "b4d105602267d50e959f725e7059cca1"
              ["OrderID"] => string(27) "DP-test150003-003-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:55"
            }
          }
          ["auth"] => NULL
        }
      }
    }
  }
  [2] => object(Mikoshiva_Settlement_Popo_Output_Order)#144 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-004"
      ["ShopID"] => string(13) "testpg0000591"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "1"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#72 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#78 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000591"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#61 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#70 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#67 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(4)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#134 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#153 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "449720b3d6837e757260220c8a0295e2"
              ["AccessPass"] => string(32) "9536d4aae3992bbd62b1c2c19fb3927f"
              ["OrderID"] => string(27) "DP-test150003-004-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:57"
            }
          }
          ["auth"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#155 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "1b04c9105c31748b575f74fd8452379a"
              ["AccessPass"] => string(32) "40a85960e88247c2197c1a163192a96f"
              ["OrderID"] => string(27) "DP-test150003-004-201005-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => int(0)
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:59"
            }
          }
        }
      }
    }
  }
  [3] => object(Mikoshiva_Settlement_Popo_Output_Order)#150 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-005"
      ["ShopID"] => string(13) "testpg0000591"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "1"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#74 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#68 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000591"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#77 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#69 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#73 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(5)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#152 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#154 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "449720b3d6837e757260220c8a0295e2"
              ["AccessPass"] => string(32) "9536d4aae3992bbd62b1c2c19fb3927f"
              ["OrderID"] => string(27) "DP-test150003-005-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 15:00:01"
            }
          }
          ["auth"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#156 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "c60f1e367febd0df309fd944085a5690"
              ["AccessPass"] => string(32) "89b18f8ff32140a2a5ea6d9d9b2835b7"
              ["OrderID"] => string(27) "DP-test150003-005-201005-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => int(0)
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 15:00:02"
            }
          }
        }
      }
    }
  }
}

array(4) {
  [0] => object(Mikoshiva_Settlement_Popo_Output_Order)#147 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-002"
      ["ShopID"] => string(13) "testpg0000153"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "1"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#65 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#59 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000153"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#76 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#75 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#64 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(2)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#133 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#145 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "08f16cf9817df9ded695a6a15e4f21bb"
              ["AccessPass"] => string(32) "b4d105602267d50e959f725e7059cca1"
              ["OrderID"] => string(27) "DP-test150003-002-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:52"
            }
          }
          ["auth"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#151 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "4f8bf6b20f8c8d67e82229704354cb08"
              ["AccessPass"] => string(32) "4165a1ea65ba03a8dd57fc253573e98d"
              ["OrderID"] => string(27) "DP-test150003-002-201005-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => int(0)
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:53"
            }
          }
        }
      }
    }
  }
  [1] => object(Mikoshiva_Settlement_Popo_Output_Order)#143 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-003"
      ["ShopID"] => string(13) "testpg0000153"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "0"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#79 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#66 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000153"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#71 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "0"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#84 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#81 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(3)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#125 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#137 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "08f16cf9817df9ded695a6a15e4f21bb"
              ["AccessPass"] => string(32) "b4d105602267d50e959f725e7059cca1"
              ["OrderID"] => string(27) "DP-test150003-003-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:55"
            }
          }
          ["auth"] => NULL
        }
      }
    }
  }
  [2] => object(Mikoshiva_Settlement_Popo_Output_Order)#144 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-004"
      ["ShopID"] => string(13) "testpg0000591"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "1"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#72 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#78 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000591"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#61 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#70 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#67 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(4)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#134 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#153 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "449720b3d6837e757260220c8a0295e2"
              ["AccessPass"] => string(32) "9536d4aae3992bbd62b1c2c19fb3927f"
              ["OrderID"] => string(27) "DP-test150003-004-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:57"
            }
          }
          ["auth"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#155 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "1b04c9105c31748b575f74fd8452379a"
              ["AccessPass"] => string(32) "40a85960e88247c2197c1a163192a96f"
              ["OrderID"] => string(27) "DP-test150003-004-201005-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => int(0)
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 14:59:59"
            }
          }
        }
      }
    }
  }
  [3] => object(Mikoshiva_Settlement_Popo_Output_Order)#150 (1) {
    ["_properties:protected"] => array(7) {
      ["MemberID"] => string(17) "DP-test150003-005"
      ["ShopID"] => string(13) "testpg0000591"
      ["chargeType"] => string(27) "CHARGE_TYPE_CARD_CONTINUITY"
      ["paymentType"] => string(19) "PAYMENT_TYPE_SINGLE"
      ["authFlag"] => string(1) "1"
      ["inputProductInfo"] => object(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo)#74 (1) {
        ["_properties:protected"] => array(6) {
          ["mstProduct"] => object(Popo_MstProduct)#68 (1) {
            ["_properties:protected"] => array(12) {
              ["productId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["shopId"] => string(13) "testpg0000591"
              ["licenseId"] => string(1) "1"
              ["productCode"] => string(3) "TST"
              ["productName"] => string(15) "テスト商品"
              ["continuityShippingDay"] => NULL
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaignProduct"] => object(Popo_MstCampaignProduct)#77 (1) {
            ["_properties:protected"] => array(46) {
              ["campaignProductId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignProductName"] => NULL
              ["campaignId"] => string(1) "1"
              ["productShippingId"] => string(1) "1"
              ["optionSalesFlag"] => string(1) "0"
              ["salesTypeNumber"] => string(1) "1"
              ["paymentType"] => string(20) "PAYMENT_TYPE_MONTHLY"
              ["price"] => string(3) "999"
              ["subscription"] => string(5) "20000"
              ["subscriptionType"] => string(24) "SUBSCRIPTION_TYPE_NORMAL"
              ["subscriptionPeriod"] => string(1) "0"
              ["midtermCancellationFlag"] => string(1) "0"
              ["creditCardFlag"] => string(1) "1"
              ["paymentDeliveryFlag"] => string(1) "1"
              ["bankTransferFlag"] => string(1) "1"
              ["creditCardInstallmentFlag"] => string(1) "0"
              ["directInstallmentFlag"] => string(1) "1"
              ["directInstallmentNumber"] => string(1) "5"
              ["provisorySalesFlag"] => string(1) "1"
              ["freePeriodCreditCard"] => string(1) "1"
              ["freePeriodTypeCreditCard"] => string(34) "FREE_PERIOD_TYPE_CREDIT_CARD_MONTH"
              ["freePeriodPaymentDelivery"] => string(1) "1"
              ["freePeriodTypePaymentDelivery"] => string(39) "FREE_PERIOD_TYPE_PAYMENT_DELIVERY_MONTH"
              ["freePeriodBankTransfer"] => string(1) "1"
              ["freePeriodTypeBankTransfer"] => string(36) "FREE_PERIOD_TYPE_BANK_TRANSFER_MONTH"
              ["guaranteePeriod"] => string(1) "1"
              ["guaranteePeriodType"] => string(26) "GUARANTY_PERIOD_TYPE_MONTH"
              ["refundWithReturnsFlag"] => string(1) "1"
              ["shippingCost"] => string(1) "1"
              ["continuityShippingCost"] => string(2) "10"
              ["paymentDeliveryFee"] => string(3) "100"
              ["continuityPaymentDeliveryFee"] => string(4) "1000"
              ["shippingStartDate"] => NULL
              ["salesNumber"] => NULL
              ["duplicationCheckFlag"] => string(1) "1"
              ["bonusFlag"] => NULL
              ["thanksMailTemplateId"] => string(1) "1"
              ["thanksMailHeader"] => string(42) "テストサンキューメールヘッダ"
              ["thanksMailFooter"] => string(42) "テストサンキューメールフッタ"
              ["memo"] => string(39) "テストキャンペーン商品メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstCampaign"] => object(Popo_MstCampaign)#69 (1) {
            ["_properties:protected"] => array(14) {
              ["campaignId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["campaignCode"] => string(7) "CP_TEST"
              ["campaignName"] => string(27) "テストキャンペーン"
              ["multipleProductFlag"] => string(1) "0"
              ["salesStartDate"] => string(19) "2010-03-25 09:38:02"
              ["salesEndDate"] => string(19) "2010-03-25 09:38:02"
              ["salesFlag"] => string(1) "0"
              ["memo"] => string(33) "テストキャンペーンメモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["mstProductShipping"] => object(Popo_MstProductShipping)#73 (1) {
            ["_properties:protected"] => array(14) {
              ["productShippingId"] => string(1) "1"
              ["divisionId"] => string(1) "1"
              ["productId"] => string(1) "1"
              ["productShippingCode"] => string(26) "TEST_PRODUCT_SHIPPING_CODE"
              ["productType"] => string(29) "PRODUCT_TYPE_MICRO_CONTINUITY"
              ["productShippingName"] => string(24) "テスト商品発送名"
              ["totalShippingNumber"] => string(1) "5"
              ["shippingFlag"] => string(1) "0"
              ["memo"] => string(27) "テスト商品発送メモ"
              ["deleteFlag"] => string(1) "0"
              ["deletionDatetime"] => NULL
              ["updateDatetime"] => string(19) "2010-03-25 09:38:02"
              ["registrationDatetime"] => string(19) "2010-03-25 09:38:02"
              ["updateTimestamp"] => string(19) "2010-03-25 09:38:02"
            }
          }
          ["paymentNumber"] => int(5)
          ["paymentAmount"] => int(10001)
        }
      }
      ["settlementOutputList"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement_List)#152 (1) {
        ["_properties:protected"] => array(2) {
          ["normal"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#154 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "449720b3d6837e757260220c8a0295e2"
              ["AccessPass"] => string(32) "9536d4aae3992bbd62b1c2c19fb3927f"
              ["OrderID"] => string(27) "DP-test150003-005-000000-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => string(1) "1"
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 15:00:01"
            }
          }
          ["auth"] => object(Mikoshiva_Settlement_Popo_Output_Order_Settlement)#156 (1) {
            ["_properties:protected"] => array(9) {
              ["AccessID"] => string(32) "c60f1e367febd0df309fd944085a5690"
              ["AccessPass"] => string(32) "89b18f8ff32140a2a5ea6d9d9b2835b7"
              ["OrderID"] => string(27) "DP-test150003-005-201005-00"
              ["ErrCode"] => NULL
              ["ErrInfo"] => NULL
              ["ErrFlag"] => int(0)
              ["paymentAmount"] => int(0)
              ["paymenShippingCost"] => NULL
              ["paymentDatetime"] => string(19) "2010-04-05 15:00:02"
            }
          }
        }
      }
    }
  }
}

'); ?>
            </pre>

            <h2 id="Mikoshiva_Settlement_Popo_Input_Order_ProductInfo">Mikoshiva_Settlement_Popo_Input_Order_ProductInfo（商品情報を保持する POPO クラス）</h2>
            <h3>Property</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>必須</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>mstProduct</td>
                <td>●</td>
                <td>Popo_MstProduct</td>
                <td>Popo_MstProduct</td>
                <td><br></td>
              </tr>
              <tr>
                <td>mstCampaignProduct</td>
                <td>●</td>
                <td>Popo_MstCampaignProduct</td>
                <td>Popo_MstCampaignProduct</td>
                <td><br></td>
              </tr>
              <tr>
                <td>mstCampaign</td>
                <td>●</td>
                <td>Popo_MstCampaign</td>
                <td>Popo_MstCampaign</td>
                <td><br></td>
              </tr>
              <tr>
                <td>mstProductShipping</td>
                <td>●</td>
                <td>Popo_MstProductShipping</td>
                <td>Popo_MstProductShipping</td>
                <td><br></td>
              </tr>
              <tr>
                <td>paymentNumber</td>
                <td>●</td>
                <td>int</td>
                <td>決済回数</td>
                <td><br></td>
              </tr>
              <tr>
                <td>paymentAmount</td>
                <td>●</td>
                <td>int</td>
                <td>支払い金額</td>
                <td><br></td>
              </tr>
            </table>





            <h2 id="Mikoshiva_Settlement_Popo_Input_Order">Mikoshiva_Settlement_Popo_Input_Order（決済を行うための基本情報を保持する POPO クラス）</h2>
            <h3>Property</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>必須</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>CardNo</td>
                <td><br></td>
                <td>string</td>
                <td>カード番号</td>
                <td><p>クレジットカード払いのみ必須</p></td>
              </tr>
              <tr>
                <td>Expire</td>
                <td><br></td>
                <td>string</td>
                <td>カード有効期限</td>
                <td><p>クレジットカード払いのみ必須</p></td>
              </tr>
              <tr>
                <td>Method</td>
                <td><br></td>
                <td>string</td>
                <td>カード支払い方法</td>
                <td>
                  <p>クレジットカード払いのみ必須</p>
                  １：一括<br>
                  ２：分割<br>
                  <del>３：ボーナス一括</del>（使用しない）<br>
                  <del>４：ボーナス分割</del>（使用しない）<br>
                  <del>５；リボ</del>（使用しない）<br>
                </td>
              </tr>
              <tr>
                <td>PayTimes</td>
                <td><br></td>
                <td>string</td>
                <td>カード登録結果 POPO クラス</td>
                <td>
                  <p>クレジットカード払いのみ必須</p>
                  支払い回数
                </td>
              </tr>
              <tr>
                <td>customerId</td>
                <td>●</td>
                <td>int</td>
                <td>顧客 ID</td>
                <td><br></br></td>
              </tr>
              <tr>
                <td>productDataList</td>
                <td>●</td>
                <td>Mikoshiva_Settlement_Popo_Input_Order_ProductInfo</td>
                <td>商品情報を保持する POPO クラス</td>
                <td><br></br></td>
              </tr>
            </table>




            <h2 id="Mikoshiva_Settlement_Execution_Order_Factory">Mikoshiva_Settlement_Execution_Order_Factory（支払い方法に応じた決済実行クラスを返す Factory クラス）</h2>
            <h3>Method</h3>
            <h4>getInstance</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$paymentType</td>
                <td>int</td>
                <td>1</td>
                <td>支払い方法</td>
                <td>この値によって決済実行クラスが決定されます</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$inputPopo</td>
                <td>Mikoshiva_Settlement_Popo_Input_Order</td>
                <td><br></td>
                <td>決済を行うための基本情報を保持する POPO クラス</td>
                <td>この POPO クラスは決済実クラスに受渡されます</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></br></td>
                <td>Mikoshiva_Settlement_Execution_Order_Interface</td>
                <td><br></td>
                <td>決済実行クラス</td>
                <td>Interface</td>
              </tr>
            </table>




            <h2 id="Mikoshiva_Settlement_Execution_Order_Interface">Mikoshiva_Settlement_Execution_Order_Interface（決済実行クラスの Interface クラス）</h2>
            <h3>Method</h3>
            <h4>__construct</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$orderPopo</td>
                <td>Mikoshiva_Settlement_Popo_Input_Order</td>
                <td><br></td>
                <td>商品情報を保持する POPO クラス</td>
                <td><br></td>
              </tr>
            </table>

            <h4>execute</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>boolean</td>
                <td><br></td>
                <td>決済実行メソッド</td>
                <td>決済の実行を行い、処理結果を返します</td>
              </tr>
            </table>

            <h4>getOutput</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>array</td>
                <td><br></td>
                <td>決済処理結果</td>
                <td>Mikoshiva_Settlement_Popo_Output_Order が詰まれた配列が返ります</td>
              </tr>
            </table>


            <h2 id="Mikoshiva_Settlement_Popo_Output_Order">Mikoshiva_Settlement_Popo_Output_Order（決済結果を保持するクラス）</h2>
            <h3>Property</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>AccessID</td>
                <td>string</td>
                <td>アクセス ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>AccessPass</td>
                <td>string</td>
                <td>アクセスパスワード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>OrderID</td>
                <td>string</td>
                <td>オーダー ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>MemberID</td>
                <td>string</td>
                <td>会員 ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>ErrCode</td>
                <td>string</td>
                <td>エラーコード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>ErrInfo</td>
                <td>string</td>
                <td>エラー詳細コード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>ErrFlag</td>
                <td>string</td>
                <td>エラーフラグ</td>
                <td>０：成功　１：失敗</td>
              <tr>
                <td>chargeType</td>
                <td>string</td>
                <td>請求方法</td>
                <td><br></td>
              </tr>
              </tr>
              <tr>
                <td>paymentType</td>
                <td>string</td>
                <td>支払い方法</td>
                <td><br></td>
              </tr>
              <tr>
                <td>paymentAmount</td>
                <td>string</td>
                <td>支払い料金</td>
                <td><br></td>
              </tr>
              <tr>
                <td>paymentDatetime</td>
                <td>string</td>
                <td>支払い日時</td>
                <td><br></td>
              </tr>
              <tr>
                <td>inputProductInfo</td>
                <td>Mikoshiva_Settlement_Popo_Input_Order_ProductInfo</td>
                <td>商品情報</td>
                <td><br></td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>




<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>



















