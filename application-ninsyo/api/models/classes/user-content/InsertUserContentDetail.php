<?php
require_once 'Mikoshiva/Controller/Model/Class/Db/Abstract.php';

/**
 * @since 2012/06/25
 * @author fujimoto
 */
class Api_Models_Classes_UserContent_InsertUserContentDetail
    extends Mikoshiva_Controller_Model_Class_Db_Abstract {

    private $_userContentList;


    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute($list = array(), $deleteFlag = '0', $serverNameList) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        // 初期化
        //-----------------------------------------------
        // 検索結果
        $output = array();
        $output['all'] = array();
        $output['error'] = array();

        $nowDatetime = Mikoshiva_date::mikoshivaNow();
        $nowDate = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');

        $datetime = new DateTime($nowDatetime);
        $datetime->modify('-1 day');
        $yesterdayDate = $datetime->format('Y-m-d');

        //-----------------------------------------------
        // 無条件コンテンツID取得
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT ";
        $sql .= "  mcd_content_id";
        $sql .= " FROM mst_condition";
        $sql .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mcd_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mcd_none_condition_flag = '1'";
        $sql .= "   AND mct_test_flag = '0'";
        $sql .= "   AND mct_publication_date <= ?";

        $params   = array();
        $params[] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $freeContentIds = array();
        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $freeContentIds[$v['mcd_content_id']] = $v['mcd_content_id'];
        }
        $output['all'][] = '無条件コンテンツ数：' . count($freeContentIds);

        $count = 0;
        foreach ($list as $hash => $content) {

            $count++;
            $hashArr = explode('|',$hash);
            if (count($hashArr) < 3 || strlen($hashArr[1]) !== HASH_LENGTH || strlen($hashArr[2]) !== HASH_LENGTH) {
                $output['error'][] = $count . '行目,hash値異常,hash：' . $hash;
                continue;
            }

            if (!isset($serverNameList[$hashArr[0]])) {
                $output['error'][] = $count . '行目,サーバー名異常,serverName：' . $hashArr[0];
                continue;
            }

            //-----------------------------------------------
            // ユーザー確認
            //-----------------------------------------------
            // SQL作成
            $sql = "";
            $sql .= "SELECT";
            $sql .= "  mus_user_id,";
            $sql .= "  mus_delete_flag";
            $sql .= " FROM mst_user";
            $sql .= " WHERE mus_server_name = ?";
            $sql .= " AND mus_id_hash = ?";
            $sql .= " AND mus_password_hash = ?";

            $params = array();
            $params[] = $hashArr[0];
            $params[] = $hashArr[1];
            $params[] = $hashArr[2];

            // SQL実行＋全件取得
            $res = array();
            $res = $this->_db->query($sql,$params);

            $userData = array();
            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                $userData = $v;
                break;
            }

            $this->_userContentList = array();
            $userContentDeleteList = array();
            $userContentDeleteAlreadyList = array();
            $userContentBlankList = array();
            $userContentForceList = array();
            $userId = null;
            $userContentData = array();

            if (count($userData) === 0) {

                //-----------------------------------------------
                // 新規ユーザー登録
                //-----------------------------------------------
                $db = null;
                $db = new Mikoshiva_Db_Simple();

                $where = array();
                $where['serverName']   = $hashArr[0];
                $where['idHash']       = $hashArr[1];
                $where['passwordHash'] = $hashArr[2];
                if (isset($hashArr[3]) && !isBlankOrNull($hashArr[3])) {
                    $where['userKey'] = $hashArr[3];
                }
                $where['deleteFlag'] = '0';
                $where['deletionDatetime'] = null;

                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUser', $where);

                $userId = $result->getUserId();
                $output['all'][] = $count . '行目,データ登録,userId:' . $userId;

            } else {
                $userId = $userData['mus_user_id'];

                //-----------------------------------------------
                // ユーザー更新
                //-----------------------------------------------
                $db = null;
                $db = new Mikoshiva_Db_Simple();

                $where = array();
                $where['userId'] = $userId;
                if (isset($hashArr[3]) && !isBlankOrNull($hashArr[3])) {
                    $where['userKey'] = $hashArr[3];
                }
                $where['deleteFlag'] = '0';
                $where['deletionDatetime'] = null;

                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUser', $where);
                //$output['all'][] = $count . '行目,データあり,userId:' . $userId;

                //-----------------------------------------------
                // ユーザーコンテンツ確認
                //-----------------------------------------------
                // SQL作成
                $sql = "";
                $sql .= "SELECT muc_content_id";
                $sql .= "  ,muc_user_content_id";
                $sql .= "  ,muc_service_start_date";
                $sql .= "  ,muc_service_end_date";
                $sql .= "  ,muc_auth_flag";
                $sql .= "  ,muc_delete_flag";
                $sql .= "  ,muc_registration_datetime";
                $sql .= "  ,mct_available_check_flag";
                $sql .= "  ,mct_content_id";
                $sql .= "  ,mct_application_type";
                $sql .= " FROM mst_user_content";
                $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
                $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
                $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
                $sql .= " WHERE muc_user_id = ?";

                $params = array();
                $params[] = $userId;

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                $applicationTypeList = array();
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $this->_userContentList[$v['muc_content_id']] = array();
                    $this->_userContentList[$v['muc_content_id']]['userContentId'] = $v['muc_user_content_id'];
                    $this->_userContentList[$v['muc_content_id']]['deleteFlag'] = $v['muc_delete_flag'];
                    $this->_userContentList[$v['muc_content_id']]['authFlag'] = $v['muc_auth_flag'];
                    $this->_userContentList[$v['muc_content_id']]['serviceStartDate'] = $v['muc_service_start_date'];
                    $this->_userContentList[$v['muc_content_id']]['serviceEndDate'] = $v['muc_service_end_date'];

                    if (!isset($userContentDeleteList[$v['mct_content_id']])) {
                        $userContentDeleteList[$v['mct_content_id']] = array();
                        $userContentDeleteAlreadyList[$v['mct_content_id']] = array();
                    }
                    $userContentDeleteList[$v['mct_content_id']] = $v['muc_user_content_id'];
                    if ($v['muc_delete_flag'] === '1') {
                        $userContentDeleteAlreadyList[$v['mct_content_id']] = $v['muc_user_content_id'];
                    }
                    $userContentData[$v['mct_content_id']] = $v['muc_user_content_id'];
                    $applicationTypeList[$v['mct_application_type']] = 1;
                }

                //-----------------------------------------------
                // ブランク期間取得
                //-----------------------------------------------
                // SQL作成
                $sql = "";
                $sql .= "SELECT *";
                $sql .= " FROM mst_user_content_blank";
                $sql .= " INNER JOIN mst_user_content ON muc_user_content_id = mub_user_content_id";
                $sql .= " WHERE muc_user_id = ?";

                $params = array();
                $params[] = $userId;

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    if (!isset($userContentBlankList[$v['muc_user_content_id']])) {
                        $userContentBlankList[$v['muc_user_content_id']] = array();
                    }
                    $userContentBlankList[$v['muc_user_content_id']][] = $v;
                }

                //-----------------------------------------------
                // 期間強制データ取得
                //-----------------------------------------------
                // SQL作成
                $sql = "";
                $sql .= "SELECT *";
                $sql .= " FROM mst_user_content_force";
                $sql .= " INNER JOIN mst_user_content ON muc_user_content_id = muf_user_content_id";
                $sql .= " WHERE muc_user_id = ?";

                $params = array();
                $params[] = $userId;

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $userContentForceList[$v['muc_user_content_id']] = $v;
                }

                //-----------------------------------------------
                // キャッシュファイル削除
                //-----------------------------------------------
                foreach ($applicationTypeList as $k => $v) {
                    if (file_exists(USER_CONTENT_XML_FILES . '/' . $k . $hashArr[1] . $hashArr[2])) {
                        $output['all'][] = $count . '行目,キャッシュファイルあり削除,appType:' . $k . ',userId:' . $userId;
                        unlink(USER_CONTENT_XML_FILES . '/' . $k . $hashArr[1] . $hashArr[2]);
                    } else {
                        //$output['all'][] = $count . '行目,キャッシュファイルなし,appType:' . $k . ',userId:' . $userId;
                    }
                }
            }

            $contents = explode(',',$content);

            foreach ($contents as $k => $contentIdStr) {
                $contentIdStr = trim($contentIdStr);

                if (isBlankOrNull($contentIdStr)) {
                    continue;
                }
                $contentIdArr = explode('|',$contentIdStr);
                $contentId = $contentIdArr[0];
                $registDate = $nowDate;
                $registEndDate = $nowDate;
                $registBlankDateList = array();

                $registDateFlag = '0';
                if (isset($contentIdArr[1]) && !isBlankOrNull($contentIdArr[1])) {
                    $registDate = $contentIdArr[1];
                    $registDateFlag = '1';

                    if (isset($contentIdArr[2])) {
                        if (!isBlankOrNull($contentIdArr[2])) {
                            $registEndDate = $contentIdArr[2];
                        }
                    }
                }
                if (isset($contentIdArr[3])) {
                    if (!isBlankOrNull($contentIdArr[3])) {
                        $registBlankDateList = explode('<',$contentIdArr[3]);
                        foreach ($registBlankDateList as $registBlankDateListNo => $registBlankDateListRow) {
                            $registBlankDateList[$registBlankDateListNo] = explode('#',$registBlankDateListRow);
                        }
                    }
                }

                // 期間強制 2013-09-18
                if (isset($this->_userContentList[$contentId])) {
                    if (isset($userContentForceList[$this->_userContentList[$contentId]['userContentId']])) {
                        if (!isBlankOrNull($userContentForceList[$this->_userContentList[$contentId]['userContentId']]['muf_start_date'])) {
                            $registDate = $userContentForceList[$this->_userContentList[$contentId]['userContentId']]['muf_start_date'];
                            $registDateFlag = '1';
                        }
                        if (!isBlankOrNull($userContentForceList[$this->_userContentList[$contentId]['userContentId']]['muf_end_date'])) {
                            if ($userContentForceList[$this->_userContentList[$contentId]['userContentId']]['muf_end_date'] < $nowDate) {
                                $registEndDate = $userContentForceList[$this->_userContentList[$contentId]['userContentId']]['muf_end_date'];
                                if (0 < $contentId) {
                                    $contentId = -$contentId;
                                }
                            }
                        }
                    }
                }

                if (is_numeric($contentId)) {

                    $minusFlag = '0';
                    if ($contentId < 0) {
                        $minusFlag = '1';
                        $contentId = $contentId * -1;
                    }

                    //-----------------------------------------------
                    // コンテンツID確認
                    //-----------------------------------------------
                    // SQL作成
                    $sql = "";
                    $sql .= "SELECT mct_content_id";
                    $sql .= " FROM mst_content";
                    $sql .= " WHERE";
                    $sql .= "   mct_delete_flag = '0'";
                    $sql .= "   AND mct_content_id = ?";

                    $params = array();
                    $params[] = $contentId;

                    // SQL実行＋全件取得
                    $res = array();
                    $res = $this->_db->query($sql,$params);

                    $check = false;
                    while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                        $check = true;
                    }

                    if ($check === false) {
                        $output['error'][] = $count . '行目,コンテンツIDは存在しません,contentId:' . $contentId;
                        continue;
                    }

                    if (isset($userContentDeleteList[$contentId])) {
                        unset($userContentDeleteList[$contentId]);
                    }

                    if ($minusFlag === '0') {
                        if (!isset($this->_userContentList[$contentId])) {

                            //-----------------------------------------------
                            // ユーザーコンテンツ登録
                            //-----------------------------------------------
                            $db = null;
                            $db = new Mikoshiva_Db_Simple();

                            $where = array();
                            $where['userId'] = $userId;
                            $where['contentId'] = $contentId;
                            $where['authFlag'] = '1';
                            $where['serviceStartDate'] = $registDate;

                            $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                            $userContentData[$result->getContentId()] = $result->getUserContentId();
                            $this->_set_result($contentId,$result);

                            $output['all'][] = $count . '行目,ユーザーコンテンツ登録,contentId:' . $contentId;

                        } else {

                            if ($this->_userContentList[$contentId]['authFlag'] === '0' || $this->_userContentList[$contentId]['deleteFlag'] === '1') {

                                //-----------------------------------------------
                                // ユーザーコンテンツ更新
                                //-----------------------------------------------
                                $db = null;
                                $db = new Mikoshiva_Db_Simple();

                                $where = array();
                                $where['userContentId'] = $this->_userContentList[$contentId]['userContentId'];
                                $where['authFlag'] = '1';
                                if ($registDateFlag === '1') {
                                    $where['serviceStartDate'] = $registDate;
                                }
                                $where['serviceEndDate'] = null;
                                $where['deleteFlag'] = '0';
                                $where['deletionDatetime'] = null;

                                if (!isBlankOrNull($registDate)) {
                                    $where['serviceStartDate'] = $registDate;
                                }

                                $serviceEndDate = $this->_userContentList[$contentId]['serviceEndDate'];

                                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                                $userContentData[$result->getContentId()] = $result->getUserContentId();
                                $this->_set_result($contentId,$result);

                                $output['all'][] = $count . '行目,ユーザーコンテンツ権限剥奪解除更新,contentId:' . $contentId;

                            } else if ($registDateFlag === '1' && $this->_userContentList[$contentId]['serviceStartDate'] !== $registDate) {

                                //-----------------------------------------------
                                // ユーザーコンテンツ更新
                                //-----------------------------------------------
                                $db = null;
                                $db = new Mikoshiva_Db_Simple();

                                $where = array();
                                $where['userContentId'] = $this->_userContentList[$contentId]['userContentId'];
                                $where['serviceStartDate'] = $registDate;

                                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                                $userContentData[$result->getContentId()] = $result->getUserContentId();
                                $this->_set_result($contentId,$result);

                                $output['all'][] = $count . '行目,ユーザーコンテンツサービス開始日更新,contentId:' . $contentId;

                            } else {
                                //$output['all'][] = $count . '行目,すでに登録済,contentId:' . $contentId;
                            }
                        }
                    } else {
                        if (isset($this->_userContentList[$contentId])) {
                            if ($this->_userContentList[$contentId]['authFlag'] === '1' || $this->_userContentList[$contentId]['deleteFlag'] === '1') {

                                //-----------------------------------------------
                                // ユーザーコンテンツ更新
                                //-----------------------------------------------
                                $db = null;
                                $db = new Mikoshiva_Db_Simple();

                                $where = array();
                                $where['userContentId'] = $this->_userContentList[$contentId]['userContentId'];
                                if ($registDateFlag === '1') {
                                    $where['serviceStartDate'] = $registDate;
                                }
                                $where['serviceEndDate'] = $registEndDate;
                                $where['authFlag'] = '0';
                                $where['deleteFlag'] = '0';
                                $where['deletionDatetime'] = null;

                                if (!isBlankOrNull($registDate)) {
                                    $where['serviceStartDate'] = $registDate . ' 00:00:00';
                                }

                                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                                $userContentData[$result->getContentId()] = $result->getUserContentId();
                                $this->_set_result($contentId,$result);

                                $output['all'][] = $count . '行目,ユーザーコンテンツ権限剥奪更新,contentId:' . $contentId;
                            } else if ($this->_userContentList[$contentId]['serviceEndDate'] !== $registEndDate) {

                                //-----------------------------------------------
                                // ユーザーコンテンツ更新
                                //-----------------------------------------------
                                $db = null;
                                $db = new Mikoshiva_Db_Simple();

                                $where = array();
                                $where['userContentId'] = $this->_userContentList[$contentId]['userContentId'];
                                if ($registDateFlag === '1') {
                                    $where['serviceStartDate'] = $registDate;
                                }
                                $where['serviceEndDate'] = $registEndDate;
                                $where['authFlag'] = '0';
                                $where['deleteFlag'] = '0';
                                $where['deletionDatetime'] = null;

                                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                                $userContentData[$result->getContentId()] = $result->getUserContentId();
                                $this->_set_result($contentId,$result);

                                $output['all'][] = $count . '行目,ユーザーコンテンツサービス終了日更新,contentId:' . $contentId;

                            } else if ($registDateFlag === '1' && $this->_userContentList[$contentId]['serviceStartDate'] !== $registDate) {

                                //-----------------------------------------------
                                // ユーザーコンテンツ更新
                                //-----------------------------------------------
                                $db = null;
                                $db = new Mikoshiva_Db_Simple();

                                $where = array();
                                $where['userContentId'] = $this->_userContentList[$contentId]['userContentId'];
                                $where['serviceStartDate'] = $registDate;

                                $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                                $userContentData[$result->getContentId()] = $result->getUserContentId();
                                $this->_set_result($contentId,$result);

                                $output['all'][] = $count . '行目,ユーザーコンテンツサービス開始日更新,contentId:' . $contentId;

                            } else {
                                //$output['all'][] = $count . '行目,すでに権限剥奪済,contentId:' . $contentId;
                            }

                        } else {
                            //-----------------------------------------------
                            // ユーザーコンテンツ登録
                            //-----------------------------------------------
                            $db = null;
                            $db = new Mikoshiva_Db_Simple();

                            $where = array();
                            $where['userId'] = $userId;
                            $where['contentId'] = $contentId;
                            $where['serviceStartDate'] = $registDate;
                            $where['serviceEndDate'] = $registEndDate;
                            $where['authFlag'] = '0';
                            $where['deleteFlag'] = '0';
                            $where['deletionDatetime'] = null;

                            $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                            $userContentData[$result->getContentId()] = $result->getUserContentId();
                            $this->_set_result($contentId,$result);

                            $output['all'][] = $count . '行目,ユーザーコンテンツ権限剥奪登録,contentId:' . $contentId;
                        }
                    }

                    //-----------------------------------------------
                    // ブランク期間登録
                    //-----------------------------------------------
                    $userContentBlankIds = array();
                    $tempBlankData = array();
                    if (isset($userContentBlankList[$userContentData[$contentId]])) {
                        $tempBlankData = $userContentBlankList[$userContentData[$contentId]];
                        foreach ($userContentBlankList[$userContentData[$contentId]] as $k => $v) {
                            $userContentBlankIds[$k] = $v['mub_user_content_blank_id'];
                        }
                    }

                    foreach ($registBlankDateList as $registBlankDateListNo => $registBlankDateListRow) {
                        if (count($registBlankDateListRow) === 2) {
                            //-----------------------------------------------
                            // ユーザーコンテンツブランク登録
                            //-----------------------------------------------
                            $db = null;
                            $db = new Mikoshiva_Db_Simple();

                            $where = array();
                            foreach ($tempBlankData as $k => $v) {
                                if ($v['mub_start_date'] === $registBlankDateListRow[0] && $v['mub_end_date'] === $registBlankDateListRow[1]) {
                                    $where['userContentBlankId'] = $userContentBlankIds[$k];
                                    unset($userContentBlankIds[$k]);
                                    unset($tempBlankData[$k]);
                                    break;
                                }
                            }
                            if (isset($userContentBlankIds[$registBlankDateListNo])) {
                                if (!isset($where['userContentBlankId'])) {
                                    $where['userContentBlankId'] = $userContentBlankIds[$registBlankDateListNo];
                                    unset($userContentBlankIds[$registBlankDateListNo]);
                                    unset($tempBlankData[$registBlankDateListNo]);
                                }
                            }
                            $where['userContentId'] = $userContentData[$contentId];
                            $where['startDate'] = $registBlankDateListRow[0];
                            $where['endDate'] = $registBlankDateListRow[1];
                            $where['deleteFlag'] = '0';
                            $where['deletionDatetime'] = null;

                            $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContentBlank', $where);

                            $output['all'][] = $count . '行目,ユーザーコンテンツ空白期間登録,contentId:' . $contentId;
                        }
                    }

                    foreach ($userContentBlankIds as $userContentBlankIdsNo => $userContentBlankIdsRow) {
                        //-----------------------------------------------
                        // ユーザーコンテンツブランク削除
                        //-----------------------------------------------
                        $db = null;
                        $db = new Mikoshiva_Db_Simple();

                        $where = array();
                        $where['userContentBlankId'] = $userContentBlankIdsRow;
                        $where['deleteFlag'] = '1';
                        $where['deletionDatetime'] = $nowDatetime;

                        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContentBlank', $where);

                        $output['all'][] = $count . '行目,ユーザーコンテンツ空白期間削除,contentId:' . $contentId;
                    }

                } else {
                    $output['error'][] = $count . '行目,コンテンツID異常,contentId:' . $contentId;
                }
            }


            foreach ($freeContentIds as $k => $contentId) {

                if (!isset($this->_userContentList[$contentId])) {

                    //-----------------------------------------------
                    // ユーザーコンテンツ登録
                    //-----------------------------------------------
                    $db = null;
                    $db = new Mikoshiva_Db_Simple();

                    $where = array();
                    $where['userId'] = $userId;
                    $where['contentId'] = $contentId;
                    $where['serviceStartDate'] = $nowDate;
                    $where['authFlag'] = '1';

                    $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                    $this->_set_result($contentId,$result);

                    $output['all'][] = $count . '行目,ユーザーコンテンツ登録,contentId:' . $contentId;

                } else {

                    if ($this->_userContentList[$contentId]['authFlag'] !== '1' || $this->_userContentList[$contentId]['deleteFlag'] === '1') {

                        //-----------------------------------------------
                        // ユーザーコンテンツ更新
                        //-----------------------------------------------
                        $db = null;
                        $db = new Mikoshiva_Db_Simple();

                        $where = array();
                        $where['userContentId'] = $this->_userContentList[$contentId]['userContentId'];
                        $where['authFlag'] = '1';
                        $where['deleteFlag'] = '0';
                        $where['deletionDatetime'] = null;

                        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                        $this->_set_result($contentId,$result);

                        $output['all'][] = $count . '行目,ユーザーコンテンツ権限剥奪解除更新,contentId:' . $contentId;
                    } else {
                        // $output['all'][] = $count . '行目,すでに登録済,contentId:' . $contentId;
                    }
                }
                if (isset($userContentDeleteList[$contentId])) {
                    unset($userContentDeleteList[$contentId]);
                }
            }

            if ($deleteFlag == 1) {
                foreach ($userContentDeleteList as $contentId => $userContentId) {
                    if (!isBlankOrNull($userContentId)) {
                        if (!isset($userContentDeleteAlreadyList[$contentId]) || count($userContentDeleteAlreadyList[$contentId]) === 0) {
                            //-----------------------------------------------
                            // ユーザーコンテンツ削除
                            //-----------------------------------------------
                            $db = null;
                            $db = new Mikoshiva_Db_Simple();

                            $where = array();
                            $where['userContentId'] = $userContentId;
                            $where['authFlag'] = '0';
                            $where['deleteFlag'] = '1';
                            $where['deletionDatetime'] = $nowDatetime;

                            $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);

                            $output['all'][] = $count . '行目,ユーザーコンテンツ削除更新,contentId:' . $contentId;
                        }
                    }
                }
            }
        }

        $output['all'] = trim(implode("\r\n",$output['all']));
        $output['error'] = trim(implode("\r\n",$output['error']));

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }

    private function _set_result($contentId,$result) {
        $this->_userContentList[$contentId]['userContentId'] = $result->getUserContentId();
        $this->_userContentList[$contentId]['deleteFlag'] = $result->getDeleteFlag();
        $this->_userContentList[$contentId]['authFlag'] = $result->getAuthFlag();
        $this->_userContentList[$contentId]['serviceStartDate'] = $result->getServiceStartDate();
        $this->_userContentList[$contentId]['serviceEndDate'] = $result->getServiceEndDate();
    }
}
