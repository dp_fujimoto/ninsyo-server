<?php
/** @package Mikoshiva_Settlement */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';
require_once 'Mikoshiva/Utility/Copy.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Card/Save.php';

/**
 * Mikoshiva_Settlement_Gmo_Exec_Alter_Abstract
 *
 * @package Mikoshiva_Settlement
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Settlement_Execution_Gmo_Card_Save_Abstract extends Mikoshiva_Settlement_Execution_Gmo_Abstract {


    /**
     * カード登録連番
     * @var string カード登録連番
     */
    protected $_cardSeq;


    /**
     * 初期化処理
     *
     * @param Popo_Abstract $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save */
        $inputPopo;


        // コンフィグからサイト ID とサイトパスを取得
        $siteId   = $gmoConfig['SITE_ID'];
        $sitePass = $gmoConfig['SITE_PASS'];


        // サイト ID とサイトパスをPOPO に詰め直す
        $inputPopo->setSiteID($siteId);
        $inputPopo->setSitePass($sitePass);
        $inputPopo->setCardSeq($this->_cardSeq);
        $inputPopo->setDefaultFlag('1');


        // GMO 処理結果 POPO クラス
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['SAVE_CARD_PGM'];

        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }




    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Exec/Mikoshiva_Settlement_Gmo_Exec_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/04
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Exception.php';


        /* @var $_inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save */
        $_inputPopo = $inputPopo;


        // サイト ID
        if(isBlankOrNull($_inputPopo->getSiteID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイト ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        // サイトパスワード
        if(isBlankOrNull($_inputPopo->getSitePass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイトパスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        // 会員 ID
        if(isBlankOrNull($_inputPopo->getMemberID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】会員 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        // カード番号
        if(isBlankOrNull($_inputPopo->getCardNo())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】クレジットカード番号 は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        // 有効期限
        if(isBlankOrNull($_inputPopo->getExpire())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】クレジットカードの有効期限は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        return true;
    }
}












