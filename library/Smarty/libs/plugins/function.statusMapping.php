<?php
/** @package Smarty */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Mapping.php';
require_once 'Mikoshiva/Utility/Exception.php';

/**
 * コードから、その意味を取得して返します。
 *
 * @param  array   $params
 *                       string code     コード
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.statusMapping.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_statusMapping($params, &$smarty) {

    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default               = array();
    $default['code']     = null;
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['code']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 status に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------

    $result = Mikoshiva_Utility_Mapping::statusMapping($data['code']);
    Mikoshiva_Logger::info(LOG_END);
    return $result;

}


?>
