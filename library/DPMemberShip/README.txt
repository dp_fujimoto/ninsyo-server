━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
ダイナミック出版様向け 会員情報出力およびログイン認証機構
date:2011/01/04
―――――――――――――――――――――――――――――――――――――――

目次
１．構成情報
２．インストール
３．各クラスのインターフェース

――――――――――――――――――――――――――――――――――――――――
１．構成情報
--------------------------------------------------------------------------------
１．ファイル一覧
/
├ DPMemberShip/
｜  ├ DPAuthzMerge.php ・・・・・CSVマージ機構クラス
｜  ├ DPCheckSession.php ・・・・セッション管理組み込み用処理
｜  ├ DPConfig.php ・・・・・・・全体の設定ファイル
｜  ├ DPLogin.php・・・・・・・・ログイン認証用クラス
｜  ├ DPMemberShip.php ・・・・・バッチから起動されるCSV出力クラス
｜  └ DPSession.php・・・・・・・セッション管理、権限管理クラス
├ htdocs/
｜  ├ login.php・・・・・・・・・ログインページ
｜  ├ menu.php ・・・・・・・・・メニューページ
｜  ├ mltYYYYMM.php・・・・・・・プロダクトコード別年月別ページ
｜  ├ mscYYYYMM.php・・・・・・・プロダクトコード別年月別ページ
｜  └ merge.php  ・・・・・・・・サーバ側CSVマージ機能
├ server/
｜  ├ authz/・・・・・・・・・・認証用csv置き場
｜  ｜  ├ プロダクトコードYYYYMM.csv
｜  ｜  └ プロダクトコードYYYYMM.csv
｜  └ save/・・・・・・・・・・・サーバ側保存用
｜      ├ プロダクトコードYYYYMM.csvYYYYMMDD_hhmmss_u_pid
｜      └ プロダクトコードYYYYMM.csvYYYYMMDD_hhmmss_u_pid
└ t/ ・・・・・・・・・・・・・・テスト用スクリプト

――――――――――――――――――――――――――――――――――――――――
２．インストール
--------------------------------------------------------------------------------
    バッチ用、サーバ用ともに同じ手順です。ポイントはinclude_pathへ追記することです。
	① 前記、ファイル一覧のDPMemberShipフォルダをphp.iniのInclude_pathへ追加する。
	② htdocsフォルダの中身をHTTPD配下へ配置する。
       ＊注意：サーバ側のみ必要。
       ＊注意：フォルダを別にする場合などはDPConfig.phpでURLを調整すること。
    ③ DPConfig.phpを調整する。

――――――――――――――――――――――――――――――――――――――――
３．各クラスのインターフェース
--------------------------------------------------------------------------------
１．基幹システムのバッチ処理より呼び出されるCSV出力クラス（PHP）

  クラス名：DPMemberShip.php
  ￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
  利用方法：
        $member = new MemberShip();
    	// ユーザの追加
        $member->add('メールアドレス','プロダクトコード');
        // サーバへの転送
        $bool = $member->dp_server_merge();
		エラー時は、Exceptionをthrowします。
  メソッド：
        １．void add($email, $product_cd, $date)
            第一引数：メールアドレス
            第二引数：プロダクトコード
            第三引数：運用年月日（yyyymmdd）
            １件づつaddメソッドを利用してローカルディスクの
            CSVファイルに追記する。
            作成するファイルは、クラスをインスタンス化した単位で
            存在します。
            バックアップも兼ねてデータはそのまま残しています。
            ex: MLT.20110104_102311_1234
                プロダクトコードYYYYMMDD_YYYYMMDD_hhmmss_u_プロセスID
            格納するCSVファイルはプロダクトコード別運用年月別で保存される。
			＊運用年月は設定値AUTHZ_START_DAYを基準に、運用年月日の日が
			　この設定値より小さい場合は、運用年月日の先月の認証情報
			　として扱う
        ２．bool dp_server_merge()
            設定値AUTH_FILE_DIR配下にあるaddメソッドで作成した
            CSVファイルをHTTPで転送します。
			転送先は、設定値「MERGE_URL_商品コード」として指定。
            複数プロダクト別CSVファイルが存在した場合は、それ毎
            に転送します。この時、何らかのエラーが発生した場合は
            Exceptionが発生し、後に続くファイルは転送されません。
            出来なかったファイルは手動でサーバサイドへ転送してください。
            既存のメールアドレスとのマージは、後述のDPAuthzMerge.phpが転送
            先のサーバ側で実施します。
			
  設定値：
        AUTH_FILE_DIR       ：バッチ側のCSVファイル格納パス
        AUTH_FILE_SERVER_DIR：サーバ側のCSVファイル格納パス
		AUTHZ_START_DAY     ：認証月開始日
        MERGE_URL_***       ：商品コード別サーバ側マージ用PHPのURL


  クラス名：DPAuthzMerge.php
  ￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
  利用方法：
        require_once('DPAuthzMerge.php');
	
        $key   = $_REQUEST['key'];
        $pcode = $_REQUEST['product_cd'];
        $data  = $_REQUEST['data'];
        $authz = new DPAuthzMerge($key,$product_cd);
        print $authz->merge($data);
  メソッド：
        １．void DPAuthzMerge($key,$pcode,$yyyymm);
            第一引数：転送認証キー（設定値MERGE_KEY）
            第二引数：プロダクトコード
            第三引数：運用年月（YYYYMM)
            
        ２．String merge($data)
            第一引数：追加するメールアドレスの一覧
            本機能は、DPMemberShipクラスからHTTP経由で起動されることを前提とした
            内部処理クラスである。
            引数で指定されたプロダクトコード別CSVファイルに第三引数の内容をマージ
            する。
            メールアドレスの重複をチェックし、マージする。既存CSVファイルとマージし
            一時ファイルを作成したあとで上書きする。
            この為、同時実行した場合は後から実行した内容で上書きされる。
            マージ対象ファイルはそのファイル名からプロダクトコードと運用年月を取得し
            コンストラクタで指定されたプロダクトコード、運用年月と同じファイルが対象
            となる。
            また、マージ後転送されてきたデータを保存用として、設定値AUTH_FILE_SERVER_SAVE_DIR
            に保存する。
  設定値：
        AUTH_FILE_DIR            ：バッチ側のCSVファイル格納パス
        AUTH_FILE_SERVER_DIR     ：サーバ側のCSVファイル格納パス
        AUTH_FILE_SERVER_SAVE_DIR：サーバ側のCSVファイル保存パス
        URL_MERGE           ：サーバ側マージ用PHPのURL
  利用例：
        htdocs/merge.php

--------------------------------------------------------------------------------
２．ログイン認証機構クラス

  クラス名：DPLogin.php
  ￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
  利用方法：
	       require_once('DPLogin.php');
	       $dpl = new DPLogin();
	       $message = $dpl->auth();
  メソッド：
        １．String auth()
            リクエストパラ―メータ（action=login）が含まれる場合に
            ログイン認証を行う。
            emailの存在確認は行っていない。
            設定値LOGIN_KEYとリクエストパラメータpasswdを照合し
            一致しない場合は、設定値MSG_LOGIN_FAILを返して終了
            一致した場合は、設定値URL_MENUへリダイレクトする            
  設定値：
        LOGIN_KEY       ：ユーザ入力のパスワードと照合する共有キー
        MSG_LOGIN_FAIL  ：認証エラーの場合に戻るエラーメッセージ
        URL_MENU        ：認証後のメニュー画面のURL
  利用例：
        htdocs/login.php

--------------------------------------------------------------------------------
３．セッション管理/権限管理機構クラス

  クラス名：DPSession.php
  ￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
  利用方法：
         * メニューページの場合
	     require_once('DPSession.php');
	     $dps = new DPSession();
	     $dps->check_session();
         * プロダクトコートMLTのページの場合
	     require_once('DPSession.php');
	     $dps = new DPSession();
	     $dps->check_session();
	     $dps->check_authz('MLT');
  メソッド：
        １．void check_session()
            セッション管理機構
            全画面のログイン処理にて保存したメールアドレスが存在しているかを
            検査し、存在しない場合はログイン画面（設定値URL_LOGIN）へメッセージ
            (設定値MSG_SESSION_FAIL)付きでリダイレクトする。

        ２．void check_authz($product_cd)
            権限管理機構
            第一引数：プロダクトコード
            引数に応じて、設定値AUTH_FILE_SERVER_DIR配下に存在するCSVファイル
            に、セッションに保存されたメールアドレスが存在するか検査する。
            検査し、存在しない場合はログイン画面（設定値URL_LOGIN）へメッセージ
            (設定値MSG_AUTHZ_FAIL)付きでリダイレクトする。
                      
  設定値：
        AUTH_FILE_SERVER_DIR ：権限検査を行う、プロダクトコード別CSVファイルの格納パス
        MSG_AUTHZ_FAIL       ：認証エラーの場合に戻るエラーメッセージ
        MSG_SESSION_FAIL     ：セッションエラーの場合に戻るエラーメッセージ
		URL_SESSION_ERROR    ：セッションチェック、認証チェックにおいてエラーとなった
		　　　　　　　　　　　　場合の転送先。エラー画面
  利用例：
        htdocs/menu.php
        htdocs/mlt.php
        htdocs/msc.php

  クラス名：DPCheckSession.php
  ￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
  利用方法：
	     require_once('DPCheckSession.php');
  メソッド：
         存在しない。内部的にはDPSessionクラスを呼び出している。
         リクエストURLからリクエストされたファイル名（ex：mlt201101.php）を特定し
         プロダクトコードおよび運用年月を抽出する。これを持って、認証、権限チェック
         を行う。
                      
  設定値：
        AUTH_FILE_SERVER_DIR ：権限検査を行う、プロダクトコード別CSVファイルの格納パス
        MSG_AUTHZ_FAIL       ：認証エラーの場合に戻るエラーメッセージ
        MSG_SESSION_FAIL     ：セッションエラーの場合に戻るエラーメッセージ
  利用例：
        htdocs/menu.php
        htdocs/mlt201101.php...
        htdocs/msc201101.php...