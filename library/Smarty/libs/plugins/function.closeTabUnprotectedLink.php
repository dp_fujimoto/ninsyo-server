<?php
require_once 'Mikoshiva/View/Helper/Exception.php';

//function.closeTabUnprotected.phpが必須
 /**
 * 保護されたタブ以外の全てのタブを閉じるjavascriptをリンク(aタグ)に実装した形で返します。
 *
 * @param  array   $params
 *                       string name   この文字列を&lt;a&gt;～&lt;/a&gt;で囲って出力
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.closeTabUnprotectedLink.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_closeTabUnprotectedLink($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default            = array();
    $default['name']    = '';
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once SMARTY_PLUGINS_DIR . 'function.closeTabUnprotected.php';

    $script = '';
    $script = smarty_function_closeTabUnprotected($params, $smarty);

    Mikoshiva_Logger::info(LOG_END);
    return '<a href="#" onclick="' . $script . '">' . $data['name'] . '</a>';
}


?>
