<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Date - 指定日時を取得する</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="指定日時を取得する">指定日時を取得する</h1>

            <p>指定日時を取得する</p>



            <!-- ========================================
              ■N 年後・N 年前
            ==========================================-->
            <h2 id="N 年後・N 年前">N 年後・N 年前</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// １年後を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->addYear(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2011-03-09


// １年前を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->addYear(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2009-03-09

');
?>
            </pre>



            <!-- ========================================
              ■N 月後・N 月前
            ==========================================-->
            <h2 id="N 月後・N 月前">N 月後・N 月前</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// １月後を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->addMonth(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-04-09


// １月前を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->subMonth(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-02-09

');
?>
            </pre>



            <!-- ========================================
              ■N 日後・N 日前
            ==========================================-->
            <h2 id="N 日後・N 日前">N 日後・N 日前</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// １日後を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->addDay(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10


// １日前を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->subDay(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-08

');
?>
            </pre>



            <!-- ========================================
              ■N 時間後・N 時間前
            ==========================================-->
            <h2 id="N 時間後・N 時間前">N 時間後・N 時間前</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// １時間後を取得する（今日が 2010/03/09 11:12:13 である場合）
$date = new Mikoshiva_Date();
echo $date->addHour(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10 12:12:13


// １時間前を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->subHour(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10 10:12:13

');
?>
            </pre>



            <!-- ========================================
              ■N 分後・N 分前
            ==========================================-->
            <h2 id="N 分後・N 分前">N 分後・N 分前</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// １分後を取得する（今日が 2010/03/09 11:12:13 である場合）
$date = new Mikoshiva_Date();
echo $date->addMinute(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10 11:13:13


// １分前を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->subMinute(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10 11:11:13

');
?>
            </pre>



            <!-- ========================================
              ■N 秒後・N 秒前
            ==========================================-->
            <h2 id="N 秒後・N 秒前">N 秒後・N 秒前</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// １秒後を取得する（今日が 2010/03/09 11:12:13 である場合）
$date = new Mikoshiva_Date();
echo $date->addSecond(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10 11:12:14


// １秒前を取得する（今日が 2010/03/09 である場合）
$date = new Mikoshiva_Date();
echo $date->subSecond(1)->toString(\'yyyy-MM-dd HH:mm:ss\'); // 2010-03-10 11:12:12

');
?>
            </pre>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















