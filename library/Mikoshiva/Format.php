<?php
/**
 * @package Mikoshiva_Format
 */

/**
 *
 * @package Mikoshiva_Format
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Format.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Format
{
	/**
	 * 配列をCSV形式にフォーマットします
	 *
	 * @param array $data
	 * @return string
	 */
	public static function arrayToCsvFormatter( array $data )
	{
		return self::_encodeFormatter('Csv', $data );
	}

	/**
	 * 配列をTSV形式にフォーマットします
	 *
	 * @param unknown_type $data
	 * @return unknown_type
	 */
	public static function arrayToTsvFormatter( array $data )
	{
		return self::_encodeFormatter('Tsv', $data );
	}

	/**
	 * CSV 形式のファイルを配列に変換します
	 *
	 * @param string $data
	 * @return array
	 */
	public static function csvToArray( $data )
	{
		return self::_decodeFormatter('Csv', $data );
	}

	/**
	 * TSV 形式のファイルを配列に変換します
	 *
	 * @param string $data
	 * @return array
	 */
	public static function tsvToArray( $data )
	{
		return self::_decodeFormatter('Tsv', $data );
	}

	/**
	 * 処理を実行します
	 *
	 * @param $format
	 * @param $data
	 * @return string
	 */
	private static function _encodeFormatter( $format, $data )
	{
		// クラス名
		$className = 'Mikoshiva_Format_Encode_' . $format;

		// クラスファイル
		$filePath  = 'Mikoshiva/Format/Encode/' . $format . '.php';

		// クラスの読み込み
		include_once $filePath;

		// クラスのチェック
		// クラスファイルが存在しなければ例外を投げる
		if ( ! class_exists( $className ) ) {
			include_once 'Mikoshiva/Format/Exception.php';
			throw new Mikoshiva_Format_Exception( $className . ' クラスが存在しません。' );
		}



		// 実行
		/* @var $class Mikoshiva_Format_Abstract */
		$class = new $className();
		return $class->process( $data );
	}

	/**
	 * 処理を実行します
	 *
	 * @param $format
	 * @param $data
	 * @return string
	 */
	private static function _decodeFormatter( $format, $data )
	{
		// クラス名
		$className = 'Mikoshiva_Format_Decode_' . $format;

		// クラスファイル
		$filePath  = 'Mikoshiva/Format/Decode/' . $format . '.php';

		// クラスの読み込み
		include_once $filePath;

		// クラスのチェック
		// クラスファイルが存在しなければ例外を投げる
		if ( ! class_exists( $className ) ) {
			include_once 'Mikoshiva/Format/Exception.php';
			throw new Mikoshiva_Format_Exception( $className . ' クラスが存在しません。' );
		}

		// 実行
		/* @var $class Mikoshiva_Format_Abstract */
		$class = new $className();
		return $class->process( $data );
	}
}