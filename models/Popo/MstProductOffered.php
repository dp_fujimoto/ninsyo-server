<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product_offered
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProductOffered extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mpo';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productOfferedId' => null,
        'productId' => null,
        'productOfferedName' => null,
        'productOfferedType' => null,
        'offeredNumber' => null,
        'offeredStartDate' => null,
        'offeredEndDate' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mpo_product_offered_id の値を返します
	 *
     * @return int mpo_product_offered_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedId() {
		return $this->_properties['productOfferedId'];
    }


    /**
     * mpo_product_offered_id の値をセットします
	 *
	 * @param int $productOfferedId mpo_product_offered_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedId($productOfferedId) {
		$this->_properties['productOfferedId'] = $productOfferedId;
    }


    /**
     * mpo_product_id の値を返します
	 *
     * @return int mpo_product_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * mpo_product_id の値をセットします
	 *
	 * @param int $productId mpo_product_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * mpo_product_offered_name の値を返します
	 *
     * @return varchar mpo_product_offered_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedName() {
		return $this->_properties['productOfferedName'];
    }


    /**
     * mpo_product_offered_name の値をセットします
	 *
	 * @param varchar $productOfferedName mpo_product_offered_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedName($productOfferedName) {
		$this->_properties['productOfferedName'] = $productOfferedName;
    }


    /**
     * mpo_product_offered_type の値を返します
	 *
     * @return varchar mpo_product_offered_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedType() {
		return $this->_properties['productOfferedType'];
    }


    /**
     * mpo_product_offered_type の値をセットします
	 *
	 * @param varchar $productOfferedType mpo_product_offered_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedType($productOfferedType) {
		$this->_properties['productOfferedType'] = $productOfferedType;
    }


    /**
     * mpo_offered_number の値を返します
	 *
     * @return int mpo_offered_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOfferedNumber() {
		return $this->_properties['offeredNumber'];
    }


    /**
     * mpo_offered_number の値をセットします
	 *
	 * @param int $offeredNumber mpo_offered_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOfferedNumber($offeredNumber) {
		$this->_properties['offeredNumber'] = $offeredNumber;
    }


    /**
     * mpo_offered_start_date の値を返します
	 *
     * @return date mpo_offered_start_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOfferedStartDate() {
		return $this->_properties['offeredStartDate'];
    }


    /**
     * mpo_offered_start_date の値をセットします
	 *
	 * @param date $offeredStartDate mpo_offered_start_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOfferedStartDate($offeredStartDate) {
		$this->_properties['offeredStartDate'] = $offeredStartDate;
    }


    /**
     * mpo_offered_end_date の値を返します
	 *
     * @return date mpo_offered_end_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOfferedEndDate() {
		return $this->_properties['offeredEndDate'];
    }


    /**
     * mpo_offered_end_date の値をセットします
	 *
	 * @param date $offeredEndDate mpo_offered_end_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOfferedEndDate($offeredEndDate) {
		$this->_properties['offeredEndDate'] = $offeredEndDate;
    }


    /**
     * mpo_memo の値を返します
	 *
     * @return text mpo_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * mpo_memo の値をセットします
	 *
	 * @param text $memo mpo_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * mpo_delete_flag の値を返します
	 *
     * @return char mpo_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mpo_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mpo_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mpo_deletion_datetime の値を返します
	 *
     * @return datetime mpo_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mpo_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mpo_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mpo_update_datetime の値を返します
	 *
     * @return datetime mpo_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mpo_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mpo_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mpo_registration_datetime の値を返します
	 *
     * @return datetime mpo_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mpo_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mpo_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mpo_update_timestamp の値を返します
	 *
     * @return timestamp mpo_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mpo_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mpo_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























