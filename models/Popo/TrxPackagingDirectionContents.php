<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_packaging_direction_contents
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxPackagingDirectionContents extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tpc';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'packagingDirectionContentsId' => null,
        'packagingDirectionId' => null,
        'shippingType' => null,
        'directionCode' => null,
        'packageNumber' => null,
        'shippingGroupLabel' => null,
        'shippingAssign' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tpc_packaging_direction_contents_id の値を返します
	 *
     * @return int tpc_packaging_direction_contents_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionContentsId() {
		return $this->_properties['packagingDirectionContentsId'];
    }


    /**
     * tpc_packaging_direction_contents_id の値をセットします
	 *
	 * @param int $packagingDirectionContentsId tpc_packaging_direction_contents_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionContentsId($packagingDirectionContentsId) {
		$this->_properties['packagingDirectionContentsId'] = $packagingDirectionContentsId;
    }


    /**
     * tpc_packaging_direction_id の値を返します
	 *
     * @return int tpc_packaging_direction_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionId() {
		return $this->_properties['packagingDirectionId'];
    }


    /**
     * tpc_packaging_direction_id の値をセットします
	 *
	 * @param int $packagingDirectionId tpc_packaging_direction_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionId($packagingDirectionId) {
		$this->_properties['packagingDirectionId'] = $packagingDirectionId;
    }


    /**
     * tpc_shipping_type の値を返します
	 *
     * @return varchar tpc_shipping_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingType() {
		return $this->_properties['shippingType'];
    }


    /**
     * tpc_shipping_type の値をセットします
	 *
	 * @param varchar $shippingType tpc_shipping_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingType($shippingType) {
		$this->_properties['shippingType'] = $shippingType;
    }


    /**
     * tpc_direction_code の値を返します
	 *
     * @return varchar tpc_direction_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionCode() {
		return $this->_properties['directionCode'];
    }


    /**
     * tpc_direction_code の値をセットします
	 *
	 * @param varchar $directionCode tpc_direction_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionCode($directionCode) {
		$this->_properties['directionCode'] = $directionCode;
    }


    /**
     * tpc_package_number の値を返します
	 *
     * @return int tpc_package_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageNumber() {
		return $this->_properties['packageNumber'];
    }


    /**
     * tpc_package_number の値をセットします
	 *
	 * @param int $packageNumber tpc_package_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageNumber($packageNumber) {
		$this->_properties['packageNumber'] = $packageNumber;
    }


    /**
     * tpc_shipping_group_label の値を返します
	 *
     * @return varchar tpc_shipping_group_label
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingGroupLabel() {
		return $this->_properties['shippingGroupLabel'];
    }


    /**
     * tpc_shipping_group_label の値をセットします
	 *
	 * @param varchar $shippingGroupLabel tpc_shipping_group_label
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingGroupLabel($shippingGroupLabel) {
		$this->_properties['shippingGroupLabel'] = $shippingGroupLabel;
    }


    /**
     * tpc_shipping_assign の値を返します
	 *
     * @return varchar tpc_shipping_assign
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAssign() {
		return $this->_properties['shippingAssign'];
    }


    /**
     * tpc_shipping_assign の値をセットします
	 *
	 * @param varchar $shippingAssign tpc_shipping_assign
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAssign($shippingAssign) {
		$this->_properties['shippingAssign'] = $shippingAssign;
    }


    /**
     * tpc_delete_flag の値を返します
	 *
     * @return char tpc_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tpc_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tpc_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tpc_deletion_datetime の値を返します
	 *
     * @return datetime tpc_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tpc_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tpc_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tpc_registration_datetime の値を返します
	 *
     * @return datetime tpc_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tpc_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tpc_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tpc_update_datetime の値を返します
	 *
     * @return datetime tpc_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tpc_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tpc_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tpc_update_timestamp の値を返します
	 *
     * @return timestamp tpc_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tpc_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tpc_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























