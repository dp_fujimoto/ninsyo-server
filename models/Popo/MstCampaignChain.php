<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_campaign_chain
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCampaignChain extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mcc';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'campaignChainId' => null,
        'divisionId' => null,
        'campaignChainCode' => null,
        'campaignId' => null,
        'transitionPattern' => null,
        'transactionPosition' => null,
        'campaignType' => null,
        'optionEssentialFlag' => null,
        'essentialCampaignProductId' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * キャンペーンチェインID の値を返します
	 *
     * @return int キャンペーンチェインID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignChainId() {
		return $this->_properties['campaignChainId'];
    }


    /**
     * キャンペーンチェインID の値をセットします
	 *
	 * @param int $campaignChainId キャンペーンチェインID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignChainId($campaignChainId) {
		$this->_properties['campaignChainId'] = $campaignChainId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * キャンペーンチェインコード の値を返します
	 *
     * @return char キャンペーンチェインコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignChainCode() {
		return $this->_properties['campaignChainCode'];
    }


    /**
     * キャンペーンチェインコード の値をセットします
	 *
	 * @param char $campaignChainCode キャンペーンチェインコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignChainCode($campaignChainCode) {
		$this->_properties['campaignChainCode'] = $campaignChainCode;
    }


    /**
     * キャンペーンID の値を返します
	 *
     * @return int キャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignId() {
		return $this->_properties['campaignId'];
    }


    /**
     * キャンペーンID の値をセットします
	 *
	 * @param int $campaignId キャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignId($campaignId) {
		$this->_properties['campaignId'] = $campaignId;
    }


    /**
     * 遷移パターン の値を返します
	 *
     * @return varchar 遷移パターン
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransitionPattern() {
		return $this->_properties['transitionPattern'];
    }


    /**
     * 遷移パターン の値をセットします
	 *
	 * @param varchar $transitionPattern 遷移パターン
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransitionPattern($transitionPattern) {
		$this->_properties['transitionPattern'] = $transitionPattern;
    }


    /**
     * セールストランザクションの位置情報 の値を返します
	 *
     * @return int セールストランザクションの位置情報
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransactionPosition() {
		return $this->_properties['transactionPosition'];
    }


    /**
     * セールストランザクションの位置情報 の値をセットします
	 *
	 * @param int $transactionPosition セールストランザクションの位置情報
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransactionPosition($transactionPosition) {
		$this->_properties['transactionPosition'] = $transactionPosition;
    }


    /**
     * キャンペーンタイプ の値を返します
	 *
     * @return varchar キャンペーンタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignType() {
		return $this->_properties['campaignType'];
    }


    /**
     * キャンペーンタイプ の値をセットします
	 *
	 * @param varchar $campaignType キャンペーンタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignType($campaignType) {
		$this->_properties['campaignType'] = $campaignType;
    }


    /**
     * オプション必須フラグ の値を返します
	 *
     * @return char オプション必須フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOptionEssentialFlag() {
		return $this->_properties['optionEssentialFlag'];
    }


    /**
     * オプション必須フラグ の値をセットします
	 *
	 * @param char $optionEssentialFlag オプション必須フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOptionEssentialFlag($optionEssentialFlag) {
		$this->_properties['optionEssentialFlag'] = $optionEssentialFlag;
    }


    /**
     * 必須キャンペーン商品ID の値を返します
	 *
     * @return int 必須キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEssentialCampaignProductId() {
		return $this->_properties['essentialCampaignProductId'];
    }


    /**
     * 必須キャンペーン商品ID の値をセットします
	 *
	 * @param int $essentialCampaignProductId 必須キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEssentialCampaignProductId($essentialCampaignProductId) {
		$this->_properties['essentialCampaignProductId'] = $essentialCampaignProductId;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























