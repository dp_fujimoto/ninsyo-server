<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


 /**
 * 外部ドメインからMikoshivaに、Ajaxでアクセスしている場合、Mikoshiva外に出るときは必ずこれで移動します。
 *
 * @param  array   $params
 *                      string url                   アクション URL
 *                      string formId    null      | リクエスト時にpostするフォーム部品のID
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.leaveFromMikoshiva.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_leaveFromMikoshiva($params, &$smarty) {


    // ログ開始
    Mikoshiva_Logger::info(LOG_START);



    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['url']      = null;
    $default['formId']   = null;


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['url']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 url に値がありません。');
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $_url      = $data['url'];
    $_formId   = $data['formId'];

    Mikoshiva_Logger::info(LOG_END);
    return "leaveFromMikoshiva('{$_url}', '{$_formId}');";
}



