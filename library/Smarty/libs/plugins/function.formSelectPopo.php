<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


    /**
     * Popoの配列を元に、セレクトボックスを生成して返します。
     *
     * @param array $params
     *                      string       name                   フォームのname, id
     *                      string       selected    空文字   | selectedにする値
     *                      array        options                同種のpopoの配列
     *                      string       valueName              value に入力させたいプロパティ名
     *                      string|array labelName              ラベルに表示されたいプロパティ名またはそのリスト
     *                      string       separator   空文字   | ラベルが複数指定された時の区切り文字
     *                      array        attribs     array()  | selectタグに指定する追加の属性 '属性名' => 値
     *                      boolean      addEmpty    false    | リストに空白を追加(デフォルト：無効)
     *                      boolean      addAllItem  false    | リストに｢全て｣を追加(デフォルト：無効)
     *                      string       listsep     "<br>\n" | 各エンティティを区切る文字列
     * @param Smarty   $smarty
     * @param Template $template
     *
     * @return string                    &lt;select&gt;DBから取得した値によるリスト&lt;/select&gt;
     *
     * @package Smarty_libs
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/02
     * @version SVN:$Id: function.formSelectPopo.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
     */
function smarty_function_formSelectPopo($params, &$smarty, &$template) {
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['name']         = '';
    $default['selected']     = '';
    $default['options']      = array();
    $default['attribs']      = array();
    $default['addEmpty']     = false;
    $default['addAllItem']   = false;
    $default['listsep']      = "<br>\n";
    $default['valueName']   = '';
    $default['labelName']   = null;
    $default['separator']   = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------

    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }

    if(isBlankOrNull($data['valueName']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 valueName に値がありません。');
    }

    if(is_array($data['labelName'])) {
        if(count($data['labelName']) === 0) {
            throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 labelName に値がありません。');
        }
    } else if(isBlankOrNull($data['labelName']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 labelName に値がありません。');
    }

    if(!is_array($data['options'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 options は配列でなければなりません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }



    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $valueName = $data['valueName'];
    if(is_array($data['labelName'])) {
        $labelNames = $data['labelName'];
    } else {
        $labelNames = array($data['labelName']);
    }
    $separator = $data['separator'];
    $options   = $data['options'];

    $optionsFormated = array();//キー名がvalue、値がラベル名となる配列
    foreach($options as $popo) {
        $value = '';
        $label = '';
        $getValue = 'get' . ucfirst($valueName);
        $value = $popo->$getValue();

        $work = array();
        foreach($labelNames as $labelName) {
            $getLabel = 'get' . ucfirst($labelName);
            $work[]   = $popo->$getLabel();
        }
        $label = implode($separator, $work);
        $optionsFormated[$value] = $label;
    }

    $dataFormated = array();
    $dataFormated['name']       = $data['name'];
    $dataFormated['selected']   = $data['selected'];
    $dataFormated['options']    = $optionsFormated;
    $dataFormated['attribs']    = $data['attribs'];
    $dataFormated['addEmpty']   = $data['addEmpty'];
    $dataFormated['addAllItem'] = $data['addAllItem'];
    $dataFormated['listsep']    = $data['listsep'];
    require_once SMARTY_PLUGINS_DIR . 'function.formSelect.php';

    Mikoshiva_Logger::info(LOG_END);
    return smarty_function_formSelect($dataFormated, $smarty, $template);

}


?>
