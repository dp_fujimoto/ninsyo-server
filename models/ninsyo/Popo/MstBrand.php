<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_brand
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstBrand extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mbr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'brandId' => null,
        'divisionId' => null,
        'brandName' => null,
        'brandCode' => null,
        'logoUrl' => null,
        'logoUpdateDatetime' => null,
        'bannerVerticalUrl' => null,
        'bannerVerticalUpdateDatetime' => null,
        'bannerSidewaysUrl' => null,
        'bannerSidewaysUpdateDatetime' => null,
        'description' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mbr_brand_id の値を返します
	 *
     * @return int mbr_brand_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBrandId() {
		return $this->_properties['brandId'];
    }


    /**
     * mbr_brand_id の値をセットします
	 *
	 * @param int $brandId mbr_brand_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBrandId($brandId) {
		$this->_properties['brandId'] = $brandId;
    }


    /**
     * mbr_division_id の値を返します
	 *
     * @return int mbr_division_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * mbr_division_id の値をセットします
	 *
	 * @param int $divisionId mbr_division_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * mbr_brand_name の値を返します
	 *
     * @return varchar mbr_brand_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBrandName() {
		return $this->_properties['brandName'];
    }


    /**
     * mbr_brand_name の値をセットします
	 *
	 * @param varchar $brandName mbr_brand_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBrandName($brandName) {
		$this->_properties['brandName'] = $brandName;
    }


    /**
     * mbr_brand_code の値を返します
	 *
     * @return varchar mbr_brand_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBrandCode() {
		return $this->_properties['brandCode'];
    }


    /**
     * mbr_brand_code の値をセットします
	 *
	 * @param varchar $brandCode mbr_brand_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBrandCode($brandCode) {
		$this->_properties['brandCode'] = $brandCode;
    }


    /**
     * mbr_logo_url の値を返します
	 *
     * @return varchar mbr_logo_url
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLogoUrl() {
		return $this->_properties['logoUrl'];
    }


    /**
     * mbr_logo_url の値をセットします
	 *
	 * @param varchar $logoUrl mbr_logo_url
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLogoUrl($logoUrl) {
		$this->_properties['logoUrl'] = $logoUrl;
    }


    /**
     * mbr_logo_update_datetime の値を返します
	 *
     * @return datetime mbr_logo_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLogoUpdateDatetime() {
		return $this->_properties['logoUpdateDatetime'];
    }


    /**
     * mbr_logo_update_datetime の値をセットします
	 *
	 * @param datetime $logoUpdateDatetime mbr_logo_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLogoUpdateDatetime($logoUpdateDatetime) {
		$this->_properties['logoUpdateDatetime'] = $logoUpdateDatetime;
    }


    /**
     * mbr_banner_vertical_url の値を返します
	 *
     * @return varchar mbr_banner_vertical_url
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBannerVerticalUrl() {
		return $this->_properties['bannerVerticalUrl'];
    }


    /**
     * mbr_banner_vertical_url の値をセットします
	 *
	 * @param varchar $bannerVerticalUrl mbr_banner_vertical_url
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBannerVerticalUrl($bannerVerticalUrl) {
		$this->_properties['bannerVerticalUrl'] = $bannerVerticalUrl;
    }


    /**
     * mbr_banner_vertical_update_datetime の値を返します
	 *
     * @return datetime mbr_banner_vertical_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBannerVerticalUpdateDatetime() {
		return $this->_properties['bannerVerticalUpdateDatetime'];
    }


    /**
     * mbr_banner_vertical_update_datetime の値をセットします
	 *
	 * @param datetime $bannerVerticalUpdateDatetime mbr_banner_vertical_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBannerVerticalUpdateDatetime($bannerVerticalUpdateDatetime) {
		$this->_properties['bannerVerticalUpdateDatetime'] = $bannerVerticalUpdateDatetime;
    }


    /**
     * mbr_banner_sideways_url の値を返します
	 *
     * @return varchar mbr_banner_sideways_url
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBannerSidewaysUrl() {
		return $this->_properties['bannerSidewaysUrl'];
    }


    /**
     * mbr_banner_sideways_url の値をセットします
	 *
	 * @param varchar $bannerSidewaysUrl mbr_banner_sideways_url
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBannerSidewaysUrl($bannerSidewaysUrl) {
		$this->_properties['bannerSidewaysUrl'] = $bannerSidewaysUrl;
    }


    /**
     * mbr_banner_sideways_update_datetime の値を返します
	 *
     * @return datetime mbr_banner_sideways_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBannerSidewaysUpdateDatetime() {
		return $this->_properties['bannerSidewaysUpdateDatetime'];
    }


    /**
     * mbr_banner_sideways_update_datetime の値をセットします
	 *
	 * @param datetime $bannerSidewaysUpdateDatetime mbr_banner_sideways_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBannerSidewaysUpdateDatetime($bannerSidewaysUpdateDatetime) {
		$this->_properties['bannerSidewaysUpdateDatetime'] = $bannerSidewaysUpdateDatetime;
    }


    /**
     * mbr_description の値を返します
	 *
     * @return text mbr_description
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDescription() {
		return $this->_properties['description'];
    }


    /**
     * mbr_description の値をセットします
	 *
	 * @param text $description mbr_description
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDescription($description) {
		$this->_properties['description'] = $description;
    }


    /**
     * mbr_memo の値を返します
	 *
     * @return text mbr_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * mbr_memo の値をセットします
	 *
	 * @param text $memo mbr_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * mbr_delete_flag の値を返します
	 *
     * @return char mbr_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mbr_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mbr_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mbr_deletion_datetime の値を返します
	 *
     * @return datetime mbr_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mbr_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mbr_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mbr_registration_datetime の値を返します
	 *
     * @return datetime mbr_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mbr_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mbr_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mbr_update_datetime の値を返します
	 *
     * @return datetime mbr_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mbr_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mbr_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mbr_update_timestamp の値を返します
	 *
     * @return timestamp mbr_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mbr_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mbr_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























