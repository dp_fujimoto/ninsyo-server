<?php header("Content-type: text/html; charset=utf-8"); ?>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<title>タブの仕様</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"ＭＳ 明朝";
	panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
	{font-family:"ＭＳ ゴシック";
	panose-1:2 11 6 9 7 2 5 8 2 4;}
@font-face
	{font-family:Century;
	panose-1:2 4 6 4 5 5 5 2 3 4;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"MS UI Gothic";
	panose-1:2 11 6 0 7 2 5 8 2 4;}
@font-face
	{font-family:"\@ＭＳ ゴシック";
	panose-1:2 11 6 9 7 2 5 8 2 4;}
@font-face
	{font-family:"\@MS UI Gothic";
	panose-1:2 11 6 0 7 2 5 8 2 4;}
@font-face
	{font-family:"\@ＭＳ 明朝";
	panose-1:2 2 6 9 4 2 5 8 3 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:11.0pt;
	font-family:"Century","serif";}
h1
	{mso-style-link:"見出し 1 \(文字\)";
	margin-top:30.0pt;
	margin-right:0mm;
	margin-bottom:4.0pt;
	margin-left:0mm;
	border:none;
	padding:0mm;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:#365F91;}
h2
	{mso-style-link:"見出し 2 \(文字\)";
	margin-top:10.0pt;
	margin-right:0mm;
	margin-bottom:4.0pt;
	margin-left:0mm;
	border:none;
	padding:0mm;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:#365F91;
	font-weight:normal;}
h3
	{mso-style-link:"見出し 3 \(文字\)";
	margin-top:10.0pt;
	margin-right:0mm;
	margin-bottom:4.0pt;
	margin-left:0mm;
	border:none;
	padding:0mm;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:#4F81BD;
	font-weight:normal;}
h4
	{mso-style-link:"見出し 4 \(文字\)";
	margin-top:10.0pt;
	margin-right:0mm;
	margin-bottom:4.0pt;
	margin-left:0mm;
	border:none;
	padding:0mm;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:#4F81BD;
	font-weight:normal;
	font-style:italic;}
h5
	{mso-style-link:"見出し 5 \(文字\)";
	margin-top:10.0pt;
	margin-right:0mm;
	margin-bottom:4.0pt;
	margin-left:0mm;
	font-size:11.0pt;
	font-family:"Arial","sans-serif";
	color:#4F81BD;
	font-weight:normal;}
h6
	{mso-style-link:"見出し 6 \(文字\)";
	margin-top:14.0pt;
	margin-right:0mm;
	margin-bottom:5.0pt;
	margin-left:0mm;
	font-size:11.0pt;
	font-family:"Arial","sans-serif";
	color:#4F81BD;
	font-weight:normal;
	font-style:italic;}
p.MsoHeading7, li.MsoHeading7, div.MsoHeading7
	{mso-style-link:"見出し 7 \(文字\)";
	margin-top:16.0pt;
	margin-right:0mm;
	margin-bottom:5.0pt;
	margin-left:0mm;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	color:#9BBB59;
	font-weight:bold;}
p.MsoHeading8, li.MsoHeading8, div.MsoHeading8
	{mso-style-link:"見出し 8 \(文字\)";
	margin-top:16.0pt;
	margin-right:0mm;
	margin-bottom:5.0pt;
	margin-left:0mm;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	color:#9BBB59;
	font-weight:bold;
	font-style:italic;}
p.MsoHeading9, li.MsoHeading9, div.MsoHeading9
	{mso-style-link:"見出し 9 \(文字\)";
	margin-top:16.0pt;
	margin-right:0mm;
	margin-bottom:5.0pt;
	margin-left:0mm;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	color:#9BBB59;
	font-style:italic;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"ヘッダー \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	layout-grid-mode:char;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"フッター \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	layout-grid-mode:char;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:9.0pt;
	font-family:"Century","serif";
	font-weight:bold;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"表題 \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	text-align:center;
	border:none;
	padding:0mm;
	font-size:30.0pt;
	font-family:"Arial","sans-serif";
	color:#243F60;
	font-style:italic;}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-link:"副題 \(文字\)";
	margin-top:10.0pt;
	margin-right:0mm;
	margin-bottom:45.0pt;
	margin-left:0mm;
	text-align:right;
	font-size:12.0pt;
	font-family:"Century","serif";
	font-style:italic;}
strong
	{letter-spacing:0pt;}
em
	{color:#5A5A5A;
	font-weight:bold;}
p.MsoDocumentMap, li.MsoDocumentMap, div.MsoDocumentMap
	{mso-style-link:"見出しマップ \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:9.0pt;
	font-family:"MS UI Gothic";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"吹き出し \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:9.0pt;
	font-family:"Arial","sans-serif";}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{mso-style-link:"行間詰め \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0mm;
	margin-right:0mm;
	margin-bottom:0mm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0mm;
	margin-right:0mm;
	margin-bottom:0mm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0mm;
	margin-right:0mm;
	margin-bottom:0mm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0mm;
	margin-right:0mm;
	margin-bottom:0mm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:11.0pt;
	font-family:"Century","serif";}
p.MsoQuote, li.MsoQuote, div.MsoQuote
	{mso-style-link:"引用文 \(文字\)";
	margin:0mm;
	margin-bottom:.0001pt;
	text-indent:18.0pt;
	font-size:11.0pt;
	font-family:"Arial","sans-serif";
	color:#5A5A5A;
	font-style:italic;}
p.MsoIntenseQuote, li.MsoIntenseQuote, div.MsoIntenseQuote
	{mso-style-link:"引用文 2 \(文字\)";
	margin-top:16.0pt;
	margin-right:72.0pt;
	margin-bottom:16.0pt;
	margin-left:72.0pt;
	text-indent:18.0pt;
	line-height:125%;
	background:#4F81BD;
	border:none;
	padding:0mm;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:white;
	font-style:italic;}
span.MsoSubtleEmphasis
	{color:#5A5A5A;
	font-style:italic;}
span.MsoIntenseEmphasis
	{color:#4F81BD;
	font-weight:bold;
	font-style:italic;}
span.MsoSubtleReference
	{color:windowtext;
	text-decoration:underline;}
span.MsoIntenseReference
	{color:#76923C;
	font-weight:bold;
	text-decoration:underline;}
span.MsoBookTitle
	{font-family:"Arial","sans-serif";
	color:windowtext;
	font-weight:bold;
	font-style:italic;}
p.MsoTocHeading, li.MsoTocHeading, div.MsoTocHeading
	{margin-top:30.0pt;
	margin-right:0mm;
	margin-bottom:4.0pt;
	margin-left:0mm;
	border:none;
	padding:0mm;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:#365F91;
	font-weight:bold;}
span.1
	{mso-style-name:"見出し 1 \(文字\)";
	mso-style-link:"見出し 1";
	font-family:"Arial","sans-serif";
	color:#365F91;
	font-weight:bold;}
span.2
	{mso-style-name:"見出し 2 \(文字\)";
	mso-style-link:"見出し 2";
	font-family:"Arial","sans-serif";
	color:#365F91;}
span.3
	{mso-style-name:"見出し 3 \(文字\)";
	mso-style-link:"見出し 3";
	font-family:"Arial","sans-serif";
	color:#4F81BD;}
span.4
	{mso-style-name:"見出し 4 \(文字\)";
	mso-style-link:"見出し 4";
	font-family:"Arial","sans-serif";
	color:#4F81BD;
	font-style:italic;}
span.5
	{mso-style-name:"見出し 5 \(文字\)";
	mso-style-link:"見出し 5";
	font-family:"Arial","sans-serif";
	color:#4F81BD;}
span.6
	{mso-style-name:"見出し 6 \(文字\)";
	mso-style-link:"見出し 6";
	font-family:"Arial","sans-serif";
	color:#4F81BD;
	font-style:italic;}
span.7
	{mso-style-name:"見出し 7 \(文字\)";
	mso-style-link:"見出し 7";
	font-family:"Arial","sans-serif";
	color:#9BBB59;
	font-weight:bold;}
span.8
	{mso-style-name:"見出し 8 \(文字\)";
	mso-style-link:"見出し 8";
	font-family:"Arial","sans-serif";
	color:#9BBB59;
	font-weight:bold;
	font-style:italic;}
span.9
	{mso-style-name:"見出し 9 \(文字\)";
	mso-style-link:"見出し 9";
	font-family:"Arial","sans-serif";
	color:#9BBB59;
	font-style:italic;}
span.a
	{mso-style-name:"表題 \(文字\)";
	mso-style-link:表題;
	font-family:"Arial","sans-serif";
	color:#243F60;
	font-style:italic;}
span.a0
	{mso-style-name:"副題 \(文字\)";
	mso-style-link:副題;
	font-family:"Century","serif";
	font-style:italic;}
span.a1
	{mso-style-name:"行間詰め \(文字\)";
	mso-style-link:行間詰め;}
span.a2
	{mso-style-name:"引用文 \(文字\)";
	mso-style-link:引用文;
	font-family:"Arial","sans-serif";
	color:#5A5A5A;
	font-style:italic;}
span.20
	{mso-style-name:"引用文 2 \(文字\)";
	mso-style-link:"引用文 2";
	font-family:"Arial","sans-serif";
	color:white;
	background:#4F81BD;
	font-style:italic;}
span.a3
	{mso-style-name:"見出しマップ \(文字\)";
	mso-style-link:見出しマップ;
	font-family:"MS UI Gothic";}
span.a4
	{mso-style-name:"吹き出し \(文字\)";
	mso-style-link:吹き出し;
	font-family:"Arial","sans-serif";}
span.a5
	{mso-style-name:"ヘッダー \(文字\)";
	mso-style-link:ヘッダー;}
span.a6
	{mso-style-name:"フッター \(文字\)";
	mso-style-link:フッター;}
.MsoChpDefault
	{font-size:11.0pt;}
.MsoPapDefault
	{text-indent:18.0pt;}
 /* Page Definitions */
 @page Section1
	{size:595.3pt 841.9pt;
	margin:99.25pt 30.0mm 30.0mm 30.0mm;
	layout-grid:18.0pt;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 ol
	{margin-bottom:0mm;}
ul
	{margin-bottom:0mm;}
-->
</style>

</head>

<body lang=JA style='text-justify-trim:punctuation'>

<div class=Section1 style='layout-grid:18.0pt'>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='font-family:"ＭＳ ゴシック"'>閉じる動作</span><span lang=EN-US>1</span></h1>

</div>

<p class=MsoNormal>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=23 height=11></td>
  <td width=12></td>
  <td width=163></td>
  <td width=33></td>
  <td width=117></td>
  <td width=4></td>
  <td width=155></td>
  <td width=41></td>
 </tr>
 <tr>
  <td height=67></td>
  <td colspan=6 align=left valign=top><img width=484 height=67
  src="/document/tab/img/image001.gif"></td>
 </tr>
 <tr>
  <td height=10></td>
 </tr>
 <tr>
  <td height=13></td>
  <td colspan=2></td>
  <td colspan=2 rowspan=3 align=left valign=top><img width=150 height=66
  src="/document/tab/img/image002.gif"></td>
 </tr>
 <tr>
  <td height=28></td>
  <td colspan=2></td>
  <td></td>
  <td colspan=2 width=196 height=28 bgcolor=white style='vertical-align:top;
  background:white'><span style='position:absolute;left:0pt;z-index:135'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td>
    <div style='padding:.7pt 5.85pt .7pt 5.85pt'>
    <p class=MsoNormal style='text-indent:0mm'><span lang=EN-US
    style='font-size:9.0pt'>C</span><span style='font-size:9.0pt;font-family:
    "ＭＳ 明朝","serif"'>を閉じる</span></p>
    </div>
    </td>
   </tr>
  </table>
  </span>&nbsp;</td>
 </tr>
 <tr>
  <td height=25></td>
 </tr>
 <tr>
  <td height=9></td>
 </tr>
 <tr>
  <td height=59></td>
  <td></td>
  <td colspan=2 align=left valign=top><img width=196 height=59
  src="/document/tab/img/image003.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoNormal><span lang=EN-US>A</span><span style='font-family:"ＭＳ 明朝","serif"'>から</span><span
lang=EN-US>B</span><span style='font-family:"ＭＳ 明朝","serif"'>，</span><span
lang=EN-US>C,</span><span style='font-family:"ＭＳ 明朝","serif"'>、</span><span
lang=EN-US>D</span><span style='font-family:"ＭＳ 明朝","serif"'>と一直線にタブを開いて、</span><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>を閉じ場合、</span><span
lang=EN-US>D</span><span style='font-family:"ＭＳ 明朝","serif"'>も閉じられます。</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<span lang=EN-US style='font-size:11.0pt;font-family:"Century","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='font-family:"ＭＳ ゴシック"'>閉じる動作２　ケース一覧</span></h1>

</div>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=47 height=8></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=436 height=313 src="/document/tab/img/image004.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<div>

<table class=40 border=1 cellspacing=0 cellpadding=0 style='border-collapse:
 collapse;border:none'>
 <tr>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  background:#4F81BD;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle align=center style='margin-left:0mm;
  text-align:center;text-align:3;text-indent:0mm'><b><span style='font-family:"ＭＳ 明朝","serif";
  color:white'>閉じるタブ</span></b></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-left:none;background:#4F81BD;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast align=center style='margin-left:0mm;
  text-align:center;text-indent:0mm'><b><span style='font-family:"ＭＳ 明朝","serif";
  color:white'>閉じられるタブ</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>F</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>FG</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>C</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>CDEFG</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>K</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>KLM</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>I</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>IJKLMNO</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<span lang=EN-US style='font-size:11.0pt;font-family:"Century","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='position:absolute;z-index:22;margin-left:24px;margin-top:43px;
width:100px;height:28px'>

<table cellpadding=0 cellspacing=0>
 <tr>
  <td width=100 height=28 bgcolor=white style='vertical-align:top;background:
  white'><span style='position:absolute;z-index:22'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td>
    <div style='padding:.7pt 5.85pt .7pt 5.85pt'>
    <p class=MsoNormal style='text-indent:0mm'><span style='font-size:9.0pt;
    font-family:"ＭＳ 明朝","serif"'>●アクティブ</span></p>
    </div>
    </td>
   </tr>
  </table>
  </span>&nbsp;</td>
 </tr>
</table>

</span><span style='font-family:"ＭＳ ゴシック"'>新規タブを開く動作－１</span></h1>

</div>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=35 height=10></td>
  <td width=16></td>
  <td width=149></td>
  <td width=150></td>
  <td width=16></td>
  <td width=21></td>
  <td width=132></td>
  <td width=31></td>
  <td width=12></td>
 </tr>
 <tr>
  <td height=71></td>
  <td colspan=6 align=left valign=top><img width=484 height=71
  src="/document/tab/img/image005.gif"></td>
 </tr>
 <tr>
  <td height=12></td>
 </tr>
 <tr>
  <td height=132></td>
  <td colspan=7 align=left valign=top><img width=515 height=132
  src="/document/tab/img/image006.gif"></td>
 </tr>
 <tr>
  <td height=12></td>
 </tr>
 <tr>
  <td height=13></td>
  <td colspan=2></td>
  <td rowspan=3 align=left valign=top><img width=150 height=67
  src="/document/tab/img/image007.gif"></td>
 </tr>
 <tr>
  <td height=28></td>
  <td colspan=2></td>
  <td></td>
  <td colspan=4 width=196 height=28 bgcolor=white style='vertical-align:top;
  background:white'><span style='position:absolute;left:0pt;z-index:44'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td>
    <div style='padding:.7pt 5.85pt .7pt 5.85pt'>
    <p class=MsoNormal style='text-indent:0mm'><span lang=EN-US
    style='font-size:9.0pt'>B</span><span style='font-size:9.0pt;font-family:
    "ＭＳ 明朝","serif"'>から再度</span><span lang=EN-US style='font-size:9.0pt'>C</span><span
    style='font-size:9.0pt;font-family:"ＭＳ 明朝","serif"'>と同じ画面を開く</span></p>
    </div>
    </td>
   </tr>
  </table>
  </span>&nbsp;</td>
 </tr>
 <tr>
  <td height=26></td>
 </tr>
 <tr>
  <td height=6></td>
 </tr>
 <tr>
  <td height=71></td>
  <td></td>
  <td colspan=4 align=left valign=top><img width=336 height=71
  src="/document/tab/img/image008.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>、</span><span
lang=EN-US>D </span><span style='font-family:"ＭＳ 明朝","serif"'>が閉じられ</span><span
lang=EN-US>C+</span><span style='font-family:"ＭＳ 明朝","serif"'>が新規タブで開きます。</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>B</span><span style='font-family:"ＭＳ 明朝","serif"'>から</span><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>とは違う画面を新規で開いた場合は以下の様に</span><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>、</span><span
lang=EN-US>D</span><span style='font-family:"ＭＳ 明朝","serif"'>は閉じられず、</span><span
lang=EN-US>E</span><span style='font-family:"ＭＳ 明朝","serif"'>が新規で作成されます。</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=35 height=22></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=484 height=149 src="/document/tab/img/image009.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<span lang=EN-US style='font-size:11.0pt;font-family:"Century","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='position:absolute;z-index:56;margin-left:24px;margin-top:43px;
width:100px;height:28px'>

<table cellpadding=0 cellspacing=0>
 <tr>
  <td width=100 height=28 bgcolor=white style='vertical-align:top;background:
  white'><span style='position:absolute;z-index:56'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td>
    <div style='padding:.7pt 5.85pt .7pt 5.85pt'>
    <p class=MsoNormal style='text-indent:0mm'><span style='font-size:9.0pt;
    font-family:"ＭＳ 明朝","serif"'>●アクティブ</span></p>
    </div>
    </td>
   </tr>
  </table>
  </span>&nbsp;</td>
 </tr>
</table>

</span><span style='font-family:"ＭＳ ゴシック"'>新規タブを開く動作－２</span></h1>

</div>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=35 height=10></td>
  <td width=163></td>
  <td width=321></td>
  <td width=25></td>
 </tr>
 <tr>
  <td height=129></td>
  <td colspan=2 align=left valign=top><img width=484 height=129
  src="/document/tab/img/image010.gif"></td>
 </tr>
 <tr>
  <td height=7></td>
 </tr>
 <tr>
  <td height=66></td>
  <td></td>
  <td colspan=2 align=left valign=top><img width=346 height=66
  src="/document/tab/img/image011.gif"></td>
 </tr>
 <tr>
  <td height=2></td>
 </tr>
 <tr>
  <td height=117></td>
  <td colspan=2 align=left valign=top><img width=484 height=117
  src="/document/tab/img/image012.gif"></td>
 </tr>
 <tr>
  <td height=7></td>
 </tr>
 <tr>
  <td height=185></td>
  <td colspan=3 align=left valign=top><img width=509 height=185
  src="/document/tab/img/image013.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>B</span><span style='font-family:"ＭＳ 明朝","serif"'>、</span><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>、</span><span
lang=EN-US>D</span><span style='font-family:"ＭＳ 明朝","serif"'>が閉じられ新規に</span><span
lang=EN-US>B+</span><span style='font-family:"ＭＳ 明朝","serif"'>が開きます（以下、上書き）。</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
style='font-family:"ＭＳ 明朝","serif"'>【新規タブを開く動作－１】と同様に</span><span lang=EN-US>B</span><span
style='font-family:"ＭＳ 明朝","serif"'>、</span><span lang=EN-US>E</span><span
style='font-family:"ＭＳ 明朝","serif"'>意外の画面を開いた場合は</span><span lang=EN-US>B</span><span
style='font-family:"ＭＳ 明朝","serif"'>、</span><span lang=EN-US>C</span><span
style='font-family:"ＭＳ 明朝","serif"'>、</span><span lang=EN-US>D</span><span
style='font-family:"ＭＳ 明朝","serif"'>、は閉じられず、</span><span lang=EN-US>H</span><span
style='font-family:"ＭＳ 明朝","serif"'>が新規に開きます。</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
style='font-family:"ＭＳ 明朝","serif"'>また、上書きとなるケースは、【現在タブ（</span><span
lang=EN-US>A</span><span style='font-family:"ＭＳ 明朝","serif"'>）】直後のタブの、初めに開いた画面が、新規で開こうとしている画面と同じ場合です。</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
style='font-family:"ＭＳ 明朝","serif"'>なので以下の様なケースが起こりえます。</span></p>

<span lang=EN-US style='font-size:11.0pt;font-family:"Century","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='position:absolute;z-index:132;margin-left:24px;margin-top:
43px;width:100px;height:28px'>

<table cellpadding=0 cellspacing=0>
 <tr>
  <td width=100 height=28 bgcolor=white style='vertical-align:top;background:
  white'><span style='position:absolute;z-index:132'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td>
    <div style='padding:.7pt 5.85pt .7pt 5.85pt'>
    <p class=MsoNormal style='text-indent:0mm'><span style='font-size:9.0pt;
    font-family:"ＭＳ 明朝","serif"'>●アクティブ</span></p>
    </div>
    </td>
   </tr>
  </table>
  </span>&nbsp;</td>
 </tr>
</table>

</span><span style='font-family:"ＭＳ ゴシック"'>新規タブを開く動作－３（起こり得るケース１）</span></h1>

</div>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=35 height=22></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=338 height=135 src="/document/tab/img/image014.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoNormal style='text-indent:0mm'><span style='font-family:"ＭＳ 明朝","serif"'>　　違うルートから同じ</span><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>の画面を開くことは可能です。</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<span lang=EN-US style='font-size:11.0pt;font-family:"Century","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='font-family:"ＭＳ ゴシック"'>指定の場所まで戻る１</span></h1>

</div>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US style='font-size:
9.0pt'>D</span><span style='font-size:9.0pt;font-family:"ＭＳ 明朝","serif"'>から【</span><span
lang=EN-US style='font-size:9.0pt'>B</span><span style='font-size:9.0pt;
font-family:"ＭＳ 明朝","serif"'>へ戻る】ボタンで</span><span lang=EN-US style='font-size:
9.0pt'>B</span><span style='font-size:9.0pt;font-family:"ＭＳ 明朝","serif"'>の画面まで戻りたい場合。</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
style='position:relative;z-index:140'><span style='left:0px;position:absolute;
left:24px;top:-1px;width:495px;height:82px'><img width=495 height=82
src="/document/tab/img/image015.gif"></span></span><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>B</span><span style='font-family:"ＭＳ 明朝","serif"'>から</span><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>へ移動する際にキーをセットしておく</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=35 height=3></td>
  <td width=113></td>
  <td width=83></td>
  <td width=263></td>
 </tr>
 <tr>
  <td height=67></td>
  <td></td>
  <td colspan=2 align=left valign=top><img width=346 height=67
  src="/document/tab/img/image016.gif"></td>
 </tr>
 <tr>
  <td height=12></td>
 </tr>
 <tr>
  <td height=71></td>
  <td colspan=2 align=left valign=top><img width=196 height=71
  src="/document/tab/img/image017.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>C</span><span style='font-family:"ＭＳ 明朝","serif"'>、</span><span
lang=EN-US>D</span><span style='font-family:"ＭＳ 明朝","serif"'>は閉じられ、フォーカスが</span><span
lang=EN-US>B</span><span style='font-family:"ＭＳ 明朝","serif"'>に移ります。</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<span lang=EN-US style='font-size:11.0pt;font-family:"Century","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>

<h1><span style='font-family:"ＭＳ ゴシック"'>指定の場所まで戻る２　ケース一覧</span></h1>

</div>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=47 height=8></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=436 height=313 src="/document/tab/img/image004.gif"></td>
 </tr>
</table>

<span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<div>

<table class=40 border=1 cellspacing=0 cellpadding=0 style='border-collapse:
 collapse;border:none'>
 <tr>
  <td width=193 valign=top style='width:145.0pt;border:solid windowtext 1.0pt;
  background:#4F81BD;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle align=center style='margin-left:0mm;
  text-align:center;text-indent:0mm'><b><span style='font-family:"ＭＳ 明朝","serif";
  color:white'>戻りたいタブ</span></b></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-left:none;background:#4F81BD;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle align=center style='margin-left:0mm;
  text-align:center;text-indent:0mm'><b><span style='font-family:"ＭＳ 明朝","serif";
  color:white'>閉じる画面のタブ</span></b></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border:solid windowtext 1.0pt;
  border-left:none;background:#4F81BD;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast align=center style='margin-left:0mm;
  text-align:center;text-indent:0mm'><b><span style='font-family:"ＭＳ 明朝","serif";
  color:white'>閉じられるタブ</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.0pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>C</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>G</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>FG</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.0pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>B</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>G</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>CDEFG</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.0pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>K</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>M</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>M</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.0pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>I</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>M</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>KLM</span></p>
  </td>
 </tr>
 <tr>
  <td width=193 valign=top style='width:145.0pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>A</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>O</span></p>
  </td>
  <td width=193 valign=top style='width:145.05pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D3DFEE;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0mm;text-indent:0mm'><span
  lang=EN-US>HIJKLMNO</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoListParagraphCxSpFirst style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:24.0pt;text-indent:0mm'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='text-indent:0mm'><span lang=EN-US>&nbsp;</span></p>

</div>

</body>

</html>
