<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * require
 */
require_once 'Mikoshiva/Validate/Exception.php';
require_once 'Mikoshiva/Validate/Abstract.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';

/**
 * カード番号が、ワンタイムデビットカードか否かを判定します。(ワンタイムデビットの場合 false)
 * 
 * 
 * 
 * @package Mikoshiva_Validate
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/10
 * @version SVN:$Id: OneTimeDebit.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_OneTimeDebit extends Mikoshiva_Validate_Abstract{

    const ONE_TIME_DEBIT          = 'oneTimeDebit';
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::ONE_TIME_DEBIT     => 'ワンタイムデビットカードはサポートされていません。',
    );
    
    /**
     * フォームのデータ を保持します
     * @var array フォームのデータ を保持します
     */
    protected $_data;
    
    
    /**
     * フォームのデータ をセットします
     *
     * @param array $data フォームのデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/15
     * @version SVN:$Id: OneTimeDebit.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {
        logInfo(LOG_START);
        $this->_data = $data;
        logInfo(LOG_END);
    }
    
    
    /**
     * クラスを初期化します。
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     */
    public function __construct() {
    }
    
    /**
     * バリデートを実行します
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @param  string $value
     * @return boolean
     */
    public function isValid($cardNo) {
        //-----------------------------------------------
        // ■入力値チェック
        //-----------------------------------------------
        if(isBlankOrNull($cardNo)) {
            return false;
        }
        
        //-----------------------------------------------
        // ■Zend レジストリからワンタイムデビットカード（の先頭につく番号）のリストを取得
        //-----------------------------------------------
        $cardList = Zend_Registry::get('ONE_TIME_DEBIT_CARD_LIST');
        
        //-----------------------------------------------
        // ■バリデート実行
        //-----------------------------------------------
        foreach($cardList as $v) {
            // リストにある番号から始まるカード番号が見つかった場合、エラー
            if(preg_match('/^' . $v . '/', $cardNo) > 0) {
                $this->_error(self::ONE_TIME_DEBIT);
                return false;
            }
        }
        Mikoshiva_Logger::debug(self::ONE_TIME_DEBIT);
        return true;
    }
}
