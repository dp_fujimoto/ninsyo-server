<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">コンテンツ一覧</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // 新規作成ボタン -->
            {newTabButton name="新規作成" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/input"}

            <!-- // フォーム開始 -->
            <form name="searchForm" id="searchForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="mode" value="search">

                <!-- 検索条件テーブル 開始-->
                <table class="COM_tableBorderZero" >
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">部署</td>
                        <td class="COM_nowrap">
                            {$attribs = array()}
                            {$attribs['onChange'] = "KO_currentTab('{$smarty.const.SUB_FOLDER_PATH}/product/content/search', '"|cat:$tabId|cat:"', 'searchForm', []);"}
                            {divisionLister name="divisionId" mode="select" selected=$form->getDivisionId() attribs=$attribs}
                        </td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">コンテンツ名or管理名</td>
                        <td class="COM_nowrap"><input type="text" name="name" value="{$form->getName()}" size="50" maxlength="200"></td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">状態</td>
                        <td class="COM_nowrap">
                            <select name="deleteFlag">
                                <option value="">通常</option>
                                <option value="1" {if $form->getDeleteFlag() === '1'}selected{/if}>削除されたもの</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">ブランド</td>
                        <td class="COM_nowrap">{formSelect name="brandId" options=$form->getBrandList() selected=$form->getBrandId() addEmpty=true}</td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">商品</td>
                        <td class="COM_nowrap">{formSelect name="productId" options=$form->getProductList() selected=$form->getProductId() addEmpty=true}</td>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">用途</td>
                        <td class="COM_nowrap">
                            {statusLister name="applicationType" selected=$form->getApplicationType() status="application_type" addEmpty=true}
                        </td>
                    </tr>
                </table>
                <!-- 検索条件テーブル 終了-->

                <br>

                {currentTabButton name="　　　検索　　　" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/search" tabId=$tabId formId="searchForm"}　　　　　　
                {currentTabButton name="リセット" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/search" tabId=$tabId}

            </form>
            <!-- // 検索フォーム終了 -->

            <hr>

            {* 検索結果あり *}
            {if count($search) > 0}

                <!-- // 検索結果件数開始 -->
                <div>
                    結果 {$search|count}件ヒットしました。<br>
                </div>
                <!-- // 検索結果件数終了 -->

                <br>

                <!-- // 検索結果リスト開始 -->
                <table class="COM_table PDT_tableWidthBasic">
                    <tr>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">詳細</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">ID</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">ブランド名／商品名</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">用途</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">コンテンツ名／管理名</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">商品KEY</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">更新日</td>
                        <td class="COM_bgColorLight COM_bold COM_center COM_nowrap">編集</td>
                    </tr>

                    {assign var="lineCount" value='0'}
                    {foreach from=$search key="k" item="v"}
                    {if $lineCount === '1'}
                        {assign var="lineColor" value="PDT_rowBackgoundColor"}
                        {assign var="lineCount" value='0'}
                    {else}
                        {assign var="lineColor" value=''}
                        {assign var="lineCount" value='1'}
                    {/if}

                    <tr>
                        <td class="COM_nowrap {$lineColor}">
                            {newTabButton name="詳細" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/detail/contentId/"|cat:$v['mct_content_id'] protected=true}
                        </td>
                        <td class="COM_nowrap {$lineColor} {if $v['mct_delete_flag'] === '1'}COM_fontRed{/if}">{$v['mct_content_id']}</td>
                        <td class="COM_nowrap {$lineColor}">{$v['mbr_brand_name']}<br><span class="COM_fontGrey">{$v['mpr_product_name']}</span></td>
                        <td class="COM_nowrap {$lineColor}">{statusMapping code=$v['mct_application_type']}{if $v['mct_test_flag'] === '1'}<span class="COM_fontRed">（テスト用）</span>{/if}</td>
                        <td class="COM_nowrap {$lineColor}">{$v['mct_content_name']}<br><span class="COM_fontGrey">{$v['mct_confirm_name']}</span></td>
                        <td class="COM_nowrap {$lineColor}">{$v['mpr_product_code']}</td>
                        <td class="COM_nowrap {$lineColor}">{$v['mct_update_timestamp']}</td>
                        <td class="COM_nowrap {$lineColor}">
                            {newTabButton name="編集" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/input/contentId/"|cat:$v['mct_content_id'] protected=true}
                        </td>
                    </tr>
                    {/foreach}
                </table>
                <!-- // 検索結果リスト終了 -->

            {else}

                <p>該当する検索結果はありません。</p>

            {/if}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->