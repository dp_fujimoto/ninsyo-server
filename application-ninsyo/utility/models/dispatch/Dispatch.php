<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';


/**
 *
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/22
 * @version SVN:$Id: Dispatch.php,v 1.1 2012/09/21 07:08:34 fujimoto Exp $
 */
class Utility_Models_Dispatch_Dispatch
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定されたacdをMikoshiva上のURL(形式 string   '/module/controller/action')に変換して返します。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                              Mikoshiva_Controller_Mapping_ActionContext $context) {
        $acd = '';
        $acd = $actionForm->getAcd();
        $ACD_MAP_LIST = Zend_Registry::get('ACD_MAP_LIST');
        if(array_key_exists($acd, $ACD_MAP_LIST)) {
            echo $ACD_MAP_LIST[$acd];
        }
        return;

    }
}
