<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_balance_payments
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxBalancePayments extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tbp';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'balancePaymentsId' => null,
        'claimId' => null,
        'productClassification' => null,
        'postedAdvanceReceived' => null,
        'postedDeferredIncome' => null,
        'postedSales' => null,
        'postedCommission' => null,
        'excessRevenues' => null,
        'postedPlanNumber' => null,
        'postedNumber' => null,
        'postedStartDate' => null,
        'postedCompleteFlag' => null,
        'postedCompleteDate' => null,
        'errorFlag' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tbp_balance_payments_id の値を返します
	 *
     * @return int tbp_balance_payments_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBalancePaymentsId() {
		return $this->_properties['balancePaymentsId'];
    }


    /**
     * tbp_balance_payments_id の値をセットします
	 *
	 * @param int $balancePaymentsId tbp_balance_payments_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBalancePaymentsId($balancePaymentsId) {
		$this->_properties['balancePaymentsId'] = $balancePaymentsId;
    }


    /**
     * tbp_claim_id の値を返します
	 *
     * @return int tbp_claim_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClaimId() {
		return $this->_properties['claimId'];
    }


    /**
     * tbp_claim_id の値をセットします
	 *
	 * @param int $claimId tbp_claim_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClaimId($claimId) {
		$this->_properties['claimId'] = $claimId;
    }


    /**
     * tbp_product_classification の値を返します
	 *
     * @return varchar tbp_product_classification
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductClassification() {
		return $this->_properties['productClassification'];
    }


    /**
     * tbp_product_classification の値をセットします
	 *
	 * @param varchar $productClassification tbp_product_classification
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductClassification($productClassification) {
		$this->_properties['productClassification'] = $productClassification;
    }


    /**
     * tbp_posted_advance_received の値を返します
	 *
     * @return int tbp_posted_advance_received
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedAdvanceReceived() {
		return $this->_properties['postedAdvanceReceived'];
    }


    /**
     * tbp_posted_advance_received の値をセットします
	 *
	 * @param int $postedAdvanceReceived tbp_posted_advance_received
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedAdvanceReceived($postedAdvanceReceived) {
		$this->_properties['postedAdvanceReceived'] = $postedAdvanceReceived;
    }


    /**
     * tbp_posted_deferred_income の値を返します
	 *
     * @return int tbp_posted_deferred_income
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedDeferredIncome() {
		return $this->_properties['postedDeferredIncome'];
    }


    /**
     * tbp_posted_deferred_income の値をセットします
	 *
	 * @param int $postedDeferredIncome tbp_posted_deferred_income
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedDeferredIncome($postedDeferredIncome) {
		$this->_properties['postedDeferredIncome'] = $postedDeferredIncome;
    }


    /**
     * tbp_posted_sales の値を返します
	 *
     * @return int tbp_posted_sales
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedSales() {
		return $this->_properties['postedSales'];
    }


    /**
     * tbp_posted_sales の値をセットします
	 *
	 * @param int $postedSales tbp_posted_sales
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedSales($postedSales) {
		$this->_properties['postedSales'] = $postedSales;
    }


    /**
     * tbp_posted_commission の値を返します
	 *
     * @return int tbp_posted_commission
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedCommission() {
		return $this->_properties['postedCommission'];
    }


    /**
     * tbp_posted_commission の値をセットします
	 *
	 * @param int $postedCommission tbp_posted_commission
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedCommission($postedCommission) {
		$this->_properties['postedCommission'] = $postedCommission;
    }


    /**
     * tbp_excess_revenues の値を返します
	 *
     * @return int tbp_excess_revenues
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExcessRevenues() {
		return $this->_properties['excessRevenues'];
    }


    /**
     * tbp_excess_revenues の値をセットします
	 *
	 * @param int $excessRevenues tbp_excess_revenues
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExcessRevenues($excessRevenues) {
		$this->_properties['excessRevenues'] = $excessRevenues;
    }


    /**
     * tbp_posted_plan_number の値を返します
	 *
     * @return int tbp_posted_plan_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedPlanNumber() {
		return $this->_properties['postedPlanNumber'];
    }


    /**
     * tbp_posted_plan_number の値をセットします
	 *
	 * @param int $postedPlanNumber tbp_posted_plan_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedPlanNumber($postedPlanNumber) {
		$this->_properties['postedPlanNumber'] = $postedPlanNumber;
    }


    /**
     * tbp_posted_number の値を返します
	 *
     * @return int tbp_posted_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedNumber() {
		return $this->_properties['postedNumber'];
    }


    /**
     * tbp_posted_number の値をセットします
	 *
	 * @param int $postedNumber tbp_posted_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedNumber($postedNumber) {
		$this->_properties['postedNumber'] = $postedNumber;
    }


    /**
     * tbp_posted_start_date の値を返します
	 *
     * @return date tbp_posted_start_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedStartDate() {
		return $this->_properties['postedStartDate'];
    }


    /**
     * tbp_posted_start_date の値をセットします
	 *
	 * @param date $postedStartDate tbp_posted_start_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedStartDate($postedStartDate) {
		$this->_properties['postedStartDate'] = $postedStartDate;
    }


    /**
     * tbp_posted_complete_flag の値を返します
	 *
     * @return char tbp_posted_complete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedCompleteFlag() {
		return $this->_properties['postedCompleteFlag'];
    }


    /**
     * tbp_posted_complete_flag の値をセットします
	 *
	 * @param char $postedCompleteFlag tbp_posted_complete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedCompleteFlag($postedCompleteFlag) {
		$this->_properties['postedCompleteFlag'] = $postedCompleteFlag;
    }


    /**
     * tbp_posted_complete_date の値を返します
	 *
     * @return date tbp_posted_complete_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPostedCompleteDate() {
		return $this->_properties['postedCompleteDate'];
    }


    /**
     * tbp_posted_complete_date の値をセットします
	 *
	 * @param date $postedCompleteDate tbp_posted_complete_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPostedCompleteDate($postedCompleteDate) {
		$this->_properties['postedCompleteDate'] = $postedCompleteDate;
    }


    /**
     * tbp_error_flag の値を返します
	 *
     * @return char tbp_error_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorFlag() {
		return $this->_properties['errorFlag'];
    }


    /**
     * tbp_error_flag の値をセットします
	 *
	 * @param char $errorFlag tbp_error_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorFlag($errorFlag) {
		$this->_properties['errorFlag'] = $errorFlag;
    }


    /**
     * tbp_memo の値を返します
	 *
     * @return text tbp_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * tbp_memo の値をセットします
	 *
	 * @param text $memo tbp_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * tbp_delete_flag の値を返します
	 *
     * @return char tbp_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tbp_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tbp_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tbp_deletion_datetime の値を返します
	 *
     * @return datetime tbp_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tbp_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tbp_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tbp_update_datetime の値を返します
	 *
     * @return datetime tbp_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tbp_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tbp_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tbp_registration_datetime の値を返します
	 *
     * @return datetime tbp_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tbp_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tbp_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tbp_update_timestamp の値を返します
	 *
     * @return timestamp tbp_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tbp_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tbp_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























