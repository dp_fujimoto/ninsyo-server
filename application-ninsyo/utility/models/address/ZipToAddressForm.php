<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * -----------------------------------------------------------------------
 *    ワンポイント
 * -----------------------------------------------------------------------
 *    ■クラス名
 *    アクションフォームクラスのクラス名は、
 *    必ず「～Form」としてください。
 *
 *    ■resetメソッド
 *    任意の初期化処理を行う場合に実装します。
 *    (メソッド定義は省略可能です)
 *
 *    ■validateメソッド
 *    任意のバリデート処理を行う場合に実装します。(独自バリデート)
 *    validation.yml定義による固定バリデート処理を実行するには、
 *    スーパークラスのvalidateメソッドを呼び出してください。
 *    「parent::reset($config, $request);」
 *    その場合、スーパークラスのvalidateメソッドが返す
 *    固定バリデートの実行結果値は、適切に処理してください。
 *    (メソッド定義は省略可能です)
 * -----------------------------------------------------------------------
 *
 * @author        K.sekiya <kurachi0223@gmail.com>
 * @since        2010/02/15
 * @version        SVN: $Id:
 *
 */
class Utility_Models_Address_ZipToAddressForm
    extends Mikoshiva_Controller_Mapping_Form_Abstract {




    /**
     * アクションフォームの初期化を行います。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {
        parent::reset($config, $request);



    }

    /**
     * バリデートを行います。エラーMSGをリターンでかえす
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {


        //-----------------------------------------------
        //
        // 基本バリデートチェック
        //
        //-----------------------------------------------
        $message = array();
        $message = parent::validate($config, $request);



        //-----------------------------------------------
        //
        // 独自バリデートチェック
        //
        //-----------------------------------------------

        return $message;
    }

}
