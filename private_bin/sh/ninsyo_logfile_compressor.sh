#!/bin/bash
#
# ログファイルを圧縮して削除するスクリプト
#
# @since  2011/03/03
# @author kawakami
#
DATE=$(date +%Y%m%d)

DIRS[0]="/var/www/dp-auth.jp/www/systems/bin_logs/debug"
DIRS[1]="/var/www/dp-auth.jp/www/systems/bin_logs/devel"
DIRS[2]="/var/www/dp-auth.jp/www/systems/bin_logs/sql"
DIRS[3]="/var/www/dp-auth.jp/www/systems/logs/debug"
DIRS[4]="/var/www/dp-auth.jp/www/systems/logs/devel"
DIRS[5]="/var/www/dp-auth.jp/www/systems/logs/sql"
DIRS[6]="/var/www/dp-auth.jp/www/systems/logs/temporary/api"
DIRS[7]="/var/www/dp-auth.jp/www/systems/logs/temporary/batch"
DIRS[8]="/var/www/dp-auth.jp/www/systems/logs/temporary/product"
DIRS[9]="/var/www/dp-auth.jp/www/systems/logs/temporary/tool"
DIRS[10]="/var/www/dp-auth.jp/www/systems/uploads/api/insert-user-content-end-task"
DIRS[11]="/var/www/dp-auth.jp/www/systems/logs/temporary/trade"

LOGFILE="/var/www/dp-auth.jp/www/systems/sh_logs/ninsyo_logfile_compressor_${DATE}.log"

for DIR in ${DIRS[@]};
do
  echo "${DIR}" >> ${LOGFILE}
  cd ${DIR}
  for FILE in $(/usr/bin/find ${DIR} -type f -name "*.txt" -daystart -mtime +3);
  do
    FILENAME=$(/bin/basename ${FILE})
    /bin/tar cvfj ${FILENAME}.tar.gz2 ${FILENAME} --remove-files >> ${LOGFILE}
  done
done
