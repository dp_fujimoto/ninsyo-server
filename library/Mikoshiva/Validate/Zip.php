<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * require
 */
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';
require_once 'Mikoshiva/Validate/Exception.php';

/**
 * 郵便番号の検証を行います
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Zip extends Zend_Validate_Abstract
{
    const STRING_EMPTY = 'stringEmpty';
    const INVALID      = 'noKana';
    const noExist      = 'noExist';

    /**
     * 郵便番号を保持します
     * @var string|array 郵便番号を保持します
     */
    protected $_zip;

    /**
     * 郵便番号下4桁のフォーム名 を保持します
     * @var string 郵便番号下4桁のフォーム名 を保持します
     */
    protected $_zip2Name;

    /**
     * リクエストデータ を保持します
     * @var array リクエストデータ を保持します
     */
    protected $data;

    /**
     * Digits filter used for validation
     *
     * @var Zend_Filter_Digits
     */
    protected static $_filter = null;

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::STRING_EMPTY => "%COLUMN_NAME% は必須項目です。",
        self::INVALID      => "%COLUMN_NAME% の形式が正しくありません。",
        self::noExist      => "入力された %COLUMN_NAME% は存在しません",
    );

    /**
     * 郵便番号をセットします
     *
     * @param string|array $zip 郵便番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setZip($zip) {
        logInfo(LOG_START);
        if(!isBlankOrNull($this->_zip2Name)) {
            $data = array();
            $data = $this->getData();
            if(isBlankOrNull($this->_zip2Name)) {
                throw new Mikoshiva_Validate_Exception('zip2のnameが空です');
            }
            $zip2 = '';
            $zip2 = $data[$this->_zip2Name];
            $this->_zip = $zip . '-' . $zip2;

        } else {
            $this->_zip = $zip;
        }
        logInfo(LOG_END);
    }

    /**
     * 郵便番号下4桁のフォーム名 をセットします
     *
     * @param string $_zip2Name 郵便番号下4桁のフォーム名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setZip2Name($zip2Name) {
        logInfo(LOG_START);
        $this->_zip2Name = $zip2Name;
        logInfo(LOG_END);
    }


    /**
     * リクエストデータ をセットします
     *
     * @param array $data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {

        logInfo(LOG_START);
        $this->data = $data;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号を返します
     *
     * @return string $zip 郵便番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getZip() {
        logInfo(LOG_START);
        return $this->_zip;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号下4桁のフォーム名 を返します
     *
     * @return string $this->_zip2Name 郵便番号下4桁のフォーム名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getZip2Name() {
        logInfo(LOG_START);
        return $this->_zip2Name;
        logInfo(LOG_END);
    }

    /**
     * リクエストデータ を返します
     *
     * @return array $this->data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getData() {
        logInfo(LOG_START);
        return $this->data;
        logInfo(LOG_END);
    }

    public function __construct($zip2Name = '') {
        $this->setZip2Name($zip2Name);
    }

    /**
     * 郵便番号チェック
     *
     * @param   Mikoshiva_Property_Zip $zip 郵便番号を保持するオブジェクト
     * @return boolean
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Interface#isValid($value)
     */
    public function isValid($value) {
        logInfo(LOG_START);
        $this->setZip($value);

        $zip = '';
        $zip = $this->_zip;

        // 形式チェック
        if (preg_match('/^\d{3}-\d{4}$/', $zip) !== 1) {
            $this->_error(self::INVALID);
            return false;
        }
/*****
        // 郵便番号を DB にチェックしに行きます
        $zip    = str_replace('-', '', $zip );
        $ar     = new Mikoshiva_Db_Ar_MstZipCode();
        $select = $ar->select()->where('mzc_zip_code = ?', $zip);
        $row    = $ar->fetchRow($select);
        if ($row === null) {
            $this->_error(self::noExist);
            Mikoshiva_Logger::debug(LOG_END);
            return false;
        }

        Mikoshiva_Logger::debug('郵便番号は正しい形式です');
****/
        logInfo(LOG_END);
        return true;
    }

}
