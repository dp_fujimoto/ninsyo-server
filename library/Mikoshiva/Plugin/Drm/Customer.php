<?php

require_once 'Zend/Http/Client.php';
require_once 'Zend/Http/Client/Adapter/Socket.php';
require_once 'Zend/Http/Client/Adapter/Exception.php';

require_once 'Mikoshiva/Plugin/Drm/Abstruct.php';

/**
 * DRM顧客関連のDRMAPIを呼び出します。
 *
 * @author k.shimowaki
 * @since version - 2010/03/18
 */
class Mikoshiva_Plugin_Drm_Customer extends Mikoshiva_Plugin_Drm_Abstruct {

    /**
     * DRMへ顧客登録を行います。
     * シナリオ・カテゴリ・反応履歴が登録されます。
     *
     * @param Popo_MstCustomer $customer 顧客情報
     * @param array $scenarioId シナリオID
     * @param array $productId 商品ID
     * @param array $customerResponseId 顧客反応履歴ID
     * @param array $categoryId カテゴリID
     * @return string
     *
     * @author k.shimowaki
     * @since version - 2010/03/18
     */
    public function createCustomer(Popo_MstCustomer $customer, array $scenarioId, array $productId, array $categoryId) {

        $c = new Popo_MstCustomer();
        $c = $customer;

        // パラメータセット
        $this->client->setParameterPost('pathCode','CUSTOMER_CREATE');									// パスコード

        $this->client->setParameterPost('email', $c->getEmail());										// メールアドレス
        $this->client->setParameterPost('lastName', $c->getLastName());									// 姓
        $this->client->setParameterPost('firstName', $c->getFirstName());								// 名
        $this->client->setParameterPost('lastNameKana', $c->getLastNameKana());							// 姓カナ
        $this->client->setParameterPost('firstNameKana', $c->getFirstNameKana());						// 名カナ
        $this->client->setParameterPost('tel', $c->getTel());											// 電話番号
        $this->client->setParameterPost('fax', $c->getFax());											// FAX番号
        $this->client->setParameterPost('zip', $c->getZip());											// 郵便番号
        $this->client->setParameterPost('prefectureName', $c->getPrefecture());							// 都道府県
        $this->client->setParameterPost('address1', $c->getAddress1());									// 住所１
        $this->client->setParameterPost('address2', $c->getAddress2());									// 住所２
        $this->client->setParameterPost('scenarioId', $scenarioId);										// シナリオID
        $this->client->setParameterPost('productId', $productId);										// 商品ID
//		$this->client->setParameterPost('customerResponseId', $customerResponseId);						// 顧客反応履歴ID
        $this->client->setParameterPost('categoryId', $categoryId);										// カテゴリID

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

    /**
     * DRMへ顧客シナリオの解除を行います。
     *
     * @param Popo_MstCustomer $customer 顧客情報
     * @param int $scenarioId シナリオID
     * @return string
     *
     * @author k.shimowaki
     * @since version - 2010/03/18
     */
    public function cancelCustomerScenario(Popo_MstCustomer $customer, $scenarioId) {

        $c = new Popo_MstCustomer();
        $c = $customer;

        // パラメータセット
        $this->client->setParameterPost('pathCode','SCENARIO_CANCEL');									// パスコード

        $this->client->setParameterPost('email', $c->getEmail());										// メールアドレス
        $this->client->setParameterPost('scenarioId', $scenarioId);										// シナリオID

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

    /**
     * DRMへ顧客シナリオの解除戻しを行います。
     *
     * @param Popo_MstCustomer $customer 顧客情報
     * @param int $scenarioId シナリオID
     * @return string
     *
     * @author kawakami
     * @since version - 2011/03/22
     */
    public function updateCustomerScenarioRecovery(Popo_MstCustomer $customer, $scenarioId) {

        $c = new Popo_MstCustomer();
        $c = $customer;

        // パラメータセット
        $this->client->setParameterPost('pathCode','SCENARIO_CANCEL_RECOVERY');                         // パスコード

        $this->client->setParameterPost('email', $c->getEmail());                                       // メールアドレス
        $this->client->setParameterPost('scenarioId', $scenarioId);                                     // シナリオID

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

//   /**
//     * 顧客情報を更新します。
//     * 渡された値がNullの場合更新対象外とします。
//     * 空文字の場合は空文字で更新を行います。
//     *
//     * Popo_TrxDmStepShipping
//     *   string address1
//     *   [string address2] (option)
//     *   [string lastName] (option)
//     *   [string firstName] (option)
//     *   [string lastNameKana] (option)
//     *   [string firstNameKana] (option)
//     *   [string tel] (option)
//     *   [string fax] (option)
//     *   [string zip] (option)
//     *   [string prefectureName] (option)
//     *   string DrmUserId
//     *   string DrmCustomerId
//     *
//     * @param Popo_TrxDmStepShipping $dmStepShipping
//     * @return string
//     *
//     * @author kawakami
//     * @since 2010/03/23
//     */
//    public function updaterCustomerInformation(Popo_TrxDmStepShipping $dmStepShipping) {
//
//        $dm = new Popo_TrxDmStepShipping();
//        $dm = $dmStepShipping;
//
//        // パラメータセット
//        $params = array();
//        $params['lastName']       = $dm->getLastName();                     // 姓
//        $params['firstName']      = $dm->getFirstName();                    // 名
//        $params['lastNameKana']   = $dm->getLastNameKana();                 // 姓カナ
//        $params['firstNameKana']  = $dm->getFirstNameKana();                // 名カナ
//        $params['tel']            = $dm->getTel();                          // 電話番号
//        $params['fax']            = $dm->getFax();                          // FAX番号
//        $params['zip']            = $dm->getZip();                          // 郵便番号
//        $params['prefectureName'] = $dm->getPrefecture();                   // 都道府県
//        $params['address1']       = $dm->getAddress1();                     // 住所1
//        $params['address2']       = $dm->getAddress2();                     // 住所2
//        $params['address3']       = "";                                     // 住所3
//        $params['address4']       = "";                                     // 住所4
//
//        $params['userId']         = $dm->getDrmUserId();                    // DRMのUserId
//        $params['customerId']     = $dm->getDrmCustomerId();                // DRMのCUstomerId
//
//        // Zend_Http_Clientにパラメーターとしてセットします
//        foreach ($params as $key => $value) {
//            if (! is_null($value)) {
//                $this->client->setParameterPost($key, $value);
//            }
//        }
//
//        // パスコード
//        $this->client->setParameterPost('pathCode','CUSTOMER_INFORMATION_UPDATE');
//
//        $response = null;
//        // 実行
//        try{
//
//            logDebug($this->client->getParameterRequest());
//            logDebug($this->client->getUri());
//
//            // 実行
//            $response = $this->client->request('POST');
//        } catch (Exception $e) {
//
//            // エラー処理
//            $this->error($e);
//            return $this->retFormat($response);
//        }
//        logDebug($this->client->getLastRequest());
//        logDebug($this->client->getLastResponse());
//
//        return $this->retFormat($response->getBody());
//
//    }

   /**
     * DRMユーザーマスタに紐付くDRM顧客情報を更新します。
     * 渡された値がNullの場合更新対象外とします。
     * 空文字の場合は空文字で更新を行います。
     *
     * @param Popo_MstCustomer $customer
     * @return string
     *
     * @author sekiya
     * @since 2012/06/21
     */
    public function updaterCustomerInformation(Popo_MstCustomer $beforeCustomer, Popo_MstCustomer $afterCustomer,$updateType = 'ALL') {

        //-----------------------------------------------
        // DRMユーザーマスタ （ 複数件 ) 取得
        //-----------------------------------------------
        $db = null;
        $db = new Mikoshiva_Db_Simple();

        // 検索条件
        $where = array();
        $where['deleteFlag'] = '0'; // 削除フラグ

        // SQL実行＋複数件取得
        $drmUserList = array();
        $drmUserList = $db->simpleManySelect('Mikoshiva_Db_Ar_MstDrmUser', $where );

        // ユーザーIDリスト
        $userIdList = array();

        // 結果をループ
        foreach ($drmUserList as $k => $drmUser) {
            $userIdList[] = $drmUser->getUserId(); // ユーザーID
        }

        // ユーザーIDリストをCSV形式文字列に変換
        $userIdCsv = '';
        $userIdCsv = implode(',',$userIdList);

        // パスコード
        $this->client->setParameterPost('pathCode','CUSTOMER_INFORMATION_UPDATE');

        // パラメータセット
        $params = array();
        $params['updateType']        = $updateType;                        // アップデートタイプ（ALL/EMAIL/ADDRESS）
        $params['userIdCsv']         = $userIdCsv;                         // DRMユーザーID（CSV形式）
        $params['beforeUpdateEmail'] = $beforeCustomer->getEmail();        // 更新前メールアドレス（update文の更新キーとして使用する）
        $params['email']             = $afterCustomer->getEmail();         // 更新後メールアドレス
        $params['lastName']          = $afterCustomer->getLastName();      // 姓
        $params['firstName']         = $afterCustomer->getFirstName();     // 名
        $params['lastNameKana']      = $afterCustomer->getLastNameKana();  // 姓カナ
        $params['firstNameKana']     = $afterCustomer->getFirstNameKana(); // 名カナ
        $params['zip']               = $afterCustomer->getZip();           // 郵便番号
        $params['prefecture']        = $afterCustomer->getPrefecture();    // 都道府県
        $params['address1']          = $afterCustomer->getAddress1();      // 市区町村番地
        $params['address2']          = $afterCustomer->getAddress2();      // ビル・マンション名
        $params['tel']               = $afterCustomer->getTel();           // 電話番号
        $params['dmNgFlag']          = $afterCustomer->getDmNgFlag();      // DMNGフラグ
        $params['_SERVER_MODE_ID_']  = _SERVER_MODE_ID_;                   // サーバーモードID

        // Zend_Http_Clientにパラメーターとしてセットします
        foreach ($params as $key => $value) {
            if (! is_null($value)) {
                $this->client->setParameterPost($key, $value);
            }
        }

        $response = null;
        // 実行
        try{
            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

   /**
     * DMNG処理を行います。
     *
     * >>必須項目
     * Popo_TrxDmStepShipping
     *   string DrmUserId
     *   string DrmCustomerId
     *
     * @param Popo_TrxDmStepShipping $dmStepShipping
     * @return string
     *
     * @author kawakami
     * @since 2010/03/23
     */
    public function updateCustomerDirectMailNoGood(Popo_TrxDmStepShipping $dmStepShipping) {

        // popo準備
        $dm = new Popo_TrxDmStepShipping();
        $dm = $dmStepShipping;

        // パラメータセット
        $this->client->setParameterPost('pathCode','CUSTOMER_DIRECT_MAIL_NO_GOOD_UPDATE');              // パスコード

        $this->client->setParameterPost('userId', $dm->getDrmUserId());                                 // DRMのUserId
        $this->client->setParameterPost('customerId', $dm->getDrmCustomerId());                         // DRMのCUstomerId

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

    /**
     * DRMの商品購入済み状態を解除します。
     *
     * @param Popo_MstCustomer $customer 顧客情報
     * @param int $productId 商品ID
     *
     * @author kawakami
     * @since 2010/10/29
     */
    public function cancelProductSales(Popo_MstCustomer $c, $productId) {

        // パラメータセット
        $this->client->setParameterPost('pathCode','PRODUCT_SALES_CANCEL');                             // パスコード

        $this->client->setParameterPost('productId', $productId);                                       // 商品ID
        $this->client->setParameterPost('email', $c->getEmail());                                       // DRMのCUstomerId

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

    /**
     * シナリオ情報を取得します
     *
     * @param int $scenarioId シナリオID
     * @return string
     *
     * @author fujimoto
     * @since version - 2011/09/22
     */
    public function scenarioInfo($scenarioId) {

        // パラメータセット
        $this->client->setParameterPost('pathCode','SCENARIO_INFO');                         // パスコード
        $this->client->setParameterPost('scenarioId', $scenarioId);                          // シナリオID

        $response = null;
        // 実行
        try{

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return $this->retFormat($response);
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());
    }

}