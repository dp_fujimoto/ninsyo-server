<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/String.php';
require_once 'Zend/Registry.php';
require_once 'Zend/View/Interface.php';

/**
 *
 * @package Mikoshiva_View
 * @author
 * @since 2010/01/27
 * @version SVN:$Id: F5Former.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_F5Former extends Mikoshiva_View_Helper_Abstract {


    public $view;

  /**
     * 変数情報を格納します
     *
     * @var String
     */
    private $_retStr = '';

    /**
     * ダンプを行い、変数情報を文字列で返します
     * オブジェクトは未対応
     *
     * @param $input String|Array ダンプを行いたい文字列または配列
     * @param $name String ダンプした際の接頭辞
     * @return string 変数の情報
     */
    public function f5Former()
    {

        // タブ ID が無ければリターン
        if (!isset($this->view->tabId) || $this->view->tabId === '') return;


        $input      = Zend_Registry::get('request')->getParams();
        $module     = $input['module'];
        $controller = $input['controller'];
        $action     = $input['action'];
        $key        = Mikoshiva_Utility_String::createRandomString(8);

        $tag  = '';
        $tag .= '<form style="display: inline;" action="" name="___f5Former_' . $input['tabId'] . '" id="___f5Former_' . $key . '">' . "\n";

        // 実行
        $this->_proccess($input);

        // 取得
        $retStr = $this->_retStr;

        // 初期化
   //     $this->_retStr = '';

        $tag .= $retStr;
        $tag .= '<input type="button" value="' . '/' . $module . '/' . $controller . '/' . $action . '" onclick="' . $this->view->currentTab('/' . $module . '/' . $controller . '/' . $action, $this->view->tabId, '___f5Former_' . $key) . '">';
        //$tag .= $this->view->currentTab('test', /' . $module . '/' . $controller . '/' . $action, $this->view->tabId, '___f5Former_' . $input['tabId']);
        $tag .= '</form>';

        return $tag;
    }

    /**
     * 変数情報のダンプを作成します
     *
     * @param $input String|Array ダンプを行いたい文字列または配列
     * @param $name String ダンプした際の接頭辞
     * @param $i
     * @param $k
     * @return void
     */
    private function _proccess( $input, $i = '0', $k = array('Error') )
    {
        if (is_object($input)) return;

        if( is_array( $input ) )
        {
            foreach( $input as $i => $value )
            {
                $temp = $k;
                $temp[] = $i;
                $this->_proccess( $value, $i, $temp );
            }
        }
        else
        {
            $__name = '';

            foreach( $k as $i => $value)
            {
                if($value !== 'Error') {

                    if( $__name !== '') {
                      $__name .= '[' . $value . ']';
                    } else {
                        $__name .= $value;
                    }
                }
            }
            $this->_retStr .= '<input type="hidden" name="' . $__name . '" value="' . $input . '">' . "\n";
        }
    }



    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }


}