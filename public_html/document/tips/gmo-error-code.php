<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - エラーコード・エラー詳細コードからエラーメッセージ（内容）を返す</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="エラーコード・エラー詳細コードからエラーメッセージ（内容）を返す">エラーコード・エラー詳細コードからエラーメッセージ（内容）を返す</h1>

            <p>
              エラーコード・エラー詳細コードからエラーメッセージ（内容）を返す。<br>
              ※取得出来なければ空文字を変えします。
            </p>



            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <pre class="wiki"><?php
highlight_string('<?php


include_once \'Mikoshiva/Utility/Gmo.php\';
dumper(Mikoshiva_Utility_Gmo::getErrorMessage(\'E01\', \'E01010001\')); // 【ショップ ID が指定されていません。】が返ります

'); ?>
            </pre>




            <h2 id="Mikoshiva_Utility_Gmo::getErrorMessage">Mikoshiva_Utility_Gmo::getErrorMessage</h2>
            <h3>Method</h3>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$errorCode</td>
                <td>string</td>
                <td><br></td>
                <td>エラーコード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>$errorCodeDetail</td>
                <td>string</td>
                <td><br></td>
                <td>エラー詳細コード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>string</td>
                <td><br></td>
                <td>エラーメッセージ（内容）</td>
                <td>※取得出来なければ空文字を変えします。</td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





















