<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product_package_external_parameter
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProductPackageExternalParameter extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mpe';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productPackageExternalParameterId' => null,
        'productPackageId' => null,
        'externalParameterId' => null,
        'creditCardFlag' => null,
        'paymentDeliveryFlag' => null,
        'bankTransferFlag' => null,
        'valueNumber' => null,
        'value1' => null,
        'value2' => null,
        'value3' => null,
        'value4' => null,
        'value5' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 商品パッケージ外部連携ID の値を返します
	 *
     * @return int 商品パッケージ外部連携ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductPackageExternalParameterId() {
		return $this->_properties['productPackageExternalParameterId'];
    }


    /**
     * 商品パッケージ外部連携ID の値をセットします
	 *
	 * @param int $productPackageExternalParameterId 商品パッケージ外部連携ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductPackageExternalParameterId($productPackageExternalParameterId) {
		$this->_properties['productPackageExternalParameterId'] = $productPackageExternalParameterId;
    }


    /**
     * 商品パッケージID の値を返します
	 *
     * @return int 商品パッケージID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductPackageId() {
		return $this->_properties['productPackageId'];
    }


    /**
     * 商品パッケージID の値をセットします
	 *
	 * @param int $productPackageId 商品パッケージID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductPackageId($productPackageId) {
		$this->_properties['productPackageId'] = $productPackageId;
    }


    /**
     * 外部連携パラメータID の値を返します
	 *
     * @return int 外部連携パラメータID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExternalParameterId() {
		return $this->_properties['externalParameterId'];
    }


    /**
     * 外部連携パラメータID の値をセットします
	 *
	 * @param int $externalParameterId 外部連携パラメータID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExternalParameterId($externalParameterId) {
		$this->_properties['externalParameterId'] = $externalParameterId;
    }


    /**
     * 適用対象カード決済フラグ の値を返します
	 *
     * @return char 適用対象カード決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCreditCardFlag() {
		return $this->_properties['creditCardFlag'];
    }


    /**
     * 適用対象カード決済フラグ の値をセットします
	 *
	 * @param char $creditCardFlag 適用対象カード決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCreditCardFlag($creditCardFlag) {
		$this->_properties['creditCardFlag'] = $creditCardFlag;
    }


    /**
     * 適用対象代金引換フラグ の値を返します
	 *
     * @return char 適用対象代金引換フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryFlag() {
		return $this->_properties['paymentDeliveryFlag'];
    }


    /**
     * 適用対象代金引換フラグ の値をセットします
	 *
	 * @param char $paymentDeliveryFlag 適用対象代金引換フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryFlag($paymentDeliveryFlag) {
		$this->_properties['paymentDeliveryFlag'] = $paymentDeliveryFlag;
    }


    /**
     * 適用対象銀行振込フラグ の値を返します
	 *
     * @return char 適用対象銀行振込フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBankTransferFlag() {
		return $this->_properties['bankTransferFlag'];
    }


    /**
     * 適用対象銀行振込フラグ の値をセットします
	 *
	 * @param char $bankTransferFlag 適用対象銀行振込フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBankTransferFlag($bankTransferFlag) {
		$this->_properties['bankTransferFlag'] = $bankTransferFlag;
    }


    /**
     * 有効値保持数 の値を返します
	 *
     * @return int 有効値保持数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValueNumber() {
		return $this->_properties['valueNumber'];
    }


    /**
     * 有効値保持数 の値をセットします
	 *
	 * @param int $valueNumber 有効値保持数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValueNumber($valueNumber) {
		$this->_properties['valueNumber'] = $valueNumber;
    }


    /**
     * 値１ の値を返します
	 *
     * @return varchar 値１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue1() {
		return $this->_properties['value1'];
    }


    /**
     * 値１ の値をセットします
	 *
	 * @param varchar $value1 値１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue1($value1) {
		$this->_properties['value1'] = $value1;
    }


    /**
     * 値２ の値を返します
	 *
     * @return varchar 値２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue2() {
		return $this->_properties['value2'];
    }


    /**
     * 値２ の値をセットします
	 *
	 * @param varchar $value2 値２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue2($value2) {
		$this->_properties['value2'] = $value2;
    }


    /**
     * 値３ の値を返します
	 *
     * @return varchar 値３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue3() {
		return $this->_properties['value3'];
    }


    /**
     * 値３ の値をセットします
	 *
	 * @param varchar $value3 値３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue3($value3) {
		$this->_properties['value3'] = $value3;
    }


    /**
     * 値４ の値を返します
	 *
     * @return varchar 値４
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue4() {
		return $this->_properties['value4'];
    }


    /**
     * 値４ の値をセットします
	 *
	 * @param varchar $value4 値４
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue4($value4) {
		$this->_properties['value4'] = $value4;
    }


    /**
     * 値５ の値を返します
	 *
     * @return varchar 値５
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue5() {
		return $this->_properties['value5'];
    }


    /**
     * 値５ の値をセットします
	 *
	 * @param varchar $value5 値５
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue5($value5) {
		$this->_properties['value5'] = $value5;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























