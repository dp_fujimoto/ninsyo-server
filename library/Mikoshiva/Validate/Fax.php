<?php
/**
 * @package Mikoshiva_Validate
 */



/**
 * Fax番号を検証します
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/28
 * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Fax extends Zend_Validate_Abstract {



    const STRING_EMPTY = 'stringEmpty';
    const INVALID      = 'noKana';


    /**
     * Digits filter used for validation
     *
     * @var Zend_Filter_Digits
     */
    protected static $_filter = null;


    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::STRING_EMPTY => "【%COLUMN_NAME%】 は必須項目です。",
        self::INVALID      => "【%COLUMN_NAME%】 の形式が正しく有りません。",
    );


    /**
     * Fax番号を保持したオブジェクト を保持します
     * @var Mikoshiva_Property_Fax Fax番号を保持したオブジェクト を保持します
     */
    protected $_fax;

    /**
     * fax番号市外局番 を保持します
     * @var string fax番号市外局番 を保持します
     */
    protected $_fax2Name;

    /**
     * fax番号契約者番号 を保持します
     * @var string fax番号契約者番号 を保持します
     */
    protected $_fax3Name;

    /**
     * リクエストデータ を保持します
     * @var array リクエストデータ を保持します
     */
    protected $_data;


    /**
     * Fax番号を保持したオブジェクト をセットします
     *
     * @param Mikoshiva_Property_Fax $fax Fax番号を保持したオブジェクト
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFax($fax) {
        logInfo(LOG_START);
        if(!isBlankOrNull($this->_fax2Name) || !isBlankOrNull($this->_fax3Name)) {
            $data = array();
            $data = $this->getData();
            if(isBlankOrNull($this->_fax2Name)) {
                throw new Mikoshiva_Validate_Exception('fax2のnameが空です');
            }
            if(isBlankOrNull($this->_fax3Name)) {
                throw new Mikoshiva_Validate_Exception('fax3のnameが空です');
            }
            $fax2 = '';
            $fax2 = $data[$this->_fax2Name];
            $fax3 = '';
            $fax3 = $data[$this->_fax3Name];
            $this->_fax = $fax . '-' . $fax2 . '-' . $fax3;

        } else {
            $this->_fax = $fax;
        }
        logInfo(LOG_END);
    }

    /**
     * fax番号市外局番 をセットします
     *
     * @param string $fax2Name fax番号市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFax2Name($fax2Name) {
        logInfo(LOG_START);
        $this->_fax2Name = $fax2Name;
        logInfo(LOG_END);
    }

    /**
     * fax番号契約者番号 をセットします
     *
     * @param string $fax3Name fax番号契約者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFax3Name($fax3Name) {
        logInfo(LOG_START);
        $this->_fax3Name = $fax3Name;
        logInfo(LOG_END);
    }

    /**
     * リクエストデータ をセットします
     *
     * @param array $data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {
        logInfo(LOG_START);
        $this->_data = $data;
        logInfo(LOG_END);
    }


    /**
     * Fax番号を保持したオブジェクト を返します
     *
     * @return Mikoshiva_Property_Fax $fax Fax番号を保持したオブジェクト
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFax() {
        logInfo(LOG_START);
        return $this->_fax;
        logInfo(LOG_END);
    }

    /**
     * fax番号市外局番 を返します
     *
     * @return string $this->_fax2Name fax番号市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFax2Name() {
        logInfo(LOG_START);
        return $this->_fax2Name;
        logInfo(LOG_END);
    }

    /**
     * fax番号契約者番号 を返します
     *
     * @return string $this->_fax3Name fax番号契約者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFax3Name() {
        logInfo(LOG_START);
        return $this->_fax3Name;
        logInfo(LOG_END);
    }

    /**
     * リクエストデータ を返します
     *
     * @return array $this->_data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getData() {
        logInfo(LOG_START);
        return $this->_data;
        logInfo(LOG_END);
    }

    public function __construct($fax2Name = '', $fax3Name = '') {
        $this->setFax2Name($fax2Name);
        $this->setFax3Name($fax3Name);
    }

    /**
     * Fax番号チェック
     *
     * @param string|array $fax Fax番号
     * @return boolean
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Interface#isValid($value)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function isValid($fax) {
        logInfo(LOG_START);
        $this->setFax($fax);

        $faxNo = $this->getFax();

        // 長さチェック
        if(strlen($faxNo) !== 12) {
            $this->_error(self::INVALID);

            logInfo(LOG_END);
            return false;

        }

        // 形式チェック
        if (preg_match('#^[0-9]{2,4}-[0-9]{2,4}-[0-9]{4}$#', $faxNo) !== 1) {
            $this->_error(self::INVALID);
            logInfo(LOG_END);
            return false;
        }

        logInfo(LOG_END);
        return true;
    }

}
