<?php
/**
 * WPAPI用基底クラス
 * WPAPIにアクセスする部品は必ずこのクラスを継承します。
 *
 * @author k.shimowaki
 * @since version - 2010/03/19
 */
abstract class Mikoshiva_Plugin_Wp_Abstruct extends Mikoshiva_Plugin_Abstruct {

}