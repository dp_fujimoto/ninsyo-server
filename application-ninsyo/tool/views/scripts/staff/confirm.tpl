<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">{if !isBlankOrNull($form->getStaffId())}スタッフ情報変更　確認{else}スタッフ情報登録　確認{/if}</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                {hiddenLister hiddenList=$form}

                <!-- // スタッフマスタ情報入力テーブル開始 -->
                <table class="COM_table COM_tableWidthLong">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ID
                        </td>
                        <td>{if !isBlankOrNull($form->getStaffId())}{$form->getStaffId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            部署<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            {dbWrite arClassName='Mikoshiva_Db_NinsyoAr_MstDivision' pkey=$form->getDivisionId() column=array('divisionName')}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            氏名<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>カナ：</td>
                                    <td>
                                        {$form->getLastNameKana()}　{$form->getFirstNameKana()}
                                    </td>
                                </tr>
                                <tr>
                                    <td>漢字：</td>
                                    <td>
                                        {$form->getLastName()}　{$form->getFirstName()}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            メールアドレス<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            {$form->getEmail()}
                        </td>
                    </tr>
                    {if $form->getLvFlag() === '1'}
                        {if isBlankOrNull($form->getStaffId()) || $form->getSpFlag() === '1'}
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                パスワード<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td>
                                {$form->getLoginPassword()}
                            </td>
                        </tr>
                        {/if}
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            状態<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            {if $form->getValidFlag() === '1'}有効{/if}
                            {if $form->getValidFlag() === '0'}無効{/if}
                            <br>
                            {if $form->getDeleteFlag() === '0'}削除しない{/if}
                            {if $form->getDeleteFlag() === '1'}削除する{/if}
                        </td>
                    </tr>
                    {/if}
                </table>
                <!-- // 商品マスタ編集フォーム終了 -->

                <br>
                <hr>
                <br>

                <!-- // 戻るボタン -->
                {currentTabButton name='　　　　　　　戻る　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/staff/confirm-back' tabId=$tabId  formId='inputForm'}
                　　　
                {if !isBlankOrNull($form->getStaffId())}
                    <!-- // 更新ボタン -->
                    {currentTabButton name='　　　　　　　更新　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/staff/complete' tabId=$tabId  formId='inputForm'}
                {else}
                    <!-- // 登録ボタン -->
                    {currentTabButton name='　　　　　　　登録　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/staff/complete' tabId=$tabId  formId='inputForm'}
                {/if}

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>
</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->