<?php
/**
 * @package    Mikoshiva_Format
 */



/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';


/**
 * 
 * 
 * @category   Mikoshiva
 * @package    Mikoshiva_Format
 * @copyright  Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Mikoshiva_Format_Exception extends Zend_Exception
{}
