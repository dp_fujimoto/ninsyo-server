<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_shipping
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxShipping extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tsh';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'shippingId' => null,
        'customerId' => null,
        'directionId' => null,
        'directionStatusId' => null,
        'chargeId' => null,
        'divisionId' => null,
        'relativeShippingId' => null,
        'campaignSalesId' => null,
        'campaignId' => null,
        'productShippingId' => null,
        'productPackageId' => null,
        'packageNumber' => null,
        'packageType' => null,
        'chargeType' => null,
        'stepType' => null,
        'stepCount' => null,
        'shippingGroupLabel' => null,
        'entityShippingFlag' => null,
        'companyName' => null,
        'lastName' => null,
        'firstName' => null,
        'lastNameKana' => null,
        'firstNameKana' => null,
        'zip' => null,
        'prefecture' => null,
        'address1' => null,
        'address2' => null,
        'tel' => null,
        'shippingType' => null,
        'expectedShippingDate' => null,
        'paymentDeliveryPrice' => null,
        'shippingAssign' => null,
        'builderId1' => null,
        'builderId2' => null,
        'builderId3' => null,
        'builderId4' => null,
        'builderId5' => null,
        'shippingStatus' => null,
        'shippingStopStatus' => null,
        'supportStatus' => null,
        'returnStatus' => null,
        'returnSupportStatus' => null,
        'returnReasonType' => null,
        'returnRequestDate' => null,
        'returnCompleteDate' => null,
        'returnLimitDate' => null,
        'responaseLimitDate' => null,
        'cancelAfterShippingDate' => null,
        'realShippingDate' => null,
        'dataReceivedDate' => null,
        'inquiryNumber' => null,
        'invoiceId' => null,
        'packagingDirectionId' => null,
        'packagingDirectionContentsId' => null,
        'shippingSlipCreateFlag' => null,
        'refundPossibilityDate' => null,
        'shippingCompleteFlag' => null,
        'dummyFlag' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 発送ID の値を返します
	 *
     * @return int 発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * 発送ID の値をセットします
	 *
	 * @param int $shippingId 発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 指示ID の値を返します
	 *
     * @return int 指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * 指示ID の値をセットします
	 *
	 * @param int $directionId 指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * 請求ID の値を返します
	 *
     * @return int 請求ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * 請求ID の値をセットします
	 *
	 * @param int $chargeId 請求ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 関連発送ID の値を返します
	 *
     * @return int 関連発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRelativeShippingId() {
		return $this->_properties['relativeShippingId'];
    }


    /**
     * 関連発送ID の値をセットします
	 *
	 * @param int $relativeShippingId 関連発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRelativeShippingId($relativeShippingId) {
		$this->_properties['relativeShippingId'] = $relativeShippingId;
    }


    /**
     * キャンペーンセールスID の値を返します
	 *
     * @return int キャンペーンセールスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesId() {
		return $this->_properties['campaignSalesId'];
    }


    /**
     * キャンペーンセールスID の値をセットします
	 *
	 * @param int $campaignSalesId キャンペーンセールスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesId($campaignSalesId) {
		$this->_properties['campaignSalesId'] = $campaignSalesId;
    }


    /**
     * キャンペーンID の値を返します
	 *
     * @return int キャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignId() {
		return $this->_properties['campaignId'];
    }


    /**
     * キャンペーンID の値をセットします
	 *
	 * @param int $campaignId キャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignId($campaignId) {
		$this->_properties['campaignId'] = $campaignId;
    }


    /**
     * 商品発送ID の値を返します
	 *
     * @return int 商品発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingId() {
		return $this->_properties['productShippingId'];
    }


    /**
     * 商品発送ID の値をセットします
	 *
	 * @param int $productShippingId 商品発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingId($productShippingId) {
		$this->_properties['productShippingId'] = $productShippingId;
    }


    /**
     * 商品パッケージID の値を返します
	 *
     * @return int 商品パッケージID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductPackageId() {
		return $this->_properties['productPackageId'];
    }


    /**
     * 商品パッケージID の値をセットします
	 *
	 * @param int $productPackageId 商品パッケージID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductPackageId($productPackageId) {
		$this->_properties['productPackageId'] = $productPackageId;
    }


    /**
     * パッケージナンバー の値を返します
	 *
     * @return int パッケージナンバー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageNumber() {
		return $this->_properties['packageNumber'];
    }


    /**
     * パッケージナンバー の値をセットします
	 *
	 * @param int $packageNumber パッケージナンバー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageNumber($packageNumber) {
		$this->_properties['packageNumber'] = $packageNumber;
    }


    /**
     * パッケージタイプ の値を返します
	 *
     * @return varchar パッケージタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageType() {
		return $this->_properties['packageType'];
    }


    /**
     * パッケージタイプ の値をセットします
	 *
	 * @param varchar $packageType パッケージタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageType($packageType) {
		$this->_properties['packageType'] = $packageType;
    }


    /**
     * 請求タイプ の値を返します
	 *
     * @return varchar 請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * 請求タイプ の値をセットします
	 *
	 * @param varchar $chargeType 請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * 適用ステップタイプ の値を返します
	 *
     * @return varchar 適用ステップタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepType() {
		return $this->_properties['stepType'];
    }


    /**
     * 適用ステップタイプ の値をセットします
	 *
	 * @param varchar $stepType 適用ステップタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepType($stepType) {
		$this->_properties['stepType'] = $stepType;
    }


    /**
     * 適用ステップカウント の値を返します
	 *
     * @return int 適用ステップカウント
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepCount() {
		return $this->_properties['stepCount'];
    }


    /**
     * 適用ステップカウント の値をセットします
	 *
	 * @param int $stepCount 適用ステップカウント
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepCount($stepCount) {
		$this->_properties['stepCount'] = $stepCount;
    }


    /**
     * 発送グループラベル の値を返します
	 *
     * @return varchar 発送グループラベル
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingGroupLabel() {
		return $this->_properties['shippingGroupLabel'];
    }


    /**
     * 発送グループラベル の値をセットします
	 *
	 * @param varchar $shippingGroupLabel 発送グループラベル
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingGroupLabel($shippingGroupLabel) {
		$this->_properties['shippingGroupLabel'] = $shippingGroupLabel;
    }


    /**
     * 実発送フラグ の値を返します
	 *
     * @return char 実発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEntityShippingFlag() {
		return $this->_properties['entityShippingFlag'];
    }


    /**
     * 実発送フラグ の値をセットします
	 *
	 * @param char $entityShippingFlag 実発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEntityShippingFlag($entityShippingFlag) {
		$this->_properties['entityShippingFlag'] = $entityShippingFlag;
    }


    /**
     * 発送先会社名 の値を返します
	 *
     * @return varchar 発送先会社名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompanyName() {
		return $this->_properties['companyName'];
    }


    /**
     * 発送先会社名 の値をセットします
	 *
	 * @param varchar $companyName 発送先会社名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompanyName($companyName) {
		$this->_properties['companyName'] = $companyName;
    }


    /**
     * 発送先お名前　姓 の値を返します
	 *
     * @return varchar 発送先お名前　姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastName() {
		return $this->_properties['lastName'];
    }


    /**
     * 発送先お名前　姓 の値をセットします
	 *
	 * @param varchar $lastName 発送先お名前　姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastName($lastName) {
		$this->_properties['lastName'] = $lastName;
    }


    /**
     * 発送先お名前　名 の値を返します
	 *
     * @return varchar 発送先お名前　名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstName() {
		return $this->_properties['firstName'];
    }


    /**
     * 発送先お名前　名 の値をセットします
	 *
	 * @param varchar $firstName 発送先お名前　名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstName($firstName) {
		$this->_properties['firstName'] = $firstName;
    }


    /**
     * 発送先お名前（カナ）　姓 の値を返します
	 *
     * @return varchar 発送先お名前（カナ）　姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastNameKana() {
		return $this->_properties['lastNameKana'];
    }


    /**
     * 発送先お名前（カナ）　姓 の値をセットします
	 *
	 * @param varchar $lastNameKana 発送先お名前（カナ）　姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastNameKana($lastNameKana) {
		$this->_properties['lastNameKana'] = $lastNameKana;
    }


    /**
     * 発送先お名前（カナ）　名 の値を返します
	 *
     * @return varchar 発送先お名前（カナ）　名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstNameKana() {
		return $this->_properties['firstNameKana'];
    }


    /**
     * 発送先お名前（カナ）　名 の値をセットします
	 *
	 * @param varchar $firstNameKana 発送先お名前（カナ）　名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstNameKana($firstNameKana) {
		$this->_properties['firstNameKana'] = $firstNameKana;
    }


    /**
     * 発送先郵便番号 の値を返します
	 *
     * @return char 発送先郵便番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZip() {
		return $this->_properties['zip'];
    }


    /**
     * 発送先郵便番号 の値をセットします
	 *
	 * @param char $zip 発送先郵便番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZip($zip) {
		$this->_properties['zip'] = $zip;
    }


    /**
     * 発送先都道府県 の値を返します
	 *
     * @return varchar 発送先都道府県
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefecture() {
		return $this->_properties['prefecture'];
    }


    /**
     * 発送先都道府県 の値をセットします
	 *
	 * @param varchar $prefecture 発送先都道府県
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefecture($prefecture) {
		$this->_properties['prefecture'] = $prefecture;
    }


    /**
     * 発送先住所１ の値を返します
	 *
     * @return varchar 発送先住所１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * 発送先住所１ の値をセットします
	 *
	 * @param varchar $address1 発送先住所１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * 発送先住所２ の値を返します
	 *
     * @return varchar 発送先住所２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * 発送先住所２ の値をセットします
	 *
	 * @param varchar $address2 発送先住所２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


    /**
     * 発送先電話番号 の値を返します
	 *
     * @return varchar 発送先電話番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTel() {
		return $this->_properties['tel'];
    }


    /**
     * 発送先電話番号 の値をセットします
	 *
	 * @param varchar $tel 発送先電話番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTel($tel) {
		$this->_properties['tel'] = $tel;
    }


    /**
     * 発送タイプ の値を返します
	 *
     * @return varchar 発送タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingType() {
		return $this->_properties['shippingType'];
    }


    /**
     * 発送タイプ の値をセットします
	 *
	 * @param varchar $shippingType 発送タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingType($shippingType) {
		$this->_properties['shippingType'] = $shippingType;
    }


    /**
     * 発送予定日 の値を返します
	 *
     * @return date 発送予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExpectedShippingDate() {
		return $this->_properties['expectedShippingDate'];
    }


    /**
     * 発送予定日 の値をセットします
	 *
	 * @param date $expectedShippingDate 発送予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExpectedShippingDate($expectedShippingDate) {
		$this->_properties['expectedShippingDate'] = $expectedShippingDate;
    }


    /**
     * 代引き金額 の値を返します
	 *
     * @return int 代引き金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryPrice() {
		return $this->_properties['paymentDeliveryPrice'];
    }


    /**
     * 代引き金額 の値をセットします
	 *
	 * @param int $paymentDeliveryPrice 代引き金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryPrice($paymentDeliveryPrice) {
		$this->_properties['paymentDeliveryPrice'] = $paymentDeliveryPrice;
    }


    /**
     * 発送指示 の値を返します
	 *
     * @return varchar 発送指示
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAssign() {
		return $this->_properties['shippingAssign'];
    }


    /**
     * 発送指示 の値をセットします
	 *
	 * @param varchar $shippingAssign 発送指示
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAssign($shippingAssign) {
		$this->_properties['shippingAssign'] = $shippingAssign;
    }


    /**
     * 部材１ の値を返します
	 *
     * @return int 部材１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId1() {
		return $this->_properties['builderId1'];
    }


    /**
     * 部材１ の値をセットします
	 *
	 * @param int $builderId1 部材１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId1($builderId1) {
		$this->_properties['builderId1'] = $builderId1;
    }


    /**
     * 部材２ の値を返します
	 *
     * @return int 部材２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId2() {
		return $this->_properties['builderId2'];
    }


    /**
     * 部材２ の値をセットします
	 *
	 * @param int $builderId2 部材２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId2($builderId2) {
		$this->_properties['builderId2'] = $builderId2;
    }


    /**
     * 部材３ の値を返します
	 *
     * @return int 部材３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId3() {
		return $this->_properties['builderId3'];
    }


    /**
     * 部材３ の値をセットします
	 *
	 * @param int $builderId3 部材３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId3($builderId3) {
		$this->_properties['builderId3'] = $builderId3;
    }


    /**
     * 部材４ の値を返します
	 *
     * @return int 部材４
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId4() {
		return $this->_properties['builderId4'];
    }


    /**
     * 部材４ の値をセットします
	 *
	 * @param int $builderId4 部材４
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId4($builderId4) {
		$this->_properties['builderId4'] = $builderId4;
    }


    /**
     * 部材５ の値を返します
	 *
     * @return int 部材５
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId5() {
		return $this->_properties['builderId5'];
    }


    /**
     * 部材５ の値をセットします
	 *
	 * @param int $builderId5 部材５
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId5($builderId5) {
		$this->_properties['builderId5'] = $builderId5;
    }


    /**
     * 発送ステータス の値を返します
	 *
     * @return varchar 発送ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStatus() {
		return $this->_properties['shippingStatus'];
    }


    /**
     * 発送ステータス の値をセットします
	 *
	 * @param varchar $shippingStatus 発送ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStatus($shippingStatus) {
		$this->_properties['shippingStatus'] = $shippingStatus;
    }


    /**
     * 発送停止ステータス の値を返します
	 *
     * @return varchar 発送停止ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStopStatus() {
		return $this->_properties['shippingStopStatus'];
    }


    /**
     * 発送停止ステータス の値をセットします
	 *
	 * @param varchar $shippingStopStatus 発送停止ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStopStatus($shippingStopStatus) {
		$this->_properties['shippingStopStatus'] = $shippingStopStatus;
    }


    /**
     * 対応ステータス の値を返します
	 *
     * @return varchar 対応ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSupportStatus() {
		return $this->_properties['supportStatus'];
    }


    /**
     * 対応ステータス の値をセットします
	 *
	 * @param varchar $supportStatus 対応ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSupportStatus($supportStatus) {
		$this->_properties['supportStatus'] = $supportStatus;
    }


    /**
     * 返品ステータス の値を返します
	 *
     * @return varchar 返品ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnStatus() {
		return $this->_properties['returnStatus'];
    }


    /**
     * 返品ステータス の値をセットします
	 *
	 * @param varchar $returnStatus 返品ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnStatus($returnStatus) {
		$this->_properties['returnStatus'] = $returnStatus;
    }


    /**
     * 返品対応ステータス の値を返します
	 *
     * @return varchar 返品対応ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnSupportStatus() {
		return $this->_properties['returnSupportStatus'];
    }


    /**
     * 返品対応ステータス の値をセットします
	 *
	 * @param varchar $returnSupportStatus 返品対応ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnSupportStatus($returnSupportStatus) {
		$this->_properties['returnSupportStatus'] = $returnSupportStatus;
    }


    /**
     * 返品理由 の値を返します
	 *
     * @return varchar 返品理由
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnReasonType() {
		return $this->_properties['returnReasonType'];
    }


    /**
     * 返品理由 の値をセットします
	 *
	 * @param varchar $returnReasonType 返品理由
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnReasonType($returnReasonType) {
		$this->_properties['returnReasonType'] = $returnReasonType;
    }


    /**
     * 返品依頼日 の値を返します
	 *
     * @return date 返品依頼日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnRequestDate() {
		return $this->_properties['returnRequestDate'];
    }


    /**
     * 返品依頼日 の値をセットします
	 *
	 * @param date $returnRequestDate 返品依頼日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnRequestDate($returnRequestDate) {
		$this->_properties['returnRequestDate'] = $returnRequestDate;
    }


    /**
     * 返品完了日 の値を返します
	 *
     * @return date 返品完了日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnCompleteDate() {
		return $this->_properties['returnCompleteDate'];
    }


    /**
     * 返品完了日 の値をセットします
	 *
	 * @param date $returnCompleteDate 返品完了日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnCompleteDate($returnCompleteDate) {
		$this->_properties['returnCompleteDate'] = $returnCompleteDate;
    }


    /**
     * 返品期限 の値を返します
	 *
     * @return date 返品期限
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReturnLimitDate() {
		return $this->_properties['returnLimitDate'];
    }


    /**
     * 返品期限 の値をセットします
	 *
	 * @param date $returnLimitDate 返品期限
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReturnLimitDate($returnLimitDate) {
		$this->_properties['returnLimitDate'] = $returnLimitDate;
    }


    /**
     * 返答期限 の値を返します
	 *
     * @return date 返答期限
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResponaseLimitDate() {
		return $this->_properties['responaseLimitDate'];
    }


    /**
     * 返答期限 の値をセットします
	 *
	 * @param date $responaseLimitDate 返答期限
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResponaseLimitDate($responaseLimitDate) {
		$this->_properties['responaseLimitDate'] = $responaseLimitDate;
    }


    /**
     * 発送後取消日時 の値を返します
	 *
     * @return datetime 発送後取消日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelAfterShippingDate() {
		return $this->_properties['cancelAfterShippingDate'];
    }


    /**
     * 発送後取消日時 の値をセットします
	 *
	 * @param datetime $cancelAfterShippingDate 発送後取消日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelAfterShippingDate($cancelAfterShippingDate) {
		$this->_properties['cancelAfterShippingDate'] = $cancelAfterShippingDate;
    }


    /**
     * 実発送日 の値を返します
	 *
     * @return date 実発送日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealShippingDate() {
		return $this->_properties['realShippingDate'];
    }


    /**
     * 実発送日 の値をセットします
	 *
	 * @param date $realShippingDate 実発送日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealShippingDate($realShippingDate) {
		$this->_properties['realShippingDate'] = $realShippingDate;
    }


    /**
     * tsh_data_received_date の値を返します
	 *
     * @return date tsh_data_received_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDataReceivedDate() {
		return $this->_properties['dataReceivedDate'];
    }


    /**
     * tsh_data_received_date の値をセットします
	 *
	 * @param date $dataReceivedDate tsh_data_received_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDataReceivedDate($dataReceivedDate) {
		$this->_properties['dataReceivedDate'] = $dataReceivedDate;
    }


    /**
     * 問合せ番号 の値を返します
	 *
     * @return varchar 問合せ番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryNumber() {
		return $this->_properties['inquiryNumber'];
    }


    /**
     * 問合せ番号 の値をセットします
	 *
	 * @param varchar $inquiryNumber 問合せ番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryNumber($inquiryNumber) {
		$this->_properties['inquiryNumber'] = $inquiryNumber;
    }


    /**
     * 送り状ID の値を返します
	 *
     * @return int 送り状ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInvoiceId() {
		return $this->_properties['invoiceId'];
    }


    /**
     * 送り状ID の値をセットします
	 *
	 * @param int $invoiceId 送り状ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInvoiceId($invoiceId) {
		$this->_properties['invoiceId'] = $invoiceId;
    }


    /**
     * 梱包指示ID の値を返します
	 *
     * @return int 梱包指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionId() {
		return $this->_properties['packagingDirectionId'];
    }


    /**
     * 梱包指示ID の値をセットします
	 *
	 * @param int $packagingDirectionId 梱包指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionId($packagingDirectionId) {
		$this->_properties['packagingDirectionId'] = $packagingDirectionId;
    }


    /**
     * 梱包指示内容ID の値を返します
	 *
     * @return int 梱包指示内容ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionContentsId() {
		return $this->_properties['packagingDirectionContentsId'];
    }


    /**
     * 梱包指示内容ID の値をセットします
	 *
	 * @param int $packagingDirectionContentsId 梱包指示内容ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionContentsId($packagingDirectionContentsId) {
		$this->_properties['packagingDirectionContentsId'] = $packagingDirectionContentsId;
    }


    /**
     * 納品書作成フラグ の値を返します
	 *
     * @return char 納品書作成フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingSlipCreateFlag() {
		return $this->_properties['shippingSlipCreateFlag'];
    }


    /**
     * 納品書作成フラグ の値をセットします
	 *
	 * @param char $shippingSlipCreateFlag 納品書作成フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingSlipCreateFlag($shippingSlipCreateFlag) {
		$this->_properties['shippingSlipCreateFlag'] = $shippingSlipCreateFlag;
    }


    /**
     * 返金可能期日 の値を返します
	 *
     * @return date 返金可能期日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundPossibilityDate() {
		return $this->_properties['refundPossibilityDate'];
    }


    /**
     * 返金可能期日 の値をセットします
	 *
	 * @param date $refundPossibilityDate 返金可能期日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundPossibilityDate($refundPossibilityDate) {
		$this->_properties['refundPossibilityDate'] = $refundPossibilityDate;
    }


    /**
     * 発送完了フラグ の値を返します
	 *
     * @return char 発送完了フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingCompleteFlag() {
		return $this->_properties['shippingCompleteFlag'];
    }


    /**
     * 発送完了フラグ の値をセットします
	 *
	 * @param char $shippingCompleteFlag 発送完了フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingCompleteFlag($shippingCompleteFlag) {
		$this->_properties['shippingCompleteFlag'] = $shippingCompleteFlag;
    }


    /**
     * ダミーフラグ の値を返します
	 *
     * @return char ダミーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDummyFlag() {
		return $this->_properties['dummyFlag'];
    }


    /**
     * ダミーフラグ の値をセットします
	 *
	 * @param char $dummyFlag ダミーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDummyFlag($dummyFlag) {
		$this->_properties['dummyFlag'] = $dummyFlag;
    }


    /**
     * 欠陥フラグ の値を返します
	 *
     * @return char 欠陥フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * 欠陥フラグ の値をセットします
	 *
	 * @param char $imperfectionFlag 欠陥フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























