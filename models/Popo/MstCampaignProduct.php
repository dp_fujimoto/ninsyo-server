<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_campaign_product
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCampaignProduct extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mcp';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'campaignProductId' => null,
        'divisionId' => null,
        'campaignProductName' => null,
        'campaignId' => null,
        'productShippingId' => null,
        'campaignProductShippingId' => null,
        'dummyCampaignProductId' => null,
        'executionWritingFlag' => null,
        'executionWritingId' => null,
        'depositFlag' => null,
        'depositSettlementFlag' => null,
        'depositCampaignProductId' => null,
        'productOfferedId' => null,
        'claimAmount' => null,
        'optionSalesFlag' => null,
        'salesTypeNumber' => null,
        'paymentType' => null,
        'suddenlyYearType' => null,
        'price' => null,
        'subscription' => null,
        'subscriptionType' => null,
        'subscriptionPeriod' => null,
        'endlessFlag' => null,
        'midtermCancellationFlag' => null,
        'creditCardFlag' => null,
        'paymentDeliveryFlag' => null,
        'bankTransferFlag' => null,
        'creditCardInstallmentFlag' => null,
        'directInstallmentFlag' => null,
        'directInstallmentNumber' => null,
        'directInstallmentPrice' => null,
        'provisorySalesFlag' => null,
        'freePeriodCreditCard' => null,
        'freePeriodTypeCreditCard' => null,
        'freePeriodPaymentDelivery' => null,
        'freePeriodTypePaymentDelivery' => null,
        'freePeriodBankTransfer' => null,
        'freePeriodTypeBankTransfer' => null,
        'guaranteePeriod' => null,
        'guaranteePeriodType' => null,
        'refundWithReturnsFlag' => null,
        'shippingCost' => null,
        'continuityShippingCost' => null,
        'paymentDeliveryFee' => null,
        'continuityPaymentDeliveryFee' => null,
        'shippingStartDate' => null,
        'salesNumber' => null,
        'duplicationCheckFlag' => null,
        'shippingAtOrderFlag' => null,
        'thanksMailHeader' => null,
        'thanksMailFooter' => null,
        'salesFlag' => null,
        'cancelMemo' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * キャンペーン商品ID の値を返します
	 *
     * @return int キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductId() {
		return $this->_properties['campaignProductId'];
    }


    /**
     * キャンペーン商品ID の値をセットします
	 *
	 * @param int $campaignProductId キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductId($campaignProductId) {
		$this->_properties['campaignProductId'] = $campaignProductId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * キャンペーン商品名 の値を返します
	 *
     * @return varchar キャンペーン商品名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductName() {
		return $this->_properties['campaignProductName'];
    }


    /**
     * キャンペーン商品名 の値をセットします
	 *
	 * @param varchar $campaignProductName キャンペーン商品名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductName($campaignProductName) {
		$this->_properties['campaignProductName'] = $campaignProductName;
    }


    /**
     * キャンペーンID の値を返します
	 *
     * @return int キャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignId() {
		return $this->_properties['campaignId'];
    }


    /**
     * キャンペーンID の値をセットします
	 *
	 * @param int $campaignId キャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignId($campaignId) {
		$this->_properties['campaignId'] = $campaignId;
    }


    /**
     * 商品発送ID の値を返します
	 *
     * @return int 商品発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingId() {
		return $this->_properties['productShippingId'];
    }


    /**
     * 商品発送ID の値をセットします
	 *
	 * @param int $productShippingId 商品発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingId($productShippingId) {
		$this->_properties['productShippingId'] = $productShippingId;
    }


    /**
     * キャンペーン商品発送ID の値を返します
	 *
     * @return int キャンペーン商品発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductShippingId() {
		return $this->_properties['campaignProductShippingId'];
    }


    /**
     * キャンペーン商品発送ID の値をセットします
	 *
	 * @param int $campaignProductShippingId キャンペーン商品発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductShippingId($campaignProductShippingId) {
		$this->_properties['campaignProductShippingId'] = $campaignProductShippingId;
    }


    /**
     * ダミーキャンペーン商品ID の値を返します
	 *
     * @return int ダミーキャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDummyCampaignProductId() {
		return $this->_properties['dummyCampaignProductId'];
    }


    /**
     * ダミーキャンペーン商品ID の値をセットします
	 *
	 * @param int $dummyCampaignProductId ダミーキャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDummyCampaignProductId($dummyCampaignProductId) {
		$this->_properties['dummyCampaignProductId'] = $dummyCampaignProductId;
    }


    /**
     * 締結書面フラグ の値を返します
	 *
     * @return char 締結書面フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionWritingFlag() {
		return $this->_properties['executionWritingFlag'];
    }


    /**
     * 締結書面フラグ の値をセットします
	 *
	 * @param char $executionWritingFlag 締結書面フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionWritingFlag($executionWritingFlag) {
		$this->_properties['executionWritingFlag'] = $executionWritingFlag;
    }


    /**
     * 締結書面ID の値を返します
	 *
     * @return int 締結書面ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionWritingId() {
		return $this->_properties['executionWritingId'];
    }


    /**
     * 締結書面ID の値をセットします
	 *
	 * @param int $executionWritingId 締結書面ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionWritingId($executionWritingId) {
		$this->_properties['executionWritingId'] = $executionWritingId;
    }


    /**
     * mcp_deposit_flag の値を返します
	 *
     * @return char mcp_deposit_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDepositFlag() {
		return $this->_properties['depositFlag'];
    }


    /**
     * mcp_deposit_flag の値をセットします
	 *
	 * @param char $depositFlag mcp_deposit_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDepositFlag($depositFlag) {
		$this->_properties['depositFlag'] = $depositFlag;
    }


    /**
     * mcp_deposit_settlement_flag の値を返します
	 *
     * @return char mcp_deposit_settlement_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDepositSettlementFlag() {
		return $this->_properties['depositSettlementFlag'];
    }


    /**
     * mcp_deposit_settlement_flag の値をセットします
	 *
	 * @param char $depositSettlementFlag mcp_deposit_settlement_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDepositSettlementFlag($depositSettlementFlag) {
		$this->_properties['depositSettlementFlag'] = $depositSettlementFlag;
    }


    /**
     * mcp_deposit_campaign_product_id の値を返します
	 *
     * @return int mcp_deposit_campaign_product_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDepositCampaignProductId() {
		return $this->_properties['depositCampaignProductId'];
    }


    /**
     * mcp_deposit_campaign_product_id の値をセットします
	 *
	 * @param int $depositCampaignProductId mcp_deposit_campaign_product_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDepositCampaignProductId($depositCampaignProductId) {
		$this->_properties['depositCampaignProductId'] = $depositCampaignProductId;
    }


    /**
     * mcp_product_offered_id の値を返します
	 *
     * @return int mcp_product_offered_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedId() {
		return $this->_properties['productOfferedId'];
    }


    /**
     * mcp_product_offered_id の値をセットします
	 *
	 * @param int $productOfferedId mcp_product_offered_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedId($productOfferedId) {
		$this->_properties['productOfferedId'] = $productOfferedId;
    }


    /**
     * mcp_claim_amount の値を返します
	 *
     * @return int mcp_claim_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClaimAmount() {
		return $this->_properties['claimAmount'];
    }


    /**
     * mcp_claim_amount の値をセットします
	 *
	 * @param int $claimAmount mcp_claim_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClaimAmount($claimAmount) {
		$this->_properties['claimAmount'] = $claimAmount;
    }


    /**
     * オプション販売フラグ の値を返します
	 *
     * @return char オプション販売フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOptionSalesFlag() {
		return $this->_properties['optionSalesFlag'];
    }


    /**
     * オプション販売フラグ の値をセットします
	 *
	 * @param char $optionSalesFlag オプション販売フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOptionSalesFlag($optionSalesFlag) {
		$this->_properties['optionSalesFlag'] = $optionSalesFlag;
    }


    /**
     * 販売タイプ番号 の値を返します
	 *
     * @return int 販売タイプ番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesTypeNumber() {
		return $this->_properties['salesTypeNumber'];
    }


    /**
     * 販売タイプ番号 の値をセットします
	 *
	 * @param int $salesTypeNumber 販売タイプ番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesTypeNumber($salesTypeNumber) {
		$this->_properties['salesTypeNumber'] = $salesTypeNumber;
    }


    /**
     * 支払いタイプ の値を返します
	 *
     * @return varchar 支払いタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentType() {
		return $this->_properties['paymentType'];
    }


    /**
     * 支払いタイプ の値をセットします
	 *
	 * @param varchar $paymentType 支払いタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentType($paymentType) {
		$this->_properties['paymentType'] = $paymentType;
    }


    /**
     * いきなり年間タイプ の値を返します
	 *
     * @return varchar いきなり年間タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSuddenlyYearType() {
		return $this->_properties['suddenlyYearType'];
    }


    /**
     * いきなり年間タイプ の値をセットします
	 *
	 * @param varchar $suddenlyYearType いきなり年間タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSuddenlyYearType($suddenlyYearType) {
		$this->_properties['suddenlyYearType'] = $suddenlyYearType;
    }


    /**
     * 初回価格 の値を返します
	 *
     * @return int 初回価格
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrice() {
		return $this->_properties['price'];
    }


    /**
     * 初回価格 の値をセットします
	 *
	 * @param int $price 初回価格
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrice($price) {
		$this->_properties['price'] = $price;
    }


    /**
     * 購読価格 の値を返します
	 *
     * @return int 購読価格
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscription() {
		return $this->_properties['subscription'];
    }


    /**
     * 購読価格 の値をセットします
	 *
	 * @param int $subscription 購読価格
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscription($subscription) {
		$this->_properties['subscription'] = $subscription;
    }


    /**
     * 購読タイプ の値を返します
	 *
     * @return varchar 購読タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionType() {
		return $this->_properties['subscriptionType'];
    }


    /**
     * 購読タイプ の値をセットします
	 *
	 * @param varchar $subscriptionType 購読タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionType($subscriptionType) {
		$this->_properties['subscriptionType'] = $subscriptionType;
    }


    /**
     * 一括購読期間 の値を返します
	 *
     * @return int 一括購読期間
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionPeriod() {
		return $this->_properties['subscriptionPeriod'];
    }


    /**
     * 一括購読期間 の値をセットします
	 *
	 * @param int $subscriptionPeriod 一括購読期間
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionPeriod($subscriptionPeriod) {
		$this->_properties['subscriptionPeriod'] = $subscriptionPeriod;
    }


    /**
     * 永久フラグ の値を返します
	 *
     * @return char 永久フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEndlessFlag() {
		return $this->_properties['endlessFlag'];
    }


    /**
     * 永久フラグ の値をセットします
	 *
	 * @param char $endlessFlag 永久フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEndlessFlag($endlessFlag) {
		$this->_properties['endlessFlag'] = $endlessFlag;
    }


    /**
     * 中途解約フラグ の値を返します
	 *
     * @return char 中途解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMidtermCancellationFlag() {
		return $this->_properties['midtermCancellationFlag'];
    }


    /**
     * 中途解約フラグ の値をセットします
	 *
	 * @param char $midtermCancellationFlag 中途解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMidtermCancellationFlag($midtermCancellationFlag) {
		$this->_properties['midtermCancellationFlag'] = $midtermCancellationFlag;
    }


    /**
     * クレジットカード決済フラグ の値を返します
	 *
     * @return char クレジットカード決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCreditCardFlag() {
		return $this->_properties['creditCardFlag'];
    }


    /**
     * クレジットカード決済フラグ の値をセットします
	 *
	 * @param char $creditCardFlag クレジットカード決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCreditCardFlag($creditCardFlag) {
		$this->_properties['creditCardFlag'] = $creditCardFlag;
    }


    /**
     * 代金引換決済フラグ の値を返します
	 *
     * @return char 代金引換決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryFlag() {
		return $this->_properties['paymentDeliveryFlag'];
    }


    /**
     * 代金引換決済フラグ の値をセットします
	 *
	 * @param char $paymentDeliveryFlag 代金引換決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryFlag($paymentDeliveryFlag) {
		$this->_properties['paymentDeliveryFlag'] = $paymentDeliveryFlag;
    }


    /**
     * 銀行振込決済フラグ の値を返します
	 *
     * @return char 銀行振込決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBankTransferFlag() {
		return $this->_properties['bankTransferFlag'];
    }


    /**
     * 銀行振込決済フラグ の値をセットします
	 *
	 * @param char $bankTransferFlag 銀行振込決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBankTransferFlag($bankTransferFlag) {
		$this->_properties['bankTransferFlag'] = $bankTransferFlag;
    }


    /**
     * クレジットカード分割決済フラグ の値を返します
	 *
     * @return char クレジットカード分割決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCreditCardInstallmentFlag() {
		return $this->_properties['creditCardInstallmentFlag'];
    }


    /**
     * クレジットカード分割決済フラグ の値をセットします
	 *
	 * @param char $creditCardInstallmentFlag クレジットカード分割決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCreditCardInstallmentFlag($creditCardInstallmentFlag) {
		$this->_properties['creditCardInstallmentFlag'] = $creditCardInstallmentFlag;
    }


    /**
     * ダイレクト分割フラグ の値を返します
	 *
     * @return char ダイレクト分割フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentFlag() {
		return $this->_properties['directInstallmentFlag'];
    }


    /**
     * ダイレクト分割フラグ の値をセットします
	 *
	 * @param char $directInstallmentFlag ダイレクト分割フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentFlag($directInstallmentFlag) {
		$this->_properties['directInstallmentFlag'] = $directInstallmentFlag;
    }


    /**
     * ダイレクト分割回数 の値を返します
	 *
     * @return int ダイレクト分割回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentNumber() {
		return $this->_properties['directInstallmentNumber'];
    }


    /**
     * ダイレクト分割回数 の値をセットします
	 *
	 * @param int $directInstallmentNumber ダイレクト分割回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentNumber($directInstallmentNumber) {
		$this->_properties['directInstallmentNumber'] = $directInstallmentNumber;
    }


    /**
     * ダイレクト分割初回金額 の値を返します
	 *
     * @return int ダイレクト分割初回金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPrice() {
		return $this->_properties['directInstallmentPrice'];
    }


    /**
     * ダイレクト分割初回金額 の値をセットします
	 *
	 * @param int $directInstallmentPrice ダイレクト分割初回金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPrice($directInstallmentPrice) {
		$this->_properties['directInstallmentPrice'] = $directInstallmentPrice;
    }


    /**
     * 仮売上フラグ の値を返します
	 *
     * @return char 仮売上フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesFlag() {
		return $this->_properties['provisorySalesFlag'];
    }


    /**
     * 仮売上フラグ の値をセットします
	 *
	 * @param char $provisorySalesFlag 仮売上フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesFlag($provisorySalesFlag) {
		$this->_properties['provisorySalesFlag'] = $provisorySalesFlag;
    }


    /**
     * 無料お試し期間(カード) の値を返します
	 *
     * @return int 無料お試し期間(カード)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodCreditCard() {
		return $this->_properties['freePeriodCreditCard'];
    }


    /**
     * 無料お試し期間(カード) の値をセットします
	 *
	 * @param int $freePeriodCreditCard 無料お試し期間(カード)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodCreditCard($freePeriodCreditCard) {
		$this->_properties['freePeriodCreditCard'] = $freePeriodCreditCard;
    }


    /**
     * 無料お試し期間タイプ(カード) の値を返します
	 *
     * @return varchar 無料お試し期間タイプ(カード)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodTypeCreditCard() {
		return $this->_properties['freePeriodTypeCreditCard'];
    }


    /**
     * 無料お試し期間タイプ(カード) の値をセットします
	 *
	 * @param varchar $freePeriodTypeCreditCard 無料お試し期間タイプ(カード)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodTypeCreditCard($freePeriodTypeCreditCard) {
		$this->_properties['freePeriodTypeCreditCard'] = $freePeriodTypeCreditCard;
    }


    /**
     * 無料お試し期間(代引) の値を返します
	 *
     * @return int 無料お試し期間(代引)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodPaymentDelivery() {
		return $this->_properties['freePeriodPaymentDelivery'];
    }


    /**
     * 無料お試し期間(代引) の値をセットします
	 *
	 * @param int $freePeriodPaymentDelivery 無料お試し期間(代引)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodPaymentDelivery($freePeriodPaymentDelivery) {
		$this->_properties['freePeriodPaymentDelivery'] = $freePeriodPaymentDelivery;
    }


    /**
     * 無料お試し期間タイプ(代引) の値を返します
	 *
     * @return varchar 無料お試し期間タイプ(代引)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodTypePaymentDelivery() {
		return $this->_properties['freePeriodTypePaymentDelivery'];
    }


    /**
     * 無料お試し期間タイプ(代引) の値をセットします
	 *
	 * @param varchar $freePeriodTypePaymentDelivery 無料お試し期間タイプ(代引)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodTypePaymentDelivery($freePeriodTypePaymentDelivery) {
		$this->_properties['freePeriodTypePaymentDelivery'] = $freePeriodTypePaymentDelivery;
    }


    /**
     * 無料お試し期間(銀行振込) の値を返します
	 *
     * @return int 無料お試し期間(銀行振込)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodBankTransfer() {
		return $this->_properties['freePeriodBankTransfer'];
    }


    /**
     * 無料お試し期間(銀行振込) の値をセットします
	 *
	 * @param int $freePeriodBankTransfer 無料お試し期間(銀行振込)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodBankTransfer($freePeriodBankTransfer) {
		$this->_properties['freePeriodBankTransfer'] = $freePeriodBankTransfer;
    }


    /**
     * 無料お試し期間タイプ(銀行振込) の値を返します
	 *
     * @return varchar 無料お試し期間タイプ(銀行振込)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodTypeBankTransfer() {
		return $this->_properties['freePeriodTypeBankTransfer'];
    }


    /**
     * 無料お試し期間タイプ(銀行振込) の値をセットします
	 *
	 * @param varchar $freePeriodTypeBankTransfer 無料お試し期間タイプ(銀行振込)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodTypeBankTransfer($freePeriodTypeBankTransfer) {
		$this->_properties['freePeriodTypeBankTransfer'] = $freePeriodTypeBankTransfer;
    }


    /**
     * 保証期間 の値を返します
	 *
     * @return int 保証期間
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriod() {
		return $this->_properties['guaranteePeriod'];
    }


    /**
     * 保証期間 の値をセットします
	 *
	 * @param int $guaranteePeriod 保証期間
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriod($guaranteePeriod) {
		$this->_properties['guaranteePeriod'] = $guaranteePeriod;
    }


    /**
     * 保証期間タイプ の値を返します
	 *
     * @return varchar 保証期間タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriodType() {
		return $this->_properties['guaranteePeriodType'];
    }


    /**
     * 保証期間タイプ の値をセットします
	 *
	 * @param varchar $guaranteePeriodType 保証期間タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriodType($guaranteePeriodType) {
		$this->_properties['guaranteePeriodType'] = $guaranteePeriodType;
    }


    /**
     * 返金要返品フラグ の値を返します
	 *
     * @return char 返金要返品フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundWithReturnsFlag() {
		return $this->_properties['refundWithReturnsFlag'];
    }


    /**
     * 返金要返品フラグ の値をセットします
	 *
	 * @param char $refundWithReturnsFlag 返金要返品フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundWithReturnsFlag($refundWithReturnsFlag) {
		$this->_properties['refundWithReturnsFlag'] = $refundWithReturnsFlag;
    }


    /**
     * 送料 の値を返します
	 *
     * @return int 送料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingCost() {
		return $this->_properties['shippingCost'];
    }


    /**
     * 送料 の値をセットします
	 *
	 * @param int $shippingCost 送料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingCost($shippingCost) {
		$this->_properties['shippingCost'] = $shippingCost;
    }


    /**
     * 継続送料 の値を返します
	 *
     * @return int 継続送料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityShippingCost() {
		return $this->_properties['continuityShippingCost'];
    }


    /**
     * 継続送料 の値をセットします
	 *
	 * @param int $continuityShippingCost 継続送料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityShippingCost($continuityShippingCost) {
		$this->_properties['continuityShippingCost'] = $continuityShippingCost;
    }


    /**
     * 代引き手数料 の値を返します
	 *
     * @return int 代引き手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryFee() {
		return $this->_properties['paymentDeliveryFee'];
    }


    /**
     * 代引き手数料 の値をセットします
	 *
	 * @param int $paymentDeliveryFee 代引き手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryFee($paymentDeliveryFee) {
		$this->_properties['paymentDeliveryFee'] = $paymentDeliveryFee;
    }


    /**
     * 継続代引き手数料 の値を返します
	 *
     * @return int 継続代引き手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityPaymentDeliveryFee() {
		return $this->_properties['continuityPaymentDeliveryFee'];
    }


    /**
     * 継続代引き手数料 の値をセットします
	 *
	 * @param int $continuityPaymentDeliveryFee 継続代引き手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityPaymentDeliveryFee($continuityPaymentDeliveryFee) {
		$this->_properties['continuityPaymentDeliveryFee'] = $continuityPaymentDeliveryFee;
    }


    /**
     * 発送開始日 の値を返します
	 *
     * @return date 発送開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStartDate() {
		return $this->_properties['shippingStartDate'];
    }


    /**
     * 発送開始日 の値をセットします
	 *
	 * @param date $shippingStartDate 発送開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStartDate($shippingStartDate) {
		$this->_properties['shippingStartDate'] = $shippingStartDate;
    }


    /**
     * 販売個数 の値を返します
	 *
     * @return int 販売個数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesNumber() {
		return $this->_properties['salesNumber'];
    }


    /**
     * 販売個数 の値をセットします
	 *
	 * @param int $salesNumber 販売個数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesNumber($salesNumber) {
		$this->_properties['salesNumber'] = $salesNumber;
    }


    /**
     * 重複購入チェックフラグ の値を返します
	 *
     * @return char 重複購入チェックフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDuplicationCheckFlag() {
		return $this->_properties['duplicationCheckFlag'];
    }


    /**
     * 重複購入チェックフラグ の値をセットします
	 *
	 * @param char $duplicationCheckFlag 重複購入チェックフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDuplicationCheckFlag($duplicationCheckFlag) {
		$this->_properties['duplicationCheckFlag'] = $duplicationCheckFlag;
    }


    /**
     * 購入時発送フラグ の値を返します
	 *
     * @return char 購入時発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAtOrderFlag() {
		return $this->_properties['shippingAtOrderFlag'];
    }


    /**
     * 購入時発送フラグ の値をセットします
	 *
	 * @param char $shippingAtOrderFlag 購入時発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAtOrderFlag($shippingAtOrderFlag) {
		$this->_properties['shippingAtOrderFlag'] = $shippingAtOrderFlag;
    }


    /**
     * サンキューメールヘッダ の値を返します
	 *
     * @return text サンキューメールヘッダ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getThanksMailHeader() {
		return $this->_properties['thanksMailHeader'];
    }


    /**
     * サンキューメールヘッダ の値をセットします
	 *
	 * @param text $thanksMailHeader サンキューメールヘッダ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setThanksMailHeader($thanksMailHeader) {
		$this->_properties['thanksMailHeader'] = $thanksMailHeader;
    }


    /**
     * サンキューメールフッダ の値を返します
	 *
     * @return text サンキューメールフッダ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getThanksMailFooter() {
		return $this->_properties['thanksMailFooter'];
    }


    /**
     * サンキューメールフッダ の値をセットします
	 *
	 * @param text $thanksMailFooter サンキューメールフッダ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setThanksMailFooter($thanksMailFooter) {
		$this->_properties['thanksMailFooter'] = $thanksMailFooter;
    }


    /**
     * 販売フラグ の値を返します
	 *
     * @return char 販売フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesFlag() {
		return $this->_properties['salesFlag'];
    }


    /**
     * 販売フラグ の値をセットします
	 *
	 * @param char $salesFlag 販売フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesFlag($salesFlag) {
		$this->_properties['salesFlag'] = $salesFlag;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelMemo() {
		return $this->_properties['cancelMemo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $cancelMemo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelMemo($cancelMemo) {
		$this->_properties['cancelMemo'] = $cancelMemo;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























