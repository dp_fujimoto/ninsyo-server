<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Date - 指定日時を取得する</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="日付の比較">日付の比較</h1>


            <!-- ========================================
              ■日付・日時が等しいかチェックする
            ==========================================-->
            <h2 id="日付・日時が等しいかチェックする">日付・日時が等しいかチェックする</h2>

            <p>右と同義 if (2010/03/11 === 2010/03/11) </p>

            <pre class="wiki">
<?php
highlight_string('<?php

// 日付が等しいかチェックする
$date = new Mikoshiva_Date(\'2010/03/11\');
dumper($date->equals(\'2010/03/10\'));  // false
dumper($date->equals(\'2010/03/11\'));  // true
dumper($date->equals(\'2010/03/12\'));  // false

// 日時が等しいかチェックする
$date = new Mikoshiva_Date(\'2010/03/11 11:12:13\');
dumper($date->equals(\'2010/03/11 10:12:13\')); // false
dumper($date->equals(\'2010/03/11 11:12:13\')); // true
dumper($date->equals(\'2010/03/11 12:12:13\')); // false

');
?>
            </pre>


            <!-- ========================================
              ■指定された日付・日時より未来であるかをチェックする
            ==========================================-->
            <h2 id="指定された日付・日時より未来であるかをチェックする">指定された日付・日時より未来であるかをチェックする</h2>

            <p>右と同義 if (2010/03/12 > 2010/03/11) </p>

            <pre class="wiki">
<?php
highlight_string('<?php

// 未来の日付であるかをチェックする
$date = new Mikoshiva_Date(\'2010/03/11\');
dumper($date->isEarlier(\'2010/03/10\'));  // false
dumper($date->isEarlier(\'2010/03/11\'));  // false
dumper($date->isEarlier(\'2010/03/12\'));  // true

// 未来の日時であるかをチェックする
$date = new Mikoshiva_Date(\'2010/03/11 11:12:13\');
dumper($date->isEarlier(\'2010/03/11 10:12:13\')); // false
dumper($date->isEarlier(\'2010/03/11 11:12:13\')); // false
dumper($date->isEarlier(\'2010/03/11 12:12:13\')); // true

');
?>
            </pre>


            <!-- ========================================
              ■指定された日付・日時より過去であるかをチェックする
            ==========================================-->
            <h2 id="指定された日付・日時より過去であるかをチェックする">指定された日付・日時より過去であるかをチェックする</h2>

            <p>右と同義 if (2010/03/10 < 2010/03/11) </p>

            <pre class="wiki">
<?php
highlight_string('<?php

// 過去の日付であるかをチェックする
$date = new Mikoshiva_Date(\'2010/03/11\');
dumper($date->isLater(\'2010/03/10\'));  // true
dumper($date->isLater(\'2010/03/11\'));  // false
dumper($date->isLater(\'2010/03/12\'));  // false

// 過去の日時であるかをチェックする
$date = new Mikoshiva_Date(\'2010/03/11 11:12:13\');
dumper($date->isLater(\'2010/03/11 10:12:13\')); // true
dumper($date->isLater(\'2010/03/11 11:12:13\')); // false
dumper($date->isLater(\'2010/03/11 12:12:13\')); // false

');
?>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















