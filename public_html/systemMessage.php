<?php


require_once dirname(__FILE__) . '/../configs/server-mode-config.php';                // サーバーモードを取得（必ず一番はじめに読み込む）
require_once dirname(__FILE__) . '/../configs/' . _SERVER_MODE_ID_ . '/test-define.php';

if  (TV_MIKOSHIVA_NOW !== null) {
    echo '<a href="" class="COM_fontBlue COM_bold">' . strtoupper(_SERVER_MODE_ID_) . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . date('Y-m-d', TV_MIKOSHIVA_NOW) . ' <span style="color: #bb0000;"></span>';
} else {
    echo '<a href="" class="COM_fontBlue COM_bold">' . strtoupper(_SERVER_MODE_ID_) . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . date('Y-m-d') . ' <span style="color: #bb0000;"></span>';
}