<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


/**
 * 与えられた配列から hidden タグを書き出します。
 *
 * @param array $params
 *                      Mikoshiva_Controller_Mapping_Form_Interface hiddenList アクションフォーム
 *                      array   deleteList array() | 削除する値
 *                      array   attribs    array() | selectタグに指定する追加の属性 '属性名' => 値
 * @param Smarty   $smarty
 * @param Template $template
 *
 * @return string
 *
 * @package Smarty_libs
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.hiddenLister.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_hiddenLister($params, &$smarty, &$template) {


    // ログスタート
    Mikoshiva_Logger::info(LOG_START);


    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['hiddenList']   = null;
    $default['deleteList']   = array();
    $default['attribs']      = array();

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if (! ($data['hiddenList'] instanceof Mikoshiva_Controller_Mapping_Form_Interface)) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 hiddenList は Mikoshiva_Controller_Mapping_Form_Interface を実装したクラスでは有りません。');
    }
    if(!is_array($data['deleteList'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 deleteList は配列でなければなりません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    /** @var $hiddenList Mikoshiva_Controller_Mapping_Form_Interface */
    $hiddenList = $data['hiddenList'];
    $deleteList = $data['deleteList'];
    $attribs    = $data['attribs'];
//    $helper = $smarty->tpl_vars['h']->value;//Zendのヘルパーを呼び出す為、ビューのインスタンスを取得



    //-------------------------------------------------------------------------------------------
    //■deleteList から指定されたキーを除外
    //-------------------------------------------------------------------------------------------
    $pList = array();
    $pList = $hiddenList->toArrayProperty();
    foreach ($deleteList as $k => $v) {
        if (array_key_exists($v, $pList)) {
            unset($pList[$v]);
        }
    }




    $newHiddenList = '';
    $cc            = array();
    $cc            = Smarty_Function_HiddenLister__ArrayRecursive::exec($pList, 'name');
    //                                                    ↓これだと、「$ccの末尾から<smarty_function_hiddenLister>に含まれる文字を検索して削除」の意になるので、
    //                                                      ikumaなら u m a が削除される。 i も条件に合致するが、直前の k で検索が終了する為削除されない。
    //$uu = explode('<smarty_function_hiddenLister>', rtrim($cc, '<smarty_function_hiddenLister>'));

    $uu = explode('<smarty_function_hiddenLister>', preg_replace('/<smarty_function_hiddenLister>$/', '', $cc));
    $tt = array();
    foreach($uu as $k => $v) {
        $tt = '';
        $tt = preg_replace('#^name\[(.*?)\](.*?)$#s', '$1$2', $v);
        list($_name, $_value) = explode('hidden--hidden--hidden', $tt);
        $newHiddenList .= smarty_function_hiddenLister__formHidden($_name, $_value, $attribs) . "\n";
    }



    return $newHiddenList;
}



function smarty_function_hiddenLister__formHidden($name, $value = null, $attribs = null) {
    $id = '';
    $id = str_replace('[', '-', $name);
    $id = str_replace(']', '', $id);
    $attribsStr = '';
    foreach($attribs as $key => $value) {
        $attribsStr = ' ' . $key . '="' . $value . '"';
    }
    return '<input type="hidden"'
         . ' name="' . htmlspecialchars($name, ENT_QUOTES) . '"'
         //. ' id="' . $id . '"'
         . ' value="' . $value . '"'
         . $attribsStr . '>';
}


/**
 * 変数情報のダンプを作成します
 *
 *  $mm                      = array();
 *  $mm['aaa']               = 'bbb';
 *  $mm['ccc']['ddd']        = 'eee';
 *  $mm['fff']['ggg']['hhh'] = 'iii';
 *
 *
 * @param $input String|Array ダンプを行いたい文字列または配列
 * @param $name String ダンプした際の接頭辞
 * @param $i
 * @param $k
 * @return void
 */
class Smarty_Function_HiddenLister__ArrayRecursive
{
    /**
     * 変数情報を格納します
     *
     * @var String
     */
    private static $retStr = '';

    /**
     * ダンプを行い、変数情報を文字列で返します
     * オブジェクトは未対応
     *
     * @param $input String|Array ダンプを行いたい文字列または配列
     * @param $name String ダンプした際の接頭辞
     * @return string 変数の情報
     */
    public static function exec( $input, $name = '$_DEBUG')
    {
        // 実行
        self::proccess($input, $name );

        // 取得
        $retStr = self::$retStr;

        // 初期化
        self::$retStr = '';

        return trim( $retStr );
    }
    public static function proccess( $input, $name, $i = '0', $k = array('Error') )
    {
        if( is_array( $input ) )
        {
            foreach( $input as $i => $value )
            {
                $temp = $k;
                $temp[] = $i;
                self::proccess( $value, $name, $i, $temp );
            }
        }
        else
        {
            self::$retStr .=  $name;
            foreach( $k as $i => $value)
            {
                if($value !== 'Error') {
                    self::$retStr .= '[' . $value . ']';
                }
            }
            self::$retStr .= 'hidden--hidden--hidden' . $input . '<smarty_function_hiddenLister>';
        }
    }
}




































