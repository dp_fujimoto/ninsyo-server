<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CreateUserHash.php';


/**
 * 入金結果ファイルアップロードし、データベースに入金結果反映を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/01/25
 * @version        SVN: $Id:
 *
 */
class User_Models_CheckUserContent_Upload
    extends Mikoshiva_Controller_Model_Db_Abstract {

    // ファイル名
    private $_fileName;

    /**
     * ファイルアップロードし、ユーザーコンテンツ情報を付与してダウンロード
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     *
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始:');

        $result = '';

        //-----------------------------------------------------
        // ▼UploadFile 操作クラスのインスタンスを生成
        //-----------------------------------------------------
        $file = null;
        $file = new Zend_File_Transfer_Adapter_Http();

        //-----------------------------------------------------
        // ▼ファイルの移動場所をセット（確認画面では一時保存ディレクトリを指定する）
        //-----------------------------------------------------
        $file->setDestination(UPLOADS_TEMP_DIR);
        $fileInfo = $file->getFileInfo(); // アップロードファイルを取得

        $contentId = $actionForm->getContentId();

        if (isset($fileInfo['fileData']['tmp_name']) && !isBlankOrNull($fileInfo['fileData']['tmp_name']) && !isBlankOrNull($contentId)) {
            $fp = fopen($fileInfo['fileData']['tmp_name'],'r');
            $this->_fileName = $fileInfo['fileData']['name'];

            $hashClass = new Api_Models_Classes_CreateUserHash();

            // ファイル1行ごとに処理
            $count = 0;
            $header = '';
            $data = array();
            while (!feof($fp)) {

                $row = mb_convert_encoding(str_replace('"','',fgets($fp)),'UTF-8','SJIS');
                if (isBlankOrNull($row)) {
                    continue;
                }

                // 行分解
                $rowArr = explode(',',$row);

                $email = trim($rowArr[0]);
                if ($count === 0) {
                    $count++;
                    if (strpos($email,'@') === FALSE) {
                        $header = $email . ',hash,min,max,' . $contentId;
                        $data[] = $header;
                        continue;
                    } else {
                        $header = 'email,hash,min,max,' . $contentId;
                        $data[] = $header;
                    }
                }

                $hash = $hashClass->execute($email);

                //-----------------------------------------------
                // ユーザー確認
                //-----------------------------------------------
                // SQL作成
                $sql = "";
                $sql .= "SELECT ";
                $sql .= "   muc_service_start_date";
                $sql .= "  ,muc_service_end_date";
                $sql .= "  ,mct_content_id";
                $sql .= " FROM mst_user";
                $sql .= " INNER JOIN mst_user_content ON mus_user_id  = muc_user_id";
                $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
                $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
                $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
                $sql .= " WHERE 1 = 1";
                $sql .= "   AND mus_delete_flag = '0'";
                $sql .= "   AND muc_delete_flag = '0'";
                $sql .= "   AND mct_delete_flag = '0'";
                $sql .= "   AND mpr_delete_flag = '0'";
                $sql .= "   AND mbr_delete_flag = '0'";
                $sql .= "   AND (muc_auth_flag = '1' OR (mct_auth_flag = '1' AND muc_auth_flag = '0'))";
                $sql .= "   AND mus_id_hash = ?";
                $sql .= "   AND mct_content_id = ?";

                $params   = array();
                $params[] = $hash;
                $params[] = $contentId;

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                $min = '';
                $max = '-';
                $ids = array();
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    if (isBlankOrNull($min)) {
                        $min = $v['muc_service_start_date'];
                    } else if ($min > $v['muc_service_start_date']) {
                        $min = $v['muc_service_start_date'];
                    }
                    if (isBlankOrNull($v['muc_service_end_date'])) {
                        $max = $v['muc_service_end_date'];
                    }
                    if (!isBlankOrNull($max)) {
                        if ($max === '-') {
                            $max = $v['muc_service_end_date'];
                        } else if ($max < $v['muc_service_end_date']) {
                            $max = $v['muc_service_end_date'];
                        }
                    }
                    $ids[$v['mct_content_id']] = $v['mct_content_id'];
                }

                if ($max === '-') {
                    $max = '';
                }

                $str = $email . ',' . $hash . ',' . $min . ',' . $max;
                $str .= ',';
                if (in_array($contentId,$ids)) {
                    $str .= '閲覧可能';
                } else {
                    $str .= '権限なし';
                }
                $data[] = $str;
            }

            //-----------------------------------------------
            // csvダウンロード
            //-----------------------------------------------
            // レスポンス
            $response = null;
            $response = $context->getResponse();

            // レスポンスをセット
            $response->setHeader('Content-disposition', 'attachment; filename=' . $this->_fileName);
            $response->setHeader('Content-type', 'application/octet-stream; name=' . $this->_fileName);
            $response->appendBody(mb_convert_encoding(implode("\r\n",$data),'SJIS','UTF-8'));
        } else {
            echo 'エラー';
        }

        logInfo('(' . __LINE__ . ')', 'return:終了');
        return null;
    }
}
