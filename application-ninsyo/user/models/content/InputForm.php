<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * -----------------------------------------------------------------------
 *    ワンポイント
 * -----------------------------------------------------------------------
 *    ■クラス名
 *    アクションフォームクラスのクラス名は、
 *    必ず「～Form」としてください。
 *
 *    ■resetメソッド
 *    任意の初期化処理を行う場合に実装します。
 *    (メソッド定義は省略可能です)
 *
 *    ■validateメソッド
 *    任意のバリデート処理を行う場合に実装します。(独自バリデート)
 *    validation.yml定義による固定バリデート処理を実行するには、
 *    スーパークラスのvalidateメソッドを呼び出してください。
 *    「parent::reset($config, $request);」
 *    その場合、スーパークラスのvalidateメソッドが返す
 *    固定バリデートの実行結果値は、適切に処理してください。
 *    (メソッド定義は省略可能です)
 * -----------------------------------------------------------------------
 *
 * @author        K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class User_Models_Content_InputForm
    extends Mikoshiva_Controller_Mapping_Form_Abstract {




    /**
     * アクションフォームの初期化を行います。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');
        parent::reset($config, $request);
        logInfo('(' . __LINE__ . ')', '終了');
    }

    /**
     * バリデートを行います。エラーMSGをリターンでかえす
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');

        $message = array();

        // サーバー名
        if (isBlankOrNull($this->getServerName())) {
            $message[] = 'サーバー名が未選択です。';
        }

        // メールアドレス
        if (isBlankOrNull($this->getEmail())) {
            $message[] = 'メールアドレスが未記入です。';
        }

        // パスワードのチェック
        if (isBlankOrNull($this->getPassword())) {
            $message[] = 'パスワードが未記入です。';
        }

        // コンテンツIDのチェック
        if (isBlankOrNull($this->getContentId())) {
            $message[] = 'コンテンツIDが未記入です。';
        } else {
            $contentId = explode(',',$this->getContentId());
            $tempContentIds = array();
            foreach ($contentId as $k => $v) {
                if (!isBlankOrNull($v)) {
                    $tempContentIds[$v] = $v;
                }
            }

            //-----------------------------------------------
            //
            // コンテンツ検索
            //
            //-----------------------------------------------
            // SQL作成
            $sql = "";
            $sql .= "SELECT";
            $sql .= "    mbr_brand_name";
            $sql .= "   ,mpr_product_name";
            $sql .= "   ,mct_content_name";
            $sql .= "   ,mct_content_id";
            $sql .= " FROM mst_content";
            $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
            $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
            $sql .= " WHERE 1 = 1 ";
            $sql .= " AND mct_delete_flag = '0'";
            $sql .= " AND mbr_delete_flag = '0'";
            $sql .= " AND mpr_delete_flag = '0'";
            $sql .= " AND mct_content_id IN (?)";

            $params = array();
            $params[] = $tempContentIds;

            $sql .= " ORDER BY mbr_brand_name, mpr_product_name, mct_content_name";

            // SQL実行＋全件取得
            $res = array();
            for ($i = 0; $i < count($params); $i++) {
                $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
            }
            $res = $this->_db->query($sql);

            $contentList = array();
            $ids = array();
            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                $contentList[$v['mct_content_id']][0] = $v['mbr_brand_name'];
                $contentList[$v['mct_content_id']][1] = $v['mpr_product_name'];
                $contentList[$v['mct_content_id']][2] = $v['mct_content_name'];
                $ids[$v['mct_content_id']] = $v['mct_content_id'];
            }
            if (count($ids) !== count($tempContentIds)) {
                $errorIds = array();
                foreach ($tempContentIds as $k => $v) {
                    if (!isset($ids[$v])) {
                        $errorIds[] = $v;
                    }
                }
                $message[] = '入力したコンテンツIDで無効なものがあります。';
                $message[] = 'ERROR CONTENT ID:' . implode(',',$errorIds);
            } else {
                ksort($ids);
                $this->setContentId(implode(',',$ids));
            }
        }

        //-----------------------------------------------
        // エラーメッセージを返す
        //-----------------------------------------------
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $message;
    }

}
