<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Exception.php';

/**
 * アクション設定ファイル取得して配列で返します。
 *
 * 配列パターン
 * configs['action-form']
 * configs['action-mapping']
 * configs['validation']
 *
 * @package Mikoshiva_Controller
 * @author kawakami
 *
 */
class Mikoshiva_Controller_Mapping_ConfigLoader {

    private $path = null;

    /**
     * コンストラクタ
     * 定義ファイルのパス情報をメンバ変数に保存します。
     *
     * @param String $path
     */
    public function __construct($path) {
        $this->path = $path;
    }

    /**
     * 定義ファイルを取得します。
     * @return array $configs
     */
    public function get() {
        $configs = array();
        $configs['action-form']    = $this->loadActionForm($this->path);
        $configs['action-mapping'] = $this->loadMapping($this->path);
        $configs['validation']     = $this->loadValidation($this->path);

        return $configs;
    }

    /**
     * アクションフォームの定義ファイルを取得します。
     * @return array $form
     */
    private function loadActionForm($path) {
        $form = array();
        $form = Spyc::YAMLLoad($this->getConfigFilePath($path, 'action-form.yml'));

        if (! isset($form['action-form'])) {
            throw new Mikoshiva_Exception(
                '【Mikoshiva_Controller_Mapping_ConfigLoader】設定ファイルに [action-form] セクションが存在しません。');
        }

        return $form['action-form'];
    }

    /**
     * アクションマッピングの定義ファイルを取得します。
     * @return array $mapping
     */
    private function loadMapping($path) {
        $mapping = array();
        $mapping = Spyc::YAMLLoad($this->getConfigFilePath($path, 'action-mapping.yml'));

        if (! isset($mapping['action-mapping'])) {
            throw new Mikoshiva_Exception(
                '【Mikoshiva_Controller_Mapping_ConfigLoader】設定ファイルに [action-mapping] セクションが存在しません。');
        }

        return $mapping['action-mapping'];
    }

    /**
     * バリデートの定義ファイルを取得します。
     * @return array $validation
     */
    private function loadValidation($path) {
        $validation = array();

		Mikoshiva_Logger::debug($path . '/validation.yml','バリデーションファイルパス');
        if (!file_exists($path . '/validation.yml')) {
        	return null;
        }

        $validation = Spyc::YAMLLoad($this->getConfigFilePath($path, 'validation.yml'));

        if (! isset($validation['validation'])) {
            throw new Mikoshiva_Exception(
                '【Mikoshiva_Controller_Mapping_ConfigLoader】設定ファイルに [validation] セクションが存在しません。');
        }

        return $validation['validation'];
    }

    /**
     * ファイルの存在チェックを行います。
     * 存在しない場Exceptionを投げます。
     *
     * @param String $fileName
     * @return String filePath
     */
    private function getConfigFilePath($path, $fileName) {
        $filePath = '';
        $filePath = $path . '/' . $fileName;

        // 存在チェック
        if (! file_exists($filePath)) {
            throw new Mikoshiva_Exception(
                '【Mikoshiva_Controller_Mapping_ConfigLoader】設定ファイル [' . $filePath . '] が存在しません。');
        }

        return $filePath;
    }

}