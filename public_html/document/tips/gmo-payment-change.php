<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - 支払い方法変更（会員登録～カード登録）</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="支払い方法変更（会員登録～カード登録）">支払い方法変更（会員登録～カード登録）</h1>

            <p>支払い方法変更（会員登録～カード登録）</p>



            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <div class="system-message">
              <p>
                実行クラスは以下の場合をシステムエラーとみなして例外を投げます。<br>
                <span style="color: #bb0000">※GMO における決済の失敗（エラー）は false を返します。</span>
              </p>
              <ol>
                <li>必須の値がセットされていない場合</li>
                <li>GMO 決済 API への接続に失敗した場合</li>
                <li>何らかの理由でレスポンスが取得出来なかった場合</li>
              </ol>
            </div>

            <pre class="wiki"><?php
highlight_string('<?php

//-----------------------------------------------
//
// ■支払い方法変更サンプル
//
//------------------------------------------------
//----------------------------------------------
// 支払い方法変更 POPO に詰める
//----------------------------------------------
include_once \'Mikoshiva/Settlement/Popo/Input/Payment/Change.php\';
$changePopo = null;
$changePopo = new Mikoshiva_Settlement_Popo_Input_Payment_Change(); // 実行クラスへ渡すための入れ物クラス（ POPO ）

$changePopo->setMemberID(\'DP-0000036742-001\');  // 会員 ID
$changePopo->setCardNo(\'4111111111111111\');     // カード番号
$changePopo->setExpire(\'1110\');                 // 有効期限


//----------------------------------------------
// 実行
//----------------------------------------------
include_once \'Mikoshiva/Settlement/Execution/Payment/Change.php\';
$paymentChangeExec = null;
$paymentChangeExec = new Mikoshiva_Settlement_Execution_Payment_Change($changePopo);
$ret               = $paymentChangeExec->execute();   // 実行  true / false


//-----------------------------------------------
// 処理結果を取得
// Mikoshiva_Settlement_Popo_Output_Payment_Change が返ります
//
// 決済成功の場合は空の Mikoshiva_Settlement_Popo_Output_Payment_Change が返ります（エラー時のプロパティしか保持していないため）
//-----------------------------------------------
$output = $paymentChangeExec->getOutput(); // 結果を取得
'); ?>
            </pre>


            <h2 id="Mikoshiva_Settlement_Popo_Input_Payment_Change">Mikoshiva_Settlement_Popo_Input_Payment_Change（GMO へ送信する値を保持する POPO 会員登録とカード登録に必要なプロパティを保持しています）</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>必須</th>
                <th>型</th>
                <th>桁</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>MemberID</td>
                <td>●</td>
                <td>string</td>
                <td>60</td>
                <td>サイト ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>CardNo</td>
                <td>●</td>
                <td>string</td>
                <td>16</td>
                <td>カード番号</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Expire</td>
                <td>●</td>
                <td>string</td>
                <td>4</td>
                <td>有効期限</td>
                <td>（YYMM）形式</td>
              </tr>
            </table>





            <h2 id="Mikoshiva_Settlement_Popo_Output_Payment_Change">Mikoshiva_Settlement_Popo_Output_Payment_Change（GMO 返却された値を保持する POPO）</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>桁</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>ErrCode</td>
                <td>string</td>
                <td>3</td>
                <td>エラーコード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続された文字列
                </td>
              </tr>
              <tr>
                <td>ErrInfo</td>
                <td>string</td>
                <td>9</td>
                <td>エラー詳細コード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続された文字列
                </td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>




<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





















