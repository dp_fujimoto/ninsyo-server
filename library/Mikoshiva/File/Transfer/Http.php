<?php


include_once 'Zend/File/Transfer/Adapter/Http.php';

class Mikoshiva_File_Transfer_Adapter_Http extends Zend_File_Transfer_Adapter_Http
{




    /**
     * コンストラクタ
     *
     * @param unknown_type $options
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/07/09
     * @version SVN:$Id: Http.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct($options = array()) {


        $this->addPrefixPath('Mikoshiva_Validate_File', 'Mikoshiva/Validate/File', Zend_File_Transfer_Adapter_Http::VALIDATE);

        parent::__construct($options);
    }
}