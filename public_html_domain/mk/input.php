<?php
	require_once './init.php';

        $response = '';

	    // セッション切れ再リクエスト時
        if (! isset($_SESSION['campaignSalesCode'])) {
            // エラーページ表示
            $response = $agent->getResponseByPost($basePath.'signin', $params);
            echo $response;
            error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[return:SESSION TIME OUT]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
            return;
        }


		switch ($_POST['code']) {
			case 'LOGIN';
				error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[case:LOGIN]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

				$campaignSalesCode = $_SESSION['campaignSalesCode'];

				// セッション初期化
				session_unset();
				$_SESSION['email'] = sha1($_POST['email']);
				$_SESSION['transactionNumber'] = '1';
				$_SESSION['campaignSalesCode'] = $campaignSalesCode;

				// パラメータ追加
				$params                      = $_POST;
				$params['campaignSalesCode'] = $campaignSalesCode;
				$params['transactionNumber'] = $_SESSION['transactionNumber'];

				// mikoshivaにpost
			    $response = $agent->getResponseByPost($basePath.'login', $params);
			    break;
			case 'INPUT';
				error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[case:INPUT]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

				$campaignSalesCode = $_SESSION['campaignSalesCode'];

				// セッション初期化
				session_unset();
				$_SESSION['email'] = sha1($_POST['email']);
				$_SESSION['transactionNumber'] = '1';
				$_SESSION['campaignSalesCode'] = $campaignSalesCode;

				// パラメータ追加
				$params                      = $_POST;
				$params['campaignSalesCode'] = $campaignSalesCode;
				$params['transactionNumber'] = $_SESSION['transactionNumber'];

				// mikoshivaにpost
			    $response = $agent->getResponseByPost($basePath.'input', $params);
			    break;
			case 'BACK_INPUT';
				error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[case:BACK_INPUT]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

				$campaignSalesCode = $_SESSION['campaignSalesCode'];

				// パラメータを追加
				$params                      = $_POST;
				$params['campaignSalesCode'] = $campaignSalesCode;

			    // mikoshivaにpost
			    $response = $agent->getResponseByPost($basePath.'input', $params);
			    error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
			    break;

			default:
				break;
		}
	    echo $response;
	    error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
