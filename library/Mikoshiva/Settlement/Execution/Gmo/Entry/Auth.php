<?php
/** @package Mikoshiva_Gmo */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Abstract.php';


/**
 * 取引登録（簡単オーソリ）を行います
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/25
 * @version SVN:$Id: Auth.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Entry_Auth extends Mikoshiva_Settlement_Execution_Gmo_Entry_Abstract {


    /**
     * 処理区分
     * @var string 処理区分
     */
    protected $_jobCd = 'AUTH';
}