<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; FTP 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#__construct">Mikoshiva_Ftp#__construct - コンストラクタ</a></li>
              <li><a href="#connect">Mikoshiva_Ftp#connect - FTP サーバーに接続を試みます</a></li>
              <li><a href="#login">Mikoshiva_Ftp#login - ログインを試みます</a></li>
              <li><a href="#cd">Mikoshiva_Ftp#cd - リモートのカレント作業ディレクトリを移動します</a></li>
              <li><a href="#pwd">Mikoshiva_Ftp#pwd - 	リモートのカレントディレクトリのlパスを表示します</a></li>
              <li><a href="#mkdir">Mikoshiva_Ftp#mkdir - リモートにディレクトリ作成します</a></li>
              <li><a href="#execute">Mikoshiva_Ftp#execute - 任意の FTP コマンドを実行します</a></li>
              <li><a href="#rename">Mikoshiva_Ftp#rename - リモートのファイル or ディレクトリの名前を変更します</a></li>
              <li><a href="#ls">Mikoshiva_Ftp#ls - リモートのファイルの一覧を表示</a></li>
              <li><a href="#rm">Mikoshiva_Ftp#rm - リモートのファイル or ディレクトリの削除を行います</a></li>
              <li><a href="#get">Mikoshiva_Ftp#get - ファイルのダウンロードを行います</a></li>
              <li><a href="#put">Mikoshiva_Ftp#put - リモートにファイルをアップロードします</a></li>
              <li><a href="#disconnect">Mikoshiva_Ftp#disconnect - FTP サーバーとの接続を切断します</a></li>
            </ol>




            <!-- ==============================================
            ■Mikoshiva_Ftp#__construct - コンストラクタ
            ============================================== -->
            <h2 id="Mikoshiva_Ftp#__construct"><a name="__construct">Mikoshiva_Ftp#__construct</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              FTP サーバと通信するための新規オブジェクトを生成します。
            </p>
            <p>
              ※１&ensp;この段階ではサーバーに接続されていません。<br>
              &emsp;&emsp;&ensp;connect メソッドを使用して初めて接続されます。<br>
              ※２&ensp;接続に失敗した場合 Mikoshiva_Ftp_Exception を投げます
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$host</td>
                <td>string</td>
                <td>NULL</td>
                <td>接続先ホスト</td>
                <td>IP アドレスあるいはドメイン名のいずれか</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$port</td>
                <td>string</td>
                <td>NULL</td>
                <td>ポート番号</td>
                <td>サーバへの接続のためのポート番号</td>
              </tr>
              <tr>
                <td>3</td>
                <td>$timeout</td>
                <td>string</td>
                <td>90</td>
                <td>タイムアウト</td>
                <td>タイムアウトを行う秒数</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#connect - FTP サーバーに接続を試みます
            ============================================== -->
            <h2 id="Mikoshiva_Ftp#connect"><a name="connect">Mikoshiva_Ftp#connect</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              FTP サーバーに接続を試みます
            </p>
            <p>
              コンストラクタでホスト名、ポート番号を入力している場合、このメソッドの引数は必要ありません。
            </p>
            <p>
              3 回接続を試みます。
              3 回とも接続に失敗した場合 Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$host</td>
                <td>string</td>
                <td>NULL</td>
                <td>接続先ホスト</td>
                <td>IP アドレスあるいはドメイン名のいずれか</td>
              </tr>
              <tr>
                <td>2</td>
                <td>$port</td>
                <td>string</td>
                <td>NULL</td>
                <td>ポート番号</td>
                <td>サーバへの接続のためのポート番号</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td>接続成功</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp();  // 引数を省略した場合は connect で指定する
$ret = $ftp->connect(\'192.168.1.143\', 21); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#login
            ============================================== -->
            <h2 id="Mikoshiva_Ftp#login"><a name="login">Mikoshiva_Ftp#login</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              FTP サーバーにログインを試みます
            </p>
            <p>
              ログインに失敗した場合 Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$username</td>
                <td>string</td>
                <td><br></td>
                <td>ログイン ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>2</td>
                <td>string</td>
                <td><br></td>
                <td>ログインパスワード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td>接続成功</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ret = $ftp->login(\'elfaris\', \'elfaris1404\'); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#cd
            ============================================== -->
            <h2><a name="cd">Mikoshiva_Ftp#cd</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのカレント作業ディレクトリを移動します
            </p>
            <p>
              ログインに失敗した場合 Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$dir</td>
                <td>string</td>
                <td><br></td>
                <td>移動するディレクトリへのパス</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td>接続成功</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ret = $ftp->cd(\'/a/b/c\'); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#pwd
            ============================================== -->
            <h2><a name="pwd">Mikoshiva_Ftp#pwd</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのカレントディレクトリのパスを表示します
            </p>
            <p>
              ログインに失敗した場合 Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>string</td>
                <td><br></td>
                <td>リモートのカレントディレクトリのパス</td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');
dumper($ftp->pwd()); // /var/wwwa/123marketing/a/b/c が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#mkdir
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="mkdir">Mikoshiva_Ftp#mkdir</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートにディレクトリを作成します
            </p>
            <p>
              ログインに失敗した場合 Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$dir</td>
                <td>string</td>
                <td><br></td>
                <td>作成したいディレクトリ名</td>
                <td>
                  絶対パス（例. '/home/mydir'）<br>
                  相対パス（例. 'mydir'）
                 </td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$recursive</td>
                <td>string</td>
                <td>false</td>
                <td>作成したいディレクトリ名</td>
                <td>指定したディレクトリがない場合に再帰的にディレクトリを作成するか</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');

// /var/wwwa/123marketing/a/b/c/test を作成
dumper($ftp->mkdir(\'test\')); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#execute
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="execute">Mikoshiva_Ftp#execute</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              任意の FTP コマンドを実行します<br>
              ※ pwd などのコマンドであっても結果は true しか返しません<br>
              （SITE EXEC コマンド自体は、 メソッドによって付け加えられます）
            </p>
            <p>
              コマンドの実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$command</td>
                <td>string</td>
                <td><br></td>
                <td>コマンド</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');

// /var/wwwa/123marketing/a/b/c/test を作成
dumper($ftp->execute(\'pwd\')); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#rename
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="rename">Mikoshiva_Ftp#rename</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのファイル or ディレクトリの名前を変更します
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$remote_from</td>
                <td>string</td>
                <td><br></td>
                <td>変更したい対象ファイル・ディレクトリ名</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$remote_to</td>
                <td>string</td>
                <td><br></td>
                <td>変更後のファイル・ディレクトリ名</td>
                <td><br></td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');

dumper($ftp->rename(\'popo.jpg\', \'taniguchi.jpg\')); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#ls
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="ls">Mikoshiva_Ftp#ls</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのファイルの一覧を表示
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$dir</td>
                <td>string</td>
                <td>NULL</td>
                <td>一覧を取得したいディレクトリまでのパス</td>
                <td>NULL の場合カレントディレクトリの一覧を返します</td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$mode</td>
                <td>int</td>
                <td>NET_FTP_DIRS_FILES</td>
                <td>一覧取得モード</td>
                <td>
                  NET_FTP_FILES_ONLY&emsp;戻り値を、ファイル名（ディレクトリ名は含まない）の配列にします<br>
                  NET_FTP_DIRS_ONLY&ensp;&emsp;戻り値を、ディレクトリ名（ファイル名は含まない）の配列にします<br>
                  NET_FTP_DIRS_FILES&emsp;戻り値を、ディレクトリ名とファイル名の両方を含む配列にします<br>
                  NET_FTP_RAWLIST&ensp;&ensp;&ensp;&emsp;戻り値を、PHP 関数の ftp_rawlist() と同じ形式の配列にします。
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>array</td>
                <td><br></td>
                <td><br></td>
                <td>
                <pre>
["rights"]      =>  パーミッション
["user"]        =>  オーナー名
["group"]       =>  グループ名
["files_inside"]=>
["date"]        =>  作成日（Unix タイムスタンプ）
["is_dir"]      =>  ディレクトリである場合”d”、ファイルである場合空文字
["name"]        =>  ファイル・ディレクトリ名
["size"]        =>  ファイルサイズ。ディレクトリの場合は０。
                </pre>
                </td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');
dumper($ftp->ls(\'./\', NET_FTP_DIRS_FILES)); // $ftp->ls(); としたモノと同義
');
?>
            </pre>


            <h3>それぞれのモードの結果</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------
// NET_FTP_FILES_ONLY
//-----------------------------------
array(2) {
  [0] => array(9) {
    ["is_dir"] => string(0) ""
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 17:58"
    ["name"] => string(1) "d"
    ["stamp"] => int(1268816280)
  }
  [1] => array(9) {
    ["is_dir"] => string(0) ""
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(6) "124629"
    ["date"] => string(12) "Mar 17 20:51"
    ["name"] => string(13) "taniguchi.jpg"
    ["stamp"] => int(1268826660)
  }
}


//-----------------------------------
// NET_FTP_DIRS_ONLY
//-----------------------------------
array(3) {
  [0] => array(9) {
    ["is_dir"] => string(1) "d"
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 20:53"
    ["name"] => string(1) "."
    ["stamp"] => int(1268826780)
  }
  [1] => array(9) {
    ["is_dir"] => string(1) "d"
    ["rights"] => string(9) "---------"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 20:53"
    ["name"] => string(2) ".."
    ["stamp"] => int(1268826780)
  }
  [2] => array(9) {
    ["is_dir"] => string(1) "d"
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 20:28"
    ["name"] => string(22) "test-2010-03-17-082846"
    ["stamp"] => int(1268825280)
  }
}


//-----------------------------------
// NET_FTP_DIRS_FILES
//-----------------------------------
array(5) {
  [0] => array(9) {
    ["is_dir"] => string(1) "d"
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 20:53"
    ["name"] => string(1) "."
    ["stamp"] => int(1268826780)
  }
  [1] => array(9) {
    ["is_dir"] => string(1) "d"
    ["rights"] => string(9) "---------"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 20:53"
    ["name"] => string(2) ".."
    ["stamp"] => int(1268826780)
  }
  [2] => array(9) {
    ["is_dir"] => string(1) "d"
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 20:28"
    ["name"] => string(22) "test-2010-03-17-082846"
    ["stamp"] => int(1268825280)
  }
  [3] => array(9) {
    ["is_dir"] => string(0) ""
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(1) "0"
    ["date"] => string(12) "Mar 17 17:58"
    ["name"] => string(1) "d"
    ["stamp"] => int(1268816280)
  }
  [4] => array(9) {
    ["is_dir"] => string(0) ""
    ["rights"] => string(9) "rwxrwxrwx"
    ["files_inside"] => string(1) "1"
    ["user"] => string(5) "noone"
    ["group"] => string(7) "nogroup"
    ["size"] => string(6) "124629"
    ["date"] => string(12) "Mar 17 20:51"
    ["name"] => string(13) "taniguchi.jpg"
    ["stamp"] => int(1268826660)
  }
}


//-----------------------------------
// NET_FTP_RAWLIST
//-----------------------------------
array(5) {
  [0] => string(51) "drwxrwxrwx 1 noone nogroup         0 Mar 17 20:53 ."
  [1] => string(52) "d--------- 1 noone nogroup         0 Mar 17 20:53 .."
  [2] => string(51) "-rwxrwxrwx 1 noone nogroup         0 Mar 17 17:58 d"
  [3] => string(63) "-rwxrwxrwx 1 noone nogroup    124629 Mar 17 20:51 taniguchi.jpg"
  [4] => string(72) "drwxrwxrwx 1 noone nogroup         0 Mar 17 20:28 test-2010-03-17-082846"
}
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#rm
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="rm">Mikoshiva_Ftp#rm</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのファイル or ディレクトリの削除を行います
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$path</td>
                <td>string</td>
                <td><br></td>
                <td>削除したいファイル・ディレクトリ</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$recursive</td>
                <td>string</td>
                <td>false</td>
                <td><br></td>
                <td>再帰的に削除するか</td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');
dumper($ftp->rm(\'./popo.jpg\')); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#get
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="get">Mikoshiva_Ftp#get</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのファイルをダウンロードします。
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$remote_file</td>
                <td>string</td>
                <td><br></td>
                <td>リモートのファイル名</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$local_file</td>
                <td>string</td>
                <td><br></td>
                <td>ローカルのファイル名</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$overwrite</td>
                <td>bool</td>
                <td>false</td>
                <td>同一ファイル名が存在していた場合に上書きを行うか</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$mode</td>
                <td>int</td>
                <td>FTP_BINARY</td>
                <td>通信モード</td>
                <td>
                  FTP_ASCII&ensp;&emsp;アスキーモード
                  FTP_BINARY&emsp;バイナリモード
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');
dumper($ftp->get(\'./popo.jpg\', \'c:\popo.jpg\')); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#put
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="put">Mikoshiva_Ftp#put</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              ローカルのファイルをリモートへアップロードします。
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$local_file</td>
                <td>string</td>
                <td><br></td>
                <td>ローカルのファイル名</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$remote_file</td>
                <td>string</td>
                <td><br></td>
                <td>リモートのファイル名</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$overwrite</td>
                <td>bool</td>
                <td>false</td>
                <td>同一ファイル名が存在していた場合に上書きを行うか</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$mode</td>
                <td>int</td>
                <td>FTP_BINARY</td>
                <td>通信モード</td>
                <td>
                  FTP_ASCII&ensp;&emsp;アスキーモード
                  FTP_BINARY&emsp;バイナリモード
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>true</td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');
dumper($ftp->put(\'C:\elfaris\popo.jpg\', \'popo.jpg\')); // true が返る
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Ftp#disconnect
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="disconnect">Mikoshiva_Ftp#disconnect</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              FTP サーバーとの接続を切断します。
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Ftp_Exception を投げます。
            </p>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■FTP・サンプル
//
//------------------------------------------------
include_once \'Mikoshiva/Ftp.php\';
$ftp = new Mikoshiva_Ftp(\'192.168.1.143\', 21);  // 引数を省略した場合は connect で指定する
$ftp->connect();
$ftp->login(\'elfaris\', \'elfaris1404\');
$ftp->cd(\'/a/b/c\');
$ftp->disconnect(); // 切断
');
?>
            </pre>






  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>














