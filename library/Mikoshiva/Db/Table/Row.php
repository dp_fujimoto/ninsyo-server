<?php
/**
 * @package Mikoshiva_Db
 */

/**
 * 
 * 
 * @package Mikoshiva_Db
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Row.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Table_Row extends Zend_Db_Table_Row_Abstract {

    protected function _transformColumn($columnName)
    {

        if (array_key_exists($columnName, $this->_data)) return $columnName;

        $_columnName = $columnName;
        $col = current($this->_table->info('cols'));
        preg_match('#^([a-z]{3})_#', $col, $prefixList);
        $prefix = $prefixList[1];


        $_columnName = $prefix . '_' . preg_replace('#([A-Z])#', '_$1', $columnName);
        $_columnName = strtolower($_columnName);
        return $_columnName;
    }

    /**
     * Returns all data as an array.
     *
     * Updates the $_data property with current row object values.
     *
     * @return array
     */
    public function toArrayFormName() {

        $d = array();
        foreach ($this->toArray() as $k => $v) {
            $_k = preg_replace('#^.*?_#', '', $k, 1);
            $_k = preg_replace('#_([a-z]+?)#e', "ucfirst('$1')", $_k);
            $d[$_k] = $v;
        }
        return $d;
    }
}
