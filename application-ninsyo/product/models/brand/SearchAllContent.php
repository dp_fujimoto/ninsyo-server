<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstProduct.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Product_Models_Brand_SearchAllContent extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 出力
        $output = array();

        //-----------------------------------------------
        //
        // ブランド検索
        //
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT";
        $sql .= " *";
        $sql .= " FROM mst_content";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1 ";
        $sql .= " AND mbr_delete_flag = '0'";
        $sql .= " AND mpr_delete_flag = '0'";
        $sql .= " AND mct_delete_flag = '0'";
        $sql .= " AND mbr_brand_id = ?";

        // パラメータセット
        $params = array();
        $params[] = $actionForm->getBrandId();

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            if (!isset($output[$v['mbr_brand_name']])) {
                $output[$v['mbr_brand_name']] = array();
                $output[$v['mbr_brand_name']]['info'] = Mikoshiva_Utility_Array::extractBeginningOfPrefix('mbr_' , $v);
                $output[$v['mbr_brand_name']]['data'] = array();
            }
            if (!isset($output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']])) {
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']] = array();
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['info'] = Mikoshiva_Utility_Array::extractBeginningOfPrefix('mpr_' , $v);
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['data'] = array();
            }

            $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['data'][$v['mct_content_name']] = Mikoshiva_Utility_Array::extractBeginningOfPrefix('mct_' , $v);
        }

        $totalRowCount = 0;

        foreach ($output as $brandName => $v) {
            foreach ($v['data'] as $productName => $v2) {
                $rowCount = count($v2['data']);
                $output[$brandName]['data'][$productName]['info']['rowCount'] = $rowCount;
                $totalRowCount += $rowCount;
            }
            $output[$brandName]['info']['rowCount'] = $totalRowCount;
        }

        // 自然順ソート
        uksort($output, 'strnatcmp');
        foreach ($output as $k => $v) {
            uksort($output[$k]['data'], 'strnatcmp');
            foreach ($v['data'] as $k2 => $v2) {
                uksort($output[$k]['data'][$k2]['data'], 'strnatcmp');
            }
        }


        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;

    }
}
