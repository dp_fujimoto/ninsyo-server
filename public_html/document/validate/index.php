<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Validate</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時  <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document">＜＜　戻る</a></p>

        <ol>
          <li>バリデータリファレンス
            <ol>
              <li><a href="/document/validate/Address.php">Address - 郵便番号と住所の整合性</a> <img src="/document/img/new.gif"></li>
              <li><a href="/document/validate/Zip.php">Zip - 郵便番号</a></li>
              <li><a href="/document/validate/Tel.php">Tel - 電話番号</a></li>
              <li><a href="/document/validate/Fax.php">Fax - FAX 番号</a></li>
              <li><a href="/document/validate/Email.php">Email - Email</a></li>
              <li><a href="/document/validate/StringLength.php">StringLength - 文字列長の範囲を指定</a></li>
              <li><a href="/document/validate/ShopId.php">ShopId - GMO ショップ ID</a></li>
              <li><a href="/document/validate/Regex.php">Regex - 正規表現による評価</a></li>
              <li><a href="/document/validate/LessThan.php">LessThan - 未満の評価</a></li>
              <li><a href="/document/validate/Kana.php">Kana - カタカナの評価</a></li>
              <li><a href="/document/validate/Ip.php">Ip - IP アドレスの評価</a></li>
              <li><a href="/document/validate/InArray.php">InArray - 指定したリストでの評価</a></li>
              <li><a href="/document/validate/Iban.php">Iban - IBAN コードの評価</a></li>
              <li><a href="/document/validate/Digits.php">Digits - 数字の評価</a></li>
              <li><a href="/document/validate/Int.php">Int - 整数の評価</a></li>
              <li><a href="/document/validate/Float.php">Float - 浮動小数点数の評価</a></li>
              <li><a href="/document/validate/Identical.php">Identical - 値の同一性を評価</a></li>
              <li><a href="/document/validate/Hostname.php">Hostname - ホスト名を評価</a></li>
              <li><a href="/document/validate/Hex.php">Hex - 16 進数を評価</a></li>
              <li><a href="/document/validate/GreaterThan.php">GreaterThan - 超過の評価</a></li>
              <li><a href="/document/validate/Float.php">Float - 浮動小数点数の評価</a></li>
              <li><a href="/document/validate/Date.php">Date - 日付の評価</a></li>
              <li><a href="/document/validate/DateLister.php">DateLister - 年・月・日の評価（別々でリクエストされる日付を評価する）</a></li>
              <li><a href="/document/validate/TimeLister.php">TimeLister - 時・分・秒の評価（別々でリクエストされる時間を評価する）</a></li>
              <li><a href="/document/validate/Ccnum.php">Ccnum - クレジットカード番号の評価</a></li>
              <li><a href="/document/validate/Between.php">Between - 指定範囲内の評価</a></li>
              <li><a href="/document/validate/Alpha.php">Alpha - アルファベットの評価</a></li>
              <li><a href="/document/validate/Alnum.php">Alnum - アルファベットと数字の評価</a></li>
              <li><a href="/document/validate/allowEmpty.php">allowEmpty - Emptyを許容するかを指定</a></li>
              <li><a href="/document/validate/NotEmpty.php">NotEmpty - Empty の評価</a></li>
              <li><a href="/document/validate/Uri.php">Uri - URI の評価</a></li>
              <li><a href="/document/validate/KishuIzon.php">KishuIzon - 機種依存文字が含まれているかを評価する</a> <img src="/document/img/new.gif"></li>
              <li>アップロードファイル
                <ol>
                    <li><a href="/document/validate/File/Size.php">Size - ファイルサイズ</a><img src="/document/img/new.gif"></li>
                    <li><a href="/document/validate/File/Extension.php">Extension - 指定した拡張子以外をエラーにする</a><img src="/document/img/new.gif"></li>
                    <li><a href="/document/validate/File/ExcludeExtension.php">ExcludeExtension - 指定した拡張子をエラーにする</a><img src="/document/img/new.gif"></li>
                    <li><a href="/document/validate/File/ImageSize.php">ImageSize - 画像のサイズを検証する</a><img src="/document/img/new.gif"></li>
                    <li><a href="/document/validate/File/IsImage.php">IsImage - イメージファイルであるかを検証する</a><img src="/document/img/new.gif"></li>
                </ol>
              </li>
              <li><a href="/document/validate/TestCard.php">TestCard - テストカード番号を使用したクライアントIPアドレスの評価</a></li>
            </ol>
          </li>
        </ol>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>