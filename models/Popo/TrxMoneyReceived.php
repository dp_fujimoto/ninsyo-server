<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_money_received
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxMoneyReceived extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tmr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'moneyReceivedId' => null,
        'chargeId' => null,
        'claimId' => null,
        'directInstallmentPaymentId' => null,
        'depositMoneyReceivedType' => null,
        'moneyReceivedAmount' => null,
        'moneyReceivedPlanDate' => null,
        'moneyReceivedDate' => null,
        'moneyReceivedType' => null,
        'moneyReceivedCompleteFlag' => null,
        'moneyReceivedCancelFlag' => null,
        'moneyReceivedCancelDate' => null,
        'resultFileType' => null,
        'resultFileName' => null,
        'resultFileRowNumber' => null,
        'resultFileId' => null,
        'resultFileUploadDatetime' => null,
        'staffId' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tmr_money_received_id の値を返します
	 *
     * @return int tmr_money_received_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedId() {
		return $this->_properties['moneyReceivedId'];
    }


    /**
     * tmr_money_received_id の値をセットします
	 *
	 * @param int $moneyReceivedId tmr_money_received_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedId($moneyReceivedId) {
		$this->_properties['moneyReceivedId'] = $moneyReceivedId;
    }


    /**
     * tmr_charge_id の値を返します
	 *
     * @return int tmr_charge_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * tmr_charge_id の値をセットします
	 *
	 * @param int $chargeId tmr_charge_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * tmr_claim_id の値を返します
	 *
     * @return int tmr_claim_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClaimId() {
		return $this->_properties['claimId'];
    }


    /**
     * tmr_claim_id の値をセットします
	 *
	 * @param int $claimId tmr_claim_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClaimId($claimId) {
		$this->_properties['claimId'] = $claimId;
    }


    /**
     * tmr_direct_installment_payment_id の値を返します
	 *
     * @return int tmr_direct_installment_payment_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPaymentId() {
		return $this->_properties['directInstallmentPaymentId'];
    }


    /**
     * tmr_direct_installment_payment_id の値をセットします
	 *
	 * @param int $directInstallmentPaymentId tmr_direct_installment_payment_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPaymentId($directInstallmentPaymentId) {
		$this->_properties['directInstallmentPaymentId'] = $directInstallmentPaymentId;
    }


    /**
     * tmr_deposit_money_received_type の値を返します
	 *
     * @return varchar tmr_deposit_money_received_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDepositMoneyReceivedType() {
		return $this->_properties['depositMoneyReceivedType'];
    }


    /**
     * tmr_deposit_money_received_type の値をセットします
	 *
	 * @param varchar $depositMoneyReceivedType tmr_deposit_money_received_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDepositMoneyReceivedType($depositMoneyReceivedType) {
		$this->_properties['depositMoneyReceivedType'] = $depositMoneyReceivedType;
    }


    /**
     * tmr_money_received_amount の値を返します
	 *
     * @return int tmr_money_received_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedAmount() {
		return $this->_properties['moneyReceivedAmount'];
    }


    /**
     * tmr_money_received_amount の値をセットします
	 *
	 * @param int $moneyReceivedAmount tmr_money_received_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedAmount($moneyReceivedAmount) {
		$this->_properties['moneyReceivedAmount'] = $moneyReceivedAmount;
    }


    /**
     * tmr_money_received_plan_date の値を返します
	 *
     * @return date tmr_money_received_plan_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedPlanDate() {
		return $this->_properties['moneyReceivedPlanDate'];
    }


    /**
     * tmr_money_received_plan_date の値をセットします
	 *
	 * @param date $moneyReceivedPlanDate tmr_money_received_plan_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedPlanDate($moneyReceivedPlanDate) {
		$this->_properties['moneyReceivedPlanDate'] = $moneyReceivedPlanDate;
    }


    /**
     * tmr_money_received_date の値を返します
	 *
     * @return date tmr_money_received_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedDate() {
		return $this->_properties['moneyReceivedDate'];
    }


    /**
     * tmr_money_received_date の値をセットします
	 *
	 * @param date $moneyReceivedDate tmr_money_received_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedDate($moneyReceivedDate) {
		$this->_properties['moneyReceivedDate'] = $moneyReceivedDate;
    }


    /**
     * tmr_money_received_type の値を返します
	 *
     * @return varchar tmr_money_received_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedType() {
		return $this->_properties['moneyReceivedType'];
    }


    /**
     * tmr_money_received_type の値をセットします
	 *
	 * @param varchar $moneyReceivedType tmr_money_received_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedType($moneyReceivedType) {
		$this->_properties['moneyReceivedType'] = $moneyReceivedType;
    }


    /**
     * tmr_money_received_complete_flag の値を返します
	 *
     * @return char tmr_money_received_complete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedCompleteFlag() {
		return $this->_properties['moneyReceivedCompleteFlag'];
    }


    /**
     * tmr_money_received_complete_flag の値をセットします
	 *
	 * @param char $moneyReceivedCompleteFlag tmr_money_received_complete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedCompleteFlag($moneyReceivedCompleteFlag) {
		$this->_properties['moneyReceivedCompleteFlag'] = $moneyReceivedCompleteFlag;
    }


    /**
     * tmr_money_received_cancel_flag の値を返します
	 *
     * @return char tmr_money_received_cancel_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedCancelFlag() {
		return $this->_properties['moneyReceivedCancelFlag'];
    }


    /**
     * tmr_money_received_cancel_flag の値をセットします
	 *
	 * @param char $moneyReceivedCancelFlag tmr_money_received_cancel_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedCancelFlag($moneyReceivedCancelFlag) {
		$this->_properties['moneyReceivedCancelFlag'] = $moneyReceivedCancelFlag;
    }


    /**
     * tmr_money_received_cancel_date の値を返します
	 *
     * @return date tmr_money_received_cancel_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedCancelDate() {
		return $this->_properties['moneyReceivedCancelDate'];
    }


    /**
     * tmr_money_received_cancel_date の値をセットします
	 *
	 * @param date $moneyReceivedCancelDate tmr_money_received_cancel_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedCancelDate($moneyReceivedCancelDate) {
		$this->_properties['moneyReceivedCancelDate'] = $moneyReceivedCancelDate;
    }


    /**
     * tmr_result_file_type の値を返します
	 *
     * @return varchar tmr_result_file_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileType() {
		return $this->_properties['resultFileType'];
    }


    /**
     * tmr_result_file_type の値をセットします
	 *
	 * @param varchar $resultFileType tmr_result_file_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileType($resultFileType) {
		$this->_properties['resultFileType'] = $resultFileType;
    }


    /**
     * tmr_result_file_name の値を返します
	 *
     * @return varchar tmr_result_file_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileName() {
		return $this->_properties['resultFileName'];
    }


    /**
     * tmr_result_file_name の値をセットします
	 *
	 * @param varchar $resultFileName tmr_result_file_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileName($resultFileName) {
		$this->_properties['resultFileName'] = $resultFileName;
    }


    /**
     * tmr_result_file_row_number の値を返します
	 *
     * @return int tmr_result_file_row_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileRowNumber() {
		return $this->_properties['resultFileRowNumber'];
    }


    /**
     * tmr_result_file_row_number の値をセットします
	 *
	 * @param int $resultFileRowNumber tmr_result_file_row_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileRowNumber($resultFileRowNumber) {
		$this->_properties['resultFileRowNumber'] = $resultFileRowNumber;
    }


    /**
     * tmr_result_file_id の値を返します
	 *
     * @return varchar tmr_result_file_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileId() {
		return $this->_properties['resultFileId'];
    }


    /**
     * tmr_result_file_id の値をセットします
	 *
	 * @param varchar $resultFileId tmr_result_file_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileId($resultFileId) {
		$this->_properties['resultFileId'] = $resultFileId;
    }


    /**
     * tmr_result_file_upload_datetime の値を返します
	 *
     * @return datetime tmr_result_file_upload_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultFileUploadDatetime() {
		return $this->_properties['resultFileUploadDatetime'];
    }


    /**
     * tmr_result_file_upload_datetime の値をセットします
	 *
	 * @param datetime $resultFileUploadDatetime tmr_result_file_upload_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultFileUploadDatetime($resultFileUploadDatetime) {
		$this->_properties['resultFileUploadDatetime'] = $resultFileUploadDatetime;
    }


    /**
     * tmr_staff_id の値を返します
	 *
     * @return int tmr_staff_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * tmr_staff_id の値をセットします
	 *
	 * @param int $staffId tmr_staff_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * tmr_imperfection_flag の値を返します
	 *
     * @return char tmr_imperfection_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * tmr_imperfection_flag の値をセットします
	 *
	 * @param char $imperfectionFlag tmr_imperfection_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * tmr_delete_flag の値を返します
	 *
     * @return char tmr_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tmr_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tmr_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tmr_deletion_datetime の値を返します
	 *
     * @return datetime tmr_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tmr_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tmr_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tmr_update_datetime の値を返します
	 *
     * @return datetime tmr_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tmr_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tmr_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tmr_registration_datetime の値を返します
	 *
     * @return datetime tmr_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tmr_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tmr_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tmr_update_timestamp の値を返します
	 *
     * @return timestamp tmr_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tmr_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tmr_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























