<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Digits
 */
require_once 'Zend/Validate/Digits.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Digits.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Digits extends Zend_Validate_Digits
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_DIGITS   => "%COLUMN_NAME%に数字以外の文字が含まれています",
        self::STRING_EMPTY => "%COLUMN_NAME%は空の文字列です"
    );
}
