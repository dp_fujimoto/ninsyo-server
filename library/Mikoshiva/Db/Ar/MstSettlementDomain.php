<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstSettlementDomain
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstSettlementDomain extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_settlement_domain';
    protected $_primary  = 'mdo_settlement_domain_id';
    protected $_metadata = array (
  'mdo_settlement_domain_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_settlement_domain_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'mdo_division_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_division_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_settlement_domain_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_settlement_domain_name',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_thanks_from_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_thanks_from_name',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_thanks_from_email' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_thanks_from_email',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '250',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_support_from_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_support_from_name',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_support_from_email' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_support_from_email',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '250',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_delete_flag',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_deletion_datetime',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_update_datetime',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_registration_datetime',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mdo_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_settlement_domain',
    'COLUMN_NAME' => 'mdo_update_timestamp',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

