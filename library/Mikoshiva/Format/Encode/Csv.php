<?php
/** @package Mikoshiva_Format */

/**
 * require
 */
require_once 'Mikoshiva/Format/Encode/Abstract.php';

/**
 *
 *
 * @package Mikoshiva_Format
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: Csv.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */

class Mikoshiva_Format_Encode_Csv extends Mikoshiva_Format_Encode_Abstract {
    /**
     * フォーマット
     * @var unknown_type
     */
    protected $_format = 'ENCLOSED_IN_DOUBLE_QUOTES';

    /**
     *
     *
     * @param unknown_type $format ENCLOSED_IN_DOUBLE_QUOTES ダブルクォーテーション囲み　デフォルト
     *                             NOT_ENCLOSED_IN_DOUBLE_QUOTES ダブルクォーテーション無し
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/22
     * @version SVN:$Id: Csv.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setFormat($format) {
        $this->_format = $format;
    }



    /**
     *
     *
     * @param  array $data
     * @return string
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/22
     * @version SVN:$Id: Csv.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function process( array $data ) {
        switch($this->_format) {
            case 'NOT_ENCLOSED_IN_DOUBLE_QUOTES':
                break;
            case 'ENCLOSED_IN_DOUBLE_QUOTES':
                foreach($data as $k => $v) {
                    $data[$k] = '"' . str_replace('"', '""', $v) . '"';
                }
                break;
            default:
                throw new Mikoshiva_Exception();
        }
        return implode( ',', $data ) . "\n";
    }
}