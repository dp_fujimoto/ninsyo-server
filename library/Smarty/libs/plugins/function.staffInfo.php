<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Db/Ar/MstStaff.php';

/**
 * スタッフID及び、カラム名を元に、スタッフ情報を返します。
 * カラム名を複数指定した場合、それらを連結して返します。
 *
 * @param array $params
 *                      string  staffId             スタッフID
 *                      string  column              カラム名、またはカラム名の配列(array(column1, column2, ...)
 *                      string  separator  空文字 | 戻り値のセパレータ(column1_data[separator]column2_data[separator]...columnN_data)
 * @param Smarty   $smarty
 *
 * @return string
 *
 * @package Smarty_libs
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.staffInfo.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_staffInfo($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['staffId']      = '';
    $default['column']       = '';
    $default['separator']    = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['staffId']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 staffId に値がありません。');
    }
    if(isBlankOrNull($data['column']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 column に値がありません。');
    }

    $staffId   = (string)$data['staffId'];
    $column    = $data['column'];
    $separator = $data['separator'];

    //-------------------------------------------------------------------------------------------
    //■スタッフテーブルからリストを取得
    //-------------------------------------------------------------------------------------------

    //キャッシュに存在する場合はキャッシュから取得
    $cache = Zend_Registry::get('cache');
    $staffList = array();
    $row       = array();
    if ( ! $row = $cache->load('smarty_function_staffInfo') ) {
        $ar        = new Mikoshiva_Db_Ar_MstStaff();
        $select    = $ar->select()->where('mst_delete_flag = ?', '0');
        $staffList = $ar->fetchAll( $select )->toArray();
        if(is_null($staffList)) {
            Mikoshiva_Logger::info(LOG_END);
            return '';
        }

        // キャッシュに書き込み
        $cache->save( $staffList, 'smarty_function_staffInfo');
    } else {

        // キャッシュから読み込み
        $staffList = $row;
    }

    // 1 件も取得出来なければ空文字を返す
    if( count( $staffList ) === 0 ) {
        Mikoshiva_Logger::info(LOG_END);
        return '';
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------

    // リストから対象スタッフのデータを取得
    $theStaff = array();
    foreach($staffList as $value) {
        if($value['mst_staff_id'] === $staffId) {
            $theStaff = $value;
            break;
        }
    }

    //対象スタッフが見つからなかった場合
    if(!isset($theStaff['mst_staff_id'])) {
        Mikoshiva_Logger::info(LOG_END);
        return '';
    }

    // データを出力用の文字列に変換
    $result = '';
    if(is_array($column)) { //第二引数が配列の場合
        foreach($column as $v) {
            $info[] = $theStaff[$v];
        }
        $result = implode($separator, $info);
    } else {                //第二引数が配列でない場合
        $result = $theStaff[$column];
    }

    Mikoshiva_Logger::info(LOG_END);
    return $result;
}


?>
