<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstZipCode
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstZipCode extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_zip_code';
    protected $_primary  = 'mzc_zip_code_id';
    protected $_metadata = array (
  'mzc_zip_code_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_zip_code_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'mzc_zip_code' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_zip_code',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '7',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mzc_prefecture_name_kana' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_prefecture_name_kana',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '10',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mzc_address1_kana' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_address1_kana',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mzc_address2_kana' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_address2_kana',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mzc_prefecture_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_prefecture_name',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '5',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mzc_address1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_address1',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '50',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mzc_address2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_zip_code',
    'COLUMN_NAME' => 'mzc_address2',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

