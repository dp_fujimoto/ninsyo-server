<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * -----------------------------------------------------------------------
 *    ワンポイント
 * -----------------------------------------------------------------------
 *    ■クラス名
 *    アクションフォームクラスのクラス名は、
 *    必ず「～Form」としてください。
 *
 *    ■resetメソッド
 *    任意の初期化処理を行う場合に実装します。
 *    (メソッド定義は省略可能です)
 *
 *    ■validateメソッド
 *    任意のバリデート処理を行う場合に実装します。(独自バリデート)
 *    validation.yml定義による固定バリデート処理を実行するには、
 *    スーパークラスのvalidateメソッドを呼び出してください。
 *    「parent::reset($config, $request);」
 *    その場合、スーパークラスのvalidateメソッドが返す
 *    固定バリデートの実行結果値は、適切に処理してください。
 *    (メソッド定義は省略可能です)
 * -----------------------------------------------------------------------
 *
 * @author        K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Content_InputForm
    extends Mikoshiva_Controller_Mapping_Form_Abstract {

    /**
     * アクションフォームの初期化を行います。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');

        if ($this->getMaxConditionNum() > 0) {

            $conditionNo            = $this->getConditionNo();
            $productCode            = $this->getProductCode();
            $serverName             = $this->getServerName();
            $designatedDate         = $this->getDesignatedDate();
            $packageNumber          = $this->getPackageNumber();
            $productShippingCode    = $this->getProductShippingCode();
            $permissionMonth        = $this->getPermissionMonth();
            $permissionDay          = $this->getPermissionDay();
            $needPeriodMonth        = $this->getNeedPeriodMonth();
            $needPeriodDay          = $this->getNeedPeriodDay();
            $campaignCode           = $this->getCampaignCode();
            $campaignSalesCode      = $this->getCampaignSalesCode();
            $subscriptionType       = $this->getSubscriptionType();
            $productType            = $this->getProductType();
            $chargeType             = $this->getChargeType();
            $cancelOkFlag           = $this->getCancelOkFlag();

            if (!is_array($conditionNo)) {
                $conditionNo = array();
                $productCode = array();
                $serverName = array();
                $designatedDate = array();
                $packageNumber = array();
                $productShippingCode = array();
                $permissionMonth = array();
                $permissionDay = array();
                $needPeriodMonth = array();
                $needPeriodDay = array();
                $campaignCode = array();
                $campaignSalesCode = array();
                $subscriptionType = array();
                $productType = array();
                $chargeType = array();
                $cancelOkFlag = array();
            }


            // この数字を増やせば、登録できる条件数が増える
            for ($i = 0; $i < $this->getMaxConditionNum(); $i++) {
                if (!isset($conditionNo[$i])) {
                    $conditionNo[$i] = 0;
                    $productCode[$i] = null;
                    $serverName[$i] = null;
                    $designatedDate[$i] = null;
                    $packageNumber[$i] = null;
                    $productShippingCode[$i] = null;
                    $permissionMonth[$i] = null;
                    $permissionDay[$i] = null;
                    $needPeriodMonth[$i] = null;
                    $needPeriodDay[$i] = null;
                    $campaignCode[$i] = null;
                    $campaignSalesCode[$i] = null;
                    $subscriptionType[$i] = null;
                    $productType[$i] = null;
                    $chargeType[$i] = null;
                    $cancelOkFlag[$i] = '0';
                }
                if (isset($productType[$i]) && $productType[$i] !== 'PRODUCT_TYPE_SINGLE') {
                    $chargeType[$i] = null;
                }
            }

            $count = count($conditionNo);
            for ($i = $this->getMaxConditionNum(); $i < $count; $i++) {
                if (isset($conditionNo[$i])) {
                    unset($conditionNo[$i]);
                }
            }


            $this->setConditionNo($conditionNo);
            $this->setProductCode($productCode);
            $this->setServerName($serverName);
            $this->setDesignatedDate($designatedDate);
            $this->setPackageNumber($packageNumber);
            $this->setProductShippingCode($productShippingCode);
            $this->setPermissionMonth($permissionMonth);
            $this->setPermissionDay($permissionDay);
            $this->setNeedPeriodMonth($needPeriodMonth);
            $this->setNeedPeriodDay($needPeriodDay);
            $this->setCampaignCode($campaignCode);
            $this->setCampaignSalesCode($campaignSalesCode);
            $this->setSubscriptionType($subscriptionType);
            $this->setProductType($productType);
            $this->setChargeType($chargeType);
            $this->setCancelOkFlag($cancelOkFlag);
        }

        parent::reset($config, $request);
        logInfo('(' . __LINE__ . ')', '終了');
    }

    /**
     * バリデートを行います。エラーMSGをリターンでかえす
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');

        $message = array();

        if ($request->getParam('action') === 'input-detail' || $request->getParam('action') === 'complete') {
            // コンテンツ名のチェック
            if (isBlankOrNull($this->getContentName())) {
                $message[] = 'コンテンツ名が未記入です。';
            } else {
//                //-----------------------------------------------
//                // コンテンツ名重複チェック
//                //-----------------------------------------------
//                $params = array();
//
//                // SQL作成
//                $sql = "";
//                $sql .= "SELECT *";
//                $sql .= " FROM mst_content";
//                $sql .= " WHERE 1 = 1";
//                $sql .= " AND mct_content_name = ?";
//                $params[] = $this->getContentName();
//
//                if (!isBlankOrNull($this->getContentId())) {
//                    $sql .= " AND mct_content_id != ?";
//                    $params[] = $this->getContentId();
//                }
//
//                $sql .= " AND mct_product_id = ?";
//                $params[] = $this->getProductId();
//
//                // SQL実行＋全件取得
//                $res = array();
//                $res = $this->_db->query($sql,$params);
//
//                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
//                    $message[] = 'すでに使われているコンテンツ名です。変更してください。';
//                    break;
//                }
            }

            // 商品名のチェック
            if (isBlankOrNull($this->getProductId())) {
                $message[] = '商品名が選択されていません。';
            }

            // コンテンツ画像URLのチェック
            if (!isBlankOrNull($this->getImageUrl())) {
                if (!preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/:\@+\$,%#]+)$/', $this->getImageUrl())) {
                    $message[] = 'コンテンツ画像URLのフォーマットが異常、もしくは使えない文字が含まれています。';
                } else {
                    try {
                        $header = get_headers($this->getImageUrl());
                    } catch (Exception $e) {
                        $message[] = 'コンテンツ画像URLに接続できません。URLを確認してください。';
                    }
                }
            }

            // メディアURLのチェック
            if ($this->getApplicationType() === 'APPLICATION_TYPE_LIBRARY') {
                if (isBlankOrNull($this->getMediaUrl())) {
                    $message[] = 'ライブラリー用なのにメディアURLが未記入です。';
                } else {
                    if (!preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/:\@+\$,%#]+)$/', $this->getMediaUrl())) {
                        $message[] = 'メディアURLのフォーマットが異常、もしくは使えない文字が含まれています。';
                    } else {
                        try {
                            $header = get_headers($this->getMediaUrl());
                        } catch (Exception $e) {
                            $message[] = 'メディアURLに接続できません。URLを確認してください。';
                        }
                    }
                }

                // メディアタイプのチェック
                if (isBlankOrNull($this->getMediaType())) {
                    $message[] = 'メディアタイプが未記入です。';
                }
            }

            // 公開日のチェック
            if (isBlankOrNull($this->getPublicationDate())) {
                $message[] = '公開日が未記入です。';
            } else {
                $temp = explode('-',$this->getPublicationDate());
                if (count($temp) !== 3 || !checkDate($temp[1],$temp[2],$temp[0])) {
                    $message[] = '公開日がフォーマットと違います。';
                }
            }

        }

        if ($request->getParam('action') === 'confirm' || $request->getParam('action') === 'complete') {
            $conditionNo = $this->getConditionNo();
            $productCode = $this->getProductCode();
            $serverName = $this->getServerName();
            $designatedDate = $this->getDesignatedDate();
            $packageNumber = $this->getPackageNumber();
            $productShippingCode = $this->getProductShippingCode();
            $cancelOkFlag = $this->getCancelOkFlag();
            $permissionMonth = $this->getPermissionMonth();
            $permissionDay = $this->getPermissionDay();
            $needPeriodMonth        = $this->getNeedPeriodMonth();
            $needPeriodDay          = $this->getNeedPeriodDay();
            $campaignCode = $this->getCampaignCode();
            $campaignSalesCode = $this->getCampaignSalesCode();
            $subscriptionType = $this->getSubscriptionType();
            $productType = $this->getProductType();
            $chargeType = $this->getChargeType();

            $case9Num = 0;
            $caseElseNum = 0;
            foreach ($conditionNo as $k => $v) {

                // 共通
                switch ($v) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        if (isBlankOrNull($serverName[$k])) {
                            $message[] = ($k+1) . '番目の条件のサーバー名が選択されていません。';
                        }
                        if (isBlankOrNull($productCode[$k])) {
                            $message[] = ($k+1) . '番目の条件の商品コードが入力されていません。';
                            echo '1';
                        }
                        if (!isBlankOrNull($campaignCode[$k]) && !preg_match('/^[0-9A-Z_-]+$/',$campaignCode[$k])) {
                            $message[] = ($k+1) . '番目の条件のオーダーフォームコードは、半角数字・半角英大文字と_と-で入力してください。';
                        }
                        if (!isBlankOrNull($campaignSalesCode[$k]) && !preg_match('/^[0-9A-Z_-]+$/',$campaignSalesCode[$k])) {
                            $message[] = ($k+1) . '番目の条件のキャンペーンコードは、半角数字・半角英大文字と_と-で入力してください。';
                        }
                        if (!isBlankOrNull($productShippingCode[$k]) && !preg_match('/^[0-9A-Z_-]+$/',$productShippingCode[$k])) {
                            $message[] = ($k+1) . '番目の条件の商品発送コードは、半角数字・半角英大文字と_と-で入力してください。';
                        }
                        if (!isBlankOrNull($packageNumber[$k]) && !is_numeric($packageNumber[$k])) {
                            $message[] = ($k+1) . '番目の条件のパッケージナンバーは、半角数字のみ入力してください。';
                        }
                        if (isBlankOrNull($productShippingCode[$k]) && !isBlankOrNull($packageNumber[$k])) {
                            $message[] = ($k+1) . '番目の条件でパッケージナンバーが指定されていますが、商品発送コードが指定されていません。';
                        }
                        $caseElseNum++;
                        break;
                    case 9:
                        $case9Num++;
                        break;

                    default:break;
                }

                // 個別
                switch ($v) {
                    case 1:
                    case 2:
                        if (isBlankOrNull($designatedDate[$k])) {
                            $message[] = ($k+1) . '番目の条件の指定日が入力されていません。';
                        } else {
                            $temp = explode('-',$designatedDate[$k]);
                            if (count($temp) !== 3 || !checkDate($temp[1],$temp[2],$temp[0])) {
                                $message[] = ($k+1) . '番目の条件の指定日がフォーマットと違います。';
                            }
                        }
                        break;
                    case 3:
                    case 4:
                        break;
                    case 5:
                        if (isBlankOrNull($cancelOkFlag[$k])) {
                            $message[] = ($k+1) . '番目の条件の解約した時の対応が選択されていません。';
                        }
                        if (!isBlankOrNull($permissionMonth[$k])) {
                            if (!is_numeric($permissionMonth[$k]) || $permissionMonth[$k] < 0) {
                                $message[] = ($k+1) . '番目の条件の許可月数に半角数字以外またはマイナスが入っています。';
                            }
                        }
                        if (!isBlankOrNull($permissionDay[$k])) {
                            if (!is_numeric($permissionDay[$k]) || $permissionDay[$k] < 0) {
                                $message[] = ($k+1) . '番目の条件の許可日数に半角数字以外またはマイナスが入っています。';
                            }
                        }
                        if (!isBlankOrNull($needPeriodMonth[$k])) {
                            if (!is_numeric($needPeriodMonth[$k]) || $needPeriodMonth[$k] < 0) {
                                $message[] = ($k+1) . '番目の条件の必要サービス期間(月)に半角数字以外またはマイナスが入っています。';
                            }
                        }
                        if (!isBlankOrNull($needPeriodDay[$k])) {
                            if (!is_numeric($needPeriodDay[$k]) || $needPeriodDay[$k] < 0) {
                                $message[] = ($k+1) . '番目の条件の必要サービス期間(日)に半角数字以外またはマイナスが入っています。';
                            }
                        }
                        break;
                    case 6:
                        break;
                    case 9:
                        break;

                    default:break;
                }
            }
            if ($case9Num > 0) {
                if ($case9Num > 1) {
                    $message[] = '「無条件に見れる」が複数選択されています。';
                }
                if ($caseElseNum > 0) {
                    $message[] = '「無条件に見れる」とそれ以外のパターンは同時に設定できません。';
                }
            }
        }

        //-----------------------------------------------
        // エラーメッセージを返す
        //-----------------------------------------------
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $message;
    }

}
