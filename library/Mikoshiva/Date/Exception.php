<?php
/**
 * @package    Mikoshiva_Date
 */



/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';


/**
 *
 *
 * @category   Mikoshiva
 * @package    Mikoshiva_Date
 * @copyright  Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Mikoshiva_Date_Exception extends Zend_Exception
{}
