<?php
/** 
 * @package Mikoshiva_Controller
 */


/**
 * Mikoshiva_Controller_Action
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/03
 * @version SVN:$Id: Action.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Controller_Action extends Zend_Controller_Action
{
    public function init()
    {
        //Zend_Layoutで使う変数 menu を menu.phtml から作成
        //$this->_helper->layout->assign('menu', $this->_helper->layout->getLayoutPath() . '/menu.phtml' );

    }
}
