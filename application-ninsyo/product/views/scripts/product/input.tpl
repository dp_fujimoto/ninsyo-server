<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">商品登録</h2>

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}" >
                <input type="hidden" name="productId" value="{$form->getProductId()}">
                <input type="hidden" name="imageNoneFlag" value="{$form->getImageNoneFlag()}" >
                <input type="hidden" name="imageUpdateDatetime" value="{$form->getImageUpdateDatetime()}" >

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ID
                        </td>
                        <td class="COM_nowrap" colspan="2">{if !isBlankOrNull($form->getProductId())}{$form->getProductId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ブランド<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            {$where = array()}
                            {$where['mbr_delete_flag'] = '0'}
                            {$where['mbr_division_id'] = $form->getDivisionId()}

                            {$attribs = array()}
                            {$attribs['onChange'] = "KO_currentTab('/product/product/confirm-back', '"|cat:$tabId|cat:"', 'inputForm', []);"}
                            {formSelectDb name='brandId' selected=$form->getBrandId() arClassName='Mikoshiva_Db_NinsyoAr_MstBrand' labelName='mbr_brand_name' valueName='mbr_brand_id' where=$where addEmpty=true attribs=$attribs}
                        </td>
                        <td class="COM_nowrap">
                            部署：
                            {$attribs = array()}
                            {$attribs['onChange'] = "KO_currentTab('/product/product/confirm-back', '"|cat:$tabId|cat:"', 'inputForm', []);"}
                            {divisionLister name="divisionId" mode="select" selected=$form->getDivisionId() attribs=$attribs}
                            　　
                            検索文字：<input type="text" onChange="PDT_productInputChgBrand{$tabId}(this.value);">
                        </td>
                    </tr>
                </table>

                <div class="{if isBlankOrNull($form->getBrandId())}COM_displayNone{/if}">
                    <table class="COM_table">
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                商品名<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="productName" value="{$form->getProductName()}" size="80" maxlength="200"><br>
                                <span class="COM_fontGrey">iosアプリ用はお客様が目にすることを前提とし、表示を考慮して長い名前に注意</span><br>
                                <span class="COM_fontGrey">web認証用の場合、サイトチェックを行うため必ずURLを入力する</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                メモ
                            </td>
                            <td class="COM_nowrap">
                                <textarea name="memo" cols="70" rows="4">{$form->getMemo()}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                状態
                            </td>
                            <td class="COM_nowrap">
                                {if !isBlankOrNull($form->getProductId())}
                                    <input type="checkbox" name="deleteFlag" value="1" {if $form->getDeleteFlag() === '1'}checked{/if}>削除する
                                {else}
                                    <input type="hidden" name="deleteFlag" value="0">新規
                                {/if}
                            </td>
                        </tr>
                    </table>

                    <br>
                    【用途がライブラリーの場合は下記の項目は入力推奨】

                    <table class="COM_table">
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                商品画像URL
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="imageUrl" value="{$form->getImageUrl()}" size="100" maxlength="500">
                                <br>
                                <span class="COM_fontGrey">リスト表示に使用  横{$smarty.const.PRODUCT_IMAGE_W}px 縦{$smarty.const.PRODUCT_IMAGE_H}pxの縮尺で作成してください</span>
                                {if $form->getImageNoneFlag() === '1'}
                                    <input type="hidden" name="imageUpdateFlag" value="1">
                                {else}
                                    <hr style="border-style: dashed;">
                                    <input type="checkbox" name="imageUpdateFlag" value="1" {if $form->getImageUpdateFlag() === '1'}checked{/if}>
                                    ←URLを変更、またはURLにある画像データを変更したらチェック
                                    <br>
                                    <span class="COM_bold">最終更新日：{$form->getImageUpdateDatetime()}</span>
                                    <br>
                                    <span class="COM_fontRed">商品画像に関して変更があった場合、チェックボックスにチェックを入れないとユーザーに反映されません</span>
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                注釈
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="description" value="{$form->getDescription()}" size="80" maxlength="132">
                                <br>
                                <span class="COM_fontGrey">お客様が目にすることを前提としたこの商品の説明文。改行不可</span>
                            </td>
                        </tr>
                    </table>


                    <br>
                    <hr>
                    <br>

                    <!-- // ページ下部のボタン開始 -->
                    <div>
                        {currentTabButton name='　　　　　　次へ　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/product/confirm" tabId=$tabId  formId='inputForm'}
                    </div>
                    <!-- // ページ下部のボタン終了 -->
                </div>

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------------
    // オーダーフォーム選択絞り
    //-----------------------------------------------------
    function PDT_productInputChgBrand{$tabId}(val) {

        var obj = document.getElementById('brandId');
        if (obj) {
            var count = 0;
            for (var i = 1; i < obj.length; i++) {
                if (i !== obj.selectedIndex) {
                    if (obj.options[i].innerHTML.search(val) != -1) {
                        obj.options[i].style.display = '';
                        obj.options[i].disabled = false;
                        count++;
                    } else {
                        obj.options[i].style.display = 'none';
                        obj.options[i].disabled = true;
                    }
                }
            }
            //alert(count+'件該当しました');
        }
    }

</script>
<!-- // カレントJavascript終了 -->