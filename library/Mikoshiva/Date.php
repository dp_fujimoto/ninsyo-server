<?php
/**
 * @package Mikoshiva_Date
 */

/**
 * require
 */
require_once 'Mikoshiva/Utility/Array.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Zend/Date.php';

/**
 * MIKOSHIVAの日付を返します
 *
 * @package Mikoshiva_Date
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Date extends Zend_Date {


    public function __construct($date = null, $part = null, $locale = 'ja') {

        if (! isBlankOrNull($date))               $dateClass = parent::__construct($date,                    $part, $locale);
        else if (isBlankOrNull(TV_MIKOSHIVA_NOW)) $dateClass = parent::__construct($_SERVER['REQUEST_TIME'], $part, $locale);
        else                                      $dateClass = parent::__construct(TV_MIKOSHIVA_NOW,         $part, $locale);

    }




    /**
     * MIKOSHIVA の現在日時を返します
     *
     * /mikoshiva/configs/test-define.php の TV_MIKOSHIVA_NOW 定数で
     * mikoshiva の現在日時を変更を行います
     *
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/04
     * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string|Zend_Locale $locale OPTIONAL Locale for parsing input
     * @return Zend_Date
     */
    public static function mikoshivaNow($format = 'yyyy-MM-dd HH:mm:ss') {

        // ログ開始
        logInfo(LOG_START);

        $dateClass = null;
        if (isBlankOrNull(TV_MIKOSHIVA_NOW)) $dateClass = new self($_SERVER['REQUEST_TIME']);
        else                                 $dateClass = new self(TV_MIKOSHIVA_NOW);


        // 終了
        logInfo(LOG_END);

        return $dateClass->toString($format);
    }

/**
 * 引数を元に、整形された文字列を返します。
 *
 * 形式　　Y-m-d H:i:s
 * 年月日のみが入力され、時分秒が入力されなかった場合は Y-m-d のみを返します。
 *
 *
 * @param numeric $Y 年(西暦)
 * @param numeric $m 月
 * @param numeric $d 日
 * @param numeric $H 時
 * @param numeric $i 分
 * @param numeric $s 秒
 * @return string $result
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/12
 * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
    public static function makeDateString($Y = null, $m = null, $d = null, $H = null, $i = null, $s = null) {

        //引数に数字/空文字/null以外がある場合、空文字を返す
        foreach(array($Y, $m, $d, $H, $i, $s) as $v) {
            if(!is_numeric($v) && !isBlankOrNull($v)) {
                return '';
            }
        }
        $date          = array();
        $formatDate    = array();
        $formatTime    = array();
        $date          = Mikoshiva_Utility_Array::removeEmpty(array($Y,  $m, $d ));
        $time          = Mikoshiva_Utility_Array::removeEmpty(array($H,  $i, $s ));
        $separaterDate = '-';
        $separaterTime = ':';

        $result  = '';
        $result .= implode($separaterDate, $date);

        if(!isBlankOrNull($result) && !isBlankOrNull($H)) {
            $result .= ' ';
        }
        $result .= implode($separaterTime, $time);

        return $result;
    }

    /**
     * datetime型の文字列、$date1, $date2を比較し、
     * $date1 >  $date2 (date1がdate2よりも未来の場合）の時、trueを返します。
     * $date1 <= $date2 の時、falseを返します。
     *
     * @param string $date1 datetime型
     * @param string $date2 datetime型
     * @return boolean
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function compareDateString($date1, $date2) {
        logInfo(LOG_START);
        Mikoshiva_Logger::debug('START compareDateString(' . $date1 . ', ' . $date2 . ')');

        $result = false;
        $zendDate = new Mikoshiva_Date();
        //$zendDate->setOptions(array('format_type' => 'php'));
        $zendDate->set($date1, 'yyyy-MM-dd HH:mm:ss');

        if($zendDate->isLater(Mikoshiva_Date::datetimeToTimestamp($date2))) {
            $result = true;
        }

        logInfo(LOG_END);
        return $result;
    }

    /**
     * 第1引数と第2引数間の期間計算を行います。
     * 戻り値 array
     *  $result['type']   = $toが大きい場合「"after"」, $toが小さい場合「"before"」
     *  $result['days']   = days(日)
     *  $result['hour']   = hour(時)
     *  $result['minute'] = minute(分)
     *  $result['second'] = second(秒)
     *
     * @param Mikoshiva_Date|string<'yyyy-MM-dd HH:mm:ss'>|string<'yyyy-MM-dd'> $from
     * @param Mikoshiva_Date|string<'yyyy-MM-dd HH:mm:ss'>|string<'yyyy-MM-dd'> $to
     * @return array
     */
    public static function timeFrameComputation($from, $to) {
        logInfo(LOG_START);

        // 初期化
        $intFromTimestamp = 0;
        $intToTimestamp   = 0;

        // unixタイムスタンプで取得します。
        if ($from instanceof Mikoshiva_Date) {
            // Mikoshiva_Date型の場合
            $intFromTimestamp = $from->getTimestamp();

        } else if (is_string($from)) {
            // string の場合
            $dateFrom = null;
            $dateFrom = new Mikoshiva_Date($from);
            $intFromTimestamp = $dateFrom->getTimestamp();

        } else {
            // エラー
            include_once 'Mikoshiva/Date/Exception.php';
            throw new Mikoshiva_Date_Exception( '第1引数(from)に未対応のオブジェクトが渡されています。' );
        }

        // unixタイムスタンプで取得します。
        if ($to instanceof Mikoshiva_Date) {
            // Mikoshiva_Date型の場合
            $intToTimestamp = $to->getTimestamp();

        } else if (is_string($to)) {
            // string の場合
            $dateTo = null;
            $dateTo = new Mikoshiva_Date($to);
            $intToTimestamp = $dateTo->getTimestamp();

        } else {
            // エラー
            include_once 'Mikoshiva/Date/Exception.php';
            throw new Mikoshiva_Date_Exception( '第2引数(to)に未対応のオブジェクトが渡されています。' );
        }

        // 計算開始
        $diffTimestamp = 0;
        $diffTimestamp = $intToTimestamp - $intFromTimestamp;    // 差を取得

        $diffTime = 0;
        $diffTime = $diffTimestamp % 86400;                     // daysのあまり(時間部分)計算

        // 日時期間を計算します。
        $result = array();
        $result['type']   = 'after';
        $result['days']   = (int)($diffTimestamp / 86400);      // days計算(日)
        $result['hour']   = (int)($diffTime / 3600);            // hour計算(時)
        $result['minute'] = (int)(($diffTime % 3600) / 60);     // minute計算(分)
        $result['second'] = (int)(($diffTime % 3600) % 60);     // second計算(秒)

        // マイナス値の場合
        // 各値のマイナス指定をクリアして「type=before」をセットします。
        if ($intFromTimestamp > $intToTimestamp) {
            // マイナス
            $result['type'] = 'before';

            // マイナス値を消去
            $result['days']   *= -1;
            $result['hour']   *= -1;
            $result['minute'] *= -1;
            $result['second'] *= -1;
        }

        logInfo(LOG_END);
        return $result;
    }

    /**
     * datetime型からUnixタイムスタンプへの変換を行います。
     *
     * @param string $date  datetime型
     * @return string       UnixTimestamp
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function datetimeToTimestamp($date) {
        logInfo(LOG_START);
        Mikoshiva_Logger::debug('START datetimeToTimestamp(' . $date . ')');
        $zendDate = new Zend_Date();
        //$zendDate->setOptions(array('format_type' => 'php'));

        logInfo(LOG_END);
        //Zend_Date::setはセットした時間のタイムスタンプを返す
        return $zendDate->set($date, 'yyyy-MM-dd HH:mm:ss');
    }


    /**
     * 指定された月の末日を取得します
     *
     * @param int $month 設定された月からの指定月数
     * @param $formt
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/12
     * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getEndMonthDate($month = 0, $formt = 'yyyy-MM-dd') {


        $date = $this->toString('yyyy-MM-dd');

        $dateClass = new self($date);
        return $dateClass->setDay(1)->addMonth($month + 1)->subDay(1)->toString($formt);
    }

}



































