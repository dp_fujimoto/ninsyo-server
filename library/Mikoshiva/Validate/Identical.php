<?php
/**
 * @package Mikoshiva_Validate
 */


/**
 * @see Zend_Validate_StringLength
 */
require_once 'Zend/Validate/Identical.php';


/**
 *
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/20
 * @version SVN:$Id: Identical.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Identical extends Zend_Validate_Identical
{

    /**
     *
     * @var unknown_type
     */
    protected $_messageTemplates = array(

        self::NOT_SAME      => "【%COLUMN_NAME%】には %token% を入力して下さい。",
        self::MISSING_TOKEN => 'トークンがありません。',
    );
}
