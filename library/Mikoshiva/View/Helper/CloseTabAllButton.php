<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/CloseTabAll.php';

/**
 * 全てのタブを閉じるjavascriptをボタンタグに実装した形で返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: CloseTabAllButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_CloseTabAllButton extends Mikoshiva_View_Helper_Abstract {

    /**
     * 全てのタブを閉じるjavascriptをボタンタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: CloseTabAllButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param unknown_type $name
     * @return string
     */
    public function closeTabAllButton($name) {
        $a = new Mikoshiva_View_Helper_CloseTabAll();
        return '<input type="button" value="' . $name . '" onclick="' . $a->closeTabAll() . '">';
    }
}
