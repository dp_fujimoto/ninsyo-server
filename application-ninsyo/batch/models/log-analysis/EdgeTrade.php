<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Mikoshiva/Batch/Exception.php';
require_once 'Mikoshiva/Db/Simple.php';

require_once 'batch/models/classes/ReportManagerWithProgressBar.php';

/**
 *
 * @author      fujimoto <fujimoto@d-publishing.jp>
 * @since       2012/07/09
 */
class Batch_Models_LogAnalysis_EdgeTrade extends Mikoshiva_Controller_Model_Db_Abstract {


    private $_reportClass;

    private $_ninsyoLogDirPath = '/api';
    private $_accessLogTotalizationType = 'ACCESS_LOG_TOTALIZATION_TYPE_EDGE_TRADER';


    /**
     * メインメソッド
     *
     * @param  Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param  Mikoshiva_Controller_Mapping_ActionContext  $context
     * @return boolean
     * @see    Mikoshiva_Controller_Model_Abstract::execute()
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm, Mikoshiva_Controller_Mapping_ActionContext $context) {

        //------------------------------------------------------------------
        //
        // ■レポートクラスを生成
        //
        //------------------------------------------------------------------
        $this->_reportClass = Batch_Models_Classes_ReportManagerWithProgressBar::getInstance();
        $this->_reportClass->setLogFileName('Batch_Models_LogAnalysis_EdgeTrade');

        //------------------------------------------------------------------
        //
        // ■ログに開始のアナウンスを書き込む
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('#######################################################################');
        $this->_reportClass->log('#   ▼Batch_Models_LogAnalysis_EdgeTrade                    START▼   #');
        $this->_reportClass->log('#######################################################################');


        //------------------------------------------------------------------
        //
        // ■処理時間を計測
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('■処理時間を計測');
        $this->_reportClass->setStockProcessTimeStart(get_class($this));


        //------------------------------------------------------------------
        //
        // ■初期化
        //
        //------------------------------------------------------------------
        $checkFileList = array();
        $config = $context->getConfig();
        $errorFlag = '0';

        //------------------------------------------------------------------
        //
        // ■リクエストの情報をログに書込
        //
        //------------------------------------------------------------------
        foreach ($actionForm->toArrayProperty() as $k => $v) {
            $this->_reportClass->log($k . ' = ' . $v);
        }

        $dateClass = new DateTime($actionForm->getDate());
        $dateClass->modify('-1 day');
        $yesterdayDate = $dateClass->format('Y-m-d');

        if (date('Y-m-d') > $yesterdayDate) {

            //-----------------------------------------------
            //
            // ■認証ログファイル処理
            //
            //-----------------------------------------------
            $resDir = opendir(LOGS_DIR . '/temporary/' . $this->_ninsyoLogDirPath);
            $filePathList = array();
            while ($fileName = readdir($resDir)) {
                if (strstr($fileName,'GetEdgeTradeUrl_') !== false && strstr($fileName,$yesterdayDate) !== false) {
                    $filePathList[$fileName] =  LOGS_DIR . '/temporary/' . $this->_ninsyoLogDirPath  . '/' . $fileName;
                    break;
                }
            }
            closedir($resDir);
            asort($filePathList);

            $totalUserData = array();
            $analysisData = array();
            $checkData = array();
            $rowCount = 0;
            foreach ($filePathList as $fileName => $filePath) {
                $this->_reportClass->log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                $this->_reportClass->log($fileName . 'の処理開始');

                $fp = null;
                $fp = fopen($filePath,'r');

                $checkFileList[] = $fileName;
                while($row = fgets($fp)) {
                    $rowCount++;
                    $checkData[$fileName . ':' . $rowCount]['data'] = $row;
                    $checkData[$fileName . ':' . $rowCount]['check'] = '0';
                    $tempArr = explode(': [',$row);
                    if (count($tempArr) == 2) {
                        $date = substr($tempArr[0],0,10);
                        $datetime = substr($tempArr[0],0,13) . ':00:00';
                        $tempArr2 = explode(' ID:',$row);
                        if (count($tempArr2) == 2) {
                            $tempArr3 = explode(' PASS:',$tempArr2[1]);
                            if (count($tempArr3) == 2) {
                                $userId = $tempArr3[0];
                                $tempArr4 = explode(' RESULT:',$tempArr3[1]);
                                if (count($tempArr4) == 2) {
                                    $tempArr5 = explode(' KEY:',$tempArr4[1]);
                                    $userResult = trim($tempArr5[0]);

                                    switch ($userResult) {
                                        case 'comspread_cycle.php': $userResult = 'ACCESS_LOG_DETAIL_TYPE_COMSPREAD_CYCLE'; break;
                                        case 'combasic_cycle.php': $userResult = 'ACCESS_LOG_DETAIL_TYPE_COMBASIC_CYCLE'; break;
                                        case 'comspread.php': $userResult = 'ACCESS_LOG_DETAIL_TYPE_COMSPREAD'; break;
                                        case 'combasic.php': $userResult = 'ACCESS_LOG_DETAIL_TYPE_COMBASIC'; break;
                                        default: $userResult = 'ACCESS_LOG_DETAIL_TYPE_FAILURE'; break;
                                    }
                                    if (!isset($totalUserData[$date])) {
                                        $totalUserData[$date] = array();
                                    }
                                    if (!isset($analysisData[$datetime])) {
                                        $analysisData[$datetime] = array();
                                    }
                                    if (!isset($analysisData[$datetime][$userResult])) {
                                        $analysisData[$datetime][$userResult] = array();
                                    }
                                    if (!isset($analysisData[$datetime][$userResult][$userId])) {
                                        $analysisData[$datetime][$userResult][$userId] = 0;
                                    }
                                    if (!isset($totalUserData[$date][$userId])) {
                                        $totalUserData[$date][$userId] = 0;
                                    }
                                    $totalUserData[$date][$userId]++;
                                    $analysisData[$datetime][$userResult][$userId]++;
                                    $checkData[$fileName . ':' . $rowCount]['check'] = '1';
                                }
                            }
                        }
                    }
                }
                fclose($fp);
            }

            //-----------------------------------------------
            //
            // ■結果を集計してＤＢに
            //
            //-----------------------------------------------
            $accessLogTotalizationIds = array();
            foreach ($totalUserData as $totalUserDataDate => $totalUserDataRow) {

                //-----------------------------------------------
                // アクセスログ集計テーブル
                //-----------------------------------------------
                // 検索条件
                $where = array();
                $where['accessLogTotalizationType'] = $this->_accessLogTotalizationType;
                $where['accessDate'] = $totalUserDataDate;
                $where['deleteFlag']  = '0';

                // SQL実行＋1件取得
                $db = null;
                $db = new Mikoshiva_Db_Simple();

                $tatPopo = null;
                $tatPopo = $db->simpleOneSelect('Mikoshiva_Db_NinsyoAr_TrxAccessLogTotalization', $where);

                $accessCount = 0;
                foreach ($totalUserDataRow as $rowUserId => $rowCount) {
                    $accessCount += $rowCount;
                }

                $tatPopo->setAccessLogTotalizationType($this->_accessLogTotalizationType);
                $tatPopo->setAccessDate($totalUserDataDate);
                $tatPopo->setAccessCount($accessCount);
                $tatPopo->setUserCount(count($totalUserDataRow));
                $tatPopo->setDeleteFlag('0');
                $tatPopo->setDeletionDatetime(null);

                $res = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_TrxAccessLogTotalization',$tatPopo);
                $this->_reportClass->log('アクセスログ集計テーブルID：' . $res->getAccessLogTotalizationId() . 'の登録');

                $accessLogTotalizationIds[$totalUserDataDate] = $res->getAccessLogTotalizationId();
            }

            foreach ($analysisData as $analysisDatetime => $analysisDataRow) {

                $this->_reportClass->log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                $this->_reportClass->log($analysisDatetime . 'のアクセス結果処理開始');

                $tempDate = substr($analysisDatetime,0,10);
                $tempHour = substr($analysisDatetime,11,2);

                //-----------------------------------------------
                // アクセスログ解析テーブル
                //-----------------------------------------------
                // 検索条件
                $where = array();
                $where['accessLogTotalizationId'] = $accessLogTotalizationIds[$tempDate];
                $where['accessHour'] = $tempHour;
                $where['deleteFlag']  = '0';

                // SQL実行＋1件取得
                $db = null;
                $db = new Mikoshiva_Db_Simple();

                $tanPopo = null;
                $tanPopo = $db->simpleOneSelect('Mikoshiva_Db_NinsyoAr_TrxAccessLogAnalysis', $where);

                $accessCount = 0;
                $tempUserIds = array();
                foreach ($analysisDataRow as $detailType => $detailRow) {
                    foreach ($detailRow as $detailRowUserId => $detailRowCount) {
                        $accessCount += $detailRowCount;
                        $tempUserIds[$detailRowUserId] = 1;
                    }
                }

                $tanPopo->setAccessLogTotalizationId($accessLogTotalizationIds[$tempDate]);
                $tanPopo->setAccessHour((int)$tempHour);
                $tanPopo->setAccessCount($accessCount);
                $tanPopo->setUserCount(count($tempUserIds));
                $tanPopo->setDeleteFlag('0');
                $tanPopo->setDeletionDatetime(null);

                $res = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_TrxAccessLogAnalysis',$tanPopo);
                $this->_reportClass->log('アクセスログ解析テーブルID：' . $res->getAccessLogAnalysisId() . 'の登録');

                $accessLogAnalysisId = $res->getAccessLogAnalysisId();

                foreach ($analysisDataRow as $detailType => $detailRow) {

                    //-----------------------------------------------
                    // アクセスログ解析詳細テーブル
                    //-----------------------------------------------
                    // 検索条件
                    $where = array();
                    $where['accessLogAnalysisId'] = $accessLogAnalysisId;
                    $where['accessLogDetailType'] = $detailType;
                    $where['deleteFlag']  = '0';

                    // SQL実行＋1件取得
                    $db = null;
                    $db = new Mikoshiva_Db_Simple();

                    $tadPopo = null;
                    $tadPopo = $db->simpleOneSelect('Mikoshiva_Db_NinsyoAr_TrxAccessLogAnalysisDetail', $where);

                    $accessCount = 0;
                    $userArr = array();
                    $userArr[] = '[対象ユーザーID]';
                    foreach ($detailRow as $detailRowUserId => $detailRowCount) {
                        $accessCount += $detailRowCount;
                        if (isBlankOrNull($detailRowUserId)) {
                            $detailRowUserId = '未入力';
                        }
                        $userArr[] = $detailRowUserId;
                    }
                    $memo = implode('|',$userArr);

                    $tadPopo->setAccessLogAnalysisId($accessLogAnalysisId);
                    $tadPopo->setAccessLogDetailType($detailType);
                    $tadPopo->setAccessCount($accessCount);
                    $tadPopo->setUserCount(count($detailRow));
                    $tadPopo->setMemo($memo);
                    $tadPopo->setDeleteFlag('0');
                    $tadPopo->setDeletionDatetime(null);

                    $res = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_TrxAccessLogAnalysisDetail',$tadPopo);
                    $this->_reportClass->log('アクセスログ解析詳細ID：' . $res->getAccessLogAnalysisDetailId() . 'の登録');
                }
            }

            //------------------------------------------------------------------
            //
            // ■リポートメール送信
            //
            //------------------------------------------------------------------
            $this->_reportClass->log('■リポートメール送信');

            if ($errorFlag === '1') {
                $title = '[' . $actionForm->getDate() . '] エッジトレーダーアクセスログ解析処理バッチでエラー';
            } else {
                $title = '[' . $actionForm->getDate() . '] エッジトレーダーアクセスログ解析処理バッチが完了';
            }

            $body   = "";
            $body  .= "[実行日]"          . "\n";
            $body  .= "{$actionForm->getDate()}"           . "\n";
            $body .= ""               . "\n";
            $body .= "[処理ファイル]" . "\n";
            foreach ($checkFileList as $k => $fileName) {
                $body .= $fileName      . "\n";
            }
            $body .= ""               . "\n";
            $body .= "[NG行]" . "\n";
            foreach ($checkData as $k => $v) {
                if ($v['check'] != '1') {
                    $body .= $k . "\n";
                }
            }

            // 送信
            $this->_reportClass->sendReportMailBase($title,$body);
        }

        //------------------------------------------------------------------
        //
        // ■処理時間を計測（終了）
        //
        //------------------------------------------------------------------
        $this->_reportClass->setStockProcessTimeEnd(get_class($this));


        //------------------------------------------------------------------
        //
        // ■ログに終了のアナウンスを書き込む
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('############################################################');
        $this->_reportClass->log('#  ▲Batch_Models_LogAnalysis_EdgeTrade            END▲   #');
        $this->_reportClass->log('############################################################');

        return true;
    }

}