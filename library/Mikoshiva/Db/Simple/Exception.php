<?php
/**
 * @package Mikoshiva_Db
 */

/**
 * 
 * 
 * @package Mikoshiva_Db
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/18
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Simple_Exception extends Zend_Db_Table_Exception {

}