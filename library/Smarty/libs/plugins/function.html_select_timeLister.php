<?php


/**
 * {html_select_time}を拡張したものです。
 * 基本的な仕様は同プラグインに従います。
 * ＜相違点＞
 * ■selectタグのname/idを個別に指定できます。
 * 　指定しなければname/idは空文字になります。
 * ■セパレータを指定できます
 * 　html_select_dateと同様に、field_separatorが指定できます。
 *
 * @param    array    $params
 *                           string    hour_name         空文字     | '時'のselectタグのname, idを指定します
 *                           string    minute_name       空文字     | '分'のselectタグのname, idを指定します
 *                           string    second_name       空文字     | '秒'のselectタグのname, idを指定します
 *                           string    meridian_name     空文字     | AM/PMのselectタグのname, idを指定します
 *                           string    field_separator   空文字     | 各selectボックス間の区切り文字を指定します
 *                           timestamp time              現在の時間 | 使用する日付/時間
 *                           boolean   display_hours     TRUE       | 時を表示するかどうか
 *                           boolean   display_minutes   TRUE       | 分を表示するかどうか
 *                           boolean   display_seconds   TRUE       | 秒を表示するかどうか
 *                           boolean   display_meridian  TRUE       | am/pm を表示するかどうか
 *                           boolean   use_24_hours      TRUE       | 24 時間クロックを用いるかどうか
 *                           integer   minute_interval     1        | ドロップダウンリストの分間隔
 *                           integer   second_interval     1        | ドロップダウンリストの秒間隔
 *                           string    field_array       null       | 結果の値をこの名前の配列に渡して出力
 *                           string    all_extra         null       | 全てのselect/inputタグに拡張属性を追加
 *                           string    hour_extra        null       | 時間のselect/inputタグに拡張属性を追加
 *                           string    minute_extra      null       | 分のselect/inputタグに拡張属性を追加
 *                           string    second_extra      null       | 秒のselect/inputタグに拡張属性を追加
 *                           string    meridian_extra    null       | am/pmのselect/inputタグに拡張属性を追加
 * @param    Smarty   &$smarty
 * @param    template &$template
 *
 */
function smarty_function_html_select_timeLister($params, $smarty, $template)
{
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    require_once 'Mikoshiva/Logger.php';
    require_once 'Mikoshiva/View/Helper/Exception.php';
    $default = array();
    $default = array('hour_name' => '',
                     'minute_name' => '',
                     'second_name' => '',
                     'meridian_name' => '',
                     'field_separator' => '',
                     'field_order_format' => '');
    $data    = $params + $default;

    // 引数を変数に代入
    $separater   = $data['field_separator'];
    $hour_name   = $data['hour_name'];
    $minute_name = $data['minute_name'];
    $second_name = $data['second_name'];
    $meridian_name = $data['meridian_name'];
    $fieldOrderFormat = $data['field_order_format'];
    
    // name から id を作成。 name[test] のようなnameの場合、 name-test のようなIDを作成する
    $hour_id     = str_replace('[', '-', $hour_name);
    $hour_id     = str_replace(']', '', $hour_id);
    $minute_id   = str_replace('[', '-', $minute_name);
    $minute_id   = str_replace(']', '', $minute_id);
    $second_id   = str_replace('[', '-', $second_name);
    $second_id   = str_replace(']', '', $second_id);
    $meridian_id = str_replace('[', '-', $meridian_name);
    $meridian_id = str_replace(']', '', $meridian_id);
    
    // paramsにオリジナル(smarty)にないものがあると動作に支障が出るのでunset
    unset($params['hour_name']);
    unset($params['minute_name']);
    unset($params['second_name']);
    unset($params['meridian_name']);
    unset($params['field_separator']);
    unset($params['field_order_format']);
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------

    require_once(SMARTY_PLUGINS_DIR . 'shared.make_timestamp.php');
    require_once(SMARTY_PLUGINS_DIR . 'function.html_options.php');
    //$smarty->loadPlugin('Smarty_shared_make_timestamp');
    //$smarty->loadPlugin('Smarty_function_html_options');

    /* Default values. */
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    //$prefix = "Time_";
    $prefix = '';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
    $time = time();
    $display_hours = true;
    $display_minutes = true;
    $display_seconds = true;
    $display_meridian = true;
    $use_24_hours = true;
    $minute_interval = 1;
    $second_interval = 1;
    /* Should the select boxes be part of an array when returned from PHP?
       e.g. setting it to "birthday", would create "birthday[Hour]",
       "birthday[Minute]", "birthday[Seconds]" & "birthday[Meridian]".
       Can be combined with prefix. */
    $field_array = null;
    $all_extra = null;
    $hour_extra = null;
    $minute_extra = null;
    $second_extra = null;
    $meridian_extra = null;

    foreach ($params as $_key => $_value) {
        switch ($_key) {
            case 'prefix':
            case 'time':
            case 'field_array':
            case 'all_extra':
            case 'hour_extra':
            case 'minute_extra':
            case 'second_extra':
            case 'meridian_extra':
                $$_key = (string)$_value;
                break;

            case 'display_hours':
            case 'display_minutes':
            case 'display_seconds':
            case 'display_meridian':
            case 'use_24_hours':
                $$_key = (bool)$_value;
                break;

            case 'minute_interval':
            case 'second_interval':
                $$_key = (int)$_value;
                break;

            default:
                trigger_error("[html_select_time] unknown parameter $_key", E_USER_WARNING);
        }
    }

    $time = smarty_make_timestamp($time);

    $html_result = '';
    $html_hours = '';
    $html_minutes = '';
    $html_seconds = '';
    $html_meridian = '';

    if ($display_hours) {
        $hours = $use_24_hours ? range(0, 23) : range(1, 12);
        $hour_fmt = $use_24_hours ? '%H' : '%I';
        for ($i = 0, $for_max = count($hours); $i < $for_max; $i++)
        $hours[$i] = sprintf('%02d', $hours[$i]);
        $html_hours .= '<select name=';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    // name属性に固定の文言 Hour が入れられている部分を改変し、ヘルパー使用者の指定した名前が反映されるようにする
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
//            $html_result .= '"' . $field_array . '[' . $prefix . 'Hour]"';
            $html_hours .= '"' . $field_array . '[' . $prefix . $hour_name . ']"';
        } else {
//            $html_result .= '"' . $prefix . 'Hour"';
            $html_hours .= '"' . $prefix . $hour_name . '"';
        }
        $html_hours .= ' id="' . $hour_id . '" ';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
        if (null !== $hour_extra) {
            $html_hours .= ' ' . $hour_extra;
        }
        if (null !== $all_extra) {
            $html_hours .= ' ' . $all_extra;
        }
        $html_hours .= '>' . "\n";
        $html_hours .= smarty_function_html_options(array('output' => $hours,
                'values' => $hours,
                'selected' => strftime($hour_fmt, $time),
                'print_result' => false),
            $smarty, $template);
        $html_hours .= "</select>\n";
    }

    if ($display_minutes) {

        $all_minutes = range(0, 59);
        for ($i = 0, $for_max = count($all_minutes); $i < $for_max; $i += $minute_interval)
        $minutes[] = sprintf('%02d', $all_minutes[$i]);
        $selected = intval(floor(strftime('%M', $time) / $minute_interval) * $minute_interval);
        $html_minutes .= '<select name=';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    // name属性に固定の文言 Minute が入れられている部分を改変し、ヘルパー使用者の指定した名前が反映されるようにする
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
//            $html_result .= '"' . $field_array . '[' . $prefix . 'Minute]"';
            $html_minutes .= '"' . $field_array . '[' . $prefix . $minute_name . ']"';
        } else {
//            $html_result .= '"' . $prefix . 'Minute"';
            $html_minutes .= '"' . $prefix . $minute_name . '"';
        }
        $html_minutes .= ' id="' . $minute_id . '" ';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
        if (null !== $minute_extra) {
            $html_minutes .= ' ' . $minute_extra;
        }
        if (null !== $all_extra) {
            $html_minutes .= ' ' . $all_extra;
        }
        $html_minutes .= '>' . "\n";

        $html_minutes .= smarty_function_html_options(array('output' => $minutes,
                'values' => $minutes,
                'selected' => $selected,
                'print_result' => false),
            $smarty, $template);
        $html_minutes .= "</select>\n";
    }

    if ($display_seconds) {
        $all_seconds = range(0, 59);
        for ($i = 0, $for_max = count($all_seconds); $i < $for_max; $i += $second_interval)
        $seconds[] = sprintf('%02d', $all_seconds[$i]);
        $selected = intval(floor(strftime('%S', $time) / $second_interval) * $second_interval);
        $html_seconds .= '<select name=';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    // name属性に固定の文言 Second が入れられている部分を改変し、ヘルパー使用者の指定した名前が反映されるようにする
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
//            $html_result .= '"' . $field_array . '[' . $prefix . 'Second]"';
            $html_seconds .= '"' . $field_array . '[' . $prefix . $second_name . ']"';
        } else {
//            $html_result .= '"' . $prefix . 'Second"';
            $html_seconds .= '"' . $prefix . $second_name . '"';
        }
        $html_seconds .= ' id="' . $second_id . '" ';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------

        if (null !== $second_extra) {
            $html_seconds .= ' ' . $second_extra;
        }
        if (null !== $all_extra) {
            $html_seconds .= ' ' . $all_extra;
        }
        $html_seconds .= '>' . "\n";

        $html_seconds .= smarty_function_html_options(array('output' => $seconds,
                'values' => $seconds,
                'selected' => $selected,
                'print_result' => false),
            $smarty, $template);
        $html_seconds .= "</select>\n";
    }

    if ($display_meridian && !$use_24_hours) {

        $html_meridian .= '<select name=';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    // name属性に固定の文言 Meridian が入れられている部分を改変し、ヘルパー使用者の指定した名前が反映されるようにする
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
//            $html_result .= '"' . $field_array . '[' . $prefix . 'Meridian]"';
            $html_meridian .= '"' . $field_array . '[' . $prefix . $meridian_name . ']"';
        } else {
//            $html_result .= '"' . $prefix . 'Meridian"';
            $html_meridian .= '"' . $prefix . $meridian_name . '"';
        }
        $html_meridian .= ' id="' . $meridian_id . '" ';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
        if (null !== $meridian_extra) {
            $html_meridian .= ' ' . $meridian_extra;
        }
        if (null !== $all_extra) {
            $html_meridian .= ' ' . $all_extra;
        }
        $html_meridian .= '>' . "\n";

        $html_meridian .= smarty_function_html_options(array('output' => array('AM', 'PM'),
                'values' => array('am', 'pm'),
                'selected' => strtolower(strftime('%p', $time)),
                'print_result' => false),
            $smarty, $template);
        $html_meridian .= "</select>\n";
    }



    if(!isBlankOrNull($fieldOrderFormat)) {
        //htmlタグに被らないよう、フォーマットを保護
        $fieldOrderFormat = str_replace('H', '##H##', $fieldOrderFormat);
        $fieldOrderFormat = str_replace('i', '##i##', $fieldOrderFormat);
        $fieldOrderFormat = str_replace('s', '##s##', $fieldOrderFormat);
        $fieldOrderFormat = str_replace('am/pm', '##am/pm##', $fieldOrderFormat);
        $html_result .= str_replace('##H##', $html_hours, $fieldOrderFormat);
        $html_result = str_replace('##i##', $html_minutes, $html_result);
        $html_result = str_replace('##s##', $html_seconds, $html_result);
        $html_result = str_replace('##am/pm##', $html_meridian, $html_result);
        return $html_result;
    }

    if ($display_hours) {
        $html_result .= $html_hours;
    }

    if ($display_minutes) {
        if ($display_hours) {
            $html_result .= $separater;
        }
        $html_result .= $html_minutes;
    }

    if ($display_seconds) {
        if ($display_minutes) {
            $html_result .= $separater;
        }
        $html_result .= $html_seconds;
    }
    if ($display_meridian  && !$use_24_hours) {
        if ($display_seconds) {
            $html_result .= $separater;
        }
        $html_result .= $html_meridian;
    }

    return $html_result;
}

?>
