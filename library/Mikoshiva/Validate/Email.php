<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * require
 */
require_once 'Mikoshiva/Validate/Hostname.php';

/**
 * Email アドレスバリデータクラス
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Email extends Zend_Validate_Abstract
{
    const INVALID    = 'invalid';
    const MISS_MATCH = 'missMatch';


    /**
     * メッセージテンプレート
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID            => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
        self::MISS_MATCH         => "【%COLUMN_NAME%】 と確認用メールアドレスが一致しません。",
    );


    /**
     * リクエストデータ を保持します
     * @var array リクエストデータ を保持します
     */
    protected $_data;

    /**
     * 確認用Eメールアドレスのフォーム名 を保持します
     * @var string 確認用Eメールアドレスのフォーム名 を保持します
     */
    protected $_emailConfirmName;


    /**
     * コンストラクタ
     *
     * @param $emailConfirmName
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/29
     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct($emailConfirmName = '') {
        $this->setEmailConfirmName($emailConfirmName);
    }


    /**
     * リクエストデータ をセットします
     *
     * @param array $data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/24
     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {
        logInfo(LOG_START);
        $this->_data = $data;
        logInfo(LOG_END);
    }


    /**
     * 確認用Eメールアドレスのフォーム名 をセットします
     *
     * @param string $emailConfirmName 確認用Eメールアドレスのフォーム名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/24
     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setEmailConfirmName($emailConfirmName) {
        $this->_emailConfirmName = $emailConfirmName;
    }


    /**
     * 検証メソッド
     *
     * @param unknown_type $value
     * @return unknown_type
     * (non-PHPdoc)
     * @see Zend/Validate/Zend_Validate_Interface#isValid($value)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/29
     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function isValid($value) {


        // ログ開始
        logInfo(LOG_START);


        // ▼同一値の検証 --------------------------------------------------------
        // サーバーモードで正規表現を変える（trainingｍの場合のみ+を許可）
        $pattern = '';
        if (_SERVER_MODE_ID_ === 'training.mikoshiva') $pattern = '#^[a-zA-Z0-9._+-]+@[a-zA-Z0-9][a-zA-Z0-9.-]+(\.[a-zA-Z]+)+$#';
        else                                           $pattern = '#^[a-zA-Z0-9._-]+@[a-zA-Z0-9][a-zA-Z0-9.-]+(\.[a-zA-Z]+)+$#';

        if (preg_match($pattern, $value) !== 1) {
            $this->_error(self::INVALID);
            return false;
        }


        // ▼同一値の検証 --------------------------------------------------------
        if (! isBlankOrNull($this->_emailConfirmName)) {

            // 確認用メールアドレスが指定されていたら同じ値であるかを検証します
            if($value !== $this->_data[$this->_emailConfirmName]) {
                $this->_error(self::MISS_MATCH);
                return false;
            }
        }


        // ログ終了
        logInfo(LOG_END);
        return true;
    }
//    const CONFIRM_NOT_MATCH = 'confirmAddressInvalid';
//
//    /**
//     * @var array
//     */
//    protected $_messageTemplates = array(
//        self::INVALID            => "【%COLUMN_NAME%】 はメールアドレスの基本的な形式 local-part@hostname ではありません",
//        self::INVALID_HOSTNAME   => "【%COLUMN_NAME%】 内の%hostname%は有効なホスト名ではありません",
//        self::INVALID_MX_RECORD  => "【%COLUMN_NAME%】 内の%hostname%は有効な MX レコードではないようです",
//        self::DOT_ATOM           => "【%COLUMN_NAME%】 内の%localPart%はドットアトム形式に合いません",
//        self::QUOTED_STRING      => "【%COLUMN_NAME%】 内の%localPart%は引用文字列形式に合いません",
//        self::INVALID_LOCAL_PART => "【%COLUMN_NAME%】 内の%localPart%は有効なローカルパートではありません",
//
//
//
//        // self::INVALID            => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        // self::INVALID_FORMAT     => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        // self::INVALID_HOSTNAME   => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        // self::INVALID_MX_RECORD  => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        // self::DOT_ATOM           => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        // self::QUOTED_STRING      => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        // self::INVALID_LOCAL_PART => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        self::LENGTH_EXCEEDED    => "【%COLUMN_NAME%】 は正しい形式で入力して下さい。",
//        self::CONFIRM_NOT_MATCH  => "【%COLUMN_NAME%】 が確認用Eメールアドレスと一致しません。",
//    );
//
//    /**
//     * リクエストデータ を保持します
//     * @var array リクエストデータ を保持します
//     */
//    protected $_data;
//
//    /**
//     * 確認用Eメールアドレスのフォーム名 を保持します
//     * @var string 確認用Eメールアドレスのフォーム名 を保持します
//     */
//    protected $_emailConfirmName;
//
//    /**
//     * リクエストデータ をセットします
//     *
//     * @param array $data リクエストデータ
//     *
//     * @author M.Ikuma <ikumamasayuki@gmail.com>
//     * @since 2010/02/24
//     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
//     */
//    public function setData($data) {
//        logInfo(LOG_START);
//        $this->_data = $data;
//        logInfo(LOG_END);
//    }
//
//    /**
//     * 確認用Eメールアドレスのフォーム名 をセットします
//     *
//     * @param string $emailConfirmName 確認用Eメールアドレスのフォーム名
//     *
//     * @author M.Ikuma <ikumamasayuki@gmail.com>
//     * @since 2010/02/24
//     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
//     */
//    public function setEmailConfirmName($emailConfirmName) {
//        logInfo(LOG_START);
//        $this->_emailConfirmName = $emailConfirmName;
//        logInfo(LOG_END);
//    }
//
//    /**
//     * リクエストデータ を返します
//     *
//     * @return array $this->_data リクエストデータ
//     *
//     * @author M.Ikuma <ikumamasayuki@gmail.com>
//     * @since 2010/02/24
//     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
//     */
//    public function getData() {
//        logInfo(LOG_START);
//        return $this->_data;
//        logInfo(LOG_END);
//    }
//
//    /**
//     * 確認用Eメールアドレスのフォーム名 を返します
//     *
//     * @return string $this->_emailConfirmName 確認用Eメールアドレスのフォーム名
//     *
//     * @author M.Ikuma <ikumamasayuki@gmail.com>
//     * @since 2010/02/24
//     * @version SVN:$Id: Email.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
//     */
//    public function getEmailConfirmName() {
//        logInfo(LOG_START);
//        return $this->_emailConfirmName;
//        logInfo(LOG_END);
//    }
//
//
//
//    public function __construct($emailConfirmName = '', $allow = Zend_Validate_Hostname::ALLOW_DNS, $validateMx = false, Zend_Validate_Hostname $hostnameValidator = null)
//    {
//        $this->setEmailConfirmName($emailConfirmName);
//        parent::__construct($allow, $validateMx, $hostnameValidator);
//    }
//
//
//    /**
//     * @param Zend_Validate_Hostname $hostnameValidator OPTIONAL Mikoshiva_Validate_Hostname instance is used if omitted
//     * @param int                    $allow             OPTIONAL
//     * @return void
//     */
//    public function setHostnameValidator(Zend_Validate_Hostname $hostnameValidator = null, $allow = Zend_Validate_Hostname::ALLOW_DNS)
//    {
//        if ($hostnameValidator === null) {
//            $hostnameValidator = new Mikoshiva_Validate_Hostname($allow);
//        }
//    	parent::setHostnameValidator($hostnameValidator, $allow);
//    }
//
//    public function isValid($value) {
//        logInfo(LOG_START);
//
//        if(!isBlankOrNull($this->_emailConfirmName)) {
//            if($value !== $this->_data[$this->_emailConfirmName]) {
//                $this->_error(self::CONFIRM_NOT_MATCH);
//
//                logInfo(LOG_END);
//                return false;
//            }
//        }
//        logInfo(LOG_END);
//        return parent::isValid($value);
//    }
}
