<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">コンテンツ登録2</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <input type="button" id="infoOn" value="　　　　説明表示　　　　" onClick="document.getElementById('info').className='';document.getElementById('infoOff').className='';this.className='COM_displayNone';">
            <input type="button" id="infoOff" class="COM_displayNone" value="　説明隠す　" onClick="document.getElementById('info').className='COM_displayNone';document.getElementById('infoOn').className='';this.className='COM_displayNone';">
<pre id="info" class="COM_displayNone">
■<span class="COM_fontRed COM_bold">各条件について</span>
注文取消により閲覧条件に合わなくなった場合は、見れなくなります。

パターンにマッチする人はサービス開始され、条件に合わなくなった時点でサービス終了として計算しています。


<a target="_blank" class="COM_bold" href="{$smarty.const.SUB_FOLDER_PATH}/document/operating-manual/content_sample.pdf">コンテンツ条件サンプルPDFを見る</a>

・購入したことがあること
      指定した商品コードの商品を購入していること。
      ただし、期間を設定できる。解約した場合にどうするか選べる。
      ※対象商品を買ったことがある人全員に見せたい場合

・指定日以前に購入していること
      指定した商品コードの商品を指定日以前に購入。解約日以降の分は見れない。（指定日の購入は除く）
      ※購入日より指定日が過去のコンテンツは見れない。

・指定日がサービス期間中であったこと
      指定した商品コードの商品を購入。指定日がサービス期間に含まれている、且つ解約日より前であること。
      ※サービス期間商品購入者が対象。サービス課金商品にのみ使ってください。
{*
・指定した発送物を持っていること
      指定した商品コードの商品を購入していること。指定したパッケージナンバーの発送データがあること。
      商品発送コードを限定することも可能。発送状況は考慮しない。（未発送でも見れる）
      ※主に発送物に対する課金系の商品購入者が対象。
*}
・購読中であること
      指定した商品コードの商品を購入。解約した場合でも、解約日から最大で１ヶ月間のサービス期間中有効。
      この条件を付与したコンテンツは、閲覧許可者以外は過去に見れていた分も見れなくなる。
      ※単発以外の商品購入者が対象　　基本的にIOSアプリでは使わない条件
{*
・一括購読中であること
      指定した商品コードの一括購読商品（年間など）を購入。解約した場合でも、解約日から最大で１ヶ月間のサービス期間中有効。
      この条件を付与したコンテンツは、閲覧許可者以外は過去に見れていた分も見れなくなる。
      ※単発以外で年間やマイクロ一括購入者が対象　　基本的にIOSアプリでは使わない条件
*}
</pre>
            <br>
            <br>

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。

            <br>
            <br>

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}" >
                {if $form->getApplicationType() !== 'APPLICATION_TYPE_WEB_PAGE_AUTH'}

                {/if}

                {$deleteList = array()}
                {$deleteList[] = 'tokenId'}
                {$deleteList[] = 'copyContentId'}
                {$deleteList[] = 'conditionNo'}
                {$deleteList[] = 'productCode'}
                {$deleteList[] = 'serverName'}
                {$deleteList[] = 'designatedDate'}
                {$deleteList[] = 'packageNumber'}
                {$deleteList[] = 'productShippingCode'}
                {$deleteList[] = 'permissionMonth'}
                {$deleteList[] = 'permissionDay'}
                {$deleteList[] = 'needPeriodMonth'}
                {$deleteList[] = 'needPeriodDay'}
                {$deleteList[] = 'campaignCode'}
                {$deleteList[] = 'campaignSalesCode'}
                {$deleteList[] = 'subscriptionType'}
                {$deleteList[] = 'productType'}
                {$deleteList[] = 'chargeType'}
                {$deleteList[] = 'availableCheckFlag'}
                {$deleteList[] = 'cancelOkFlag'}
                {$deleteList[] = 'allConditionFlag'}
                {$deleteList[] = 'authFlag'}
                {hiddenLister hiddenList=$form deleteList=$deleteList}

                {$conditionNo = $form->getConditionNo()}
                {$productCode = $form->getProductCode()}
                {$serverName = $form->getServerName()}
                {$designatedDate = $form->getDesignatedDate()}
                {$packageNumber = $form->getPackageNumber()}
                {$productShippingCode = $form->getProductShippingCode()}
                {$cancelOkFlag = $form->getCancelOkFlag()}
                {$permissionMonth = $form->getPermissionMonth()}
                {$permissionDay = $form->getPermissionDay()}
                {$needPeriodMonth = $form->getNeedPeriodMonth()}
                {$needPeriodDay = $form->getNeedPeriodDay()}
                {$campaignCode = $form->getCampaignCode()}
                {$campaignSalesCode = $form->getCampaignSalesCode()}
                {$subscriptionType = $form->getSubscriptionType()}
                {$productType = $form->getProductType()}
                {$chargeType = $form->getChargeType()}

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap">
                            判定条件１<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input name="allConditionFlag" type="radio" value="0" {if $form->getAllConditionFlag() !== '1'}checked{/if}>設定した条件の<span class="COM_fontRed">どれか</span>に該当すれば、閲覧できる<br>
                            <input name="allConditionFlag" type="radio" value="1" {if $form->getAllConditionFlag() === '1'}checked{/if}>設定した条件の<span class="COM_fontRed">全て</span>に該当すれば、閲覧できる
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap">
                            判定条件２<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input name="availableCheckFlag" type="radio" value="0" {if $form->getAvailableCheckFlag() !== '1'}checked{/if}>指定なし<br>
                            <input name="availableCheckFlag" type="radio" value="1" {if $form->getAvailableCheckFlag() === '1'}checked{/if}>請求取消分・商品金額以上の返金がある分はサービス対象外とする。
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap">
                            判定条件３<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input name="authFlag" type="radio" value="0" {if $form->getAuthFlag() !== '1'}checked{/if}>サービス期間中のみ閲覧可能<br>
                            <input name="authFlag" type="radio" value="1" {if $form->getAuthFlag() === '1'}checked{/if}>一度でも条件を満たした人は閲覧可能
                            <br>
                            ※現在継続していないが、過去に商品を購読していた人の閲覧を許可する場合は↓を選択。
                        </td>
                    </tr>
                </table>

                <br>

                <span class="COM_fontGreen">
                商品発送コードとパッケージナンバーを両方指定した場合、対象となる発送データから検索する
                <br>
                商品発送コードのみ指定した場合、対象となるオーダーフォームの購入データから検索する（発送物がない状態でもＯＫ）
                <br>
                パッケージナンバーのみ指定した場合、対象となる発送データから検索する
                </span>
                <br>

                <br>

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap">
                            NO
                        </td>
                        <td class="COM_bgColorLight COM_nowrap">
                            パターン<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_bgColorLight COM_nowrap">
                            共通条件
                        </td>
                        <td class="COM_bgColorLight COM_nowrap">
                            パターン別条件
                        </td>
                    </tr>

                    {foreach from=$conditionNo key="k" item="v"}
                    <tr>
                        <td class="COM_nowrap">
                            {$k + 1}
                        </td>
                        <td class="COM_nowrap COM_top">
                            {if $k > 0}
                                <input type="radio" name="conditionNo[{$k}]" value="0" {if $v == 0}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},0,0,0,0,0);">この条件を使用しない
                                <hr>
                            {else}
                                <input type="radio" name="conditionNo[{$k}]" value="9" {if $v == 0 || $v == 9}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},0,0,0,0,0);">無条件に見れる
                                <hr>
                            {/if}
                            <input type="radio" name="conditionNo[{$k}]" value="5" {if $v == 5}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},1,0,0,1,1);">購入したことがあること
                            <br>
                            <input type="radio" name="conditionNo[{$k}]" value="1" {if $v == 1}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},1,1,0,0,0);">指定日以前に購入していること
                            <br>
                            <input type="radio" name="conditionNo[{$k}]" value="2" {if $v == 2}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},1,1,0,0,0);">指定日にサービスINしていたこと
                            <br>
                            {*
                            <input type="radio" name="conditionNo[{$k}]" value="6" {if $v == 6}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},1,0,1,0,0);">指定した発送物を持っていること
                            <br>
                            *}
                            <input type="radio" name="conditionNo[{$k}]" value="3" {if $v == 3}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},1,0,0,0,1);">購読中であること
                            <br>
                            {*
                            <input type="radio" name="conditionNo[{$k}]" value="4" {if $v == 4}checked{/if} onClick="PDT_chgConditionType{$tabId}({$k},1,0,0,0,0);">一括購読中であること
                            <br>*}
                        </td>
                        <td class="COM_nowrap" >
                            <div id="type1_{$k}" class="{if $v == 0 || $v == 9}COM_displayNone{/if}">
                                <table class="COM_tableBorderZero">
                                    <tr>
                                        <td class="COM_nowrap">
                                            サーバー名<span class="COM_fontRed COM_bold">*</span>：
                                        </td>
                                        <td class="COM_nowrap">
                                            <select name="serverName[{$k}]">
                                                <option value="mikoshiva" {if $serverName[$k] === 'mikoshiva'}selected{/if}>mikoshiva</option>
                                                <option value="shimant" {if $serverName[$k] === 'shimant'}selected{/if}>shimant</option>
                                                <option value="gusiken" {if $serverName[$k] === 'gusiken'}selected{/if}>gusiken</option>
                                                <option value="test.mikoshiva" {if $serverName[$k] === 'test.mikoshiva'}selected{/if}>test.mikoshiva</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap" colspan="2">
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            商品コード<span class="COM_fontRed COM_bold">*</span>：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="productCode[{$k}]" value="{$productCode[$k]}" size="10" maxlength="20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            オーダーフォームコード：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="campaignCode[{$k}]" value="{$campaignCode[$k]}" size="50" maxlength="100">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            キャンペーンコード：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="campaignSalesCode[{$k}]" value="{$campaignSalesCode[$k]}" size="50" maxlength="100">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            商品発送コード：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="productShippingCode[{$k}]" value="{$productShippingCode[$k]}" size="30" maxlength="30">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            パッケージナンバー：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="packageNumber[{$k}]" value="{$packageNumber[$k]}" size="6" maxlength="6">
                                            　入力例：1・5・201208
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            購読タイプ：
                                        </td>
                                        <td class="COM_nowrap">
                                            <table>
                                                <tr>
                                                    <td class="COM_nowrap">
                                                        <input type="radio" name="subscriptionType[{$k}]" value="" {if isBlankOrNull($subscriptionType[$k])}checked{/if}>指定なし
                                                    </td>
                                                    <td class="COM_nowrap">
                                                        <input type="radio" name="subscriptionType[{$k}]" value="SUBSCRIPTION_TYPE_NORMAL" {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_NORMAL'}checked{/if}>通常<br>
                                                        <input type="radio" name="subscriptionType[{$k}]" value="SUBSCRIPTION_TYPE_YEAR" {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_YEAR'}checked{/if}>年間<br>
                                                        <input type="radio" name="subscriptionType[{$k}]" value="SUBSCRIPTION_TYPE_YEAR_CONTINUITY" {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_YEAR_CONTINUITY'}checked{/if}>年間追加<br>
                                                        <input type="radio" name="subscriptionType[{$k}]" value="SUBSCRIPTION_TYPE_MICRO_CONTINUITY_UPGRADE" {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_MICRO_CONTINUITY_UPGRADE'}checked{/if}>マイクロアップグレード<br>
                                                        <input type="radio" name="subscriptionType[{$k}]" value="VIP" {if $subscriptionType[$k] === 'VIP'}checked{/if}>VIP(特殊）
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            商品タイプ：
                                        </td>
                                        <td class="COM_nowrap">
                                            <table>
                                                <tr>
                                                    <td class="COM_nowrap">
                                                        <input type="radio" name="productType[{$k}]" value="" {if isBlankOrNull($productType[$k])}checked{/if} onClick="PDT_chgProductType{$tabId}({$k},0);">指定なし
                                                    </td>
                                                    <td class="COM_nowrap">
                                                        <input type="radio" name="productType[{$k}]" value="PRODUCT_TYPE_SINGLE" {if $productType[$k] === 'PRODUCT_TYPE_SINGLE'}checked{/if} onClick="PDT_chgProductType{$tabId}({$k},1);">
                                                        単発<br>
                                                        <input type="radio" name="productType[{$k}]" value="PRODUCT_TYPE_MICRO_CONTINUITY" {if $productType[$k] === 'PRODUCT_TYPE_MICRO_CONTINUITY'}checked{/if} onClick="PDT_chgProductType{$tabId}({$k},0);">
                                                        マイクロコンティニュイティ<br>
                                                        <input type="radio" name="productType[{$k}]" value="PRODUCT_TYPE_CONTINUITY" {if $productType[$k] === 'PRODUCT_TYPE_CONTINUITY'}checked{/if} onClick="PDT_chgProductType{$tabId}({$k},0);">
                                                        コンティニュイティ
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="charge_type_th_{$k}" class="COM_nowrap {if $productType[$k] === 'PRODUCT_TYPE_SINGLE'}{else}COM_displayNone{/if}">
                                            請求タイプ：
                                        </td>
                                        <td id="charge_type_td_{$k}" class="COM_nowrap {if $productType[$k] === 'PRODUCT_TYPE_SINGLE'}{else}COM_displayNone{/if}">
                                            <table class="">
                                                <tr>
                                                    <td class="COM_nowrap">
                                                        <input type="radio" name="chargeType[{$k}]" value="" {if isBlankOrNull($chargeType[$k])}checked{/if}>指定なし
                                                    </td>
                                                    <td class="COM_nowrap">
                                                        <input type="radio" name="chargeType[{$k}]" value="CHARGE_TYPE_CARD" {if $chargeType[$k] === 'CHARGE_TYPE_CARD'}checked{/if}>カード<br>
                                                        <input type="radio" name="chargeType[{$k}]" value="CHARGE_TYPE_CASH_ON_DELIVERY" {if $chargeType[$k] === 'CHARGE_TYPE_CASH_ON_DELIVERY'}checked{/if}>代引き<br>
                                                        <input type="radio" name="chargeType[{$k}]" value="CHARGE_TYPE_BANK_TRANSFER" {if $chargeType[$k] === 'CHARGE_TYPE_BANK_TRANSFER'}checked{/if}>銀行振込
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td class="COM_nowrap">
                            <div id="type2_{$k}" class="{if $v == 1 || $v == 2}{else}COM_displayNone{/if}">
                                <table class="COM_tableBorderZero">
                                    <tr>
                                        <td class="COM_nowrap">
                                            指定日<span class="COM_fontRed COM_bold">*</span>：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="designatedDate[{$k}]" value="{$designatedDate[$k]}" size="10" maxlength="10">
                                            <br>
                                            例：2012-06-24
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            {*
                            <div id="type3_{$k}" class="{if $v == 6}{else}COM_displayNone{/if}">
                                商品発送コードを忘れず設定すること
                                <br>
                                <br>
                                <table class="COM_tableBorderZero">
                                    <tr>
                                        <td class="COM_nowrap">
                                            パッケージナンバー<span class="COM_fontRed COM_bold">*</span>：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="packageNumber[{$k}]" value="{$packageNumber[$k]}" size="6" maxlength="6">
                                            <br>
                                            例：1 または 201206
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            *}
                            <div id="type4_{$k}" class="{if $v == 5}{else}COM_displayNone{/if}">
                                <table class="COM_tableBorderZero">
                                    <tr>
                                        <td class="COM_nowrap">
                                            解約した時<span class="COM_fontRed COM_bold">*</span>：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="radio" name="cancelOkFlag[{$k}]" value="1" {if $cancelOkFlag[$k] === '1'}checked{/if}>サービス継続
                                            <br>
                                            <input type="radio" name="cancelOkFlag[{$k}]" value="0" {if $cancelOkFlag[$k] !== '1'}checked{/if}>サービス終了
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap" colspan="2">
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="COM_nowrap">
                                            サービス継続期間：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="permissionMonth[{$k}]" value="{$permissionMonth[$k]}" size="2" maxlength="2">ヶ月＋
                                            <input type="text" name="permissionDay[{$k}]" value="{$permissionDay[$k]}" size="2" maxlength="2">日
                                            <br>
                                            購入してから閲覧許可される期間<br>(0または未入力の場合ずっと)
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="type5_{$k}" class="{if $v == 5 || $v == 3}{else}COM_displayNone{/if}">
                                <table class="COM_tableBorderZero">
                                    <tr>
                                        <td class="COM_nowrap">
                                            必要サービス期間：
                                        </td>
                                        <td class="COM_nowrap">
                                            <input type="text" name="needPeriodMonth[{$k}]" value="{$needPeriodMonth[$k]}" size="2" maxlength="2">ヶ月＋
                                            <input type="text" name="needPeriodDay[{$k}]" value="{$needPeriodDay[$k]}" size="2" maxlength="2">日
                                            <br>
                                            購入してからこの期間を超えないと権利が発生しない<br>(0または未入力の場合はチェックしない)<br>※主にiosアプリ用設定
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    {/foreach}
                </table>

                <br>
                <hr>
                <br>

                {currentTabButton name='　　　戻る　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/input-detail-back" tabId=$tabId  formId='inputForm'}
                　　　
                {currentTabButton name='　　　　　　　次へ　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/confirm" tabId=$tabId  formId='inputForm'}

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->

<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------------
    // 条件変更による表示切り替え
    //-----------------------------------------------------
    function PDT_chgConditionType{$tabId}(key,no1,no2,no3,no4,no5) {

        if (no1 == 0) {
            document.getElementById('type1_' + key).className = 'COM_displayNone';
        } else {
            document.getElementById('type1_' + key).className = '';
        }

        if (no2 == 0) {
            document.getElementById('type2_' + key).className = 'COM_displayNone';
        } else {
            document.getElementById('type2_' + key).className = '';
        }

        if (no3 == 0) {
            // document.getElementById('type3_' + key).className = 'COM_displayNone';
        } else {
            // document.getElementById('type3_' + key).className = '';
        }

        if (no4 == 0) {
            document.getElementById('type4_' + key).className = 'COM_displayNone';
        } else {
            document.getElementById('type4_' + key).className = '';
        }
        
        if (no5 == 0) {
            document.getElementById('type5_' + key).className = 'COM_displayNone';
        } else {
            document.getElementById('type5_' + key).className = '';
        }

    }
    
    //-----------------------------------------------------
    // 商品タイプ変更による表示切り替え
    //-----------------------------------------------------
    function PDT_chgProductType{$tabId}(key,val) {

        if (val == 0) {
            document.getElementById('charge_type_th_' + key).className = 'COM_displayNone';
            document.getElementById('charge_type_td_' + key).className = 'COM_displayNone';
        } else {
            document.getElementById('charge_type_th_' + key).className = '';
            document.getElementById('charge_type_td_' + key).className = '';
        }
    }

</script>
<!-- // カレントJavascript終了 -->
