<?php
/** @package Mikoshiva_Validate */

/**
 * Mikoshiva_Validate_Auto_Checker#addCheck 用の追加検証クラス
 *
 * 追加検証を行う際は必ずこのクラスを継承すること
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/05
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Validate_Auto_Add_Check_Abstract {


    /**
     * リクエストクラス
     *
     * @var Zend_Controller_Request_Http
     */
    protected $_request;


    /**
     * DB アダプタークラス
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    /**
     * コンストラクタ
     *
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/05
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct() {
        $this->_request = Zend_Registry::get('request');
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
    }

    /**
     * 実行メソッド
     *
     * @return string メッセージ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/05
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    abstract public function execute(array $form = array());
}