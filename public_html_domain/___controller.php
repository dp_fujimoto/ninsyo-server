<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/_SYSTEM_/config/define.php';
require_once 'Mikoshiva/Relay/Agent.php';


// セッションスタート
session_start();
$configPath = '';
$configPath = './config.ini';
if ( !file_exists( $configPath ) ){
   error_log(__FILE__ . '[if:コンフィグファイル無し]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
   echo '設定ファイル( ' . $configPath . ' )が存在しません。';
   exit;
}
$conf       = array();
$conf       = parse_ini_file( $configPath, true );

$basePath = $conf['first_section']['base'];

$agent  = null;
$agent  = new Mikoshiva_Relay_Agent();

$params = array();

// コードによって処理を分ける

switch ($_POST['code']) {
	/**
	 * ログインが必要かチェックします。
	 */
	case 'LOGIN_CHECK':
		error_log(__FILE__ . '[case:LOGIN_CHECK]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

	    // GETにしたい場合はgetResponseByGet(url, params)を使用してください。
		// mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'login-check', $_POST);
        echo $response;
        error_log(__FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * ログインを行います。
	 * ログインチェックの結果ログインが必要な場合はこちらを通ります。
	 */
	case 'LOGIN';
		error_log(__FILE__ . '[case:LOGIN]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		$campaignSalesCode = $_SESSION['campaignSalesCode'];

		// セッション初期化
		session_unset();
		$_SESSION['email'] = sha1($_POST['email']);
		$_SESSION['transactionNumber'] = '1';
		$_SESSION['campaignSalesCode'] = $campaignSalesCode;

		// パラメータ追加
		$params                      = $_POST;
		$params['campaignSalesCode'] = $campaignSalesCode;
		$params['transactionNumber'] = $_SESSION['transactionNumber'];

		// mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'login', $params);
        echo $response;
        error_log(__FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * オーダー登録へ遷移します。。
	 * ログイン不要な場合はこちらを通ります。
	 */
	case 'INPUT';
		error_log(__FILE__ . '[case:INPUT]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		$campaignSalesCode = $_SESSION['campaignSalesCode'];

		// セッション初期化
		session_unset();
		$_SESSION['email'] = sha1($_POST['email']);
		$_SESSION['transactionNumber'] = '1';
		$_SESSION['campaignSalesCode'] = $campaignSalesCode;

		// パラメータ追加
		$params                      = $_POST;
		$params['campaignSalesCode'] = $campaignSalesCode;
		$params['transactionNumber'] = $_SESSION['transactionNumber'];

		// mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'input', $params);

        echo $response;
        error_log(__FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * ログインパスワードを忘れた場合に入力されたメールアドレス宛てに
	 * パスワード通知メールを送信します。
	 */
	case 'REMINDER';
		error_log(__FILE__ . '[case:REMINDER]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		// mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'reminder', $_POST);
        echo $response;
        error_log(__FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * オーダー確認へ遷移します。
	 */
	case 'CONFIRM';
		error_log(__FILE__ . '[case:CONFIRM]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		if ($_SESSION['email'] !== sha1($_POST['email'])) {
			error_log(__FILE__ . '[if:セッションのメールアドレスと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
	        header('Location: ../index/'.$_SESSION['campaignSalesCode']);
		} else {
			error_log(__FILE__ . '[else:セッションのメールアドレスと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
    		// mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'confirm', $_POST);
            echo $response;
		}
        error_log(__FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * オーダー完了へ遷移します。
	 */
	case 'COMPLETE';
		error_log(__FILE__ . '[case:COMPLETE]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		if ($_SESSION['transactionNumber'] === $_POST['transactionNumber']) {
			error_log(__FILE__ . '[if:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			if ($_SESSION['email'] !== sha1($_POST['email'])) {
				error_log(__FILE__ . '[if:セッションのメールアドレスと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		        header('Location: ../index/'.$_SESSION['campaignSalesCode']);
		        break;
			} else {
				error_log(__FILE__ . '[else:セッションのメールアドレスと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
                $response = $agent->getResponseByPost($basePath.'complete', $_POST);
                $f = explode("\n", $response);
				$ret = '';
				error_log(__FILE__ .  '[foreach:ループ開始]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
				foreach ($f as $key => $val) {
					error_log(__FILE__ . '[foreach:ループキー:'.$key . ']' . '(' . __LINE__ . ')', 3, './controllerlog.txt');

					$search = array();

					// 値引き渡し用特殊タグ検索
					// 購入履歴ID
					if (preg_match('/^.*<miko.*id=\"pid\".*?>(.*)<\/miko>/',$val,$search) === 1) {
						error_log(__FILE__ . '[if:購入履歴ID用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						if (!isset($_SESSION['purchaseId'])) {
							error_log(__FILE__ . '[if:セッションに購入履歴IDなし]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
							$_SESSION['purchaseId'] = $search[1];
						}
						error_log(__FILE__ . '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						continue;
					}
					// 顧客ID
					if (preg_match('/^.*<miko.*id=\"customerId\".*?>(.*)<\/miko>/',$val,$search) === 1) {
						error_log(__FILE__ . '[if:顧客ID用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						if (!isset($_SESSION['customerId'])) {
							error_log(__FILE__ . '[if:セッションに顧客IDなし]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
							$_SESSION['customerId'] = $search[1];
						}
						error_log(__FILE__ . '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						continue;
					}
					// 発送先番号
					if (preg_match('/^.*<miko.*id=\"addressNumber\".*?>(.*)<\/miko>/',$val,$search) === 1) {
						error_log(__FILE__ . '[if:発送先番号用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						if (!isset($_SESSION['addressNumber'])) {
							error_log(__FILE__ . '[if:セッションに発送先番号なし]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
							$_SESSION['addressNumber'] = $search[1];
						}
						error_log(__FILE__ . '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						continue;
					}

					$ret .= $val ."\n";
				}
				echo $ret."\n";
		        $_SESSION['transactionNumber'] = '2';
			}
		} else {
			error_log(__FILE__ . '[else:セッションのトランザクションNoと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			$transactionNumber = $_SESSION['transactionNumber'];
			if ($transactionNumber !== '1') {
				error_log(__FILE__ . '[if:トランザクションNoが１ではない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

				// 初回決済が完了しているのでアップセルに設定
				$_POST['campaignType'] = 'CAMPAIGN_TYPE_UP_SELL';
				$_POST['addressNumber'] = $_SESSION['addressNumber'];
				$_POST['transactionNumber'] = $_SESSION['transactionNumber'];
				$_POST['customerId'] = $_SESSION['customerId'];

        		// mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'oneclick', $_POST);

                echo $response;
			} else {
				error_log(__FILE__ . '[else:トランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
        		// mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'error', $params);

                echo $response;
			}
		}
        error_log(__FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * ワンクリック商品の決済を行い完了画面へ遷移します。
	 */
	case 'ONECLICK_COMPLETE';
		error_log(__FILE__ . '[case:ONECLICK_COMPLETE]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		if ($_SESSION['transactionNumber'] == $_POST['transactionNumber']) {
			error_log(__FILE__ . '[if:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			if ($_SESSION['email'] !== sha1($_POST['email'])) {
				error_log(__FILE__ . '[if:セッションのメールアドレスと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		        header('Location: ../index/'.$_SESSION['campaignSalesCode']);
			} else {
				error_log(__FILE__ . '[else:セッションのメールアドレスと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

				$_POST['purchaseId'] = $_SESSION['purchaseId'];
                $response = $agent->getResponseByPost($basePath.'upsell', $_POST);
                $f = explode("\n", $response);
				$ret = '';
				error_log(__FILE__ .  '[foreach:ループ開始]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
				foreach ($f as $key => $val) {
					error_log(__FILE__ . '[foreach:ループキー:'.$key . ']' . '(' . __LINE__ . ')', 3, './controllerlog.txt');

					$search = array();

					// 値引き渡し用特殊タグ検索
					// 購入履歴ID
					if (preg_match('/^.*<miko.*id=\"pid\".*?>(.*)<\/miko>/',$val,$search) === 1) {
						error_log(__FILE__ .  '[if:購入履歴ID用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						if (!isset($_SESSION['purchaseId'])) {
							error_log(__FILE__ .  '[if:セッションに購入履歴IDなし]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
							$_SESSION['purchaseId'] = $search[1];
						}
						error_log(__FILE__ .  '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						continue;
					}
					// 顧客ID
					if (preg_match('/^.*<miko.*id=\"customerId\".*?>(.*)<\/miko>/',$val,$search) === 1) {
						error_log(__FILE__ .  '[if:顧客ID用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						if (!isset($_SESSION['customerId'])) {
							error_log(__FILE__ .  '[if:セッションに顧客IDなし]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
							$_SESSION['customerId'] = $search[1];
						}
						error_log(__FILE__ .  '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						continue;
					}
					// 発送先番号
					if (preg_match('/^.*<miko.*id=\"addressNumber\".*?>(.*)<\/miko>/',$val,$search) === 1) {
						error_log(__FILE__ .  '[if:発送先番号用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						if (!isset($_SESSION['addressNumber'])) {
							error_log(__FILE__ .  '[if:セッションに発送先番号なし]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
							$_SESSION['addressNumber'] = $search[1];
						}
						error_log(__FILE__ .  '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
						continue;
					}

					$ret .= $val ."\n";
				}
				echo $ret."\n";
		        $_SESSION['transactionNumber'] = $_SESSION['transactionNumber'] += 1;
			}
		} else {
			error_log(__FILE__ .  '[else:セッションのトランザクションNoと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			$transactionNumber = $_SESSION['transactionNumber'];
			if ($transactionNumber !== '1') {
				error_log(__FILE__ .  '[if:トランザクションNoが１ではない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

				// 初回決済が完了しているのでアップセルに設定
				$_POST['campaignType'] = 'CAMPAIGN_TYPE_UP_SELL';
				$_POST['addressNumber'] = $_SESSION['addressNumber'];
				$_POST['transactionNumber'] = $_SESSION['transactionNumber'];
				$_POST['customerId'] = $_SESSION['customerId'];

        		// mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'oneclick', $_POST);

                echo $response;
			} else {
				error_log(__FILE__ .  '[else:トランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
        		// mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'error', $params);

                echo $response;
			}
		}
        error_log(__FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * ダウンセルへ遷移します。
	 */
	case 'ONECLICK_DOWN';
		error_log(__FILE__ .  '[case:ONECLICK_DOWN]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		if ($_POST['transactionNumber'] == $_SESSION['transactionNumber']) {
			error_log(__FILE__ .  '[if:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			$_POST['transactionNumber'] = $_POST['transactionNumber'] += 1;
		} else {
			error_log(__FILE__ .  '[else:セッションのトランザクションNoと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			$_POST['transactionNumber'] = $_SESSION['transactionNumber'] -= 1;
		}

        // mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'downsell', $_POST);

        echo $response;
        $_SESSION['transactionNumber'] = $_POST['transactionNumber'];

        error_log(__FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * オーダー確認からオーダー登録へ戻ります。
	 */
	case 'BACK_INPUT';
		error_log(__FILE__ .  '[case:BACK_INPUT]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		$campaignSalesCode = $_SESSION['campaignSalesCode'];

		// パラメータを追加
		$params                      = $_POST;
		$params['campaignSalesCode'] = $campaignSalesCode;

        // mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'input', $params);

        echo $response;
        error_log(__FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * ワンクリック画面の確認画面を表示します。
	 */
	case 'ONECLICK_CONFIRM';
		error_log(__FILE__ .  '[case:ONECLICK_CONFIRM]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		$campaignSalesCode = $_SESSION['campaignSalesCode'];

		// パラメータを追加
		$params                      = $_POST;
		$params['campaignSalesCode'] = $campaignSalesCode;

        // mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'oneclick-confirm-check', $params);

        echo $response;
        error_log(__FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;

	/**
	 * エラー画面から対応した入力画面に遷移します。
	 */
	case 'ERROR_BACK_FORM';
		error_log(__FILE__ .  '[case:ERROR_BACK_FORM]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');

		$transactionNumber = $_POST['transactionNumber'];
		if ($transactionNumber + 1 == $_SESSION['transactionNumber']) {
			error_log(__FILE__ .  '[if:セッションのトランザクションNo - 1と一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			$campaignSalesCode = $_SESSION['campaignSalesCode'];
			$_SESSION['transactionNumber'] = $transactionNumber;
            if ($transactionNumber == '1') {
				error_log(__FILE__ .  '[if:セッションのトランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
                // パラメータを追加
                $params                      = $_POST;
                $params['campaignSalesCode'] = $campaignSalesCode;

                // mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'input', $params);

                echo $response;
			} else {
				error_log(__FILE__ .  '[else:セッションのトランザクションNoが１以外]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
                // mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'oneclick', $_POST);

                echo $response;
			}
		} else if ($transactionNumber == $_SESSION['transactionNumber']) {
			error_log(__FILE__ .  '[elseif:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
			$campaignSalesCode = $_SESSION['campaignSalesCode'];
			if ($transactionNumber === '1') {
				error_log(__FILE__ .  '[if:セッションのトランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
                // パラメータを追加
                $params                      = $_POST;
                $params['campaignSalesCode'] = $campaignSalesCode;

                // mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'input', $params);

                echo $response;
			} else {
				error_log(__FILE__ .  '[if:セッションのトランザクションNoが１以外]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
                // mikoshivaにpost
                $response = $agent->getResponseByPost($basePath.'oneclick', $_POST);

                echo $response;
			}
		}
        error_log(__FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;


	default:
		error_log(__FILE__ .  '[case:default]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
        error_log(__FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './controllerlog.txt');
		break;
}
