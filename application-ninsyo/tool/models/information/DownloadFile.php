<?php

require_once 'Mikoshiva/Controller/Model/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * 指定した添付ファイルを開く
 *
 * @author       K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/28
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Information_DownloadFile
    extends Mikoshiva_Controller_Model_Abstract {

    /**
     * 指定した添付ファイルを開く
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 添付ファイルの配列番号を取得
        $name = $actionForm->getName();
        $hash = $actionForm->getHash();

        $fileData = null;

        //-----------------------------------------------
        // ファイルの内容を取得する
        //-----------------------------------------------
        if (file_exists(UPLOADS_DIR . '/tool/information/' . $hash)) {
            $path = UPLOADS_DIR . '/tool/information/' . $hash;
            $fileData = file_get_contents($path);
        }

        $str = '';
        if (!isBlankOrNull($fileData)) {
            $fileName = urldecode($name);

            //===================================================
            // ■view の自動描画を中止
            //===================================================
            Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNorender();
            Zend_Layout::getMvcInstance()->disableLayout();


            //===================================================
            // ■ヘッダを作成
            //===================================================
            $fileName = '"' . $fileName . '"';
            $response = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->getResponse();
            $response->setHeader('Content-disposition', 'attachment; filename=' . $fileName);
            $response->setHeader('Content-type', 'application/octet-stream; name=' . $fileName);
            $response->appendBody($fileData);
        } else {
            $str = 'ダウンロードに失敗しました。';
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $str;
    }

}
