<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_action_analysis
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxActionAnalysis extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'taa';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'actionAnalysisId' => null,
        'moduleName' => null,
        'controllerName' => null,
        'actionName' => null,
        'executionCount' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * taa_action_analysis_id の値を返します
	 *
     * @return int taa_action_analysis_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getActionAnalysisId() {
		return $this->_properties['actionAnalysisId'];
    }


    /**
     * taa_action_analysis_id の値をセットします
	 *
	 * @param int $actionAnalysisId taa_action_analysis_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setActionAnalysisId($actionAnalysisId) {
		$this->_properties['actionAnalysisId'] = $actionAnalysisId;
    }


    /**
     * taa_module_name の値を返します
	 *
     * @return varchar taa_module_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getModuleName() {
		return $this->_properties['moduleName'];
    }


    /**
     * taa_module_name の値をセットします
	 *
	 * @param varchar $moduleName taa_module_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setModuleName($moduleName) {
		$this->_properties['moduleName'] = $moduleName;
    }


    /**
     * taa_controller_name の値を返します
	 *
     * @return varchar taa_controller_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getControllerName() {
		return $this->_properties['controllerName'];
    }


    /**
     * taa_controller_name の値をセットします
	 *
	 * @param varchar $controllerName taa_controller_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setControllerName($controllerName) {
		$this->_properties['controllerName'] = $controllerName;
    }


    /**
     * taa_action_name の値を返します
	 *
     * @return varchar taa_action_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getActionName() {
		return $this->_properties['actionName'];
    }


    /**
     * taa_action_name の値をセットします
	 *
	 * @param varchar $actionName taa_action_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setActionName($actionName) {
		$this->_properties['actionName'] = $actionName;
    }


    /**
     * taa_execution_count の値を返します
	 *
     * @return int taa_execution_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionCount() {
		return $this->_properties['executionCount'];
    }


    /**
     * taa_execution_count の値をセットします
	 *
	 * @param int $executionCount taa_execution_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionCount($executionCount) {
		$this->_properties['executionCount'] = $executionCount;
    }


    /**
     * taa_delete_flag の値を返します
	 *
     * @return char taa_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * taa_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag taa_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * taa_deletion_datetime の値を返します
	 *
     * @return datetime taa_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * taa_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime taa_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * taa_registration_datetime の値を返します
	 *
     * @return datetime taa_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * taa_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime taa_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * taa_update_datetime の値を返します
	 *
     * @return datetime taa_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * taa_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime taa_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * taa_update_timestamp の値を返します
	 *
     * @return timestamp taa_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * taa_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp taa_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























