<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * %TABLE_NAME%
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class %CLASS_NAME% extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = '%COLUMN_PREFIX%';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
%PRO_START%        %PRO_END%    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



%METHOD_START%
    /**
     * %COL_NAME_IMI% の値を返します
	 *
     * @return %DATA_TYPE% %COL_NAME_IMI%
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function get%METHOD_NAME%() {
		return $this->_properties['%COL_NAME%'];
    }


    /**
     * %COL_NAME_IMI% の値をセットします
	 *
	 * @param %DATA_TYPE% $%PROPERTY_NAME% %COL_NAME_IMI%
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function set%METHOD_NAME%($%PROPERTY_NAME%) {
		$this->_properties['%COL_NAME%'] = $%PROPERTY_NAME%;
    }

%METHOD_END%
}

























