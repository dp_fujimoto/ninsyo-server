<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Zend/View/Interface.php';
require_once 'Zend/Validate/Date.php';


 /**
  * 年のリストを作成します
  *
  * @package Mikoshiva_View
  * @author T.Tsukasa <taniguchi@kaihatsu.com>
  * @since 2009/12/16
  * @version SVN:$Id: SecondLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
  */
class Mikoshiva_View_Helper_SecondLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;

    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function secondLister($name, $selected = null, $attribs = null)
    {
        $secondList      = array();
        $secondList[''] = '';
        $secondList['01'] = '01';
        $secondList['02'] = '02';
        $secondList['03'] = '03';
        $secondList['04'] = '04';
        $secondList['05'] = '05';
        $secondList['06'] = '06';
        $secondList['07'] = '07';
        $secondList['08'] = '08';
        $secondList['09'] = '09';
        $secondList['10'] = '10';
        $secondList['11'] = '11';
        $secondList['12'] = '12';
        $secondList['13'] = '13';
        $secondList['14'] = '14';
        $secondList['15'] = '15';
        $secondList['16'] = '16';
        $secondList['17'] = '17';
        $secondList['18'] = '18';
        $secondList['19'] = '19';
        $secondList['20'] = '20';
        $secondList['21'] = '21';
        $secondList['22'] = '22';
        $secondList['23'] = '23';
        $secondList['24'] = '24';
        $secondList['25'] = '25';
        $secondList['26'] = '26';
        $secondList['27'] = '27';
        $secondList['28'] = '28';
        $secondList['29'] = '29';
        $secondList['30'] = '30';
        $secondList['31'] = '31';
        $secondList['32'] = '32';
        $secondList['33'] = '33';
        $secondList['34'] = '34';
        $secondList['35'] = '35';
        $secondList['36'] = '36';
        $secondList['37'] = '37';
        $secondList['38'] = '38';
        $secondList['39'] = '39';
        $secondList['40'] = '40';
        $secondList['41'] = '41';
        $secondList['42'] = '42';
        $secondList['43'] = '43';
        $secondList['44'] = '44';
        $secondList['45'] = '45';
        $secondList['46'] = '46';
        $secondList['47'] = '47';
        $secondList['48'] = '48';
        $secondList['49'] = '49';
        $secondList['50'] = '50';
        $secondList['51'] = '51';
        $secondList['52'] = '52';
        $secondList['53'] = '53';
        $secondList['54'] = '54';
        $secondList['55'] = '55';
        $secondList['56'] = '56';
        $secondList['57'] = '57';
        $secondList['58'] = '58';
        $secondList['59'] = '59';

        // ◆selected 2009/12/24 00:00:00 で来た場合にも対応
        $_selected  = $selected;
        if (isset($_selected)) {
            $_selected = str_replace('/', '-', $_selected );
            $validator1 = new Zend_Validate_Date('Y-m-d H:i:s');
            if ($validator1->isValid($_selected)) {

                $matche = array();
                preg_match('#^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:(\d{2})#', $_selected, $matche);
                $_selected = $matche[1];
            }
        }

        return $this->view->formSelect($name,
                                       $_selected,
                                       $attribs,
                                       $secondList);
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
