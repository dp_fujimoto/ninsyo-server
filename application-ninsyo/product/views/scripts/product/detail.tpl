<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">商品詳細</h2>

            {include file='../application-ninsyo/product/views/scripts/product/detailTemplate.tpl' form=$form}

            {closeTabButton name='タブを閉じる' tabId=$tabId}
        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->