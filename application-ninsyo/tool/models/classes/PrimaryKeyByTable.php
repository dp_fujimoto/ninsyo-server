<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Model/Class/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * 指定したテーブルのプライマリーキーカラム名を取得します。
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2011/09/09
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Classes_PrimaryKeyByTable
    extends Mikoshiva_Controller_Model_Class_Db_Abstract {

    /**
    * 指定したテーブルのプライマリーキーカラム名を取得します。
    *
    * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
    * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
    * @param Mikoshiva_Controller_Mapping_ActionContext $context
    * @return object
    */
    public function execute($tableName) {

        $primaryKey = null;

        if (!isBlankOrNull($tableName)) {

            //-----------------------------------------------
            // カラム情報取得
            //-----------------------------------------------
            // SQL作成
            $sql = "";
            $sql .= "SHOW COLUMNS ";
            $sql .= "FROM  ";
            $sql .= $tableName;

            // SQL実行
            $res = $this->_db->query($sql);

            /*
                array(6) {
                    ["Field"] => string(16) "mdr_direction_id"
                    ["Type"] => string(7) "int(11)"
                    ["Null"] => string(2) "NO"
                    ["Key"] => string(3) "PRI"
                    ["Default"] => NULL
                    ["Extra"] => string(14) "auto_increment"
                }
            */


            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                if ($v['Key'] === 'PRI') {
                    $primaryKey = $v['Field'];
                }
            }
        }
        return $primaryKey;
    }
}
