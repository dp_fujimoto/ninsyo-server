<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';
/**
 * 配列要素を指定された文字列により連結する
 *
 * @package Mikoshiva_View
 * @since $Date: 2012/12/19 03:43:22 $
 * @author $Author: fujimoto $
 *
 */
class Mikoshiva_View_Helper_Imploader extends Mikoshiva_View_Helper_Abstract
{
    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function imploader( $pieces, $glue = '<br>')
    {
        logInfo(LOG_START);
        if ( ! is_array( $pieces ) ) return $pieces;
        logInfo(LOG_END);
        return implode( $glue, $pieces );
    }
}
