<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstContent.php';
require_once 'Popo/MstCondition.php';
require_once 'tool/models/Operation.php';
require_once 'api/models/Operation.php';

/**
 * コンテンツ情報の登録を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Content_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * コンテンツ情報の登録を行う。
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // ログ文
        $logText= '';

        // 空のデータはnullにするため、配列にして処理してからactionFormに詰める
        $data = Mikoshiva_Utility_Copy::copyToArray($actionForm);
        foreach ($data as $k => $v) {
            $data[$k] = Mikoshiva_Utility_Convert::blankToNull($v);
        }
        Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($actionForm,$data);

        //-----------------------------------------------
        // コンテンツ登録
        //-----------------------------------------------
        $contentId = $actionForm->getContentId();
        if (!isBlankOrNull($contentId)) {

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $contentData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstContent', $contentId);
            $logText .= $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName() . 'がコンテンツID:' . $contentId . 'のデータ更新。';

            $actionForm->setImageUpdateDatetime($contentData->getImageUpdateDatetime());
            $actionForm->setMediaUpdateDatetime($contentData->getMediaUpdateDatetime());
        } else {
            $contentData = new Popo_MstContent();
            $logText .= $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName() . 'が' . $actionForm->getContentName() . 'というコンテンツ登録。';
        }

        if ($actionForm->getApplicationType() !== 'APPLICATION_TYPE_LIBRARY') {
            $actionForm->setImageUrl(null);
            $actionForm->setImageUpdateDatetime(null);
            $actionForm->setMediaUrl(null);
            $actionForm->setMediaUpdateDatetime(null);
            $actionForm->setMediaType(null);
            $actionForm->setDescription(null);
        }

        Mikoshiva_Utility_Copy::copyToPopo($contentData, $actionForm);

        if (!isBlankOrNull($contentData->getImageUrl()) && $actionForm->getImageUpdateFlag() === '1') {
            $contentData->setImageUpdateDatetime(Mikoshiva_date::mikoshivaNow());
        }
        if (!isBlankOrNull($contentData->getMediaUrl()) && $actionForm->getMediaUpdateFlag() === '1') {
            $contentData->setMediaUpdateDatetime(Mikoshiva_date::mikoshivaNow());
        }

        if (isBlankOrNull($contentData->getTestFlag())) {
            $contentData->setTestFlag('0');
        }

        if (isBlankOrNull($contentData->getDeletionDatetime())) {
            if ($contentData->getDeleteFlag() === '1') {
                $contentData->setDeletionDatetime(Mikoshiva_date::mikoshivaNow());
            } else {
                $contentData->setDeleteFlag('0');
                $contentData->setDeletionDatetime(null);
            }
        } else {
            if ($contentData->getDeleteFlag() !== '1') {
                $contentData->setDeleteFlag('0');
                $contentData->setDeletionDatetime(null);
            }
        }

        if (isBlankOrNull($contentData->getAvailableCheckFlag())) {
            $contentData->setAvailableCheckFlag('0');
        }
        if (isBlankOrNull($contentData->getAllConditionFlag())) {
            $contentData->setAllConditionFlag('0');
        }
        if (isBlankOrNull($contentData->getAuthFlag())) {
            $contentData->setAuthFlag('0');
        }

        // 更新（シンプルクラス）
        $db = null;
        $db = new Mikoshiva_Db_Simple();

        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstContent', $contentData);

        $contentId = $result->getContentId();


        //-----------------------------------------------
        // 条件登録
        //-----------------------------------------------
        // 使わない可能性のあるIDのデータに削除フラグを立てる
        $sql = "";
        $sql .= "UPDATE";
        $sql .= "  mst_condition ";
        $sql .= " SET ";
        $sql .= "  mcd_delete_flag = 1 ";
        $sql .= " ,mcd_deletion_datetime = ? ";
        $sql .= " ,mcd_update_datetime = ? ";
        $sql .= " WHERE 1 = 1 ";
        $sql .= " AND mcd_delete_flag = '0'";
        $sql .= " AND mcd_content_id = ?";

        // パラメータセット
        $params = array();
        $params[] = Mikoshiva_Date::mikoshivaNow();
        $params[] = Mikoshiva_Date::mikoshivaNow();
        $params[] = $contentId;

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        // 商品提供IDが同じデータを検索
        // SQL作成
        $sql = '';
        $sql .= " SELECT * ";
        $sql .= " FROM mst_condition";
        $sql .= " WHERE 1 = 1 ";
        $sql .= " AND mcd_content_id = ?";

        // パラメータセット
        $params = array();
        $params[] = $contentId;

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $conditionIds = array();
        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $conditionIds[] = $v['mcd_condition_id'];
        }

        $formData = $actionForm->toArrayProperty();

        $key = 0;
        for ($i = 0; $i < count($formData['conditionNo']); $i++) {
            if (isset($conditionIds[$key])) {
                $conditionData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstCondition', $conditionIds[$key]);
            } else {
                $conditionData = new Popo_MstCondition();
            }

            $conditionData->sethavingFlag('0');
            $conditionData->setServiseInFlag('0');
            $conditionData->setSubscriptionFlag('0');
            $conditionData->setBundleFlag('0');
            $conditionData->setPurchaseFlag('0');
            $conditionData->setShippingDataFlag('0');
            $conditionData->setNoneConditionFlag('0');
            $conditionData->setCancelOkFlag('0');

            switch ($formData['conditionNo'][$i]) {
                case 1:
                    $conditionData->sethavingFlag('1');
                    $formData['permissionMonth'][$i] = null;
                    $formData['permissionDay'][$i] = null;
                    $formData['needPeriodMonth'][$i] = null;
                    $formData['needPeriodDay'][$i] = null;
                    break;
                case 2:
                    $conditionData->setServiseInFlag('1');
                    $formData['permissionMonth'][$i] = null;
                    $formData['permissionDay'][$i] = null;
                    $formData['needPeriodMonth'][$i] = null;
                    $formData['needPeriodDay'][$i] = null;
                    break;
                case 3:
                    $conditionData->setSubscriptionFlag('1');
                    $formData['designatedDate'][$i] = null;
                    $formData['permissionMonth'][$i] = null;
                    $formData['permissionDay'][$i] = null;
                    break;
                case 4:
                    $conditionData->setBundleFlag('1');
                    $formData['designatedDate'][$i] = null;
                    $formData['permissionMonth'][$i] = null;
                    $formData['permissionDay'][$i] = null;
                    $formData['needPeriodMonth'][$i] = null;
                    $formData['needPeriodDay'][$i] = null;
                    break;
                case 5:
                    $conditionData->setPurchaseFlag('1');
                    $conditionData->setCancelOkFlag($formData['cancelOkFlag'][$i]);
                    $formData['designatedDate'][$i] = null;
                    break;
                case 6:
                    $conditionData->setShippingDataFlag('1');
                    $formData['designatedDate'][$i] = null;
                    $formData['permissionMonth'][$i] = null;
                    $formData['permissionDay'][$i] = null;
                    $formData['needPeriodMonth'][$i] = null;
                    $formData['needPeriodDay'][$i] = null;
                    break;
                case 9:
                    $conditionData->setNoneConditionFlag('1');
                    $formData['productCode'][$i] = null;
                    $formData['serverName'][$i] = null;
                    $formData['campaignCode'][$i] = null;
                    $formData['campaignSalesCode'][$i] = null;
                    $formData['subscriptionType'][$i] = null;
                    $formData['productType'][$i] = null;
                    $formData['designatedDate'][$i] = null;
                    $formData['packageNumber'][$i] = null;
                    $formData['productShippingCode'][$i] = null;
                    $formData['permissionMonth'][$i] = null;
                    $formData['permissionDay'][$i] = null;
                    $formData['needPeriodMonth'][$i] = null;
                    $formData['needPeriodDay'][$i] = null;
                    break;
                default:
                    continue 2;
                    break;
            }

            if ($formData['permissionMonth'][$i] == 0) {
                $formData['permissionMonth'][$i] = null;
            }
            if ($formData['permissionDay'][$i] == 0) {
                $formData['permissionDay'][$i] = null;
            }
            if ($formData['needPeriodMonth'][$i] == 0) {
                $formData['needPeriodMonth'][$i] = null;
            }
            if ($formData['needPeriodDay'][$i] == 0) {
                $formData['needPeriodDay'][$i] = null;
            }
            if ($formData['productType'][$i] !== 'PRODUCT_TYPE_SINGLE') {
                $formData['chargeType'][$i] = null;
            }

            $conditionData->setContentId($contentId);
            $conditionData->setProductCode($formData['productCode'][$i]);
            $conditionData->setServerName($formData['serverName'][$i]);
            $conditionData->setDesignatedDate($formData['designatedDate'][$i]);
            $conditionData->setPackageNumber($formData['packageNumber'][$i]);
            $conditionData->setProductShippingCode($formData['productShippingCode'][$i]);
            $conditionData->setPermissionMonth($formData['permissionMonth'][$i]);
            $conditionData->setPermissionDay($formData['permissionDay'][$i]);
            $conditionData->setNeedPeriodMonth($formData['needPeriodMonth'][$i]);
            $conditionData->setNeedPeriodDay($formData['needPeriodDay'][$i]);
            $conditionData->setCampaignCode($formData['campaignCode'][$i]);
            $conditionData->setCampaignSalesCode($formData['campaignSalesCode'][$i]);
            $conditionData->setSubscriptionType($formData['subscriptionType'][$i]);
            $conditionData->setProductType($formData['productType'][$i]);
            $conditionData->setChargeType($formData['chargeType'][$i]);
            $conditionData->setDeleteFlag('0');
            $conditionData->setDeletionDatetime(null);

            // 更新（シンプルクラス）
            $result = $db->simpleOneCreateOrUpdateById( 'Mikoshiva_Db_NinsyoAr_MstCondition', $conditionData);

            $logText .= $formData['conditionNo'][$i] . 'の条件登録。';
            $key++;
        }

        //-----------------------------------------------
        // キャッシュファイル削除
        //-----------------------------------------------
        $operation = new Api_Models_Operation();
        $operation->deleteConditionCacheFile($actionForm, $context);

        file_get_contents(DELETE_CONDITION_CACHE_FILE_API);

        $operation->deleteUserCacheFile($actionForm, $context);

        file_get_contents(DELETE_USER_CACHE_FILE_API);

        //-----------------------------------------------
        // アクションログ保存
        //-----------------------------------------------
        $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

        $operation = new Tool_Models_Operation();
        $operation->insertActionLog($logText);

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $actionForm;
    }
}
