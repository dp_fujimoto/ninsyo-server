<?php

/**
 * MIKOSHIVA Framework がどのサーバーで動いているか設定します
 * ！！！！外部ドメインの設定ファイルにも別で存在します！！！！
 *
 *
 * 【説明】
 * MIKOSHIVA Framework がどのサーバーで動いているか設定するための設定ファイル
 *
 * 【概要】
 * MIKOSHIVA Framework を他のドメインで動かすためにこの設定をします
 * この設定を行うことで各定数定義などを円滑に処理します
 *
 * 【変更履歴】
 * 2011-02-10 谷口司 作成
 */

// ▼サーバーモード
//   mikoshiva.com → mikoshiva
//      shimant.com → shimant
define('_SERVER_MODE_ID_', 'ninsyo-mikoshiva'); // 小文字！

// ▼アプリケーションタイプ
define('_APPLICATION_TYPE_ID_', 'ninsyo'); // 小文字！
