<?php
require_once 'Mikoshiva/Db/Ar/MstOperationLog.php';
require_once 'Mikoshiva/Date.php';


/**
 * Dao_MstOperationLog
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 * @version SVN:$Id: Operation.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Log_Operation {

    /**
     * オペレーションログを書き出します
     *
     * @param string $oprationName
     * @param string $operationContent
     * @param string $customerId
     * @param string $productCode
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/14
     * @version SVN:$Id: Operation.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function register($sql) {
        //--------------------------------------------------------------------
        // ■ 入力チェック
        //--------------------------------------------------------------------
        // オペレーションログ書き込み対象のクエリかを判定
        if($this->isRegistOperationLog($sql) === false) {
            return;
        }

        //--------------------------------------------------------------------
        // ■ レジストリ取得
        //--------------------------------------------------------------------
        $registry = null;
        $registry = Zend_Registry::getInstance();
        //--------------------------------------------------------------------
        // ■ データ作成
        //--------------------------------------------------------------------
        $sqlType    = '';
        $sqlType    = $this->getSqlType($sql);
        // DB オブジェクトを生成
        $mol = null;
        $mol = new Mikoshiva_Db_Ar_MstOperationLog();

        // セッションの取得
        $staff = new Zend_Session_Namespace('staff');
        if($staff->mstStaff !== null) {
            /* @var $mstStaff Popo_MstStaff */
            $staffId  = $staff->mstStaff->getStaffId();
        } else {
            $staffId  = null;
        }
        $request    = $registry->get('request');
        $module     = $request->getModuleName();
        $controller = $request->getControllerName();
        $action     = $request->getActionName();
        $url        = "$module/$controller/$action";



        $modelName = '';
        $actionForm = null;
        if($registry->isRegistered('modelName')) {      // 通常時
            $modelName  = $registry->get('modelName');
            $actionForm = $registry->get('actionForm');
        } elseif($registry->isRegistered('actionForm')) {  // アクションフォーム作成中に呼び出された場合、モデル名が取得できないのでアクションフォーム名を入れる
            $actionForm = $registry->get('actionForm');
            $modelName  = get_class($actionForm);
        }

        //if(isBlankOrNull($actionForm)) {
        //    $actionFormProperties = '------------';
        //} else {
            $actionFormProperties = '';
            $actionFormProperties = Mikoshiva_Debug_VarDump::exec($actionForm->toArrayProperty());
        //}
        //-------------------------------------------------------------------- //--------------------------------------------------------------------
        // ■ DB更新
        //--------------------------------------------------------------------
        // INSERT するデータを作成
        $data                              = array();
        $data['mol_login_id']               = $staffId; // ログインID（実行者）
        $data['mol_operation_url']          = $url; // オペレーションURL
        $data['mol_model_name']             = $modelName; // Model名
        $data['mol_sql']                    = $sql; // SQL
        $data['mol_sql_type']               = $sqlType; // SQLタイプ
        $data['mol_action_form_properties'] = $actionFormProperties; // ActionFormプロパティ
        $data['mol_update_datetime']        = Mikoshiva_Date::mikoshivaNow(); // 更新日
        $data['mol_registration_datetime']  = Mikoshiva_Date::mikoshivaNow(); // 登録日

        // INSERT
        $result = $mol->insert($data);
        $result = '';
        return $result;
    }

    /**
     * SQL文から、SQLのタイプを切り出して返します。
     *
     * @param $sql
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/23
     * @version SVN:$Id: Operation.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    private function getSqlType($sql) {
        $work = array();
        $work = explode(' ', trim($sql));
        return trim($work[0]);
    }


    /**
     * オペレーションログに記録すべきSQLか否か判定します。
     *
     * @param $sql
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/23
     * @version SVN:$Id: Operation.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function isRegistOperationLog($sql) {
        // オペレーションログ書き込みクエリの場合、falseを返す
        if(preg_match('/mst_operation|trx_action_analysis|trx_customer_rfm_ranking|trx_claim|trx_money_received|trx_real_refund|trx_balance_payments_execution|trx_balance_payments|trx_balance_payments_detail/', $sql) === 1) {
            return false;
        }
        $sqlType    = '';
        $sqlType    = $this->getSqlType($sql);
        switch($sqlType) {
            case 'INSERT':
                return (OPERATION_LOG_INSERT_WRITE_FLAG === 1) ? true : false;
                break;
            case 'UPDATE':
                return (OPERATION_LOG_UPDATE_WRITE_FLAG === 1) ? true : false;
                break;
            case 'DELETE':
                return (OPERATION_LOG_DELETE_WRITE_FLAG === 1) ? true : false;
                break;
            case 'SELECT':
                return (OPERATION_LOG_SELECT_WRITE_FLAG === 1) ? true : false;
                break;
            default:
                break;
        }
        return false;
    }
}






























