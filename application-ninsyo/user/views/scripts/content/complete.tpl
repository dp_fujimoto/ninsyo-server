<!-- // メインテーブル開始 -->
<table id="COM_painSingle">

    <tr>

        <!-- // ペイン開始 -->
        <td class="COM_nowrap">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ユーザーコンテンツ登録完了</h2>

            {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
            【ログ】<br>
            {str_replace("\n",'<br>',$complete['all'])}
            <br>
            <br>
            {/if}
            【エラー】<br>
            {if !isBlankOrNull($complete['error'])}
                システムユニットに報告してください。<br>{str_replace("\n",'<br>',$complete['error'])}
            {else}
                なし
            {/if}
            <br>
            <br>
            <br>
            {closeTabButton name='タブを閉じる' tabId=$tabId}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->