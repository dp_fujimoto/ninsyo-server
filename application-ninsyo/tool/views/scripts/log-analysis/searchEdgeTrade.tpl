<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">エッジトレーダーアクセス解析</h2>

            <!-- // フォーム開始 -->
            <form name="searchForm" id="searchForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="mode" value="calculate">

                <!-- 検索条件テーブル 開始-->
                <table class="COM_table">
                    <tr>
                        <td class="COM_nowrap COM_bgColorDeep">抽出対象日</td>
                        <td class="COM_nowrap">
                            {html_select_dateLister time=$form->getDate() year_name='year' month_name='month' field_separator=' ' display_days=false start_year="2012" end_year="+1" day_separator=""}
                        </td>
                        <td class="COM_nowrap" colspan="2">
                            {currentTabButton name="　　　検索　　　" url=$smarty.const.SUB_FOLDER_PATH|cat:"/tool/log-analysis/search-edge-trade" tabId=$tabId formId="searchForm"}
                        </td>
                    </tr>
                </table>
                <!-- 検索条件テーブル 終了-->
            </form>
            <!-- // 検索フォーム終了 -->

            <hr>


            {* 検索結果あり *}
            {if count($output) > 0}
                <div style="background-color:#ffcc66">
                    {$form->getYear()}年{$form->getMonth()}月
                </div>

                【ログインユーザー数】<br>
                <iframe style="text-valign:bottom;" srcdoc="{$form->getUserChartHtml()}" width="1100px" height="350px"></iframe>
                <br>
                <br>

                {$emptyArr = array()}
                {$dayArr = array()}
                {foreach from=$output key="day" item="v"}
                    {$emptyArr[$day] = ''}
                    {$dayArr[$day] = $day}
                {/foreach}

                ※人数をクリックすると下のテキストエリアに対象ユーザーIDが表示されます。<br>

                <table class="COM_table">
                    <tr>
                        <td class="COM_bold COM_nowrap COM_bgColorDeep">タイプ／日</td>
                        {foreach from=$dayArr key="k" item="v"}
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">
                            {$v}
                        </td>
                        {/foreach}
                    </tr>
                    {foreach from=$form->getTypeList() key="type" item="typeName"}
                    <tr>
                        <td class="COM_nowrap">{statusMapping code=$type}</td>

                        {foreach from=$dayArr key="k" item="v"}
                        <td class="COM_nowrap" onClick="TOL_searchEdgeTradeViewMemo{$tabId}(this);">
                            {if isset($output[$k]['detail'][$type]['user'])}
                                {$output[$k]['detail'][$type]['user']}
                                <div class="COM_displayNone">{$output[$k]['detail'][$type]['memo']}</div>
                            {else}0
                            {/if}
                        </td>
                        {/foreach}
                    </tr>
                    {/foreach}
                </table>

                <textarea id="memoArea" cols="100" rows="5"></textarea>

                <br>
                <br>

                【アクセス数】<br>
                <iframe style="text-valign:bottom;" srcdoc="{$form->getAccessChartHtml()}" width="1100px" height="350px"></iframe>

                <br>
                <br>

                <table class="COM_table">
                    <tr>
                        <td class="COM_bold COM_nowrap COM_bgColorDeep">タイプ／日</td>
                        {foreach from=$dayArr key="k" item="v"}
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">
                            {$v}
                        </td>
                        {/foreach}
                    </tr>
                    {foreach from=$form->getTypeList() key="type" item="typeName"}
                    <tr>
                        <td class="COM_nowrap">{statusMapping code=$type}</td>

                        {foreach from=$dayArr key="k" item="v"}
                        <td class="COM_nowrap">
                            {if isset($output[$k]['detail'][$type]['count'])}{$output[$k]['detail'][$type]['count']}{else}0{/if}
                        </td>
                        {/foreach}
                    </tr>
                    {/foreach}
                </table>

                <br>
                <br>

                【時間別アクセス成功数】<br>
                <iframe style="text-valign:bottom;" srcdoc="{$form->getTimeChartHtml()}" width="1100px" height="350px"></iframe>

                <br>
                <br>

                <table class="COM_table">
                    <tr>
                        <td class="COM_bold COM_nowrap COM_bgColorDeep">タイプ／時</td>
                        {foreach from=$form->getTimeAnalysisData() key="hour" item="v"}
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">
                            {$hour}
                        </td>
                        {/foreach}
                    </tr>
                    <tr>
                        <td class="COM_nowrap">アクセス数</td>
                        {foreach from=$form->getTimeAnalysisData() key="hour" item="v"}
                        <td class="COM_nowrap">
                            {$v['count']}
                        </td>
                        {/foreach}
                    </tr>
                    <tr>
                        <td class="COM_nowrap">ユーザー数</td>
                        {foreach from=$form->getTimeAnalysisData() key="hour" item="v"}
                        <td class="COM_nowrap">
                            {$v['user']}
                        </td>
                        {/foreach}
                    </tr>
                </table>

            {* 検索結果なし *}
            {else}

                <p>条件を選択して検索ボタンを押してください</p>

            {/if}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // メモ表示ON
    //-----------------------------------------------
    function TOL_searchEdgeTradeViewMemo{$tabId}(obj){

        var elements = obj.getElementsByTagName("div");
        if (elements.length == 1) {
            var divObj = elements[0];
            document.getElementById('memoArea').innerHTML = divObj.innerHTML;
        }
    }

</script>
<!-- // カレントJavascript終了 -->