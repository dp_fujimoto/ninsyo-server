<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Utility/Session.php';
require_once 'Mikoshiva/Logger.php';

/**
 * セッションにセットされた部門IDを返します。
 *
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/22
 * @version SVN:$Id: DivisionCheck.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_DivisionCheck extends Mikoshiva_View_Helper_Abstract {

    /**
     * セッションにセットされた部門IDを返します。
     * ただし、部門変更が行われていて、かつフラグとしてtrueが渡された場合、
     * 変更後の部門IDを返します。
     *
     * @param  boolean $flagChangeDivision フラグ(default false)
     * @return string                      部門ID
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/22
     * @version SVN:$Id: DivisionCheck.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    function divisionCheck($flagChangedDivision = false) {
        logInfo(LOG_START);
            $result = Mikoshiva_Utility_Session::divisionCheck($flagChangedDivision);
            logInfo(LOG_END);
        return $result;
    }

}
