<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Payment_Change
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Payment_Change extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'MemberID' => null,
        'CardNo' => null,
        'Expire' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * MemberID
	 *
     * @return  MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMemberID() {
		return $this->_properties['MemberID'];
    }


    /**
     * MemberID
	 *
	 * @param  $MemberID MemberID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMemberID($MemberID) {
		$this->_properties['MemberID'] = $MemberID;
    }


    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * Expire
	 *
     * @return  Expire
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getExpire() {
		return $this->_properties['Expire'];
    }


    /**
     * Expire
	 *
	 * @param  $Expire Expire
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setExpire($Expire) {
		$this->_properties['Expire'] = $Expire;
    }


}

























