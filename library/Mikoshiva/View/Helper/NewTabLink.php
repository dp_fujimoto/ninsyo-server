<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/NewTab.php';

/**
 * 新しいタブを開くjavascriptをリンクタグに実装した形で実装します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: NewTabLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_NewTabLink extends Mikoshiva_View_Helper_Abstract {

    /**
     * 新しいタブを開くjavascriptをリンクタグに実装した形で実装します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: NewTabLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $name ボタンの vlue 値
     * @param string $url 遷移先 URL
     * @param boolean $storage true|保存flase|保存しない
     * @return string
     */
    public function newTabLink( $name, $url, $storage = false) {
        $a = new Mikoshiva_View_Helper_NewTab();
        return '<a href="#" onclick="' . $a->newTab($url, $storage) . '">' . $name . '</a>';
    }
}
