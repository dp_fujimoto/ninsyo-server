#!/usr/bin/perl -w
##################################################
#
# Server Status Reporter
#
#  利用モジュール
#  Net::SMTP - SMTP(Simple Mail Transfer Protocol)
#
#  kawakami 2012/03/12
#
##################################################

# import
use strict;
use warnings;

use Net::SMTP;
use Encode qw(from_to);


##################################################
## const
##################################################

my %config = (
    logdir   => '/var/log/script',
    code     => 'euc-jp',
    server   => 'Mikoshiva',
    hostname => '211.10.131.127',
    smtphost => 'localhost',
    errorsto => 'error@mikoshiva.com',
    from     => 'status@mikoshiva.com',
    fromname => 'Mikoshiva Reporter',
#    to       => 'direct.errors@gmail.com'
#    to       => 'kawakami@kaihatsu.com'
    to       => 'system_report@d-publishing.jp'
);


##################################################
## main
##################################################

sub main() {
    # 日時データを取得
    my %calendar = getDateTime();

    # ログファイルリスト
    my @logFiles = (
        ['mnt_load_average_recorder_'        . $calendar{date} . '.log','18']
    );

    # コマンドリスト
    my %commandList = (
        diskFree    => 'df -h',
        memoryFree  => '/usr/bin/free -k',
        processList => 'ps auxf',
        httpdCount  => 'ps aux | grep -c httpd'
    );

    # メール組み立て
    my $subject = '[INFO] ' . $config{server} . ' ServerStatusReport (' . $config{server} . ')';
    my $body = '';
    $body .= "********************************************************************************\n";
    $body .= $config{server} . " サーバーステータス レポート\n";
    $body .= $calendar{datetime} . "\n";
    $body .= "\n";
    $body .= "================================================================================\n";
    $body .= "host: " . $config{hostname} . "\n";

    $body .= getStandardOutput($commandList{httpdCount});     # Apache Process Count
    $body .= getStandardOutput($commandList{diskFree});       # Disk Free
    $body .= getStandardOutput($commandList{memoryFree});     # Memory Free
    $body .= getTailLogs(\@logFiles, $config{logdir});
    $body .= getStandardOutput($commandList{processList});    # Process List


    $body .= "********************************************************************************\n";

    # メールメッセージ組み立て
    my %mailMessage = (
        code     => $config{code},
        smtphost => $config{smtphost},
        errorsto => $config{errorsto},
        from     => $config{from},
        fromname => $config{fromname},
        to       => $config{to},
        subject  => $subject,
        body     => $body
    );

    # メール送信
    sendMail(\%mailMessage, \%calendar);

}


##################################################
## function
##################################################
#
# コマンドの発行結果をフォーマットして返します
# @Param $command
#
sub getStandardOutput($) {
    my ($command) = @_;
    my $body = '';

    if (defined($command)) {
        my $process = `$command`;
        if (! defined($process)) {
            $process = '';
        }

        $body .= "================================================================================\n";
        $body .= "# " . $command . "\n";
        $body .= "\n";
        $body .= $process;
        $body .= "\n";
    }

    return $body;
}

#
# ログファイルの最後から
# 指定行取得してフォーマットして返します
# @Param array  @logFiles
# @Param string $logDirectory
#
sub getTailLogs(\@$) {
    my ($logFiles, $logDirectory) = @_;

    my $body        = '';
    my $logFileName = '';
    my $logFile     = '';
    my $linage      = '';

    # 要素数を取得
    my $num = @$logFiles;

    for (my $i = 0; $i < $num; $i++) {
        $logFileName = $$logFiles[$i][0];
        $linage      = $$logFiles[$i][1];

        if (defined($logFileName) && defined($linage)) {
            $logFile = $logDirectory . '/' . $logFileName;

            my $log = `/usr/bin/tail -n $linage $logFile`;
            if (! defined($log)) {
                $log = '';
            }

            $body .= "================================================================================\n";
            $body .= "# " . $logFileName . "\n";
            $body .= "\n";
            $body .= $log;
            $body .= "\n";
        }

        sleep 1;
    }

    return $body;
}

#
# メール送信を行います
# @Param hash %mailMessage
# @Param hash %calendar
#
sub sendMail(\%\%) {
    my ($mailMessage, $calendar) = @_;

    my $code      = $$mailMessage{code};
    my $smtphost  = $$mailMessage{smtphost};
    my $to        = $$mailMessage{to};
    my $from      = $$mailMessage{from};
    my $fromname  = $$mailMessage{fromname};
    my $subject   = $$mailMessage{subject};
    my $body      = $$mailMessage{body};
    my $errorsto  = $$mailMessage{errorsto};
    my $timestamp = $$calendar{timestamp};

    #from_to($subject, $code, "MIME-Header-ISO_2022_JP");
    from_to($body, $code, "iso-2022-jp");

    my $smtp = Net::SMTP->new($smtphost);
    $smtp->mail($from);
    $smtp->to(split(/,/, $to));
    $smtp->data();
    $smtp->datasend("Date: " . $timestamp . "\n");
    $smtp->datasend("From: " . $fromname . " <" . $from . ">\n");
    $smtp->datasend("To: " . $to . "\n");
    $smtp->datasend("Subject: " . $subject . "\n");
    $smtp->datasend("Errors-To: " . $errorsto . "\n");
    $smtp->datasend("Message-ID: <" . time . "." . int(rand(1000)) . "." . $from . ">\n");
    $smtp->datasend("Content-Transfer-Encoding: 7bit\n");
    $smtp->datasend("Content-Type: text/plain; charset=\"ISO-2022-JP\"\n\n");
    $smtp->datasend($body . "\n");
    $smtp->dataend();
    $smtp->quit;
}

#
# 現在の日時データを3タイプで返します
# メールヘッダに埋める現在の日時データ
# @return string "Mon, 7 Jan 2008 09:00:00 +0900 (JST)"
# 現在の日時データ
# @return string "2008-01-07 09:00:00"
# 現在の年月日
# @return string "20080107"
#
sub getDateTime() {
    $ENV{"TZ"} = "JST-9";
    my ($sec, $min, $hour, $mday, $mon, $year, $wday) = localtime(time);
    my @week  = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    my @month = ("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                 "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    my $timestamp = sprintf("%s, %d %s %04d %02d:%02d:%02d +0900 (JST)",
                        $week[$wday], $mday, $month[$mon], $year + 1900, $hour, $min, $sec);

    my $datetime = sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $year + 1900, $mon + 1, $mday, $hour, $min, $sec);

    my $date = sprintf("%04d%02d%02d", $year + 1900, $mon + 1, $mday);

    my %calendar = (
        timestamp => $timestamp,    # "Mon, 7 Jan 2008 09:00:00 +0900 (JST)"
        datetime  => $datetime,     # "2008-01-07 09:00:00"
        date      => $date          # "20080107"
    );

    return %calendar;
}


##################################################
## main start
##################################################

main();
