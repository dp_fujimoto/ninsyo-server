<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（ヘルパー） ドキュメント &gt; ヘルパー 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/helper">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> ヘルパー</h1>


            <p>時、分、秒、それぞれのセレクトボックスのhtmlを返します。</p>
<!-- =================================================================
    ■<?php echo basename(__FILE__, '.php') ?>■
====================================================================== -->
          <table class="wiki">
            <tr>
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
                    <tr> 
            <td id="html_select_timeLister" class="name">hour_name
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">空文字</td> 
            <td id="html_select_timeLister" class="summary"> '時'のselectタグのname, idを指定します
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">minute_name
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">空文字</td> 
            <td id="html_select_timeLister" class="summary"> '分'のselectタグのname, idを指定します
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">second_name
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">空文字</td> 
            <td id="html_select_timeLister" class="summary"> '秒'のselectタグのname, idを指定します
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">meridian_name
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">空文字</td> 
            <td id="html_select_timeLister" class="summary"> AM/PMのselectタグのname, idを指定します
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">field_separator
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">空文字</td> 
            <td id="html_select_timeLister" class="summary"> 各selectボックス間の区切り文字を指定します
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">time
</td> 
            <td id="html_select_timeLister" class="type">timestamp
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">現在の時間</td> 
            <td id="html_select_timeLister" class="summary"> 使用する日付/時間
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">display_hours
</td> 
            <td id="html_select_timeLister" class="type">boolean
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">TRUE</td> 
            <td id="html_select_timeLister" class="summary"> 時を表示するかどうか
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">display_minutes
</td> 
            <td id="html_select_timeLister" class="type">boolean
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">TRUE</td> 
            <td id="html_select_timeLister" class="summary"> 分を表示するかどうか
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">display_seconds
</td> 
            <td id="html_select_timeLister" class="type">boolean
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">TRUE</td> 
            <td id="html_select_timeLister" class="summary"> 秒を表示するかどうか
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">display_meridian
</td> 
            <td id="html_select_timeLister" class="type">boolean
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">TRUE</td> 
            <td id="html_select_timeLister" class="summary"> am/pm を表示するかどうか
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">use_24_hours
</td> 
            <td id="html_select_timeLister" class="type">boolean
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">TRUE</td> 
            <td id="html_select_timeLister" class="summary"> 24 時間クロックを用いるかどうか
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">minute_interval
</td> 
            <td id="html_select_timeLister" class="type">integer
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">1</td> 
            <td id="html_select_timeLister" class="summary"> ドロップダウンリストの分間隔
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">second_interval
</td> 
            <td id="html_select_timeLister" class="type">integer
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">1</td> 
            <td id="html_select_timeLister" class="summary"> ドロップダウンリストの秒間隔
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">field_array
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">null</td> 
            <td id="html_select_timeLister" class="summary"> 結果の値をこの名前の配列に渡して出力
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">all_extra
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">null</td> 
            <td id="html_select_timeLister" class="summary"> 全てのselect/inputタグに拡張属性を追加
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">hour_extra
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">null</td> 
            <td id="html_select_timeLister" class="summary"> 時間のselect/inputタグに拡張属性を追加
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">minute_extra
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">null</td> 
            <td id="html_select_timeLister" class="summary"> 分のselect/inputタグに拡張属性を追加
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">second_extra
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">null</td> 
            <td id="html_select_timeLister" class="summary"> 秒のselect/inputタグに拡張属性を追加
</td> 
          </tr> 
                    <tr> 
            <td id="html_select_timeLister" class="name">meridian_extra
</td> 
            <td id="html_select_timeLister" class="type">string
</td> 
            <td id="html_select_timeLister" class="indispensable">-</td> 
            <td id="html_select_timeLister" class="default">null</td> 
            <td id="html_select_timeLister" class="summary"> am/pmのselect/inputタグに拡張属性を追加
</td> 
          </tr> 
                  </table> 
      <a href="#" style="text-align:right">TOP</a>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string("
{html_select_timeLister time='12:34:56' hour_name='hour' minute_name='minute' display_seconds=false field_separator=' : '}
"); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <span class="wiki"><?php
highlight_string('
<select name="hour" id="hour">
<option label="00" value="00">00</option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>

<option label="07" value="07">07</option>
<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12" selected="selected">12</option>
<option label="13" value="13">13</option>
<option label="14" value="14">14</option>
<option label="15" value="15">15</option>

<option label="16" value="16">16</option>
<option label="17" value="17">17</option>
<option label="18" value="18">18</option>
<option label="19" value="19">19</option>
<option label="20" value="20">20</option>
<option label="21" value="21">21</option>
<option label="22" value="22">22</option>
<option label="23" value="23">23</option>
</select>

 : <select name="minute" id="minute">
<option label="00" value="00">00</option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>
<option label="07" value="07">07</option>

<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12">12</option>
<option label="13" value="13">13</option>
<option label="14" value="14">14</option>
<option label="15" value="15">15</option>
<option label="16" value="16">16</option>

<option label="17" value="17">17</option>
<option label="18" value="18">18</option>
<option label="19" value="19">19</option>
<option label="20" value="20">20</option>
<option label="21" value="21">21</option>
<option label="22" value="22">22</option>
<option label="23" value="23">23</option>
<option label="24" value="24">24</option>
<option label="25" value="25">25</option>

<option label="26" value="26">26</option>
<option label="27" value="27">27</option>
<option label="28" value="28">28</option>
<option label="29" value="29">29</option>
<option label="30" value="30">30</option>
<option label="31" value="31">31</option>
<option label="32" value="32">32</option>
<option label="33" value="33">33</option>
<option label="34" value="34" selected="selected">34</option>

<option label="35" value="35">35</option>
<option label="36" value="36">36</option>
<option label="37" value="37">37</option>
<option label="38" value="38">38</option>
<option label="39" value="39">39</option>
<option label="40" value="40">40</option>
<option label="41" value="41">41</option>
<option label="42" value="42">42</option>
<option label="43" value="43">43</option>

<option label="44" value="44">44</option>
<option label="45" value="45">45</option>
<option label="46" value="46">46</option>
<option label="47" value="47">47</option>
<option label="48" value="48">48</option>
<option label="49" value="49">49</option>
<option label="50" value="50">50</option>
<option label="51" value="51">51</option>
<option label="52" value="52">52</option>

<option label="53" value="53">53</option>
<option label="54" value="54">54</option>
<option label="55" value="55">55</option>
<option label="56" value="56">56</option>
<option label="57" value="57">57</option>
<option label="58" value="58">58</option>
<option label="59" value="59">59</option>
</select>



'); ?>
</span>
            <h2 id="使用例">結果(表示)</h2>

<select name="hour" id="hour">
<option label="00" value="00">00</option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>

<option label="07" value="07">07</option>
<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12" selected="selected">12</option>
<option label="13" value="13">13</option>
<option label="14" value="14">14</option>
<option label="15" value="15">15</option>

<option label="16" value="16">16</option>
<option label="17" value="17">17</option>
<option label="18" value="18">18</option>
<option label="19" value="19">19</option>
<option label="20" value="20">20</option>
<option label="21" value="21">21</option>
<option label="22" value="22">22</option>
<option label="23" value="23">23</option>
</select>

 : <select name="minute" id="minute">
<option label="00" value="00">00</option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>
<option label="07" value="07">07</option>

<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12">12</option>
<option label="13" value="13">13</option>
<option label="14" value="14">14</option>
<option label="15" value="15">15</option>
<option label="16" value="16">16</option>

<option label="17" value="17">17</option>
<option label="18" value="18">18</option>
<option label="19" value="19">19</option>
<option label="20" value="20">20</option>
<option label="21" value="21">21</option>
<option label="22" value="22">22</option>
<option label="23" value="23">23</option>
<option label="24" value="24">24</option>
<option label="25" value="25">25</option>

<option label="26" value="26">26</option>
<option label="27" value="27">27</option>
<option label="28" value="28">28</option>
<option label="29" value="29">29</option>
<option label="30" value="30">30</option>
<option label="31" value="31">31</option>
<option label="32" value="32">32</option>
<option label="33" value="33">33</option>
<option label="34" value="34" selected="selected">34</option>

<option label="35" value="35">35</option>
<option label="36" value="36">36</option>
<option label="37" value="37">37</option>
<option label="38" value="38">38</option>
<option label="39" value="39">39</option>
<option label="40" value="40">40</option>
<option label="41" value="41">41</option>
<option label="42" value="42">42</option>
<option label="43" value="43">43</option>

<option label="44" value="44">44</option>
<option label="45" value="45">45</option>
<option label="46" value="46">46</option>
<option label="47" value="47">47</option>
<option label="48" value="48">48</option>
<option label="49" value="49">49</option>
<option label="50" value="50">50</option>
<option label="51" value="51">51</option>
<option label="52" value="52">52</option>

<option label="53" value="53">53</option>
<option label="54" value="54">54</option>
<option label="55" value="55">55</option>
<option label="56" value="56">56</option>
<option label="57" value="57">57</option>
<option label="58" value="58">58</option>
<option label="59" value="59">59</option>
</select>
            
  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>


