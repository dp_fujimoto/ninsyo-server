<?php
/******************************************************************************
 *
 * ◆外部ドメインAjax中継コントローラ◆
 *
 * 概要：mikoshiva.com よりも外のドメインでの遷移処理を行います
 *
 * （注意）リクエストに acd 及び actionUrl がともに含まれない場合、
 *         注文のロジックに入ります。
 *         こちらは入力チェック等が行われて居ない為、予測不能なエラーが出る場合があります。
 *
 *
 *                              2010/05/12 T.Taniguchi 作成
 *
 *
 *******************************************************************************/

//--------------------------------------------------
//
// ◆各必須ファイルを読み込み◆
//
//---------------------------------------------------
require_once $_SERVER['DOCUMENT_ROOT'] . '/_SYSTEM_/config/define.php';
require_once 'Mikoshiva/Relay/Agent.php';


//--------------------------------------------------
//
// ■リクエスト初期化
//
//---------------------------------------------------
$request = array();
$request = $_GET + $_POST;

if(!isset($request['namespace'])) {
    $request['namespace'] = '';
}
if(!isset($request['tcd'])) {
    $request['tcd'] = '';
}
if(!isset($request['acd'])) {
    $request['acd'] = '';
}
if(!isset($request['actionUrl'])) {
    $request['actionUrl'] = '';
}

//--------------------------------------------------------------------
// ■ MikoshivaへのAjax
//    リクエストに actionUrl または acd が含まれる場合はMikoshivaへのAjaxとして処理します。
//    これらが空の場合、注文処理を行います。
//--------------------------------------------------------------------
if($request['actionUrl'] !== '' || $request['acd'] !== '') {
    session_start();


    //--------------------------------------------------------------------
    // ■ Mikoshiva処理用の初期化
    //--------------------------------------------------------------------
    $cookie      = null;
    $namespace   = '';
    $param       = array();
    $agent       = null;
    $tcd         = '';
    $actionName  = '';
    $url         = '';


    //--------------------------------------------------------------------
    // ■ リクエスト先Url / アクション名を取得
    //--------------------------------------------------------------------
    $url = getUrl($request);

    $work = array();
    $work = explode('/', $url);
    $actionName = $work[2];

    //--------------------------------------------------------------------
    // ■ リロード判定
    //--------------------------------------------------------------------
    if($request['namespace'] === '') {
        //--------------------------------------------------------------------
        // ■ 入力チェック
        //--------------------------------------------------------------------
        if($request['tcd'] === '') {
            mikoshivaRequestError('必須引数 tcd が空です。');
            exit;
        }
        if($request['acd'] === '') {
            mikoshivaRequestError('必須引数 acd が空です。');
            exit;
        }

        //--------------------------------------------------------------------
        // ■ Mikoshivaへのリクエストを作成
        //--------------------------------------------------------------------
        $param        = $request;
        $param['tcd'] = $request['tcd'];
        $namespace    = $request['acd'];
        $agent        = new Mikoshiva_Relay_Agent();

        // セッションの値をリセット
        unset($_SESSION['mikoshiva'][$namespace]);
        //session_destroy();
        //session_start();

        //--------------------------------------------------------------------
        // ■ Mikoshivaよりリクエストを取得
        //--------------------------------------------------------------------
        $response = $agent->getResponseByPost(MIKOSHIVA_URL . $url, $param);

        //--------------------------------------------------------------------
        // ■ 出力1
        //    エスケープ等を通さない出力
        //    tcd=DUMMY(.jsファイル読み込みなどに使用)
        //--------------------------------------------------------------------
        if($request['tcd'] === 'DUMMY') {
            echo $response;
            exit;
        }
        //--------------------------------------------------------------------
        // ■ Mikoshivaとのセッションと、tcdをセッションに保存
        //--------------------------------------------------------------------
        $_SESSION['mikoshiva'][$namespace]['cookie'] = $agent->getCookie();
        $_SESSION['mikoshiva'][$namespace]['tcd']    = $request['tcd'];

        //--------------------------------------------------------------------
        // ■ 出力前の補正
        //--------------------------------------------------------------------
        $response = preg_replace('/KO_.*?\(.*?\)/', '', $response); // Mikoshiva固有のJSが通ると、javascript実行エラーで画面が表示されなくなる
        $response = preg_replace('/(\n)/', '\\\\${1}', $response);
        $response = str_replace('\'', '\\\'', $response);

        //--------------------------------------------------------------------
        // ■ 出力2
        //    window.nameは namespace名をJavaScriptから参照するために使用する
        //--------------------------------------------------------------------
        echo "document.write('{$response}');";
        echo "window.name = '{$namespace}';";

    // 通常の場合はセッションに$_POSTを保存
    } else {
        $namespace    = $request['namespace'];
        //--------------------------------------------------------------------
        // ■ 入力チェック
        //--------------------------------------------------------------------
        if(!isset($_SESSION['mikoshiva'][$namespace]['tcd'])) {
            mikoshivaRequestError('必須引数 tcd が空です。', true);
            exit;
        }
        if(!isset($_SESSION['mikoshiva'][$namespace]['cookie'])) {
            mikoshivaRequestError('セッションにMikoshiva の cookie が保持されていません。', true);
            exit;
        }
        //--------------------------------------------------------------------
        // ■ Mikoshivaへのリクエストを作成
        //--------------------------------------------------------------------
        $namespace    = $request['namespace'];
        $param        = $request;
        $param['tcd'] = $_SESSION['mikoshiva'][$namespace]['tcd'];
        $cookie       = $_SESSION['mikoshiva'][$namespace]['cookie'];
        $agent        = new Mikoshiva_Relay_Agent($cookie);

        // 完了画面表示時はセッションを削除
        if($actionName === 'complete') {
            $_SESSION['mikoshiva'][$namespace] = array();
        }

        //--------------------------------------------------------------------
        // ■ Mikoshivaよりリクエストを取得
        //--------------------------------------------------------------------
        $response = $agent->getResponseByPost(MIKOSHIVA_URL . $url, $param);

        //--------------------------------------------------------------------
        // ■ "外部ドメイン - Mikoshiva間のセッションcookie"をセッションに保存
        //--------------------------------------------------------------------
        $_SESSION['mikoshiva'][$namespace]['cookie']       = $agent->getCookie();

        //--------------------------------------------------------------------
        // ■ 出力
        //--------------------------------------------------------------------
        echo $response;
    }

    exit;
}


/**
 * config/acd-mapping.phpからacdを基にリクエスト先を取得します。
 *
 * $request['actionUrl']が設定されている場合は、そのURLを返します。
 *
 * @param unknown_type $request
 * @return unknown_type
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/05/12
 * @version SVN:$Id: controller.php,v 1.1 2012/09/21 07:08:32 fujimoto Exp $
 */
function getUrl($request) {
    $url = '';
    // URLの変換
    // 以下の2パターンを想定
    // module_controller_action
    // /module/controller/action
    if($request['actionUrl'] !== '') {
        $url = $request['actionUrl'];
    }

    if($request['acd'] !== '') {
        //--------------------------------------------------------------------
        // ■ Mikoshivaより対応するurlを取得
        //--------------------------------------------------------------------
        $agent        = new Mikoshiva_Relay_Agent();
        $param        = array();
        $param['acd'] = $request['acd'];
        $response = trim($agent->getResponseByPost(MIKOSHIVA_URL . '/utility/dispatch/dispatch', $param));


        if(preg_match('#/..*?/..*?/..*?#', $response) == 1) {
            $url = $response;
        } else {
            mikoshivaRequestError('acd:' . $request['acd'] . 'は設定ファイルに存在しません。', true);
            exit;
        }
    }
    return $url;

}

/**
 * 入力値エラーの出力処理
 *
 * @param unknown_type $message
 * @param unknown_type $addScriptFlag
 * @return unknown_type
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/05/12
 * @version SVN:$Id: controller.php,v 1.1 2012/09/21 07:08:32 fujimoto Exp $
 */
function mikoshivaRequestError($message, $addScriptFlag = false) {
    $message = '通信中にエラーが発生しました。 詳細(' . $message . ')';

    $result  = '';
    $result .= "alert('{$message}');\n";
    $result .= "window.name = '';\n";
    $result .= "history.back();\n";
    if($addScriptFlag === true) {
        $result = '<script type="text/javascript">' . "\n" . $result . '</script>';
    }
    echo $result . "\n";
}
