<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_balance_payments_execution
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxBalancePaymentsExecution extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tbe';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'balancePaymentsExecutionId' => null,
        'executionPlanDate' => null,
        'executionTargetYear' => null,
        'executionTargetMonth' => null,
        'executionVersion' => null,
        'targetBalancePaymentsId' => null,
        'executionPlanFlag' => null,
        'completeFlag' => null,
        'completeDatetime' => null,
        'advanceReceivedCount' => null,
        'deferredIncomeCount' => null,
        'salesCount' => null,
        'commissionCount' => null,
        'advanceReceivedRefundCount' => null,
        'deferredIncomeRefundCount' => null,
        'salesRefundCount' => null,
        'commissionRefundCount' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tbe_balance_payments_execution_id の値を返します
	 *
     * @return int tbe_balance_payments_execution_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBalancePaymentsExecutionId() {
		return $this->_properties['balancePaymentsExecutionId'];
    }


    /**
     * tbe_balance_payments_execution_id の値をセットします
	 *
	 * @param int $balancePaymentsExecutionId tbe_balance_payments_execution_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBalancePaymentsExecutionId($balancePaymentsExecutionId) {
		$this->_properties['balancePaymentsExecutionId'] = $balancePaymentsExecutionId;
    }


    /**
     * tbe_execution_plan_date の値を返します
	 *
     * @return date tbe_execution_plan_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionPlanDate() {
		return $this->_properties['executionPlanDate'];
    }


    /**
     * tbe_execution_plan_date の値をセットします
	 *
	 * @param date $executionPlanDate tbe_execution_plan_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionPlanDate($executionPlanDate) {
		$this->_properties['executionPlanDate'] = $executionPlanDate;
    }


    /**
     * tbe_execution_target_year の値を返します
	 *
     * @return varchar tbe_execution_target_year
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionTargetYear() {
		return $this->_properties['executionTargetYear'];
    }


    /**
     * tbe_execution_target_year の値をセットします
	 *
	 * @param varchar $executionTargetYear tbe_execution_target_year
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionTargetYear($executionTargetYear) {
		$this->_properties['executionTargetYear'] = $executionTargetYear;
    }


    /**
     * tbe_execution_target_month の値を返します
	 *
     * @return varchar tbe_execution_target_month
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionTargetMonth() {
		return $this->_properties['executionTargetMonth'];
    }


    /**
     * tbe_execution_target_month の値をセットします
	 *
	 * @param varchar $executionTargetMonth tbe_execution_target_month
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionTargetMonth($executionTargetMonth) {
		$this->_properties['executionTargetMonth'] = $executionTargetMonth;
    }


    /**
     * tbe_execution_version の値を返します
	 *
     * @return int tbe_execution_version
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionVersion() {
		return $this->_properties['executionVersion'];
    }


    /**
     * tbe_execution_version の値をセットします
	 *
	 * @param int $executionVersion tbe_execution_version
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionVersion($executionVersion) {
		$this->_properties['executionVersion'] = $executionVersion;
    }


    /**
     * tbe_target_balance_payments_id の値を返します
	 *
     * @return text tbe_target_balance_payments_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTargetBalancePaymentsId() {
		return $this->_properties['targetBalancePaymentsId'];
    }


    /**
     * tbe_target_balance_payments_id の値をセットします
	 *
	 * @param text $targetBalancePaymentsId tbe_target_balance_payments_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTargetBalancePaymentsId($targetBalancePaymentsId) {
		$this->_properties['targetBalancePaymentsId'] = $targetBalancePaymentsId;
    }


    /**
     * tbe_execution_plan_flag の値を返します
	 *
     * @return char tbe_execution_plan_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutionPlanFlag() {
		return $this->_properties['executionPlanFlag'];
    }


    /**
     * tbe_execution_plan_flag の値をセットします
	 *
	 * @param char $executionPlanFlag tbe_execution_plan_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutionPlanFlag($executionPlanFlag) {
		$this->_properties['executionPlanFlag'] = $executionPlanFlag;
    }


    /**
     * tbe_complete_flag の値を返します
	 *
     * @return char tbe_complete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompleteFlag() {
		return $this->_properties['completeFlag'];
    }


    /**
     * tbe_complete_flag の値をセットします
	 *
	 * @param char $completeFlag tbe_complete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompleteFlag($completeFlag) {
		$this->_properties['completeFlag'] = $completeFlag;
    }


    /**
     * tbe_complete_datetime の値を返します
	 *
     * @return datetime tbe_complete_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompleteDatetime() {
		return $this->_properties['completeDatetime'];
    }


    /**
     * tbe_complete_datetime の値をセットします
	 *
	 * @param datetime $completeDatetime tbe_complete_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompleteDatetime($completeDatetime) {
		$this->_properties['completeDatetime'] = $completeDatetime;
    }


    /**
     * tbe_advance_received_count の値を返します
	 *
     * @return int tbe_advance_received_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAdvanceReceivedCount() {
		return $this->_properties['advanceReceivedCount'];
    }


    /**
     * tbe_advance_received_count の値をセットします
	 *
	 * @param int $advanceReceivedCount tbe_advance_received_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAdvanceReceivedCount($advanceReceivedCount) {
		$this->_properties['advanceReceivedCount'] = $advanceReceivedCount;
    }


    /**
     * tbe_deferred_income_count の値を返します
	 *
     * @return int tbe_deferred_income_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeferredIncomeCount() {
		return $this->_properties['deferredIncomeCount'];
    }


    /**
     * tbe_deferred_income_count の値をセットします
	 *
	 * @param int $deferredIncomeCount tbe_deferred_income_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeferredIncomeCount($deferredIncomeCount) {
		$this->_properties['deferredIncomeCount'] = $deferredIncomeCount;
    }


    /**
     * tbe_sales_count の値を返します
	 *
     * @return int tbe_sales_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesCount() {
		return $this->_properties['salesCount'];
    }


    /**
     * tbe_sales_count の値をセットします
	 *
	 * @param int $salesCount tbe_sales_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesCount($salesCount) {
		$this->_properties['salesCount'] = $salesCount;
    }


    /**
     * tbe_commission_count の値を返します
	 *
     * @return int tbe_commission_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCommissionCount() {
		return $this->_properties['commissionCount'];
    }


    /**
     * tbe_commission_count の値をセットします
	 *
	 * @param int $commissionCount tbe_commission_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCommissionCount($commissionCount) {
		$this->_properties['commissionCount'] = $commissionCount;
    }


    /**
     * tbe_advance_received_refund_count の値を返します
	 *
     * @return int tbe_advance_received_refund_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAdvanceReceivedRefundCount() {
		return $this->_properties['advanceReceivedRefundCount'];
    }


    /**
     * tbe_advance_received_refund_count の値をセットします
	 *
	 * @param int $advanceReceivedRefundCount tbe_advance_received_refund_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAdvanceReceivedRefundCount($advanceReceivedRefundCount) {
		$this->_properties['advanceReceivedRefundCount'] = $advanceReceivedRefundCount;
    }


    /**
     * tbe_deferred_income_refund_count の値を返します
	 *
     * @return int tbe_deferred_income_refund_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeferredIncomeRefundCount() {
		return $this->_properties['deferredIncomeRefundCount'];
    }


    /**
     * tbe_deferred_income_refund_count の値をセットします
	 *
	 * @param int $deferredIncomeRefundCount tbe_deferred_income_refund_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeferredIncomeRefundCount($deferredIncomeRefundCount) {
		$this->_properties['deferredIncomeRefundCount'] = $deferredIncomeRefundCount;
    }


    /**
     * tbe_sales_refund_count の値を返します
	 *
     * @return int tbe_sales_refund_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesRefundCount() {
		return $this->_properties['salesRefundCount'];
    }


    /**
     * tbe_sales_refund_count の値をセットします
	 *
	 * @param int $salesRefundCount tbe_sales_refund_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesRefundCount($salesRefundCount) {
		$this->_properties['salesRefundCount'] = $salesRefundCount;
    }


    /**
     * tbe_commission_refund_count の値を返します
	 *
     * @return int tbe_commission_refund_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCommissionRefundCount() {
		return $this->_properties['commissionRefundCount'];
    }


    /**
     * tbe_commission_refund_count の値をセットします
	 *
	 * @param int $commissionRefundCount tbe_commission_refund_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCommissionRefundCount($commissionRefundCount) {
		$this->_properties['commissionRefundCount'] = $commissionRefundCount;
    }


    /**
     * tbe_delete_flag の値を返します
	 *
     * @return char tbe_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tbe_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tbe_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tbe_deletion_datetime の値を返します
	 *
     * @return datetime tbe_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tbe_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tbe_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tbe_update_datetime の値を返します
	 *
     * @return datetime tbe_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tbe_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tbe_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tbe_registration_datetime の値を返します
	 *
     * @return datetime tbe_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tbe_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tbe_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tbe_update_timestamp の値を返します
	 *
     * @return timestamp tbe_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tbe_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tbe_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























