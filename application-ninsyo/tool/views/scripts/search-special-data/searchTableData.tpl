<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">
        
            <h2 id="page_title_{$tabId}">テーブルデータ検索・編集</h2>
            
            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}
            {if $form->getViewOnlyFlag() !== '1'}
                <form name="searchForm" id="searchForm" method="post" onSubmit="return false;">
                    <input type="hidden" name="viewOnlyFlag" value="1">
                    <table class="COM_table">
                        <tr>
                            <td class="COM_nowrap COM_bgColorDeep">
                                テーブル：
                            </td>
                            <td class="COM_nowrap">
                                {$attribs = array()}
                                {$attribs['onChange'] = "KO_currentTab('/tool/search-special-data/search-table-data/viewOnlyFlag/0', $tabId, 'searchForm', []);"}
                                {formSelect name="tableName" options=$tableNameList selected=$form->getTableName() attribs=$attribs addEmpty=true}
                            </td>
                            <td class="COM_nowrap COM_bgColorDeep">
                                PrimaryKey-ID：
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="tableId" value="">
                            </td>
                            <td class="COM_nowrap">

                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap COM_bgColorDeep">
                                指定カラム名Ａ：
                            </td>
                            <td class="COM_nowrap">
                                <select name="keyColumn[0]">
                                {if is_array($form->getTableInfo())}
                                {foreach from=$form->getTableInfo() key="k" item="v"}
                                    <option value="{$k}">{$k}</option>
                                {/foreach}
                                {/if}
                                </select>
                            </td>
                            <td class="COM_nowrap COM_bgColorDeep">
                                値Ａ（カンマつなぎでIN句OK)：
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="keyId[0]" value="">
                            </td>
                            <td class="COM_nowrap">
                                指定カラム名と値はセットで入力
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap COM_bgColorDeep">
                                指定カラム名Ｂ：
                            </td>
                            <td class="COM_nowrap">
                                <select name="keyColumn[1]">
                                {if is_array($form->getTableInfo())}
                                {foreach from=$form->getTableInfo() key="k" item="v"}
                                    <option value="{$k}">{$k}</option>
                                {/foreach}
                                {/if}
                                </select>
                            </td>
                            <td class="COM_nowrap COM_bgColorDeep">
                                値Ｂ（カンマつなぎでIN句OK)：
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="keyId[1]" value="">
                            </td>
                            <td class="COM_nowrap">
                                指定カラム名と値はセットで入力
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap COM_bgColorDeep">
                                指定カラム名Ｃ：
                            </td>
                            <td class="COM_nowrap">
                                <select name="keyColumn[2]">
                                {if is_array($form->getTableInfo())}
                                {foreach from=$form->getTableInfo() key="k" item="v"}
                                    <option value="{$k}">{$k}</option>
                                {/foreach}
                                {/if}
                                </select>
                            </td>
                            <td class="COM_nowrap COM_bgColorDeep">
                                値Ｃ（カンマつなぎでIN句OK)：
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="keyId[2]" value="">
                            </td>
                            <td class="COM_nowrap">
                                指定カラム名と値はセットで入力
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap COM_bgColorDeep">
                                指定カラム名Ｄ：
                            </td>
                            <td class="COM_nowrap">
                                <select name="keyColumn[3]">
                                {if is_array($form->getTableInfo())}
                                {foreach from=$form->getTableInfo() key="k" item="v"}
                                    <option value="{$k}">{$k}</option>
                                {/foreach}
                                {/if}
                                </select>
                            </td>
                            <td class="COM_nowrap COM_bgColorDeep">
                                値Ｄ（カンマつなぎでIN句OK)：
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="keyId[3]" value="">
                            </td>
                            <td class="COM_nowrap">
                                指定カラム名と値はセットで入力
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap COM_bgColorDeep">
                                指定カラム名Ｅ：
                            </td>
                            <td class="COM_nowrap">
                                <select name="keyColumn[4]">
                                {if is_array($form->getTableInfo())}
                                {foreach from=$form->getTableInfo() key="k" item="v"}
                                    <option value="{$k}">{$k}</option>
                                {/foreach}
                                {/if}
                                </select>
                            </td>
                            <td class="COM_nowrap COM_bgColorDeep">
                                値Ｅ（カンマつなぎでIN句OK)：
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="keyId[4]" value="">
                            </td>
                            <td class="COM_nowrap">
                                指定カラム名と値はセットで入力
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap" colspan="4">
                                {newTabButton name="　　検索　　" url="/tool/search-special-data/search-table-data" formId="searchForm" protected=true}
                            </td>
                        </tr>
                    </table>
                </form>
            {/if}
            <!-- // フォーム終了 -->

            <hr>

            {if count($search) === 1}
                {$deleteList = array()}
                {$deleteList[] = 'tabId'}
                {$deleteList[] = 'action'}
                {$deleteList[] = 'module'}
                {$deleteList[] = 'controller'}
                {$deleteList[] = 'tableIdName'}
                {$deleteList[] = 'columnName'}
                {$deleteList[] = 'statusName'}
                {$deleteList[] = 'updateName'}
                {$deleteList[] = 'updateData'}

                {$tableList = $form->getTableList()}
                {$statusList = $form->getStatusList()}
                {$columnName = $form->getColumnName()}
                {$updateColumnNameArr = $form->getUpdateColumnNameArr()}
                {$tableInfo = $form->getTableInfo()}
                {$errMsg = $form->getErrMsg()}

                {currentTabButton name="変更箇所一括更新" url="/tool/search-special-data/update-table-data"  tabId=$tabId formId="updateForm"|cat:$tabId}
                <br>

                <form id="updateForm{$tabId}" method="post" onSubmit="return false;">
                    {hiddenLister hiddenList=$form deleteList=$deleteList}
                    <table>
                        <tr>
                            {foreach from=$tableList key="prefix" item="tableData"}
                            {if isset($search[$prefix]) && count($search[$prefix]) > 0 && isset($tableData['name'])}
                                <td class="COM_top" id="td{$tableData['no']}">
                                    {if isset($columnName[$tableData['table']])}　【<span class="COM_bold COM_fontBlue">{$columnName[$tableData['table']]}</span>】{/if}
                                    <span class="">{$tableData['table']}</span>
                                    <span class="COM_bold COM_fontBlue">　{$search[$prefix]|count}</span>件ヒット<br>
                                    <table class="COM_table">
                                        {foreach from=$search[$prefix][0] key="k" item="v"}
                                            <tr>
                                            {foreach from=$search[$prefix] key="k2" item="v2"}
                                                {if $k2 == 0}
                                                    <td class="COM_bgColorLight COM_center COM_nowrap">
                                                        {if isset($columnName[$k])}<span class="COM_bold">{$columnName[$k]}</span><br>{/if}
                                                        <span class="COM_fontGrey">{$k}</span>
                                                    </td>
                                                {/if}
                                                {$id = ''}
                                                {$id = $tableData['table']|cat:'||'|cat:$k|cat:'||'|cat:$tableData['primary']|cat:'||'|cat:$v2[$tableData['primary']]}
                                                {if isset($errMsg[$id])}
                                                    {$class = 'COM_left COM_nowrap'}
                                                {elseif isset($updateColumnNameArr[$id])}
                                                    {$class = 'COM_left COM_nowrap COM_bgColorGreen'}
                                                {else}
                                                    {$class = 'COM_left COM_nowrap'}
                                                {/if}
                                                {if $k === $tableData['primary']}
                                                    {$onClick = ''}
                                                {elseif stristr($k,'_registration_datetime') || stristr($k,'_update_datetime') || stristr($k,'_update_timestamp')}
                                                    {$onClick = ''}
                                                {else}
                                                    {$onClick = "TOL_updateTableData{$tabId}('{$id}',this);"}
                                                {/if}
                                                <td class="{$class}" onClick="{$onClick}">
                                                    {if stristr($k,'_memo') || strstr($k,'_body')}
                                                        <pre>{$v2[$k]}</pre>
                                                    {elseif stristr($k,'_header') || strstr($k,'_footer')}
                                                        <pre>{$v2[$k]}</pre>
                                                    {else}
                                                        {$v2[$k]}
                                                    {/if}
                                                    {if isset($statusName[$v2[$k]])}
                                                        <br><span class="COM_fontGrey">{$statusName[$v2[$k]]}</span>
                                                    {/if}
                                                    {if isset($errMsg[$id])}
                                                        <br><span class="COM_bold COM_fontRed">{$errMsg[$id]}</span>
                                                    {/if}

                                                    {if $k === $tableData['primary']}
                                                    {elseif $k === $prefix|cat:'_registration_datetime' || $k === $prefix|cat:'_update_datetime' || $k === $prefix|cat:'_update_timestamp'}
                                                    {else}
                                                        <div class="COM_displayNone">
                                                            {if isset($tableInfo[$k])}
                                                                {if isset($tableInfo[$k]) && ($tableInfo[$k]["Type"] === 'text' || $tableInfo[$k]["Type"] === 'mediumtext')}
                                                                    <textarea name="updateDataTemp[{$id}]" cols="30" rows="3">{$v2[$k]}</textarea>
                                                                {elseif isset($tableInfo[$k]) && $tableInfo[$k]["Type"] === 'char(1)'}
                                                                    <select name="updateDataTemp[{$id}]">
                                                                        <option value="0">0</option>
                                                                        <option value="1" {if $v2[$k] != '0'}selected{/if}>1</option>
                                                                    </select>
                                                                {elseif stristr($k,'_type') || stristr($k,'_status')}
                                                                    {$type = substr($k,4)}
                                                                    {if isset($statusList[$type])}
                                                                        {if isset($tableInfo[$k]) && $tableInfo[$k]["Null"] === 'NO'}
                                                                            {statusLister name='updateDataTemp['|cat:$id|cat:']' mode='select' selected=$v2[$k] status=$type}
                                                                        {else}
                                                                            {statusLister name='updateDataTemp['|cat:$id|cat:']' mode='select' selected=$v2[$k] status=$type addEmpty=true}
                                                                        {/if}
                                                                    {else}
                                                                        <input type="text" name="updateDataTemp[{$id}]" value="{$v2[$k]}" size="100">
                                                                    {/if}
                                                                {else}
                                                                    <input type="text" name="updateDataTemp[{$id}]" value="{$v2[$k]}" size="100">
                                                                {/if}
                                                            {/if}
                                                        </div>
                                                    {/if}
                                                </td>
                                            {/foreach}
                                            </tr>
                                        {/foreach}
                                    </table>
                                </td>
                            {/if}
                            {/foreach}
                        </tr>
                    </table>
                </form>
                {if count($updateColumnNameArr) > 0}
                    <!-- // カレントJavascript開始 -->
                    <script type="text/javascript">
                        alert('{$form->getUpdateMsg()}');
                    </script>
                    <!-- // カレントJavascript終了 -->

                {/if}

            {elseif $form->getViewOnlyFlag() === '1'}
                検索結果なし
            {/if}
        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->

<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 更新時表示切替
    //-----------------------------------------------
    function TOL_updateTableData{$tabId}(id,obj){
        body = obj.innerHTML;
        bodySplit = body.split('<div class="COM_displayNone">');
        if (bodySplit[1]) {
            divHtml = bodySplit[1].split('</div>');
            htmlStr = divHtml[0];
            htmlStr = htmlStr + '<input type="hidden" name="updateName[]" value="' + id + '">';
            htmlStr = htmlStr.replace('updateDataTemp','updateData');
            htmlStr = htmlStr.replace('<output','<input');
            obj.innerHTML = htmlStr;
        }
    }

</script>
<!-- // カレントJavascript終了 -->