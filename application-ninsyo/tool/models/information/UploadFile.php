<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Zend/File/Transfer/Adapter/Http.php';
require_once 'Popo/MstInformationHistory.php';

/**
 * 添付ファイルを追加する
 *
 * @author       K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/28
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Information_UploadFile
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 添付ファイルを追加する
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $db = new Mikoshiva_Db_Simple();
        $informationHistoryId = $actionForm->getInformationHistoryId();

        //-----------------------------------------------
        //
        // 添付ファイルの保持
        //
        //-----------------------------------------------
        //-----------------------------------------------------
        // ▼UploadFile 操作クラスのインスタンスを生成
        //-----------------------------------------------------
        $file = null;
        $file = new Zend_File_Transfer_Adapter_Http();

        //-----------------------------------------------------
        // ▼ファイルの移動場所をセット（確認画面では一時保存ディレクトリを指定する）
        //-----------------------------------------------------
        $file->setDestination(UPLOADS_TEMP_DIR);

        //-----------------------------------------------------
        // ▼ファイル名を変更します（この時点では実行されません） ＋ 添付ファイルデータの詰め替え
        //-----------------------------------------------------
        $tempFileList = array();
        $tempFileList = $file->getFileInfo(); // アップロードファイルを取得

        foreach ($tempFileList as $k => $v) {
            if (!isBlankOrNull($v['name'])) {

                // ファイル名
                $hash = '';
                $hash = Mikoshiva_Utility_String::createRandomString(32); // 32 文字のランダム文字列を生成 （ sha1 の方がいい？）

                // ファイル名を変更する
                $filePath = null;
                $filePath = UPLOADS_DIR . '/tool/information/' . $hash;

                $file->addFilter('Rename', $filePath, $k);
                $file->receive($k);

                $informationHistory = $db->simpleOneSelectById('Mikoshiva_Db_Ar_MstInformationHistory', $actionForm->getInformationHistoryId());

                $informationHistory->setBody( $informationHistory->getBody() . '___FILEDATA___' . $v['name'] . '|||' . $hash);

                // 更新（シンプルクラス）
                $result = $db->simpleOneCreateOrUpdateById( 'Mikoshiva_Db_Ar_MstInformationHistory', $informationHistory  );
            }
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $actionForm;
    }
}
