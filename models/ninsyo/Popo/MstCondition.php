<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_condition
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCondition extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mcd';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'conditionId' => null,
        'contentId' => null,
        'serverName' => null,
        'productCode' => null,
        'productShippingCode' => null,
        'packageNumber' => null,
        'designatedDate' => null,
        'havingFlag' => null,
        'serviseInFlag' => null,
        'subscriptionFlag' => null,
        'bundleFlag' => null,
        'purchaseFlag' => null,
        'shippingDataFlag' => null,
        'noneConditionFlag' => null,
        'cancelOkFlag' => null,
        'permissionMonth' => null,
        'permissionDay' => null,
        'needPeriodMonth' => null,
        'needPeriodDay' => null,
        'campaignCode' => null,
        'campaignSalesCode' => null,
        'subscriptionType' => null,
        'productType' => null,
        'chargeType' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mcd_condition_id の値を返します
	 *
     * @return int mcd_condition_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getConditionId() {
		return $this->_properties['conditionId'];
    }


    /**
     * mcd_condition_id の値をセットします
	 *
	 * @param int $conditionId mcd_condition_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setConditionId($conditionId) {
		$this->_properties['conditionId'] = $conditionId;
    }


    /**
     * mcd_content_id の値を返します
	 *
     * @return int mcd_content_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContentId() {
		return $this->_properties['contentId'];
    }


    /**
     * mcd_content_id の値をセットします
	 *
	 * @param int $contentId mcd_content_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContentId($contentId) {
		$this->_properties['contentId'] = $contentId;
    }


    /**
     * mcd_server_name の値を返します
	 *
     * @return varchar mcd_server_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServerName() {
		return $this->_properties['serverName'];
    }


    /**
     * mcd_server_name の値をセットします
	 *
	 * @param varchar $serverName mcd_server_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServerName($serverName) {
		$this->_properties['serverName'] = $serverName;
    }


    /**
     * mcd_product_code の値を返します
	 *
     * @return varchar mcd_product_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductCode() {
		return $this->_properties['productCode'];
    }


    /**
     * mcd_product_code の値をセットします
	 *
	 * @param varchar $productCode mcd_product_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductCode($productCode) {
		$this->_properties['productCode'] = $productCode;
    }


    /**
     * mcd_product_shipping_code の値を返します
	 *
     * @return varchar mcd_product_shipping_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductShippingCode() {
		return $this->_properties['productShippingCode'];
    }


    /**
     * mcd_product_shipping_code の値をセットします
	 *
	 * @param varchar $productShippingCode mcd_product_shipping_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductShippingCode($productShippingCode) {
		$this->_properties['productShippingCode'] = $productShippingCode;
    }


    /**
     * mcd_package_number の値を返します
	 *
     * @return varchar mcd_package_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackageNumber() {
		return $this->_properties['packageNumber'];
    }


    /**
     * mcd_package_number の値をセットします
	 *
	 * @param varchar $packageNumber mcd_package_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackageNumber($packageNumber) {
		$this->_properties['packageNumber'] = $packageNumber;
    }


    /**
     * mcd_designated_date の値を返します
	 *
     * @return date mcd_designated_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDesignatedDate() {
		return $this->_properties['designatedDate'];
    }


    /**
     * mcd_designated_date の値をセットします
	 *
	 * @param date $designatedDate mcd_designated_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDesignatedDate($designatedDate) {
		$this->_properties['designatedDate'] = $designatedDate;
    }


    /**
     * mcd_having_flag の値を返します
	 *
     * @return char mcd_having_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHavingFlag() {
		return $this->_properties['havingFlag'];
    }


    /**
     * mcd_having_flag の値をセットします
	 *
	 * @param char $havingFlag mcd_having_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHavingFlag($havingFlag) {
		$this->_properties['havingFlag'] = $havingFlag;
    }


    /**
     * mcd_servise_in_flag の値を返します
	 *
     * @return char mcd_servise_in_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiseInFlag() {
		return $this->_properties['serviseInFlag'];
    }


    /**
     * mcd_servise_in_flag の値をセットします
	 *
	 * @param char $serviseInFlag mcd_servise_in_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiseInFlag($serviseInFlag) {
		$this->_properties['serviseInFlag'] = $serviseInFlag;
    }


    /**
     * mcd_subscription_flag の値を返します
	 *
     * @return char mcd_subscription_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionFlag() {
		return $this->_properties['subscriptionFlag'];
    }


    /**
     * mcd_subscription_flag の値をセットします
	 *
	 * @param char $subscriptionFlag mcd_subscription_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionFlag($subscriptionFlag) {
		$this->_properties['subscriptionFlag'] = $subscriptionFlag;
    }


    /**
     * mcd_bundle_flag の値を返します
	 *
     * @return char mcd_bundle_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBundleFlag() {
		return $this->_properties['bundleFlag'];
    }


    /**
     * mcd_bundle_flag の値をセットします
	 *
	 * @param char $bundleFlag mcd_bundle_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBundleFlag($bundleFlag) {
		$this->_properties['bundleFlag'] = $bundleFlag;
    }


    /**
     * mcd_purchase_flag の値を返します
	 *
     * @return char mcd_purchase_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseFlag() {
		return $this->_properties['purchaseFlag'];
    }


    /**
     * mcd_purchase_flag の値をセットします
	 *
	 * @param char $purchaseFlag mcd_purchase_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseFlag($purchaseFlag) {
		$this->_properties['purchaseFlag'] = $purchaseFlag;
    }


    /**
     * mcd_shipping_data_flag の値を返します
	 *
     * @return char mcd_shipping_data_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingDataFlag() {
		return $this->_properties['shippingDataFlag'];
    }


    /**
     * mcd_shipping_data_flag の値をセットします
	 *
	 * @param char $shippingDataFlag mcd_shipping_data_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingDataFlag($shippingDataFlag) {
		$this->_properties['shippingDataFlag'] = $shippingDataFlag;
    }


    /**
     * mcd_none_condition_flag の値を返します
	 *
     * @return char mcd_none_condition_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNoneConditionFlag() {
		return $this->_properties['noneConditionFlag'];
    }


    /**
     * mcd_none_condition_flag の値をセットします
	 *
	 * @param char $noneConditionFlag mcd_none_condition_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNoneConditionFlag($noneConditionFlag) {
		$this->_properties['noneConditionFlag'] = $noneConditionFlag;
    }


    /**
     * mcd_cancel_ok_flag の値を返します
	 *
     * @return char mcd_cancel_ok_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelOkFlag() {
		return $this->_properties['cancelOkFlag'];
    }


    /**
     * mcd_cancel_ok_flag の値をセットします
	 *
	 * @param char $cancelOkFlag mcd_cancel_ok_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelOkFlag($cancelOkFlag) {
		$this->_properties['cancelOkFlag'] = $cancelOkFlag;
    }


    /**
     * mcd_permission_month の値を返します
	 *
     * @return int mcd_permission_month
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPermissionMonth() {
		return $this->_properties['permissionMonth'];
    }


    /**
     * mcd_permission_month の値をセットします
	 *
	 * @param int $permissionMonth mcd_permission_month
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPermissionMonth($permissionMonth) {
		$this->_properties['permissionMonth'] = $permissionMonth;
    }


    /**
     * mcd_permission_day の値を返します
	 *
     * @return int mcd_permission_day
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPermissionDay() {
		return $this->_properties['permissionDay'];
    }


    /**
     * mcd_permission_day の値をセットします
	 *
	 * @param int $permissionDay mcd_permission_day
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPermissionDay($permissionDay) {
		$this->_properties['permissionDay'] = $permissionDay;
    }


    /**
     * mcd_need_period_month の値を返します
	 *
     * @return int mcd_need_period_month
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNeedPeriodMonth() {
		return $this->_properties['needPeriodMonth'];
    }


    /**
     * mcd_need_period_month の値をセットします
	 *
	 * @param int $needPeriodMonth mcd_need_period_month
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNeedPeriodMonth($needPeriodMonth) {
		$this->_properties['needPeriodMonth'] = $needPeriodMonth;
    }


    /**
     * mcd_need_period_day の値を返します
	 *
     * @return int mcd_need_period_day
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNeedPeriodDay() {
		return $this->_properties['needPeriodDay'];
    }


    /**
     * mcd_need_period_day の値をセットします
	 *
	 * @param int $needPeriodDay mcd_need_period_day
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNeedPeriodDay($needPeriodDay) {
		$this->_properties['needPeriodDay'] = $needPeriodDay;
    }


    /**
     * mcd_campaign_code の値を返します
	 *
     * @return varchar mcd_campaign_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignCode() {
		return $this->_properties['campaignCode'];
    }


    /**
     * mcd_campaign_code の値をセットします
	 *
	 * @param varchar $campaignCode mcd_campaign_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignCode($campaignCode) {
		$this->_properties['campaignCode'] = $campaignCode;
    }


    /**
     * mcd_campaign_sales_code の値を返します
	 *
     * @return varchar mcd_campaign_sales_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesCode() {
		return $this->_properties['campaignSalesCode'];
    }


    /**
     * mcd_campaign_sales_code の値をセットします
	 *
	 * @param varchar $campaignSalesCode mcd_campaign_sales_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesCode($campaignSalesCode) {
		$this->_properties['campaignSalesCode'] = $campaignSalesCode;
    }


    /**
     * mcd_subscription_type の値を返します
	 *
     * @return varchar mcd_subscription_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionType() {
		return $this->_properties['subscriptionType'];
    }


    /**
     * mcd_subscription_type の値をセットします
	 *
	 * @param varchar $subscriptionType mcd_subscription_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionType($subscriptionType) {
		$this->_properties['subscriptionType'] = $subscriptionType;
    }


    /**
     * mcd_product_type の値を返します
	 *
     * @return varchar mcd_product_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductType() {
		return $this->_properties['productType'];
    }


    /**
     * mcd_product_type の値をセットします
	 *
	 * @param varchar $productType mcd_product_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductType($productType) {
		$this->_properties['productType'] = $productType;
    }


    /**
     * mcd_charge_type の値を返します
	 *
     * @return varchar mcd_charge_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * mcd_charge_type の値をセットします
	 *
	 * @param varchar $chargeType mcd_charge_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * mcd_delete_flag の値を返します
	 *
     * @return char mcd_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mcd_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mcd_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mcd_deletion_datetime の値を返します
	 *
     * @return datetime mcd_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mcd_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mcd_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mcd_registration_datetime の値を返します
	 *
     * @return datetime mcd_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mcd_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mcd_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mcd_update_datetime の値を返します
	 *
     * @return datetime mcd_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mcd_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mcd_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mcd_update_timestamp の値を返します
	 *
     * @return timestamp mcd_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mcd_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mcd_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























