<?php
/**
 * @package Mikoshiva_Settlement
 */

/**
 * require_once
 */
require_once 'Mikoshiva/Settlement/Execution/Order/Interface.php';



/**
 * 決済処理実行クラスの Abstract
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/08
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Settlement_Execution_Order_Abstract implements Mikoshiva_Settlement_Execution_Order_Interface {


    /**
     * Mikoshiva_Settlement_Popo_Input_Order
     * @var Mikoshiva_Settlement_Popo_Input_Order
     */
    protected $_orderInputPopo;


    /**
     * Mikoshiva_Settlement_Popo_Output_Order
     * @var Mikoshiva_Settlement_Popo_Output_Order
     */
    protected $_orderOutputPopo;


    /**
     * コンストラクタ
     *
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/08
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo) {
        $this->_orderInputPopo = $orderInputPopo;
    }


    /**
     * Output クラスを返します
     *
     * @return Mikoshiva_Settlement_Popo_Output_Order Output クラスを返します
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Order/Mikoshiva_Settlement_Execution_Order_Interface#getOutput()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_orderOutputPopo;
    }
}