<?php
require_once 'Mikoshiva/Controller/Action/Front/Abstract.php';

/**
 *
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/22
 * @version SVN:$Id: AddressController.php,v 1.1 2012/09/21 07:08:28 fujimoto Exp $
 */
class Utility_AddressController extends Mikoshiva_Controller_Action_Front_Abstract {

    public function indexAction() {
        $this->_helper->viewRenderer->setNoRender();
    }
    /**
     * $_GET['zip1']-$_GET['zip2']の郵便番号から地名を取得し、JSON形式で返します。
     * 有効でない郵便番号の場合、'error'にエラーメッセージを入れてJSON形式で返します。
     *
     * @return unknown_type
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/05
     * @version
     */
    public function zipToAddressAction() {
    }
}
