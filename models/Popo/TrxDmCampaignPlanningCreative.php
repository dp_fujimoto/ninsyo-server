<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_dm_campaign_planning_creative
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxDmCampaignPlanningCreative extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tdc';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'dmCampaignPlanningCreativeId' => null,
        'dmCampaignPlanningId' => null,
        'number' => null,
        'serverType' => null,
        'fileName' => null,
        'fileNamehash' => null,
        'fileType' => null,
        'fileSize' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * ダイレクトメールキャンペーンプランニングクリエイティブID の値を返します
	 *
     * @return int ダイレクトメールキャンペーンプランニングクリエイティブID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignPlanningCreativeId() {
		return $this->_properties['dmCampaignPlanningCreativeId'];
    }


    /**
     * ダイレクトメールキャンペーンプランニングクリエイティブID の値をセットします
	 *
	 * @param int $dmCampaignPlanningCreativeId ダイレクトメールキャンペーンプランニングクリエイティブID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignPlanningCreativeId($dmCampaignPlanningCreativeId) {
		$this->_properties['dmCampaignPlanningCreativeId'] = $dmCampaignPlanningCreativeId;
    }


    /**
     * ダイレクトメールキャンペーンプランニングID の値を返します
	 *
     * @return int ダイレクトメールキャンペーンプランニングID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignPlanningId() {
		return $this->_properties['dmCampaignPlanningId'];
    }


    /**
     * ダイレクトメールキャンペーンプランニングID の値をセットします
	 *
	 * @param int $dmCampaignPlanningId ダイレクトメールキャンペーンプランニングID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignPlanningId($dmCampaignPlanningId) {
		$this->_properties['dmCampaignPlanningId'] = $dmCampaignPlanningId;
    }


    /**
     * ファイル番号(順番) の値を返します
	 *
     * @return int ファイル番号(順番)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNumber() {
		return $this->_properties['number'];
    }


    /**
     * ファイル番号(順番) の値をセットします
	 *
	 * @param int $number ファイル番号(順番)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNumber($number) {
		$this->_properties['number'] = $number;
    }


    /**
     * サーバータイプ の値を返します
	 *
     * @return varchar サーバータイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServerType() {
		return $this->_properties['serverType'];
    }


    /**
     * サーバータイプ の値をセットします
	 *
	 * @param varchar $serverType サーバータイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServerType($serverType) {
		$this->_properties['serverType'] = $serverType;
    }


    /**
     * ファイル名 の値を返します
	 *
     * @return varchar ファイル名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileName() {
		return $this->_properties['fileName'];
    }


    /**
     * ファイル名 の値をセットします
	 *
	 * @param varchar $fileName ファイル名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileName($fileName) {
		$this->_properties['fileName'] = $fileName;
    }


    /**
     * ファイル名(ハッシュ) の値を返します
	 *
     * @return char ファイル名(ハッシュ)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileNamehash() {
		return $this->_properties['fileNamehash'];
    }


    /**
     * ファイル名(ハッシュ) の値をセットします
	 *
	 * @param char $fileNamehash ファイル名(ハッシュ)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileNamehash($fileNamehash) {
		$this->_properties['fileNamehash'] = $fileNamehash;
    }


    /**
     * ファイルタイプ の値を返します
	 *
     * @return varchar ファイルタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileType() {
		return $this->_properties['fileType'];
    }


    /**
     * ファイルタイプ の値をセットします
	 *
	 * @param varchar $fileType ファイルタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileType($fileType) {
		$this->_properties['fileType'] = $fileType;
    }


    /**
     * ファイルサイズ の値を返します
	 *
     * @return int ファイルサイズ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileSize() {
		return $this->_properties['fileSize'];
    }


    /**
     * ファイルサイズ の値をセットします
	 *
	 * @param int $fileSize ファイルサイズ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileSize($fileSize) {
		$this->_properties['fileSize'] = $fileSize;
    }


    /**
     * メモ の値を返します
	 *
     * @return varchar メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param varchar $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























