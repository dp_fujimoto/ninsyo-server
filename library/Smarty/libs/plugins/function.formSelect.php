<?php

require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


/**
 * 与えられた配列からセレクトボックスのフォームを作成して返します。
 *
 * @param array    $params
 *                      string  name                   フォームのname, id
 *                      string  selected    空文字   | selectedにする値
 *                      array   options     array()  | キー名がvalue、値がラベル名となる配列
 *                      array   attribs     array()  | selectタグに指定する追加の属性 '属性名' => 値
 *                      boolean addEmpty    false    | リストに空白を追加(デフォルト：無効)
 *                      boolean addAllItem  false    | リストに｢全て｣を追加(デフォルト：無効)
 *                      string  listsep     '<br>'   | 各エンティティを区切る文字列
 * @param Smarty  &$smarty
 * @return
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/28
 * @version SVN:$Id: function.formSelect.php,v 1.3 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_formSelect($params, &$smarty, &$template) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default               = array();
    $default['name']       = '';
    $default['selected']   = '';
    $default['options']    = array();
    $default['attribs']    = array();
    $default['addEmpty']   = false;
    $default['addAllItem'] = false;
    $default['listsep']    = '<br>';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(!is_array($data['options'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 options は配列でなければなりません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }

    //-------------------------------------------------------------------------------------------
    //■条件設定に従い、空オプション、｢全て｣オプションを先頭に追加
    //-------------------------------------------------------------------------------------------
    $options = array();
    $options = $data['options'];

    if ($data['addAllItem']) {
        $options = array('0'=>'全て') + $options;
    }
    if ($data['addEmpty']) {
        $options = array(''=>'') + $options;
    }

    $data['options'] = $options;

    //-------------------------------------------------------------------------------------------
    //■idを変換
    //  ここの変換では閉じかっこがなくとも - に変換されるが、これは互換性を保つため。
    //  閉じかっこを考慮するなら、'preg_replace('/\[(.*?)\]/', '-${1}', $name)を再帰的に適用する。
    //-------------------------------------------------------------------------------------------
    $id = '';
    $id = str_replace('[', '-', $data['name']);
    $id = str_replace(']', '', $id);

    //-------------------------------------------------------------------------------------------
    //■optionタグを取得
    //-------------------------------------------------------------------------------------------
    require_once SMARTY_PLUGINS_DIR . 'function.html_options.php';

    $entrustData = array();
    $entrustData['id'] = $id;
    $entrustData['name']     = $data['name'];
    $entrustData['selected'] = $data['selected'];
    $entrustData['options']  = $data['options'];
    $entrustData = $entrustData + $data['attribs'];

    $str = '';
    $str = smarty_function_html_options($entrustData, $smarty, $template);
    //-------------------------------------------------------------------------------------------
    //■listsep適用
    //-------------------------------------------------------------------------------------------
    //$result = preg_replace('/\n/', $data['listsep'] . "\n", $str);
    //$result = $str . $data['listsep'];
    $result = $str;

    Mikoshiva_Logger::info(LOG_END);
    return $result;

}


?>
