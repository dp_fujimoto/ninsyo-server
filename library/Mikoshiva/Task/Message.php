<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Date.php';
require_once 'Mikoshiva/Db/Simple.php';
require_once 'Mikoshiva/Db/Sequencer.php';
require_once 'Mikoshiva/Utility/Integer.php';
require_once 'Mikoshiva/Task/Message/Exception.php';

require_once 'Popo/TrxInquiryDetail.php';

/**
 * Mikoshivaタスクメッセージ
 *
 * @package Mikoshiva_Task
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/15
 * @version SVN:$Id: Message.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Task_Message {

    /** 送受信タイプ (RECEPTION=受信) */
    const STATUS_TRANSCEIVING_TYPE_RECEPTION = 'TRANSCEIVING_TYPE_RECEPTION';

    /** 問合せステータス (NEW=新規) */
    const STATUS_INQUIRY_STATUS_NEW          = 'INQUIRY_STATUS_NEW';

    // エラーメッセージ
    private $_preErrorMessage = 'オペレーターへの通知に失敗しました。';


    /**
     * メールテンプレートの情報を使用してオペレーターへアラートを発行します。
     * 全部署に発行する場合は、配列の値に「0」を含めて下さい。
     *
     * @param string           $subject       問合せ件名
     * @param string           $body          問合せ本文
     * @param array(option)    $divisionValue アラートを表示する部署ID
     * @param int|null(option) $staffId       アラートを表示するスタッフID
     * @param int|null(option) $customerId    アラート対象となる顧客ID
     * @param string           $receptionDatetime   受付日
     * @return boolean
     */
    public function sendAlert(
            $subject, $body,
            $divisionValues = array(), $staffId = null, $customerId = null, $receptionDatetime = null) {
        logInfo(LOG_START);

        $sbj = '';
        $sbj = '【アラート】' . trim(preg_replace('/(\n|\r|\t)/', '', $subject));

        $bool = false;
        $bool = $this->register($sbj, $body, 'INQUIRY_TYPE_ALERT', $divisionValues, $staffId, $customerId, $receptionDatetime);

        logInfo(LOG_END);
        return $bool;
    }


    /**
     * オペレーターへアラートを発行します。
     * 全部署に発行する場合は、配列の値に「0」を含めて下さい。
     *
     * @param string           $templateKey    メールテンプレートキー
     * @param array(option)    $replaceList    置換リスト
     * @param array(option)    $divisionValues アラートを表示する部署ID
     * @param int|null(option) $staffId        アラートを表示するスタッフID
     * @param int|null(option) $customerId     アラート対象となる顧客ID
     * @param string           $receptionDatetime   受付日
     * @return boolean
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/08
     * @version SVN:$Id: Message.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function sendAlertTemplate(
        $templateKey,
        array $replaceList = array(), array $divisionValues = array(), $staffId = null, $customerId = null, $receptionDatetime = null) {


        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");


        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");


        return $this->_sendTemplate('Alert', $templateKey, $replaceList, $divisionValues, $staffId, $customerId, $receptionDatetime);
    }


    /**
     * オペレーターへタスクを発行します。
     * 全部署に発行する場合は、配列の値に「0」を含めて下さい。
     *
     * @param string           $subject       問合せ件名
     * @param string           $body          問合せ本文
     * @param array(option)    $divisionValue アラートを表示する部署ID
     * @param int|null(option) $staffId       アラートを表示するスタッフID
     * @param int|null(option) $customerId    アラート対象となる顧客ID
     * @param string           $receptionDatetime   受付日
     * @return boolean
     */
    public function sendTask(
            $subject, $body,
            $divisionValues = array(), $staffId = null, $customerId = null, $receptionDatetime = null) {
        logInfo(LOG_START);

        $sbj = '';
        $sbj = '【タスク】' . trim(preg_replace('/(\n|\r|\t)/', '', $subject));

        $bool = false;
        $bool = $this->register($sbj, $body, 'INQUIRY_TYPE_TASK', $divisionValues, $staffId, $customerId, $receptionDatetime);

        logInfo(LOG_END);
        return $bool;
    }


    /**
     * メールテンプレートの情報を使用してオペレーターへタスクを発行します。
     * 全部署に発行する場合は、配列の値に「0」を含めて下さい。
     *
     * @param string           $templateKey    メールテンプレートキー
     * @param array(option)    $replaceList    置換リスト
     * @param array(option)    $divisionValues アラートを表示する部署ID
     * @param int|null(option) $staffId        アラートを表示するスタッフID
     * @param int|null(option) $customerId     アラート対象となる顧客ID
     * @param string           $receptionDatetime   受付日
     * @return boolean
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/08
     * @version SVN:$Id: Message.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function sendTaskTemplate(
            $templateKey,
            array $replaceList = array(), array $divisionValues = array(), $staffId = null, $customerId = null, $receptionDatetime = null) {


        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");


        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");


        return $this->_sendTemplate('Task', $templateKey, $replaceList, $divisionValues, $staffId, $customerId, $receptionDatetime);
    }


    /**
     * メールテンプレートの情報を取得し、$fName を元にアラート or タスクの処理を行います
     *
     * @param string           $fName          function 名
     * @param string           $templateKey    メールテンプレートキー
     * @param array(option)    $replaceList    置換リスト
     * @param array(option)    $divisionValues アラートを表示する部署ID
     * @param int|null(option) $staffId        アラートを表示するスタッフID
     * @param int|null(option) $customerId     アラート対象となる顧客ID
     * @param string           $receptionDatetime   受付日
     * @return boolean
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/08
     * @version SVN:$Id: Message.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    private function _sendTemplate(
            $fName, $templateKey,
            array $replaceList = array(), array $divisionValues = array(), $staffId = null, $customerId = null, $receptionDatetime = null) {


        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");


        // ▼インスタンス生成
        include_once 'Mikoshiva/Db/Simple.php';
        $simple = null;
        $simple = new Mikoshiva_Db_Simple();

        // ▼実行 / POPO 取得
        $mailTemplateList = array();
        $mailTemplateList = $simple->simpleManySelect('Mikoshiva_Db_Ar_MstMailTemplate', array('templateKey' => $templateKey));

        // ▼取得出来なければ例外を投げる
        if (count($mailTemplateList) === 0) {
            include_once 'Mikoshiva/Task/Message/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】指定されたキーは存在しませんでした。”{$templateKey}”";
            // ---------------------- ▲ログ終了▲ ---------------------- //
            logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
            throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $message);
        }

        // ▼POPO を取り出す
        /* @var $mailTemplatePopo Popo_MstMailTemplate */
        $mailTemplatePopo = null;
        $mailTemplatePopo = $mailTemplateList[0];

        // ▼置換処理 Subject
        $subject = '';
        $subject = $mailTemplatePopo->getSubject();
        $subject = Mikoshiva_Utility_Convert::templateReplacer($subject, $replaceList);

        // ▼置換処理 Body
        $body = '';
        $body = $mailTemplatePopo->getTextBody();
        $body = Mikoshiva_Utility_Convert::templateReplacer($body, $replaceList);

        // ▼メソッド名を生成
        $functionName = '';
        $functionName = "send{$fName}";

        // ▼実行
        $ret = null;
        $ret = $this->$functionName($subject, $body, $divisionValues, $staffId, $customerId, $receptionDatetime);



        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
        return $ret;
    }


    /**
     * オペレーターへアラートを発行するためのデータを
     * 問合わせテーブルに作成を行います。
     *
     * @param string           $subject       問合せ件名
     * @param string           $body          問合せ本文
     * @param string           $inquiryType   問合せタイプ
     * @param array(option)    $divisionValue アラートを表示する部署ID
     * @param int|null(option) $staffId       アラートを表示するスタッフID
     * @param int|null(option) $customerId    アラート対象となる顧客ID
     * @param string           $receptionDatetime   受付日
     * @return boolean
     */
    private function register(
            $subject, $body, $inquiryType,
            $divisionValues = array(), $staffId = null, $customerId = null, $receptionDatetime = null) {
        logInfo(LOG_START);

        // 問い合わせID
        $inquiryId = null;

        // シーケンステーブルから問い合わせIDを取得します。
        $seq = null;
        $seq = new Mikoshiva_Db_Sequencer();
        $inquiryId = $seq->nextSequenceId('tid_inquiry_id');    // シーケンス名指定
        $seq->closeConnection();                                // コネクションクローズ


        // 部署値を生成します。
        // 引数の値が空の場合、部署値最大を表す「0」値をセットします。
        if (is_null($divisionValues) || ! is_array($divisionValues) || count($divisionValues) === 0) {
            $divisionValues = array();
            $divisionValues[] = 0;
        }

        // $divisionValuesからbit値に変換します
        $divisionValue = null;
        $divisionValue = Mikoshiva_Utility_Integer::convertBitsToIntValue($divisionValues);

        // 問い合わせ番号チェック
        if (!numberCheck($inquiryId)) {
            // 作成失敗
            $massage = '【Mikoshiva_Task_Notice#register】問い合わせ番号取得エラー';
            throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $massage);
        }

        // スタッフIDチェック
        if (! isBlankOrNull($staffId)) {

            // フォーマットチェック
            if (! numberCheck($staffId)) {
                $massage = '【Mikoshiva_Task_Notice#register】スタッフIDの指定が不正です';
                throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $massage);
            }

            // マスタ登録チェック
            $simple = null;
            $simple = new Mikoshiva_Db_Simple();

            /* @var $staff Popo_MstStaff */
            $staff = null;
            $staff = $simple->simpleOneSelectById('Mikoshiva_Db_Ar_MstStaff', $staffId);

            if (isBlankOrNull($staff->getStaffId())) {
                $massage = '【Mikoshiva_Task_Notice#register】未登録のスタッフIDです';
                throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $massage);
            }

        }

        // 顧客IDチェック
        if (! isBlankOrNull($customerId)) {

            // フォーマットチェック
            if (! numberCheck($customerId)) {
                $massage = '【Mikoshiva_Task_Notice#register】顧客IDの指定が不正です';
                throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $massage);
            }

            // マスタ登録チェック
            $simple = null;
            $simple = new Mikoshiva_Db_Simple();

            /* @var $customer Popo_MstCustomer */
            $customer = null;
            $customer = $simple->simpleOneSelectById('Mikoshiva_Db_Ar_MstCustomer', $customerId);

            if (isBlankOrNull($customer->getCustomerId())) {
                $massage = '【Mikoshiva_Task_Notice#register】未登録の顧客IDです';
                throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $massage);
            }

        }

        // Mikoshiva時間
        $now = Mikoshiva_Date::mikoshivaNow();

        if (isBlankOrNull($receptionDatetime)) {
            $receptionDatetime = $now;
        }

        // 問い合わせ詳細のデータ作成用POPOを準備します。
        $tid = null;
        $tid = new Popo_TrxInquiryDetail();

        $tid->setInquiryDetailId(null);                                         // 問合せ詳細ID
        $tid->setInquiryId($inquiryId);                                         // 問合せID
        $tid->setInquiryBranchNumber(1);                                        // 枝番
        $tid->setInquiryCustomerId(null);                                       // 問合せ顧客ID
        $tid->setCustomerId($customerId);                                       // 顧客ID
        $tid->setFormerInquiryDetailId(null);                                   // 対応元問合せ詳細ID
        $tid->setTransceivingType(self::STATUS_TRANSCEIVING_TYPE_RECEPTION);    // 送受信タイプ (RECEPTION=受信)
        $tid->setSubject($subject);                                             // 件名
        $tid->setBody($body);                                                   // 本文
        $tid->setInquiryWay(null);                                              // 問合せ方法 (MAIL=メール)
        $tid->setHeader(null);                                                  // メールヘッダ
        $tid->setSource("MikoshivaSystemMessage");                              // 送信元(From)
        $tid->setDestinationTo(null);                                           // 送信先(to)
        $tid->setDestinationCc(null);                                           // 送信先(Cc)
        $tid->setDestinationBcc(null);                                          // 送信先(Bcc)
        $tid->setDestinationReplyTo(null);                                      // 送信先(Reply-to)
        $tid->setDivisionValue($divisionValue);                                 // 部署値 (2147483647=全部署,bit指定=各部署)
        $tid->setInquiryType($inquiryType);                                     // 問合せタイプ (TASK=タスク,ALERT=アラート)
        $tid->setInquiryLabelId(null);                                          // 問合せラベルID
        $tid->setInquiryStatus(self::STATUS_INQUIRY_STATUS_NEW);                // 問合せステータス (NEW=新規)
        $tid->setStaffId($staffId);                                             // 担当者ID
        $tid->setInquiryGroupingId1(null);                                      // 問合せ分類ID1
        $tid->setInquiryGroupingId2(null);                                      // 問合せ分類ID2
        $tid->setInquiryGroupingId3(null);                                      // 問合せ分類ID3
        $tid->setAppliedSortRuleId(null);                                       // 適用振分ルールID
        $tid->setAppliedTemplateId(null);                                       // 適用テンプレートID
        $tid->setAppliedAddressId(null);                                        // 適用宛先ID
        $tid->setAppliedSignatureId(null);                                      // 適用署名ID
        $tid->setUpdateAllowingFlag('0');                                       // 更新許可フラグ (0=不可,1=可)
        $tid->setTrashFlag('0');                                                // ゴミ箱フラグ (0=指定無し,1=ゴミ箱のみ可)
        $tid->setReceptionDatetime($receptionDatetime);                         // 受付日
        $tid->setMemo(null);                                                    // メモ
        $tid->setDeleteFlag('0');                                               // 削除フラグ
        $tid->setDeletionDatetime(null);                                        // 削除日時
        $tid->setRegistrationDatetime($now);                                    // 登録日時
        $tid->setUpdateDatetime($now);                                          // 更新日時
        $tid->setUpdateTimestamp(null);                                         // システム更新日時


        // 問い合わせ詳細にデータを作成します。
        $db = null;
        $db = new Mikoshiva_Db_Simple();
        $tid = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_TrxInquiryDetail', $tid);

        // データの作成を確認します。
        if (isBlankOrNull($tid->getInquiryDetailId())) {
            // 作成失敗
            $massage = '【Mikoshiva_Task_Notice#register】問い合わせ詳細作成エラー';
            throw new Mikoshiva_Task_Message_Exception($this->_preErrorMessage . $massage);
        }

        logInfo(LOG_END);
        return true;
    }

}