<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Popo/Abstract.php';


/**
 * Popo 関連クラス
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
 */
abstract class Popo_Table_Abstract extends Popo_Abstract{


   /**
    * テーブルのカラムの Prefix
    *
    * @var string
    */
    protected $_columnPrefix;


    /**
     * Prefix 名
     *
     * @param string $prefix
     * @return string
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/21
     * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
     */
    public function getColumnPrefix() {
        return $this->_columnPrefix;
    }
}






















