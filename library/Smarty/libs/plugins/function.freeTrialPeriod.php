<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Zend/Registry.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 【請求タイプ】から【無料お試し期間】を割り出し表示を行います
 *
 * @param array    $params
 *                      string  name フォームのname, id
 * @param Smarty   $smarty
 * @param Template $template
 * @return string
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: function.freeTrialPeriod.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_freeTrialPeriod($params, &$smarty) {

    // ---------------------- ▼ログ開始▼ ---------------------- //
    logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");



    //-----------------------------------------
    //
    // ■デフォルト値を定義
    //
    //-----------------------------------------
    $default           = array();
    $default['mcp']    = null;
    $default['tds']    = null;
    $default['period'] = null;
    $default['period'] = null;
    $default['empty']  = '';


    //-----------------------------------------
    //
    // ■入力値に存在しないキーをデフォルト値で置換
    //
    //-----------------------------------------
    $data = array();
    $data = $params + $default;

    //-----------------------------------------
    //
    // ■値を移し替え
    //
    //-----------------------------------------
    /* @var $mcp Popo_MstCampaignProduct */
    /* @var $tds Popo_TrxDirectionStatus */
    $mcp    = $data['mcp'];
    $tds    = $data['tds'];
    $period = $data['period'];
    $empty  = $data['empty'];



    //-----------------------------------------
    //
    // ■入力値チェック
    //
    //-----------------------------------------
    //include_once 'Popo/MstCampaignProduct.php';
    if(isBlankOrNull($mcp) || ! ($mcp instanceof Popo_MstCampaignProduct)) {
        $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】必須引数 mcp に値がありません。";
        logDebug($message);
        throw new Mikoshiva_View_Helper_Exception($message);
    }
    if(isBlankOrNull($tds) || ! ($tds instanceof Popo_TrxDirectionStatus)) {
        $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】必須引数 tds に値がありません。";
        logDebug($message);
        throw new Mikoshiva_View_Helper_Exception($message);
    }

    //-----------------------------------------
    //
    // ■処理を開始
    //
    // CHARGE_TYPE_CARD             カード
    // CHARGE_TYPE_CASH_ON_DELIVERY 代金引換
    // CHARGE_TYPE_BANK_TRANSFER    銀行振込
    // CHARGE_TYPE_PAYPAL           PayPal
    //
    //-----------------------------------------
    $chargeType               = $tds->getChargeType();
    $freePeriodMedhodName     = ''; // お試し期間
    $freePeriodTypeMedhodName = ''; // お試し期間タイプ
    switch ($chargeType) {

        // ▼カード ------------------------------------------
        case 'CHARGE_TYPE_CARD':
            $freePeriodMedhodName     = 'getFreePeriodCreditCard';
            $freePeriodTypeMedhodName = 'getFreePeriodTypeCreditCard';
            break;

        // ▼代金引換 ------------------------------------------
        case 'CHARGE_TYPE_CASH_ON_DELIVERY':
            $freePeriodMedhodName     = 'getFreePeriodPaymentDelivery';
            $freePeriodTypeMedhodName = 'getFreePeriodTypePaymentDelivery';
            break;

        // ▼銀行振込 ------------------------------------------
        case 'CHARGE_TYPE_BANK_TRANSFER':
            $freePeriodMedhodName     = 'getFreePeriodBankTransfer';
            $freePeriodTypeMedhodName = 'getFreePeriodTypeBankTransfer';
            break;
/******
        // ▼PayPal ------------------------------------------
        case 'CHARGE_TYPE_PAYPAL':
            $ret .= $mcp->getFreePeriodPaymentDelivery();
            $ret .= ' ';
            $ret .= Mikoshiva_Utility_Mapping::statusMapping($mcp->getFreePeriodTypeCreditCard());
            break;
*****/
        default:
            include_once 'Mikoshiva/View/Helper/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】指定された請求タイプには対応していません”{$chargeType}”";
            logDebug($message);
            throw new Mikoshiva_View_Helper_Exception($message);
    }


    //------------------------------------------------------
    //
    // ■表示文言を生成し、返却
    //
    //------------------------------------------------------
    $ret = '';
    //echo $mcp->getCampaignProductId();exit;
    // 期間が０でかつ、 $empty が null で無ければ $empty を返す
    if ((string)$mcp->$freePeriodMedhodName() === '0' && $empty !== null) {
        $ret = $empty;
        return $ret;
    }

    // ▼１年・１ヶ月・１日を生成
    $period     = $mcp->$freePeriodMedhodName();
    $periodType = Mikoshiva_Utility_Mapping::statusMapping($mcp->$freePeriodTypeMedhodName());
    if ($periodType === '月') {
        $periodType = 'ヶ月';
    }

    $ret .= "{$period} {$periodType}";

    // 終了日を生成
    $trialEndDate = '';
    $trialEndDate = _smarty_function_freeTrialPeriod_createTrialEndDate(
        $tds->getProposalDatetime(),
        $mcp->$freePeriodTypeMedhodName(),
        $mcp->$freePeriodMedhodName()
    );

    // ▼お試し終了日
    $ret .= " ({$trialEndDate} 終了)";

    // ▼お試し期間が過ぎていた場合の文言
    //dumper($trialEndDate);
    //dumper(Mikoshiva_Date::mikoshivaNow());
    if (strtotime($trialEndDate) < strtotime(Mikoshiva_Date::mikoshivaNow())) {
        $ret .=  ' 超過！';
    }


    // ---------------------- ▲ログ終了▲ ---------------------- //
    logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
    return $ret;
}

function mm($chargeType) {

}



/**
 * 購入日からお試し期間を算出します
 *
 * @param string $proposalDatetime 購入日
 * @param string $freePeriodType   お試しタイプ
 * @param int    $freePeriod       お試し期間
 * @return string
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/06/15
 * @version SVN:$Id: function.freeTrialPeriod.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function _smarty_function_freeTrialPeriod_createTrialEndDate($proposalDatetime, $freePeriodType, $freePeriod) {


    // ▼日付クラス
    $dateClass = null;
    $dateClass = new Mikoshiva_Date($proposalDatetime);


    // 終了日を生成
    $trialEndDate = '';
    switch (Mikoshiva_Utility_Mapping::statusMapping($freePeriodType)) {

        // 年
        case '年':
            $trialEndDate = $dateClass->addYear($freePeriod)->toString('yyyy-MM-dd');
            break;

        // 年
        case '月':
            $trialEndDate = $dateClass->addMonth($freePeriod)->toString('yyyy-MM-dd');
            break;

        // 年
        case '日':
            $trialEndDate = $dateClass->addDay($freePeriod)->toString('yyyy-MM-dd');
            break;

        default:
    }
    return $trialEndDate;
}























