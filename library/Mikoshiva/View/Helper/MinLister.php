<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Zend/View/Interface.php';
require_once 'Zend/Validate/Date.php';

/**
 * 年のリストを作成します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/16
 * @version SVN:$Id: MinLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_MinLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;

    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function minLister($name, $selected = null, $attribs = null)
    {
        logInfo(LOG_START);
        $minutehList       = array();
        $minutehList[''] = '';
        $minutehList['00'] = '00';
        $minutehList['01'] = '01';
        $minutehList['02'] = '02';
        $minutehList['03'] = '03';
        $minutehList['04'] = '04';
        $minutehList['05'] = '05';
        $minutehList['06'] = '06';
        $minutehList['07'] = '07';
        $minutehList['08'] = '08';
        $minutehList['09'] = '09';
        $minutehList['10'] = '10';
        $minutehList['11'] = '11';
        $minutehList['12'] = '12';
        $minutehList['13'] = '13';
        $minutehList['14'] = '14';
        $minutehList['15'] = '15';
        $minutehList['16'] = '16';
        $minutehList['17'] = '17';
        $minutehList['18'] = '18';
        $minutehList['19'] = '19';
        $minutehList['20'] = '20';
        $minutehList['21'] = '21';
        $minutehList['22'] = '22';
        $minutehList['23'] = '23';
        $minutehList['24'] = '24';
        $minutehList['25'] = '25';
        $minutehList['26'] = '26';
        $minutehList['27'] = '27';
        $minutehList['28'] = '28';
        $minutehList['29'] = '29';
        $minutehList['30'] = '30';
        $minutehList['31'] = '31';
        $minutehList['32'] = '32';
        $minutehList['33'] = '33';
        $minutehList['34'] = '34';
        $minutehList['35'] = '35';
        $minutehList['36'] = '36';
        $minutehList['37'] = '37';
        $minutehList['38'] = '38';
        $minutehList['39'] = '39';
        $minutehList['40'] = '40';
        $minutehList['41'] = '41';
        $minutehList['42'] = '42';
        $minutehList['43'] = '43';
        $minutehList['44'] = '44';
        $minutehList['45'] = '45';
        $minutehList['46'] = '46';
        $minutehList['47'] = '47';
        $minutehList['48'] = '48';
        $minutehList['49'] = '49';
        $minutehList['50'] = '50';
        $minutehList['51'] = '51';
        $minutehList['52'] = '52';
        $minutehList['53'] = '53';
        $minutehList['54'] = '54';
        $minutehList['55'] = '55';
        $minutehList['56'] = '56';
        $minutehList['57'] = '57';
        $minutehList['58'] = '58';
        $minutehList['59'] = '59';

        // ◆selected 2009/12/24 00:00:00 で来た場合にも対応
        $_selected = $selected;
        if (isset($_selected)) {
            $_selected = str_replace('/', '-', $_selected );
            $validator1 = new Zend_Validate_Date('Y-m-d H:i:s');
            if ($validator1->isValid($_selected)) {

                $matche = array();
                preg_match('#^\d{4}-\d{2}-\d{2} \d{2}:(\d{2}):\d{2}#', $_selected, $matche);
                $_selected = $matche[1];
            }
        }

        logInfo(LOG_END);
        return $this->view->formSelect($name,
                                       $_selected,
                                       $attribs,
                                       $minutehList);
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
