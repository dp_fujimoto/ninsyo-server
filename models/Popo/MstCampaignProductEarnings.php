<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_campaign_product_earnings
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCampaignProductEarnings extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mea';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'campaignProductEarningsId' => null,
        'campaignProductId' => null,
        'serviceStartDate' => null,
        'firstPrice' => null,
        'subscription' => null,
        'divideNumber' => null,
        'productCategory' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * キャンペーン商品収支ID の値を返します
	 *
     * @return int キャンペーン商品収支ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductEarningsId() {
		return $this->_properties['campaignProductEarningsId'];
    }


    /**
     * キャンペーン商品収支ID の値をセットします
	 *
	 * @param int $campaignProductEarningsId キャンペーン商品収支ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductEarningsId($campaignProductEarningsId) {
		$this->_properties['campaignProductEarningsId'] = $campaignProductEarningsId;
    }


    /**
     * キャンペーン商品ID の値を返します
	 *
     * @return int キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductId() {
		return $this->_properties['campaignProductId'];
    }


    /**
     * キャンペーン商品ID の値をセットします
	 *
	 * @param int $campaignProductId キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductId($campaignProductId) {
		$this->_properties['campaignProductId'] = $campaignProductId;
    }


    /**
     * サービス開始日 の値を返します
	 *
     * @return varchar サービス開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceStartDate() {
		return $this->_properties['serviceStartDate'];
    }


    /**
     * サービス開始日 の値をセットします
	 *
	 * @param varchar $serviceStartDate サービス開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceStartDate($serviceStartDate) {
		$this->_properties['serviceStartDate'] = $serviceStartDate;
    }


    /**
     * 初回金額 の値を返します
	 *
     * @return int 初回金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstPrice() {
		return $this->_properties['firstPrice'];
    }


    /**
     * 初回金額 の値をセットします
	 *
	 * @param int $firstPrice 初回金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstPrice($firstPrice) {
		$this->_properties['firstPrice'] = $firstPrice;
    }


    /**
     * 継続金額 の値を返します
	 *
     * @return int 継続金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscription() {
		return $this->_properties['subscription'];
    }


    /**
     * 継続金額 の値をセットします
	 *
	 * @param int $subscription 継続金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscription($subscription) {
		$this->_properties['subscription'] = $subscription;
    }


    /**
     * 分割回数 の値を返します
	 *
     * @return int 分割回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivideNumber() {
		return $this->_properties['divideNumber'];
    }


    /**
     * 分割回数 の値をセットします
	 *
	 * @param int $divideNumber 分割回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivideNumber($divideNumber) {
		$this->_properties['divideNumber'] = $divideNumber;
    }


    /**
     * 商品区分 の値を返します
	 *
     * @return varchar 商品区分
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductCategory() {
		return $this->_properties['productCategory'];
    }


    /**
     * 商品区分 の値をセットします
	 *
	 * @param varchar $productCategory 商品区分
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductCategory($productCategory) {
		$this->_properties['productCategory'] = $productCategory;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























