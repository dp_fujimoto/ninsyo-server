<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';

/**
 * 全てのタブを閉じるjavascriptを返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: CloseTabAll.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_CloseTabAll extends Mikoshiva_View_Helper_Abstract {

    /**
     * 全てのタブを閉じるjavascriptを返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: CloseTabAll.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @return string
     */
    public function closeTabAll() {

        return 'KO_closeTabAll();';
    }
}
