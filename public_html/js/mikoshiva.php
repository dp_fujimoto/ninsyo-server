

var _tabStatusProperties = function() {
    this.namespaceList  = [];
    this.tabRootPoint   = false;
    this.tabRootThemeId = 0;
    this.saveName       = null;
}
//------------------------------------------------------------
//
// ■初期化
//
//-------------------------------------------------------------
var _tabNumber    = 2; // タブ番号 常に自動裁判され続けます
//var _tabStatusProperties = {'namespaceList': []};
var _tabStatus    = {0: new _tabStatusProperties()};
var _nowTabActive = 0;      // 現在のアクティブなタブ ID
var _tabRootInfo  = {0: {}}; // tab ルート情報
var _tabNamespace = {0: ''}; // tab の名前空間
var _latestGroupId = 0;

/**
 * 親メニューがクリックされたときに新しいタブを開きます
 *
 * @return
 */
function OYA_newTab(url) {


    //------------------------------------------------------------
    // ■初期化
    //-------------------------------------------------------------
    ++_tabNumber;            // タブ ID をカウントアップ
    tabStatus     = {};
    _tabStatus     = {0: new _tabStatusProperties()}; // タブステータス
    _nowTabActive = 0;       // 現在のアクティブなタブ ID
    _tabRootInfo  = {0: {}}; // tab ルート情報
    _tabNamespace = {0: ''}; // tab の名前空間
    _latestGroupId = 0;


    //------------------------------------------------------------
    // ■tab ルート情報を更新
    //-------------------------------------------------------------
    _tabRoot_add(url, _tabRootInfo, 0, _tabNumber, false);

    //console.log(_tabRootInfo);
    //console.log(_tabStatus);



    // ◆タブを初期化
    $('#tab').html('<ul id="sub-menu"></ul>');

    // ◆tab直下の全div要素を非表示
    $('#tab > div').hide();


    // ◆タブを作成
    $('<li id="tab_li_' + _tabNumber + '"><span  onclick="KO_focusTab(' + _tabNumber + ')">' + getGroup(_tabNumber) + 'タブ&ensp;' + (_tabNumber) + '</span>&ensp;<span onclick="closeTab(_tabRootInfo, ' + _tabNumber + ', false);"><img src="/img/close_button.gif" width="12" hight="10"></span></li>').addClass('selectOn').appendTo('#sub-menu');

    // ◆コンテンツの箱を作成し、読み込み完了までローディングアニメーションを表示
    $('<div id="tab_div_' + (_tabNumber) + '"><p align="center"><img src="/img/loading.gif"></p></div>').appendTo('#tab');

    // ◆アクションを叩きコンテンツの箱に上書き
    $.post(url, 'tabId=' + _tabNumber, function(data, status){
        $('#tab_div_' + _tabNumber).html(data)
    });

    // ◆sub-menu直下の全li要素にマウスオーバーしたらリンク要素に偽装
    $('#sub-menu > li').hover(
        function(){$(this).css('cursor','pointer');},
        function(){$(this).css('cursor','default');}
    );

    //tabStatus[_tabNumber] = {'save': true};

    // 現在のアクティブなタブ
    _nowTabActive = _tabNumber;

    // 親メニューのリンクをセットします
    $('#systemInfoLastOyaUrl').html(url);


    // 画面位置を一番上に戻す
    $('html,body').animate({ scrollTop: 0 }, 0);
}



/**
 * アクションが叩かれたときに自分のタブに表示する処理
 *
 * @return
 */
function KO_currentTab(url, tabId, formId, onloadList) {


    //------------------------------------------
    // ◆form の値を一括取得し、クエリの生成を行います
    //------------------------------------------
    var _form = 'tabId=' + tabId;
    if (formId !== null) {
        _form += '&' + $('#' + formId).serialize();
    }


    //------------------------------------------
    // ◆namespaceList を上書き
    //------------------------------------------
    //_tabStatus[tabId]['namespaceList'][0] = url;


    //------------------------------------------
    //■type="file" が存在するかをチェック
    //------------------------------------------
    var uploadFormCheck = false;
    if (formId !== null) {
        if ($('#' + formId + ' input[type="file"]').length > 0) {
            uploadFormCheck = true;
        }
    }


    //------------------------------------------
    //■POST、結果を新規コンテンツに埋める
    //------------------------------------------
    if (uploadFormCheck) {
//alert('ファイルアップロードの場合');
        // ファイルアップロードの場合
        $('#' + formId).upload(url, _form, function(res) {
                $('#tab_div_' + tabId).html(res)
        }, 'text');

    } else {
//alert('ファイルアップロードじゃない場合');
        // ファイルアップロードじゃない場合
        $.post(url, _form, function(data, status){
            $('#tab_div_' + tabId).html(data)
        });
    }



    //------------------------------------------
    // ■現在のアクティブなタブをセット
    //------------------------------------------
    _nowTabActive = tabId;


    //------------------------------------------
    // ■画面位置を一番上に戻す
    //------------------------------------------
    $('html,body').animate({ scrollTop: 0 }, 0);
}

/**
 * アクションを叩くときに新しいタブを作成する処理
 *
 * @return
 */
function KO_newTab(url, formId, saveName) {


    //------------------------------------------------------------
    // ▼現在のアクティブなタブに対して名前を付ける
    //------------------------------------------------------------
    if (saveName !== '') {
        _tabStatus[_nowTabActive]['saveName'] = saveName;
    }


    //------------------------------------------------------------
    // ▼タブナンバーをカウントアップ
    //------------------------------------------------------------
    ++_tabNumber;


    //------------------------------------------------------------
    // ■tab ルート情報を更新
    //-------------------------------------------------------------
    _tabRoot_add(url, _tabRootInfo, _nowTabActive, _tabNumber, false);
    //console.log(_tabRootInfo);
    //console.log(_tabStatus);


    //------------------------------------------------------------
    // ▼form の値を一括取得
    //------------------------------------------------------------
    var _form = 'tabId=' + _tabNumber;
    if (formId !== '') {
        _form += '&' + $('#' + formId).serialize();
    }


    //------------------------------------------------------------
    //■type="file" が存在するかをチェック
    //------------------------------------------------------------
    var uploadFormCheck = false;
    if (formId !== '') {
        if ($('#' + formId + ' input[type="file"]').length > 0) {
            uploadFormCheck = true;
        }
    }


    // ◆sub-menu直下の全 li 要素の class 属性を削除
    //   一度全てのタブを非アクティブにする
    $('#sub-menu > li').removeClass('selectOn');
    $('#sub-menu > li').addClass('selectOff');


    // ◆tab 直下の全 div 要素（コンテンツ）を非表示
    $('#tab > div').hide();


    // ◆クリックしたタブのインデックス番号と同じdiv要素をフェード表示
    $('#tab > div').eq($('#sub-menu > li').index(this)).show();


    // ◆タブを新規作成し、アクティブ表示にする
    $('<li id="tab_li_' + _tabNumber + '"><span onclick="KO_focusTab(' + _tabNumber + ')">' + getGroup(_tabNumber) + 'タブ&ensp;' + _tabNumber + '</span>&ensp;<span onclick="closeTab(_tabRootInfo, ' + _tabNumber + ', false);"><img src="/img/close_button.gif" width="12" hight="10"></span></li>').addClass('selectOn').appendTo('#sub-menu');


    // ◆コンテンツの箱を作成し、ローディングアニメーションを表示
    $('<div id="tab_div_' + _tabNumber + '"><p align="center"><img src="/img/loading.gif"></p></div>').appendTo('#tab');


    //------------------------------------------
    //■POST、結果を新規コンテンツに埋める
    //------------------------------------------
    if (uploadFormCheck) {
//alert('ファイルアップロードの場合');
        // ファイルアップロードの場合
        $('#' + formId).upload(url, _form, function(res) {
                $('#tab_div_' + _tabNumber).html(res)
        }, 'text');

    } else {
//alert('ファイルアップロードじゃない場合');
        // ファイルアップロードじゃない場合
        $.post(url, _form, function(data, status){
            $('#tab_div_' + _tabNumber).html(data)
        });
    }


    //------------------------------------------
    //■ID 属性の変更
    //------------------------------------------
    var mm = $('#tab_div_' + _nowTabActive + ' [id]');
    for (var i = 0; i < mm.length; i++) {

        //alert($("#tab_div_" + _tabNumber + " [id]").eq(i).attr('id'));
        var idName  = $("#tab_div_" + _nowTabActive + " [id]").eq(i).attr('id');
            idName += '_tabId_' + _nowTabActive;
            $("#tab_div_" + _nowTabActive + " [id]").eq(i).attr('id', idName);
    }


    // ◆sub-menu直下の全li要素にマウスオーバーしたらリンク要素に偽装
    $('#sub-menu > li').hover(
        function(){$(this).css('cursor','pointer');},
        function(){$(this).css('cursor','default');}
    );



    // 現在のアクティブなタブ
    _nowTabActive = _tabNumber;

    // 画面位置を一番上に戻す
    $('html,body').animate({ scrollTop: 0 }, 0);
}

/**
 * 選択されたタブに対しフォーカスを当てます
 *
 * @param ko_menu
 * @return
 */
function KO_focusTab(ko_menu) {

    //------------------------------------------
    //■ID 属性の変更
    //------------------------------------------
    var mm = $('#tab_div_' + _nowTabActive + ' [id]');

    for (var i = 0; i < mm.length; i++) {

        //alert($('#tab_div_' + _nowTabActive + ' [id]').eq(i).attr('id'));
        var idName  = $("#tab_div_" + _nowTabActive + " [id]").eq(i).attr('id');
            idName += '_tabId_' + _nowTabActive;
            $("#tab_div_" + _nowTabActive + " [id]").eq(i).attr('id', idName);
    }


    //------------------------------------------
    //■ID 属性の復元
    //------------------------------------------
    var mm = $('#tab_div_' + ko_menu + ' [id]');

    for (var i = 0; i < mm.length; i++) {

        //alert($('#tab_div_' + ko_menu + ' [id]').eq(i).attr('id'));
        var idName  = $("#tab_div_" + ko_menu + " [id]").eq(i).attr('id');
            idName  = idName.replace('_tabId_' + ko_menu, '');
            $("#tab_div_" + ko_menu + " [id]").eq(i).attr('id', idName);
    }



    // ◆#sub-menu 直下の全 li 要素の class 属性を削除
    //   一度全てのタブを非アクティブにする
    $('#sub-menu > li').removeClass('selectOn');
    $('#sub-menu > li').addClass('selectOff');


    // ◆#tab直下の全div要素を非表示
    //   全コンテンツを一度非アクティブにする
    $('#tab > div').hide();


    // ◆クリックした li 要素に select を追加
    //   クリックしたタブをアクティブにする
    $('#tab_li_' + ko_menu).removeClass('selectOff');
    $('#tab_li_' + ko_menu).addClass('selectOn');



    // ◆クリックしたタブのインデックス番号と同じdiv要素をフェード表示
    //   クリックされたタブのコンテンツ内容を表示
    //////$('#tab > div').eq( $('#sub-menu > li').index(ko_menu) ).show();
    $('#tab_li_' + ko_menu).show();
    $('#tab_div_' + ko_menu).show();


    // ◆sub-menu直下の全li要素にマウスオーバーしたらリンク要素に偽装
    $('#sub-menu > li').hover(
        function(){$(this).css('cursor','pointer');},
        function(){$(this).css('cursor','default');}
    );

    // 現在のアクティブなタブ
    _nowTabActive = ko_menu;

    // 画面位置を一番上に戻す
    $('html,body').animate({ scrollTop: 0 }, 0);
}


/**
 * 選択されたタブを削除します
 *
 * @param ko_menu
 * @return
 */
function KO_closeTab(tabnum) {

    // 現在タブより左側のタブのindex番号を取得（タブIDではない）
    var nowTabId = $('#sub-menu > li').index($('#tab_li_' + tabnum));

    // フォーカスをアクティブなタブの左のタブへ移す
    if ($('#sub-menu > li:nth-child(' + nowTabId + ')').attr('id') !== undefined) {
        var leftTabId = $('#sub-menu > li:nth-child(' + ( nowTabId ) + ')').attr('id').replace(/tab_li_/, '');

        // 現在タブと削除するタブが同じで有ればフォーカスを移す
        if ( String(tabnum) === String(_nowTabActive)) KO_focusTab(leftTabId);

    }
    else {
        // 現在のアクティブなタブ
        _nowTabActive = 0;
    }

    // 現在タブの削除
    $('#tab_li_' + tabnum).remove();
    $('#tab_div_' + tabnum).remove();

    // タブステータスから削除
    if (_tabStatus[tabnum]) {
        delete _tabStatus[tabnum];
    }

    // 画面位置を一番上に戻す
    $('html,body').animate({ scrollTop: 0 }, 0);

}

/**
 * 全てのタブを閉じます
 *
 * @return

function KO_closeTabAll() {

    $('#tab').html('');
    tabStatus = {};

    // 現在のアクティブなタブ
    _nowTabActive = 0;

    // 画面位置を一番上に戻す
    $('html,body').animate({ scrollTop: 0 }, 0);
}
 */



/**
 * タブ情報を追加します
 *
 *  // サンプルデータ
 *  // a[1][2][3][5]
 *  //        [4][6]
 *  var a = {};
 *  a = {1: {2: {3: {5: null},
 *               4: {6: null}
 *               }
 *          }
 *      };
 *
 *
 * @var tabRootInfo  string|array タブ情報
 * @var currentTabId string       現在のアクティブなタブ ID
 * @var newTabId     string       新規に追加するタブ ID
 */
function _tabRoot_add (namespace, tabRootInfo, currentTabId, newTabId, deleteFlag) {




// console.log('------------------------');
// console.log('443:');
// console.log(tabRootInfo);
// console.log('------------------------');
    //-------------------------------------------
    // ■tabRootInfo を再帰的に処理し、
    //   pointerTabId と同じタブ ID 以降のタブを削除します
    //   tabRootInfo[1][2][3][4] // ルートと呼称します
    //                [5][6] // ルートと呼称します
    //-------------------------------------------
    if (typeof tabRootInfo === 'object') {

        for(i in tabRootInfo) {

            // 処理
            if (i == currentTabId) {


                //-------------------------------------------
                // ■新規タブで開こうとしている URL が
                //   currentTabId 直後に開いているタブの URL と同じ URL で有れば
                //   そのルートごと削除する
                // 例：[1][2][3][4] 2 に戻り、再度 3 を newTab で開こうとした場合のケースです
                //--------------------------------------------
                var _namespace;
                _namespace = ((namespace + '/').match(/(^\/.*?\/.*?\/.*?\/)/)||[])[1]||null;

                if (_namespace === null) {
                    _namespace = (namespace + '/').match(/(^\/.*?\/.*?\/)/)[1];
                    _namespace = _namespace + 'index/';
                }
                //alert(_namespace);
                for (k in tabRootInfo[i]) {

                    if (_tabStatus[k]['namespaceList'][0] === _namespace) {
                        var temp_i = i;
                        var temp_k = k;
                        arguments.callee(namespace, tabRootInfo[i][k], currentTabId, newTabId, true);
                        KO_closeTab(temp_k);
                        delete tabRootInfo[temp_i][temp_k];
                        //delete _tabStatus[i][j];
                    }
                }






                // ▼現在のアクティブなタブ ID のルート配列に追加します
                //   追加して終了
                //
                //   ここで Object を new することで tabRootInfo[4] が削除される（currentTabId が 3 である場合）
                //   newTabId を追加することで以下の様なルートが出来上がる
                //   tabRootInfo[1][2][3][7]
                //
                //   currentTabId が 2 である場合 tabRootInfo[1][2][7] となります。
                //tabRootInfo[i]           = new Object();
                //tabRootInfo[i][newTabId] = new Object();
                //arguments.callee(namespace, tabRootInfo[i], currentTabId, newTabId, true);
                //tabRootInfo[currentTabId]           = new Object();
                tabRootInfo[currentTabId][newTabId] = new Object();

                _tabStatus[newTabId] = new _tabStatusProperties();
                _tabStatus[newTabId]['namespaceList'].push(_namespace);

                if (_tabStatus[currentTabId]['tabRootPoint'] === false) {
                    _tabStatus[newTabId]['tabRootThemeId'] = _tabStatus[currentTabId]['tabRootThemeId'];
                    _tabStatus[currentTabId]['tabRootPoint'] = true;
                } else {
                    ++_latestGroupId;
                    _tabStatus[newTabId]['tabRootThemeId'] = _latestGroupId;
                }


                return;

            } else if (deleteFlag === true) {

                // ▼tabRootInfo[3] を削除するため tabRootInfo[3] 以降が削除されます
                //   tabRootInfo[3][4] が削除される
                //delete tabRootInfo[i];
                KO_closeTab(i);
                arguments.callee(namespace, tabRootInfo[i], currentTabId, newTabId, true);
                delete tabRootInfo[i];
                //return;

            } else {

                // ▼currentTabId が見つかるまで再帰的に探しに行く
                // console.log('------------------------');
                // console.log('489:');
                // console.log(i);
                // console.log('------------------------');
                arguments.callee(namespace, tabRootInfo[i], currentTabId, newTabId, false);
            }
        }
    }
    else {
        // console.log('------------------------');
        // console.log('500:');
        // console.log(i);
        // console.log('------------------------');
        //pp = 1;
    }
}



/**
 * 指定された Tab へフォーカスを移し、その Tab 以降の Tab を閉じます
 *
 *
 * 例：以下の様に Tab を開いていたと仮定
 *     [g] から [d] に戻りたいとした場合、[e][f][g] が閉じられます
 *     [d][h][i] のルートは存続されます
 *
 * tabInfo[1][2][3][4][5][6]
 *                    [7][8]
 *           [9][a][b][c]
 *                 [d][e][f]
 *                       [g]
 *                    [h][i]
 *
 *
 *
 * @var tabInfo      string|array タブ情報
 * @var pointerTabId string       戻したいタブ ID
 * @var closeTabId   string       closeTab が実行されたタブ ID
 * @var deleteFlag   boolean      closeTab が実行されたタブ ID
 */
function closeTabLocation (tabRootInfo, pointerName, closeTabId, deleteFlag) {



    if (typeof tabRootInfo === 'object') {

        for(i in tabRootInfo) {

            // [d] に戻りたいと仮定
            // [d] の saveName とリクエストの pointerName が同じ場合
            if (_tabStatus[i]['saveName'] === pointerName) {


                // 閉じるボタンを押されたタブのグループ名を取得
                // 例：[g] が閉じるボタンを押されたタブと仮定する
                var closeTabGroupName = getGroup(closeTabId);
                var temp_i = i; // 戻りたいタブ ID


                // [d] 以下の同じグループを閉じる
                closeRet = false; //
                for (k in tabRootInfo[i]) {

                    // [g] のタブグループと [d] 以下のタブグループが同じ場合閉じて行く
                    // [e] が同じグループである場合、[e][f][g]が閉じられます
                    if (closeTabGroupName === getGroup(k)) {
                        KO_closeTab(k);
                        arguments.callee(tabRootInfo[i][k], pointerName, closeTabId, true);
                        delete tabRootInfo[temp_i][k];
                        closeRet = true;
                        break;
                    }
                }

                if (closeRet === false) {
                    //i = temp_i;
                    continue;
                }

                // フォーカスを pointerName のタブに写す
                KO_focusTab(temp_i);
                //console.log(_tabStatus);
                break;


            } else if (deleteFlag === true) {

                // ▼tabInfo[3] を削除するため tabInfo[3] 以降が削除されます
                //   tabInfo[3][4] が削除される
                KO_closeTab(i);
                arguments.callee(tabRootInfo[i], pointerName, closeTabId, true);
                delete tabRootInfo[i];

            } else {

                // ▼pointerTabId が見つかるまで再帰的に探しに行く
                arguments.callee(tabRootInfo[i], pointerName, closeTabId, false);
            }
        }
    }
    else {
        //pp = 1;
    }
}







/**
 * 指定された Tab を含めた、以降の Tab を全て閉じます
 *
 *
 * 例：以下の様に Tab を開いていたと仮定
 *     [d] を閉じると仮定した場合、[d][e][f][g][h][i] が閉じられます
 *     フォーカスは KO_closeTab の仕様に準処します
 *
 * tabInfo[1][2][3][4][5][6]
 *                    [7][8]
 *           [9][a][b][c]
 *                 [d][e][f]
 *                       [g]
 *                    [h][i]
 *
 * @var tabInfo      string|array タブ情報
 * @var pointerTabId string       戻したいタブ ID
 * @var closeTabId   string       closeTab が実行されたタブ ID
 * @var deleteFlag   boolean      closeTab が実行されたタブ ID
 */
function closeTab (tabRootInfo, closeTabId, deleteFlag) {



    if (typeof tabRootInfo === 'object') {

        for(i in tabRootInfo) {


            // [d] を閉じたいと仮定
            // [d] の TabId と i が同じで有れば、
            if (i == closeTabId) {


                // [d] 以下の同じグループを閉じ、自信の tabId も閉じる
                for (k in tabRootInfo[closeTabId]) {

                    // [d][e][f][g][h][j] を閉じます
                    KO_closeTab(k);
                    arguments.callee(tabRootInfo[closeTabId][k], closeTabId, true);
                    delete tabRootInfo[closeTabId][k];
                }


                KO_closeTab(closeTabId);
                delete tabRootInfo[closeTabId];
                return;

            } else if (deleteFlag === true) {

                // ▼tabInfo[3] を削除するため tabInfo[3] 以降が削除されます
                //   tabInfo[3][4] が削除される
                KO_closeTab(i);
                arguments.callee(tabRootInfo[i], closeTabId, true);
                delete tabRootInfo[i];

            } else {

                // ▼pointerTabId が見つかるまで再帰的に探しに行く
                arguments.callee(tabRootInfo[i], closeTabId, false);
            }
        }
    }
    else {
        //pp = 1;
    }
}



/**
 * 指定された TabID のグループを取得する
 */
function getGroup(tabId) {

    var vv = ['■', '◆', '▲', '▼', '♪', '♯', '&#9824;', '&#9829;', '&#9827;', '&#9830;', '％', '￥', '＠', '＊', '♂', '♀', '〒', '＄', '&#1044;', '&#974;', '&#169;', '&#230;', '&#510;'];
    if (! _tabStatus[tabId]) {
        return '';
    }
    var groupId = _tabStatus[tabId]['tabRootThemeId'];
    return vv[groupId];
}










