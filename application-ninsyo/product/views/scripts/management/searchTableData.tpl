<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">テーブルデータ簡易検索</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="searchForm{$tabId}" id="searchForm{$tabId}" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="mode" value="search">

                <!-- 検索条件テーブル 開始-->
                <table class="COM_table" >
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorDeep">テーブル名</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorDeep">ボタン</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorDeep" colspan="2">項目１（primary key）</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorDeep" colspan="2">項目２</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorDeep" colspan="2">項目３</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorDeep" colspan="2">項目４</td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight">ブランド</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight"><input type="button" id="chgButton1" value="表示OFF" onclick="PDT_searchResultDisplayChg{$tabId}('1');"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">ブランドID</td>
                        <td class="COM_nowrap"><input type="text" name="mbrBrandId" value="{$form->getMbrBrandId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">部署ID</td>
                        <td class="COM_nowrap"><input type="text" name="mbrDivisionId" value="{$form->getMbrDivisionId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight">商品</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight"><input type="button" id="chgButton2" value="表示OFF" onclick="PDT_searchResultDisplayChg{$tabId}('2');"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">商品ID</td>
                        <td class="COM_nowrap"><input type="text" name="mprProductId" value="{$form->getMprProductId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">ブランドID</td>
                        <td class="COM_nowrap"><input type="text" name="mprBrandId" value="{$form->getMprBrandId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight">コンテンツ</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight"><input type="button" id="chgButton3" value="表示OFF" onclick="PDT_searchResultDisplayChg{$tabId}('3');"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">コンテンツID</td>
                        <td class="COM_nowrap"><input type="text" name="mctContentId" value="{$form->getMctContentId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">商品ID</td>
                        <td class="COM_nowrap"><input type="text" name="mctProductId" value="{$form->getMctProductId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight">条件</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight"><input type="button" id="chgButton4" value="表示OFF" onclick="PDT_searchResultDisplayChg{$tabId}('4');"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">条件ID</td>
                        <td class="COM_nowrap"><input type="text" name="mcdConditionId" value="{$form->getMcdConditionId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">コンテンツID</td>
                        <td class="COM_nowrap"><input type="text" name="mcdContentId" value="{$form->getMcdContentId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight">ユーザー</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight"><input type="button" id="chgButton5" value="表示OFF" onclick="PDT_searchResultDisplayChg{$tabId}('5');"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">ユーザーID</td>
                        <td class="COM_nowrap"><input type="text" name="musUserId" value="{$form->getMusUserId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight">ユーザーコンテンツ</td>
                        <td class="COM_nowrap COM_tdWidthBasic COM_bgColorLight"><input type="button" id="chgButton6" value="表示OFF" onclick="PDT_searchResultDisplayChg{$tabId}('6');"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">ユーザーコンテンツID</td>
                        <td class="COM_nowrap"><input type="text" name="mucUserContentId" value="{$form->getMucUserContentId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">ユーザーID</td>
                        <td class="COM_nowrap"><input type="text" name="mucUserId" value="{$form->getMucUserId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic">コンテンツID</td>
                        <td class="COM_nowrap"><input type="text" name="mucContentId" value="{$form->getMucContentId()}" size="10"></td>
                        <td class="COM_nowrap COM_tdWidthBasic"></td>
                        <td class="COM_nowrap"></td>
                    </tr>

                </table>
                <!-- 検索条件テーブル 終了-->

                【連動検索】コンテンツID：
                <input type="text" name="contentAllId" value="{$form->getContentAllId()}" size="10" maxlength="10">
                　　
                連動検索すると上記の条件は<span onClick="javascript:document.getElementById('updateSpan').className=''">消えます</span>。

                <br>

                <!-- // 検索ボタン -->
                {currentTabButton name="　　　検索　　　" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/management/search-table-data" tabId=$tabId formId="searchForm"|cat:$tabId}　　　　　　
                <!-- // リセットボタン -->
                <input type="button" onclick="clearText{$tabId}();" value="入力リセット">

                {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                　　　　　
                <span id="updateSpan" class="COM_displayNone"><input type="checkbox" name="updateFlag" value="1" {if $form->getUpdateFlag() == 1}checked{/if}>更新あり</span>
                {/if}


            </form>
            <!-- // 検索フォーム終了 -->

            <hr>

            {$columnName = $form->getColumnName()}
            {$statusName = $form->getStatusName()}

            {if $form->getUpdateFlag() == 1}
                {currentTabButton name="変更箇所一括更新" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/management/update-table-data"  tabId=$tabId formId="updateForm"|cat:$tabId}　
                {newTabButton name="変更箇所一括更新　(別タブで結果表示)" url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/management/update-table-data"  formId="updateForm"|cat:$tabId}
                <br>
                {include file=$smarty.const.APPLICATION_PATH|cat:'/product/views/scripts/management/searchUpdate.tpl'}
            {else}
                {include file=$smarty.const.APPLICATION_PATH|cat:'/product/views/scripts/management/searchResult.tpl'}
            {/if}
        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 入力内容消去
    //-----------------------------------------------
    function clearText{$tabId}(){

        for(i=0;i < document.searchForm{$tabId}.elements.length;i++){

            if ( document.searchForm{$tabId}.elements[i].type == 'text'){
                document.searchForm{$tabId}.elements[i].value = "";
            }
        }
    }

    //-----------------------------------------------
    // 表示切替
    //-----------------------------------------------
    function PDT_searchResultDisplayChg{$tabId}(no){

        if (document.getElementById('td' + no)) {
            var className = document.getElementById('td' + no).className;
            if (className == 'COM_top COM_displayNone') {
                document.getElementById('td' + no).className = 'COM_top';
                document.getElementById('chgButton' + no).value = '表示OFF';
            } else {
                document.getElementById('td' + no).className = 'COM_top COM_displayNone';
                document.getElementById('chgButton' + no).value = '表示ON';
            }
        }
    }

</script>
<!-- // カレントJavascript終了 -->