<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Order_Work
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Order_Work extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'MemberID' => null,
        'OrderID' => null,
        'CardNo' => null,
        'Expire' => null,
        'ShopID' => null,
        'Amount' => null,
        'AccessID' => null,
        'AccessPass' => null,
        'Method' => null,
        'PayTimes' => null,
        'cancelInputPopo' => null,
        'settlementOutputPopo' => null,
        'inputProductInfo' => null
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * MemberID
	 *
     * @return  MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMemberID() {
		return $this->_properties['MemberID'];
    }


    /**
     * MemberID
	 *
	 * @param  $MemberID MemberID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMemberID($MemberID) {
		$this->_properties['MemberID'] = $MemberID;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * Expire
	 *
     * @return  Expire
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getExpire() {
		return $this->_properties['Expire'];
    }


    /**
     * Expire
	 *
	 * @param  $Expire Expire
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setExpire($Expire) {
		$this->_properties['Expire'] = $Expire;
    }


    /**
     * ShopID
	 *
     * @return  ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getShopID() {
		return $this->_properties['ShopID'];
    }


    /**
     * ShopID
	 *
	 * @param  $ShopID ShopID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setShopID($ShopID) {
		$this->_properties['ShopID'] = $ShopID;
    }


    /**
     * Amount
	 *
     * @return  Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getAmount() {
		return $this->_properties['Amount'];
    }


    /**
     * Amount
	 *
	 * @param  $Amount Amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setAmount($Amount) {
		$this->_properties['Amount'] = $Amount;
    }


    /**
     * AccessID
	 *
     * @return  AccessID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getAccessID() {
		return $this->_properties['AccessID'];
    }


    /**
     * AccessID
	 *
	 * @param  $AccessID AccessID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setAccessID($AccessID) {
		$this->_properties['AccessID'] = $AccessID;
    }


    /**
     * AccessPass
	 *
     * @return  AccessPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getAccessPass() {
		return $this->_properties['AccessPass'];
    }


    /**
     * AccessPass
	 *
	 * @param  $AccessPass AccessPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setAccessPass($AccessPass) {
		$this->_properties['AccessPass'] = $AccessPass;
    }


    /**
     * Method
	 *
     * @return  Method
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMethod() {
		return $this->_properties['Method'];
    }


    /**
     * Method
	 *
	 * @param  $Method Method
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMethod($Method) {
		$this->_properties['Method'] = $Method;
    }


    /**
     * PayTimes
	 *
     * @return  PayTimes
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getPayTimes() {
		return $this->_properties['PayTimes'];
    }


    /**
     * PayTimes
	 *
	 * @param  $PayTimes PayTimes
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setPayTimes($PayTimes) {
		$this->_properties['PayTimes'] = $PayTimes;
    }


    /**
     * cancelInputPopo
	 *
     * @return Mikoshiva_Settlement_Popo_Input_Gmo_Alter cancelInputPopo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getCancelInputPopo() {
		return $this->_properties['cancelInputPopo'];
    }


    /**
     * cancelInputPopo
	 *
	 * @param Mikoshiva_Settlement_Popo_Input_Gmo_Alter $cancelInputPopo cancelInputPopo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setCancelInputPopo(Mikoshiva_Settlement_Popo_Input_Gmo_Alter $cancelInputPopo) {
		$this->_properties['cancelInputPopo'] = $cancelInputPopo;
    }


    /**
     * settlementOutputPopoList
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Order_Settlement_Result_List cancelInputPopo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getSettlementOutputPopoList() {
		return $this->_properties['settlementOutputPopoList'];
    }


    /**
     * settlementOutputPopoList
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Order_Settlement_Result_List $cancelInputPopo cancelInputPopo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setSettlementOutputPopoList(Mikoshiva_Settlement_Popo_Output_Order_Settlement_Result_List $cancelInputPopoList) {
		$this->_properties['settlementOutputPopoList'] = $cancelInputPopoList;
    }


    /**
     * inputProductInfo
	 *
     * @return Mikoshiva_Settlement_Popo_Input_Order_ProductInfo inputProductInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getInputProductInfo() {
		return $this->_properties['inputProductInfo'];
    }


    /**
     * inputProductInfo
	 *
	 * @param Mikoshiva_Settlement_Popo_Input_Order_ProductInfo $inputProductInfo inputProductInfo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Work.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setInputProductInfo(Mikoshiva_Settlement_Popo_Input_Order_ProductInfo $inputProductInfo) {
		$this->_properties['inputProductInfo'] = $inputProductInfo;
    }


}

























