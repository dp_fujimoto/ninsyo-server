<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/View/Helper/Exception.php';


 /**
 * 現在開いている全てのタブを閉じるjavascriptを返します。
 *
 * @param  array   $params  現在未使用
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.closeTabAll.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_closeTabAll($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------

    Mikoshiva_Logger::info(LOG_END);
    return 'KO_closeTabAll();';
}


?>
