<?php
class StackTrace {
    /**
     *    スタックトレースを全部繋げて文字列にする
     *    @param    string    $delim    スタックトレース間の区切り文字。デフォルトは改行
     *    @return   string    文字列にしたスタックトレース
     */
    public static function toString($m,$delim = "\n") {
        $arr = array();
        foreach ($m as $trace) {
            $args = array();
            foreach ($trace['args'] as $arg) {
                $args[] = StackTrace::_var2str($arg);
            }
 
            $line = $trace['class'] . $trace['type'] . $trace['function'];
            $line .= '(' . implode(', ', $args) . ')';
            $line .= ' at ' . $trace['file'] . ' (' . $trace['line'] . ')';
            $arr[] = $line;
        }
        return implode($delim, $arr);
    }
    /**
     *    値を表す文字列にする
     *    @private
     */
    private static function _var2str($var) {
        $type = gettype($var);
        switch ($type) {
            case 'boolean':
                return $var ? 'true' : 'false';
            case 'integer':
            case 'double':
                return $var;
            case 'string':
                return "'$var'";
            case 'array':
                $arr = array();
                foreach ($var as $child) {
                    $arr[] = StackTrace::_var2str($child);
                }
                return 'array(' . implode(", ", $arr) . ')';
            case 'object':
                return get_class($var) . ' Object';
            default:
                return $type;
        }
    }
}