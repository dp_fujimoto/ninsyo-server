<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_test_dm_step_shipping
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxTestDmStepShipping extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'ttd';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'dmStepShippingId' => null,
        'divisionId' => null,
        'relativeDmStepShippingId' => null,
        'shippingPackage' => null,
        'companyName' => null,
        'lastName' => null,
        'firstName' => null,
        'lastNameKana' => null,
        'firstNameKana' => null,
        'zip' => null,
        'prefecture' => null,
        'address1' => null,
        'address2' => null,
        'tel' => null,
        'expectedShippingDate' => null,
        'dmShippingStatus' => null,
        'realShippingDate' => null,
        'inquiryNumber' => null,
        'dirtyFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * DMステップ発送ID の値を返します
	 *
     * @return int DMステップ発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmStepShippingId() {
		return $this->_properties['dmStepShippingId'];
    }


    /**
     * DMステップ発送ID の値をセットします
	 *
	 * @param int $dmStepShippingId DMステップ発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmStepShippingId($dmStepShippingId) {
		$this->_properties['dmStepShippingId'] = $dmStepShippingId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 関連DMステップ発送ID の値を返します
	 *
     * @return int 関連DMステップ発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRelativeDmStepShippingId() {
		return $this->_properties['relativeDmStepShippingId'];
    }


    /**
     * 関連DMステップ発送ID の値をセットします
	 *
	 * @param int $relativeDmStepShippingId 関連DMステップ発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRelativeDmStepShippingId($relativeDmStepShippingId) {
		$this->_properties['relativeDmStepShippingId'] = $relativeDmStepShippingId;
    }


    /**
     * 発送パッケージ の値を返します
	 *
     * @return varchar 発送パッケージ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingPackage() {
		return $this->_properties['shippingPackage'];
    }


    /**
     * 発送パッケージ の値をセットします
	 *
	 * @param varchar $shippingPackage 発送パッケージ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingPackage($shippingPackage) {
		$this->_properties['shippingPackage'] = $shippingPackage;
    }


    /**
     * 発送先会社名 の値を返します
	 *
     * @return varchar 発送先会社名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompanyName() {
		return $this->_properties['companyName'];
    }


    /**
     * 発送先会社名 の値をセットします
	 *
	 * @param varchar $companyName 発送先会社名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompanyName($companyName) {
		$this->_properties['companyName'] = $companyName;
    }


    /**
     * 発送先お名前　姓 の値を返します
	 *
     * @return varchar 発送先お名前　姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastName() {
		return $this->_properties['lastName'];
    }


    /**
     * 発送先お名前　姓 の値をセットします
	 *
	 * @param varchar $lastName 発送先お名前　姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastName($lastName) {
		$this->_properties['lastName'] = $lastName;
    }


    /**
     * 発送先お名前　名 の値を返します
	 *
     * @return varchar 発送先お名前　名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstName() {
		return $this->_properties['firstName'];
    }


    /**
     * 発送先お名前　名 の値をセットします
	 *
	 * @param varchar $firstName 発送先お名前　名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstName($firstName) {
		$this->_properties['firstName'] = $firstName;
    }


    /**
     * 発送先お名前（カナ）　姓 の値を返します
	 *
     * @return varchar 発送先お名前（カナ）　姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastNameKana() {
		return $this->_properties['lastNameKana'];
    }


    /**
     * 発送先お名前（カナ）　姓 の値をセットします
	 *
	 * @param varchar $lastNameKana 発送先お名前（カナ）　姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastNameKana($lastNameKana) {
		$this->_properties['lastNameKana'] = $lastNameKana;
    }


    /**
     * 発送先お名前（カナ）　名 の値を返します
	 *
     * @return varchar 発送先お名前（カナ）　名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstNameKana() {
		return $this->_properties['firstNameKana'];
    }


    /**
     * 発送先お名前（カナ）　名 の値をセットします
	 *
	 * @param varchar $firstNameKana 発送先お名前（カナ）　名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstNameKana($firstNameKana) {
		$this->_properties['firstNameKana'] = $firstNameKana;
    }


    /**
     * 発送先郵便番号 の値を返します
	 *
     * @return char 発送先郵便番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZip() {
		return $this->_properties['zip'];
    }


    /**
     * 発送先郵便番号 の値をセットします
	 *
	 * @param char $zip 発送先郵便番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZip($zip) {
		$this->_properties['zip'] = $zip;
    }


    /**
     * 発送先都道府県 の値を返します
	 *
     * @return varchar 発送先都道府県
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefecture() {
		return $this->_properties['prefecture'];
    }


    /**
     * 発送先都道府県 の値をセットします
	 *
	 * @param varchar $prefecture 発送先都道府県
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefecture($prefecture) {
		$this->_properties['prefecture'] = $prefecture;
    }


    /**
     * 発送先住所１ の値を返します
	 *
     * @return varchar 発送先住所１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * 発送先住所１ の値をセットします
	 *
	 * @param varchar $address1 発送先住所１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * 発送先住所２ の値を返します
	 *
     * @return varchar 発送先住所２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * 発送先住所２ の値をセットします
	 *
	 * @param varchar $address2 発送先住所２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


    /**
     * 発送先電話番号 の値を返します
	 *
     * @return varchar 発送先電話番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTel() {
		return $this->_properties['tel'];
    }


    /**
     * 発送先電話番号 の値をセットします
	 *
	 * @param varchar $tel 発送先電話番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTel($tel) {
		$this->_properties['tel'] = $tel;
    }


    /**
     * 発送予定日 の値を返します
	 *
     * @return date 発送予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExpectedShippingDate() {
		return $this->_properties['expectedShippingDate'];
    }


    /**
     * 発送予定日 の値をセットします
	 *
	 * @param date $expectedShippingDate 発送予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExpectedShippingDate($expectedShippingDate) {
		$this->_properties['expectedShippingDate'] = $expectedShippingDate;
    }


    /**
     * DM発送ステータス の値を返します
	 *
     * @return varchar DM発送ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmShippingStatus() {
		return $this->_properties['dmShippingStatus'];
    }


    /**
     * DM発送ステータス の値をセットします
	 *
	 * @param varchar $dmShippingStatus DM発送ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmShippingStatus($dmShippingStatus) {
		$this->_properties['dmShippingStatus'] = $dmShippingStatus;
    }


    /**
     * 実発送日 の値を返します
	 *
     * @return date 実発送日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealShippingDate() {
		return $this->_properties['realShippingDate'];
    }


    /**
     * 実発送日 の値をセットします
	 *
	 * @param date $realShippingDate 実発送日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealShippingDate($realShippingDate) {
		$this->_properties['realShippingDate'] = $realShippingDate;
    }


    /**
     * 問合せ番号 の値を返します
	 *
     * @return varchar 問合せ番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryNumber() {
		return $this->_properties['inquiryNumber'];
    }


    /**
     * 問合せ番号 の値をセットします
	 *
	 * @param varchar $inquiryNumber 問合せ番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryNumber($inquiryNumber) {
		$this->_properties['inquiryNumber'] = $inquiryNumber;
    }


    /**
     * ダーティーフラグ の値を返します
	 *
     * @return char ダーティーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirtyFlag() {
		return $this->_properties['dirtyFlag'];
    }


    /**
     * ダーティーフラグ の値をセットします
	 *
	 * @param char $dirtyFlag ダーティーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirtyFlag($dirtyFlag) {
		$this->_properties['dirtyFlag'] = $dirtyFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























