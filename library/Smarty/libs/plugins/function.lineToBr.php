<?php
require_once 'Mikoshiva/Logger.php';


 /**
 * 改行コードを改行タグに変換します
 *
 * @param  array   $params
 *                       string str       変換対象の文字列
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.lineToBr.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_lineToBr($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default        = array();
    $default['str'] = '';
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $str = $params['str'];
    $str = str_replace("\r\n", '<br>', $str);
    $str = str_replace("\r", '<br>', $str);
    $str = str_replace("\n", '<br>', $str);


    Mikoshiva_Logger::info(LOG_END);
    return $str;

}


?>
