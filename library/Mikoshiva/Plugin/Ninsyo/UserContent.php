<?php
require_once 'Mikoshiva/Plugin/Ninsyo/Abstruct.php';

/**
 * ワードプレスアカウント関連のAPIを呼び出します。
 *
 * @author k.fujimoto
 * @since version - 2012/09/07
 */
class Mikoshiva_Plugin_Ninsyo_UserContent extends Mikoshiva_Plugin_Ninsyo_Abstruct {

    public function insertUserContent($customerId, $productCode) {

        // パラメータセット
        $this->client->setParameterPost('customerId', $customerId);
        // $this->client->setParameterPost('productCode', $productCode);
        $this->client->setParameterPost('exeType', '0');

        try{
            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {
            // エラー処理
            $this->error($e);
            return null;
        }

        return true;
    }
}