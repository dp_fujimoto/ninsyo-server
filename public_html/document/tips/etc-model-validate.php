<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Date - Model の中でバリデート処理を行いたい</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1>Model の中でバリデート処理を行いたい</h1>



            <!-- ========================================
              ■サンプル
            ==========================================-->
            <h2>サンプル</h2>

            <p>
              Model の中でバリデート処理を行う場合、検証定義は必ず使用する Model の中で定義しなければなりません。<br>
              どこか別の場所に定義することは許されません。
            </p>

            <pre class="wiki">
<?php
highlight_string('<?php
require_once \'Mikoshiva/Controller/Model/Db/Abstract.php\';
require_once \'Mikoshiva/Validate/Auto/Checker.php\';

/**
 * Model の中でバリデートサンプル
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/04/13
 * @version SVN:$Id: etc-model-validate.php,v 1.1 2012/09/21 07:08:06 fujimoto Exp $
 */
class Test1_Models_Test2_Test3Model extends Mikoshiva_Controller_Model_Db_Abstract {


    /**
     * 検証定義
     * @var array 検証定義
     */
    protected $_validationDefinitiontList = array(

        // 姓 --------------------------------------------------
        \'lastName\' => array(
            0            => array (\'StringLength\', 1, 20),
            \'allowEmpty\' => false,
        ),

        // 名 --------------------------------------------------
        \'firstName\' => array(
            0            => array (\'StringLength\', 1, 20),
            \'allowEmpty\' => false,
        ),

        // 姓カナ ------------------------------------------------
        \'lastNameKana\' => array(
            0          => array (\'StringLength\', 1, 20),
            1          => \'Kana\',
            \'allowEmpty\' => false,
        ),

        // 名カナ ------------------------------------------------
        \'firstNameKana\' => array(
            0            => array (\'StringLength\', 1, 20),
            1          => \'Kana\',
            \'allowEmpty\' => false,
        ),

        // メールアドレス --------------------------------------------
        \'email\' => array(
            0            => \'Email\',
            \'allowEmpty\' => false,
        ),

        // 郵便番号 -----------------------------------------------
        \'zip1\' => array(
            0            => array (\'Zip\', \'zip2\'),
            \'allowEmpty\' => false,
        ),
    );


    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_ActionForm $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $contex
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $contex) {

    // バリデータクラスのインスタンスを生成
    $v = null;
    $v = new Mikoshiva_Validate_Auto_Checker();

    // 検証対象となる値を配列でセット（ここでは ActionForm のリクエスト値をセットしています）
    $v->setValidationTargetData($actionForm->toArrayProperty());

    // 検証定義をセット（ここでは上に記述しているメンバ変数をセットしています）
    $v->setValidationDefinitiontList($this->_validationDefinitiontList);

    // 検証実行。返却値は成功で有れば空の配列。失敗であればメッセージを値に持つ配列です。
    $message = array();
    $message = $v->check();




    return null;
}
');
?>
            </pre>

            <!-- ========================================
              ■結果
            ==========================================-->
            <h2>結果</h2>
            <pre class="wiki">
<?php
highlight_string('<?php
array(6) {
  [0] => string(55) "【姓】は 20 文字以内で入力して下さい。"
  [1] => string(55) "【名】は 20 文字以内で入力して下さい。"
  [2] => string(67) "【姓（カナ）】は 20 文字以内で入力して下さい。"
  [3] => string(67) "【名（カナ）】は 20 文字以内で入力して下さい。"
  [4] => string(55) "【メールアドレス】 の入力は必須です。"
  [5] => string(59) "入力された 郵便番号（左） は存在しません"
}
');
?>
            </pre>




  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















