<?php
require_once 'Mikoshiva/Controller/Model/Class/Db/Abstract.php';

/**
 * セッションキーをチェックします。
 *
 * @since 2012/06/25
 * @author fujimoto
 */
class Api_Models_Classes_CheckSessionId
    extends Mikoshiva_Controller_Model_Class_Db_Abstract {

    /**
     * execute
     *
     */
    public function execute($sessionId) {
        logInfo('(' . __LINE__ . ')', '開始');

        $result = false;

        if ($handle = opendir(SESSION_DIR)) {

            /* ディレクトリをループする際の正しい方法です */
            while (false !== ($fileName = readdir($handle))) {

                if ($fileName === 'sess_' . $sessionId) {
                    $data = file_get_contents(SESSION_DIR . '/' . $fileName);
                    $dataArr = explode(';',$data);
                    if (!isBlankOrNull($data)) {
                        foreach ($dataArr as $k => $v) {
                            $timestamp = str_replace('i:','',$v);
                            if (is_numeric($timestamp) && strlen($timestamp) === 10) {
                                if (strtotime(Mikoshiva_date::mikoshivaNow()) < $timestamp) {
                                    $result = true;
                                    break;
                                }
                            }
                        }
                    } else {
                        if (isset($_SESSION['__ZF']["staff"]["ENT"])) {
                            $timestamp = $_SESSION['__ZF']["staff"]["ENT"];
                            if (is_numeric($timestamp) && strlen($timestamp) === 10) {
                                if (strtotime(Mikoshiva_date::mikoshivaNow()) < $timestamp) {
                                    $result = true;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
            }
            closedir($handle);
        }

        if ($result !== true) {
            // return 値の変更は処理の負担になる
            die;
        }

        logInfo('(' . __LINE__ . ')', '終了');
        return $result;
    }
}



