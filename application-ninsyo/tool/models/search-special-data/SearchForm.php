<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since 2011/05/27
 * @version        SVN: $Id:
 *
 */
class Tool_Models_SearchSpecialData_SearchForm
    extends Mikoshiva_Controller_Mapping_Form_Abstract {




    /**
     * アクションフォームの初期化を行います。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');
        parent::reset($config, $request);
        logInfo('(' . __LINE__ . ')', '終了');
    }

    /**
     * バリデートを行います。エラーMSGをリターンでかえす
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        //
        // 基本バリデートチェック
        //
        //-----------------------------------------------
        $message = array();
        $message = parent::validate($config, $request);



        //-----------------------------------------------
        //
        // 独自バリデートチェック
        //
        //-----------------------------------------------

        //-----------------------------------------------
        // エラーメッセージを返す
        //-----------------------------------------------
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $message;
    }

}
