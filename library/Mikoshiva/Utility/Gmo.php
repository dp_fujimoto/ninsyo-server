<?php
/**
 * @package Mikoshiva_Utility
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Exception.php';
require_once 'Mikoshiva/Date.php';

/**
 * GMOユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/25
 * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Utility_Gmo {

    /**
     * オーダーIDから、再請求IDを作成して返します。
     *
     * 例）
     *    引数："DP-1234567891-123-123456-01"
     *    戻値："DP-1234567891-123-123456-02"
     *
     * オーダーIDの末尾は必ず2桁の数字、かつ、オーダーIDは一意であるため、末尾が99のものは許容しません。
     *
     * @param  string $orderId
     * @return string $rechargeOrderId
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/07/05
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getRechargeOrderId($orderId) {
        $frontOrderId = substr($orderId, 0, 25);
        $rearOrderId = intval(substr($orderId, -2));

        if($rearOrderId === 99) {
            throw new Mikoshiva_Utility_Exception("【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】与えられたオーダーID: {$orderId} は、これ以上カウントアップできません。");
        }

        $rearOrderId = str_pad(strval($rearOrderId + 1), 2, "0", STR_PAD_LEFT);
        $rechargeOrderId = $frontOrderId.$rearOrderId;

        return $rechargeOrderId;
    }

    /**
     * オーダー ID から 会員 ID を取り出します
     *
     * @param string $orderId
     * @return string
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/05/31
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getFromOrderIdToMemberId($orderId) {


        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");



        $ret       = null;
        $matchList = array();
        if (preg_match('#^(' . ORDER_ID_PREFIX . '-.{10}-.{3})-.{6}-.{2}$#', $orderId, $matchList) !== 1) {

            // ▼形式が不正であれば例外を投げる DP-0000091270-004-000000-00
            include_once 'Mikoshiva/Utility/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】オーダー ID の形式が不正です。";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }


        // 会員 ID を取り出す
        $memberId = '';
        $memberId = $matchList[1];


        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");


        return $memberId;
    }



    /**
     * カスタマーIDと購入回数を元に、会員IDを作成して返します。
     *
     * @param  integer $customerId       顧客ID
     * @param  integer $purchaseFrequency 購入回数
     * @return string  $memberId          会員ID
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/22
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function createGmoMemberId($customerId, $purchaseFrequency) {


        // ログ開始
        logInfo(LOG_START);


        //--------------------------------------------
        // ■入力値チェック
        //--------------------------------------------
/*
        if (!numberCheck($customerId)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】第一引数の値が数値ではありません";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }

        if (!numberCheck($purchaseFrequency)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】第二引数の値が数値ではありません";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }
*/

        //--------------------------------------------
        //■メンバー作成
        //  形式：DP-xxxxxxxxxx-yyy
        //           xxx：顧客ID(10桁、余りは0埋め)、yyy：購入回数(3ケタ、余りは0埋め)
        //--------------------------------------------

        $data = array();
        $data[] = ORDER_ID_PREFIX;

        $data[] = str_pad($customerId, 10, "0", STR_PAD_LEFT);
        //$data[] = sprintf("%010d", $customerId);
        $data[] = sprintf("%03d", $purchaseFrequency);

        $memberId = '';
        $memberId = implode('-', $data);


        // ログ終了
        logInfo(LOG_END);



        return $memberId;
    }


    /**
     * カスタマーIDと購入回数と課金日までの日数を元に、会員IDを作成して返します。
     *
     * @param  integer $customerId      顧客ID
     * @param  integer $purchaseCount   購入回数
     * @param  integer $nexPaymentDays  現在日から次の支払日までの日数
     * @return string  $SettlementCount 決済回数
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/22
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function createGmoOrderId($customerId, $purchaseCount, $freePeriodType = null, $freePeriod = 0, $settlementCount = 0) {


        // ログ開始
        logInfo(LOG_START);


        //--------------------------------------------
        // ■入力値チェック
        //--------------------------------------------
/*
        if (! numberCheck($customerId)) {
            logInfo(LOG_END);
            throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Gmo#makeGmoOrderId】第一引数の値が数値ではありません');
        }
        if (! numberCheck($purchaseCount)) {
            logInfo(LOG_END);
            throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Gmo#makeGmoOrderId】第二引数の値が数値ではありません');
        }
        if (! (numberCheck($nexPaymentDays) || $nexPaymentDays === null)) {
            logInfo(LOG_END);
            throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Gmo#makeGmoOrderId】第三引数の値が数値ではありません');
        }
*/

        //--------------------------------------------
        //■オーダーID作成
        //  形式：会員ID-xxxxxx-00
        //           xxx：西暦年+月(6桁、yyyyMM形式)
        //--------------------------------------------
        $idData   = array();
        $idData[] = Mikoshiva_Utility_Gmo::createGmoMemberId($customerId, $purchaseCount);


        // 会員 ID より右側を生成
        switch (true) {

            // 新規オーダー ID の生成 ------------------------------------

            // 年 ------------------------------------
            case (preg_match('#YEAR$#', $freePeriodType) === 1):
                $date = new Mikoshiva_Date();
                $date->addYear($freePeriod);
                $idData[] = $date->toString('yyyyMM');
                $idData[] = str_pad($settlementCount, 2, '0', STR_PAD_LEFT);  // 左 0 埋め
                break;

            // 月 ------------------------------------
            case (preg_match('#MONTH$#', $freePeriodType) === 1):
                $date = new Mikoshiva_Date();
                $date->addMonth($freePeriod);
                $idData[] = $date->toString('yyyyMM');
                $idData[] = str_pad($settlementCount, 2, '0', STR_PAD_LEFT);  // 左 0 埋め
                break;

            // 日 ------------------------------------
            case (preg_match('#DAY$#', $freePeriodType) === 1):
                $date = new Mikoshiva_Date();
                $date->addDay($freePeriod);
                $idData[] = $date->toString('yyyyMM');
                $idData[] = str_pad($settlementCount, 2, '0', STR_PAD_LEFT);  // 左 0 埋め
                break;

            // デフォルト
            default:
            case ($freePeriodType === null):
                $idData[] = '000000';
                $idData[] = '00';
                break;
        }


        // 結合
        $orderId = implode('-', $idData);


        // ログ終了;
        logInfo(LOG_END);
        return $orderId;
    }


    /**
     * GMOのコンフィグを返します。
     * テスト用ショップと本番用ショップのIDが被った場合はテスト用のものを優先します。
     *
     * @return array
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/22
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getShopInfo($shopId) {


        // ---------------------- ▼ログ開始▼ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】開始");


        // ▼ショップID のチェック
        if(isBlankOrNull($shopId)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ショップ ID が空です。";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }

        // ▼GMOのマッピングデータを取得
        $GMO_CONFIG = array();
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG');

        // ▼ショップ ID からショップ情報を抜き出してリターン
        if(isset($GMO_CONFIG['TEST']['SHOP'][$shopId])) {

            // ---------------------- ▲ログ終了▲ ---------------------- //
            logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
            return $GMO_CONFIG['TEST']['SHOP'][$shopId];
        }


        if(isset($GMO_CONFIG['REAL']['SHOP'][$shopId])) {

            // ---------------------- ▲ログ終了▲ ---------------------- //
            logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
            return $GMO_CONFIG['REAL']['SHOP'][$shopId];
        }



        $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】該当するショップがありません";
        logDebug($message);


        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
        throw new Mikoshiva_Utility_Exception($message);
    }



    /**
     * テストショップIDのリストを返却します
     *
     * @return array
     */
    public static function getTestShopIdList() {

        // 設定情報を取得
        $GMO_CONFIG = array();
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG');

        // ▼ショップ ID からショップ情報を抜き出してリターン
        return array_keys($GMO_CONFIG['TEST']['SHOP']);
    }



    /**
     * テストクレジットカードであるかをチェックします
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/07
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function isTestCreditCard($cardNo) {

        // ▼テストクレジットカードリストを取得
        $testCardList = array();
        $testCardList = Zend_Registry::get('GMO_TEST_CARD_LIST');

        // ▼カード番号が有れば true
        if (array_key_exists($cardNo, $testCardList)) {
            return true;
        }
        return false;
    }



    /**
     * GMO のエラーコードを返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/10
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     * @param $error_code
     * @return array
     */
    public static function getErrorMessage($errorCodeDetail) {

        include_once 'Mikoshiva/Db/Ar/MstGmoErrorCode.php';
        $gec    = new Mikoshiva_Db_Ar_MstGmoErrorCode();
        $select = $gec->select()->where('mge_error_code_detail = ?', $errorCodeDetail);

        $row = $gec->fetchRow($select);

        if ($row === null) return '';

        $arr = array();
        $arr = $row->toArray();

        return $arr['mge_error_message'];
    }


    /**
     * デビットカードであるかをチェックします
     *
     * <test>
     *     $DEBIT_CARD_LIST = array(
     *         '415', // false
     *         '421', // false
     *         '421', // false
     *         '454', // false
     *         '454', // false
     *         '473', // false
     *         '540', // false
     *         '517', // false
     *         '461', // false
     *
     *         '4153141234567890', // true
     *         '4216141234567890', // true
     *         '4216151234567890', // true
     *         '4542021234567890', // true
     *         '4542031234567890', // true
     *         '4730631234567890', // true
     *         '5409971234567890', // true
     *         '5178951234567890', // true
     *         '4619351234567890', // true
     *      );
     * </test>
     *
     * @param string $cardNo カード番号
     * @return boolean true:デビットカード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/26
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function isDebitCard($cardNo) {


        // デビットカードリストを取得
        include_once 'Zend/Registry.php';
        $DEBIT_CARD_LIST = Zend_Registry::get('DEBIT_CARD_LIST');

        // 検証
        $ret = false;
        foreach ($DEBIT_CARD_LIST as $debitCard) {

            // 検証にマッチしたら false にして break
            if (preg_match('#^' . $debitCard . '#', $cardNo) === 1) {
                $ret = true;
                break;
            }
        }

        return $ret;
    }

    /**
     * ショップID一覧を配列で返します。
     *
     * @return array
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2010/04/09
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getShopIdList() {

        // GMO_CONFIGデータを取得します。
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG');

        // ショップIDを配列で返します。
        return array_keys($GMO_CONFIG[GMO_FLAG]['SHOP']);
    }


    /**
     * ショップ名を返します
     *
     *
     * @param string $shopId ショップID
     */
    public static function getShopName($shopId) {
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG');
        return $GMO_CONFIG['REAL']['SHOP'][$shopId]['NAME'];
    }

}









































