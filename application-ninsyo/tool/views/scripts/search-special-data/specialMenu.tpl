<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">特殊メニュー</h2>

            <table>

            {if (int)$smarty.session.staff.mstStaff->getStaffId() == 4}
                <tr>
                    <td class="COM_top" style="padding-right:30px">
                        <span class="COM_bold COM_fontBlue">よく使う</span>：<br>
                        <table>
                            <tr><td>{newTabLink url="/tool/search-special-data/search-table-data" name="テーブルデータ検索"}</td></tr>
                        </table>
                    </td>
                    <td class="COM_top" style="padding-right:30px">
                        <span class="COM_bold COM_fontBlue">試作品</span>：<br>
                        <table>
                            <tr><td>{newTabLink url="/tool/search-special-data/execute-select-sql" name=" select SQL 実行"}</td></tr>
                        </table>
                    </td>
                    <td class="COM_top" style="padding-right:30px">
                        <span class="COM_bold COM_fontBlue">注意試作品</span>：<br>
                        <table>
                            <tr><td>{newTabLink url=$smarty.const.SUB_FOLDER_PATH|cat:"/tool/batch-test/input" name=" バッチテスト実行"}</td></tr>
                        </table>
                    </td>
                    <td class="COM_top" style="padding-right:30px">
                        <span class="COM_bold COM_fontBlue">未完成品</span>：<br>
                        <table>
                        </table>
                    </td>
                </tr>

            {elseif (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
                <tr>
                    <td class="COM_top" style="padding-right:30px">
                        <table>
                        </table>
                    </td>
                </tr>
            {/if}
            </table>
        </td>
        <!-- // ペイン終了 -->
    </tr>
</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->