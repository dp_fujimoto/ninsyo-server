{$tableList = $form->getTableList()}
{$statusList = $form->getStatusList()}
{$updateColumnNameArr = $form->getUpdateColumnNameArr()}

<table>
    <tr>
        {foreach from=$tableList key="prefix" item="tableData"}
        {if isset($search[$prefix]) && count($search[$prefix]) > 0 && isset($tableData['name'])}
            <td class="COM_top" id="td{$tableData['no']}">
                {$tableData['name']} <span class="COM_bold COM_fontBlue">{$search[$prefix]|count}</span>件ヒット<br>
                <table class="COM_table">
                    {foreach from=$search[$prefix][0] key="k" item="v"}
                        <tr>
                        {foreach from=$search[$prefix] key="k2" item="v2"}
                            {if $k2 == 0}
                                <td class="COM_bgColorLight COM_center COM_nowrap">
                                    {if isset($columnName[$k])}<span class="COM_bold">{$columnName[$k]}</span><br>{/if}
                                    <span class="COM_fontGrey">{$k}</span>
                                </td>
                            {/if}

                            {$class = 'COM_left COM_nowrap'}
                            <td class="{$class}">
                                {if stristr($k,'_memo') || strstr($k,'_body')}
                                    <pre>{$v2[$k]}</pre>
                                {elseif stristr($k,'_header') || strstr($k,'_footer')}
                                    <pre>{$v2[$k]}</pre>
                                {else}
                                    {$v2[$k]}
                                {/if}
                                {if isset($statusName[$v2[$k]])}
                                    <br><span class="COM_fontGrey">{$statusName[$v2[$k]]}</span>
                                {/if}
                            </td>
                        {/foreach}
                        </tr>
                    {/foreach}
                </table>
            </td>
        {/if}
        {/foreach}
    </tr>
</table>

<form id="updateForm{$tabId}" method="post" onSubmit="return false;">
    {foreach from=$tableList key="prefix" item="tableData"}
        {foreach from=$tableData key="k" item="v"}
            <input type="hidden" name="tableList[{$prefix}][{$k}]" value="{$v}">
        {/foreach}
    {/foreach}
</form>