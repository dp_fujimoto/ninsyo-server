<?php
/** @package Mikoshiva_Gmo */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Exec/Card.php';

/**
 * Mikoshiva_Settlement_Execution_Gmo_Exec_Card
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Exec_Card extends Mikoshiva_Settlement_Execution_Gmo_Abstract {




    /**
     * 初期化処理
     *
     * @param Popo_Abstract $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Card */
        $inputPopo;                                         // ジョブコード


        // GMO 結果 POPO クラスをセット
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Card();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['EXEC_PGM'];
        // dumper($this->_gmoConnectioniUrl);
        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }


    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Exec/Mikoshiva_Settlement_Gmo_Exec_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/04
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Exec/Exception.php';


        /* @var $inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Exec_Card */
        $inputPopo;

        // 取引 ID
        if(isBlankOrNull($inputPopo->getAccessID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // 取引パスワード
        if(isBlankOrNull($inputPopo->getAccessPass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引パスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // オーダー ID
        if(isBlankOrNull($inputPopo->getOrderID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】オーダー ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // クレジットカード番号
        if(isBlankOrNull($inputPopo->getCardNo())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】クレジットカード番号は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // クレジットカード有効期限
        if(isBlankOrNull($inputPopo->getExpire())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】クレジットカード有効期限は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);


        return true;
    }
}












