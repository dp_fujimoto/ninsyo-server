#!/bin/bash
#
# MySQL Database Backup Script
#
# create 2012/03/02 kawakami

## define variables
TODAY=$(date +%Y%m%d)
TWO_DAYS_AGO=$(date -d -2day +%Y%m%d)

DB_BKUP_DIR=/bkup/db
DB_NAME_ARRAY=$(/bin/ls -l /var/lib/mysql/data/ | /bin/awk '$1 ~ /d/ {print $9 }')
DB_PASSWORD="66Unu2fL"
DB_MIKOSHIVA_NAME="gusiken"

LOG_DIR=/var/log/script
LOG_FILE=${LOG_DIR}/mnt_bkup_mysql_${TODAY}.log
LOG_ERROR=${LOG_DIR}/mnt_bkup_mysql_${TODAY}.err.log


## function Backup Mikoshiva Database ##
function bkupMikoshivaDatabase() {

  ## check & delete old dump file
  ls ${DB_BKUP_DIR}/${DB_MIKOSHIVA_NAME}_bkup_${TWO_DAYS_AGO}.dump.gz
  if [ $? -eq 0 ]; then
    rm -f ${DB_BKUP_DIR}/${DB_MIKOSHIVA_NAME}_bkup_${TWO_DAYS_AGO}.dump.gz
  fi
  ls ${DB_BKUP_DIR}/${DB_MIKOSHIVA_NAME}_bkup_oplog_${TWO_DAYS_AGO}.dump.gz
  if [ $? -eq 0 ]; then
    rm -f ${DB_BKUP_DIR}/${DB_MIKOSHIVA_NAME}_bkup_oplog_${TWO_DAYS_AGO}.dump.gz
  fi

  ## dump database
  su - mysql -c "mysqldump -u root -p${DB_PASSWORD} --single-transaction --master-data=2 --hex-blob ${DB_MIKOSHIVA_NAME} mst_operation_log | /bin/gzip > ${DB_BKUP_DIR}/${DB_MIKOSHIVA_NAME}_bkup_oplog_${TODAY}.dump.gz"
  sleep 5
  su - mysql -c "mysqldump -u root -p${DB_PASSWORD} --single-transaction --master-data=2 --hex-blob --ignore-table=${DB_MIKOSHIVA_NAME}.mst_operation_log ${DB_MIKOSHIVA_NAME} | /bin/gzip > ${DB_BKUP_DIR}/${DB_MIKOSHIVA_NAME}_bkup_${TODAY}.dump.gz"

  if [ $? -ne 0 ]; then
    return 1
  fi

  return 0
}

## function Backup Other Database ##
function bkupOtherDatabase() {

  ## check & delete old dump file
  ls ${DB_BKUP_DIR}/bkup_${dbName}_${TWO_DAYS_AGO}.dump.gz
  if [ $? -eq 0 ]; then
    rm -f ${DB_BKUP_DIR}/bkup_${dbName}_${TWO_DAYS_AGO}.dump.gz
  fi

  ## dump database
  su - mysql -c "mysqldump -u root -p${DB_PASSWORD} --single-transaction --master-data=2 --hex-blob ${dbName} | /bin/gzip > ${DB_BKUP_DIR}/bkup_${dbName}_${TODAY}.dump.gz"

  if [ $? -ne 0 ]; then
    return 1
  fi

  return 0
}


## MAIN ##
{
  echo [$(date +%F_%T)]:batch start

  ## check & make directory
  ls ${DB_BKUP_DIR}
  if [ $? -ne 0 ]; then
    mkdir ${DB_BKUP_DIR}
  fi

  ## backup
  for dbName in ${DB_NAME_ARRAY[*]};
  do
    ## dump database
    if [ ${dbName} = "${DB_MIKOSHIVA_NAME}" ]; then
      bkupMikoshivaDatabase
    else
      bkupOtherDatabase
    fi

    ## result message
    if [ $? -ne 0 ]; then
      echo [$(date +%F_%T)]:[${dbName} dump failed]
    else
      echo [$(date +%F_%T)]:[${dbName} dump success]
    fi

    sleep 2
  done

  echo [$(date +%F_%T)]:batch succeed

} 1>>${LOG_FILE} 2>>${LOG_ERROR}

