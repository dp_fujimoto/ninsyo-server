<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'CardSeq' => null,
        'CardNo' => null,
        'Forward' => null,
        'ErrCode' => null,
        'ErrInfo' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * CardSeq
	 *
     * @return  CardSeq
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardSeq() {
		return $this->_properties['CardSeq'];
    }


    /**
     * CardSeq
	 *
	 * @param  $CardSeq CardSeq
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardSeq($CardSeq) {
		$this->_properties['CardSeq'] = $CardSeq;
    }


    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * Forward
	 *
     * @return  Forward
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getForward() {
		return $this->_properties['Forward'];
    }


    /**
     * Forward
	 *
	 * @param  $Forward Forward
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setForward($Forward) {
		$this->_properties['Forward'] = $Forward;
    }


    /**
     * ErrCode
	 *
     * @return  ErrCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrCode() {
		return $this->_properties['ErrCode'];
    }


    /**
     * ErrCode
	 *
	 * @param  $ErrCode ErrCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrCode($ErrCode) {
		$this->_properties['ErrCode'] = $ErrCode;
    }


    /**
     * ErrInfo
	 *
     * @return  ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrInfo() {
		return $this->_properties['ErrInfo'];
    }


    /**
     * ErrInfo
	 *
	 * @param  $ErrInfo ErrInfo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrInfo($ErrInfo) {
		$this->_properties['ErrInfo'] = $ErrInfo;
    }


}

























