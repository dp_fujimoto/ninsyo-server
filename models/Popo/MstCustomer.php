<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_customer
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCustomer extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mcu';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'customerId' => null,
        'email' => null,
        'password' => null,
        'lastName' => null,
        'firstName' => null,
        'lastNameKana' => null,
        'firstNameKana' => null,
        'customerType' => null,
        'companyName' => null,
        'zip' => null,
        'prefecture' => null,
        'address1' => null,
        'address2' => null,
        'tel' => null,
        'fax' => null,
        'birthDate' => null,
        'totalPurchaseNumber' => null,
        'point' => null,
        'accountFlag' => null,
        'blackFlag' => null,
        'dmNgFlag' => null,
        'unpaidFlag' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * メールアドレス の値を返します
	 *
     * @return varchar メールアドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEmail() {
		return $this->_properties['email'];
    }


    /**
     * メールアドレス の値をセットします
	 *
	 * @param varchar $email メールアドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEmail($email) {
		$this->_properties['email'] = $email;
    }


    /**
     * パスワード の値を返します
	 *
     * @return varchar パスワード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPassword() {
		return $this->_properties['password'];
    }


    /**
     * パスワード の値をセットします
	 *
	 * @param varchar $password パスワード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPassword($password) {
		$this->_properties['password'] = $password;
    }


    /**
     * 姓 の値を返します
	 *
     * @return varchar 姓
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastName() {
		return $this->_properties['lastName'];
    }


    /**
     * 姓 の値をセットします
	 *
	 * @param varchar $lastName 姓
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastName($lastName) {
		$this->_properties['lastName'] = $lastName;
    }


    /**
     * 名 の値を返します
	 *
     * @return varchar 名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstName() {
		return $this->_properties['firstName'];
    }


    /**
     * 名 の値をセットします
	 *
	 * @param varchar $firstName 名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstName($firstName) {
		$this->_properties['firstName'] = $firstName;
    }


    /**
     * 姓カナ の値を返します
	 *
     * @return varchar 姓カナ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastNameKana() {
		return $this->_properties['lastNameKana'];
    }


    /**
     * 姓カナ の値をセットします
	 *
	 * @param varchar $lastNameKana 姓カナ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastNameKana($lastNameKana) {
		$this->_properties['lastNameKana'] = $lastNameKana;
    }


    /**
     * 名カナ の値を返します
	 *
     * @return varchar 名カナ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstNameKana() {
		return $this->_properties['firstNameKana'];
    }


    /**
     * 名カナ の値をセットします
	 *
	 * @param varchar $firstNameKana 名カナ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstNameKana($firstNameKana) {
		$this->_properties['firstNameKana'] = $firstNameKana;
    }


    /**
     * 顧客タイプ の値を返します
	 *
     * @return varchar 顧客タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerType() {
		return $this->_properties['customerType'];
    }


    /**
     * 顧客タイプ の値をセットします
	 *
	 * @param varchar $customerType 顧客タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerType($customerType) {
		$this->_properties['customerType'] = $customerType;
    }


    /**
     * 会社名 の値を返します
	 *
     * @return varchar 会社名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompanyName() {
		return $this->_properties['companyName'];
    }


    /**
     * 会社名 の値をセットします
	 *
	 * @param varchar $companyName 会社名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompanyName($companyName) {
		$this->_properties['companyName'] = $companyName;
    }


    /**
     * 郵便番号 の値を返します
	 *
     * @return char 郵便番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZip() {
		return $this->_properties['zip'];
    }


    /**
     * 郵便番号 の値をセットします
	 *
	 * @param char $zip 郵便番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZip($zip) {
		$this->_properties['zip'] = $zip;
    }


    /**
     * 都道府県 の値を返します
	 *
     * @return varchar 都道府県
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefecture() {
		return $this->_properties['prefecture'];
    }


    /**
     * 都道府県 の値をセットします
	 *
	 * @param varchar $prefecture 都道府県
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefecture($prefecture) {
		$this->_properties['prefecture'] = $prefecture;
    }


    /**
     * 市区町村番地 の値を返します
	 *
     * @return varchar 市区町村番地
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * 市区町村番地 の値をセットします
	 *
	 * @param varchar $address1 市区町村番地
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * ビル・マンション名 の値を返します
	 *
     * @return varchar ビル・マンション名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * ビル・マンション名 の値をセットします
	 *
	 * @param varchar $address2 ビル・マンション名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


    /**
     * 電話番号 の値を返します
	 *
     * @return varchar 電話番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTel() {
		return $this->_properties['tel'];
    }


    /**
     * 電話番号 の値をセットします
	 *
	 * @param varchar $tel 電話番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTel($tel) {
		$this->_properties['tel'] = $tel;
    }


    /**
     * FAX番号 の値を返します
	 *
     * @return varchar FAX番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFax() {
		return $this->_properties['fax'];
    }


    /**
     * FAX番号 の値をセットします
	 *
	 * @param varchar $fax FAX番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFax($fax) {
		$this->_properties['fax'] = $fax;
    }


    /**
     * 生年月日 の値を返します
	 *
     * @return date 生年月日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBirthDate() {
		return $this->_properties['birthDate'];
    }


    /**
     * 生年月日 の値をセットします
	 *
	 * @param date $birthDate 生年月日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBirthDate($birthDate) {
		$this->_properties['birthDate'] = $birthDate;
    }


    /**
     * 合計購入回数 の値を返します
	 *
     * @return int 合計購入回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTotalPurchaseNumber() {
		return $this->_properties['totalPurchaseNumber'];
    }


    /**
     * 合計購入回数 の値をセットします
	 *
	 * @param int $totalPurchaseNumber 合計購入回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTotalPurchaseNumber($totalPurchaseNumber) {
		$this->_properties['totalPurchaseNumber'] = $totalPurchaseNumber;
    }


    /**
     * ポイント の値を返します
	 *
     * @return int ポイント
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPoint() {
		return $this->_properties['point'];
    }


    /**
     * ポイント の値をセットします
	 *
	 * @param int $point ポイント
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPoint($point) {
		$this->_properties['point'] = $point;
    }


    /**
     * アカウントフラグ の値を返します
	 *
     * @return char アカウントフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccountFlag() {
		return $this->_properties['accountFlag'];
    }


    /**
     * アカウントフラグ の値をセットします
	 *
	 * @param char $accountFlag アカウントフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccountFlag($accountFlag) {
		$this->_properties['accountFlag'] = $accountFlag;
    }


    /**
     * ブラック顧客フラグ の値を返します
	 *
     * @return char ブラック顧客フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBlackFlag() {
		return $this->_properties['blackFlag'];
    }


    /**
     * ブラック顧客フラグ の値をセットします
	 *
	 * @param char $blackFlag ブラック顧客フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBlackFlag($blackFlag) {
		$this->_properties['blackFlag'] = $blackFlag;
    }


    /**
     * mcu_dm_ng_flag の値を返します
	 *
     * @return char mcu_dm_ng_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmNgFlag() {
		return $this->_properties['dmNgFlag'];
    }


    /**
     * mcu_dm_ng_flag の値をセットします
	 *
	 * @param char $dmNgFlag mcu_dm_ng_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmNgFlag($dmNgFlag) {
		$this->_properties['dmNgFlag'] = $dmNgFlag;
    }


    /**
     * 未払い顧客フラグ の値を返します
	 *
     * @return char 未払い顧客フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUnpaidFlag() {
		return $this->_properties['unpaidFlag'];
    }


    /**
     * 未払い顧客フラグ の値をセットします
	 *
	 * @param char $unpaidFlag 未払い顧客フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUnpaidFlag($unpaidFlag) {
		$this->_properties['unpaidFlag'] = $unpaidFlag;
    }


    /**
     * 欠陥フラグ の値を返します
	 *
     * @return char 欠陥フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * 欠陥フラグ の値をセットします
	 *
	 * @param char $imperfectionFlag 欠陥フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























