<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Order
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Order extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'CardNo' => null,
        'Expire' => null,
        'Method' => null,
        'PayTimes' => null,
        'customerId' => null,
        'chargeType' => null,
        'paymentType' => null,
        'productDataList' => null,
        'memberId' => null,
    );



    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }

    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * Expire
	 *
     * @return  Expire
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getExpire() {
		return $this->_properties['Expire'];
    }


    /**
     * Expire
	 *
	 * @param  $Expire Expire
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setExpire($Expire) {
		$this->_properties['Expire'] = $Expire;
    }


    /**
     * Method
	 *
     * @return  Method
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMethod() {
		return $this->_properties['Method'];
    }


    /**
     * Method
	 *
	 * @param  $Method Method
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMethod($Method) {
		$this->_properties['Method'] = $Method;
    }


    /**
     * PayTimes
	 *
     * @return  PayTimes
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getPayTimes() {
		return $this->_properties['PayTimes'];
    }


    /**
     * PayTimes
	 *
	 * @param  $PayTimes PayTimes
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setPayTimes($PayTimes) {
		$this->_properties['PayTimes'] = $PayTimes;
    }


    /**
     * customerId
	 *
     * @return  customerId
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * customerId
	 *
	 * @param  $customerId customerId
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * chargeType
	 *
     * @return string chargeType
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * chargeType
	 *
	 * @param string $chargeType chargeType
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * paymentType
	 *
     * @return string paymentType
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getPaymentType() {
		return $this->_properties['paymentType'];
    }


    /**
     * paymentType
	 *
	 * @param string $paymentType paymentType
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setPaymentType($paymentType) {
		$this->_properties['paymentType'] = $paymentType;
    }


    /**
     * productDataList
	 *
     * @return  productDataList
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getProductDataList() {
		return $this->_properties['productDataList'];
    }


    /**
     * productDataList
	 *
	 * @param  $productDataList productDataList
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setProductDataList($productDataList) {
		$this->_properties['productDataList'] = $productDataList;
    }


    /**
     * memberId
     *
     * @return string memberId
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMemberId() {
        return $this->_properties['memberId'];
    }


    /**
     * memberId
     *
     * @param string $memberId memberId
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMemberId($memberId) {
        $this->_properties['memberId'] = $memberId;
    }


}

























