<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/Operation.php';
require_once 'api/models/user-content/GetEdgeTradeItem.php';
require_once 'api/models/user-content/GetRegistrationDate6.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Content_GetProductContentData extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $productCode = $actionForm->getProductCode();
        $nowDateStrtotime = strtotime(Mikoshiva_Date::mikoshivaNow('yyyy-MM-dd'));

        //-----------------------------------------------
        // コンテンツ条件情報取得
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT *";
        $sql .= " FROM mst_condition";
        $sql .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mcd_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND mct_application_type != ?";
        $sql .= "   AND mcd_product_code = ?";
        $sql .= "   AND mcd_server_name = ?";
        $sql .= "   AND (mcd_product_shipping_code is null OR mcd_product_shipping_code = '' OR mcd_product_shipping_code IN ('" . $actionForm->getProductShippingCode() . "'))";
        $sql .= "   AND (mcd_campaign_code is null OR mcd_campaign_code = '' OR mcd_campaign_code = ?)";
        $sql .= "   AND (mcd_campaign_sales_code is null OR mcd_campaign_sales_code = '' OR mcd_campaign_sales_code = ?)";
        $sql .= "   AND (mcd_subscription_type is null OR mcd_subscription_type = '' OR mcd_subscription_type = ?)";
        $sql .= "   AND (mcd_product_type is null OR mcd_product_type = '' OR mcd_product_type = ?)";
        $sql .= " GROUP BY mct_content_id";
        $sql .= " ORDER BY mct_application_type, mbr_brand_name, mpr_product_name, mct_content_name";

        $params   = array();
        $params[] = 'APPLICATION_TYPE_LIBRARY';
        $params[] = $productCode;
        $params[] = $actionForm->getServerName();
        $params[] = $actionForm->getCampaignCode();
        $params[] = $actionForm->getCampaignSalesCode();
        $params[] = $actionForm->getSubscriptionType();
        $params[] = $actionForm->getProductType();

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $data = array();
        $userData = array();
        while($row = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $rowData = array();

            //-----------------------------------------------
            // 条件情報取得
            //-----------------------------------------------
            // SQL作成
            $sql2 = "";
            $sql2 .= "SELECT *";
            $sql2 .= " FROM mst_condition";
            $sql2 .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
            $sql2 .= " WHERE 1 = 1";
            $sql2 .= "   AND mcd_delete_flag = '0'";
            $sql2 .= "   AND mct_delete_flag = '0'";
            $sql2 .= "   AND mct_content_id = ?";

            $params2   = array();
            $params2[] = $row['mct_content_id'];

            // SQL実行＋全件取得
            $res2 = array();
            $res2 = $this->_db->query($sql2,$params2);

            $count = 1;
            $type = '';
            while($v = $res2->fetch(Zend_Db::FETCH_ASSOC)){

                if (isBlankOrNull($type)) {
                    if ($v['mct_all_condition_flag'] === '1') {
                        $type .= '■全ての条件に該当すること。<br>';
                    } else {
                        $type .= '■どれかの条件に該当すること。<br>';
                    }
                    if ($v['mct_auth_flag'] === '1') {
                        $type .= '（一度でも条件を満たせばログイン可）<br>';
                    } else {
                        $type .= '（条件を満たしている間のみログイン可）<br>';
                    }
                    if ($v['mct_available_check_flag'] === '1') {
                        $type .= '（請求取消分・商品金額以上の返金がある分はサービス対象外）<br>';
                    }
                }

                $str = '';
                $str .= '<tr><td style="background-color:#adff2f;" nowrap>' . '条件' . $count . '</td>';

                $flag = '0';
                if ($v['mct_all_condition_flag'] !== '1') {
                    if ($productCode === $v['mcd_product_code']) {
                        if (!isBlankOrNull($v['mcd_campaign_code'])) {
                            if ($actionForm->getCampaignCode() === $v['mcd_campaign_code']) {
                                $str .= '<td>';
                            } else {
                                $str .= '<td style="color:gray;">';
                                $flag = '1';
                            }
                        } else {
                            $str .= '<td>';
                        }
                    } else {
                        $str .= '<td style="color:gray;">';
                        $flag = '1';
                    }
                }

                if ($flag === '0' && $productCode === $v['mcd_product_code']) {
                    $str .= $v['mcd_server_name'] . 'で、';
                    $str .= '<span style="color:red">' . $v['mcd_product_code'] . '</span>';
                } else {
                    $str .= $v['mcd_server_name'] . 'で、';
                    $str .= $v['mcd_product_code'];
                }
                if (!isBlankOrNull($v['mcd_product_type'])) {
                    $productType = '';
                    switch ($v['mcd_product_type']) {
                        case 'PRODUCT_TYPE_SINGLE': $productType = '単発'; break;
                        case 'PRODUCT_TYPE_MICRO_CONTINUITY': $productType = 'マイクロコンティニュイティ'; break;
                        case 'PRODUCT_TYPE_CONTINUITY': $productType = 'コンティニュイティ'; break;
                    }
                    $str .= $productType;
                }
                if (!isBlankOrNull($v['mcd_subscription_type'])) {
                    $subscriptionType = '';
                    switch ($v['mcd_subscription_type']) {
                        case 'SUBSCRIPTION_TYPE_NORMAL': $subscriptionType = '（通常）'; break;
                        case 'SUBSCRIPTION_TYPE_YEAR': $subscriptionType = '（年間）'; break;
                        case 'SUBSCRIPTION_TYPE_YEAR_CONTINUITY': $subscriptionType = '（年間追加）'; break;
                        case 'SUBSCRIPTION_TYPE_MICRO_CONTINUITY_UPGRADE': $subscriptionType = '（マイクロアップグレード）'; break;
                        case 'VIP': $subscriptionType = '（VIP）'; break;
                    }
                    $str .= $subscriptionType;
                }

                $str .= 'を';

                if (!isBlankOrNull($v['mcd_campaign_sales_code']) || !isBlankOrNull($v['mcd_campaign_code'])) {
                    $str .= '<br>';
                    if (!isBlankOrNull($v['mcd_campaign_sales_code'])) {
                        $str .= '指定キャンペーン：' . $v['mcd_campaign_sales_code'] . '、';
                    }
                    if (!isBlankOrNull($v['mcd_campaign_code'])) {
                        if ($actionForm->getCampaignCode() === $v['mcd_campaign_code']) {
                            $str .= '指定オーダーフォーム：' . '<span style="color:red">' . $v['mcd_campaign_code'] . '</span>、';
                        } else {
                            $str .= '指定オーダーフォーム：' . $v['mcd_campaign_code'] . '、';
                        }
                    }
                    $str .= 'で<br>';
                }

                if (!isBlankOrNull($v['mcd_charge_type'])) {
                    $chargeType = '';
                    switch ($v['mcd_charge_type']) {
                        case 'CHARGE_TYPE_CARD': $chargeType = 'カード'; break;
                        case 'CHARGE_TYPE_CASH_ON_DELIVERY': $chargeType = '代引き'; break;
                        case 'CHARGE_TYPE_BANK_TRANSFER': $chargeType = '銀行振込'; break;
                    }
                    $str .= $chargeType;
                    $str .= 'にて';
                }


                if ($v['mcd_having_flag'] === '1') {
                    // '指定日以前に購入';
                    $str .= $v['mcd_designated_date'] . '以前に購入していること。（解約するまでが会員期間）';
                }
                if ($v['mcd_servise_in_flag'] === '1') {
                    // '指定日にサービスIN';
                    $str .= '購入していて' . $v['mcd_designated_date'] . 'はサービス期間内であったこと。';
                }
                if ($v['mcd_subscription_flag'] === '1') {
                    $str .= '購入し、購読中であること。';
                }
                if ($v['mcd_bundle_flag'] === '1') {
                    $str .= '購入し、一括購読中であること。';
                }
                if ($v['mcd_purchase_flag'] === '1') {
                    $str .= '購入したことがあること。';
                    if ($v['mcd_cancel_ok_flag'] === '1') {
                        $str .= '（解約しても会員とする）';
                    } else {
                        $str .= '（解約日までを会員期間とする）';
                    }
                }

                if (!isBlankOrNull($v['mcd_product_shipping_code']) || !isBlankOrNull($v['mcd_package_number'])) {
                    $str .= '<br>さらに対象商品の発送データに';
                    if (!isBlankOrNull($v['mcd_product_shipping_code'])) {
                        $str .= '「商品発送コード：' . $v['mcd_product_shipping_code'] . '」';
                    }
                    if (!isBlankOrNull($v['mcd_package_number'])) {
                        $str .= '「パッケージナンバー：' . $v['mcd_package_number'] . '」';
                    }
                    $str .= 'が存在すること。';
                }

                if ($v['mcd_purchase_flag'] === '1') {
                    if (!isBlankOrNull($v['mcd_permission_month']) || !isBlankOrNull($v['mcd_permission_day'])) {
                        $str .= '<br>ただし、';
                        $str .= '「会員期間は最大';
                        if ($v['mcd_permission_month'] > 0) {
                            $str .= $v['mcd_permission_month'] . 'ヶ月';
                        }
                        if ($v['mcd_permission_day'] > 0) {
                            $str .= $v['mcd_permission_day'] . '日';
                        }
                        $str .= 'に限定」';
                    }
                    if (!isBlankOrNull($v['mcd_need_period_month']) || !isBlankOrNull($v['mcd_need_period_day'])) {
                        $str .= '<br>ただし、';
                        $str .= '「会員期間が';
                        if ($v['mcd_need_period_month'] > 0) {
                            $str .= $v['mcd_need_period_month'] . 'ヶ月';
                        }
                        if ($v['mcd_need_period_day'] > 0) {
                            $str .= $v['mcd_need_period_day'] . '日';
                        }
                        $str .= '以上に限定」';
                    }
                }

                $str .= '</td></tr>';

                $rowData[$count] = $str;
                $count++;
            }
            $row['content_detail'] = $type . '<br><table>' . implode('',$rowData) . '</table>';
            $row['user'] = '';

            if (!isBlankOrNull($actionForm->getId()) && !isBlankOrNull($actionForm->getPassword())) {

                if ($row['mct_application_type'] === 'APPLICATION_TYPE_WEB_PAGE_AUTH') {

                    if (!isset($userData[$row['mpr_product_code']])) {

                        $actionForm->setProductCode($row['mpr_product_code']);

                        $class = new Api_Models_UserContent_GetRegistrationDate6();
                        $classResult = $class->execute($actionForm,$context);
                        // $min . ',' . $max . ',' . implode('-',$contentData) . ',' . $passwordHash . ',' . implode('-',$blankData);

                        $tempArr = explode(',',$classResult);
                        $contentData2 = array();
                        if (isset($tempArr[2]) && !isBlankOrNull($tempArr[2])) {
                            $contentData = explode('-',$tempArr[2]);
                            foreach ($contentData as $k2 => $v2) {
                                $v2 = explode('|',$v2);
                                if (count($v2) === 3) {
                                    $contentData2[$v2[0]] = array();
                                    $contentData2[$v2[0]]['start'] = $v2[1];
                                    $contentData2[$v2[0]]['end'] = $v2[2];
                                    $contentData2[$v2[0]]['blank'] = array();
                                }
                            }
                            if (isset($tempArr[4]) && !isBlankOrNull($tempArr[4])) {
                                $blankData = explode('-',$tempArr[4]);
                                foreach ($blankData as $k2 => $v2) {
                                    $v2 = explode('|',$v2);
                                    if (count($v2) === 3) {
                                        if (isset($contentData2[$v2[0]])) {
                                            $blankRow = array();
                                            $blankRow['start'] = $v2[1];
                                            $blankRow['end'] = $v2[2];
                                            $contentData2[$v2[0]]['blank'][] = $blankRow;
                                        }
                                    }
                                }
                            }
                            foreach ($contentData2 as $k2 => $v2) {
                                if ($nowDateStrtotime === strtotime($v2['end'])) {
                                    $contentData2[$k2] = '会員期間：' . $v2['start'] . '～の' . ((int)((strtotime($v2['end']) - strtotime($v2['start'])) / 86400) + 1) . '日間';
                                } else {
                                    $contentData2[$k2] = '会員期間：' . $v2['start'] . '～' . $v2['end'] . 'の' . ((int)((strtotime($v2['end']) - strtotime($v2['start'])) / 86400) + 1) . '日間';
                                    if ($v['mct_auth_flag'] === '1') {
                                        $contentData2[$k2] .= '※現在もログイン可、上記の会員期間に応じたページを見れる。';
                                    }
                                }
                                if (count($v2['blank']) > 0) {
                                    $contentData2[$k2] .= '<br><br><br>ただし、以下の期間は除く。<br>';
                                    foreach ($v2['blank'] as $k3 => $v3) {
                                        $contentData2[$k2] .= $v3['start'] . '～' . $v3['end'] . 'の' . ((int)((strtotime($v3['end']) - strtotime($v3['start'])) / 86400) + 1) . '日間<br>';
                                    }
                                }
                            }
                        }

                        $userData[$row['mpr_product_code']] = $contentData2;
                    }
                    $tempData = $userData[$row['mpr_product_code']];

                    if (isset($tempData[$row['mct_content_id']])) {
                        $row['user'] = $tempData[$row['mct_content_id']];
                    } else {
                        $row['user'] = '権限なし';
                    }
                } else if ($row['mct_application_type'] === 'APPLICATION_TYPE_EDGE_TRADER') {
                    $actionForm->setProductCode($row['mpr_product_code']);

                    $class = new Api_Models_UserContent_GetEdgeTradeItem();
                    $classResult = $class->execute($actionForm,$context);

                    if (count($classResult) > 0) {
                        $row['user'] .= '今現在、権限があります。';
                    } else {
                        $row['user'] .= '権限なし。';
                    }
                }
            }

            $data[] = $row;
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return json_encode($data);
    }
}
