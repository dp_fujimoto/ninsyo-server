<?php
require_once 'Mikoshiva/View/Helper/Exception.php';

//function.closeTab.phpが必須
 /**
 * 指定されたIDを持つタブを閉じるjavascript をリンク(aタグ)に実装した形で返します。
 *
 * @param  array   $params
 *                       string name  リンクテキスト
 *                       string tabId 閉じるタブのタブID
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.closeTabLink.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_closeTabLink($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default            = array();
    $default['name']    = '';
    $default['tabId']   = null;
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(isBlankOrNull($data['tabId']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 tabId に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once dirname(__FILE__) . '/function.closeTab.php';

    $script = '';
    $script = smarty_function_closeTab($params, $smarty);

    Mikoshiva_Logger::info(LOG_END);
    return '<a href="#" onclick="' . $script . '">' . $data['name'] . '</a>';
}


?>
