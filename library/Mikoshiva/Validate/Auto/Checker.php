<?php
/** @package Mikoshiva_Validate */

/**
 * require
 */
require_once 'spyc.php';
require_once 'Mikoshiva/Filter/Input.php';
require_once 'Mikoshiva/Validate/Exception.php';
require_once 'Mikoshiva/Validate/Message/Exception.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 検証定義ファイルを元に入力値のチェックを行います
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Auto_Checker {



    //-------------------------------------------
    // ■プロパティ
    //-------------------------------------------
    /**
     * 検証対象データ
     *
     * @var array 検証対象データ
     */
    protected $_validationTargetData = array();


    /**
     * エラーメッセージのプレフィックス
     *
     * @var string
     */
    protected $_prefix;


    /**
     * 検証定義リスト を保持します
     * @var array 検証定義リスト を保持します
     */
    protected $_validationDefinitiontList;


    /**
     * エラー時の表示処理定義
     *
     * @var array
     */
    //protected $_render = array();


    //-----------------------------------------
    // ■検証対象データ
    //-----------------------------------------
    /**
     * 検証対象データをセットします
     *
     * @param array $validationData 検証対象データをセットします
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/14
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setValidationTargetData(array $validationTargetData) {
        $this->_validationTargetData = $validationTargetData;
    }


    /**
     * 検証対象データを返します
     *
     * @return Mikoshiva_Controller_Mapping_Form_Abstract 検証対象データ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/14
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getValidationTargetData() {
        return $this->_validationTargetData;
    }


    //------------------------------------------------
    // ■検証定義
    //------------------------------------------------
    /**
     * 検証定義リスト をセットします
     *
     * @param array $validationDefinitiontList 検証定義リスト
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/12
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setValidationDefinitiontList(array $validationDefinitiontList) {
        logInfo(LOG_START);
        $this->_validationDefinitiontList = $validationDefinitiontList;
        logInfo(LOG_END);
    }


    /**
     * 検証定義リスト を返します
     *
     * @return array $this->_validationDefinitiontList 検証定義リスト
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/12
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getValidationDefinitiontList() {
        logInfo(LOG_START);
        return $this->_validationDefinitiontList;
        logInfo(LOG_END);
    }


    /**
     * エラーメッセージのプレフィックスをセットします
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/06
     * @version
     */
    public function setPrefix($prefix) {
        $this->_prefix = $prefix;
    }


    /**
     * エラーメッセージのプレフィックスを返します
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/06
     * @version
     */
    public function getPrefix() {
        return $this->_prefix;
    }

    /**
     * コンストラクタ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/14
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct() {
        $this->_init();
    }


    /**
     * 初期化を行います
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/14
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _init() {
    }



    /**
     * 検証を実行します
     *
     * @return boolean true：成功|false：失敗
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/15
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function check() {
//dumper($_REQUEST);//exit;
//dumper($_FILES);//exit;
        //----------------------------------------------------------------------
        // ■検証対象データがセットされていなければエラー
        //-----------------------------------------------------------------------
        // プロパティのチェック
        if (count($this->_validationTargetData) === 0) {
            throw new Exception('【Mikoshiva_Validate_Auto_Checker#check】検証対象データがセットされていません');
        }


        //----------------------------------------------------------------------
        // ■検証定義のチェック
        //----------------------------------------------------------------------
        if (count($this->_validationDefinitiontList) === 0) {
            throw new Exception('【Mikoshiva_Validate_Auto_Checker#check】検証定義データがセットされていません');
        }


        //----------------------------------------------------------------------
        //
        // ■アップロードファイルの検証
        //   ファイルセクションが存在すればアップロードファイルの検証を行う
        //
        //----------------------------------------------------------------------
        $fileMessage = array(); // エラーメッセージ格納変数
        if (isset($this->_validationDefinitiontList['file'])) { // {{{ if:1

            // アップロードファイル検証定義を取得
            $fileValidationDefinitiontList = $this->_validationDefinitiontList['file'];
//dumper($fileValidationDefinitiontList);exit;
            // アップロードファイルの検証定義を削除
            // ※リクエストの値では使わないため
            unset($this->_validationDefinitiontList['file']);



            // ◆検証
            foreach ($fileValidationDefinitiontList as $requestName => $fileValidateList) { // {{{ foreach:1

                //---------------------------------------------------------------------------
                // allowEmpty の記述がなければ必須チェックを行わない
                // ※デフォルトは必須チェックを行います
                //---------------------------------------------------------------------------
                $opton = array();
                if (! isset($fileValidateList['allowEmpty'])) {
                    $opton['ignoreNoFile'] = true;
                }


                // インスタンス生成
                include_once 'Mikoshiva/File/Transfer/Http.php';
                $file = new Mikoshiva_File_Transfer_Adapter_Http($opton);
                $file->addPrefixPath('Mikoshiva_Validate_File', 'Mikoshiva/Validate/File', Zend_File_Transfer_Adapter_Http::VALIDATE);
//dumper($file->getFileInfo());exit;
                //---------------------------------------------------------------------------
                // 1項目の検証定義をセット（追加）する
                //---------------------------------------------------------------------------
                foreach ($fileValidateList as $validateIndex => $validateContent) { // {{{ foreach:2


                    // $validateIndex が数字でなければコンテニュー
                    if (! numberCheck($validateIndex)) {continue;}



                    // 初期化
                    $_validator           = null;
                    $_breakChainOnFailure = false;
                    $_options             = null;
                    $_files               = null;

                    $_validator                                           = $validateContent[0]; // 必須
                    if (isset($validateContent[1])) $_breakChainOnFailure = $validateContent[1];
                    if (isset($validateContent[2])) $_options             = $validateContent[2];
                    if (isset($validateContent[3])) $_files               = $validateContent[3];


                    // 定義を追加
                    $file->addValidator($_validator,
                                        $_breakChainOnFailure,
                                        $_options,
                                        $_files);
                } // }}} foreach:2


                //---------------------------------------------------------------------------
                // 検証実行
                // 検証に引っかかったら、そのリクエスト値への検証を中止し、次の検証を行う
                //---------------------------------------------------------------------------
                if (! $file->isValid($requestName)) {
                    $fileMessage[$requestName] = $file->getMessages(); // エラーメッセージ
                    //dumper($fileMessage[$requestName]);
                }

            } // }}} foreach:1
        } // }}} if:1


        //dumper($fMessage);
        //----------------------------------------------------------------------
        // ■検証対象データを精査
        //   値が飛んできていなかったら強制的に空文字をセットします
        //   ※値を入れないと検証されないため
        //----------------------------------------------------------------------
        foreach ($this->_validationDefinitiontList as $k => $v) {

            // 値が存在しなければ空文字をセット
            if (!isset($this->_validationTargetData[ $k ])) {
                $this->_validationTargetData[ $k ] = '';
            }
        }


        //----------------------------------------------------------------------
        // ■検証実行
        //----------------------------------------------------------------------
        $input = new Mikoshiva_Filter_Input(array(), $this->_validationDefinitiontList, $this->_validationTargetData);
        $input->addValidatorPrefixPath('Mikoshiva_Validate', 'Mikoshiva/Validate'); // Mikodhiva 拡張クラスをセット


        //----------------------------------------------------------------------
        // ■エラーメッセージの項目名をマッピングされた名前に置換し再生成
        //----------------------------------------------------------------------
        $COLUMN_MAP_LIST = array();
        $COLUMN_MAP_LIST = Zend_Registry::get('COLUMN_MAP_LIST'); // 項目名のマッピングデータを取得

        $message          = array(); // メッセージ
        $errorMessageList = array(); // エラーメッセージリスト
        $errorMessageList = $input->getMessages() + $fileMessage;
        foreach ($errorMessageList as $k => $v) {

            // 置換 mcu_last_name → lastName
            $_k  = '';
            $_k = preg_replace('#^.*?_#', '', $k, 1);
            $_k = preg_replace('#_([a-z]+?)#e', "ucfirst('$1')", $_k);

            // 存在しない項目であればエラー
            if (! isset( $COLUMN_MAP_LIST[ $_k ])) {
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】検証項目リスト /mikoshiva/configs/column_map_list.php に”{$_k}”は存在しません";
                logDebug($message);
                throw new Mikoshiva_Validate_Exception($message);
            }


            // 項目名を置換
            foreach ($v as $k2 => $v2) {
                $message[] = str_replace('%COLUMN_NAME%', $COLUMN_MAP_LIST[$_k], $v2 );
            }
        }


        //----------------------------------------------------------------------
        // ■メッセージをreturn
        //----------------------------------------------------------------------
        return $message;


    }
}
