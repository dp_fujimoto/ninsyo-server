// 画面
//document.write('<div class="COM_srcBorder1024"></div>');
//document.write('<div class="COM_srcBorder768"></div>');

function zipToAddress(idZip1, idZip2, idPrefecture, idAddress){
    //郵便番号が3桁+4桁の数字であることをチェックする。
    var zip1 = $('#' + idZip1).val();
    var zip2 = $('#' + idZip2).val();

    if(zip1.length != 3) {
      alert('郵便番号の形式が正しくありません。');
      return;
    }
    if(isNaN(zip1)){
        alert('郵便番号の形式が正しくありません。');
        return;
      }
    if(zip2.length != 4) {
      alert('郵便番号の形式が正しくありません。');
      return;
    }
    if(isNaN(zip2)){
      alert('郵便番号の形式が正しくありません。');
      return;
    }
    // ※数値チェックの追加 2012-01-25 谷口司
    if (zip1.match(/^[0-9]{3}$/) === null) {
        alert('郵便番号（左）は数字３文字で入力して下さい。');
        return;
    }
    // ※数値チェックの追加 2012-01-25 谷口司
    if (zip2.match(/^[0-9]{4}$/) === null) {
        alert('郵便番号（右）は数字４文字で入力して下さい。');
        return;
    }

    //郵便番号をzipToAddressActionに送り、JSON形式でデータを取得する。
    //リクエスト先
    $.getJSON( "/utility/address/zip-to-address",
    { "zip1": zip1, "zip2": zip2 },
      function(data) {
        //取得したデータにerrorが含まれていた場合、警告を出して終了する。
        if(data.error != null) {
            alert(data.error);
          return;
        }
        //取得したデータをフォームに反映する。
        $('#' + idPrefecture).val(data.mzc_prefecture_name);
        $('#' + idAddress).val(data.mzc_address1 + data.mzc_address2);
    }
    );
}

/**
 * Ajax で指定されたアクションへリクエストを行い、
 * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行います。
 *
 * @param url アクション先 URL
 * @param targetId リクエスト結果を追記無いし、上書きするID名
 * @param formId フォーム ID
 * @param mode 追記モード
 * @return
 */
function requestToId(url, targetId, tabId, formId, mode) {

    // ◆form の値を一括取得
    var _form = 'tabId=' + tabId;
    if (formId !== '') {
        _form += '&' + $('#' + formId).serialize();
    }
    //------------------------------------------
    //■type="file" が存在するかをチェック
    //------------------------------------------
    var uploadFormCheck = false;
    if (formId !== null) {
        if ($('#' + formId + ' input[type="file"]').length > 0) {
            uploadFormCheck = true;
        }
    }

    //------------------------------------------
    //■POST、結果を新規コンテンツに埋める
    //------------------------------------------
    if (uploadFormCheck) {
        //alert('ファイルアップロードの場合');
        // ファイルアップロードの場合
        $('#' + formId).upload(url, _form, function(res) {
            if (mode === 'FRONT') {
                // 前方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(res + _html);
            } else if (mode === 'BACK') {
                // 後方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(_html + res);
            } else {
                // 上書き
                $('#' + targetId).html(res);
            }
        }, 'text');

    } else {
        //alert('ファイルアップロードじゃない場合');
        // ファイルアップロードじゃない場合
        $.post(url, _form, function(data, status){
            if (mode === 'FRONT') {
                // 前方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(data + _html);
            } else if (mode === 'BACK') {
                // 後方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(_html + data);
            } else {
                // 上書き
                $('#' + targetId).html(data);
            }
        });
    }
}



/**
 * 部署変更を行います
 * @return void
 */
function COM_divisionChanger(nowDivisionId, changeDivisionId){

    // セッションの権限を変更します
    $.post('./utility/division/change', 'changeDivisionId=' + changeDivisionId, function(data, status) {

//        // メニューバーの配色を変更します
//        var color = '';
//        switch (changeDivisionId) {
//            case '1':
//                color = '#003300';
//                break;
//            case '2':
//                color = '#3A5675';
//                break;
//            case '3':
//                color = '#754646';
//                break;
//            case '4':
//                color = '#757546';
//                break;
//            case '5':
//                color = '#844F6A';
//                break;
//            case '6':
//                color = '#8C6636';
//                break;
//        }
//        $('div.MegaMenu').css('background-color', color);

//        // 最後に叩かれた親メニューを実行します
//        OYA_newTab($('#systemInfoLastOyaUrl').html());
    });

/***
    // 「OK」時の処理開始 ＋ 確認ダイアログの表示
    if(window.confirm('部署を変更すると全てのタブが閉じられます。\nよろしいですか？')) {

        $.post('/utility/division/change', 'changeDivisionId=' + changeDivisionId, function(data, status){
            location.reload();
        });

    } else {

        $('#_divisionChanger').val(nowDivisionId);
    }
****/
}

/**
 * タブ名変更
 * @return void
 */
function COM_chgTabText(tabId){
    if (document.getElementById('page_title_' + tabId)) {
        document.getElementById('tab_text_' + tabId).innerHTML = document.getElementById('page_title_' + tabId).innerHTML;
        document.getElementById('tab_text_' + tabId).className = 'COM_fontWhite';
    }
}











