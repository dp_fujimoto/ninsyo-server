<?php
require_once 'Mikoshiva/Exception.php';

/**
 * Mikoshivaファイルダウンロード履歴例外
 *
 * @package Mikoshiva_Task
 * @author fujimoto <fujimoto@d-publishing.jp>
 * @since 2011/03/16
 */
class Mikoshiva_Log_FileDownloadHistory_Exception extends Mikoshiva_Exception {

}