<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Zend/Session/Namespace.php';

/**
 * セッションの値を返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: GetSession.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_GetSession extends Mikoshiva_View_Helper_Abstract
{
    /**
     * セッションの値を返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: GetSession.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $namespace 名前空間の名前
     * @param string $name 取り出したいキー名
     * @return mixed
     */
    public function getSession( $namespace = null, $name = null )
    {
        $session = null;
        if ( isset( $namespace ) ) {
            $session = new Zend_Session_Namespace( $namespace );
        } else {
            $session = new Zend_Session_Namespace();
        }

        if ( ! isset( $name ) ) {
            return $session;
        } else if( is_array( $session ) ) {
            return $session[ $name ];
        } else {
            return $session->$name;
        }
    }
}
