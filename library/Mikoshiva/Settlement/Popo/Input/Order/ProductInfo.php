<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Order_ProductInfo
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Order_ProductInfo extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'mstProduct' => null,
        'mstCampaignProduct' => null,
        'mstCampaign' => null,
        'mstProductShipping' => null,
        'paymentNumber' => null,
        'paymentAmount' => '0',
        'provisorySalesFlag' => '0',
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * mstProduct
	 *
     * @return Popo_MstProduct mstProduct
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMstProduct() {
		return $this->_properties['mstProduct'];
    }


    /**
     * mstProduct
	 *
	 * @param Popo_MstProduct $mstProduct mstProduct
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMstProduct($mstProduct) {
		$this->_properties['mstProduct'] = $mstProduct;
    }


    /**
     * mstCampaignProduct
	 *
     * @return Popo_MstCampaignProduct mstCampaignProduct
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMstCampaignProduct() {
		return $this->_properties['mstCampaignProduct'];
    }


    /**
     * mstCampaignProduct
	 *
	 * @param Popo_MstCampaignProduct $mstCampaignProduct mstCampaignProduct
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMstCampaignProduct($mstCampaignProduct) {
		$this->_properties['mstCampaignProduct'] = $mstCampaignProduct;
    }


    /**
     * mstCampaign
	 *
     * @return Popo_MstCampaign mstCampaign
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMstCampaign() {
		return $this->_properties['mstCampaign'];
    }


    /**
     * mstCampaign
	 *
	 * @param Popo_MstCampaign $mstCampaign mstCampaign
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMstCampaign($mstCampaign) {
		$this->_properties['mstCampaign'] = $mstCampaign;
    }


    /**
     * mstProductShipping
	 *
     * @return Popo_MstProductShipping mstProductShipping
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getMstProductShipping() {
		return $this->_properties['mstProductShipping'];
    }


    /**
     * mstProductShipping
	 *
	 * @param Popo_MstProductShipping $mstProductShipping mstProductShipping
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setMstProductShipping($mstProductShipping) {
		$this->_properties['mstProductShipping'] = $mstProductShipping;
    }


    /**
     * paymentNumber
	 *
     * @return  paymentNumber
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getPaymentNumber() {
		return $this->_properties['paymentNumber'];
    }


    /**
     * paymentNumber
	 *
	 * @param  $paymentNumber paymentNumber
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setPaymentNumber($paymentNumber) {
		$this->_properties['paymentNumber'] = $paymentNumber;
    }


    /**
     * paymentAmount
	 *
     * @return  paymentAmount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getPaymentAmount() {
		return $this->_properties['paymentAmount'];
    }


    /**
     * paymentAmount
	 *
	 * @param  $paymentAmount paymentAmount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setPaymentAmount($paymentAmount) {
		$this->_properties['paymentAmount'] = $paymentAmount;
    }


    /**
     * provisorySalesFlag
     *
     * @return  provisorySalesFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function getProvisorySalesFlag() {
        return $this->_properties['provisorySalesFlag'];
    }


    /**
     * provisorySalesFlag
     *
     * @param  $provisorySalesFlag provisorySalesFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: ProductInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     */
    public function setProvisorySalesFlag($provisorySalesFlag) {
        $this->_properties['provisorySalesFlag'] = $provisorySalesFlag;
    }


}

























