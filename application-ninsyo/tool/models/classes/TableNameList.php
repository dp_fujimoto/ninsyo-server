<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * テーブル名のリストを作成します。
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2010/02/15
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Classes_TableNameList
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
    * テーブル名のリストを作成します。
    *
    * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
    * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
    * @param Mikoshiva_Controller_Mapping_ActionContext $context
    * @return object
    */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {

        //-----------------------------------------------
        // テーブル名一覧作成
        //-----------------------------------------------
        $tableList = array();

        // SQL作成
        $sql = '';
        $sql .= 'SHOW TABLES';

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql);

        while ($v = $res->fetch(Zend_Db::FETCH_ASSOC)) {
            foreach ($v as $k2 => $v2) {
                $tableList[$v2] = $v2;
            }
        }

        return $tableList;
    }
}
