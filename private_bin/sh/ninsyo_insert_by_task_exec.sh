#!/bin/bash
#
# ninsyo_insert_by_task
#
# @since  2012/3/27
# @author f.fujimoto
#

#include library classpath
. /usr/local/bin/miko_include_classpath.sh

##################################################
# define variables
##################################################
DATE=$(date +%Y%m%d)

EXECDIR="/var/www/${SERVERMODEID}/www/private_bin"
EXECCLASS="${EXECDIR}/batch.php"

LOGDIR="/var/www/${SERVERMODEID}/www/systems/sh_logs"
LOGFILE="${LOGDIR}/ninsyo_insert_by_task_${DATE}.log"

##################################################
# start message
##################################################
echo [$(date +%F_%T)]:batch start >> ${LOGFILE}

##################################################
# execute
##################################################
YYYYMMDD=$(date +%F)
HHMMSS=$(date +%T)

MODEL="batch"
CONTROLLER="user-content"
ACTION="insert-by-task"

PARAMETER=""

echo "/usr/local/bin/php ${EXECCLASS} --zfm=${MODEL} --zfc=${CONTROLLER} --zfa=${ACTION} --zfp=${PARAMETER}" >> ${LOGFILE}
/usr/local/bin/php ${EXECCLASS} --zfm="${MODEL}" --zfc="${CONTROLLER}" --zfa="${ACTION}" --zfp="${PARAMETER}" >> ${LOGFILE} 2>&1

##################################################
# end message
##################################################
STATUS=$?
if [ ${STATUS} -eq 0 ]; then
  STDMSG="succeed"
else
  STDMSG="failure"
fi
echo [$(date +%F_%T)]:batch ${STDMSG} >> ${LOGFILE}
