<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_gmo_transaction_history
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstGmoTransactionHistory extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mgh';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'gmoTransactionHistoryId' => null,
        'transactionGroupKey' => null,
        'orderId' => null,
        'memberId' => null,
        'shopId' => null,
        'accessId' => null,
        'accessPass' => null,
        'cardNo' => null,
        'method' => null,
        'payTimes' => null,
        'entryStatus' => null,
        'amount' => null,
        'tranDate' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * ＧＭＯ決済履歴ＩＤ の値を返します
	 *
     * @return int ＧＭＯ決済履歴ＩＤ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGmoTransactionHistoryId() {
		return $this->_properties['gmoTransactionHistoryId'];
    }


    /**
     * ＧＭＯ決済履歴ＩＤ の値をセットします
	 *
	 * @param int $gmoTransactionHistoryId ＧＭＯ決済履歴ＩＤ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGmoTransactionHistoryId($gmoTransactionHistoryId) {
		$this->_properties['gmoTransactionHistoryId'] = $gmoTransactionHistoryId;
    }


    /**
     * トランザクショングループキー の値を返します
	 *
     * @return varchar トランザクショングループキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransactionGroupKey() {
		return $this->_properties['transactionGroupKey'];
    }


    /**
     * トランザクショングループキー の値をセットします
	 *
	 * @param varchar $transactionGroupKey トランザクショングループキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransactionGroupKey($transactionGroupKey) {
		$this->_properties['transactionGroupKey'] = $transactionGroupKey;
    }


    /**
     * オーダーID の値を返します
	 *
     * @return char オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderId() {
		return $this->_properties['orderId'];
    }


    /**
     * オーダーID の値をセットします
	 *
	 * @param char $orderId オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderId($orderId) {
		$this->_properties['orderId'] = $orderId;
    }


    /**
     * 会員ID の値を返します
	 *
     * @return varchar 会員ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemberId() {
		return $this->_properties['memberId'];
    }


    /**
     * 会員ID の値をセットします
	 *
	 * @param varchar $memberId 会員ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemberId($memberId) {
		$this->_properties['memberId'] = $memberId;
    }


    /**
     * ショップID の値を返します
	 *
     * @return varchar ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param varchar $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * 取引ID の値を返します
	 *
     * @return char 取引ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessId() {
		return $this->_properties['accessId'];
    }


    /**
     * 取引ID の値をセットします
	 *
	 * @param char $accessId 取引ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessId($accessId) {
		$this->_properties['accessId'] = $accessId;
    }


    /**
     * 取引パスワード の値を返します
	 *
     * @return char 取引パスワード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessPass() {
		return $this->_properties['accessPass'];
    }


    /**
     * 取引パスワード の値をセットします
	 *
	 * @param char $accessPass 取引パスワード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessPass($accessPass) {
		$this->_properties['accessPass'] = $accessPass;
    }


    /**
     * カード番号 の値を返します
	 *
     * @return char カード番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCardNo() {
		return $this->_properties['cardNo'];
    }


    /**
     * カード番号 の値をセットします
	 *
	 * @param char $cardNo カード番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCardNo($cardNo) {
		$this->_properties['cardNo'] = $cardNo;
    }


    /**
     * 支払方法 の値を返します
	 *
     * @return char 支払方法
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMethod() {
		return $this->_properties['method'];
    }


    /**
     * 支払方法 の値をセットします
	 *
	 * @param char $method 支払方法
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMethod($method) {
		$this->_properties['method'] = $method;
    }


    /**
     * 支払回数 の値を返します
	 *
     * @return char 支払回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPayTimes() {
		return $this->_properties['payTimes'];
    }


    /**
     * 支払回数 の値をセットします
	 *
	 * @param char $payTimes 支払回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPayTimes($payTimes) {
		$this->_properties['payTimes'] = $payTimes;
    }


    /**
     * 取引ステータス の値を返します
	 *
     * @return varchar 取引ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEntryStatus() {
		return $this->_properties['entryStatus'];
    }


    /**
     * 取引ステータス の値をセットします
	 *
	 * @param varchar $entryStatus 取引ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEntryStatus($entryStatus) {
		$this->_properties['entryStatus'] = $entryStatus;
    }


    /**
     * 支払い金額 の値を返します
	 *
     * @return int 支払い金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAmount() {
		return $this->_properties['amount'];
    }


    /**
     * 支払い金額 の値をセットします
	 *
	 * @param int $amount 支払い金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAmount($amount) {
		$this->_properties['amount'] = $amount;
    }


    /**
     * 決済日付 の値を返します
	 *
     * @return datetime 決済日付
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTranDate() {
		return $this->_properties['tranDate'];
    }


    /**
     * 決済日付 の値をセットします
	 *
	 * @param datetime $tranDate 決済日付
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTranDate($tranDate) {
		$this->_properties['tranDate'] = $tranDate;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return timestamp 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























