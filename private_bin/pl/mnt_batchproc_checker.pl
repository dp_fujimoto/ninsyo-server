#!/usr/bin/perl -w
##################################################
#
# Mikoshiva BatchProc Checker (127)
#
#  利用モジュール
#  Net::SMTP - SMTP(Simple Mail Transfer Protocol)
#
#  kawakami 2011/01/26
#
##################################################

# import
use strict;
use warnings;

use Net::SMTP;
use Encode qw(from_to);


##################################################
## const
##################################################

my %config = (
    logdir   => '/var/www/mikoshiva.com/www/systems/sh_logs',
    code     => 'euc-jp',
    name     => 'Mikoshiva',
    server   => '127',
    hostname => '211.10.131.127',
    smtphost => 'localhost',
    errorsto => 'error@mikoshiva.com',
    from     => 'status@mikoshiva.com',
    fromname => 'Mikoshiva Reporter',
#    to       => 'direct.errors@gmail.com'
    to       => 'kawakami@kaihatsu.com'
);


##################################################
## main
##################################################

sub main() {
    # 日時データを取得
    my %calendar = getDateTime();

    # ログファイルリスト
    my @logFiles = (
        ['miko_charge_shipping_continuity_'  . $calendar{date} . '.log','11',3]
    );

    # コマンドリスト
    my %commandList = (
        procDrm        => 'ps aux | awk \'/^USER|drm_/ && !/awk/ { print $0; }\'',
        procQmail      => 'ps aux | awk \'/^USER|qmail-/ && !/qmail-remote/ { print $0; }\'',
        diskFree       => 'df -h',
        memoryFree     => '/usr/bin/free -k'
    );

    # TailLogs取得
    my $tailLogsResultMap = getTailLogs(\@logFiles, $config{logdir});

    # メール組み立て
    my $subject = '[INFO] ' . $config{name} . ' 請求発送バッチ実行結果 (' . $config{server} . ')';
    my $body = '';
    
    # 警告メッセージ追記
    if ($$tailLogsResultMap{warnFlag} == "1") {
      $body .= getWarnMessage($body);
    }
    
    $body .= "********************************************************************************\n";
    $body .= "請求発送バッチ実行結果\n";
    $body .= $calendar{datetime} . "\n";
    $body .= "\n";
    $body .= "================================================================================\n";
    $body .= "host: " . $config{hostname} . " (" . $config{name} . ")\n";

#    $body .= getStandardOutput($commandList{procDrm});        # DRM Process
#    $body .= getStandardOutput($commandList{procQmail});      # qmail Process
#    $body .= getStandardOutput($commandList{diskFree});       # Disk Free
#    $body .= getStandardOutput($commandList{memoryFree});     # Memory Free

    $body .= $$tailLogsResultMap{logsBody};                   # TailLogs Body
    $body .= "********************************************************************************\n";

    # メールメッセージ組み立て
    my %mailMessage = (
        code     => $config{code},
        smtphost => $config{smtphost},
        errorsto => $config{errorsto},
        from     => $config{from},
        fromname => $config{fromname},
        to       => $config{to},
        subject  => $subject,
        body     => $body
    );

    # メール送信
    sendMail(\%mailMessage, \%calendar);
}


##################################################
## function
##################################################
#
# コマンドの発行結果をフォーマットして返します
# @Param $command
#
sub getStandardOutput($) {
    my ($command) = @_;
    my $body = '';

    if (defined($command)) {
        my $process = `$command`;
        if (! defined($process)) {
            $process = '';
        }

        $body .= "================================================================================\n";
        $body .= "# " . $command . "\n";
        $body .= "\n";
        $body .= $process;
        $body .= "\n";
    }

    return $body;
}

#
# ログファイルの最後から
# 指定行取得してフォーマットして返します
# @Param array  @logFiles
# @Param string $logDirectory
# @return hash %resultMap
#
sub getTailLogs(\@$) {
    my ($logFiles, $logDirectory) = @_;

    # 警告フラグ
    my $warnFlag    = '0';

    # 初期化
    my $body        = '';
    my $logFileName = '';
    my $logFile     = '';
    my $linage      = '';
    my $warnlimit   = 0;

    # 要素数を取得
    my $num = @$logFiles;

    for (my $i = 0; $i < $num; $i++) {
        $logFileName = $$logFiles[$i][0];
        $linage      = $$logFiles[$i][1];
        $warnlimit   = $$logFiles[$i][2];

        if (defined($logFileName) && defined($linage) && defined($warnlimit)) {
            $logFile = $logDirectory . '/' . $logFileName;

            my $log = `/usr/bin/tail -n $linage $logFile | /usr/bin/nkf -e -u`;
            if (! defined($log)) {
                $log = '';
            }

            # メール本文組立
            $body .= "================================================================================\n";
            $body .= "# " . $logFileName . "\n";
            $body .= "\n";
            $body .= $log;
            $body .= "\n";

            # 警告対象チェック
            my $count = @{[$log =~ /(\[\d{4}-\d{2}-\d{2}_\d{2}:\d{2}:\d{2}\] this script has already run\.)/g]};
            if ($count >= $warnlimit) {
                # 警告フラグON
                $warnFlag = "1";
            }
        }

        sleep 1;
    }

    # リザルトマップ構築
    my %resultMap = (
        logsBody => $body,
        warnFlag => $warnFlag
    );

    return \%resultMap;
}

#
# 警告メッセージを返します
# @return string $body
#
sub getWarnMessage($) {
    my ($body) = @_;
    
    # 警告メッセージ組み立て
    my $message = "";    
    $message .= "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■\n";
    $message .= "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■\n";
    $message .= "\n";
    $message .= "                      [Warning!]バッチを確認して下さい！\n";
    $message .= "\n";
    $message .= "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■\n";
    $message .= "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■\n";
    $message .= "\n";

    # 本文追加
    if (defined($body)) { 
        $message .= $body;
    }
    
    return $message;
}

#
# メール送信を行います
# @Param hash %mailMessage
# @Param hash %calendar
#
sub sendMail(\%\%) {
    my ($mailMessage, $calendar) = @_;

    my $code      = $$mailMessage{code};
    my $smtphost  = $$mailMessage{smtphost};
    my $to        = $$mailMessage{to};
    my $from      = $$mailMessage{from};
    my $fromname  = $$mailMessage{fromname};
    my $subject   = $$mailMessage{subject};
    my $body      = $$mailMessage{body};
    my $errorsto  = $$mailMessage{errorsto};
    my $timestamp = $$calendar{timestamp};

    from_to($subject, $code, "MIME-Header-ISO_2022_JP");
    from_to($body, $code, "iso-2022-jp");

    my $smtp = Net::SMTP->new($smtphost);
    $smtp->mail($from);
    $smtp->to(split(/,/, $to));
    $smtp->data();
    $smtp->datasend("Date: " . $timestamp . "\n");
    $smtp->datasend("From: " . $fromname . " <" . $from . ">\n");
    $smtp->datasend("To: " . $to . "\n");
    $smtp->datasend("Subject: " . $subject . "\n");
    $smtp->datasend("Errors-To: " . $errorsto . "\n");
    $smtp->datasend("Message-ID: <" . time . "." . int(rand(1000)) . "." . $from . ">\n");
    $smtp->datasend("Content-Transfer-Encoding: 7bit\n");
    $smtp->datasend("Content-Type: text/plain; charset=\"ISO-2022-JP\"\n\n");
    $smtp->datasend($body . "\n");
    $smtp->dataend();
    $smtp->quit;
}

#
# 現在の日時データを3タイプで返します
# メールヘッダに埋める現在の日時データ
# @return string "Mon, 7 Jan 2008 09:00:00 +0900 (JST)"
# 現在の日時データ
# @return string "2008-01-07 09:00:00"
# 現在の年月日
# @return string "20080107"
#
sub getDateTime() {
    $ENV{"TZ"} = "JST-9";
    my ($sec, $min, $hour, $mday, $mon, $year, $wday) = localtime(time);
    my @week  = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    my @month = ("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                 "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    my $timestamp = sprintf("%s, %d %s %04d %02d:%02d:%02d +0900 (JST)",
                        $week[$wday], $mday, $month[$mon], $year + 1900, $hour, $min, $sec);

    my $datetime = sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $year + 1900, $mon + 1, $mday, $hour, $min, $sec);

    my $date = sprintf("%04d%02d%02d", $year + 1900, $mon + 1, $mday);

    my %calendar = (
        timestamp => $timestamp,    # "Mon, 7 Jan 2008 09:00:00 +0900 (JST)"
        datetime  => $datetime,     # "2008-01-07 09:00:00"
        date      => $date          # "20080107"
    );

    return %calendar;
}


##################################################
## main start
##################################################

main();
