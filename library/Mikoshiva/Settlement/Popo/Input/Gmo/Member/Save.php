<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Input_Member_Save
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Member_Save extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'SiteID' => null,
        'SitePass' => null,
        'MemberID' => null,
        'MemberName' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * SiteID
	 *
     * @return  SiteID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSiteID() {
		return $this->_properties['SiteID'];
    }


    /**
     * SiteID
	 *
	 * @param  $SiteID SiteID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSiteID($SiteID) {
		$this->_properties['SiteID'] = $SiteID;
    }


    /**
     * SitePass
	 *
     * @return  SitePass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSitePass() {
		return $this->_properties['SitePass'];
    }


    /**
     * SitePass
	 *
	 * @param  $SitePass SitePass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSitePass($SitePass) {
		$this->_properties['SitePass'] = $SitePass;
    }


    /**
     * MemberID
	 *
     * @return  MemberID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMemberID() {
		return $this->_properties['MemberID'];
    }


    /**
     * MemberID
	 *
	 * @param  $MemberID MemberID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMemberID($MemberID) {
		$this->_properties['MemberID'] = $MemberID;
    }


    /**
     * MemberName
	 *
     * @return  MemberName
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMemberName() {
		return $this->_properties['MemberName'];
    }


    /**
     * MemberName
	 *
	 * @param  $MemberName MemberName
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Save.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMemberName($MemberName) {
		$this->_properties['MemberName'] = $MemberName;
    }


}

























