<?php
require_once 'Mikoshiva/Plugin/Abstruct.php';
require_once 'Mikoshiva/Plugin/Luffy/Output.php';

/**
 * LuffyAPI用基底クラス
 * LuffyAPIにアクセスする部品は必ずこのクラスを継承します。
 *
 * @author k.shimowaki
 * @since version - 2011/12/16
 */
abstract class Mikoshiva_Plugin_Luffy_Abstruct extends Mikoshiva_Plugin_Abstruct {

    // 承認パス（LUFFYをsha1でハッシュ化したもの）
    const PASS_CODE = '1589a8ca345d35df2a8b7ba2f0ea8341a5a75fec';


    /**
     * コンストラクタ
     *
     * @param string $url
     */
    public function __construct($url) {

        parent::__construct($url);
        $this->client->setParameterPost('pass', self::PASS_CODE);

    }

    /**
     * 返却用のフォーマットに変換します。
     *
     * @param string $body
     * @return array
     *
     * @author k.shimowaki
     */
    protected function retFormat($body) {

        $out = new Mikoshiva_Plugin_Luffy_Output();
        $ex = explode('||',$body);

        if (count($ex) === 1) {

            $out->setResult('error');
            $out->setErrorMessage('LUFFY連動システムで致命的なエラーが発生しました。');

        } else {

            $out->setResult($ex[0]);
            $out->setErrorCode($ex[1]);
            $out->setErrorMessage($ex[2]);
            $out->setExecTime($ex[3]);

            if ($ex[0] === 'error') {
                $out->setErrorMessage('LUFFY連動システムでエラーが発生しました。');
            }
        }

        // 結果がfailureだった場合はエラーメール送信
        if ($out->getResult() === 'failure') {
        	$this->error(new Mikoshiva_Exception('Luffy外部連携でfailureが返却されました。'));
        }
logTemporary('【Luffy結果返却値】：'.print_r($out,true));
        return $out;
    }
}