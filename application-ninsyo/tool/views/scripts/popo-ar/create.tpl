
<p>
    POPO と AR が作成・更新されました。
    以下のファイルをコミットして下さい。
</p>
<div>
    ・&emsp;/mikoshiva/configs/table-info.php<br>
    ・&emsp;/mikoshiva/models/ninsyo/Popo （以下全て）<br>
    ・&emsp;/mikoshiva/library/Mikoshiva/Db/NinsyoAr （以下全て）<br>
</div>



<table id="popo-ar">
    <caption>作成・更新されたテーブル一覧</caption>
    <tr>
        <th>テーブル名</th>
    </tr>
{foreach from=$tableList item="tableInfo"}
    <tr>
        {if $tableInfo.create === '○'}
        <td style="background:none repeat scroll 0 0 #550000;; color:#fff;">{$tableInfo.tableName}</td>
        {else}
        <td>{$tableInfo.tableName}</td>
        {/if}
    </tr>
{/foreach}
</table>

<script>
function popoArTrColor () {

    // ページ内のテーブルを取得
    var table = $("#popo-ar");
    for(i=0; i < table.length; i++){
        tr = table[i].getElementsByTagName("tr");
        for(j=0; j<tr.length; j++){
            // 偶数の時
            if (j % 2 == 0) {
                $(tr[j]).css("background-color", "#ddd");
            } else {
                $(tr[j]).css("background-color", "#fff");
            }
        }
    }
}
popoArTrColor();

</script>