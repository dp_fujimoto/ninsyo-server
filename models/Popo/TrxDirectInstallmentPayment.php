<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_direct_installment_payment
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxDirectInstallmentPayment extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tdi';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'directInstallmentPaymentId' => null,
        'divisionId' => null,
        'chargeId' => null,
        'directInstallmentPaymentNumber' => null,
        'customerId' => null,
        'shopId' => null,
        'orderId' => null,
        'accessId' => null,
        'accessPass' => null,
        'amount' => null,
        'chargeDate' => null,
        'realChargeType' => null,
        'accountingFlag' => null,
        'chargeFlag' => null,
        'chargeErrorFlag' => null,
        'rechargeOrderId' => null,
        'rechargeType' => null,
        'rechargeDueDate' => null,
        'rechargeDate' => null,
        'rechargeErrorFlag' => null,
        'errorType' => null,
        'errorCode' => null,
        'errorInfo' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * ダイレクト分割払い請求ID の値を返します
	 *
     * @return int ダイレクト分割払い請求ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPaymentId() {
		return $this->_properties['directInstallmentPaymentId'];
    }


    /**
     * ダイレクト分割払い請求ID の値をセットします
	 *
	 * @param int $directInstallmentPaymentId ダイレクト分割払い請求ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPaymentId($directInstallmentPaymentId) {
		$this->_properties['directInstallmentPaymentId'] = $directInstallmentPaymentId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 請求ID の値を返します
	 *
     * @return int 請求ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * 請求ID の値をセットします
	 *
	 * @param int $chargeId 請求ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * ダイレクト分割払い番号 の値を返します
	 *
     * @return int ダイレクト分割払い番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPaymentNumber() {
		return $this->_properties['directInstallmentPaymentNumber'];
    }


    /**
     * ダイレクト分割払い番号 の値をセットします
	 *
	 * @param int $directInstallmentPaymentNumber ダイレクト分割払い番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPaymentNumber($directInstallmentPaymentNumber) {
		$this->_properties['directInstallmentPaymentNumber'] = $directInstallmentPaymentNumber;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * オーダーID の値を返します
	 *
     * @return char オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderId() {
		return $this->_properties['orderId'];
    }


    /**
     * オーダーID の値をセットします
	 *
	 * @param char $orderId オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderId($orderId) {
		$this->_properties['orderId'] = $orderId;
    }


    /**
     * GMOアクセスID の値を返します
	 *
     * @return char GMOアクセスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessId() {
		return $this->_properties['accessId'];
    }


    /**
     * GMOアクセスID の値をセットします
	 *
	 * @param char $accessId GMOアクセスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessId($accessId) {
		$this->_properties['accessId'] = $accessId;
    }


    /**
     * GMOアクセスパス の値を返します
	 *
     * @return char GMOアクセスパス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessPass() {
		return $this->_properties['accessPass'];
    }


    /**
     * GMOアクセスパス の値をセットします
	 *
	 * @param char $accessPass GMOアクセスパス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessPass($accessPass) {
		$this->_properties['accessPass'] = $accessPass;
    }


    /**
     * 金額 の値を返します
	 *
     * @return int 金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAmount() {
		return $this->_properties['amount'];
    }


    /**
     * 金額 の値をセットします
	 *
	 * @param int $amount 金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAmount($amount) {
		$this->_properties['amount'] = $amount;
    }


    /**
     * 請求日 の値を返します
	 *
     * @return date 請求日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeDate() {
		return $this->_properties['chargeDate'];
    }


    /**
     * 請求日 の値をセットします
	 *
	 * @param date $chargeDate 請求日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeDate($chargeDate) {
		$this->_properties['chargeDate'] = $chargeDate;
    }


    /**
     * 実請求タイプ の値を返します
	 *
     * @return varchar 実請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealChargeType() {
		return $this->_properties['realChargeType'];
    }


    /**
     * 実請求タイプ の値をセットします
	 *
	 * @param varchar $realChargeType 実請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealChargeType($realChargeType) {
		$this->_properties['realChargeType'] = $realChargeType;
    }


    /**
     * 課金済フラグ の値を返します
	 *
     * @return char 課金済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccountingFlag() {
		return $this->_properties['accountingFlag'];
    }


    /**
     * 課金済フラグ の値をセットします
	 *
	 * @param char $accountingFlag 課金済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccountingFlag($accountingFlag) {
		$this->_properties['accountingFlag'] = $accountingFlag;
    }


    /**
     * 請求フラグ の値を返します
	 *
     * @return char 請求フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeFlag() {
		return $this->_properties['chargeFlag'];
    }


    /**
     * 請求フラグ の値をセットします
	 *
	 * @param char $chargeFlag 請求フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeFlag($chargeFlag) {
		$this->_properties['chargeFlag'] = $chargeFlag;
    }


    /**
     * 請求エラーフラグ の値を返します
	 *
     * @return char 請求エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeErrorFlag() {
		return $this->_properties['chargeErrorFlag'];
    }


    /**
     * 請求エラーフラグ の値をセットします
	 *
	 * @param char $chargeErrorFlag 請求エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeErrorFlag($chargeErrorFlag) {
		$this->_properties['chargeErrorFlag'] = $chargeErrorFlag;
    }


    /**
     * 再請求オーダーID の値を返します
	 *
     * @return char 再請求オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeOrderId() {
		return $this->_properties['rechargeOrderId'];
    }


    /**
     * 再請求オーダーID の値をセットします
	 *
	 * @param char $rechargeOrderId 再請求オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeOrderId($rechargeOrderId) {
		$this->_properties['rechargeOrderId'] = $rechargeOrderId;
    }


    /**
     * 再請求タイプ の値を返します
	 *
     * @return varchar 再請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeType() {
		return $this->_properties['rechargeType'];
    }


    /**
     * 再請求タイプ の値をセットします
	 *
	 * @param varchar $rechargeType 再請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeType($rechargeType) {
		$this->_properties['rechargeType'] = $rechargeType;
    }


    /**
     * 再請求予定日 の値を返します
	 *
     * @return date 再請求予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeDueDate() {
		return $this->_properties['rechargeDueDate'];
    }


    /**
     * 再請求予定日 の値をセットします
	 *
	 * @param date $rechargeDueDate 再請求予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeDueDate($rechargeDueDate) {
		$this->_properties['rechargeDueDate'] = $rechargeDueDate;
    }


    /**
     * 再請求日 の値を返します
	 *
     * @return date 再請求日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeDate() {
		return $this->_properties['rechargeDate'];
    }


    /**
     * 再請求日 の値をセットします
	 *
	 * @param date $rechargeDate 再請求日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeDate($rechargeDate) {
		$this->_properties['rechargeDate'] = $rechargeDate;
    }


    /**
     * 再請求エラーフラグ の値を返します
	 *
     * @return char 再請求エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeErrorFlag() {
		return $this->_properties['rechargeErrorFlag'];
    }


    /**
     * 再請求エラーフラグ の値をセットします
	 *
	 * @param char $rechargeErrorFlag 再請求エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeErrorFlag($rechargeErrorFlag) {
		$this->_properties['rechargeErrorFlag'] = $rechargeErrorFlag;
    }


    /**
     * エラータイプ の値を返します
	 *
     * @return char エラータイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorType() {
		return $this->_properties['errorType'];
    }


    /**
     * エラータイプ の値をセットします
	 *
	 * @param char $errorType エラータイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorType($errorType) {
		$this->_properties['errorType'] = $errorType;
    }


    /**
     * エラーコード の値を返します
	 *
     * @return char エラーコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorCode() {
		return $this->_properties['errorCode'];
    }


    /**
     * エラーコード の値をセットします
	 *
	 * @param char $errorCode エラーコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorCode($errorCode) {
		$this->_properties['errorCode'] = $errorCode;
    }


    /**
     * エラー情報 の値を返します
	 *
     * @return char エラー情報
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorInfo() {
		return $this->_properties['errorInfo'];
    }


    /**
     * エラー情報 の値をセットします
	 *
	 * @param char $errorInfo エラー情報
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorInfo($errorInfo) {
		$this->_properties['errorInfo'] = $errorInfo;
    }


    /**
     * 欠陥フラグ の値を返します
	 *
     * @return char 欠陥フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * 欠陥フラグ の値をセットします
	 *
	 * @param char $imperfectionFlag 欠陥フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























