<?php
/**
 * Zend_Http_Client のラップクラス
 *
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/23
 * @version SVN:$Id: Client.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Http_Client extends Zend_Http_Client {


    /**
     * Configuration array, set using the constructor or using ::setConfig()
     *
     * @var array
     */
    protected $config = array(
        'maxredirects'    => 5,
        'strictredirects' => false,
        'useragent'       => 'Zend_Http_Client',
        'timeout'         => 60,
        'adapter'         => 'Zend_Http_Client_Adapter_Socket',
        'httpversion'     => self::HTTP_1,
        'keepalive'       => false,
        'storeresponse'   => true,
        'strict'          => true
    );


    /**
     * コンストラクタ
     *
     * @param $uri
     * @param $config
     * @return unknown_type
     */
    public function __construct($uri = null, $config = null)
    {
        parent::__construct($uri, $config);
    }

    public function getParameterRequest() {
        return $this->paramsPost + $this->paramsGet;
    }

    public function getParameterGet() {
        return $this->paramsGet;
    }

    public function getParameterPost() {
        return $this->paramsPost;
    }

    /**
     * (non-PHPdoc)
     * @see library/Zend/Http/Zend_Http_Client#request($method)
     */
    public function request($method = null)
    {
        return parent::request($method);

        /*
        // テスト
        require_once 'Mikoshiva/Http/Response.php';

        $header = array();
        $header["Date"] = "Tue, 23 Mar 2010 09:56:20 GMT";
        $header["Server" ]= "Apache/2.2.14 (Win32) PHP/5.2.12";
        $header["X-powered-by"] = "PHP/5.2.12";
        $header["Set-cookie"] = "ZDEDebuggerPresent=php,phtml,php3; path=/";
        $header["Content-length"] = "757";
        $header["Connection"] = "close";
        $header["Content-type"] = "text/html";

        // 成功
        $body = "success||||||2010-03-23 08:50:27";

        // 失敗
        $body = "failure||E_01021||customer none||2010-03-23 08:46:35";


        return new Mikoshiva_Http_Response(200, $header, $body, '1.1', 'OK');
        */
    }

}