<?php
/** @package Mikoshiva_Gmo */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Exec/Run.php';

/**
 * Mikoshiva_Settlement_Execution_Gmo_Exec_Run
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Exec_Run extends Mikoshiva_Settlement_Execution_Gmo_Abstract {




    /**
     * 初期化処理
     *
     * @param Popo_Abstract $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Exec_Run */
        $inputPopo;


        // コンフィグからサイト ID とサイトパスを取得
        $siteId   = $gmoConfig['SITE_ID'];
        $sitePass = $gmoConfig['SITE_PASS'];


        // サイト ID とサイトパスをPOPO に詰め直す
        $inputPopo->setSiteID($siteId);
        $inputPopo->setSitePass($sitePass);


        // カード登録連番（固定）
        $inputPopo->setCardSeq('0');


        // GMO 結果 POPO クラスをセット
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Run();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['EXEC_PGM'];

        // dumper($this->_gmoConnectioniUrl);
        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }


    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Exec/Mikoshiva_Settlement_Gmo_Exec_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/04
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Exec/Exception.php';


        /* @var $inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Exec_Run */
        $inputPopo;

        // 取引 ID
        if(isBlankOrNull($inputPopo->getAccessID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // 取引パスワード
        if(isBlankOrNull($inputPopo->getAccessPass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引パスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // オーダー ID
        if(isBlankOrNull($inputPopo->getOrderID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】オーダー ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // 支払い方法
        // if(isBlankOrNull($inputPopo->getMethod())) return false;


        // 支払回数 ID
        // if(isBlankOrNull($inputPopo->getPayTimes())) return false;


        // サイト ID
        if(isBlankOrNull($inputPopo->getSiteID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイト ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // サイトパスワード
        if(isBlankOrNull($inputPopo->getSitePass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイトパスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // 会員 ID
        if(isBlankOrNull($inputPopo->getMemberID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】会員 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // カード登録連番
        if(isBlankOrNull($inputPopo->getCardSeq())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】カード登録連番 は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Exec_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);


        return true;
    }
}












