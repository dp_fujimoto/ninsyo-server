<?php
/** @package Mikoshiva_Drm */

/**
 * require
 */
require_once 'Mikoshiva/Exception.php';

/**
 * 
 * 
 * @package Mikoshiva_Drm
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Drm_Exception extends Mikoshiva_Exception {

}