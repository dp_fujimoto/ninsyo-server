<?php
/**
 * @package Mikoshiva_Log
 */

/**
 * require
 */
include_once 'Mikoshiva/Utility/Class.php';
include_once 'Mikoshiva/Debug/VarDump.php';
include_once 'Zend/Controller/Request/Http.php';
include_once 'Mikoshiva/Utility/Class.php';


/**
 * Zend_log のラップクラス
 *
 * @package Mikoshiva_Log
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/28
 * @version SVN:$Id: Log.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Log extends Zend_Log {


    /**
     * MIKOSHIVA のログを記録します
     *
     * @param string|array $message    メッセージ
     * @param string       $addMessage 追記文字列
     * @param string       $priority
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/18
     * @version SVN:$Id: Log.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function logger($message, $addMessage = '', $priority) {


        $_message = $message;
        if (is_array($_message)) {
            $_message = "\n" . Mikoshiva_Debug_VarDump::exec($_message, '$_DEBUG');
        }

        if (! isBlankOrNull($addMessage)) {
            try {
                $_message = '[' . $addMessage . '] ' . $_message;
            }catch (Exception $e) {
            }
        }


        /* @var $request Zend_Controller_Request_Http */
        $request        = Zend_Registry::get('request');
        $moduleName     = $request->getModuleName();
        $controllerName = $request->getControllerName();
        $actionName     = $request->getActionName();

        $requestUri = '';
        if(method_exists($request, 'getRequestUri')) {
            $requestUri = $request->getRequestUri();
        }

        $prefi  = '';
        $prefix = '['
                  . $moduleName
                  . '_'
                  . $controllerName
                  . '_'
                  . $actionName
                  . '] ';

        if(!isBlankOrNull($requestUri)) {
            $prefix .= '['
                      . session_id()
                      . '] ';
        }

        // lib/Mikoshiva クラスは出力しない
        $logExeClass = Mikoshiva_Utility_Class::getCalledClass(4);
        if (LOG_MIKOSHIVA_CLASS === false) {
            if (preg_match('#^Mikoshiva_#', $logExeClass) === 1) {
                return;
            }
        }

        $_message = $prefix
                  . Mikoshiva_Utility_Class::getCalledClass(4)
                  . ' '
                  . $_message;
        $this->log($_message, $priority);
    }
}







