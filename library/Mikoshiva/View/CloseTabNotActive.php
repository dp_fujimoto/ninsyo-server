<?php
/**
 * @package Mikoshiva_View
 */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';

/**
 * 新しいタブを開きます
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: CloseTabNotActive.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_View_Helper_ColseTabNotActive extends Mikoshiva_View_Helper_Abstract {

    /**
     * 新しいタブを開き、タブの保管を行います
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: CloseTabNotActive.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     * @param string $url 新規タブの URL
     * @param boolean $storage タブの保存 trueなら保存
     * @return unknown_type
     */
    public function newTabLink( $url, $name, $key = '', $storage = false, $id = null ) {
        return '<a href="#" '
             .     'name="" '
             .     'id="" '
             .     'onclick="$(\'#con0\').load(\'' . $url . '\');"'
             . '>' . $name . '</a>';
    }
}
