<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">{if !isBlankOrNull($form->getStaffId())}スタッフ情報変更　入力{else}スタッフ情報登録　入力{/if}</h2>

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            {if $form->getLvFlag() !== '0'}
                <br>
                <span class="COM_bold">変更したログイン情報は次回ログイン時に有効になります。</span>
                <br>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                {if $form->getLvFlag() === '1'}
                    <input type="hidden" name="staffId" value="{$form->getStaffId()}">
                    <input type="hidden" name="lvFlag" value="{$form->getLvFlag()}">
                {/if}
                {if $form->getSpFlag() === '1'}
                    <input type="hidden" name="spFlag" value="{$form->getSpFlag()}">
                {/if}


                <!-- // スタッフマスタ情報入力テーブル開始 -->
                <table class="COM_table COM_tableWidthLong">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ID
                        </td>
                        <td>{if !isBlankOrNull($form->getStaffId())}{$form->getStaffId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            部署<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            {divisionLister name="divisionId" mode="select" selected=$form->getDivisionId()}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            氏名<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>カナ：</td>
                                    <td>
                                        <input type="text" name="lastNameKana" value="{$form->getLastNameKana()}" size="20" maxlength="30">　
                                        <input type="text" name="firstNameKana" value="{$form->getFirstNameKana()}" size="20" maxlength="30">
                                    </td>
                                </tr>
                                <tr>
                                    <td>漢字：</td>
                                    <td>
                                        <input type="text" name="lastName" value="{$form->getLastName()}" size="20" maxlength="20">　
                                        <input type="text" name="firstName" value="{$form->getFirstName()}" size="20" maxlength="20">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            メールアドレス<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            {if $form->getLvFlag() === '1'}
                                <input type="text" name="email" value="{$form->getEmail()}" size="70" maxlength="200">
                            {else}
                                {$form->getEmail()}
                                <input type="hidden" name="email" value="{$form->getEmail()}">
                            {/if}
                        </td>
                    </tr>
                    {if $form->getLvFlag() === '1'}
                        {if isBlankOrNull($form->getStaffId()) || $form->getSpFlag() === '1'}
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                パスワード<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td>
                                <input type="text" name="loginPassword" value="{$form->getLoginPassword()}" size="20" maxlength="20">
                                <input type="hidden" name="validFlag" value="1">
                                <input type="hidden" name="deleteFlag" value="0">
                            </td>
                        </tr>
                        {/if}
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                状態<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td>
                                <input type="radio" name="validFlag" value="1" {if $form->getValidFlag() === '1'}checked{/if}>有効　
                                <input type="radio" name="validFlag" value="0" {if $form->getValidFlag() === '0'}checked{/if}>無効
                                <br>
                                <input type="radio" name="deleteFlag" value="0" {if $form->getDeleteFlag() === '0'}checked{/if}>削除しない　
                                <input type="radio" name="deleteFlag" value="1" {if $form->getDeleteFlag() === '1'}checked{/if}>削除する
                            </td>
                        </tr>
                    {/if}
                </table>
                <!-- // 商品マスタ編集フォーム終了 -->

                <br>
                <hr>
                <br>

                <!-- // ページ下部のボタン開始 -->
                <div>
                    {currentTabButton name='　　　　　　次へ　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/staff/confirm' tabId=$tabId  formId='inputForm'}
                </div>
                <!-- // ページ下部のボタン終了 -->

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->