<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_campaign_product_external_parameter
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCampaignProductExternalParameter extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mce';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'campaignProductExternalParameterId' => null,
        'campaignProductId' => null,
        'externalParameterId' => null,
        'stepTiming' => null,
        'creditCardFlag' => null,
        'paymentDeliveryFlag' => null,
        'bankTransferFlag' => null,
        'valueNumber' => null,
        'value1' => null,
        'value2' => null,
        'value3' => null,
        'value4' => null,
        'value5' => null,
        'cancelValue1Flag' => null,
        'cancelValue2Flag' => null,
        'cancelValue3Flag' => null,
        'cancelValue4Flag' => null,
        'cancelValue5Flag' => null,
        'subscriptionEndCancelFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * キャンペーン商品外部連携ID の値を返します
	 *
     * @return int キャンペーン商品外部連携ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductExternalParameterId() {
		return $this->_properties['campaignProductExternalParameterId'];
    }


    /**
     * キャンペーン商品外部連携ID の値をセットします
	 *
	 * @param int $campaignProductExternalParameterId キャンペーン商品外部連携ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductExternalParameterId($campaignProductExternalParameterId) {
		$this->_properties['campaignProductExternalParameterId'] = $campaignProductExternalParameterId;
    }


    /**
     * キャンペーン商品ID の値を返します
	 *
     * @return int キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductId() {
		return $this->_properties['campaignProductId'];
    }


    /**
     * キャンペーン商品ID の値をセットします
	 *
	 * @param int $campaignProductId キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductId($campaignProductId) {
		$this->_properties['campaignProductId'] = $campaignProductId;
    }


    /**
     * 外部連携パラメータID の値を返します
	 *
     * @return int 外部連携パラメータID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExternalParameterId() {
		return $this->_properties['externalParameterId'];
    }


    /**
     * 外部連携パラメータID の値をセットします
	 *
	 * @param int $externalParameterId 外部連携パラメータID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExternalParameterId($externalParameterId) {
		$this->_properties['externalParameterId'] = $externalParameterId;
    }


    /**
     * 適用ステップタイミング の値を返します
	 *
     * @return int 適用ステップタイミング
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStepTiming() {
		return $this->_properties['stepTiming'];
    }


    /**
     * 適用ステップタイミング の値をセットします
	 *
	 * @param int $stepTiming 適用ステップタイミング
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStepTiming($stepTiming) {
		$this->_properties['stepTiming'] = $stepTiming;
    }


    /**
     * 適用対象カード決済フラグ の値を返します
	 *
     * @return char 適用対象カード決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCreditCardFlag() {
		return $this->_properties['creditCardFlag'];
    }


    /**
     * 適用対象カード決済フラグ の値をセットします
	 *
	 * @param char $creditCardFlag 適用対象カード決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCreditCardFlag($creditCardFlag) {
		$this->_properties['creditCardFlag'] = $creditCardFlag;
    }


    /**
     * 適用対象代金引換フラグ の値を返します
	 *
     * @return char 適用対象代金引換フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryFlag() {
		return $this->_properties['paymentDeliveryFlag'];
    }


    /**
     * 適用対象代金引換フラグ の値をセットします
	 *
	 * @param char $paymentDeliveryFlag 適用対象代金引換フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryFlag($paymentDeliveryFlag) {
		$this->_properties['paymentDeliveryFlag'] = $paymentDeliveryFlag;
    }


    /**
     * 適用対象銀行振込フラグ の値を返します
	 *
     * @return char 適用対象銀行振込フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBankTransferFlag() {
		return $this->_properties['bankTransferFlag'];
    }


    /**
     * 適用対象銀行振込フラグ の値をセットします
	 *
	 * @param char $bankTransferFlag 適用対象銀行振込フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBankTransferFlag($bankTransferFlag) {
		$this->_properties['bankTransferFlag'] = $bankTransferFlag;
    }


    /**
     * 有効値保持数 の値を返します
	 *
     * @return int 有効値保持数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValueNumber() {
		return $this->_properties['valueNumber'];
    }


    /**
     * 有効値保持数 の値をセットします
	 *
	 * @param int $valueNumber 有効値保持数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValueNumber($valueNumber) {
		$this->_properties['valueNumber'] = $valueNumber;
    }


    /**
     * 値１ の値を返します
	 *
     * @return varchar 値１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue1() {
		return $this->_properties['value1'];
    }


    /**
     * 値１ の値をセットします
	 *
	 * @param varchar $value1 値１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue1($value1) {
		$this->_properties['value1'] = $value1;
    }


    /**
     * 値２ の値を返します
	 *
     * @return varchar 値２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue2() {
		return $this->_properties['value2'];
    }


    /**
     * 値２ の値をセットします
	 *
	 * @param varchar $value2 値２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue2($value2) {
		$this->_properties['value2'] = $value2;
    }


    /**
     * 値３ の値を返します
	 *
     * @return varchar 値３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue3() {
		return $this->_properties['value3'];
    }


    /**
     * 値３ の値をセットします
	 *
	 * @param varchar $value3 値３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue3($value3) {
		$this->_properties['value3'] = $value3;
    }


    /**
     * 値４ の値を返します
	 *
     * @return varchar 値４
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue4() {
		return $this->_properties['value4'];
    }


    /**
     * 値４ の値をセットします
	 *
	 * @param varchar $value4 値４
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue4($value4) {
		$this->_properties['value4'] = $value4;
    }


    /**
     * 値５ の値を返します
	 *
     * @return varchar 値５
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue5() {
		return $this->_properties['value5'];
    }


    /**
     * 値５ の値をセットします
	 *
	 * @param varchar $value5 値５
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue5($value5) {
		$this->_properties['value5'] = $value5;
    }


    /**
     * キャンセル値１フラグ の値を返します
	 *
     * @return char キャンセル値１フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue1Flag() {
		return $this->_properties['cancelValue1Flag'];
    }


    /**
     * キャンセル値１フラグ の値をセットします
	 *
	 * @param char $cancelValue1Flag キャンセル値１フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue1Flag($cancelValue1Flag) {
		$this->_properties['cancelValue1Flag'] = $cancelValue1Flag;
    }


    /**
     * キャンセル値２フラグ の値を返します
	 *
     * @return char キャンセル値２フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue2Flag() {
		return $this->_properties['cancelValue2Flag'];
    }


    /**
     * キャンセル値２フラグ の値をセットします
	 *
	 * @param char $cancelValue2Flag キャンセル値２フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue2Flag($cancelValue2Flag) {
		$this->_properties['cancelValue2Flag'] = $cancelValue2Flag;
    }


    /**
     * キャンセル値３フラグ の値を返します
	 *
     * @return char キャンセル値３フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue3Flag() {
		return $this->_properties['cancelValue3Flag'];
    }


    /**
     * キャンセル値３フラグ の値をセットします
	 *
	 * @param char $cancelValue3Flag キャンセル値３フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue3Flag($cancelValue3Flag) {
		$this->_properties['cancelValue3Flag'] = $cancelValue3Flag;
    }


    /**
     * キャンセル値４フラグ の値を返します
	 *
     * @return char キャンセル値４フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue4Flag() {
		return $this->_properties['cancelValue4Flag'];
    }


    /**
     * キャンセル値４フラグ の値をセットします
	 *
	 * @param char $cancelValue4Flag キャンセル値４フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue4Flag($cancelValue4Flag) {
		$this->_properties['cancelValue4Flag'] = $cancelValue4Flag;
    }


    /**
     * キャンセル値５フラグ の値を返します
	 *
     * @return char キャンセル値５フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancelValue5Flag() {
		return $this->_properties['cancelValue5Flag'];
    }


    /**
     * キャンセル値５フラグ の値をセットします
	 *
	 * @param char $cancelValue5Flag キャンセル値５フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancelValue5Flag($cancelValue5Flag) {
		$this->_properties['cancelValue5Flag'] = $cancelValue5Flag;
    }


    /**
     * 購読終了キャンセルフラグ の値を返します
	 *
     * @return char 購読終了キャンセルフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionEndCancelFlag() {
		return $this->_properties['subscriptionEndCancelFlag'];
    }


    /**
     * 購読終了キャンセルフラグ の値をセットします
	 *
	 * @param char $subscriptionEndCancelFlag 購読終了キャンセルフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionEndCancelFlag($subscriptionEndCancelFlag) {
		$this->_properties['subscriptionEndCancelFlag'] = $subscriptionEndCancelFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























