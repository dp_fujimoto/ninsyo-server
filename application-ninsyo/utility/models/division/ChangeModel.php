<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';


/**
 *
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/22
 * @version SVN:$Id: ChangeModel.php,v 1.1 2012/09/21 07:08:31 fujimoto Exp $
 */
class Utility_Models_Division_ChangeModel
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定された条件で顧客検索を行い、検索結果を取得します
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                              Mikoshiva_Controller_Mapping_ActionContext $context) {


      include_once 'Zend/Session/Namespace.php';
      $staff = new Zend_Session_Namespace('staff');
      $staff->changeDivisionId = $actionForm->getChangeDivisionId();
      //dumper($staff);
    }
}
