<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_TrxCustomerThanksMailAttachmentFile
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_TrxCustomerThanksMailAttachmentFile extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'trx_customer_thanks_mail_attachment_file';
    protected $_primary  = 'ttf_customer_thanks_mail_attachment_file_id';
    protected $_metadata = array (
  'ttf_customer_thanks_mail_attachment_file_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_customer_thanks_mail_attachment_file_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'ttf_customer_thanks_mail_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_customer_thanks_mail_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_number' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_number',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_file_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_file_name',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '255',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_file_namehash' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_file_namehash',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '32',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_file_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_file_type',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_file_extension' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_file_extension',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '20',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_file_size' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_file_size',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_error_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_error_flag',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_delete_flag',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_deletion_datetime',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_update_datetime',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_registration_datetime',
    'COLUMN_POSITION' => 13,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'ttf_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_thanks_mail_attachment_file',
    'COLUMN_NAME' => 'ttf_update_timestamp',
    'COLUMN_POSITION' => 14,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

