<?php
/** @package Mikoshiva_Settlement */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Popo/Output/Payment/Change.php';

include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Member/Save.php';
include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Card/Save.php';
include_once 'Mikoshiva/Utility/Copy.php';

require_once 'Mikoshiva/Settlement/Execution/Gmo/Member/Save.php';
require_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Save/Register.php';





/**
 * 支払い方法変更で使われる実行クラス
 * 会員登録とカード登録を行います
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/05
 * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Payment_Change {


    /**
     * Mikoshiva_Settlement_Gmo_Popo_Input_Payment_Change
     * @var Mikoshiva_Settlement_Gmo_Popo_Input_Payment_Change
     */
    protected $_inputPopo;


    /**
     * Mikoshiva_Settlement_Gmo_Popo_Output_Payment_Change
     * @var Mikoshiva_Settlement_Gmo_Popo_Output_Payment_Change
     */
    protected $_outputPopo;


    /**
     * コンストラクタ
     *
     * @param $inputPopoList
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/05
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct(Mikoshiva_Settlement_Popo_Input_Payment_Change $inputPopo) {


        // inputPopo をセット
        $this->_inputPopo = $inputPopo;


        // Output クラスのインスタンスを生成
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Payment_Change();
    }


    /**
     * 実行メソッド
     *
     * @return boolean true:成功|false:失敗
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/05
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function execute() {


        // inputPopo を取得
        $inputPopo = $this->_inputPopo;
//dumper($inputPopo);//exit;


        //-------------------------------------------
        //
        // ▼会員登録
        //
        //-------------------------------------------
        // inputPopo から会員登録 POPO にコピー
        $memberPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Member_Save();
        Mikoshiva_Utility_Copy::copyToPopo($memberPopo, $inputPopo);
//dumper($memberPopo);//exit;


        // 会員登録実行
        $memberExec = new Mikoshiva_Settlement_Execution_Gmo_Member_Save($memberPopo);
        $ret        = $memberExec->execute();
//dumper($ret);//exit;
//dumper($memberExec->getOutput());//exit;


        // エラー処理
        if (! $ret)  {
            $output = $this->setOutput($memberExec->getOutput());
            return false;
        }

        //-------------------------------------------
        //
        // ▼カード登録
        //
        //-------------------------------------------
        // inputPopo から会員登録 POPO にコピー
        $caradPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Card_Save();
        Mikoshiva_Utility_Copy::copyToPopo($caradPopo, $inputPopo);
//dumper($caradPopo);//exit;

        // 会員登録実行クラス
        $cardExec = new Mikoshiva_Settlement_Execution_Gmo_Card_Save_Register($caradPopo);
        $ret      = $cardExec->execute();

        // エラー処理
        if (! $ret)  {
            $output = $this->setOutput($cardExec->getOutput());
            return false;
        }
//dumper($ret);//exit;
//dumper($cardExec->getOutput());//exit;

        //------------------------------------------------------------
        // ■ GMO 取引履歴マスタに履歴を登録
        //------------------------------------------------------------
        $data = array();
        $data['memberId']    = $memberExec->getOutput()->getMemberId();
        $data['cardNo']      = Mikoshiva_Utility_Convert::creditCardSecret($inputPopo->getCardNo());
        $data['entryStatus'] = 'ENTRY_STATUS_PAYMENT_CHANGE';
        //$cardExec->getOutput()->getErrCode();
        //$cardExec->getOutput()->getErrInfo();

        // 実行
        include_once 'Mikoshiva/Db/Simple.php';
        $simple = new Mikoshiva_Db_Simple();
        $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);



        // 成功
        return true;
    }


    /**
     * 処理結果を POPO にコピーします
     *
     * @param Popo_Abstract $output
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/23
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setOutput(Popo_Abstract $output) {
        Mikoshiva_Utility_Copy::copyToPopo($this->_outputPopo, $output);
    }


    /**
     * Output クラス
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/05
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_outputPopo;
    }
}







































