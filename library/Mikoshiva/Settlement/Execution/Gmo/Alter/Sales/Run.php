<?php

include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Sales.php';

/**
 * 仮売上 → 実売上の処理を行い、処理が成功した時だけ DB 登録を行います
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/06/03
 * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Alter_Sales_Run {



    /**
     * Popo_Abstract
     * @var Popo_Abstract
     */
    protected $_outputPopo;


    /**
     * メイン処理
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/03
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function execute(Mikoshiva_Settlement_Popo_Input_Gmo_Alter $inputPopo) {


        //------------------------------------------
        // ■カード更新処理
        //------------------------------------------
        // ▼インスタンス生成
        $alterSalesExec = null;
        $alterSalesExec = new Mikoshiva_Settlement_Execution_Gmo_Alter_Sales($inputPopo);

        // ▼実行
        $ret = null;
        $ret = $alterSalesExec->execute();

        // ▼結果
        /* @var $output Mikoshiva_Settlement_Popo_Output_Gmo_Alter */
        $output = null;
        $output = $alterSalesExec->getOutput();
        $this->_outputPopo = $output;

        // ▼処理に失敗していたら false を投げる
        if (! $ret) {
            return false;
        }

        //------------------------------------------
        // ■DB 登録処理
        //------------------------------------------
        // ▼登録データを生成
        $data = array();
        $data = array(
            //'mgh_gmo_transaction_history_id' => '',
            //'mgh_order_id'                   => '',
            //'memberId'                  => $inputPopo->getMemberID(),
            'shopId'                    => $inputPopo->getShopID(),
            'accessId'                  => $inputPopo->getAccessID(),
            'accessPass'                => $inputPopo->getAccessPass(),
            //'cardNo'                    => Mikoshiva_Utility_Convert::creditCardSecret($inputPopo->getCardNo()),
            //'mgh_method'                     => '',
            //'mgh_pay_times'                  => '',
            'entryStatus'               => 'ENTRY_STATUS_SALES',
            'amount'                    => $inputPopo->getAmount(),
            'tranDate'                  => $output->getTranDate(),
            //'mgh_delete_flag'                => '',
            //'mgh_deletion_datetime'          => '',
            'registrationDatetime'      => Mikoshiva_Date::mikoshivaNow(),
            'updateDatetime'            => Mikoshiva_Date::mikoshivaNow(),
            //'mgh_update_timestamp'           => '',
        );

        // ▼登録
        $simple = null;
        $simple = new Mikoshiva_Db_Simple();
        $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);


        return true;
    }


    /**
     * GMO との通信結果を返します
     *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Alter
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/18
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_outputPopo;
    }
}
























