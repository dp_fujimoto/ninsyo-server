/*********************************
*
* 証明書用サイトシールを表示します
*
*********************************/
function drowSslSiteSeal(){
    var host = document.domain;

    $.post('/mk/siteSeal.php', { hostName: host }, function(data, status){
    	// ジオトラストのサイトシールを表示
        if (data == 'SSL_TYPE_GEOTRUST') {

            document.getElementById('geotrustSealHeader').style.display = "";
            document.getElementById('verisignSealHeader').style.display = "none";

            document.getElementById('geotrustSealFooter').style.display = "";
            document.getElementById('verisignSealFooter').style.display = "none";

        // ベリサインのサイトシールを表示
        } else if (data == 'SSL_TYPE_VERISIGN') {

            document.getElementById('geotrustSealHeader').style.display = "none";
            document.getElementById('verisignSealHeader').style.display = "";

            document.getElementById('geotrustSealFooter').style.display = "none";
            document.getElementById('verisignSealFooter').style.display = "";
        } else {

            document.getElementById('geotrustSealHeader').style.display = "none";
            document.getElementById('verisignSealHeader').style.display = "none";

            document.getElementById('geotrustSealFooter').style.display = "none";
            document.getElementById('verisignSealFooter').style.display = "none";
        }
    });

}
