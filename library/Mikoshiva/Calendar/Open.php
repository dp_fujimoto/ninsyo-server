<?php
/**
 * @package Mikoshiva_Calendar
 */


/**
 * require
 */
require_once 'Mikoshiva/Date.php';


/**
 * カレンダー祝日取得クラス
 *
 * @package Mikoshiva_Calendar
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/18
 * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Calendar_Open {

    /**
     * 祝日・休日すべて
     * @var int
     */
    const ALL             = 0;


    /**
     * 船井休日
     * @var int
     */
    const FUNAI           = 1;


    /**
     * ダイレクト出版休日
     * @var int
     */
    const DIRECT          = 2;


    /**
     * 国民の祝日
     * @var int
     */
    const HOLIDAY         = 3;


    /**
     * 国民の祝日・船井休日
     * @var int
     */
    const HOLIDAY_FUNAI    = 4;


    /**
     * 国民の祝日・ダイレクト出版休日
     * @var int
     */
    const HOLIDAY_DIRECT = 5;


    /**
     * 船井・ダイレクト出版休日
     * @var int
     */
    const FUNAI_DIRECT    = 6;


    /**
     * （基底日日付＋指定日数）から最短の”前”営業日を取得します
     *
     * @param string $baseDate 基底日
     * @param int    $addDay   日数
     * @param string $format   日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getBeforeHolidayDay($baseDate, $addDay = 0, $format = 'yyyy-MM-dd', $mode = self::ALL) {
        return self::_getHoliday($baseDate, $addDay, $format, $mode, 'addDay', -1);
    }


    /**
     * （基底日日付＋指定日数）から最短の”翌”営業日を取得します
     *
     * @param string $baseDate 基底日
     * @param int    $addDay   日数
     * @param string $format   日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getAfterHolidayDay($baseDate, $addDay = 0, $format = 'yyyy-MM-dd', $mode = self::ALL) {
        return self::_getHoliday($baseDate, $addDay, $format, $mode, 'addDay', 1);
    }


    /**
     * （基底日日付＋指定月数）から最短の”前”営業日を取得します
     *
     * （基底日日付＋指定月数）が 2010/01/31 で、月数が 1 で有った場合、2010/02/28 です 2010/03/03 ではない
     *
     * @param string $baseDate 基底日
     * @param int    $addMonth 月数
     * @param string $format   出力日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getBeforeHolidayMonth($baseDate, $addMonth = 0, $format = 'yyyy-MM-dd', $mode = self::ALL) {
        return self::_getHoliday($baseDate, $addMonth, $format, $mode, 'addMonth', -1);
    }


    /**
     * （基底日日付＋指定月数）から最短の”翌”営業日を取得します
     *
     * （基底日日付＋指定月数）が 2010/01/31 で、月数が 1 で有った場合、2010/02/28 です 2010/03/03 ではない
     *
     * @param string $baseDate 基底日
     * @param int    $addMonth 月数
     * @param string $format   出力日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getAfterHolidayMonth($baseDate, $addMonth = 0, $format = 'yyyy-MM-dd', $mode = self::ALL) {
        return self::_getHoliday($baseDate, $addMonth, $format, $mode, 'addMonth', 1);
    }


    /**
     * 基底日日付から指定した営業日が経過した営業日を取得します。
     *
     * 基底日日付が 2010/02/01 で、経過日数が 3 で、間が全て営業日であった場合、2010/02/04
     * 同条件で、2010/02/02が休日であった場合、2010/02/05
     *
     * @param string $baseDate 基底日
     * @param int    $count    経過日数
     * @param string $format   出力日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getAfterManyBusinessDayDay($baseDate, $addDay, $format = 'yyyy-MM-dd', $mode = self::ALL) {
        return self::_getBusinessday($baseDate, $addDay, $format, $mode, 1);
    }


    /**
     * 基底日日付から指定した営業日を遡った時点の営業日を取得します。
     *
     * 基底日日付が 2010/02/05 で、経過日数が 3 で、間が全て営業日であった場合、2010/02/02
     * 同条件で、2010/02/02が休日であった場合、2010/02/01
     *
     * @param string $baseDate 基底日
     * @param int    $count    経過日数
     * @param string $format   出力日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getBeforeManyBusinessDayDay($baseDate, $addDay, $format = 'yyyy-MM-dd', $mode = self::ALL) {
        return self::_getBusinessday($baseDate, $addDay, $format, $mode, -1);
    }


    /**
     * 基底日日付から指定した営業日が経過した営業日を取得します。
     *
     * 基底日日付が 2010/02/01 で、経過日数が 3 で、間が全て営業日であった場合、2010/02/04
     * 同条件で、2010/02/02が休日であった場合、2010/02/05
     *
     *
     *
     * @param string $baseDate 基底日
     * @param int    $count    経過日数
     * @param string $format   出力日付フォーマット
     * @param int    $mode     祝日・休日対象モード
     * @param int    $openMode      営業日モード（1：翌営業日 -1：前営業日）
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected static function _getBusinessDay($baseDate, $count, $format, $mode, $openMode) {
        //--------------------------------------------------------------------
        // ■ 前処理
        //--------------------------------------------------------------------
        $date = null;
        $date = new Mikoshiva_Date($baseDate);

        $startDate = '';
        $startDate = $date->toString('yyyy-MM-dd');

        // 経過日が0なら、基底日を返す
        if($count === 0) {
            return $date->toString($format);
        }

        //--------------------------------------------------------------------
        // ■ 日付リスト$dayListの作成
        //   $dayList[0] => string(10) "2010-03-12"
        //   $dayList[1] => string(10) "2010-03-13"
        //   $dayList[2] => string(10) "2010-03-14"
        //--------------------------------------------------------------------
        // 日付リストを何件作成するかを決定
        // 数値の根拠 ：週休二日(7日/5 = 1.4）、GW等連休分が10日
        $dayListLength = '';
        $dayListLength = (int)($count * 1.5 + 10);
        //$dayListLength = 1; // 再帰テスト用

        $dayList   = array();
        for ($i = 0; $i < $dayListLength; $i++) {
            $date->addDay($openMode);
            $dayList[] = $date->toString('yyyy-MM-dd');
        }

        $endDate = '';
        $endDate = $date->toString('yyyy-MM-dd');

        //--------------------------------------------------------------------
        // ■ 休日リストの作成
        //   $dayList[0] => string(10) "2010-02-11"
        //   $dayList[1] => string(10) "2010-02-13"
        //   $dayList[2] => string(10) "2010-02-14"
        //--------------------------------------------------------------------
        include_once 'Mikoshiva/Db/Ar/MstHoliday.php';
        $db      = new Mikoshiva_Db_Ar_MstHoliday();
        $select  = $db->select('mho_holiday_date');

        // WHERE -------------------------------------
        self::_setHolidayDayToWhere($select, $mode); // 祝日モード

        if ($openMode > 0) {
            $select->order('mho_holiday_date ASC');
            $select->where('mho_holiday_date > ?', $startDate);
            $select->where('mho_holiday_date <= ?', $endDate);
        } else {
            $select->order('mho_holiday_date DESC');
            $select->where('mho_holiday_date < ?', $startDate);
            $select->where('mho_holiday_date >= ?', $endDate);
        }

        self::_setHolidayDayToWhere($select, $mode);

        // 実行
        $rows = $db->fetchAll($select)->toArray();

        $holidayList = array();
        foreach($rows as $row) {
            $holidayList[] = $row['mho_holiday_date'];
        }

        //--------------------------------------------------------------------
        // ■ 営業日リストの作成
        //--------------------------------------------------------------------
        // array_diff(日付リスト, 休日リスト)の結果(添え字が歯抜けの配列になる)を添え字が0から始まる配列に詰め直し、営業日リストを作成
        $businessDayList = array();
        $businessDayList = array_merge(array_diff($dayList, $holidayList));

        //--------------------------------------------------------------------
        // ■ $count営業日後の取得
        //--------------------------------------------------------------------
        // 最初に作成したリストの範囲を超える場合は、それを踏まえて再起呼び出し
        if(count($businessDayList) < $count) {
            return self::getBusinessDay($date->toString('yyyy-MM-dd'), $count - count($businessDayList), $format, $mode, $openMode);
        } else {
            $date->setDate($businessDayList[$count - 1]);
            return $date->toString($format);
        }

    }

    /**
     * （基底日日付＋指定月数 or 指定日数）から最短の”翌”営業日 or ”前”営業日を取得します
     *
     * @param string $baseDate      基底日
     * @param int    $addDay        日数・月数
     * @param string $format        出力日付フォーマット
     * @param string $mode          祝日・休日対象モード
     * @param string $addMethodName Mikoshiva_Date のメソッド名
     * @param int    $openMode      営業日モード（1：翌営業日 -1：前営業日）
     *
     * @return string 営業日
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected static function _getHoliday($baseDate, $addDay, $format, $mode, $addMethodName, $openMode) {


        // ログ開始
        logInfo(LOG_START);


        //-----------------------------------------------------
        // ■指定日を取得 2010/03/05
        //-----------------------------------------------------
        $date = new Mikoshiva_Date($baseDate);
        $date->$addMethodName($addDay);
        $startDate = $date->toString('yyyy-MM-dd'); // 2010-03-15
        //dumper($startDate);// exit;


        //-----------------------------------------------------
        // ■指定日から -20 日までの配列を生成する
        //
        //   [0] => string(10) "2010-03-15"
        //   [1] => string(10) "2010-03-14"
        //   [2] => string(10) "2010-03-13"
        //   [3] => string(10) "2010-03-12"
        //   [4] => string(10) "2010-03-11"
        //   [5] => string(10) "2010-03-10"
        //   [6] => string(10) "2010-03-09"
        //   [7] => string(10) "2010-03-08"
        //   [8] => string(10) "2010-03-07"
        //   [9] => string(10) "2010-03-06"
        //   [10] => string(10) "2010-03-05"
        //   [11] => string(10) "2010-03-04"
        //   [12] => string(10) "2010-03-03"
        //   [13] => string(10) "2010-03-02"
        //   [14] => string(10) "2010-03-01"
        //   [15] => string(10) "2010-02-28"
        //   [16] => string(10) "2010-02-27"
        //   [17] => string(10) "2010-02-26"
        //   [18] => string(10) "2010-02-25"
        //   [19] => string(10) "2010-02-24"
        //   [20] => string(10) "2010-02-23"
        //-----------------------------------------------------
        $dateList   = array();
        $dateList[] = $startDate;
        for ($i = 0; $i < 20; $i++) {

            $date->addDay($openMode);
        	$dateList[] = $date->toString('yyyy-MM-dd');
        }
        //dumper($dateList);//exit;


        //-----------------------------------------------------
        // ■DB から指定日より -20 日分の祝日を取得する
        //
        //  [0] => array(4) {
        //    ["mho_holiday_id"] => string(1) "2"
        //    ["mho_holiday_date"] => string(10) "2010-03-03"
        //    ["mho_holiday_name"] => string(24) "谷口さんの誕生日"
        //    ["mho_holiday_attribute"] => string(7) "HOLIDAY"
        //  }
        //  [1] => array(4) {
        //    ["mho_holiday_id"] => string(1) "3"
        //    ["mho_holiday_date"] => string(10) "2010-03-01"
        //    ["mho_holiday_name"] => string(15) "なんかの日"
        //    ["mho_holiday_attribute"] => string(7) "HOLIDAY"
        //  }
        //-----------------------------------------------------
        include_once 'Mikoshiva/Db/Ar/MstHoliday.php';
        $db      = new Mikoshiva_Db_Ar_MstHoliday();
        $select  = $db->select();

        // WHERE -------------------------------------
        self::_setHolidayDayToWhere($select, $mode); // 祝日モード
        // 2010/03/11（20 日前） - 2010/03/31（指定日）
        if ($openMode > 0) {
            $select->where('mho_holiday_date >= ?', $startDate);
            $select->where('mho_holiday_date <= ?', $dateList[count($dateList) - 1]);
        } else {
            $select->where('mho_holiday_date <= ?', $startDate);
            $select->where('mho_holiday_date >= ?', $dateList[count($dateList) - 1]);
        }
        ////$select->order('mho_holiday_date DESC'); // 降順
        // SELECT `mst_holiday`.* FROM `mst_holiday` WHERE (mho_holiday_attribute <= '2010-03-15') AND (mho_holiday_attribute >= '2010-02-23') ORDER BY `mho_holiday_attribute`
        //echo $select;//exit;

        // 実行
        $row = $db->fetchAll($select);
        //dumper($row->toArray());//exit;


        //-----------------------------------------------------
        // ■取得出来なければ指定日を結果として返す
        //-----------------------------------------------------
        if ($row->count() === 0) {
            $date = null;
            $date = new Mikoshiva_Date($startDate);
            return $date->toString($format); // 2010-03-15
        }


        //-----------------------------------------------------
        // ■祝日リストを生成
        //   [0] => string(10) "2010-03-03"
        //   [1] => string(10) "2010-03-01"
        //-----------------------------------------------------
        $holidayList = array();
        foreach ($row->toArray() as $k => $v) {
            $holidayList[] = $v['mho_holiday_date'];
        }
        //dumper($holidayList);//exit;


        //-----------------------------------------------------
        // ■指定日から -20 日の日付が祝日リストに無ければ直近の値をセット
        //-----------------------------------------------------
        $openDay = '';
        foreach ($dateList as $k => $v) {

            // 配列の中に存在していなければ営業日
            if (! in_array($v, $holidayList, true)) {
                $openDay = $v;
                break;
            }
        }
        //dumper($openDay);// exit; // 2010-03-14  （2010-03-15 が祝日で有れば）


        //-----------------------------------------------------
        // ■もし営業日は取得出来なければ例外を投げる(通常ではありえない)
        //-----------------------------------------------------
        if ($openDay === '') {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】営業日が取得出来ませんでした。";
            logInfo($message);
            throw new Exception($message);
        }


        //-----------------------------------------------------
        // ■営業日が取得出来たら指定されたフォーマットにして返す
        //-----------------------------------------------------
        $date = null;
        $date = new Mikoshiva_Date($openDay);
        return $date->toString($format);
    }


    /**
     * 祝日モードを元に WHERE をセットします
     *
     * @param Zend_Db_Table_Select $select
     * @param int $mode モード
     *
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/18
     * @version SVN:$Id: Open.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected static function _setHolidayDayToWhere(Zend_Db_Table_Select $select, $mode) {


        switch ($mode) {


            // ALL ------------------------------------
            case 0:
                break;

            // FUNAI ----------------------------------
            case 1:
                $where = $select->where('mho_holiday_attribute = ?', 'HOLIDAY_ATTRIBUTE_FUNAI_LOGISTICS');
                break;

            // DIRECT ---------------------------------
            case 2:
                $where = $select->where('mho_holiday_attribute = ?', 'HOLIDAY_ATTRIBUTE_DIRECT_PUBLISHING');
                break;

            // HOLIDAY -------------------------------
            case 3:
                $where = $select->where('mho_holiday_attribute = ?', 'HOLIDAY_ATTRIBUTE_NORMAL');
                break;

            // HOLIDAY_FUNAI -------------------------
            case 4:
                $where = $select->where('mho_holiday_attribute in (?)', array('HOLIDAY_ATTRIBUTE_NORMAL', 'HOLIDAY_ATTRIBUTE_FUNAI_LOGISTICS'));
                break;

            // HOLIDAY_DIRECT ------------------------
            case 5:
                $where = $select->where('mho_holiday_attribute in (?)', array('HOLIDAY_ATTRIBUTE_NORMAL', 'HOLIDAY_ATTRIBUTE_DIRECT_PUBLISHING'));
                break;

            // FUNAI_DIRECT ---------------------------
            case 6:
                $where = $select->where('mho_holiday_attribute in (?)', array('HOLIDAY_ATTRIBUTE_FUNAI_LOGISTICS', 'HOLIDAY_ATTRIBUTE_DIRECT_PUBLISHING'));
                break;

            // default ---------------------------
            default:
                include_once 'Mikoshiva/Calendar/Exception.php';
                $message = "【" . __CLASS__ . "::" . __FUNCTION__ . " （" . __LINE__ . "）】指定された”{$mode}”は定義されてません。";
                logDebug($message);
                throw new Mikoshiva_Calendar_Exception($message);
        }
    }



}
