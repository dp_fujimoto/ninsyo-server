<?php
/**
 * 認証API用、定数定義ファイル（mikoshiva）
 *
 * 【説明】
 *
 * 【概要】
 *
 * 【メモ】
 *
 *
 *【変更履歴】
 * 2012-09-01 藤本浩史 作成
 */


//------------------------------------------------------------------
// ■０１．パス
//------------------------------------------------------------------
define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('LOG_TEMPORARY_PATH'  , DOCUMENT_ROOT . '/../systems/logs/temporary');


//------------------------------------------------------------------
// ■０２．ドメイン（起動ドメインを定義します）
//------------------------------------------------------------------
// ▼設定
if (strncmp(PHP_OS, 'WIN', 3) === 0) define('_MIKOSHIVA_DOMAIN_', 'mikoshiva-ninsyo');         // ローカル
else                                 define('_MIKOSHIVA_DOMAIN_', 'www.dp-auth.jp'); // 本番


//------------------------------------------------------------------
// ■０３．HTTPS かどうかで URL 定義を切り替える
//------------------------------------------------------------------
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    // ◆--- https の場合 ---◆
    define('MIKOSHIVA_URL',       'https://' . _MIKOSHIVA_DOMAIN_); // URL
} else {
    // ◆--- http の場合 ---◆
    define('MIKOSHIVA_URL',       'http://' . _MIKOSHIVA_DOMAIN_);  // URL
}


//------------------------------------------------------------------
// ■０４．ini_set
//------------------------------------------------------------------
//ini_set('include_path', ini_get('include_path')  . PATH_SEPARATOR . PEAR_PATH);


//------------------------------------------------------------------
// ■０５．各種定数
//------------------------------------------------------------------
//define('SYSTEM_EMAIL', 'fujimoto@d-publishing.jp');
define('SYSTEM_EMAIL', 'direct.errors@gmail.com');

//------------------------------------------------------------------
// ■０６．関数
//------------------------------------------------------------------
//送信関数
function send_socket($url, $post, $server){ 
    try {
        $param = http_build_query($post);
    
        $url_array = parse_url(MIKOSHIVA_URL . $url);
        // $port = 80; normal
        $port = 443; // ssl 
        $host = $url_array['host'];
        $path = $url_array['path'];

        if($path === ""){
            $path = "/";
        }

        $data = '';
        $header = '';
        // $sock = fsockopen($host, $port); normal

        $sock = fsockopen('ssl://' . $host, $port); //ssl
        $request = "POST " . $path . " HTTP/1.1\r\n"
            . "Host: " . $host . "\r\n"
            . "Content-Type: application/x-www-form-urlencoded\r\n"
            . "Content-Length: " . strlen($param) . "\r\n"
            . "User-Agent: MSIE7.0\r\n"
            . "Connection: Close\r\n"
            . "From: ninsyo-server <admin@ninsyo-mikoshiva.com>" . "\r\n"
            . "\r\n"
            .  $param . "\r\n";
            
        if(!$sock){
            mail(SYSTEM_EMAIL, 'socket error!', $request . "\n" . http_build_query($server));
            $data = 'ng';
        }else{
            fputs($sock, $request);
            
            do { 
                $header .= fgets($sock, 4096); 
            } while (strpos($header, "\r\n\r\n") === false);
            
            while(!feof($sock)){
                $data .= fgets($sock);
            }
            fclose($sock);
        }
        return $data;
    } catch (Exception $e) {
        mail(SYSTEM_EMAIL, 'api error!', $url . "\n" . http_build_query($server));
    }
}
