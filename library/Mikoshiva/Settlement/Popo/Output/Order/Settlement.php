<?php
/** @package Mikoshiva_Settlement */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Output_Order_Settlement
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Order_Settlement extends Popo_Abstract {



    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'masterOrderID' => null,
        'masterJobCd' => null,
        'AccessID' => null,
        'AccessPass' => null,
        'OrderID' => null,
        'ErrCode' => null,
        'ErrInfo' => null,
        'ErrFlag' => null,
        'paymentAmount' => null,
        'paymenShippingCost' => null,
        'paymentDatetime' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * masterOrderID
	 *
     * @return  masterOrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMasterOrderID() {
		return $this->_properties['masterOrderID'];
    }


    /**
     * masterOrderID
	 *
	 * @param  $masterOrderID masterOrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMasterOrderID($masterOrderID) {
		$this->_properties['masterOrderID'] = $masterOrderID;
    }



    /**
     * masterJobCd
	 *
     * @return  masterJobCd
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMasterJobCd() {
		return $this->_properties['masterJobCd'];
    }


    /**
     * masterJobCd
	 *
	 * @param  $masterJobCd masterJobCd
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMasterJobCd($masterJobCd) {
		$this->_properties['masterJobCd'] = $masterJobCd;
    }



    /**
     * AccessID
	 *
     * @return  AccessID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessID() {
		return $this->_properties['AccessID'];
    }


    /**
     * AccessID
	 *
	 * @param  $AccessID AccessID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessID($AccessID) {
		$this->_properties['AccessID'] = $AccessID;
    }


    /**
     * AccessPass
	 *
     * @return  AccessPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessPass() {
		return $this->_properties['AccessPass'];
    }


    /**
     * AccessPass
	 *
	 * @param  $AccessPass AccessPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessPass($AccessPass) {
		$this->_properties['AccessPass'] = $AccessPass;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * ErrCode
	 *
     * @return  ErrCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrCode() {
		return $this->_properties['ErrCode'];
    }


    /**
     * ErrCode
	 *
	 * @param  $ErrCode ErrCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrCode($ErrCode) {
		$this->_properties['ErrCode'] = $ErrCode;
    }


    /**
     * ErrInfo
	 *
     * @return  ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrInfo() {
		return $this->_properties['ErrInfo'];
    }


    /**
     * ErrInfo
	 *
	 * @param  $ErrInfo ErrInfo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrInfo($ErrInfo) {
		$this->_properties['ErrInfo'] = $ErrInfo;
    }


    /**
     * ErrFlag
	 *
     * @return  ErrFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrFlag() {
		return $this->_properties['ErrFlag'];
    }


    /**
     * ErrFlag
	 *
	 * @param  $ErrFlag ErrFlag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrFlag($ErrFlag) {
		$this->_properties['ErrFlag'] = $ErrFlag;
    }


    /**
     * paymentAmount
	 *
     * @return  paymentAmount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getPaymentAmount() {
		return $this->_properties['paymentAmount'];
    }


    /**
     * paymentAmount
	 *
	 * @param  $paymentAmount paymentAmount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setPaymentAmount($paymentAmount) {
		$this->_properties['paymentAmount'] = $paymentAmount;
    }


    /**
     * paymenShippingCost
	 *
     * @return int paymenShippingCost
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getPaymentShippingCost() {
		return $this->_properties['paymenShippingCost'];
    }


    /**
     * paymenShippingCost
	 *
	 * @param int $paymenShippingCost paymenShippingCost
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setPaymentShippingCost($paymenShippingCost) {
		$this->_properties['paymenShippingCost'] = $paymenShippingCost;
    }


    /**
     * paymentDatetime
	 *
     * @return  paymentDatetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getPaymentDatetime() {
		return $this->_properties['paymentDatetime'];
    }


    /**
     * paymentDatetime
	 *
	 * @param  $paymentDatetime paymentDatetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Settlement.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setPaymentDatetime($paymentDatetime) {
		$this->_properties['paymentDatetime'] = $paymentDatetime;
    }
}

























