<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_customer_thanks_mail_attachment_file
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxCustomerThanksMailAttachmentFile extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'ttf';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'customerThanksMailAttachmentFileId' => null,
        'customerThanksMailId' => null,
        'number' => null,
        'fileName' => null,
        'fileNamehash' => null,
        'fileType' => null,
        'fileExtension' => null,
        'fileSize' => null,
        'errorFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * ttf_customer_thanks_mail_attachment_file_id の値を返します
	 *
     * @return int ttf_customer_thanks_mail_attachment_file_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerThanksMailAttachmentFileId() {
		return $this->_properties['customerThanksMailAttachmentFileId'];
    }


    /**
     * ttf_customer_thanks_mail_attachment_file_id の値をセットします
	 *
	 * @param int $customerThanksMailAttachmentFileId ttf_customer_thanks_mail_attachment_file_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerThanksMailAttachmentFileId($customerThanksMailAttachmentFileId) {
		$this->_properties['customerThanksMailAttachmentFileId'] = $customerThanksMailAttachmentFileId;
    }


    /**
     * ttf_customer_thanks_mail_id の値を返します
	 *
     * @return int ttf_customer_thanks_mail_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerThanksMailId() {
		return $this->_properties['customerThanksMailId'];
    }


    /**
     * ttf_customer_thanks_mail_id の値をセットします
	 *
	 * @param int $customerThanksMailId ttf_customer_thanks_mail_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerThanksMailId($customerThanksMailId) {
		$this->_properties['customerThanksMailId'] = $customerThanksMailId;
    }


    /**
     * ttf_number の値を返します
	 *
     * @return int ttf_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNumber() {
		return $this->_properties['number'];
    }


    /**
     * ttf_number の値をセットします
	 *
	 * @param int $number ttf_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNumber($number) {
		$this->_properties['number'] = $number;
    }


    /**
     * ttf_file_name の値を返します
	 *
     * @return varchar ttf_file_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileName() {
		return $this->_properties['fileName'];
    }


    /**
     * ttf_file_name の値をセットします
	 *
	 * @param varchar $fileName ttf_file_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileName($fileName) {
		$this->_properties['fileName'] = $fileName;
    }


    /**
     * ttf_file_namehash の値を返します
	 *
     * @return char ttf_file_namehash
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileNamehash() {
		return $this->_properties['fileNamehash'];
    }


    /**
     * ttf_file_namehash の値をセットします
	 *
	 * @param char $fileNamehash ttf_file_namehash
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileNamehash($fileNamehash) {
		$this->_properties['fileNamehash'] = $fileNamehash;
    }


    /**
     * ttf_file_type の値を返します
	 *
     * @return varchar ttf_file_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileType() {
		return $this->_properties['fileType'];
    }


    /**
     * ttf_file_type の値をセットします
	 *
	 * @param varchar $fileType ttf_file_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileType($fileType) {
		$this->_properties['fileType'] = $fileType;
    }


    /**
     * ttf_file_extension の値を返します
	 *
     * @return varchar ttf_file_extension
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileExtension() {
		return $this->_properties['fileExtension'];
    }


    /**
     * ttf_file_extension の値をセットします
	 *
	 * @param varchar $fileExtension ttf_file_extension
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileExtension($fileExtension) {
		$this->_properties['fileExtension'] = $fileExtension;
    }


    /**
     * ttf_file_size の値を返します
	 *
     * @return int ttf_file_size
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFileSize() {
		return $this->_properties['fileSize'];
    }


    /**
     * ttf_file_size の値をセットします
	 *
	 * @param int $fileSize ttf_file_size
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFileSize($fileSize) {
		$this->_properties['fileSize'] = $fileSize;
    }


    /**
     * ttf_error_flag の値を返します
	 *
     * @return char ttf_error_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorFlag() {
		return $this->_properties['errorFlag'];
    }


    /**
     * ttf_error_flag の値をセットします
	 *
	 * @param char $errorFlag ttf_error_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorFlag($errorFlag) {
		$this->_properties['errorFlag'] = $errorFlag;
    }


    /**
     * ttf_delete_flag の値を返します
	 *
     * @return char ttf_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * ttf_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag ttf_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * ttf_deletion_datetime の値を返します
	 *
     * @return datetime ttf_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * ttf_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime ttf_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * ttf_update_datetime の値を返します
	 *
     * @return datetime ttf_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * ttf_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime ttf_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * ttf_registration_datetime の値を返します
	 *
     * @return datetime ttf_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * ttf_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime ttf_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * ttf_update_timestamp の値を返します
	 *
     * @return timestamp ttf_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * ttf_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp ttf_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























