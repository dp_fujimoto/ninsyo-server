<?php

/**
 * @package Mikoshiva_Plugin
 */


/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';


/**
 * Mikoshiva_Plugin でエラーが発生した際に使用する例外クラス
 *
 * @package Mikoshiva_Plugin
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/10
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Plugin_Exception extends Zend_Exception {

}
