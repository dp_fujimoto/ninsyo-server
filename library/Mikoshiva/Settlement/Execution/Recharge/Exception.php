<?php
/**
 * @package Mikoshiva_Settlement
 */

/**
 * require
 */
require_once 'Mikoshiva/Exception.php';

/**
 * Mikoshiva_Settlement_Execution_Order_Exception
 *
 * @package Mikoshiva_Settlement
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Recharge_Exception extends Mikoshiva_Exception {

}
