<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Output_Payment_Change
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Payment_Change extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'ErrCode' => null,
        'ErrInfo' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * ErrCode
	 *
     * @return  ErrCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrCode() {
		return $this->_properties['ErrCode'];
    }


    /**
     * ErrCode
	 *
	 * @param  $ErrCode ErrCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrCode($ErrCode) {
		$this->_properties['ErrCode'] = $ErrCode;
    }


    /**
     * ErrInfo
	 *
     * @return  ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrInfo() {
		return $this->_properties['ErrInfo'];
    }


    /**
     * ErrInfo
	 *
	 * @param  $ErrInfo ErrInfo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrInfo($ErrInfo) {
		$this->_properties['ErrInfo'] = $ErrInfo;
    }


}

























