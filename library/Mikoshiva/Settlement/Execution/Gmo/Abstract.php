<?php
/** @package Mikoshiva_Settlement_ */

/**
 * GMO 関連クラスで使用する Abstract クラス
 * このクラスでは主に通信処理のみを記述しています
 *
 * @package Mikoshiva_Settlement_
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Settlement_Execution_Gmo_Abstract {

    /**
     * 接続RETRY回数
     *
     * @var unknown_type
     */
    const CONNECTION_RETRY_COUNT = 3;


    /**
     * Popo_Abstract
     * @var Popo_Abstract
     */
    protected $_inputPopo;


    /**
     * Popo_Abstract
     * @var Popo_Abstract
     */
    protected $_outputPopo;


    /**
     * GMO の設定リスト
     * @var array GMO の設定リスト
     */
    protected $_gmoConfig;


    /**
     * GMO 接続先 URL
     * @var string GMO 接続先 URL
     */
    protected $_gmoConnectioniUrl;


    /**
     * コンストラクタ
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/01
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);


        //---------------------------------------------
        // ■GMO コンフィグをセット
        //---------------------------------------------
        $GMO_CONFIG = array();
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG'); //GMOのマッピングデータを取得
        $this->_gmoConfig = $GMO_CONFIG[GMO_FLAG];


        //---------------------------------------------
        // ■input クラスをセット
        //---------------------------------------------
        $this->_inputPopo = $inputPopo;


        // ログ終了
        logInfo(LOG_END);
    }


    /**
     * 初期化処理
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {

    }


    /**
     * GMO への POST 処理
     *
     * @return boolean true:成功|false:失敗
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/01
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function execute() {


        // ログ開始
        logInfo(LOG_START);


        //---------------------------------------------------
        // ■初期化
        //---------------------------------------------------
        $this->init($this->_inputPopo, $this->_gmoConfig);
//exit;

        //---------------------------------------------------
        // ■必須パラメータのチェックを行います
        //---------------------------------------------------
        if (! $this->_checkParameter($this->_inputPopo)) {
            include_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】必須パラメータチェックでエラーになりました。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }
//exit;

        //---------------------------------------------------
        //■インスタンスを生成
        //---------------------------------------------------
        include_once 'Zend/Http/Client.php';
        $client = new Zend_Http_Client($this->_gmoConnectioniUrl,
                                       array('adapter'      => 'Zend_Http_Client_Adapter_Socket',
                                             'ssltransport' => 'ssl',
                                             'timeout'      => 60));
//umper($client);//exit;

        //---------------------------------------------------
        //■POST するデータをセット
        //---------------------------------------------------
        $this->_setPostData($client, $this->_inputPopo);
//dumper($client);exit;


        //---------------------------------------------------
        //■実行
        //---------------------------------------------------
        $connectionRetryCount = self::CONNECTION_RETRY_COUNT;
        $connectionRetryFlag  = '0';
        $myClassName          = get_class($this);
        for ($i = 1; $i < $connectionRetryCount + 1; $i++) {
            try {
                // ▼接続
                $response = $client->request('POST');

                // ▼リトライフラグが立っていたら正常終了したことを報告
                if ($connectionRetryFlag === '1') {
                    reportMail("{$i}/{$connectionRetryCount}回目-GMO との接続に成功しました。{$myClassName}", "{$i}/{$connectionRetryCount}回目-GMO との接続に成功しました。\n{$myClassName}");
                }

                // ▼正常に処理が終わったならループから抜ける
                break;
            } catch (Exception $e) {
                // 接続RETRY回数に達したら例外にする
                if ($i === ($connectionRetryCount)) {
                    throw $e;
                }
            }

            // ▼リトライフラグを立てる
            $connectionRetryFlag = '1';

            // ▼リトライすることを報告する
            // reportMail("{$connectionRetryCount}/{$i}回目-GMO との接続に失敗したためリトライを行います。", $body);
            errorMail($e, "{$i}/{$connectionRetryCount}回目-GMO との接続に失敗したためリトライを行います。{$myClassName}");
        }


        //---------------------------------------------------
        //■レスポンスが取得出来なければ例外を投げる
        //---------------------------------------------------
        if (! isset($response)) {
            include_once 'Mikoshiva/Settlement/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】GMO サーバーとの通信においてレスポンスを取得出来ませんでした。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Exception($message);
        }


        // スタータス200(OK)でない場合
        if ($response->getStatus() !== 200) {
            include_once 'Mikoshiva/Settlement/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】GMO サーバーとの通信に失敗しました。ステータスコード：（{$response->getStatus()}）";
            logDebug($message);
            throw new Mikoshiva_Settlement_Exception($message);
        }


        //---------------------------------------------------
        //■output クラスに詰める
        //---------------------------------------------------
        parse_str(trim($response->getBody()), $receiveData);
        $this->setOutput($this->_outputPopo, $receiveData);


        //---------------------------------------------------
        //■処理結果
        //---------------------------------------------------
        $ret = true;
        if (array_key_exists('ErrCode', $receiveData)) $ret = false;


        // ログ終了
        logInfo(LOG_END);
        return $ret;
    }

    /**
     * 文字列を暗号化します
     *
     * @param  string $data 暗号化対象文字列
     * @return string       暗号化済み文字列
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function encode($data) {
        logInfo(LOG_START);
        if(!isset($data)) {
            //データなし
            logInfo(LOG_END);
            return '';
        }


        $result = array();
        $error = '';
        exec($this->_gmoConfig[GMO_CONFIG_REAL_NAME]['MASK_PGM'] . ' -e "' . $data . '"', $result, $error);
        if($error) {
            //エンコードコマンド実行失敗
            logInfo(LOG_END);
            return '';
        }
        logInfo(LOG_END);
        return $result[0];
   }



    /**
     * 暗号化された文字列をデコードします
     *
     * @param  string $data 暗号化済み文字列
     * @return string       デコード済み文字列
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function decode($data) {
        logInfo(LOG_START);
        if(!isset($data)) {
            //データなし
            logInfo(LOG_END);
            return '';
        }


        $result = array();
        $error = '';
        exec($this->_gmoConfig[GMO_CONFIG_REAL_NAME]['MASK_PGM'] . ' -d ' . $data, $result, $error);
        if($error) {
            //デコードコマンド実行失敗
            logInfo(LOG_END);
            return '';
        }
        logInfo(LOG_END);
        return $result[0];

    }



    /**
     * input クラスのセッター
     *
     * @param Popo_Abstract $input
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setInput(Popo_Abstract $inputPopo) {
        $this->_inputPopo = $inputPopo;
    }


    /**
     * GMO 通信クラスを返します
     *
     * @return Popo_Abstract
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getInput() {
        return $this->_inputPopo;
    }


    /**
     * GMO との通信結果を返します
     *
     * @return Popo_Abstract
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/18
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_outputPopo;
    }


    /**
     * Output クラスにセットします
     *
     * @param Popo_Abstract $outputPopo
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setOutput(Popo_Abstract $outputPopo, $data) {


        // ログ開始
        logInfo('LOG_START');


        $output = null;
        $output = Mikoshiva_Utility_Copy::copyToPopo($outputPopo, $data);

        //
        $this->_outputPopo = $output;


        // ログエンド
        logInfo('LOG_END');
    }


    /**
     * GMO に POST するパラメータをセットします
     *
     * @param Popo_Abstract $popo GMO にセットする値を保持した POPO
     *
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected function _setPostData(Zend_Http_Client $client, Popo_Abstract $popo) {


        // ログ開始
        logInfo('LOG_START');


        // セット
        foreach ($popo->getProperties() as $k => $v) {
            if (! isBlankOrNull($v)) $client->setParameterPost($k, $v);
        }


        // ログ終了
        logInfo('LOG_END');
    }


    /**
     * 必須パラメータのチェックを行います
     *
     * @return boolean
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    abstract protected function _checkParameter(Popo_Abstract $inputPopo);
}









