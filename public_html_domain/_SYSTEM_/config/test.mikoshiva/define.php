<?php



/**
 * 外部ドメイン用、定数定義ファイル（mikoshiva）
 *
 * 【説明】
 *
 * 【概要】
 *
 * 【メモ】
 *
 *
 *【変更履歴】
 * 2011-02-14 谷口司 作成
 */



//------------------------------------------------------------------
// ■０１．パス
//------------------------------------------------------------------
define('SYSTEM_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/_SYSTEM_/');
define('LIBRARY_PATH' , SYSTEM_ROOT . '/library/');
define('CONFIG_PATH'  , SYSTEM_ROOT . '/config/');
define('PEAR_PATH'    , LIBRARY_PATH . 'PEAR/');


//------------------------------------------------------------------
// ■０２．ドメイン（起動ドメインを定義します）
//------------------------------------------------------------------
// ▼設定
if (strncmp(PHP_OS, 'WIN', 3) === 0) define('_MIKOSHIVA_DOMAIN_', 'test.mikoshiva');         // ローカル
else                                 define('_MIKOSHIVA_DOMAIN_', 'test.mikoshiva.com'); // 本番


//------------------------------------------------------------------
// ■０３．HTTPS かどうかで URL 定義を切り替える
//------------------------------------------------------------------
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    // ◆--- https の場合 ---◆
    define('MIKOSHIVA_URL',       'https://' . _MIKOSHIVA_DOMAIN_); // URL
    define('MIKOSHIVA_ORDER_URL', MIKOSHIVA_URL . '/order/order/'); // オーダー URL
} else {
    // ◆--- http の場合 ---◆
    define('MIKOSHIVA_URL',       'http://' . _MIKOSHIVA_DOMAIN_);  // URL
    define('MIKOSHIVA_ORDER_URL', MIKOSHIVA_URL . '/order/order/'); // オーダー URL
}


//------------------------------------------------------------------
// ■０４．ini_set
//------------------------------------------------------------------
ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . LIBRARY_PATH . PATH_SEPARATOR . PEAR_PATH);


//------------------------------------------------------------------
// ■０５．各種定数
//------------------------------------------------------------------
define('OD_TOKUSHOHO_URL',              'https://www.securepayment.jp/os2/DStokushoho.html'); // 特商法URL
define('OD_PRIVACY_URL',                'https://www.securepayment.jp/os2/DSprivacy.html');   // プライバシーポリシーURL
define('OD_SUPPORT_URL',                'https://www.123marketing.jp/support/');              // サポートURL
define('OD_HANDLING_PERSONAL_INFO_URL', 'https://www.d-publishing.jp/privacy/MKprivacy.php'); // 個人情報取り扱いURL


























