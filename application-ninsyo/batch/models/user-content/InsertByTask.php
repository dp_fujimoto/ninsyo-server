<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Mikoshiva/Batch/Exception.php';
require_once 'Mikoshiva/Db/Simple.php';

// プログレスバー関連クラス
require_once 'Zend/ProgressBar.php';
require_once 'Zend/ProgressBar/Adapter/Console.php';

require_once 'batch/models/classes/user-content/InsertByTaskReportManager.php';
require_once 'api/models/Operation.php';


/**
 *
 * @author      fujimoto <fujimoto@d-publishing.jp>
 * @since       2012/07/09
 */
class Batch_Models_UserContent_InsertByTask extends Mikoshiva_Controller_Model_Db_Abstract {


    private $_reportClass;

    private $_taskDirPath = '/api/insert-user-content-task';
    private $_endTaskDirPath = '/api/insert-user-content-end-task';


    /**
     * メインメソッド
     *
     * @param  Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param  Mikoshiva_Controller_Mapping_ActionContext  $context
     * @return boolean
     * @see    Mikoshiva_Controller_Model_Abstract::execute()
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm, Mikoshiva_Controller_Mapping_ActionContext $context) {

        // 必要
        ini_set('memory_limit',512000000);

        // logファイル名
        if (isBlankOrNull($actionForm->getLogFileName())) {
            $actionForm->setLogFileName('InsertByTask');
        }

        //------------------------------------------------------------------
        //
        // ■レポートクラスを生成
        //
        //------------------------------------------------------------------
        $this->_reportClass = Batch_Models_Classes_UserContent_InsertByTaskReportManager::getInstance();
        $this->_reportClass->setLogFileName($actionForm->getLogFileName());

        //------------------------------------------------------------------
        //
        // ■ログに開始のアナウンスを書き込む
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('#######################################################################');
        $this->_reportClass->log('#   ▼Batch_Models_UserContent_InsertByTask                    START▼   #');
        $this->_reportClass->log('#######################################################################');


        //------------------------------------------------------------------
        //
        // ■処理時間を計測
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('■処理時間を計測');
        $this->_reportClass->setStockProcessTimeStart(get_class($this));


        //------------------------------------------------------------------
        //
        // ■リクエストの値を取得
        //
        //------------------------------------------------------------------


        //------------------------------------------------------------------
        //
        // ■初期化
        //
        //------------------------------------------------------------------
        $filePathList = array();
        $updateServerNameList = array();
        $config = $context->getConfig();

        $date = new DateTime();
        $date->modify('-5 day'); // 2012-11-21 とりあえず休日中の停止をなくすため５日に設定
        $yesterdayDatetime = $date->format('Y-m-d') . ' 00:00:00';

        $nowDatetime = Mikoshiva_date::mikoshivaNow();

        // 共通クラス
        $class = new Api_Models_Operation();

        //------------------------------------------------------------------
        //
        // ■リクエストの情報をログに書込
        //
        //------------------------------------------------------------------
        foreach ($actionForm->toArrayProperty() as $k => $v) {
            $this->_reportClass->log($k . ' = ' . $v);
        }

        //------------------------------------------------------------------
        // ■プログレスバー作成
        //------------------------------------------------------------------
        $adapter     = new Zend_ProgressBar_Adapter_Console(); // アダプタの作成
        $adapter->setElements(array(
            Zend_ProgressBar_Adapter_Console::ELEMENT_PERCENT,
            Zend_ProgressBar_Adapter_Console::ELEMENT_BAR,
            Zend_ProgressBar_Adapter_Console::ELEMENT_ETA,
            Zend_ProgressBar_Adapter_Console::ELEMENT_TEXT)
        );

        //-----------------------------------------------
        // ■タスクファイルリスト取得
        //-----------------------------------------------
        $resDir = opendir(UPLOADS_DIR . $this->_taskDirPath);

        //ディレクトリ内のファイル名を１つずつを取得
        while ($fileName = readdir($resDir)) {
            //取得したファイル名を表示

            if (strstr($fileName,'insertUserContentTask') !== false) {
                $filePathList[$fileName] =  UPLOADS_DIR . $this->_taskDirPath  . '/' . $fileName;
                $fileNameArr = explode('_',$fileName);
                $updateServerNameList[$fileNameArr[0]] = 1;
            }
        }
        closedir($resDir);
        asort($filePathList);

        //------------------------------------------------------------------
        // ■ファイル内容を処理
        //------------------------------------------------------------------
        foreach ($filePathList as $fileName => $filePath) {
            $this->_reportClass->log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
            $this->_reportClass->log($fileName . 'の処理開始');
            $this->_reportClass->setStockProcessTimeEnd(get_class($this));
            $this->_reportClass->log($this->_reportClass->logStockProcessTimeDifference(get_class($this)));
            $userContentList = array();

            $fp = null;
            $fp = fopen($filePath,'r');

            if (!isBlankOrNull($fp)) {
                while($row = fgets($fp)) {
                    $rowArr = explode(',',$row);
                    if (count($rowArr) > 1) {
                        $hash = $rowArr[0];
                        $userContentList[$hash] = str_replace($hash,'',$row);
                    }
                }
                fclose($fp);
                $this->_reportClass->fileList[] = $fileName . ' ----- ' . count($userContentList) . '件';

                $this->_reportClass->log(count($userContentList) . '件のユーザーコンテンツリスト抽出');
                $res = $class->insertUserContentDetail($userContentList, '1', $config['NINSYO_SERVER_NAME_LIST']);

                $this->_reportClass->log('■ユーザーコンテンツ登録結果');
                $this->_reportClass->log($res['all']);
                $this->_reportClass->log('■ユーザーコンテンツ登録エラー結果');
                $this->_reportClass->log($res['error']);

                // 移動
                $command = "cd " . UPLOADS_DIR . $this->_taskDirPath . ";";
                $command .= "mv " . $fileName . " " . UPLOADS_DIR . $this->_endTaskDirPath . ";";
                $command .= "cd " . UPLOADS_DIR . $this->_endTaskDirPath . ";";
                $command .= "mv " . $fileName . " " . $fileName . ".txt;";
                exec($command);
            }
        }

        $this->_reportClass->log('■更新されていないユーザー削除');
        foreach ($updateServerNameList as $serverName => $noReasonFlag) {
            $this->_reportClass->log('対象サーバー名：' . $serverName);
            $this->_reportClass->log('削除対象日時：' . $yesterdayDatetime . 'より前が最終更新日');
            $this->_reportClass->setStockProcessTimeEnd(get_class($this));
            $this->_reportClass->log($this->_reportClass->logStockProcessTimeDifference(get_class($this)));

            try {
                //-----------------------------------------------
                // 更新されていないユーザーを削除
                //-----------------------------------------------
                // SQL作成
                $sql = "";
                $sql .= "SELECT ";
                $sql .= "  mus_user_id";
                $sql .= "  ,mus_id_hash";
                $sql .= "  ,mus_password_hash";
                $sql .= "  ,muc_user_content_id";
                $sql .= " FROM mst_user";
                $sql .= " LEFT JOIN mst_user_content ON mus_user_id = muc_user_id AND muc_delete_flag = '0'";
                $sql .= " WHERE 1 = 1";
                $sql .= "   AND mus_delete_flag = '0'";
                $sql .= "   AND mus_update_datetime < ?";
                $sql .= "   AND mus_server_name = ?";

                $params   = array();
                $params[] = $yesterdayDatetime;
                $params[] = $serverName;

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                $deleteUserIds = array();
                $deleteUserContentIds = array();
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    if (!isset($deleteUserIds[$v['mus_user_id']])) {
                        //-----------------------------------------------
                        // ユーザー削除
                        //-----------------------------------------------
                        $where = array();
                        $where['userId']   = $v['mus_user_id'];
                        $where['deleteFlag'] = '1';
                        $where['deletionDatetime'] = $nowDatetime;

                        $db = null;
                        $db = new Mikoshiva_Db_Simple();
                        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUser', $where);
                        $deleteUserIds[$v['mus_user_id']] = $v['mus_user_id'];

                        $this->_reportClass->log('ユーザーID：' . $v['mus_user_id']);

                        //-----------------------------------------------
                        // キャッシュファイル削除
                        //-----------------------------------------------
                        if (file_exists(USER_CONTENT_XML_FILES . '/' . $v['mus_id_hash'] . $v['mus_password_hash'])) {
                            unlink(USER_CONTENT_XML_FILES . '/' . $v['mus_id_hash'] . $v['mus_password_hash']);
                        }
                    }

                    if (!isBlankOrNull($v['muc_user_content_id'])) {
                        //-----------------------------------------------
                        // ユーザーコンテンツ削除
                        //-----------------------------------------------
                        $where = array();
                        $where['userContentId']   = $v['muc_user_content_id'];
                        $where['deleteFlag'] = '1';
                        $where['deletionDatetime'] = $nowDatetime;

                        $this->_reportClass->log('ユーザーコンテンツID：' . $v['muc_user_content_id']);

                        $db = null;
                        $db = new Mikoshiva_Db_Simple();
                        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                        $deleteUserContentIds[$v['muc_user_content_id']] = $v['muc_user_content_id'];
                    }
                }

                $this->_reportClass->deleteUserNum += count($deleteUserIds);
                $this->_reportClass->deleteUserContentNum += count($deleteUserContentIds);
            } catch (Exception $e) {
                $this->_reportClass->log('削除時exceptionエラー');
            }
            $this->_reportClass->log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
        }


        //-----------------------------------------------
        // ■古いタスクファイル削除
        //-----------------------------------------------
        $this->_reportClass->setStockProcessTimeEnd(get_class($this));
        $this->_reportClass->log($this->_reportClass->logStockProcessTimeDifference(get_class($this)));

        $date = new DateTime();
        $date->modify('-10 day');
        $yesterdayDatetime = $date->format('Y-m-d') . ' 00:00:00';

        $resDir = opendir(UPLOADS_DIR . $this->_endTaskDirPath);

        //ディレクトリ内のファイル名を１つずつを取得
        while ($fileName = readdir($resDir)) {
            //取得したファイル名を表示

            if (strstr($fileName,'insertUserContentTask') !== false) {
                $filePath =  UPLOADS_DIR . $this->_endTaskDirPath  . '/' . $fileName;
                $mod = filemtime($filePath);
                $fileUpdateDatetime = date("Y-m-d H:i:s",$mod);
                if ($fileUpdateDatetime < $yesterdayDatetime) {
                    unlink($filePath);
                }
            }
        }
        closedir($resDir);

        //------------------------------------------------------------------
        //
        // ■処理時間を計測（終了）
        //
        //------------------------------------------------------------------
        $this->_reportClass->setStockProcessTimeEnd(get_class($this));
        $this->_reportClass->log($this->_reportClass->logStockProcessTimeDifference(get_class($this)));


        //------------------------------------------------------------------
        //
        // ■リポートメール送信
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('■リポートメール送信');
        $this->_reportClass->sendReportMail(Mikoshiva_date::mikoshivaNow('yyyy-MM-dd'));


        //-----------------------------------------------
        // ユーザーキャッシュファイル削除
        //-----------------------------------------------
        $operation = new Api_Models_Operation();
        $operation->deleteUserCacheFile($actionForm, $context);

        file_get_contents(DELETE_USER_CACHE_FILE_API);

        //------------------------------------------------------------------
        //
        // ■ログに終了のアナウンスを書き込む
        //
        //------------------------------------------------------------------
        $this->_reportClass->log('############################################################');
        $this->_reportClass->log('#  ▲Batch_Models_UserContent_InsertByTask            END▲   #');
        $this->_reportClass->log('############################################################');

        return true;
    }

}