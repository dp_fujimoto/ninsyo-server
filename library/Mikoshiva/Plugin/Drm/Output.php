<?php
require_once 'Popo/Abstract.php';

/**
 * DRMAPIの実行結果を返却します。
 *
 * @author k.shimowaki
 * @since version - 2010/05/20
 */
class Mikoshiva_Plugin_Drm_Output extends Popo_Abstract {

	public $result = null;
	public $errorCode = null;
	public $errorMessage = null;
	public $execTime = null;

	/**
	 * setter
	 *
	 * @param $errorCode
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function setErrorCode($errorCode) {
		$this->errorCode = $errorCode;
	}

	/**
	 * getter
	 *
	 * @return $errorCode
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function getErrorCode() {
		return $this->errorCode;
	}

	/**
	 * setter
	 *
	 * @param $errorMessage
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function setErrorMessage($errorMessage) {
		$this->errorMessage = $errorMessage;
	}

	/**
	 * getter
	 *
	 * @return $errorMessage
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function getErrorMessage() {
		return $this->errorMessage;
	}

	/**
	 * setter
	 *
	 * @param $execTime
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function setExecTime($execTime) {
		$this->execTime = $execTime;
	}

	/**
	 * getter
	 *
	 * @return $execTime
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function getExecTime() {
		return $this->execTime;
	}

	/**
	 * setter
	 *
	 * @param $result
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function setResult($result) {
		$this->result = $result;
	}

	/**
	 * getter
	 *
	 * @return $result
	 * @since 2010/05/20
	 * @version SVN:$Id:
	 **/
	public function getResult() {
		return $this->result;
	}

}