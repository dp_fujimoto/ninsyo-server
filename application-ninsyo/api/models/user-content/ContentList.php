<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CreateUserHash.php';
require_once 'api/models/classes/CheckSessionId.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_ContentList extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 検索結果
        $output = array();

        $class = new Api_Models_Classes_CheckSessionId();
        $class->execute($actionForm->getSessionId());

        $class = new Api_Models_Classes_CreateUserHash();
        $idHash = $class->execute($actionForm->getId());
        $passwordHash = $class->execute($actionForm->getPassword());


        // アプリケーションナンバーからタイプ取得
        $appNum = 1;
        $config = $context->getConfig();
        $count = 0;
        $applicationType = '';
        foreach ($config['SHORT_CODE_MAP_LIST']['application_type'] as $k => $v) {
            $count++;
            if ($count === $appNum) {
                $applicationType = $k;
                break;
            }
        }

        // xmlが存在する場合、そちらを返す
        $hash = $applicationType . $idHash . $passwordHash;
        $actionForm->setHash($hash);
        if (file_exists(USER_CONTENT_XML_FILES . '/' . $hash)) {
            $content = file_get_contents(USER_CONTENT_XML_FILES . '/' . $hash);
            return $content;
        }

        //-----------------------------------------------
        // ユーザー確認
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT ";
        $sql .= "  mbr_banner_sideways_update_datetime,";
        $sql .= "  mbr_banner_sideways_url,";
        $sql .= "  mbr_banner_vertical_update_datetime,";
        $sql .= "  mbr_banner_vertical_url,";
        $sql .= "  mbr_brand_code,";
        $sql .= "  mbr_brand_id,";
        $sql .= "  mbr_brand_name,";
        $sql .= "  mbr_description,";
        $sql .= "  mbr_logo_update_datetime,";
        $sql .= "  mbr_logo_url,";
        $sql .= "  mct_content_id,";
        $sql .= "  mct_content_name,";
        $sql .= "  mct_description,";
        $sql .= "  mct_image_update_datetime,";
        $sql .= "  mct_image_url,";
        $sql .= "  mct_media_type,";
        $sql .= "  mct_media_update_datetime,";
        $sql .= "  mct_media_url,";
        $sql .= "  mct_publication_date,";
        $sql .= "  mpr_description,";
        $sql .= "  mpr_image_update_datetime,";
        $sql .= "  mpr_image_url,";
        $sql .= "  mpr_product_id,";
        $sql .= "  mpr_product_name";
        $sql .= " FROM mst_user";
        $sql .= " INNER JOIN mst_user_content ON mus_user_id  = muc_user_id";
        $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mus_delete_flag = '0'";
        $sql .= "   AND muc_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND muc_auth_flag = '1'";
        $sql .= "   AND mus_id_hash = ?";
        $sql .= "   AND mus_password_hash = ?";
        $sql .= "   AND mct_publication_date <= ?";
        $sql .= "   AND mct_application_type = ?";
        $sql .= " GROUP BY mct_content_id";

        $params   = array();
        $params[] = $idHash;
        $params[] = $passwordHash;
        $params[] = Mikoshiva_date::mikoshivaNow('yyyy-MM-dd');
        $params[] = $applicationType;

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            if (!isset($output[$v['mbr_brand_name']])) {
                $output[$v['mbr_brand_name']] = array();
                $output[$v['mbr_brand_name']]['des'] = $v['mbr_description'];
                $output[$v['mbr_brand_name']]['code'] = $v['mbr_brand_code'];
                $output[$v['mbr_brand_name']]['logo'] = $v['mbr_logo_url'];
                $output[$v['mbr_brand_name']]['logoKey'] = 'bl' . $v['mbr_brand_id'] . 't' . $this->makeDatetimeStr($v['mbr_logo_update_datetime']);
                $output[$v['mbr_brand_name']]['vertical'] = $v['mbr_banner_vertical_url'];
                $output[$v['mbr_brand_name']]['verticalKey'] = 'bv' . $v['mbr_brand_id'] . 't' . $this->makeDatetimeStr($v['mbr_banner_vertical_update_datetime']);
                $output[$v['mbr_brand_name']]['sideways'] = $v['mbr_banner_sideways_url'];
                $output[$v['mbr_brand_name']]['sidewaysKey'] = 'bs' . $v['mbr_brand_id'] . 't' . $this->makeDatetimeStr($v['mbr_banner_sideways_update_datetime']);
                $output[$v['mbr_brand_name']]['data'] = array();
            }
            if (!isset($output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']] )) {
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']] = array();
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['des'] = $v['mpr_description'];
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['image'] = $v['mpr_image_url'];
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['imageKey'] = 'pi' . $v['mpr_product_id'] . 't' . $this->makeDatetimeStr($v['mpr_image_update_datetime']);
                $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['data']  = array();
            }

            $tempArr = array();
            $tempArr['image']     = $v['mct_image_url'];
            $tempArr['imageKey']  = 'ci' . $v['mct_content_id'] . 't' . $this->makeDatetimeStr($v['mct_image_update_datetime']);
            $tempArr['media']     = $v['mct_media_url'];
            $tempArr['mediaKey']  = 'cm' . $v['mct_content_id'] . 't' . $this->makeDatetimeStr($v['mct_media_update_datetime']);
            $tempArr['mediaType'] = $v['mct_media_type'];
            $tempArr['des']       = $v['mct_description'];
            $tempArr['date']      = $v['mct_publication_date'];

            $output[$v['mbr_brand_name']]['data'][$v['mpr_product_name']]['data'][$v['mct_content_name']] = $tempArr;
        }

        // 自然順ソート
        uksort($output, 'strnatcmp');
        foreach ($output as $k => $v) {
            uksort($output[$k]['data'], 'strnatcmp');
            foreach ($v['data'] as $k2 => $v2) {
                uksort($output[$k]['data'][$k2]['data'], 'strnatcmp');
            }
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }

    private function makeDatetimeStr($datetime) {
        $str = str_replace(' ','',$datetime);
        $str = str_replace(':','',$str);
        $str = str_replace('-','',$str);
        return $str;
    }

}
