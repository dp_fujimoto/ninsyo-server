<?php
include '../api/define-api.php';

//------------------------------------------------------------------
// WEB認証
//------------------------------------------------------------------
$url = '/api/user-content/get-registration-date7';

$logStr = date('Y-m-d H:i:s') . ' ';
foreach ($_POST as $k => $v) {
    $logStr .= $k . ':' . $v . ',';
}
$post = array();
$post['id'] = '';
if (isset($_POST['id'])) {
    $post['id'] = $_POST['id'];
}
$post['password'] = '';
if (isset($_POST['password'])) {
    $post['password'] = $_POST['password'];
}
$post['productCode'] = '';
if (isset($_POST['key'])) {
    $post['productCode'] = $_POST['key'];
}

if ($post['id'] === 'admin') {
    $url = '/api/user-content/get-registration-date7-admin';
}
try {
    file_put_contents(LOG_TEMPORARY_PATH . '/web/authing7_' . date('Y-m-d') . '.log',$logStr . "\n",FILE_APPEND);
} catch (Exception $e) {}
echo send_socket($url, $post, $_SERVER);
