<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_builder_order
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxBuilderOrder extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tbo';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'builderOrderId' => null,
        'builderId' => null,
        'builderTraderId' => null,
        'orderPrice' => null,
        'orderNumber' => null,
        'orderDatetime' => null,
        'expectedDatetime' => null,
        'deliveryNumber' => null,
        'deliveryDatetime' => null,
        'staffId' => null,
        'deliveryStatus' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 部材発注ID の値を返します
	 *
     * @return int 部材発注ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderOrderId() {
		return $this->_properties['builderOrderId'];
    }


    /**
     * 部材発注ID の値をセットします
	 *
	 * @param int $builderOrderId 部材発注ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderOrderId($builderOrderId) {
		$this->_properties['builderOrderId'] = $builderOrderId;
    }


    /**
     * 部材ID の値を返します
	 *
     * @return int 部材ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderId() {
		return $this->_properties['builderId'];
    }


    /**
     * 部材ID の値をセットします
	 *
	 * @param int $builderId 部材ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderId($builderId) {
		$this->_properties['builderId'] = $builderId;
    }


    /**
     * 部材業者ID の値を返します
	 *
     * @return int 部材業者ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBuilderTraderId() {
		return $this->_properties['builderTraderId'];
    }


    /**
     * 部材業者ID の値をセットします
	 *
	 * @param int $builderTraderId 部材業者ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBuilderTraderId($builderTraderId) {
		$this->_properties['builderTraderId'] = $builderTraderId;
    }


    /**
     * 発注金額 の値を返します
	 *
     * @return int 発注金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderPrice() {
		return $this->_properties['orderPrice'];
    }


    /**
     * 発注金額 の値をセットします
	 *
	 * @param int $orderPrice 発注金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderPrice($orderPrice) {
		$this->_properties['orderPrice'] = $orderPrice;
    }


    /**
     * 発注数 の値を返します
	 *
     * @return int 発注数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderNumber() {
		return $this->_properties['orderNumber'];
    }


    /**
     * 発注数 の値をセットします
	 *
	 * @param int $orderNumber 発注数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderNumber($orderNumber) {
		$this->_properties['orderNumber'] = $orderNumber;
    }


    /**
     * 発注日 の値を返します
	 *
     * @return datetime 発注日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderDatetime() {
		return $this->_properties['orderDatetime'];
    }


    /**
     * 発注日 の値をセットします
	 *
	 * @param datetime $orderDatetime 発注日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderDatetime($orderDatetime) {
		$this->_properties['orderDatetime'] = $orderDatetime;
    }


    /**
     * 納品予定日 の値を返します
	 *
     * @return datetime 納品予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExpectedDatetime() {
		return $this->_properties['expectedDatetime'];
    }


    /**
     * 納品予定日 の値をセットします
	 *
	 * @param datetime $expectedDatetime 納品予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExpectedDatetime($expectedDatetime) {
		$this->_properties['expectedDatetime'] = $expectedDatetime;
    }


    /**
     * 納品数 の値を返します
	 *
     * @return int 納品数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeliveryNumber() {
		return $this->_properties['deliveryNumber'];
    }


    /**
     * 納品数 の値をセットします
	 *
	 * @param int $deliveryNumber 納品数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeliveryNumber($deliveryNumber) {
		$this->_properties['deliveryNumber'] = $deliveryNumber;
    }


    /**
     * 納品日 の値を返します
	 *
     * @return datetime 納品日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeliveryDatetime() {
		return $this->_properties['deliveryDatetime'];
    }


    /**
     * 納品日 の値をセットします
	 *
	 * @param datetime $deliveryDatetime 納品日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeliveryDatetime($deliveryDatetime) {
		$this->_properties['deliveryDatetime'] = $deliveryDatetime;
    }


    /**
     * 発注者 の値を返します
	 *
     * @return int 発注者
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * 発注者 の値をセットします
	 *
	 * @param int $staffId 発注者
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * 発注ステータス の値を返します
	 *
     * @return varchar 発注ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeliveryStatus() {
		return $this->_properties['deliveryStatus'];
    }


    /**
     * 発注ステータス の値をセットします
	 *
	 * @param varchar $deliveryStatus 発注ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeliveryStatus($deliveryStatus) {
		$this->_properties['deliveryStatus'] = $deliveryStatus;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























