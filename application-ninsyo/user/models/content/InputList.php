<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/Operation.php';
require_once 'tool/models/Operation.php';

/**
 * 指定したコンテンツIDの情報を入力画面にプリセットする。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class User_Models_Content_InputList
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したコンテンツIDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $output = '';
        $contentList = array();
        $list = array();
        $contentId = $actionForm->getContentId();
        $startDate = $actionForm->getStartDate();

        if (!isBlankOrNull($startDate)) {
            $tempArr = explode('-',$startDate);
            if (count($tempArr) !== 3 || !checkdate($tempArr[1],$tempArr[2],$tempArr[0])) {
                return '会員開始日が不正';
            }
        }

        if (!isBlankOrNull($contentId)) {

            //-----------------------------------------------
            //
            // コンテンツ検索
            //
            //-----------------------------------------------
            // SQL作成
            $sql = "";
            $sql .= "SELECT";
            $sql .= "    mbr_brand_name";
            $sql .= "   ,mpr_product_name";
            $sql .= "   ,mct_content_name";
            $sql .= "   ,mct_content_id";
            $sql .= " FROM mst_content";
            $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
            $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
            $sql .= " WHERE 1 = 1 ";
            $sql .= "   AND mct_delete_flag = '0'";
            $sql .= "   AND mbr_delete_flag = '0'";
            $sql .= "   AND mpr_delete_flag = '0'";
            $sql .= "   AND mct_content_id IN (?)";

            $params = array();
            $params[] = explode(',',$contentId);

            $sql .= " ORDER BY mbr_brand_name, mpr_product_name, mct_content_name";

            // SQL実行＋全件取得
            $res = array();
            for ($i = 0; $i < count($params); $i++) {
                $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
            }
            $res = $this->_db->query($sql);

            if (!isBlankOrNull($startDate)) {
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $contentList[$v['mct_content_id']] = $v['mct_content_id'] . '|' . $startDate;
                }
            } else {
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $contentList[$v['mct_content_id']] = $v['mct_content_id'];
                }
            }

            if (count($contentList) > 0) {

                //-----------------------------------------------------
                // ▼UploadFile 操作クラスのインスタンスを生成
                //-----------------------------------------------------
                $file = null;
                $file = new Zend_File_Transfer_Adapter_Http();

                //-----------------------------------------------------
                // ▼ファイルの移動場所をセット（確認画面では一時保存ディレクトリを指定する）
                //-----------------------------------------------------
                $file->setDestination(UPLOADS_TEMP_DIR);
                $fileInfo = $file->getFileInfo(); // アップロードファイルを取得

                // ファイルがある場合
                if (isset($fileInfo['addressFile']['tmp_name']) && !isBlankOrNull($fileInfo['addressFile']['tmp_name'])) {
                    $serverName = $actionForm->getServerName();
                    $fileRowNum = 0;
                    $fileName = '';
                    $errorRow = array();

                    // 共通クラス
                    $class = new Api_Models_Operation();

                    $fp = fopen($fileInfo['addressFile']['tmp_name'],'rb');
                    $fileName = $fileInfo['addressFile']['name'];

                    while( ! feof( $fp )) {
                        $fileRowNum++;

                        //===============================================
                        // １行を分割し、各値を取り出す
                        //===============================================
                        $row = str_replace('"','',fgets( $fp ));
                        $row = str_replace("'",'',$row);
                        $buffer  = trim($row);

                        if ($buffer != "") {
                            $tempArr = explode(',',$buffer);
                            if (count($tempArr) === 2) {
                                $email = trim($tempArr[0]);
                                $password = trim($tempArr[1]);

                                if (!isBlankOrNull($email) && preg_match('/^[_a-z0-9\-\.]+[\.\+_a-z0-9\-]*@[a-z0-9\-\.]+$/i', $email)){
                                    if (!isBlankOrNull($password) && preg_match('/^[_a-z0-9\-\.]+$/i', $password)){
                                        // ok
                                        $hashArr = array();
                                        $hashArr[] = $serverName;
                                        $hashArr[] = $class->createUserHash($email);
                                        $hashArr[] = $class->createUserHash($password);

                                        $hash = implode('|',$hashArr);

                                        $list[$hash] = '';

                                    } else {
                                        $errorRow[$fileRowNum] = 'パスワードが不正';
                                    }
                                } else {
                                    $errorRow[$fileRowNum] = 'メールアドレスが不正';
                                }
                            } else {
                                $errorRow[$fileRowNum] = 'カンマの数が不正';
                            }
                        }
                    }
                    // ファイル閉じる
                    fclose($fp);

                    if (count($errorRow) > 0) {
                        $output .= implode("<br>",$errorRow) . "<br>";
                    }

                    if (count($list) > 0) {

                        foreach ($list as $k => $v) {
                            $list[$k] = implode(',',$contentList);
                        }

                        // 共通クラス
                        $config = $context->getConfig();
                        $result = $class->insertUserContentDetail($list, '0', $config['NINSYO_SERVER_NAME_LIST']);

                        //-----------------------------------------------
                        // ユーザーキャッシュファイル削除
                        //-----------------------------------------------
                        $operation = new Api_Models_Operation();
                        $operation->deleteUserCacheFile($actionForm, $context);

                        file_get_contents(DELETE_USER_CACHE_FILE_API);

                        //-----------------------------------------------
                        // アクションログ保存
                        //-----------------------------------------------
                        $logText = '';
                        if (isset($result['all'])) {
                            $logText .= $result['all'] . "\r\n";
                        }
                        if (isset($result['error']) && !isBlankOrNull($result['error'])) {
                            $logText .= $result['error'] . "\r\n";
                        }

                        $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

                        $actionForm->setDebug($logText);

                        $operation = new Tool_Models_Operation();
                        $operation->insertActionLog($logText);

                        $output .= '登録完了！';
                    } else {
                        $output .= 'ファイルから対象者を取得できませんでした。';
                    }
                } else {
                    $output .= 'ファイルが選択されていません。';
                }
            } else {
                $output .= '有効なコンテンツＩＤがありません。';
            }
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;
    }

}
