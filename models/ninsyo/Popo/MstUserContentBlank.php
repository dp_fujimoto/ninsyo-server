<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_user_content_blank
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstUserContentBlank extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mub';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'userContentBlankId' => null,
        'userContentId' => null,
        'startDate' => null,
        'endDate' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mub_user_content_blank_id の値を返します
	 *
     * @return int mub_user_content_blank_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserContentBlankId() {
		return $this->_properties['userContentBlankId'];
    }


    /**
     * mub_user_content_blank_id の値をセットします
	 *
	 * @param int $userContentBlankId mub_user_content_blank_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserContentBlankId($userContentBlankId) {
		$this->_properties['userContentBlankId'] = $userContentBlankId;
    }


    /**
     * mub_user_content_id の値を返します
	 *
     * @return int mub_user_content_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserContentId() {
		return $this->_properties['userContentId'];
    }


    /**
     * mub_user_content_id の値をセットします
	 *
	 * @param int $userContentId mub_user_content_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserContentId($userContentId) {
		$this->_properties['userContentId'] = $userContentId;
    }


    /**
     * mub_start_date の値を返します
	 *
     * @return date mub_start_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStartDate() {
		return $this->_properties['startDate'];
    }


    /**
     * mub_start_date の値をセットします
	 *
	 * @param date $startDate mub_start_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStartDate($startDate) {
		$this->_properties['startDate'] = $startDate;
    }


    /**
     * mub_end_date の値を返します
	 *
     * @return date mub_end_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEndDate() {
		return $this->_properties['endDate'];
    }


    /**
     * mub_end_date の値をセットします
	 *
	 * @param date $endDate mub_end_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEndDate($endDate) {
		$this->_properties['endDate'] = $endDate;
    }


    /**
     * mub_delete_flag の値を返します
	 *
     * @return char mub_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mub_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mub_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mub_deletion_datetime の値を返します
	 *
     * @return datetime mub_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mub_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mub_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mub_registration_datetime の値を返します
	 *
     * @return datetime mub_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mub_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mub_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mub_update_datetime の値を返します
	 *
     * @return datetime mub_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mub_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mub_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mub_update_timestamp の値を返します
	 *
     * @return timestamp mub_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mub_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mub_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























