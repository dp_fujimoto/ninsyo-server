<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CheckSessionId.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Content_DeleteConditionCacheFile extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        // 条件リストキャッシュファイル削除
        //-----------------------------------------------
        $res_dir = opendir(CONDITION_JSON_FILES);
        while ($fileName = readdir($res_dir)){
            if (!is_dir(CONDITION_JSON_FILES . '/' . $fileName)) {
                unlink(CONDITION_JSON_FILES . '/' . $fileName);
            }
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
