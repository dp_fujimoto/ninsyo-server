<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_direction
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstDirection extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mdr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'directionId' => null,
        'divisionId' => null,
        'customerId' => null,
        'productId' => null,
        'productType' => null,
        'shopId' => null,
        'proposalDatetime' => null,
        'directionBaseDate' => null,
        'continuityShippingDay' => null,
        'nextDirectionDate' => null,
        'nextStepShippingFlag' => null,
        'serviceStartDate' => null,
        'serviceStopDate' => null,
        'servicePeriodChargeFlag' => null,
        'freePeriodEndDate' => null,
        'numberSwitchDay' => null,
        'shippingNumber' => null,
        'chargeNumber' => null,
        'forceContinuityShippingFlag' => null,
        'prechargeFlag' => null,
        'launderingErrorFlag' => null,
        'launderingErrorDatetime' => null,
        'chargeErrorFlag' => null,
        'chargeErrorDatetime' => null,
        'stopFlag' => null,
        'stopDatetime' => null,
        'stopReasonType' => null,
        'subscriptionRestartDate' => null,
        'trialCancelFlag' => null,
        'subscriptionCancelFlag' => null,
        'subscriptionCancelDatetime' => null,
        'singleCancelFlag' => null,
        'singleCancelDatetime' => null,
        'endFlag' => null,
        'endDatetime' => null,
        'orderCancelFlag' => null,
        'orderCancelDatetime' => null,
        'memo' => null,
        'suddenlyYearFlag' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 指示ID の値を返します
	 *
     * @return int 指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * 指示ID の値をセットします
	 *
	 * @param int $directionId 指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 商品ID の値を返します
	 *
     * @return int 商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * 商品ID の値をセットします
	 *
	 * @param int $productId 商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * 商品タイプ の値を返します
	 *
     * @return varchar 商品タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductType() {
		return $this->_properties['productType'];
    }


    /**
     * 商品タイプ の値をセットします
	 *
	 * @param varchar $productType 商品タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductType($productType) {
		$this->_properties['productType'] = $productType;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * 申込日時 の値を返します
	 *
     * @return datetime 申込日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProposalDatetime() {
		return $this->_properties['proposalDatetime'];
    }


    /**
     * 申込日時 の値をセットします
	 *
	 * @param datetime $proposalDatetime 申込日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProposalDatetime($proposalDatetime) {
		$this->_properties['proposalDatetime'] = $proposalDatetime;
    }


    /**
     * 指示基準日 の値を返します
	 *
     * @return date 指示基準日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionBaseDate() {
		return $this->_properties['directionBaseDate'];
    }


    /**
     * 指示基準日 の値をセットします
	 *
	 * @param date $directionBaseDate 指示基準日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionBaseDate($directionBaseDate) {
		$this->_properties['directionBaseDate'] = $directionBaseDate;
    }


    /**
     * 継続発送日 の値を返します
	 *
     * @return int 継続発送日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityShippingDay() {
		return $this->_properties['continuityShippingDay'];
    }


    /**
     * 継続発送日 の値をセットします
	 *
	 * @param int $continuityShippingDay 継続発送日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityShippingDay($continuityShippingDay) {
		$this->_properties['continuityShippingDay'] = $continuityShippingDay;
    }


    /**
     * 次回指示年月日 の値を返します
	 *
     * @return date 次回指示年月日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNextDirectionDate() {
		return $this->_properties['nextDirectionDate'];
    }


    /**
     * 次回指示年月日 の値をセットします
	 *
	 * @param date $nextDirectionDate 次回指示年月日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNextDirectionDate($nextDirectionDate) {
		$this->_properties['nextDirectionDate'] = $nextDirectionDate;
    }


    /**
     * 次回ステップ発送フラグ の値を返します
	 *
     * @return char 次回ステップ発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNextStepShippingFlag() {
		return $this->_properties['nextStepShippingFlag'];
    }


    /**
     * 次回ステップ発送フラグ の値をセットします
	 *
	 * @param char $nextStepShippingFlag 次回ステップ発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNextStepShippingFlag($nextStepShippingFlag) {
		$this->_properties['nextStepShippingFlag'] = $nextStepShippingFlag;
    }


    /**
     * サービス開始日 の値を返します
	 *
     * @return date サービス開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceStartDate() {
		return $this->_properties['serviceStartDate'];
    }


    /**
     * サービス開始日 の値をセットします
	 *
	 * @param date $serviceStartDate サービス開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceStartDate($serviceStartDate) {
		$this->_properties['serviceStartDate'] = $serviceStartDate;
    }


    /**
     * サービス終了日 の値を返します
	 *
     * @return date サービス終了日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceStopDate() {
		return $this->_properties['serviceStopDate'];
    }


    /**
     * サービス終了日 の値をセットします
	 *
	 * @param date $serviceStopDate サービス終了日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceStopDate($serviceStopDate) {
		$this->_properties['serviceStopDate'] = $serviceStopDate;
    }


    /**
     * サービス期間課金フラグ の値を返します
	 *
     * @return char サービス期間課金フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServicePeriodChargeFlag() {
		return $this->_properties['servicePeriodChargeFlag'];
    }


    /**
     * サービス期間課金フラグ の値をセットします
	 *
	 * @param char $servicePeriodChargeFlag サービス期間課金フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServicePeriodChargeFlag($servicePeriodChargeFlag) {
		$this->_properties['servicePeriodChargeFlag'] = $servicePeriodChargeFlag;
    }


    /**
     * お試し期間終了日 の値を返します
	 *
     * @return date お試し期間終了日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodEndDate() {
		return $this->_properties['freePeriodEndDate'];
    }


    /**
     * お試し期間終了日 の値をセットします
	 *
	 * @param date $freePeriodEndDate お試し期間終了日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodEndDate($freePeriodEndDate) {
		$this->_properties['freePeriodEndDate'] = $freePeriodEndDate;
    }


    /**
     * 号数切替日 の値を返します
	 *
     * @return int 号数切替日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNumberSwitchDay() {
		return $this->_properties['numberSwitchDay'];
    }


    /**
     * 号数切替日 の値をセットします
	 *
	 * @param int $numberSwitchDay 号数切替日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNumberSwitchDay($numberSwitchDay) {
		$this->_properties['numberSwitchDay'] = $numberSwitchDay;
    }


    /**
     * 発送回数 の値を返します
	 *
     * @return int 発送回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingNumber() {
		return $this->_properties['shippingNumber'];
    }


    /**
     * 発送回数 の値をセットします
	 *
	 * @param int $shippingNumber 発送回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingNumber($shippingNumber) {
		$this->_properties['shippingNumber'] = $shippingNumber;
    }


    /**
     * 請求回数 の値を返します
	 *
     * @return int 請求回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeNumber() {
		return $this->_properties['chargeNumber'];
    }


    /**
     * 請求回数 の値をセットします
	 *
	 * @param int $chargeNumber 請求回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeNumber($chargeNumber) {
		$this->_properties['chargeNumber'] = $chargeNumber;
    }


    /**
     * 継続発送日強制フラグ の値を返します
	 *
     * @return char 継続発送日強制フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getForceContinuityShippingFlag() {
		return $this->_properties['forceContinuityShippingFlag'];
    }


    /**
     * 継続発送日強制フラグ の値をセットします
	 *
	 * @param char $forceContinuityShippingFlag 継続発送日強制フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setForceContinuityShippingFlag($forceContinuityShippingFlag) {
		$this->_properties['forceContinuityShippingFlag'] = $forceContinuityShippingFlag;
    }


    /**
     * 前課金フラグ の値を返します
	 *
     * @return char 前課金フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrechargeFlag() {
		return $this->_properties['prechargeFlag'];
    }


    /**
     * 前課金フラグ の値をセットします
	 *
	 * @param char $prechargeFlag 前課金フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrechargeFlag($prechargeFlag) {
		$this->_properties['prechargeFlag'] = $prechargeFlag;
    }


    /**
     * 洗替エラーフラグ の値を返します
	 *
     * @return char 洗替エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLaunderingErrorFlag() {
		return $this->_properties['launderingErrorFlag'];
    }


    /**
     * 洗替エラーフラグ の値をセットします
	 *
	 * @param char $launderingErrorFlag 洗替エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLaunderingErrorFlag($launderingErrorFlag) {
		$this->_properties['launderingErrorFlag'] = $launderingErrorFlag;
    }


    /**
     * 洗替エラー日時 の値を返します
	 *
     * @return datetime 洗替エラー日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLaunderingErrorDatetime() {
		return $this->_properties['launderingErrorDatetime'];
    }


    /**
     * 洗替エラー日時 の値をセットします
	 *
	 * @param datetime $launderingErrorDatetime 洗替エラー日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLaunderingErrorDatetime($launderingErrorDatetime) {
		$this->_properties['launderingErrorDatetime'] = $launderingErrorDatetime;
    }


    /**
     * 請求エラーフラグ の値を返します
	 *
     * @return char 請求エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeErrorFlag() {
		return $this->_properties['chargeErrorFlag'];
    }


    /**
     * 請求エラーフラグ の値をセットします
	 *
	 * @param char $chargeErrorFlag 請求エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeErrorFlag($chargeErrorFlag) {
		$this->_properties['chargeErrorFlag'] = $chargeErrorFlag;
    }


    /**
     * 請求エラー日時 の値を返します
	 *
     * @return datetime 請求エラー日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeErrorDatetime() {
		return $this->_properties['chargeErrorDatetime'];
    }


    /**
     * 請求エラー日時 の値をセットします
	 *
	 * @param datetime $chargeErrorDatetime 請求エラー日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeErrorDatetime($chargeErrorDatetime) {
		$this->_properties['chargeErrorDatetime'] = $chargeErrorDatetime;
    }


    /**
     * 停止フラグ の値を返します
	 *
     * @return char 停止フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopFlag() {
		return $this->_properties['stopFlag'];
    }


    /**
     * 停止フラグ の値をセットします
	 *
	 * @param char $stopFlag 停止フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopFlag($stopFlag) {
		$this->_properties['stopFlag'] = $stopFlag;
    }


    /**
     * 停止日時 の値を返します
	 *
     * @return datetime 停止日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopDatetime() {
		return $this->_properties['stopDatetime'];
    }


    /**
     * 停止日時 の値をセットします
	 *
	 * @param datetime $stopDatetime 停止日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopDatetime($stopDatetime) {
		$this->_properties['stopDatetime'] = $stopDatetime;
    }


    /**
     * 停止理由 の値を返します
	 *
     * @return varchar 停止理由
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopReasonType() {
		return $this->_properties['stopReasonType'];
    }


    /**
     * 停止理由 の値をセットします
	 *
	 * @param varchar $stopReasonType 停止理由
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopReasonType($stopReasonType) {
		$this->_properties['stopReasonType'] = $stopReasonType;
    }


    /**
     * 購読再開予定日 の値を返します
	 *
     * @return date 購読再開予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionRestartDate() {
		return $this->_properties['subscriptionRestartDate'];
    }


    /**
     * 購読再開予定日 の値をセットします
	 *
	 * @param date $subscriptionRestartDate 購読再開予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionRestartDate($subscriptionRestartDate) {
		$this->_properties['subscriptionRestartDate'] = $subscriptionRestartDate;
    }


    /**
     * お試し解約フラグ の値を返します
	 *
     * @return char お試し解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTrialCancelFlag() {
		return $this->_properties['trialCancelFlag'];
    }


    /**
     * お試し解約フラグ の値をセットします
	 *
	 * @param char $trialCancelFlag お試し解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTrialCancelFlag($trialCancelFlag) {
		$this->_properties['trialCancelFlag'] = $trialCancelFlag;
    }


    /**
     * 購読解約フラグ の値を返します
	 *
     * @return char 購読解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionCancelFlag() {
		return $this->_properties['subscriptionCancelFlag'];
    }


    /**
     * 購読解約フラグ の値をセットします
	 *
	 * @param char $subscriptionCancelFlag 購読解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionCancelFlag($subscriptionCancelFlag) {
		$this->_properties['subscriptionCancelFlag'] = $subscriptionCancelFlag;
    }


    /**
     * 購読解約日時 の値を返します
	 *
     * @return datetime 購読解約日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubscriptionCancelDatetime() {
		return $this->_properties['subscriptionCancelDatetime'];
    }


    /**
     * 購読解約日時 の値をセットします
	 *
	 * @param datetime $subscriptionCancelDatetime 購読解約日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubscriptionCancelDatetime($subscriptionCancelDatetime) {
		$this->_properties['subscriptionCancelDatetime'] = $subscriptionCancelDatetime;
    }


    /**
     * 単発解約フラグ の値を返します
	 *
     * @return char 単発解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSingleCancelFlag() {
		return $this->_properties['singleCancelFlag'];
    }


    /**
     * 単発解約フラグ の値をセットします
	 *
	 * @param char $singleCancelFlag 単発解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSingleCancelFlag($singleCancelFlag) {
		$this->_properties['singleCancelFlag'] = $singleCancelFlag;
    }


    /**
     * 単発解約日時 の値を返します
	 *
     * @return datetime 単発解約日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSingleCancelDatetime() {
		return $this->_properties['singleCancelDatetime'];
    }


    /**
     * 単発解約日時 の値をセットします
	 *
	 * @param datetime $singleCancelDatetime 単発解約日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSingleCancelDatetime($singleCancelDatetime) {
		$this->_properties['singleCancelDatetime'] = $singleCancelDatetime;
    }


    /**
     * 終了フラグ の値を返します
	 *
     * @return char 終了フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEndFlag() {
		return $this->_properties['endFlag'];
    }


    /**
     * 終了フラグ の値をセットします
	 *
	 * @param char $endFlag 終了フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEndFlag($endFlag) {
		$this->_properties['endFlag'] = $endFlag;
    }


    /**
     * 終了日時 の値を返します
	 *
     * @return datetime 終了日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEndDatetime() {
		return $this->_properties['endDatetime'];
    }


    /**
     * 終了日時 の値をセットします
	 *
	 * @param datetime $endDatetime 終了日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEndDatetime($endDatetime) {
		$this->_properties['endDatetime'] = $endDatetime;
    }


    /**
     * 注文取消フラグ の値を返します
	 *
     * @return char 注文取消フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderCancelFlag() {
		return $this->_properties['orderCancelFlag'];
    }


    /**
     * 注文取消フラグ の値をセットします
	 *
	 * @param char $orderCancelFlag 注文取消フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderCancelFlag($orderCancelFlag) {
		$this->_properties['orderCancelFlag'] = $orderCancelFlag;
    }


    /**
     * 注文取消日時 の値を返します
	 *
     * @return datetime 注文取消日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderCancelDatetime() {
		return $this->_properties['orderCancelDatetime'];
    }


    /**
     * 注文取消日時 の値をセットします
	 *
	 * @param datetime $orderCancelDatetime 注文取消日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderCancelDatetime($orderCancelDatetime) {
		$this->_properties['orderCancelDatetime'] = $orderCancelDatetime;
    }


    /**
     * メモ の値を返します
	 *
     * @return text メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param text $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * mdr_suddenly_year_flag の値を返します
	 *
     * @return char mdr_suddenly_year_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSuddenlyYearFlag() {
		return $this->_properties['suddenlyYearFlag'];
    }


    /**
     * mdr_suddenly_year_flag の値をセットします
	 *
	 * @param char $suddenlyYearFlag mdr_suddenly_year_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSuddenlyYearFlag($suddenlyYearFlag) {
		$this->_properties['suddenlyYearFlag'] = $suddenlyYearFlag;
    }


    /**
     * 欠陥フラグ の値を返します
	 *
     * @return char 欠陥フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * 欠陥フラグ の値をセットします
	 *
	 * @param char $imperfectionFlag 欠陥フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























