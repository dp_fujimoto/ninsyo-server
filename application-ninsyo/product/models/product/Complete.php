<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstProduct.php';
require_once 'tool/models/Operation.php';
require_once 'api/models/Operation.php';

/**
 * 商品情報の登録を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Product_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 商品情報の登録を行う。
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // ログ文
        $logText= '';

        // 空のデータはnullにするため、配列にして処理してからactionFormに詰める
        $data = Mikoshiva_Utility_Copy::copyToArray($actionForm);
        foreach ($data as $k => $v) {
            $data[$k] = Mikoshiva_Utility_Convert::blankToNull($v);
        }
        Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($actionForm,$data);

        //-----------------------------------------------
        // 商品登録
        //-----------------------------------------------
        $productId = $actionForm->getProductId();
        if (!isBlankOrNull($productId)) {

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $productData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstProduct', $productId);
            $logText .= $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName() . 'が商品ID:' . $productId . 'のデータ更新。';

            $actionForm->setProductCode($productData->getProductCode());
            $actionForm->setImageUpdateDatetime($productData->getImageUpdateDatetime());
        } else {
            $productData = new Popo_MstProduct();
            $logText .= $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName() . 'が' . $actionForm->getProductName() . 'という商品登録。';

            // ランダム文字列生成（配列利用）
            $count = 0;
            do {
                $strinit = "abcdefghkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ012345679";
                $strarray = preg_split("//", $strinit, 0, PREG_SPLIT_NO_EMPTY);
                $str = '';
                for ($i = 0; $i < 10; $i++) {
                    $str .= $strarray[array_rand($strarray, 1)];
                }
                $actionForm->setProductCode($str);

                // SQL作成
                $sql = "";
                $sql .= "SELECT";
                $sql .= " mpr_product_code";
                $sql .= " FROM mst_product";
                $sql .= " WHERE 1 = 1 ";
                $sql .= " AND mpr_product_code = ?";

                // パラメータセット
                $params = array();
                $params[] = $str;

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                $count = 0;
                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $count++;
                }

            } while ($count > 0);
        }

        Mikoshiva_Utility_Copy::copyToPopo($productData, $actionForm);

        if (!isBlankOrNull($productData->getImageUrl()) && $actionForm->getImageUpdateFlag() === '1') {
            $productData->setImageUpdateDatetime(Mikoshiva_date::mikoshivaNow());
        }

        if (isBlankOrNull($productData->getDeletionDatetime())) {
            if ($productData->getDeleteFlag() === '1') {
                $productData->setDeletionDatetime(Mikoshiva_date::mikoshivaNow());
            } else {
                $productData->setDeleteFlag('0');
                $productData->setDeletionDatetime(null);
            }
        } else {
            if ($productData->getDeleteFlag() !== '1') {
                $productData->setDeleteFlag('0');
                $productData->setDeletionDatetime(null);
            }
        }

        // 更新（シンプルクラス）
        $db = null;
        $db = new Mikoshiva_Db_Simple();

        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstProduct', $productData);

        //-----------------------------------------------
        // キャッシュファイル削除
        //-----------------------------------------------
        $operation = new Api_Models_Operation();
        $operation->deleteConditionCacheFile($actionForm, $context);

        file_get_contents(DELETE_CONDITION_CACHE_FILE_API);

        $operation->deleteUserCacheFile($actionForm, $context);

        file_get_contents(DELETE_USER_CACHE_FILE_API);

        //-----------------------------------------------
        // アクションログ保存
        //-----------------------------------------------
        $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

        $operation = new Tool_Models_Operation();
        $operation->insertActionLog($logText);

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $actionForm;
    }
}
