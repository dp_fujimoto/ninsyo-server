<?php
/** @package Mikoshiva_Settlement */

require_once 'Popo/Abstract.php';
include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Member/Save.php';
include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Card/Save.php';
include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Entry/Register.php';
include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Exec/Run.php';


/**
 * Mikoshiva_Settlement_Popo_Output_Order_Settlement_List
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Order_Settlement_Result_List extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'member' => null,
        'card' => null,
        'entry' => null,
        'exec' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
        $this->setMember(new Mikoshiva_Settlement_Popo_Output_Gmo_Member_Save());
        $this->setCard(new Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save());
        $this->setEntry(new Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register());
        $this->setExec(new Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Run());
    }



    /**
     * member
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Member_Save member
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMember() {
		return $this->_properties['member'];
    }


    /**
     * member
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Gmo_Member_Save $member member
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMember(Mikoshiva_Settlement_Popo_Output_Gmo_Member_Save $member) {
		$this->_properties['member'] = $member;
    }


    /**
     * card
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save card
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCard() {
		return $this->_properties['card'];
    }


    /**
     * card
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save $card card
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCard(Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save $card) {
		$this->_properties['card'] = $card;
    }


    /**
     * entry
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register entry
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getEntry() {
		return $this->_properties['entry'];
    }


    /**
     * entry
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register $entry entry
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setEntry(Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register $entry) {
		$this->_properties['entry'] = $entry;
    }


    /**
     * exec
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Run exec
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getExec() {
		return $this->_properties['exec'];
    }


    /**
     * exec
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Run $exec exec
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setExec(Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Run $exec) {
		$this->_properties['exec'] = $exec;
    }


}

























