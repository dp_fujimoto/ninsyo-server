{assign var='_backgroundColor' value='#000000'}

<div class="MegaMenu" style="background-color: {$_backgroundColor}">

{*****************************************************************
System
******************************************************************}
    <a class="MegaMenuLink COM_bold" href="javascript:void(0)" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='white'">System</a>
    <div class="MegaMenuContent">
      <table class="MegaMenuTable" border="0">
        <tr>
          <td class="COM_nowrap">
            <ul class="MegaMenuLists">
              <li><a href="{$smarty.const.SUB_FOLDER_PATH}">【トップ】</a></li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/search-special-data/convert-mac-csv');"href="#">windows用csvファイルをmac用に変換</a></li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/staff/input/staffId/{$smarty.session.staff.mstStaff->getStaffId()}');"href="#">【ログイン情報変更】</a></li>
              <li><a href="{$smarty.const.SUB_FOLDER_PATH}/login">【ログアウト】</a></li>
            </ul>
          </td>

          {if (int)$smarty.session.staff.mstStaff->getStaffId() < 9}
          <td class="COM_nowrap">
            <ul class="MegaMenuLists">
              <li><span class="COM_bold COM_fontGrey">システム用メニュー</span></li>
              <li>スタッフIDが8以下の人のみ表示</li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/staff/search');"href="#">スタッフ一覧</a></li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/product/management/search-table-data');"href="#">テーブルデータ検索</a></li>
              {if $smarty.const.REAL_FLAG == '0'}
                  <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/popo-ar/index');"href="#">POPO 生成</a></li>
              {/if}
            </ul>
          </td>
          {/if}
        </tr>
      </table>
    </div>

{*****************************************************************
コンテンツ管理
******************************************************************}
    <a class="MegaMenuLink COM_bold" href="javascript:void(0)" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='white'">コンテンツ管理</a>
    <div class="MegaMenuContent">
      <table class="MegaMenuTable" border="0">
        <tr>
          <td class="COM_nowrap">
            メニュー
            <br>
            <ul class="MegaMenuLists">
              <li>
                <a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/product/brand/search')" href="#">１．ブランド検索</a>
              </li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/product/product/search')" href="#">２．商品検索</a></li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/product/content/search')" href="#">３．コンテンツ検索</a></li>
            </ul>
          </td>
          <td class="COM_nowrap">
            説明書
            <br>
            <ul class="MegaMenuLists">
              <li>
                <a target="_blank" href="{$smarty.const.SUB_FOLDER_PATH}/document/operating-manual/brand_manual.pdf" href="#">ブランド画面説明書</a>
              </li>
              <li>
                <a target="_blank" href="{$smarty.const.SUB_FOLDER_PATH}/document/operating-manual/product_manual.pdf" href="#">商品画面説明書</a>
              </li>
              <li>
                <a target="_blank" href="{$smarty.const.SUB_FOLDER_PATH}/document/operating-manual/content_manual.pdf" href="#">コンテンツ画面説明書</a>
              </li>
            </ul>
          </td>
        </tr>
      </table>
    </div>

{*****************************************************************
ユーザー管理
******************************************************************}
    <a class="MegaMenuLink COM_bold" href="javascript:void(0)" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='white'">ユーザー管理</a>
    <div class="MegaMenuContent">
      <table class="MegaMenuTable" border="0">
        <tr>
          <td class="COM_nowrap">
            メニュー
            <br>
            <ul class="MegaMenuLists">
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/user/content/search/deleteFlag/0/applicationType/APPLICATION_TYPE_WEB_PAGE_AUTH')" href="#">ユーザーコンテンツ検索</a></li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/user/check-user-content/index')" href="#">ユーザーコンテンツ存在チェック</a></li>
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/user/content/input-list')" href="#">ユーザーコンテンツ一括登録</a></li>
            </ul>
          </td>
          <td class="COM_nowrap">
            説明書
            <br>
            <ul class="MegaMenuLists">
              <li>
                <a target="_blank" href="{$smarty.const.SUB_FOLDER_PATH}/document/operating-manual/user_content_manual.pdf" href="#">ユーザーコンテンツ画面説明書</a>
              </li>
            </ul>
          </td>
        </tr>
      </table>
    </div>

{*****************************************************************
アクセスログ解析
******************************************************************}
    <a class="MegaMenuLink COM_bold" href="javascript:void(0)" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='white'">アクセスログ解析</a>
    <div class="MegaMenuContent">
      <table class="MegaMenuTable" border="0">
        <tr>
          <td class="COM_nowrap">
            メニュー
            <br>
            <ul class="MegaMenuLists">
              <li><a onclick="OYA_newTab('{$smarty.const.SUB_FOLDER_PATH}/tool/log-analysis/search-edge-trade')" href="#">エッジトレードアプリ</a></li>
            </ul>
          </td>
          {*
          <td class="COM_nowrap">
            説明書
            <br>
            <ul class="MegaMenuLists">
              <li>
                <a target="_blank" href="{$smarty.const.SUB_FOLDER_PATH}/document/operating-manual/user_content_manual.pdf" href="#">ユーザーコンテンツ画面説明書</a>
              </li>
            </ul>
          </td>
          *}
        </tr>
      </table>
    </div>
</div>
