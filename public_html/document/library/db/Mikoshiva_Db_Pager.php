<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; Pager 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <div class="system-message">
              <p>
                このクラスで使用する ActionForm は必ず <strong>Mikoshiva_Controller_Mapping_Form_Pager_Abstract</strong> クラスを継承して下さい。
              </p>
            </div>



            <ol>
              <!-- <li><a href="#__construct">Mikoshiva_Db_Pager#__construct - コンストラクタ</a></li>  -->
              <li><a href="#fetchAllLimit">Mikoshiva_Db_Pager#fetchAllLimit - データを取得し、またページャーバーのHTMLをアクションフォームにセットします。</a></li>
              <li><a href="#getPagerBar">Mikoshiva_Db_Pager#getPagerBar - ページャーバーのHTMLを取得します。</a></li>
              <li><a href="#getTotalCount">Mikoshiva_Db_Pager#getTotalCount - 検索条件にヒットしたレコードの合計件数を取得します。</a></li>
            </ol>


            <!-- ==============================================
            ■Mikoshiva_Db_Pager#fetchAllLimit
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="fetchAllLimit">Mikoshiva_Db_Pager#fetchAllLimit</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              リモートのファイルの一覧を表示
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Db_Pager_Exception を投げます。
            </p>

            <div class="system-message">
              <p>
                このメソッドは SQL の結果から自動的には PagerBar を作り出し ActionForm にセットします。
              </p>
            </div>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$actionForm</td>
                <td>Mikoshiva_Controller_Mapping_Form_Pager_Abstract</td>
                <td></td>
                <td>アクションフォーム</td>
                <td>NULL の場合例外を出力します。</td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$pagerPerPage</td>
                <td>int</td>
                <td></td>
                <td>1ページ当たりの表示件数</td>
                <td>NULL の場合例外を出力します。</td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$sql</td>
                <td>string</td>
                <td></td>
                <td>DB問い合わせに使用するSQL</td>
                <td>NULL の場合例外を出力します。</td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$params</td>
                <td>array</td>
                <td>array()</td>
                <td>sqlのプレースホルダを置換する値の配列</td>
                <td></td>
              </tr>
              <tr>
                <td><?php echo ++$i; ?></td>
                <td>$fetchMode</td>
                <td>string</td>
                <td>null</td>
                <td>フェッチモード</td>
                <td>フェッチモードを特に変更したい場合のみ使用します。<br>
                    nullの場合、Zend_Db::FETCH_ASSOC(Zend Frameworkのデフォルト)になります。
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>array</td>
                <td><br></td>
                <td><br></td>
                <td>
                従来のfetchAllと同様、「『カラム名をキーとする連想配列』の配列」を返します。
                <pre>
[][カラム名]        =>  値
                </pre>
                </td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
モデル
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■Pager・サンプル
//
//-----------------------------------------------
  $data = array();
  require_once \'Mikoshiva/Db/Pager.php\';
  $pager = new Mikoshiva_Db_Pager();
  // $dataにはクエリの結果が返される。
  $data  = $pager->fetchAllLimit($actionForm, 20, $sql, $params);
');
?>
            </pre>

            <pre class="wiki">
ビュー
<?php
highlight_string('
結果　<span class="COM_fontRed COM_bold">{$form->getPagerTotalCount()}</span>件　HITしました。<br>
<hr>
{$form->getPagerBar()}
');
?>
            </pre>

            <h3>結果</h3>
            <pre class="wiki">
表示
            <img src="/document/img/Mikoshiva_Pager_1.jpg">
            </pre>
            <pre class="wiki">
ソース
            <?php
            echo(htmlspecialchars('
結果　<span class="COM_fontRed COM_bold">336</span>件　HITしました。<br>
<hr>
<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/1\', \'4\', \'null\', []);">最初のページ</a>
<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/8\', \'4\', \'null\', []);"> ＜ </a>
...<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/4\', \'4\', \'null\', []);">4</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/5\', \'4\', \'null\', []);">5</a>

 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/6\', \'4\', \'null\', []);">6</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/7\', \'4\', \'null\', []);">7</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/8\', \'4\', \'null\', []);">8</a>
 9 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/10\', \'4\', \'null\', []);">10</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/11\', \'4\', \'null\', []);">11</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/12\', \'4\', \'null\', []);">12</a>

 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/13\', \'4\', \'null\', []);">13</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/14\', \'4\', \'null\', []);">14</a>
...<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/10\', \'4\', \'null\', []);"> ＞ </a>
<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/17\', \'4\', \'null\', []);">最後のページ</a>
<br>
9 / 17ページ
'));
?>
            </pre>


            <!-- ==============================================
            ■Mikoshiva_Db_Pager#getPagerBar
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="getPagerBar">Mikoshiva_Db_Pager#getPagerBar</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              ページャーバーのHTMLを取得します。
            </p>
            <p>
              実行に失敗した場合は Mikoshiva_Db_Pager_Exception を投げます。
            </p>

            <div class="system-message">
              <p>
                このメソッドは別途 PagerBar を改変する必要がある場合に使用します。<br>
                #fetchAllLimit メソッドを使用した際にこのメソッドと同じ値が ActionForm にセットされます。
              </p>
            </div>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>string</td>
                <td><br></td>
                <td><br></td>
                <td>
                  ページャーバーを表示するHTMLを取得します。
                </td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
モデル
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■Pager・サンプル
//
//------------------------------------------------
  $data = array();
  require_once \'Mikoshiva/Db/Pager.php\';
  $pager = new Mikoshiva_Db_Pager();
  // $dataにはクエリの結果が返される。
  // この時点でアクションフォームにもページャーバーのHTMLがセットされている。
  $data  = $pager->fetchAllLimit($this->_db, $actionForm, 20, $sql, $params);

  dumper($pager->getPagerBar());

');
?>
            </pre>

            <h3>結果</h3>
            <pre class="wiki">
            <?php
            echo(htmlspecialchars('
string(2883) "結果　<span class="COM_fontRed COM_bold">336</span>件　HITしました。<br>
1 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/2\', \'4\', \'null\', []);">2</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/3\', \'4\', \'null\', []);">3</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/4\', \'4\', \'null\', []);">4</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/5\', \'4\', \'null\', []);">5</a>
 <a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/6\', \'4\', \'null\', []);">6</a>
...<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/2\', \'4\', \'null\', []);"> ＞ </a>
<a href="#" onclick="KO_currentTab(\'/shipping/step-shipping/search/module/shipping/dmStepShippingId//dmShippingType/DM_SHIPPING_TYPE_CATALOG/lastName//firstName//lastNameKana//firstNameKana//prefecture//address1//expectedShippingDate//dmShippingStatus//dmSupportStatus//mode/search/zip1//zip2//tel1//tel2//tel3//year//month//day//pagerPerPage/20/pagerCurrentPage/17\', \'4\', \'null\', []);">最後のページ</a>
<br>
1 / 17ページ<br>
"
            '));
            ?>
            </pre>


            <!-- ==============================================
            ■Mikoshiva_Db_Pager#getTotalCount
            ============================================== -->
            <?php $i = 0; ?>
            <h2><a name="getTotalCount">Mikoshiva_Db_Pager#getTotalCount</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              検索条件にヒットしたレコードの合計件数を取得します。
            </p>

            <div class="system-message">
              <p>
                このメソッドは別途 PagerTotalCount を改変する必要がある場合に使用します。<br>
                #fetchAllLimit メソッドを使用した際にこのメソッドと同じ値が ActionForm にセットされます。
              </p>
            </div>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>string</td>
                <td><br></td>
                <td><br></td>
                <td>
                  検索条件にヒットしたレコードの合計件数を取得します。
                </td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
モデル
<?php
highlight_string('<?php
//-----------------------------------------------
//
// ■Pager・サンプル
//
//------------------------------------------------
  $data = array();
  require_once \'Mikoshiva/Db/Pager.php\';
  $pager = new Mikoshiva_Db_Pager();
  $data  = $pager->fetchAllLimit($this->_db, $actionForm, 20, $sql, $params);

  dumper($pager->getTotalCount());

');
?>
            </pre>

            <h3>結果</h3>
            <pre class="wiki">
            <?php
            echo(htmlspecialchars('
string(3) "336"
            '));
            ?>
            </pre>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>













