<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/CloseTabAll.php';


/**
 * 全てのタブを閉じるjavascriptをリンクタグに実装した形で返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: CloseTabAllLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_CloseTabAllLink extends Mikoshiva_View_Helper_Abstract {

    /**
     * 全てのタブを閉じるjavascriptをリンクタグに実装した形で返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: CloseTabAllLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param unknown_type $name
     * @return unknown_type
     */
    public function closeTabAllLink($name) {
        $a = new Mikoshiva_View_Helper_CloseTabAll();
        return '<a href="#" onclick="' . $a->closeTabAll() . '">' . $name . '</a>';
    }
}
