<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'tool/models/classes/PrimaryKeyByTable.php';
require_once 'tool/models/classes/ColumnNameList.php';


/**
 * SQL実行結果を取得
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/02/17
 * @version        SVN: $Id:
 *
 */
class Tool_Models_SearchSpecialData_ExecuteSelectSql
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * SQL実行結果を取得
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // リターンする配列
        $output = array();

        $sql = $actionForm->getSql();

        if (!isBlankOrNull($sql)) {

            try {
                $this->_db->beginTransaction();

                $sqlArr = explode(';',$sql);

                $count = 0;
                foreach ($sqlArr as $kkk => $vvv) {
                    $exeSql = trim($vvv);
                    if (!isBlankOrNull($exeSql)) {
                        $count++;
                        $res = $this->_db->query($exeSql);

                        $flag = '0';
                        while ($v = $res->fetch(Zend_Db::FETCH_ASSOC)) {
                            if ($flag === '0') {
                                $flag = '1';
                                $tempArr = array();
                                foreach ($v as $k2 => $v2) {
                                    $tempArr[] = $k2;
                                }
                                $output[] = $tempArr;
                            }
                            $output[] = $v;
                        }
                    }
                }
                echo '実行SQL数：' . $count;



                $this->_db->rollback();

            } catch (Mikoshiva_Exception $e) {
                echo 'SQLエラーが発生しました。';
                dumper($e);
                $this->_db->rollback();
            }
        }

        return $output;
    }
}
