<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_direction_status_history
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxDirectionStatusHistory extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tdh';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'directionStatusHistoryId' => null,
        'directionStatusHistoryNumber' => null,
        'tdsDirectionStatusId' => null,
        'tdsDirectionId' => null,
        'tdsDivisionId' => null,
        'tdsCustomerId' => null,
        'tdsStatusNumber' => null,
        'tdsCampaignProductId' => null,
        'tdsCampaignSalesId' => null,
        'tdsPurchaseDetailId' => null,
        'tdsMemberId' => null,
        'tdsAddressNumber' => null,
        'tdsChargeType' => null,
        'tdsProposalDatetime' => null,
        'tdsFreePeriod' => null,
        'tdsFreePeriodType' => null,
        'tdsFreePeriodEndDate' => null,
        'tdsGuaranteePeriod' => null,
        'tdsGuaranteePeriodType' => null,
        'tdsMasterOrderId' => null,
        'tdsMasterEntryStatus' => null,
        'tdsFirstOrderId' => null,
        'tdsFirstAmount' => null,
        'tdsFirstShippingAmount' => null,
        'tdsFirstDeliveryFee' => null,
        'tdsProvisorySalesOrderId' => null,
        'tdsContinuityOrderIdLastNumber' => null,
        'tdsContinuityAmount' => null,
        'tdsContinuityShippingAmount' => null,
        'tdsContinuityDeliveryFee' => null,
        'tdsCardDivideNumber' => null,
        'tdsCardDivideType' => null,
        'tdsSubscriptionPackageFlag' => null,
        'tdsShippingStartDate' => null,
        'tdsPackageShippingNumber' => null,
        'tdsPackageShippingPlanNumber' => null,
        'tdsShippingNumber' => null,
        'tdsChargeNumber' => null,
        'tdsDirectInstallmentFlag' => null,
        'tdsDirectInstallmentChargeNumber' => null,
        'tdsDirectInstallmentPrice' => null,
        'tdsDebitFlag' => null,
        'tdsActiveFlag' => null,
        'tdsMidtermCancellationFlag' => null,
        'tdsApplicationType' => null,
        'tdsShippingAtOrderFlag' => null,
        'tdsStopFlag' => null,
        'tdsStopDatetime' => null,
        'tdsStopReasonType' => null,
        'tdsSubscriptionRestartDate' => null,
        'tdsSubscriptionCancelFlag' => null,
        'tdsSubscriptionCancelDatetime' => null,
        'tdsSingleCancelFlag' => null,
        'tdsSingleCancelDatetime' => null,
        'tdsOrderCancelFlag' => null,
        'tdsOrderCancelDatetime' => null,
        'tdsDummyFlag' => null,
        'tdsSuddenlyYearFlag' => null,
        'tdsEndlessFlag' => null,
        'tdsImperfectionFlag' => null,
        'tdsDeleteFlag' => null,
        'tdsDeletionDatetime' => null,
        'tdsRegistrationDatetime' => null,
        'tdsUpdateDatetime' => null,
        'tdsUpdateTimestamp' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 指示ステータス履歴ID の値を返します
	 *
     * @return int 指示ステータス履歴ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusHistoryId() {
		return $this->_properties['directionStatusHistoryId'];
    }


    /**
     * 指示ステータス履歴ID の値をセットします
	 *
	 * @param int $directionStatusHistoryId 指示ステータス履歴ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusHistoryId($directionStatusHistoryId) {
		$this->_properties['directionStatusHistoryId'] = $directionStatusHistoryId;
    }


    /**
     * 指示ステータス履歴番号 の値を返します
	 *
     * @return int 指示ステータス履歴番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusHistoryNumber() {
		return $this->_properties['directionStatusHistoryNumber'];
    }


    /**
     * 指示ステータス履歴番号 の値をセットします
	 *
	 * @param int $directionStatusHistoryNumber 指示ステータス履歴番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusHistoryNumber($directionStatusHistoryNumber) {
		$this->_properties['directionStatusHistoryNumber'] = $directionStatusHistoryNumber;
    }


    /**
     * 指示ステータスID（指示ステータステーブル） の値を返します
	 *
     * @return int 指示ステータスID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDirectionStatusId() {
		return $this->_properties['tdsDirectionStatusId'];
    }


    /**
     * 指示ステータスID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsDirectionStatusId 指示ステータスID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDirectionStatusId($tdsDirectionStatusId) {
		$this->_properties['tdsDirectionStatusId'] = $tdsDirectionStatusId;
    }


    /**
     * 指示ID（指示ステータステーブル） の値を返します
	 *
     * @return int 指示ID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDirectionId() {
		return $this->_properties['tdsDirectionId'];
    }


    /**
     * 指示ID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsDirectionId 指示ID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDirectionId($tdsDirectionId) {
		$this->_properties['tdsDirectionId'] = $tdsDirectionId;
    }


    /**
     * 部署ID（指示ステータステーブル） の値を返します
	 *
     * @return int 部署ID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDivisionId() {
		return $this->_properties['tdsDivisionId'];
    }


    /**
     * 部署ID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsDivisionId 部署ID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDivisionId($tdsDivisionId) {
		$this->_properties['tdsDivisionId'] = $tdsDivisionId;
    }


    /**
     * 顧客ID（指示ステータステーブル） の値を返します
	 *
     * @return int 顧客ID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsCustomerId() {
		return $this->_properties['tdsCustomerId'];
    }


    /**
     * 顧客ID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsCustomerId 顧客ID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsCustomerId($tdsCustomerId) {
		$this->_properties['tdsCustomerId'] = $tdsCustomerId;
    }


    /**
     * ステータスNo（指示ステータステーブル） の値を返します
	 *
     * @return int ステータスNo（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsStatusNumber() {
		return $this->_properties['tdsStatusNumber'];
    }


    /**
     * ステータスNo（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsStatusNumber ステータスNo（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsStatusNumber($tdsStatusNumber) {
		$this->_properties['tdsStatusNumber'] = $tdsStatusNumber;
    }


    /**
     * キャンペーン商品ID（指示ステータステーブル） の値を返します
	 *
     * @return int キャンペーン商品ID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsCampaignProductId() {
		return $this->_properties['tdsCampaignProductId'];
    }


    /**
     * キャンペーン商品ID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsCampaignProductId キャンペーン商品ID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsCampaignProductId($tdsCampaignProductId) {
		$this->_properties['tdsCampaignProductId'] = $tdsCampaignProductId;
    }


    /**
     * キャンペーンセールスID（指示ステータステーブル） の値を返します
	 *
     * @return int キャンペーンセールスID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsCampaignSalesId() {
		return $this->_properties['tdsCampaignSalesId'];
    }


    /**
     * キャンペーンセールスID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsCampaignSalesId キャンペーンセールスID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsCampaignSalesId($tdsCampaignSalesId) {
		$this->_properties['tdsCampaignSalesId'] = $tdsCampaignSalesId;
    }


    /**
     * 購入履歴詳細ID（指示ステータステーブル） の値を返します
	 *
     * @return int 購入履歴詳細ID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsPurchaseDetailId() {
		return $this->_properties['tdsPurchaseDetailId'];
    }


    /**
     * 購入履歴詳細ID（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsPurchaseDetailId 購入履歴詳細ID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsPurchaseDetailId($tdsPurchaseDetailId) {
		$this->_properties['tdsPurchaseDetailId'] = $tdsPurchaseDetailId;
    }


    /**
     * 会員ID（指示ステータステーブル） の値を返します
	 *
     * @return char 会員ID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsMemberId() {
		return $this->_properties['tdsMemberId'];
    }


    /**
     * 会員ID（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsMemberId 会員ID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsMemberId($tdsMemberId) {
		$this->_properties['tdsMemberId'] = $tdsMemberId;
    }


    /**
     * 発送先No（指示ステータステーブル） の値を返します
	 *
     * @return int 発送先No（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsAddressNumber() {
		return $this->_properties['tdsAddressNumber'];
    }


    /**
     * 発送先No（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsAddressNumber 発送先No（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsAddressNumber($tdsAddressNumber) {
		$this->_properties['tdsAddressNumber'] = $tdsAddressNumber;
    }


    /**
     * 請求タイプ（指示ステータステーブル） の値を返します
	 *
     * @return varchar 請求タイプ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsChargeType() {
		return $this->_properties['tdsChargeType'];
    }


    /**
     * 請求タイプ（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsChargeType 請求タイプ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsChargeType($tdsChargeType) {
		$this->_properties['tdsChargeType'] = $tdsChargeType;
    }


    /**
     * 申込日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 申込日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsProposalDatetime() {
		return $this->_properties['tdsProposalDatetime'];
    }


    /**
     * 申込日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsProposalDatetime 申込日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsProposalDatetime($tdsProposalDatetime) {
		$this->_properties['tdsProposalDatetime'] = $tdsProposalDatetime;
    }


    /**
     * お試し期間（指示ステータステーブル） の値を返します
	 *
     * @return int お試し期間（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFreePeriod() {
		return $this->_properties['tdsFreePeriod'];
    }


    /**
     * お試し期間（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsFreePeriod お試し期間（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFreePeriod($tdsFreePeriod) {
		$this->_properties['tdsFreePeriod'] = $tdsFreePeriod;
    }


    /**
     * お試し期間タイプ（指示ステータステーブル） の値を返します
	 *
     * @return varchar お試し期間タイプ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFreePeriodType() {
		return $this->_properties['tdsFreePeriodType'];
    }


    /**
     * お試し期間タイプ（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsFreePeriodType お試し期間タイプ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFreePeriodType($tdsFreePeriodType) {
		$this->_properties['tdsFreePeriodType'] = $tdsFreePeriodType;
    }


    /**
     * お試し期間終了日（指示ステータステーブル） の値を返します
	 *
     * @return date お試し期間終了日（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFreePeriodEndDate() {
		return $this->_properties['tdsFreePeriodEndDate'];
    }


    /**
     * お試し期間終了日（指示ステータステーブル） の値をセットします
	 *
	 * @param date $tdsFreePeriodEndDate お試し期間終了日（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFreePeriodEndDate($tdsFreePeriodEndDate) {
		$this->_properties['tdsFreePeriodEndDate'] = $tdsFreePeriodEndDate;
    }


    /**
     * 返金保証期間（指示ステータステーブル） の値を返します
	 *
     * @return int 返金保証期間（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsGuaranteePeriod() {
		return $this->_properties['tdsGuaranteePeriod'];
    }


    /**
     * 返金保証期間（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsGuaranteePeriod 返金保証期間（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsGuaranteePeriod($tdsGuaranteePeriod) {
		$this->_properties['tdsGuaranteePeriod'] = $tdsGuaranteePeriod;
    }


    /**
     * 返金保証期間タイプ（指示ステータステーブル） の値を返します
	 *
     * @return varchar 返金保証期間タイプ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsGuaranteePeriodType() {
		return $this->_properties['tdsGuaranteePeriodType'];
    }


    /**
     * 返金保証期間タイプ（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsGuaranteePeriodType 返金保証期間タイプ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsGuaranteePeriodType($tdsGuaranteePeriodType) {
		$this->_properties['tdsGuaranteePeriodType'] = $tdsGuaranteePeriodType;
    }


    /**
     * マスターオーダーID（指示ステータステーブル） の値を返します
	 *
     * @return char マスターオーダーID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsMasterOrderId() {
		return $this->_properties['tdsMasterOrderId'];
    }


    /**
     * マスターオーダーID（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsMasterOrderId マスターオーダーID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsMasterOrderId($tdsMasterOrderId) {
		$this->_properties['tdsMasterOrderId'] = $tdsMasterOrderId;
    }


    /**
     * マスター取引ステータス（指示ステータステーブル） の値を返します
	 *
     * @return varchar マスター取引ステータス（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsMasterEntryStatus() {
		return $this->_properties['tdsMasterEntryStatus'];
    }


    /**
     * マスター取引ステータス（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsMasterEntryStatus マスター取引ステータス（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsMasterEntryStatus($tdsMasterEntryStatus) {
		$this->_properties['tdsMasterEntryStatus'] = $tdsMasterEntryStatus;
    }


    /**
     * 初回オーダーID（指示ステータステーブル） の値を返します
	 *
     * @return char 初回オーダーID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFirstOrderId() {
		return $this->_properties['tdsFirstOrderId'];
    }


    /**
     * 初回オーダーID（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsFirstOrderId 初回オーダーID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFirstOrderId($tdsFirstOrderId) {
		$this->_properties['tdsFirstOrderId'] = $tdsFirstOrderId;
    }


    /**
     * 初回商品金額（指示ステータステーブル） の値を返します
	 *
     * @return int 初回商品金額（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFirstAmount() {
		return $this->_properties['tdsFirstAmount'];
    }


    /**
     * 初回商品金額（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsFirstAmount 初回商品金額（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFirstAmount($tdsFirstAmount) {
		$this->_properties['tdsFirstAmount'] = $tdsFirstAmount;
    }


    /**
     * 初回発送金額（指示ステータステーブル） の値を返します
	 *
     * @return int 初回発送金額（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFirstShippingAmount() {
		return $this->_properties['tdsFirstShippingAmount'];
    }


    /**
     * 初回発送金額（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsFirstShippingAmount 初回発送金額（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFirstShippingAmount($tdsFirstShippingAmount) {
		$this->_properties['tdsFirstShippingAmount'] = $tdsFirstShippingAmount;
    }


    /**
     * 初回代引手数料（指示ステータステーブル） の値を返します
	 *
     * @return int 初回代引手数料（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsFirstDeliveryFee() {
		return $this->_properties['tdsFirstDeliveryFee'];
    }


    /**
     * 初回代引手数料（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsFirstDeliveryFee 初回代引手数料（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsFirstDeliveryFee($tdsFirstDeliveryFee) {
		$this->_properties['tdsFirstDeliveryFee'] = $tdsFirstDeliveryFee;
    }


    /**
     * 仮売上オーダーID（指示ステータステーブル） の値を返します
	 *
     * @return char 仮売上オーダーID（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsProvisorySalesOrderId() {
		return $this->_properties['tdsProvisorySalesOrderId'];
    }


    /**
     * 仮売上オーダーID（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsProvisorySalesOrderId 仮売上オーダーID（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsProvisorySalesOrderId($tdsProvisorySalesOrderId) {
		$this->_properties['tdsProvisorySalesOrderId'] = $tdsProvisorySalesOrderId;
    }


    /**
     * 継続オーダーID末尾番号（指示ステータステーブル） の値を返します
	 *
     * @return varchar 継続オーダーID末尾番号（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsContinuityOrderIdLastNumber() {
		return $this->_properties['tdsContinuityOrderIdLastNumber'];
    }


    /**
     * 継続オーダーID末尾番号（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsContinuityOrderIdLastNumber 継続オーダーID末尾番号（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsContinuityOrderIdLastNumber($tdsContinuityOrderIdLastNumber) {
		$this->_properties['tdsContinuityOrderIdLastNumber'] = $tdsContinuityOrderIdLastNumber;
    }


    /**
     * 継続金額（指示ステータステーブル） の値を返します
	 *
     * @return int 継続金額（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsContinuityAmount() {
		return $this->_properties['tdsContinuityAmount'];
    }


    /**
     * 継続金額（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsContinuityAmount 継続金額（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsContinuityAmount($tdsContinuityAmount) {
		$this->_properties['tdsContinuityAmount'] = $tdsContinuityAmount;
    }


    /**
     * 継続発送金額（指示ステータステーブル） の値を返します
	 *
     * @return int 継続発送金額（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsContinuityShippingAmount() {
		return $this->_properties['tdsContinuityShippingAmount'];
    }


    /**
     * 継続発送金額（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsContinuityShippingAmount 継続発送金額（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsContinuityShippingAmount($tdsContinuityShippingAmount) {
		$this->_properties['tdsContinuityShippingAmount'] = $tdsContinuityShippingAmount;
    }


    /**
     * 継続代引手数料（指示ステータステーブル） の値を返します
	 *
     * @return int 継続代引手数料（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsContinuityDeliveryFee() {
		return $this->_properties['tdsContinuityDeliveryFee'];
    }


    /**
     * 継続代引手数料（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsContinuityDeliveryFee 継続代引手数料（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsContinuityDeliveryFee($tdsContinuityDeliveryFee) {
		$this->_properties['tdsContinuityDeliveryFee'] = $tdsContinuityDeliveryFee;
    }


    /**
     * カード分割回数 の値を返します
	 *
     * @return int カード分割回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsCardDivideNumber() {
		return $this->_properties['tdsCardDivideNumber'];
    }


    /**
     * カード分割回数 の値をセットします
	 *
	 * @param int $tdsCardDivideNumber カード分割回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsCardDivideNumber($tdsCardDivideNumber) {
		$this->_properties['tdsCardDivideNumber'] = $tdsCardDivideNumber;
    }


    /**
     * カード分割タイプ の値を返します
	 *
     * @return varchar カード分割タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsCardDivideType() {
		return $this->_properties['tdsCardDivideType'];
    }


    /**
     * カード分割タイプ の値をセットします
	 *
	 * @param varchar $tdsCardDivideType カード分割タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsCardDivideType($tdsCardDivideType) {
		$this->_properties['tdsCardDivideType'] = $tdsCardDivideType;
    }


    /**
     * 一括購読フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 一括購読フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSubscriptionPackageFlag() {
		return $this->_properties['tdsSubscriptionPackageFlag'];
    }


    /**
     * 一括購読フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsSubscriptionPackageFlag 一括購読フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSubscriptionPackageFlag($tdsSubscriptionPackageFlag) {
		$this->_properties['tdsSubscriptionPackageFlag'] = $tdsSubscriptionPackageFlag;
    }


    /**
     * 発送開始日（指示ステータステーブル） の値を返します
	 *
     * @return date 発送開始日（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsShippingStartDate() {
		return $this->_properties['tdsShippingStartDate'];
    }


    /**
     * 発送開始日（指示ステータステーブル） の値をセットします
	 *
	 * @param date $tdsShippingStartDate 発送開始日（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsShippingStartDate($tdsShippingStartDate) {
		$this->_properties['tdsShippingStartDate'] = $tdsShippingStartDate;
    }


    /**
     * 購読発送回数（指示ステータステーブル） の値を返します
	 *
     * @return int 購読発送回数（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsPackageShippingNumber() {
		return $this->_properties['tdsPackageShippingNumber'];
    }


    /**
     * 購読発送回数（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsPackageShippingNumber 購読発送回数（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsPackageShippingNumber($tdsPackageShippingNumber) {
		$this->_properties['tdsPackageShippingNumber'] = $tdsPackageShippingNumber;
    }


    /**
     * 一括購読発送予定回数（指示ステータステーブル） の値を返します
	 *
     * @return int 一括購読発送予定回数（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsPackageShippingPlanNumber() {
		return $this->_properties['tdsPackageShippingPlanNumber'];
    }


    /**
     * 一括購読発送予定回数（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsPackageShippingPlanNumber 一括購読発送予定回数（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsPackageShippingPlanNumber($tdsPackageShippingPlanNumber) {
		$this->_properties['tdsPackageShippingPlanNumber'] = $tdsPackageShippingPlanNumber;
    }


    /**
     * 発送回数（指示ステータステーブル） の値を返します
	 *
     * @return int 発送回数（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsShippingNumber() {
		return $this->_properties['tdsShippingNumber'];
    }


    /**
     * 発送回数（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsShippingNumber 発送回数（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsShippingNumber($tdsShippingNumber) {
		$this->_properties['tdsShippingNumber'] = $tdsShippingNumber;
    }


    /**
     * 請求回数（指示ステータステーブル） の値を返します
	 *
     * @return int 請求回数（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsChargeNumber() {
		return $this->_properties['tdsChargeNumber'];
    }


    /**
     * 請求回数（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsChargeNumber 請求回数（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsChargeNumber($tdsChargeNumber) {
		$this->_properties['tdsChargeNumber'] = $tdsChargeNumber;
    }


    /**
     * ダイレクト分割フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char ダイレクト分割フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDirectInstallmentFlag() {
		return $this->_properties['tdsDirectInstallmentFlag'];
    }


    /**
     * ダイレクト分割フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsDirectInstallmentFlag ダイレクト分割フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDirectInstallmentFlag($tdsDirectInstallmentFlag) {
		$this->_properties['tdsDirectInstallmentFlag'] = $tdsDirectInstallmentFlag;
    }


    /**
     * ダイレクト分割請求回数（指示ステータステーブル） の値を返します
	 *
     * @return int ダイレクト分割請求回数（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDirectInstallmentChargeNumber() {
		return $this->_properties['tdsDirectInstallmentChargeNumber'];
    }


    /**
     * ダイレクト分割請求回数（指示ステータステーブル） の値をセットします
	 *
	 * @param int $tdsDirectInstallmentChargeNumber ダイレクト分割請求回数（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDirectInstallmentChargeNumber($tdsDirectInstallmentChargeNumber) {
		$this->_properties['tdsDirectInstallmentChargeNumber'] = $tdsDirectInstallmentChargeNumber;
    }


    /**
     * ダイレクト分割初回金額 の値を返します
	 *
     * @return int ダイレクト分割初回金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDirectInstallmentPrice() {
		return $this->_properties['tdsDirectInstallmentPrice'];
    }


    /**
     * ダイレクト分割初回金額 の値をセットします
	 *
	 * @param int $tdsDirectInstallmentPrice ダイレクト分割初回金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDirectInstallmentPrice($tdsDirectInstallmentPrice) {
		$this->_properties['tdsDirectInstallmentPrice'] = $tdsDirectInstallmentPrice;
    }


    /**
     * デビットフラグ（指示ステータステーブル） の値を返します
	 *
     * @return char デビットフラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDebitFlag() {
		return $this->_properties['tdsDebitFlag'];
    }


    /**
     * デビットフラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsDebitFlag デビットフラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDebitFlag($tdsDebitFlag) {
		$this->_properties['tdsDebitFlag'] = $tdsDebitFlag;
    }


    /**
     * アクティブフラグ（指示ステータステーブル） の値を返します
	 *
     * @return char アクティブフラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsActiveFlag() {
		return $this->_properties['tdsActiveFlag'];
    }


    /**
     * アクティブフラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsActiveFlag アクティブフラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsActiveFlag($tdsActiveFlag) {
		$this->_properties['tdsActiveFlag'] = $tdsActiveFlag;
    }


    /**
     * 途中解約フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 途中解約フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsMidtermCancellationFlag() {
		return $this->_properties['tdsMidtermCancellationFlag'];
    }


    /**
     * 途中解約フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsMidtermCancellationFlag 途中解約フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsMidtermCancellationFlag($tdsMidtermCancellationFlag) {
		$this->_properties['tdsMidtermCancellationFlag'] = $tdsMidtermCancellationFlag;
    }


    /**
     * 申込方法（指示ステータステーブル） の値を返します
	 *
     * @return varchar 申込方法（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsApplicationType() {
		return $this->_properties['tdsApplicationType'];
    }


    /**
     * 申込方法（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsApplicationType 申込方法（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsApplicationType($tdsApplicationType) {
		$this->_properties['tdsApplicationType'] = $tdsApplicationType;
    }


    /**
     * 購入時発送フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 購入時発送フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsShippingAtOrderFlag() {
		return $this->_properties['tdsShippingAtOrderFlag'];
    }


    /**
     * 購入時発送フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsShippingAtOrderFlag 購入時発送フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsShippingAtOrderFlag($tdsShippingAtOrderFlag) {
		$this->_properties['tdsShippingAtOrderFlag'] = $tdsShippingAtOrderFlag;
    }


    /**
     * 停止フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 停止フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsStopFlag() {
		return $this->_properties['tdsStopFlag'];
    }


    /**
     * 停止フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsStopFlag 停止フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsStopFlag($tdsStopFlag) {
		$this->_properties['tdsStopFlag'] = $tdsStopFlag;
    }


    /**
     * 停止日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 停止日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsStopDatetime() {
		return $this->_properties['tdsStopDatetime'];
    }


    /**
     * 停止日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsStopDatetime 停止日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsStopDatetime($tdsStopDatetime) {
		$this->_properties['tdsStopDatetime'] = $tdsStopDatetime;
    }


    /**
     * 停止理由（指示ステータステーブル） の値を返します
	 *
     * @return varchar 停止理由（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsStopReasonType() {
		return $this->_properties['tdsStopReasonType'];
    }


    /**
     * 停止理由（指示ステータステーブル） の値をセットします
	 *
	 * @param varchar $tdsStopReasonType 停止理由（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsStopReasonType($tdsStopReasonType) {
		$this->_properties['tdsStopReasonType'] = $tdsStopReasonType;
    }


    /**
     * 購読再開予定日（指示ステータステーブル） の値を返します
	 *
     * @return date 購読再開予定日（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSubscriptionRestartDate() {
		return $this->_properties['tdsSubscriptionRestartDate'];
    }


    /**
     * 購読再開予定日（指示ステータステーブル） の値をセットします
	 *
	 * @param date $tdsSubscriptionRestartDate 購読再開予定日（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSubscriptionRestartDate($tdsSubscriptionRestartDate) {
		$this->_properties['tdsSubscriptionRestartDate'] = $tdsSubscriptionRestartDate;
    }


    /**
     * 購読解約フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 購読解約フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSubscriptionCancelFlag() {
		return $this->_properties['tdsSubscriptionCancelFlag'];
    }


    /**
     * 購読解約フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsSubscriptionCancelFlag 購読解約フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSubscriptionCancelFlag($tdsSubscriptionCancelFlag) {
		$this->_properties['tdsSubscriptionCancelFlag'] = $tdsSubscriptionCancelFlag;
    }


    /**
     * 購読解約日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 購読解約日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSubscriptionCancelDatetime() {
		return $this->_properties['tdsSubscriptionCancelDatetime'];
    }


    /**
     * 購読解約日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsSubscriptionCancelDatetime 購読解約日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSubscriptionCancelDatetime($tdsSubscriptionCancelDatetime) {
		$this->_properties['tdsSubscriptionCancelDatetime'] = $tdsSubscriptionCancelDatetime;
    }


    /**
     * 単発解約フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 単発解約フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSingleCancelFlag() {
		return $this->_properties['tdsSingleCancelFlag'];
    }


    /**
     * 単発解約フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsSingleCancelFlag 単発解約フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSingleCancelFlag($tdsSingleCancelFlag) {
		$this->_properties['tdsSingleCancelFlag'] = $tdsSingleCancelFlag;
    }


    /**
     * 単発解約日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 単発解約日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSingleCancelDatetime() {
		return $this->_properties['tdsSingleCancelDatetime'];
    }


    /**
     * 単発解約日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsSingleCancelDatetime 単発解約日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSingleCancelDatetime($tdsSingleCancelDatetime) {
		$this->_properties['tdsSingleCancelDatetime'] = $tdsSingleCancelDatetime;
    }


    /**
     * 注文取消フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 注文取消フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsOrderCancelFlag() {
		return $this->_properties['tdsOrderCancelFlag'];
    }


    /**
     * 注文取消フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsOrderCancelFlag 注文取消フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsOrderCancelFlag($tdsOrderCancelFlag) {
		$this->_properties['tdsOrderCancelFlag'] = $tdsOrderCancelFlag;
    }


    /**
     * 注文取消日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 注文取消日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsOrderCancelDatetime() {
		return $this->_properties['tdsOrderCancelDatetime'];
    }


    /**
     * 注文取消日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsOrderCancelDatetime 注文取消日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsOrderCancelDatetime($tdsOrderCancelDatetime) {
		$this->_properties['tdsOrderCancelDatetime'] = $tdsOrderCancelDatetime;
    }


    /**
     * tdh_tds_dummy_flag の値を返します
	 *
     * @return char tdh_tds_dummy_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDummyFlag() {
		return $this->_properties['tdsDummyFlag'];
    }


    /**
     * tdh_tds_dummy_flag の値をセットします
	 *
	 * @param char $tdsDummyFlag tdh_tds_dummy_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDummyFlag($tdsDummyFlag) {
		$this->_properties['tdsDummyFlag'] = $tdsDummyFlag;
    }


    /**
     * tdh_tds_suddenly_year_flag の値を返します
	 *
     * @return char tdh_tds_suddenly_year_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsSuddenlyYearFlag() {
		return $this->_properties['tdsSuddenlyYearFlag'];
    }


    /**
     * tdh_tds_suddenly_year_flag の値をセットします
	 *
	 * @param char $tdsSuddenlyYearFlag tdh_tds_suddenly_year_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsSuddenlyYearFlag($tdsSuddenlyYearFlag) {
		$this->_properties['tdsSuddenlyYearFlag'] = $tdsSuddenlyYearFlag;
    }


    /**
     * tdh_tds_endless_flag の値を返します
	 *
     * @return char tdh_tds_endless_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsEndlessFlag() {
		return $this->_properties['tdsEndlessFlag'];
    }


    /**
     * tdh_tds_endless_flag の値をセットします
	 *
	 * @param char $tdsEndlessFlag tdh_tds_endless_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsEndlessFlag($tdsEndlessFlag) {
		$this->_properties['tdsEndlessFlag'] = $tdsEndlessFlag;
    }


    /**
     * tdh_tds_imperfection_flag の値を返します
	 *
     * @return char tdh_tds_imperfection_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsImperfectionFlag() {
		return $this->_properties['tdsImperfectionFlag'];
    }


    /**
     * tdh_tds_imperfection_flag の値をセットします
	 *
	 * @param char $tdsImperfectionFlag tdh_tds_imperfection_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsImperfectionFlag($tdsImperfectionFlag) {
		$this->_properties['tdsImperfectionFlag'] = $tdsImperfectionFlag;
    }


    /**
     * 削除フラグ（指示ステータステーブル） の値を返します
	 *
     * @return char 削除フラグ（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDeleteFlag() {
		return $this->_properties['tdsDeleteFlag'];
    }


    /**
     * 削除フラグ（指示ステータステーブル） の値をセットします
	 *
	 * @param char $tdsDeleteFlag 削除フラグ（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDeleteFlag($tdsDeleteFlag) {
		$this->_properties['tdsDeleteFlag'] = $tdsDeleteFlag;
    }


    /**
     * 削除日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 削除日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsDeletionDatetime() {
		return $this->_properties['tdsDeletionDatetime'];
    }


    /**
     * 削除日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsDeletionDatetime 削除日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsDeletionDatetime($tdsDeletionDatetime) {
		$this->_properties['tdsDeletionDatetime'] = $tdsDeletionDatetime;
    }


    /**
     * 登録日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 登録日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsRegistrationDatetime() {
		return $this->_properties['tdsRegistrationDatetime'];
    }


    /**
     * 登録日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsRegistrationDatetime 登録日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsRegistrationDatetime($tdsRegistrationDatetime) {
		$this->_properties['tdsRegistrationDatetime'] = $tdsRegistrationDatetime;
    }


    /**
     * 更新日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime 更新日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsUpdateDatetime() {
		return $this->_properties['tdsUpdateDatetime'];
    }


    /**
     * 更新日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsUpdateDatetime 更新日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsUpdateDatetime($tdsUpdateDatetime) {
		$this->_properties['tdsUpdateDatetime'] = $tdsUpdateDatetime;
    }


    /**
     * システム更新日時（指示ステータステーブル） の値を返します
	 *
     * @return datetime システム更新日時（指示ステータステーブル）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTdsUpdateTimestamp() {
		return $this->_properties['tdsUpdateTimestamp'];
    }


    /**
     * システム更新日時（指示ステータステーブル） の値をセットします
	 *
	 * @param datetime $tdsUpdateTimestamp システム更新日時（指示ステータステーブル）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTdsUpdateTimestamp($tdsUpdateTimestamp) {
		$this->_properties['tdsUpdateTimestamp'] = $tdsUpdateTimestamp;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























