<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'tool/models/Operation.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Product_Models_Management_UpdateTableData extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $updateData = $actionForm->getUpdateData();
        $updateName = $actionForm->getUpdateName();
        $updateColumnNameArr = array();
        $now = Mikoshiva_Date::mikoshivaNow();
        $errMsg = array();
        $filePath = UPLOADS_DIR . '/tool' . '/updateTableData.txt';


        // $tableData[テーブル名][プライマリーＩＤ][カラム名] = 内容
        $tableData = array();
        $primaryKeyList = array();

        if (is_array($updateName)) {
            foreach ($updateName as $k => $v) {
                $temp = explode('||',$v);
                if (count($temp) === 4) {
                    if (!isset($tableData[$temp[0]])) {
                        $tableData[$temp[0]] = array();
                        $primaryKeyList[$temp[0]] = $temp[2];
                    }
                    if (!isset($tableData[$temp[0]][$temp[3]])) {
                        $tableData[$temp[0]][$temp[3]] = array();
                    }
                    $str= trim($updateData[$v]);
                    $tableData[$temp[0]][$temp[3]][$temp[1]] = $str;
                }
            }
        }

        foreach ($tableData as $tableName => $primaryKeyArr) {

            $temp = explode('_',$primaryKeyList[$tableName]);
            $prefix = $temp[0];

            // カラム情報取得
            $columnData = array();

            // SQL作成
            $sql = "";
            $sql .= "SHOW COLUMNS ";
            $sql .= "FROM  ";
            $sql .= $tableName;

            // SQL実行
            $res = $this->_db->query($sql);

            /*
                array(6) {
                    ["Field"] => string(16) "mdr_direction_id"
                    ["Type"] => string(7) "int(11)"
                    ["Null"] => string(2) "NO"
                    ["Key"] => string(3) "PRI"
                    ["Default"] => NULL
                    ["Extra"] => string(14) "auto_increment"
                }
            */

            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                $columnData[$v['Field']] = $v;
            }

            // テーブルが指示ステータスの場合、指示ステータス履歴にデータを残す
            if ($tableName === 'trx_direction_status') {
                $class = new Tool_Models_Operation();
            }

            foreach ($primaryKeyArr as $primaryKey => $columnArr) {

                // SQL作成
                $sql = "";
                $sql .= "SELECT * ";
                $sql .= "FROM  ";
                $sql .= $tableName;
                $sql .= " WHERE ";
                $sql .= $primaryKeyList[$tableName] . " = " . $primaryKey;

                // SQL実行
                $res = $this->_db->fetchAll($sql);
                countCheck( $res , 1 );

                $nowData = $res[0];
                $logArr = array();
                $logArr[] = 'UPDATE_TABLE_NAME:' . $tableName;
                $logArr[] = 'TABLE_ID:' . $primaryKey;
                $logArr[] = 'STAFF:' . $context->getStaff()->mstStaff->getStaffId() . '-' . $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName();

                // SQL作成
                $sql = "";
                $sql .= "UPDATE ";
                $sql .= $tableName;
                $sql .= " SET ";

                $count = 0;
                $noCount = 0;
                $params = array();

                foreach ($columnArr as $column => $value) {

                    $id = $tableName . '||' . $column . '||' . $primaryKeyList[$tableName] . '||' .$primaryKey;

                    if ($column === $primaryKeyList[$tableName]) {
                        continue;
                    } else if ($column === $prefix . '_update_datetime') {
                        continue;
                    } else if ($column === $prefix . '_update_timestamp') {
                        continue;
                    } else if (isBlankOrNull($nowData[$column]) && isBlankOrNull($value)) {
                        continue;
                    } else if ($nowData[$column] == $value) {
                        continue;
                    } else if (isBlankOrNull($value)) {
                        if ($columnData[$column]['Null'] === 'YES') {
                            if ($count > 0)  {
                                $sql .= ", ";
                            }
                            $sql .= $column . " = null ";
                            $value = 'null';
                        } else {
                            $errMsg[$id] = 'not null制約により更新失敗';
                            continue;
                        }
                    } else {
                        if ($count > 0)  {
                            $sql .= ", ";
                        }
                        $sql .= $column . " = ? ";
                        $params[] = $value;
                    }
                    $logArr[] = $column . ':' . $nowData[$column] . '[→]' . $value;
                    if (mb_strlen($nowData[$column]) > 50) {

                    } else {

                    }

                    // 結果表示用
                    if (strstr($nowData[$column],"\n")) {
                        $val1 = '…(改行ありのため省略)';
                    } else {
                        $val1 = $nowData[$column];
                    }
                    if (strstr($value,"\n")) {
                        $val2 = '…(改行ありのため省略)';
                    } else {
                        $val2 = $value;
                    }
                    $updateColumnNameArr[$id] = $tableName . '|++|' . $primaryKey . '|++|' . $column . '　：　' . $val1 . '　→　' . $val2;
                    $count++;
                }
                if ($count > 0) {
                    $column = $prefix . '_update_datetime';
                    if (isset($columnData[$column])) {
                        $id = $tableName . '||' . $column . '||' . $primaryKeyList[$tableName] . '||' . $primaryKey;
                        $logArr[] = $column . ':' . $nowData[$column] . '[→]' . $now;
                        $updateColumnNameArr[$id] = $tableName . '|++|' . $primaryKey . '|++|' . $column . '　：　' . $nowData[$column] . '　→　' . $now;
                        $sql .= ", " . $column . " = ? ";
                        $params[] = $now;
                    }

                    $column = $prefix . '_update_timestamp';
                    if (isset($columnData[$column])) {
                        $id = $tableName . '||' . $column . '||' . $primaryKeyList[$tableName] . '||' . $primaryKey;
                        $logArr[] = $column . ':' . $nowData[$column] . '[→]' . $now;
                        $updateColumnNameArr[$id] = $tableName . '|++|' . $primaryKey . '|++|' . $column . '　：　' . $nowData[$column] . '　→　' . $now;
                        $sql .= ", " . $column . " = ? ";
                        $params[] = $now;
                    }

                    $sql .= " WHERE ";
                    $sql .= $primaryKeyList[$tableName] . " = " . $primaryKey;

                    // テーブルが指示ステータスの場合、指示ステータス履歴にデータを残す
                    if ($tableName === 'trx_direction_status') {
                        $result = $class->insertDirectionStatusHistory($primaryKey);
                    }

                    // SQL実行
                    $res = $this->_db->query($sql,$params);
                    file_put_contents($filePath,'[実行日:' . Mikoshiva_date::mikoshivaNow() . ']|++|' . implode('|++|',$logArr) . "\n",FILE_APPEND);
                }
            }
        }
        $actionForm->setUpdateColumnNameArr($updateColumnNameArr);
        $actionForm->setErrMsg($errMsg);

        if (count($updateColumnNameArr) > 0) {
            $tableList     = $actionForm->getTableList();
            $tableNameList = array();
            $data          = array();
            foreach ($tableList as $k => $v) {
                $tableNameList[$v['table']] = $v['name'];
            }

            foreach ($updateColumnNameArr as $k => $v) {
                $tempArr = explode('|++|',$v);
                $data[$tempArr[0]][$tempArr[1]][] = $tempArr[2];
            }

            $updateMsg = '更新完了！\n';

            $updateMsg .= '----------------------------------------------\n';

            foreach ($data as $tableName => $v) {
                $updateMsg .= '【' . $tableNameList[$tableName] . '】\n';

                foreach ($v as $primaryKey => $v2) {
                    $updateMsg .= '■' . $primaryKeyList[$tableName] . '：' . $primaryKey . '\n';

                    foreach ($v2 as $k => $v3) {
                        $no = mb_convert_kana(($k + 1),'N');
                        $updateMsg .= $no . '．' . $v3 . '\n';
                    }
                    $updateMsg .= '\n';
                }

                $updateMsg .= '----------------------------------------------\n';
            }


            $actionForm->setUpdateMsg($updateMsg);
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return true;
    }
}
