<?php
/** @package Mikoshiva_Controller */

/**
 * Model_Controller_DB_Exceptoin
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Controller_Model_DB_Exception extends Exception {

}

