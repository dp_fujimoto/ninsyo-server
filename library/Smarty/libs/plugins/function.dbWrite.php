<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


/**
 * データベースからプライマリーキーによって値を取得し、
 * 指定されたカラムの値を返します。
 * 複数のカラムを指定する場合は配列を用います。
 * 　　この時、セパレータを指定することが可能です。
 *
 * @param array    $params
 *                      string     arClassName          アクティブレコード名
 *                      string     pkey                 プライマリーキーの値（複合プライマリーキー未対応）
 *                      string     column               カラム名、またはカラム名の配列(array(column1, column2, ...)
 *                      string     separator   空文字 | 戻り値のセパレータ(column1_data[separator]column2_data[separator]...columnN_data)
 * @param Smarty   $smarty
 *
 * @return string
 *
 * @package Smarty_libs
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.dbWrite.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_dbWrite($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);

    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['arClassName']  = '';
    $default['pkey']         = '';
    $default['column']       = '';
    $default['separator']    = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['arClassName']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 arClassName に値がありません。');
    }
    if(isBlankOrNull($data['pkey']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 pkey に値がありません。');
    }
    if(isBlankOrNull($data['column']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 column に値がありません。');
    }

    $arClassName = $data['arClassName'];
    $pkey        = $data['pkey'];
    $column      = $data['column'];
    $separator   = $data['separator'];

    //-------------------------------------------------------------------------------------------
    //■テーブルからリストを取得
    //-------------------------------------------------------------------------------------------
    /* @var $arClass Zend_Db_Table_Abstract */
    $arFileName = '';
    require_once str_replace('_', '/', $arClassName) . '.php';
    $arClass = new $arClassName();
    $select  = $arClass->select();

    $select->from($arClass, $column);

    //テーブル情報からプライマリーキーを取得
    $primaryList = $arClass->info('primary');
    //プライマリーキーがなかったら空文字を返す
    if(!isset($primaryList)) return '';
    $select = $arClass->select();
    $select->where($primaryList[1] . ' = ?', $pkey);

    // 取得
    $row = $arClass->fetchRow($select);

    // 取得件数が 0 件で有れば空文字を返す
    if (count($row) === 0) return '';


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    // 配列から出力用の文字列を作成
    if(is_array($column)) {
        $d = array();
        foreach($column as $key) {
            $d[] = $row[$key];
        }
        $result = implode($separator, $d);
    } else {
        $result = $row[$column];
    }

    Mikoshiva_Logger::info(LOG_END);
    return $result;
}



?>
