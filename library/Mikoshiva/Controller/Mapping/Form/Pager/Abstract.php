<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * -----------------------------------------------------------------------
 *  ワンポイント
 * -----------------------------------------------------------------------
 *  ■クラス名
 *  アクションフォームクラスのクラス名は、
 *  必ず「～Form」としてください。
 *
 *  ■resetメソッド
 *  任意の初期化処理を行う場合に実装します。
 *  (メソッド定義は省略可能です)
 *
 *  ■validateメソッド
 *  任意のバリデート処理を行う場合に実装します。(独自バリデート)
 *  validation.yml定義による固定バリデート処理を実行するには、
 *  スーパークラスのvalidateメソッドを呼び出してください。
 *  「parent::reset($config, $request);」
 *  その場合、スーパークラスのvalidateメソッドが返す
 *  固定バリデートの実行結果値は、適切に処理してください。
 *  (メソッド定義は省略可能です)
 * -----------------------------------------------------------------------
 *
 * @author      kawakami <kawakami@kaihatsu.com>
 * @since       2010/02/15
 * @version     SVN: $Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 *
 */
abstract class Mikoshiva_Controller_Mapping_Form_Pager_Abstract
    extends Mikoshiva_Controller_Mapping_Form_Abstract {
    public function __construct(array $confProperties, array $param) { 
        $additionalProperties = array();
        $additionalProperties['pagerCurrentPage'] = ''; // 現在のページ数を保持
        $additionalProperties['pagerPerPage']     = ''; // 1ページ毎の件数を保持
        $additionalProperties['pagerBar']         = ''; // リンク文字列を保持
        $additionalProperties['pagerTotalCount']  = ''; // 全体のヒット件数を保持
        $confProperties = $additionalProperties + $confProperties;
        parent::__construct($confProperties, $param);
    }

}
