<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Exception.php';

/**
 * Integer ユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/15
 * @version SVN:$Id: Integer.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Integer {

    /**
     * ID(bit)からintValueを作成して返します。
     * 与えられた値(配列の値)をビット扱いとし
     * 最大31までの複数項目をint型の値によって表します。
     *
     * @param int | array $bits
     * @return int $intValue | null
     *
     * @author (base) fujimoto <fujimoto0720@gmail.com>
     */
    public static function convertBitsToIntValue($bits) {
        logInfo(LOG_START);
        Mikoshiva_Logger::debug('START convertBitsToIntValue(' . $bits . ')');

        // 初期化
        $bitsArr  = array();
        $intValue = 0;

        // IDが配列でない場合
        if (!is_array($bits)) {
            $bitsArr[0] = $bits;

        // IDが配列の場合
        } else {
            // 重複したIDを削除してID配列に詰め替える
            $bitsArr = array_unique($bits);
        }

        //dumper($bitsArr);

        // IDからintValueを作成する
        // intValue値はビット演算に使用される
        foreach ($bitsArr as $k => $v) {

            // 数字以外もしくは値が32以上であればエラーなのでdivisionValueを0にしてループ終了
            if (!numberCheck($v) || $v > 31) {
                throw new Mikoshiva_Utility_Exception(
                    '【Mikoshiva_Utility_Integer#convertBitToIntValue】数値以外、又は、32以上の値が指定されました。');
            }

            // もしIDに0が含まれていれば全てに該当する数値を代入しループ終了
            if ((int)$v === 0) {
                // int4は32ビットで、先頭1ビットは符号を表すのに使うため、残り31ビット分のを表現できる。 ※31までの制限が発生する
                // 全てのビットを立てた値は 2^0 + 2^1 + 2^2 +・・・・・ 2^30 + 2^31 = 2147483647
                $intValue =  2147483647;
                break;
            }

            // IDに対するビットを立てる。例） IDが３の場合 ＝ ２の３乗 ＝ 2^3 ＝ 1 << (3-1)
            $intValue += 1 << ($v - 1);
        }

        // 値が0の場合、DBに値を保持しないようにnullにする
        if ($intValue === 0) {
            $intValue = null;
        }

        logInfo(LOG_END);
        return $intValue;
    }

    /**
     * intValueからをID(bit)を格納した配列を作成します。
     *
     * @param $int
     * @return array $bits
     *
     * @author (base) fujimoto <fujimoto0720@gmail.com>
     */
    public static function convertIntToBitsValue($int) {
        logInfo(LOG_START);
        Mikoshiva_Logger::debug('START convertIntToBitsValue(' . $int . ')');

        // もしIDが取得できない場合はnullを返す
        if (isBlankOrNull($int)) {
            return array();
        }

        $bitsArr = array();

        // 引数にint値より大きい値であれば0を返す
        if ($int >= '2147483647') {
            $bitsArr[] = '0';

        } else {

            // ＆演算を行うため、最大でも31ID(bit)しか存在できない
            for ($i = 1; $i < 32; $i++) {

                // もしintValueと$iの&演算の結果が正なら$iをID(bit)として格納する
                $tempValue = pow(2,($i - 1));
                if ((int)$int & $tempValue) {
                    $bitsArr[] = (int)$i;
                }
            }
        }

        logInfo(LOG_END);
        return $bitsArr;
    }

}