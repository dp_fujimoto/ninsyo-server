<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Alpha
 */
require_once 'Zend/Validate/Alpha.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Alpha.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Alpha extends Zend_Validate_Alpha
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_ALPHA    => "%COLUMN_NAME%にアルファベット以外の文字が含まれています",
        self::STRING_EMPTY => "%COLUMN_NAME%は空の文字列です"
    );
}
