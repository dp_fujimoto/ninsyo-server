<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Action/Abstract.php';


/**
 * Mikoshiva_Controller_Action_Admin_Abstract
 *
 * FRONT に使用する Action Controller はこの抽象クラスを継承すること
 *
 * レイアウトの表示を削除しています
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/03
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Action_Front_Abstract extends Mikoshiva_Controller_Action_Abstract {


    /**
     * Action メソッドの処理の前に認証チェックを行います
     * 認証に失敗した場合はログイン画面へ強制的に遷移します
     *
     * init() の後に処理されます
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see library/Zend/Controller/Zend_Controller_Action#preDispatch()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/04
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function preDispatch() {


        // レイアウトを非表示
        $this->_helper->layout->disableLayout();

        // 親メソッドを処理
        parent::preDispatch();
    }

}





















