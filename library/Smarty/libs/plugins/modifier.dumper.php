<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */


/**
 * テンプレート上で与えられた引数の内容をダンプします。
 *
 * 第 3 引数をデフォルト値の true にすると 2 度表示されるので false にします
 *
 * 使用例
 * {$form|dumper}
 *
 * Zend_Debug::dump をラップしたものです。
 *
 * @param mixed  $var   ダンプしたい変数
 * @param string $label null|OPTIONAL Label to prepend to output.
 * @param bool   $echo  true|OPTIONAL Echo output if true.
 * @return string
 * @link http://smarty.php.net/manual/en/language.modifier.cat.php cat
 *          (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @version 1.0
 */
function smarty_modifier_dumper($var, $label = null, $echo = false)
{
    return dumper($var, $label, $echo);
}


