<!-- // メインテーブル開始 -->
<table id="COM_painSingle">

    <tr>

        <!-- // ペイン開始 -->
        <td>

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">{if !isBlankOrNull($form->getStaffId())}スタッフ変更　完了{else}スタッフ登録　完了{/if}</h2>

            <span>{if !isBlankOrNull($form->getStaffId())}変更{else}登録{/if}しました。</span>
            <br>
            <br>
            <br>
            {closeTabButton name='タブを閉じる' tabId=$tabId}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->