<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProduct extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mpr';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productId' => null,
        'divisionId' => null,
        'shopId' => null,
        'licenseId' => null,
        'productCode' => null,
        'productName' => null,
        'continuityShippingDay' => null,
        'forceContinuityShippingFlag' => null,
        'servicePeriodChargeFlag' => null,
        'firstStockShortageFlag' => null,
        'continuityStockShortageFlag' => null,
        'seminarFlag' => null,
        'consultingFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 商品ID の値を返します
	 *
     * @return int 商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * 商品ID の値をセットします
	 *
	 * @param int $productId 商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * ライセンサーID の値を返します
	 *
     * @return int ライセンサーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLicenseId() {
		return $this->_properties['licenseId'];
    }


    /**
     * ライセンサーID の値をセットします
	 *
	 * @param int $licenseId ライセンサーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLicenseId($licenseId) {
		$this->_properties['licenseId'] = $licenseId;
    }


    /**
     * 商品コード の値を返します
	 *
     * @return varchar 商品コード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductCode() {
		return $this->_properties['productCode'];
    }


    /**
     * 商品コード の値をセットします
	 *
	 * @param varchar $productCode 商品コード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductCode($productCode) {
		$this->_properties['productCode'] = $productCode;
    }


    /**
     * 商品名 の値を返します
	 *
     * @return varchar 商品名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductName() {
		return $this->_properties['productName'];
    }


    /**
     * 商品名 の値をセットします
	 *
	 * @param varchar $productName 商品名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductName($productName) {
		$this->_properties['productName'] = $productName;
    }


    /**
     * 継続発送日(一斉発送日) の値を返します
	 *
     * @return int 継続発送日(一斉発送日)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityShippingDay() {
		return $this->_properties['continuityShippingDay'];
    }


    /**
     * 継続発送日(一斉発送日) の値をセットします
	 *
	 * @param int $continuityShippingDay 継続発送日(一斉発送日)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityShippingDay($continuityShippingDay) {
		$this->_properties['continuityShippingDay'] = $continuityShippingDay;
    }


    /**
     * 継続発送日強制フラグ の値を返します
	 *
     * @return char 継続発送日強制フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getForceContinuityShippingFlag() {
		return $this->_properties['forceContinuityShippingFlag'];
    }


    /**
     * 継続発送日強制フラグ の値をセットします
	 *
	 * @param char $forceContinuityShippingFlag 継続発送日強制フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setForceContinuityShippingFlag($forceContinuityShippingFlag) {
		$this->_properties['forceContinuityShippingFlag'] = $forceContinuityShippingFlag;
    }


    /**
     * サービス期間課金フラグ の値を返します
	 *
     * @return char サービス期間課金フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServicePeriodChargeFlag() {
		return $this->_properties['servicePeriodChargeFlag'];
    }


    /**
     * サービス期間課金フラグ の値をセットします
	 *
	 * @param char $servicePeriodChargeFlag サービス期間課金フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServicePeriodChargeFlag($servicePeriodChargeFlag) {
		$this->_properties['servicePeriodChargeFlag'] = $servicePeriodChargeFlag;
    }


    /**
     * 初回発送在庫不足フラグ の値を返します
	 *
     * @return char 初回発送在庫不足フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstStockShortageFlag() {
		return $this->_properties['firstStockShortageFlag'];
    }


    /**
     * 初回発送在庫不足フラグ の値をセットします
	 *
	 * @param char $firstStockShortageFlag 初回発送在庫不足フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstStockShortageFlag($firstStockShortageFlag) {
		$this->_properties['firstStockShortageFlag'] = $firstStockShortageFlag;
    }


    /**
     * 一斉発送在庫不足フラグ の値を返します
	 *
     * @return char 一斉発送在庫不足フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityStockShortageFlag() {
		return $this->_properties['continuityStockShortageFlag'];
    }


    /**
     * 一斉発送在庫不足フラグ の値をセットします
	 *
	 * @param char $continuityStockShortageFlag 一斉発送在庫不足フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityStockShortageFlag($continuityStockShortageFlag) {
		$this->_properties['continuityStockShortageFlag'] = $continuityStockShortageFlag;
    }


    /**
     * セミナーフラグ の値を返します
	 *
     * @return char セミナーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSeminarFlag() {
		return $this->_properties['seminarFlag'];
    }


    /**
     * セミナーフラグ の値をセットします
	 *
	 * @param char $seminarFlag セミナーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSeminarFlag($seminarFlag) {
		$this->_properties['seminarFlag'] = $seminarFlag;
    }


    /**
     * mpr_consulting_flag の値を返します
	 *
     * @return char mpr_consulting_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getConsultingFlag() {
		return $this->_properties['consultingFlag'];
    }


    /**
     * mpr_consulting_flag の値をセットします
	 *
	 * @param char $consultingFlag mpr_consulting_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setConsultingFlag($consultingFlag) {
		$this->_properties['consultingFlag'] = $consultingFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























