<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_dm_campaign_search_product
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstDmCampaignSearchProduct extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'msp';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'dmCampaignSearchProductId' => null,
        'dmCampaignId' => null,
        'number' => null,
        'productId1' => null,
        'contractStatusType1' => null,
        'fromProposalDayNumber1' => null,
        'toProposalDayNumber1' => null,
        'productId2' => null,
        'contractStatusType2' => null,
        'fromProposalDayNumber2' => null,
        'toProposalDayNumber2' => null,
        'productId3' => null,
        'contractStatusType3' => null,
        'fromProposalDayNumber3' => null,
        'toProposalDayNumber3' => null,
        'productId4' => null,
        'contractStatusType4' => null,
        'fromProposalDayNumber4' => null,
        'toProposalDayNumber4' => null,
        'productId5' => null,
        'contractStatusType5' => null,
        'fromProposalDayNumber5' => null,
        'toProposalDayNumber5' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * ダイレクトメールキャンペーン検索条件商品ID の値を返します
	 *
     * @return int ダイレクトメールキャンペーン検索条件商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignSearchProductId() {
		return $this->_properties['dmCampaignSearchProductId'];
    }


    /**
     * ダイレクトメールキャンペーン検索条件商品ID の値をセットします
	 *
	 * @param int $dmCampaignSearchProductId ダイレクトメールキャンペーン検索条件商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignSearchProductId($dmCampaignSearchProductId) {
		$this->_properties['dmCampaignSearchProductId'] = $dmCampaignSearchProductId;
    }


    /**
     * ダイレクトメールキャンペーンID の値を返します
	 *
     * @return int ダイレクトメールキャンペーンID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDmCampaignId() {
		return $this->_properties['dmCampaignId'];
    }


    /**
     * ダイレクトメールキャンペーンID の値をセットします
	 *
	 * @param int $dmCampaignId ダイレクトメールキャンペーンID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDmCampaignId($dmCampaignId) {
		$this->_properties['dmCampaignId'] = $dmCampaignId;
    }


    /**
     * 条件順位 の値を返します
	 *
     * @return int 条件順位
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNumber() {
		return $this->_properties['number'];
    }


    /**
     * 条件順位 の値をセットします
	 *
	 * @param int $number 条件順位
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNumber($number) {
		$this->_properties['number'] = $number;
    }


    /**
     * 商品ID1 の値を返します
	 *
     * @return int 商品ID1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId1() {
		return $this->_properties['productId1'];
    }


    /**
     * 商品ID1 の値をセットします
	 *
	 * @param int $productId1 商品ID1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId1($productId1) {
		$this->_properties['productId1'] = $productId1;
    }


    /**
     * 契約状態タイプ1 の値を返します
	 *
     * @return varchar 契約状態タイプ1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContractStatusType1() {
		return $this->_properties['contractStatusType1'];
    }


    /**
     * 契約状態タイプ1 の値をセットします
	 *
	 * @param varchar $contractStatusType1 契約状態タイプ1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContractStatusType1($contractStatusType1) {
		$this->_properties['contractStatusType1'] = $contractStatusType1;
    }


    /**
     * msp_from_proposal_day_number1 の値を返します
	 *
     * @return int msp_from_proposal_day_number1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromProposalDayNumber1() {
		return $this->_properties['fromProposalDayNumber1'];
    }


    /**
     * msp_from_proposal_day_number1 の値をセットします
	 *
	 * @param int $fromProposalDayNumber1 msp_from_proposal_day_number1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromProposalDayNumber1($fromProposalDayNumber1) {
		$this->_properties['fromProposalDayNumber1'] = $fromProposalDayNumber1;
    }


    /**
     * msp_to_proposal_day_number1 の値を返します
	 *
     * @return int msp_to_proposal_day_number1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToProposalDayNumber1() {
		return $this->_properties['toProposalDayNumber1'];
    }


    /**
     * msp_to_proposal_day_number1 の値をセットします
	 *
	 * @param int $toProposalDayNumber1 msp_to_proposal_day_number1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToProposalDayNumber1($toProposalDayNumber1) {
		$this->_properties['toProposalDayNumber1'] = $toProposalDayNumber1;
    }


    /**
     * 商品ID2 の値を返します
	 *
     * @return int 商品ID2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId2() {
		return $this->_properties['productId2'];
    }


    /**
     * 商品ID2 の値をセットします
	 *
	 * @param int $productId2 商品ID2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId2($productId2) {
		$this->_properties['productId2'] = $productId2;
    }


    /**
     * 契約状態タイプ2 の値を返します
	 *
     * @return varchar 契約状態タイプ2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContractStatusType2() {
		return $this->_properties['contractStatusType2'];
    }


    /**
     * 契約状態タイプ2 の値をセットします
	 *
	 * @param varchar $contractStatusType2 契約状態タイプ2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContractStatusType2($contractStatusType2) {
		$this->_properties['contractStatusType2'] = $contractStatusType2;
    }


    /**
     * msp_from_proposal_day_number2 の値を返します
	 *
     * @return int msp_from_proposal_day_number2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromProposalDayNumber2() {
		return $this->_properties['fromProposalDayNumber2'];
    }


    /**
     * msp_from_proposal_day_number2 の値をセットします
	 *
	 * @param int $fromProposalDayNumber2 msp_from_proposal_day_number2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromProposalDayNumber2($fromProposalDayNumber2) {
		$this->_properties['fromProposalDayNumber2'] = $fromProposalDayNumber2;
    }


    /**
     * msp_to_proposal_day_number2 の値を返します
	 *
     * @return int msp_to_proposal_day_number2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToProposalDayNumber2() {
		return $this->_properties['toProposalDayNumber2'];
    }


    /**
     * msp_to_proposal_day_number2 の値をセットします
	 *
	 * @param int $toProposalDayNumber2 msp_to_proposal_day_number2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToProposalDayNumber2($toProposalDayNumber2) {
		$this->_properties['toProposalDayNumber2'] = $toProposalDayNumber2;
    }


    /**
     * 商品ID3 の値を返します
	 *
     * @return int 商品ID3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId3() {
		return $this->_properties['productId3'];
    }


    /**
     * 商品ID3 の値をセットします
	 *
	 * @param int $productId3 商品ID3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId3($productId3) {
		$this->_properties['productId3'] = $productId3;
    }


    /**
     * 契約状態タイプ3 の値を返します
	 *
     * @return varchar 契約状態タイプ3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContractStatusType3() {
		return $this->_properties['contractStatusType3'];
    }


    /**
     * 契約状態タイプ3 の値をセットします
	 *
	 * @param varchar $contractStatusType3 契約状態タイプ3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContractStatusType3($contractStatusType3) {
		$this->_properties['contractStatusType3'] = $contractStatusType3;
    }


    /**
     * msp_from_proposal_day_number3 の値を返します
	 *
     * @return int msp_from_proposal_day_number3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromProposalDayNumber3() {
		return $this->_properties['fromProposalDayNumber3'];
    }


    /**
     * msp_from_proposal_day_number3 の値をセットします
	 *
	 * @param int $fromProposalDayNumber3 msp_from_proposal_day_number3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromProposalDayNumber3($fromProposalDayNumber3) {
		$this->_properties['fromProposalDayNumber3'] = $fromProposalDayNumber3;
    }


    /**
     * msp_to_proposal_day_number3 の値を返します
	 *
     * @return int msp_to_proposal_day_number3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToProposalDayNumber3() {
		return $this->_properties['toProposalDayNumber3'];
    }


    /**
     * msp_to_proposal_day_number3 の値をセットします
	 *
	 * @param int $toProposalDayNumber3 msp_to_proposal_day_number3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToProposalDayNumber3($toProposalDayNumber3) {
		$this->_properties['toProposalDayNumber3'] = $toProposalDayNumber3;
    }


    /**
     * 商品ID4 の値を返します
	 *
     * @return int 商品ID4
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId4() {
		return $this->_properties['productId4'];
    }


    /**
     * 商品ID4 の値をセットします
	 *
	 * @param int $productId4 商品ID4
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId4($productId4) {
		$this->_properties['productId4'] = $productId4;
    }


    /**
     * 契約状態タイプ4 の値を返します
	 *
     * @return varchar 契約状態タイプ4
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContractStatusType4() {
		return $this->_properties['contractStatusType4'];
    }


    /**
     * 契約状態タイプ4 の値をセットします
	 *
	 * @param varchar $contractStatusType4 契約状態タイプ4
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContractStatusType4($contractStatusType4) {
		$this->_properties['contractStatusType4'] = $contractStatusType4;
    }


    /**
     * msp_from_proposal_day_number4 の値を返します
	 *
     * @return int msp_from_proposal_day_number4
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromProposalDayNumber4() {
		return $this->_properties['fromProposalDayNumber4'];
    }


    /**
     * msp_from_proposal_day_number4 の値をセットします
	 *
	 * @param int $fromProposalDayNumber4 msp_from_proposal_day_number4
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromProposalDayNumber4($fromProposalDayNumber4) {
		$this->_properties['fromProposalDayNumber4'] = $fromProposalDayNumber4;
    }


    /**
     * msp_to_proposal_day_number4 の値を返します
	 *
     * @return int msp_to_proposal_day_number4
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToProposalDayNumber4() {
		return $this->_properties['toProposalDayNumber4'];
    }


    /**
     * msp_to_proposal_day_number4 の値をセットします
	 *
	 * @param int $toProposalDayNumber4 msp_to_proposal_day_number4
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToProposalDayNumber4($toProposalDayNumber4) {
		$this->_properties['toProposalDayNumber4'] = $toProposalDayNumber4;
    }


    /**
     * 商品ID5 の値を返します
	 *
     * @return int 商品ID5
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId5() {
		return $this->_properties['productId5'];
    }


    /**
     * 商品ID5 の値をセットします
	 *
	 * @param int $productId5 商品ID5
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId5($productId5) {
		$this->_properties['productId5'] = $productId5;
    }


    /**
     * 契約状態タイプ5 の値を返します
	 *
     * @return varchar 契約状態タイプ5
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContractStatusType5() {
		return $this->_properties['contractStatusType5'];
    }


    /**
     * 契約状態タイプ5 の値をセットします
	 *
	 * @param varchar $contractStatusType5 契約状態タイプ5
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContractStatusType5($contractStatusType5) {
		$this->_properties['contractStatusType5'] = $contractStatusType5;
    }


    /**
     * msp_from_proposal_day_number5 の値を返します
	 *
     * @return int msp_from_proposal_day_number5
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromProposalDayNumber5() {
		return $this->_properties['fromProposalDayNumber5'];
    }


    /**
     * msp_from_proposal_day_number5 の値をセットします
	 *
	 * @param int $fromProposalDayNumber5 msp_from_proposal_day_number5
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromProposalDayNumber5($fromProposalDayNumber5) {
		$this->_properties['fromProposalDayNumber5'] = $fromProposalDayNumber5;
    }


    /**
     * msp_to_proposal_day_number5 の値を返します
	 *
     * @return int msp_to_proposal_day_number5
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToProposalDayNumber5() {
		return $this->_properties['toProposalDayNumber5'];
    }


    /**
     * msp_to_proposal_day_number5 の値をセットします
	 *
	 * @param int $toProposalDayNumber5 msp_to_proposal_day_number5
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToProposalDayNumber5($toProposalDayNumber5) {
		$this->_properties['toProposalDayNumber5'] = $toProposalDayNumber5;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























