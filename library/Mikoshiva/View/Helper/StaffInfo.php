<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Db/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Zend/Registry.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Ar/MstStaff.php';

/**
 * スタッフ情報を返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/11
 * @version SVN:$Id: StaffInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_StaffInfo extends Mikoshiva_View_Helper_Abstract {



    /**
     * スタッフID及び、カラム名を元に、スタッフ情報を返します
     *
     * @param  string        $staffId    スタッフID
     * @param  string|array  $column     カラム名、またはカラム名の配列(array(column1, column2, ...)
     * @param  string        $separater  戻り値のセパレータ(column1_data[separater]column2_data[separater]...columnN_data)
     * @return string        $result
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/21
     * @version SVN:$Id: StaffInfo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function staffInfo($staffId, $column = 'mst_last_name', $separater = '') {
        logInfo(LOG_START);

        if(numberCheck($staffId)) {
            $staffId = (string)$staffId;
        } else {
            logInfo(LOG_END);
            return '';
        }

        if(isBlankOrNull($column)) {
            logInfo(LOG_END);
            return '';
        }

        //==========================================
        // スタッフテーブルからリストを取得
        //==========================================
        //キャッシュに存在する場合はキャッシュから取得
        $cache = Zend_Registry::get('cache');
        $staffList = array();
        $row       = array();
        if ( ! $row = $cache->load('Mikoshiva_View_Helper_StaffInfo') ) {
            $ar        = new Ar_MstStaff();
            $select    = $ar->select()->where('mst_delete_flag = ?', '0');
            $staffList = $ar->fetchAll( $select )->toArray();
            if(is_null($staffList)) {
                logInfo(LOG_END);
                return '';
            }

            // キャッシュに書き込み
            $cache->save( $staffList, 'Mikoshiva_View_Helper_StaffInfo');
        } else {

            // キャッシュから読み込み
            $staffList = $row;
        }

        //------------------------------------
        // リストから対象スタッフのデータを取得
        //------------------------------------
        $theStaff = array();
        foreach($staffList as $value) {
            if($value['mst_staff_id'] === $staffId) {
                $theStaff = $value;
                break;
            }
        }

        //対象スタッフが見つからなかった場合
        if(!isset($theStaff)) {
            logInfo(LOG_END);
            return '';
        }

        //------------------------------------
        // データを出力用の文字列に変換
        //------------------------------------
        $result = '';
        if(is_array($column)) { //第二引数が配列の場合
            foreach($column as $v) {
                $info[] = $theStaff[$v];
            }
            $result = implode($separater, $info);
        } else {                //第二引数が配列でない場合
            $result = $theStaff[$column];
        }

        logInfo(LOG_END);
        return $result;
    }

}















