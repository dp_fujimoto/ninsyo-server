<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_access_log_analysis_detail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxAccessLogAnalysisDetail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tad';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'accessLogAnalysisDetailId' => null,
        'accessLogAnalysisId' => null,
        'accessLogDetailType' => null,
        'accessCount' => null,
        'userCount' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tad_access_log_analysis_detail_id の値を返します
	 *
     * @return int tad_access_log_analysis_detail_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogAnalysisDetailId() {
		return $this->_properties['accessLogAnalysisDetailId'];
    }


    /**
     * tad_access_log_analysis_detail_id の値をセットします
	 *
	 * @param int $accessLogAnalysisDetailId tad_access_log_analysis_detail_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogAnalysisDetailId($accessLogAnalysisDetailId) {
		$this->_properties['accessLogAnalysisDetailId'] = $accessLogAnalysisDetailId;
    }


    /**
     * tad_access_log_analysis_id の値を返します
	 *
     * @return int tad_access_log_analysis_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogAnalysisId() {
		return $this->_properties['accessLogAnalysisId'];
    }


    /**
     * tad_access_log_analysis_id の値をセットします
	 *
	 * @param int $accessLogAnalysisId tad_access_log_analysis_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogAnalysisId($accessLogAnalysisId) {
		$this->_properties['accessLogAnalysisId'] = $accessLogAnalysisId;
    }


    /**
     * tad_access_log_detail_type の値を返します
	 *
     * @return varchar tad_access_log_detail_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogDetailType() {
		return $this->_properties['accessLogDetailType'];
    }


    /**
     * tad_access_log_detail_type の値をセットします
	 *
	 * @param varchar $accessLogDetailType tad_access_log_detail_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogDetailType($accessLogDetailType) {
		$this->_properties['accessLogDetailType'] = $accessLogDetailType;
    }


    /**
     * tad_access_count の値を返します
	 *
     * @return int tad_access_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessCount() {
		return $this->_properties['accessCount'];
    }


    /**
     * tad_access_count の値をセットします
	 *
	 * @param int $accessCount tad_access_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessCount($accessCount) {
		$this->_properties['accessCount'] = $accessCount;
    }


    /**
     * tad_user_count の値を返します
	 *
     * @return int tad_user_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserCount() {
		return $this->_properties['userCount'];
    }


    /**
     * tad_user_count の値をセットします
	 *
	 * @param int $userCount tad_user_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserCount($userCount) {
		$this->_properties['userCount'] = $userCount;
    }


    /**
     * tad_memo の値を返します
	 *
     * @return mediumtext tad_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * tad_memo の値をセットします
	 *
	 * @param mediumtext $memo tad_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * tad_delete_flag の値を返します
	 *
     * @return char tad_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tad_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tad_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tad_deletion_datetime の値を返します
	 *
     * @return datetime tad_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tad_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tad_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tad_registration_datetime の値を返します
	 *
     * @return datetime tad_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tad_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tad_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tad_update_datetime の値を返します
	 *
     * @return datetime tad_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tad_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tad_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tad_update_timestamp の値を返します
	 *
     * @return timestamp tad_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tad_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tad_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























