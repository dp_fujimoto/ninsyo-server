<?php
/* @package Err */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Action/Abstract.php';
require_once 'Mikoshiva/Logger.php';
/**
 * 実践的なエラーコントローラ
 */
/*
// コンポーネントをロードする
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Config/Ini.php';
require_once 'Zend/Log.php';
require_once 'Zend/Log/Writer/Stream.php';
*/
class Error_ErrorController extends Mikoshiva_Controller_Action_Abstract
{
/*
    public function errorAction()
    {
        // 設定ファイルから動作モードを取得する
        //$config = new Zend_Config_Ini('../application/lib/config.ini', null);
        //$debugFlg = ($config->debug == '1') ? true : false;

        // レスポンスオブジェクトを取得する
        $response = $this->getResponse();

        // エラーハンドラを取得する
        $errors = $this->_getParam('error_handler');

        // エラーの種別ごとにメッセージを表示する
        switch ($errors->type) {

            // コントローラが見つからない場合
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $response->setRawHeader('HTTP/1.1 404 Not Found');
                $this->_helper->viewRenderer->renderScript('error/404.phtml');
                break;

            // その他のエラーの場合
            default:

                // メッセージがセットされていればそれを表示する
                $exception = $errors->exception;
                if ($exception !== null) {

                    // デバッグモードならエラーメッセージをセットする

                        //$this->view->message = $exception->getTrace();
                        //print_r($exception->getMessage());
                        //Mikoshiva_Debug_VarDump::exec($exception->getTraceAsString() );

                    // リリースモードならエラーをログファイルへ記録する
                    Mikoshiva_Logger::emerg(
                            $exception->getMessage()
                            . "\n"
                            . $exception->getTraceAsString()
                    );
                        ///$logger->log($exception->getTraceAsString(), Zend_Log::ERR);

                        $debug  = nl2br(print_r($exception->getMessage(), true));
                        $debug .= '<br><br>';
                        $debug .= nl2br(print_r($exception->getTraceAsString() , true));


                        //-----------------------------------------
                        // メールの送信を行います
                        //-----------------------------------------
                        //$mail->readingAttachment(LOGS_DIR . '/error/' . date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt');
                        //$mail->setBodyText('テスト' . "\n" . $debug);
                        ////$mail->send();
                        Zend_Debug::dump($exception->getMessage());
                        Zend_Debug::dump($exception->getTraceAsString());
                        //echo $debug;
                        // エラー画面を表示する
                        //$this->view->message = 'システムエラーが発生しました。';
                }
                break;
        }
    }
    */
}
