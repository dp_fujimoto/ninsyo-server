<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Format - 配列をCSVにする</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="配列をCSVにする">配列をCSVにする</h1>



            <!-- ========================================
              ■配列をCSVにする
            ==========================================-->
            <h2 id="配列をCSVにする">配列をCSVにする</h2>

            <pre class="wiki">
<?php
echo preg_replace('/[\r\n]*/', '', highlight_string('<?php

        require_once \'mikoshiva/format/encode/csv.php\';
        $converter = new mikoshiva_format_encode_csv();

        // デフォルトでは"(ダブルクォート)で囲ったcsvに変換。データに"が含まれる場合は、""に変換。
        $data = array(\'a\', \'"b"\', \'c\');
        dumper($converter->process($data));
        
        // ダブルクォート無しの場合
        $data = array(\'a\', \'"b"\', \'c\');
        $converter->setformat(\'not_enclosed_in_double_quotes\'); // フォーマットを "ダブルクォート囲みなし" に変更
        dumper($converter->process($data));
?>
', true));
?>
</pre>
<pre class="wiki">
string(16) ""a","""b""","c"
"

string(8) "a,"b",c
"

「一行」にして返すので、最後に改行(\n)が追加される
</pre>











            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















