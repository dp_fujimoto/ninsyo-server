/**
 * fmanager
 *
 * Copyright 2011, kawakami
 * Date: 2011/02/09
 */
$(function() {
  // field
  var interval       = 500;
  var target         = false;
  var checkTimeoutId = false;
  var inputCache     = new InputCache();

  // class
  function Validation(_result, _message) {
    this.result  = _result ;
    this.message = _message;
  }
  function ValiRule(_name, _required, _maxlength, _regexps) {
    this.name      = _name;
    this.required  = _required;
    this.maxlength = _maxlength;
    this.regexps   = _regexps;
  }
  function ValiReg(_pattern, _message) {
    this.pattern = _pattern;
    this.message = _message;
  }
  function InputCache() {
    this.name  = "";
    this.value = "";
    this.setInputCache = function(jquery) {
      this.name  = jquery.attr("name");
      this.value = jquery.val();
    };
    this.isCheck = function(jquery) {
      return (jquery.attr("name") === this.name && jquery.val() === this.value);
    };
  }

  // function
  function getValidationRule() {
    var rules = new Array();
    rules["lastName"]                     = new ValiRule("※お名前(姓)は<br>",         true, 50,  null);
    rules["firstName"]                    = new ValiRule("※お名前(名)は<br>",         true, 50,  null);
    rules["lastNameKana"]                 = new ValiRule("※お名前カナ(姓)は<br>",     true, 50,  new Array( new ValiReg("^[ァ-ロワヲンー]*$", "全角カタカナ") ));
    rules["firstNameKana"]                = new ValiRule("※お名前カナ(名)は<br>",     true, 50,  new Array( new ValiReg("^[ァ-ロワヲンー]*$", "全角カタカナ") ));
    rules["zip1"]                         = new ValiRule("※郵便番号は<br>",           true, 3,   new Array( new ValiReg("^[0-9]{3}$", "半角数字") ));
    rules["zip2"]                         = new ValiRule("※郵便番号は<br>",           true, 4,   new Array( new ValiReg("^[0-9]{4}$", "半角数字") ));
    rules["address1"]                     = new ValiRule("※市区町村番地は<br>",       true, 100, null);
    rules["tel1"]                         = new ValiRule("※電話番号は<br>",           true, 3,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    rules["tel2"]                         = new ValiRule("※電話番号は<br>",           true, 4,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    rules["tel3"]                         = new ValiRule("※電話番号は<br>",           true, 4,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
//    rules["email"]                        = new ValiRule("※メールアドレス",           true, 250, new Array( new ValiReg("^[a-zA-Z0-9._+-]+@[a-zA-Z0-9][a-zA-Z0-9.-]+(\.[a-zA-Z]+)+$", "半角数値") ));
    rules["password"]                     = new ValiRule("※パスワードは<br>",         true, 20,  new Array( new ValiReg("^[0-9a-zA-Z]{4,20}$", "半角英数字(4桁以上)") ));
    rules["passwordConfirm"]              = new ValiRule("※パスワード確認は<br>",     true, 20,  new Array( new ValiReg("^[0-9a-zA-Z]{4,20}$", "半角英数字(4桁以上)") ));
    rules["cardNo"]                       = new ValiRule("※カード番号は<br>",         true, 16,  new Array( new ValiReg("^[0-9]{16}$", "半角数字(16桁)") ));
    rules["shippingAddressLastName"]      = new ValiRule("※受取人(姓)は<br>",         true, 50,  null);
    rules["shippingAddressFirstName"]     = new ValiRule("※受取人(名)は<br>",         true, 50,  null);
    rules["shippingAddressLastNameKana"]  = new ValiRule("※受取人カナ(姓)は<br>",     true, 50,  new Array( new ValiReg("^[ァ-ロワヲンー]*$", "全角カタカナ") ));
    rules["shippingAddressFirstNameKana"] = new ValiRule("※受取人カナ(名)は<br>",     true, 50,  new Array( new ValiReg("^[ァ-ロワヲンー]*$", "全角カタカナ") ));
    rules["shippingAddressZip1"]          = new ValiRule("※配送先郵便番号は<br>",     true, 3,   new Array( new ValiReg("^[0-9]{3}$", "半角数字") ));
    rules["shippingAddressZip2"]          = new ValiRule("※配送先郵便番号は<br>",     true, 4,   new Array( new ValiReg("^[0-9]{4}$", "半角数字") ));
    rules["shippingAddressAddress1"]      = new ValiRule("※配送先市区町村番地は<br>", true, 100, null);
    rules["shippingAddressTel1"]          = new ValiRule("※配送先電話番号は<br>",     true, 3,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    rules["shippingAddressTel2"]          = new ValiRule("※配送先電話番号は<br>",     true, 4,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    rules["shippingAddressTel3"]          = new ValiRule("※配送先電話番号は<br>",     true, 4,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    rules["expirationDateYear"]           = new ValiRule("※カード有効期限(年)<br>",   true, 2,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    rules["expirationDateMonth"]          = new ValiRule("※カード有効期限(月)<br>",   true, 2,   new Array( new ValiReg("^[0-9]*$", "半角数字") ));
    return rules;
  }
  function isValidation(inputName, inputValue) {
    var validation = new Validation(true, null);
    var value = $.trim(inputValue);
    var rules = getValidationRule();
    if (inputName in rules) {
      var rule = rules[inputName];
      // required
      if (rule.required !== null && rule.required === true) {
        if (value === "") {
          validation.result = false;
          validation.message = rule.name + "　「必須項目」です。";
          return validation;
        }
      }
      // maxlength
      if (rule.maxlenght !== null) {
        if (rule.maxlenght < value.length) {
          validation.result = false;
          validation.message = rule.name + "　「" + rule.maxlength + "以内」で<br>　入力して下さい。";
          return validation;
        }
      }
      // regexps
      if (rule.regexps !== null) {
        for (var i = 0; i < rule.regexps.length; i++) {
          var regExp = new RegExp(rule.regexps[i].pattern);
          if (! value.match(regExp)) {
            validation.result = false;
            validation.message = rule.name + "　「" + rule.regexps[i].message + "」で<br>　入力して下さい。";
            return validation;
          }
        }
      }
    }
    return validation;
  }
  function check(jquery, noticeFlag) {
    var validation = isValidation(jquery.attr("name"), jquery.val());
    if (validation.result === true) {
        jquery.removeClass("error");
        if (noticeFlag === true) {
            $("#tn_message").html("");
        }
    } else {
        jquery.addClass("error");
        if (noticeFlag === true) {
          $("#tn_message").html(validation.message);
        }
    }
    return validation;
  }
  function setInputCheckEvent() {
    if (! inputCache.isCheck(target)) {
      inputCache.setInputCache(target);
      check(target, true);
    }
  	checkTimeoutId = setTimeout(setInputCheckEvent, interval);
  }
  function clearInputCheckEvent() {
  	if (checkTimeoutId !== false) {
      clearTimeout(checkTimeoutId);
      checkTimeoutId = false;
    }
  }

  // main
  $(":text,:password").each(function() {
    check($(this), false);
  }).keyup(function(event) {
    target = $(event.target);
    clearInputCheckEvent();
    setInputCheckEvent();
  }).mouseup(function(event) {
    target = $(event.target);
    clearInputCheckEvent();
    setInputCheckEvent();
  });

  $("select[name=expirationDateYear],select[name=expirationDateMonth]").each(function() {
    check($(this), false);
  }).change(function(event) {
    target = $(event.target);
    clearInputCheckEvent();
    setInputCheckEvent();
  });

});