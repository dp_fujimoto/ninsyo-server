<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_operation_log
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstOperationLog extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mol';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'operationLogId' => null,
        'loginId' => null,
        'operationUrl' => null,
        'modelName' => null,
        'sql' => null,
        'sqlType' => null,
        'actionFormProperties' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * オペレーションログID の値を返します
	 *
     * @return int オペレーションログID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOperationLogId() {
		return $this->_properties['operationLogId'];
    }


    /**
     * オペレーションログID の値をセットします
	 *
	 * @param int $operationLogId オペレーションログID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOperationLogId($operationLogId) {
		$this->_properties['operationLogId'] = $operationLogId;
    }


    /**
     * ログインID（実行者） の値を返します
	 *
     * @return int ログインID（実行者）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLoginId() {
		return $this->_properties['loginId'];
    }


    /**
     * ログインID（実行者） の値をセットします
	 *
	 * @param int $loginId ログインID（実行者）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLoginId($loginId) {
		$this->_properties['loginId'] = $loginId;
    }


    /**
     * オペレーションURL の値を返します
	 *
     * @return varchar オペレーションURL
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOperationUrl() {
		return $this->_properties['operationUrl'];
    }


    /**
     * オペレーションURL の値をセットします
	 *
	 * @param varchar $operationUrl オペレーションURL
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOperationUrl($operationUrl) {
		$this->_properties['operationUrl'] = $operationUrl;
    }


    /**
     * Model名 の値を返します
	 *
     * @return varchar Model名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getModelName() {
		return $this->_properties['modelName'];
    }


    /**
     * Model名 の値をセットします
	 *
	 * @param varchar $modelName Model名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setModelName($modelName) {
		$this->_properties['modelName'] = $modelName;
    }


    /**
     * SQL の値を返します
	 *
     * @return text SQL
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSql() {
		return $this->_properties['sql'];
    }


    /**
     * SQL の値をセットします
	 *
	 * @param text $sql SQL
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSql($sql) {
		$this->_properties['sql'] = $sql;
    }


    /**
     * SQLタイプ の値を返します
	 *
     * @return varchar SQLタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSqlType() {
		return $this->_properties['sqlType'];
    }


    /**
     * SQLタイプ の値をセットします
	 *
	 * @param varchar $sqlType SQLタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSqlType($sqlType) {
		$this->_properties['sqlType'] = $sqlType;
    }


    /**
     * ActionFormプロパティ の値を返します
	 *
     * @return mediumtext ActionFormプロパティ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getActionFormProperties() {
		return $this->_properties['actionFormProperties'];
    }


    /**
     * ActionFormプロパティ の値をセットします
	 *
	 * @param mediumtext $actionFormProperties ActionFormプロパティ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setActionFormProperties($actionFormProperties) {
		$this->_properties['actionFormProperties'] = $actionFormProperties;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return varchar 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param varchar $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日 の値を返します
	 *
     * @return datetime 削除日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 更新日 の値を返します
	 *
     * @return datetime 更新日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 登録日 の値を返します
	 *
     * @return datetime 登録日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * タイムスタンプ の値を返します
	 *
     * @return timestamp タイムスタンプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * タイムスタンプ の値をセットします
	 *
	 * @param timestamp $updateTimestamp タイムスタンプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























