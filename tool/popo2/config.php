<?php



// ----------------------------- ファイルパス作成するファイル名まで記述してください。
$createPath = 'C:\eclipse-miko2\workspace\mikoshiva\library\Mikoshiva\Settlement\Gmo\Popo\Output\Alter.php';


// ----------------------------- クラス名
$className  = 'Mikoshiva_Settlement_Gmo_Popo_Output_Alter';


// ----------------------------- プロパティ名
$properties = array(
        'AccessID'   => null,
        'AccessPass' => null,
        'Forward'    => null,
        'Approve'    => null,
        'TranID'     => null,
        'TranDate'   => null,
        'ErrCode'    => null,
        'ErrInfo'    => null,
);