<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_NotEmpty
 */
require_once 'Zend/Validate/NotEmpty.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/10
 * @version SVN:$Id: NotEmpty.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_NotEmpty extends Zend_Validate_NotEmpty
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::IS_EMPTY => "%COLUMN_NAME% の入力は必須です。"
    );
}
