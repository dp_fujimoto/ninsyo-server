<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Int
 */
require_once 'Zend/Validate/Int.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Int.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Int extends Zend_Validate_Int
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_INT => "%COLUMN_NAME%は整数ではないようです"
    );
}
