<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <div class="system-message">
                <p>
                    この Mikoshiva_Mail は Zend_Mail を拡張したクラスです。<br>
                    このドキュメントで記述されていない動作に関しては <a href="http://framework.zend.com/manual/1.9/ja/zend.mail.html">Zend_Mail のマニュアル</a>を参照して下さい。
                </p>
            </div>

            <ol>
              <li><a href="#__construct">Mikoshiva_Log_Operation#__construct - コンストラクタ</a></li>
              <li><a href="#readingAttachment">Mikoshiva_Log_Operation#readingAttachment - 渡されたパスからファイルを読み込み添付ファイルをセットします</a></li>
              <li><a href="#setSubjectReplace">Mikoshiva_Log_Operation#setSubjectReplace - 件名を置換処理を行った後セットします</a></li>
              <li><a href="#setBodyTextReplace">Mikoshiva_Log_Operation#setBodyTextReplace - 本文を置換処理を行った後セットします（テキスト）</a></li>
              <li><a href="#setBodyHtmlReplace">Mikoshiva_Log_Operation#setBodyHtmlReplace - 本文を置換処理を行った後セットします（HTML）</a></li>
              <li><a href="#setMailTemplateById">Mikoshiva_Log_Operation#setMailTemplateById - プライマリキー を使用しメールテンプレートの情報の取得を行いセットします</a></li>
              <li><a href="#setMailTemplateByKey">Mikoshiva_Log_Operation#setMailTemplateByKey - メールキー を使用しメールテンプレートの情報の取得を行いセットします</a></li>
              <li><a href="#setBodyText">Mikoshiva_Log_Operation#setBodyText - エンコード処理を行い本文をセットします（テキスト）</a></li>
              <li><a href="#setBodyHtml">Mikoshiva_Log_Operation#setBodyHtml - エンコード処理を行い本文をセットします（HTML）</a></li>
              <li><a href="#setSubject">Mikoshiva_Log_Operation#setSubject - エンコード処理を行い件名をセットします</a></li>
              <li><a href="#addTo">Mikoshiva_Log_Operation#addTo - エンコード処理行い送信先を追加します</a></li>
              <li><a href="#addCc">Mikoshiva_Log_Operation#addCc - エンコード処理を行い Cc を追加します</a></li>
              <li><a href="#setFrom">Mikoshiva_Log_Operation#setFrom - エンコード処理を行い From をセットします</a></li>
              <li><a href="#setReplyTo">Mikoshiva_Log_Operation#setReplyTo - エンコード処理を行い Reply-To をセットします</a></li>
            </ol>




            <!-- ==============================================
            ■Mikoshiva_Db_Simple##__construct
            ============================================== -->
            <h2><a name="__construct">Mikoshiva_Db_Simple#__construct</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <p>
              コンストラクタです。<br>
              ここでメールの文字セットを指定します。
            </p>

            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$charset</td>
                <td>string</td>
                <td>ISO-2022-JP</td>
                <td>オペレーション名</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Mail.php\';

// インスタンスを生成
$mail = new Mikoshiva_Mail();
');
?>
            </pre>




            <!-- ==============================================
            ■Mikoshiva_Log_Operation#readingAttachment
            ============================================== -->
            <h2><a name="readingAttachment">Mikoshiva_Log_Operation#readingAttachment</a></h2>

            <p>
              件名を置換処理を行った後セットします
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$filePath</td>
                <td>string</td>
                <td><br></td>
                <td>ファイルパス</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Mail.php\';

// 顧客 ID で取得
$mail = new Mikoshiva_Mail();
$mail->readingAttachment(\'C:\eclipse-miko2\workspace\mikoshiva\application\test1\views\scripts\test2\test8.tpl\');
$mail->readingAttachment(\'C:\eclipse-miko2\workspace\mikoshiva\application\test1\views\scripts\test2\test9.tpl\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setSubjectReplace
            ============================================== -->
            <h2><a name="setSubjectReplace">Mikoshiva_Log_Operation#setSubjectReplace</a></h2>

            <p>
              件名を置換処理を行った後セットします
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$subject</td>
                <td>string</td>
                <td><br></td>
                <td>件名</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$replacementList</td>
                <td>array</td>
                <td><br></td>
                <td>置換リスト</td>
                <td>array()</td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();


// 置換リストを作成
$replaceList                = array();
$replaceList[]              = $customerPopo;
$replaceList[\'DOKI_DOKI\'] = \'ドキドキ\';

// セット
// POPO で置換する文字列はカラム名を ％ で囲みます。
// POPO 意外を置換する場合は、配列のキー名を ％ で囲みます。
$mail->setSubjectReplace(\'%mcu_last_name%さんに%DOKI_DOKI%なお知らせです！\', $replaceList);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
<?php echo htmlspecialchars('
谷口さんにドキドキなお知らせです！
', ENT_NOQUOTES); ?>
</pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setBodyTextReplace
            ============================================== -->
            <h2><a name="setBodyTextReplace">Mikoshiva_Log_Operation#setBodyTextReplace</a></h2>

            <p>
              本文を置換処理を行った後セットします（テキスト）
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$body</td>
                <td>string</td>
                <td><br></td>
                <td>本文</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$replacementList</td>
                <td>array</td>
                <td>array()</td>
                <td>置換リスト</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$charset</td>
                <td>string</td>
                <td>NULL</td>
                <td>文字セット</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$encoding</td>
                <td>string</td>
                <td>Zend_Mime::ENCODING_QUOTEDPRINTABLE</td>
                <td>エンコード</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();


// 置換リストを作成
$replaceList                = array();
$replaceList[]              = $customerPopo;
$replaceList[\'DOKI_DOKI\'] = \'ドキドキ\';

// セット
// POPO で置換する文字列はカラム名を ％ で囲みます。
// POPO 意外を置換する場合は、配列のキー名を ％ で囲みます。
$mail->setBodyTextReplace(\'%mcu_last_name%さんに%DOKI_DOKI%なお知らせです！\', $replaceList);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
<?php echo htmlspecialchars('
谷口さんにドキドキなお知らせです！
', ENT_NOQUOTES); ?>
</pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setBodyHtmlReplace
            ============================================== -->
            <h2><a name="setBodyHtmlReplace">Mikoshiva_Log_Operation#setBodyHtmlReplace</a></h2>

            <p>
              本文を置換処理を行った後セットします（HTML）
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$body</td>
                <td>string</td>
                <td><br></td>
                <td>本文</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$replacementList</td>
                <td>array</td>
                <td>array()</td>
                <td>置換リスト</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$charset</td>
                <td>string</td>
                <td>NULL</td>
                <td>文字セット</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$encoding</td>
                <td>string</td>
                <td>Zend_Mime::ENCODING_QUOTEDPRINTABLE</td>
                <td>エンコード</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();


// 置換リストを作成
$replaceList                = array();
$replaceList[]              = $customerPopo;
$replaceList[\'DOKI_DOKI\'] = \'ドキドキ\';

// セット
// POPO で置換する文字列はカラム名を ％ で囲みます。
// POPO 意外を置換する場合は、配列のキー名を ％ で囲みます。
$mail->setBodyTextReplace(\'%mcu_last_name%さんに%DOKI_DOKI%なお知らせです！\', $replaceList);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
<?php echo htmlspecialchars('
谷口さんにドキドキなお知らせです！
', ENT_NOQUOTES); ?>
</pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setMailTemplateById
            ============================================== -->
            <h2><a name="setMailTemplateById">Mikoshiva_Log_Operation#setMailTemplateById</a></h2>

            <p>
              ID を使用しメールテンプレートの情報を取得します
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$tableName</td>
                <td>string</td>
                <td><br></td>
                <td>クラス名</td>
                <td>Namespace を除いたクラス名</td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$id</td>
                <td>int</td>
                <td><br></br></td>
                <td>Primary Key</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$replacementList</td>
                <td>array</td>
                <td>array()</td>
                <td>置換リスト</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();


// 置換リストを作成
$replaceList                = array();
$replaceList[]              = $customerPopo;
$replaceList[\'DOKI_DOKI\'] = \'ドキドキ\';


// メールテンプレートを取得しセット
// 件名・本文・From がセットされます
$mail->setMailTemplateById(\'MstMailTemplate\', 1, $replaceList);
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setMailTemplateByKey
            ============================================== -->
            <h2><a name="setMailTemplateByKey">Mikoshiva_Log_Operation#setMailTemplateByKey</a></h2>

            <p>
              メールキーを使用しメールテンプレートの情報を取得します
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$tableName</td>
                <td>string</td>
                <td><br></td>
                <td>クラス名</td>
                <td>Namespace を除いたクラス名</td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$key</td>
                <td>string</td>
                <td><br></td>
                <td>メールキー</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$replacementList</td>
                <td>array</td>
                <td>array()</td>
                <td>置換リスト</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();


// 置換リストを作成
$replaceList                = array();
$replaceList[]              = $customerPopo;
$replaceList[\'DOKI_DOKI\'] = \'ドキドキ\';


// メールキーが TSUKASA のメールテンプレートを取得しセット
// 件名・本文・From がセットされます
$mail->setMailTemplateByKey(\'MstMailTemplate\', \'TSUKASA\', $replaceList);
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setBodyText
            ============================================== -->
            <h2><a name="setBodyText">Mikoshiva_Log_Operation#setBodyText</a></h2>

            <p>
              エンコード処理を行い本文をセットします（テキスト）
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$body</td>
                <td>string</td>
                <td><br></td>
                <td>本文</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$charset</td>
                <td>string</td>
                <td>NULL</td>
                <td>文字セット</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$encoding</td>
                <td>string</td>
                <td>Zend_Mime::ENCODING_QUOTEDPRINTABLE</td>
                <td>エンコード</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->setBodyText(\'谷口さんにドキドキおなお知らせです！\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setBodyHtml
            ============================================== -->
            <h2><a name="setBodyHtml">Mikoshiva_Log_Operation#setBodyHtml</a></h2>

            <p>
              エンコード処理を行い本文をセットします（HTML）
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$body</td>
                <td>string</td>
                <td><br></td>
                <td>本文</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$charset</td>
                <td>string</td>
                <td>NULL</td>
                <td>文字セット</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$encoding</td>
                <td>string</td>
                <td>Zend_Mime::ENCODING_QUOTEDPRINTABLE</td>
                <td>エンコード</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->setBodyHtml(\'谷口さんにドキドキおなお知らせです！\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setSubject
            ============================================== -->
            <h2><a name="setSubject">Mikoshiva_Log_Operation#setSubject</a></h2>

            <p>
              エンコード処理を行い件名をセットします
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$subject</td>
                <td>string</td>
                <td><br></td>
                <td>件名</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->setSubject(\'谷口さんにドキドキなお知らせです！\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#addTo
            ============================================== -->
            <h2><a name="addTo">Mikoshiva_Log_Operation#addTo</a></h2>

            <p>
              エンコード処理行い送信先を追加します
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$email</td>
                <td>string</td>
                <td><br></td>
                <td>メールアドレス</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$name</td>
                <td>string</td>
                <td>カラム文字</td>
                <td>受信者名</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->addTo(\'elfaris@taupe.plala.or.jp\', \'谷口\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#addCc
            ============================================== -->
            <h2><a name="addCc">Mikoshiva_Log_Operation#addCc</a></h2>

            <p>
              エンコード処理行い Cc を追加します
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$email</td>
                <td>string</td>
                <td><br></td>
                <td>メールアドレス</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$name</td>
                <td>string</td>
                <td>カラム文字</td>
                <td>受信者名</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->addCc(\'elfaris@taupe.plala.or.jp\', \'谷口\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setFrom
            ============================================== -->
            <h2><a name="setFrom">Mikoshiva_Log_Operation#setFrom</a></h2>

            <p>
              エンコード処理行い From を追加します
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$email</td>
                <td>string</td>
                <td><br></td>
                <td>メールアドレス</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$name</td>
                <td>string</td>
                <td>カラム文字</td>
                <td>受信者名</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->setFrom(\'elfaris@taupe.plala.or.jp\', \'谷口\');
');
?>
            </pre>







            <?php $i = 0; ?>
            <!-- ==============================================
            ■Mikoshiva_Log_Operation#setReplyTo
            ============================================== -->
            <h2><a name="setReplyTo">Mikoshiva_Log_Operation#setReplyTo</a></h2>

            <p>
              エンコード処理を行い Reply-To をセットします
            </p>
            <h3>Method</h3>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$email</td>
                <td>string</td>
                <td><br></td>
                <td>メールアドレス</td>
                <td><br></td>
              </tr>
              <tr>
                <td><?php echo ++$i ?></td>
                <td>$name</td>
                <td>string</td>
                <td>カラム文字</td>
                <td>受信者名</td>
                <td><br></td>
              </tr>
            </table>



            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();

// セット
$mail->setReplyTo(\'elfaris@taupe.plala.or.jp\', \'谷口\');
');
?>
            </pre>




  </div>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






























