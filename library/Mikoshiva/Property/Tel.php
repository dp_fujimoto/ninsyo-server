<?php
/**
 * @package Mikoshiva_Property
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';

/**
 * 電話番号を保持します
 * 
 * @package Mikoshiva_Property
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/09
 * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Property_Tel {
    
    /**
     * 市外局番 を保持します
     * @var string 市外局番 を保持します
     */
    protected $_tel1;

    /**
     * 市内局番 を保持します
     * @var string 市内局番 を保持します
     */
    protected $_tel2;

    /**
     * 加入者番号 を保持します
     * @var string 加入者番号 を保持します
     */
    protected $_tel3;

    /**
     * 市外局番 をセットします
     *
     * @param string $tel1 市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setTel1($tel1) {
        logInfo(LOG_START);
        $this->_tel1 = $tel1;
        logInfo(LOG_END);
    }

    /**
     * 市内局番 をセットします
     *
     * @param string $tel2 市内局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setTel2($tel2) {
        logInfo(LOG_START);
        $this->_tel2 = $tel2;
        logInfo(LOG_END);
    }

    /**
     * 加入者番号 をセットします
     *
     * @param string $tel3 加入者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setTel3($tel3) {
        logInfo(LOG_START);
        $this->_tel3 = $tel3;
        logInfo(LOG_END);
    }

    /**
     * 市外局番 を返します
     *
     * @return string $tel1 市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getTel1() {
        logInfo(LOG_START);
        return $this->_tel1;
        logInfo(LOG_END);
    }

    /**
     * 市内局番 を返します
     *
     * @return string $tel2 市内局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getTel2() {
        logInfo(LOG_START);
        return $this->_tel2;
        logInfo(LOG_END);
    }

    /**
     * 加入者番号 を返します
     *
     * @return string $tel3 加入者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getTel3() {
        logInfo(LOG_START);
        return $this->_tel3;
        logInfo(LOG_END);
    }

    
    /**
     * 電話番号をstring(ハイフンを含む)で返します。
     *
     * @return string
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function toString() {
        return $this->_tel1 . '-' . $this->_tel2 . '-' . $this->_tel3;
    }
    
    /**
     * 初期設定を行います。
     *
     * @param string $tel1
     * @param string $tel2
     * @param string $tel3
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct($tel1 = '', $tel2 = '', $tel3 = '') {
        $this->setTel1($tel1);
        $this->setTel2($tel2);
        $this->setTel3($tel3);
    }
    
}