<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
 /**
 * 外部ドメインからMikoshivaに、Ajaxでアクセスしている場合、Mikoshiva外に出るときは必ずこれで移動します。
 * 
 * リンク(aタグ)に実装した形で返します。
 *
 * @param  array   $params
 *                      string name                  リンクテキスト
 *                      string url                   アクション URL
 *                      string formId    null      | リクエスト時にpostするフォームのID
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.leaveFromMikoshivaLink.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_leaveFromMikoshivaLink($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['name']     = '';
    $default['url']      = '';
    $default['formId']   = null;
    
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(isBlankOrNull($data['url']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 url に値がありません。');
    }
    if(isBlankOrNull($data['targetId']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 targetId に値がありません。');
    }
    if($data['append'] !== 'FRONT' && $data['append'] !== 'BACK' && $data['append'] !== 'OVERWRITE') {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】append の指定が誤っています。指定値=>' . $data['append']);
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once SMARTY_PLUGINS_DIR . 'function.leaveFromMikoshiva.php';

    $script = '';
    $script = smarty_function_leaveFromMikoshiva($params, $smarty);

    Mikoshiva_Logger::info(LOG_END);
    return '<a href="#" onclick="' . $script . '">' . $data['name'] . '</a>';

}


?>
