<?php
/**
 * @package Mikoshiva_Validate
 */


/**
 * require
 */
require_once 'spyc.php';
require_once 'Mikoshiva/Filter/Input.php';
require_once 'Mikoshiva/Validate/Exception.php';
require_once 'Mikoshiva/Validate/Message/Exception.php';

/**
 * 検証定義ファイルを元に入力値のチェックを行います
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Checker {

    /**
     * 検証ファイルを元に検証を行います
     *
     * $validationDefinition
     * data => 検証対象のデータ
     * separatorName => 検証ファイルのセパレーター名
     *
     * $errorDefinition エラーだった場合の処理
     * 0 => _forward or redirect
     * 1 以降は 0 に応じた引数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/03
     * @version SVN:$Id: Checker.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param array $validationDefinition 検証定義
     * @param array $errorDefinition エラー処理定義
     * @throws Mikoshiva_Validate_Exception Mikoshiva_Validate_Message_Exception
     * @return array エラーメッセージ
     */
	public static function check(
	   array $validationDefinition, array $errorDefinition = array() ) {

	    //---------------------------------------
	    // 引数の検証
	    //---------------------------------------
	    // ◆検証データが無ければエラー
	    if (!isset($validationDefinition['data'])) {
            throw new Mikoshiva_Validate_Exception('Mikoshiva_Validate_Checker#check 第一引数に検証データが存在しません。');
	    }

	    // ◆エラー処理定義が不正で有ればエラー
	    if (count($errorDefinition) !== 0) {
	        if($errorDefinition[0] !== 'forward' && $errorDefinition[0] !== 'redirect') {
                throw new Mikoshiva_Validate_Exception('Mikoshiva_Validate_Checker#check 第二引数のエラー定義が不正です。');
	        }
        }

	    // リクエストをセット
	    $request        = Zend_Registry::get('request');

        // リクエストクラスからパス情報を取得
		$moduleName     = $request->getModuleName();     // モジュール名
		$controllerName = $request->getControllerName(); // コントローラー名
		$separatorName     = isset($validationDefinition['separatorName'])
		                ? $validationDefinition['separatorName']
		                : $request->getActionName();     // アクション名

		// 検証定義ファイルのパスを解決
		$validationFilePath = APPLICATION_PATH
		                    . '/'
		                    . $moduleName
		                    . '/configs/'
		                    . $controllerName
		                    . 'Validetor.yml';

        // ◆検証定義ファイルの存在チェック
		if (!file_exists($validationFilePath)) {
			throw new Mikoshiva_Validate_Exception( $validationFilePath . ' に検証定義ファイルが存在しません。');
		}

		// 検証定義ファイルの読み込み
    	$validatorList = Spyc::YAMLLoad( $validationFilePath );

    	//--------------------------------------------
		// 検証定義ファイルに指定されたセクションが存在しなけれエラー
		//--------------------------------------------
		if (!isset($validatorList[$separatorName])) {
			throw new Mikoshiva_Validate_Exception('検証定義ファイルに ' . $separatorName . ' セクションが存在しません。');
		}

    	//--------------------------------------------
		// 検証実行
		//--------------------------------------------
		$input = new Mikoshiva_Filter_Input( array(), $validatorList[ $separatorName ], $validationDefinition['data'] );
		$input->addValidatorPrefixPath('Mikoshiva_Validate', 'Mikoshiva/Validate'); // Mikodhiva 拡張クラスをセット


    	//-------------------------------------------------
		// エラーメッセージの項目名をマッピングされた名前に置換し再生成
		//-------------------------------------------------
        $COLUMN_MAP_LIST = Zend_Registry::get('COLUMN_MAP_LIST'); // 項目名のマッピングデータを取得
		$message = array(); // メッセージ
		foreach ($input->getMessages() as $k => $v) {

			// 存在しない項目であればエラー
			if (!isset( $COLUMN_MAP_LIST[ $k ])) {
				throw new Mikoshiva_Validate_Exception('項目リストに【 ' . $k . ' 】 は存在しません。');
			}

			// 項目名を置換
			foreach ($v as $k2 => $v2) {
				$message[] = str_replace('%COLUMN_NAME%', $COLUMN_MAP_LIST[$k], $v2 );
			}
		}

        //---------------------------------------------------
        // エラーで無ければリターンで終了
        //---------------------------------------------------
		if (count( $message ) === 0) return array();


        //---------------------------------------------------
        // エラーであればメッセージを view にセット
        //---------------------------------------------------
        /* @var $ac Mikoshiva_Controller_Action_Admin_Abstract */
        $ac = Zend_Registry::get('actionController'); // 現在のアクションコントローラを取得
        $ac->view->message = $message; // エラーメッセージをセット


        //---------------------------------------------------
        // エラー定義が有ればそれに従って処理を行う
        //---------------------------------------------------
        if (count($errorDefinition) !== 0) {

            // _forward
            if ($errorDefinition[0] === 'forward') {

                $methodName = $errorDefinition[0];
                $one        = isset($errorDefinition[1]) ? $errorDefinition[1] : null;
                $tow        = isset($errorDefinition[2]) ? $errorDefinition[2] : null;
                $three      = isset($errorDefinition[3]) ? $errorDefinition[3] : null;
                $four       = isset($errorDefinition[4]) ? $errorDefinition[4] : null;

                $ac->$methodName(
                    $one,
                    $tow,
                    $three,
                    $four
                );
            }

            // _redirect
            if ($errorDefinition[0] === 'redirect') {

                $methodName = $errorDefinition[0];
                $one        = isset($errorDefinition[1]) ? $errorDefinition[1] : null;
                $tow        = isset($errorDefinition[2]) ? $errorDefinition[2] : array();

                $ac->$methodName(
                    $one,
                    $tow
                );
            }

            //---------------------------------------------------
            // 処理完了後例外を発生させる
            // これはアクションの処理をここで終了させたい為の処理です。
            // 上のクラスで try ～ catch されるのを期待しています。
            //---------------------------------------------------
        }

        throw new Mikoshiva_Validate_Message_Exception('入力エラーが発生しました。');

        // return $message;
    }
}




















