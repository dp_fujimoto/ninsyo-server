<?php
$url = 'https://www.dp-auth.jp/accessLog.php';

if (strlen($sn) == 0 || strlen($sc) == 0) {
    die;
}

$cookie_name = $sn . $sc . $ru;

if (strlen($cookie_name) > 0) {
    $ck = 0;
    if (isset($_COOKIE[$cookie_name])) {
        $ck = 1;
    }
    if ($ck == 0) {
        $timeout = time() + 30 * 24 * 60 * 60; // 30 days
        $value = date('Y-m-d H:i:s');
        setcookie($cookie_name, $value, $timeout);
    }

        
    $data = array(
        'sn' => $sn,
        'sc' => $sc,
        'ru' => $ru,
        'ck' => $ck,
    );
    
    $http_build_query = http_build_query($data);
    
    $headers = array(
        'Content-Type: application/x-www-form-urlencoded',
        'Content-Length: ' . strlen($http_build_query),
    );
    $options = array('http' => array(
        'method' => 'POST',
        'content' => $http_build_query,
        'header' => implode("\r\n", $headers),
    ));
    $contents = file_get_contents($url, false, stream_context_create($options));
}
?>