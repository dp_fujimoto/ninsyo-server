{$deleteList = array()}
{$deleteList[] = 'tabId'}
{$deleteList[] = 'action'}
{$deleteList[] = 'module'}
{$deleteList[] = 'controller'}
{$deleteList[] = 'tableName'}
{$deleteList[] = 'tableIdName'}
{$deleteList[] = 'columnName'}
{$deleteList[] = 'statusName'}
{$deleteList[] = 'updateName'}
{$deleteList[] = 'updateData'}

{$tableList = $form->getTableList()}
{$statusList = $form->getStatusList()}
{$updateColumnNameArr = $form->getUpdateColumnNameArr()}
{$errMsg = $form->getErrMsg()}

<form id="updateForm{$tabId}" method="post" onSubmit="return false;">
    {hiddenLister hiddenList=$form deleteList=$deleteList}
    <table>
        <tr>
            {foreach from=$tableList key="prefix" item="tableData"}
            {if isset($search[$prefix]) && count($search[$prefix]) > 0 && isset($tableData['name'])}
                <td class="COM_top" id="td{$tableData['no']}">
                    {$tableData['name']} <span class="COM_bold COM_fontBlue">{$search[$prefix]|count}</span>件ヒット<br>
                    <table class="COM_table">
                        {foreach from=$search[$prefix][0] key="k" item="v"}
                            <tr>
                            {foreach from=$search[$prefix] key="k2" item="v2"}
                                {if $k2 == 0}
                                    <td class="COM_bgColorLight COM_center COM_nowrap">
                                        {if isset($columnName[$k])}<span class="COM_bold">{$columnName[$k]}</span><br>{/if}
                                        <span class="COM_fontGrey">{$k}</span>
                                    </td>
                                {/if}
                                {$id = ''}
                                {$id = $tableData['table']|cat:'||'|cat:$k|cat:'||'|cat:$tableData['primary']|cat:'||'|cat:$v2[$tableData['primary']]}
                                {if isset($errMsg[$id])}
                                    {$class = 'COM_left COM_nowrap'}
                                {elseif isset($updateColumnNameArr[$id])}
                                    {$class = 'COM_left COM_nowrap COM_bgColorGreen'}
                                {else}
                                    {$class = 'COM_left COM_nowrap'}
                                {/if}
                                {if $k === $tableData['primary']}
                                    {$onClick = ''}
                                {elseif stristr($k,'_update_datetime') || stristr($k,'_update_timestamp')}
                                    {$onClick = ''}
                                {else}
                                    {$onClick = "PDT_updateTableData{$tabId}('{$id}',this);"}
                                {/if}
                                <td class="{$class}" onClick="{$onClick}">
                                    {if stristr($k,'_memo') || strstr($k,'_body')}
                                        <pre>{$v2[$k]}</pre>
                                    {elseif stristr($k,'_header') || strstr($k,'_footer')}
                                        <pre>{$v2[$k]}</pre>
                                    {else}
                                        {$v2[$k]}
                                    {/if}
                                    {if isset($statusName[$v2[$k]])}
                                        <br><span class="COM_fontGrey">{$statusName[$v2[$k]]}</span>
                                    {/if}
                                    {if isset($errMsg[$id])}
                                        <br><span class="COM_bold COM_fontRed">{$errMsg[$id]}</span>
                                    {/if}

                                    {if $k === $tableData['primary']}
                                    {elseif stristr($k,'_update_datetime') || stristr($k,'_update_timestamp')}
                                    {else}
                                        <div class="COM_displayNone">
                                            {if stristr($k,'_memo') || strstr($k,'_body')}
                                                <textarea name="updateDataTemp[{$id}]" cols="30" rows="1">{$v2[$k]}</textarea>
                                            {elseif stristr($k,'_header') || strstr($k,'_footer')}
                                                <textarea name="updateDataTemp[{$id}]" cols="30" rows="1">{$v2[$k]}</textarea>
                                            {elseif stristr($k,'_type') || stristr($k,'_status')}
                                                {$type = substr($k,4)}
                                                {if isset($statusList[$type])}
                                                    {statusLister name='updateDataTemp['|cat:$id|cat:']' mode='select' selected=$v2[$k] status=$type addEmpty=true}
                                                {else}
                                                    <input type="text" name="updateDataTemp[{$id}]" value="{$v2[$k]}" size="40">
                                                {/if}
                                            {else}
                                                <input type="text" name="updateDataTemp[{$id}]" value="{$v2[$k]}" size="40">
                                            {/if}
                                        </div>
                                    {/if}
                                </td>
                            {/foreach}
                            </tr>
                        {/foreach}
                    </table>
                </td>
            {/if}
            {/foreach}
        </tr>
    </table>
</form>
{if count($updateColumnNameArr) > 0}
    <!-- // カレントJavascript開始 -->
    <script type="text/javascript">
        alert('{$form->getUpdateMsg()}');
    </script>
    <!-- // カレントJavascript終了 -->

{/if}

<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 更新時表示切替
    //-----------------------------------------------
    function PDT_updateTableData{$tabId}(id,obj){
        body = obj.innerHTML;
        bodySplit = body.split('<div class="COM_displayNone">');
        if (bodySplit[1]) {
            divHtml = bodySplit[1].split('</div>');
            htmlStr = divHtml[0];
            htmlStr = htmlStr + '<input type="hidden" name="updateName[]" value="' + id + '">';
            htmlStr = htmlStr.replace('updateDataTemp','updateData');
            htmlStr = htmlStr.replace('<output','<input');
            obj.innerHTML = htmlStr;
        }
    }

</script>
<!-- // カレントJavascript終了 -->