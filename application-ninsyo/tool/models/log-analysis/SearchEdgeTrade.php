<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';


/**
 * 指定したテーブルのデータを取得
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2011/09/09
 * @version        SVN: $Id:
 *
 */
class Tool_Models_LogAnalysis_SearchEdgeTrade
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したテーブルのデータを取得
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // リターンする配列
        $output = array();
        $timeAnalysisData = array();

        if (isBlankOrNull($actionForm->getYear())) {
            $actionForm->setYear(date('Y'));
        }
        if (isBlankOrNull($actionForm->getMonth())) {
            $actionForm->setMonth(date('m'));
        }

        $startDate = $actionForm->getYear() . '-' . $actionForm->getMonth() . '-01';
        $actionForm->setDate($startDate);

        $config = $context->getConfig();
        $typeList = $config['SHORT_CODE_MAP_LIST']["access_log_detail_type"];
        $actionForm->setTypeList($typeList);

        $dateClass = new DateTime($startDate);
        $dateClass->modify('+1 month');
        $endDate = $dateClass->format('Y-m-d');
        $dateClass->modify('-1 day');
        $endDay = (int)$dateClass->format('d');

        $nowYm = date('Y-m');
        $nowDay = date('d');

        for ($i = 1; $i <= $endDay; $i++) {
            if ($nowYm === $actionForm->getYear() . '-' . $actionForm->getMonth()) {
                if ($nowDay <= $i) {
                    break;
                }
            }
            $output[$i] = array();
            $output[$i]['count'] = 0;
            $output[$i]['user'] = 0;
            $output[$i]['detail'] = array();
        }

        for ($i = 0; $i < 24; $i++) {
            $timeAnalysisData[$i] = array();
            $timeAnalysisData[$i]['count'] = 0;
            $timeAnalysisData[$i]['user'] = array();
        }

        //-----------------------------------------------
        //
        // 検索フォームからサブミットされたら、条件を追加して検索を行う。
        //
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT";
        $sql .= " *";
        $sql .= " FROM trx_access_log_totalization";
        $sql .= " INNER JOIN trx_access_log_analysis ON tat_access_log_totalization_id = tan_access_log_totalization_id";
        $sql .= " INNER JOIN trx_access_log_analysis_detail ON tan_access_log_analysis_id = tad_access_log_analysis_id";
        $sql .= " WHERE 1 = 1";
        $sql .= " AND tat_delete_flag = '0'";
        $sql .= " AND tan_delete_flag = '0'";
        $sql .= " AND tad_delete_flag = '0'";
        $sql .= " AND tat_access_log_totalization_type = 'ACCESS_LOG_TOTALIZATION_TYPE_EDGE_TRADER'";
        $sql .= " AND tat_access_date >= ?";
        $sql .= " AND tat_access_date < ?";


        $params   = array();
        $params[] = $startDate;
        $params[] = $endDate;

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $max = 0;
        while($row = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $day = (int)substr($row['tat_access_date'],8,2);
            $output[$day]['count'] = $row['tat_access_count'];
            $output[$day]['user'] = $row['tat_user_count'];
            if (!isset($output[$day]['detail'][$row['tad_access_log_detail_type']])) {
                $output[$day]['detail'][$row['tad_access_log_detail_type']] = array();
                $output[$day]['detail'][$row['tad_access_log_detail_type']]['count'] = 0;
                $output[$day]['detail'][$row['tad_access_log_detail_type']]['user'] = 0;
                $output[$day]['detail'][$row['tad_access_log_detail_type']]['memo'] = '';
            }
            $output[$day]['detail'][$row['tad_access_log_detail_type']]['count'] += $row['tad_access_count'];
            $output[$day]['detail'][$row['tad_access_log_detail_type']]['memo'] .= trim(str_replace('[対象ユーザーID]','',$row['tad_memo']));

            if ($row['tad_access_log_detail_type'] !== 'ACCESS_LOG_DETAIL_TYPE_FAILURE') {
                $timeAnalysisData[$row['tan_access_hour']]['count'] += $row['tad_access_count'];

                $memoArr = explode('|',$row['tad_memo']);
                foreach ($memoArr as $k3 => $v3) {
                    if (!isBlankOrNull($v3)) {
                        $timeAnalysisData[$row['tan_access_hour']]['user'][$v3] = 1;
                    }
                }
            }
        }
        foreach ($timeAnalysisData as $k => $v) {
            $timeAnalysisData[$k]['user'] = count($v['user']);
        }
        $actionForm->setMax($max);
        $actionForm->setTimeAnalysisData($timeAnalysisData);

        // メモの内容整理
        foreach ($output as $k => $v) {
            if (isset($v['detail'])) {
                foreach ($v['detail'] as $k2 => $v2) {
                    $memoArr = explode('|',$v2['memo']);
                    $memoArr = array_unique($memoArr);
                    sort($memoArr);
                    foreach ($memoArr as $k3 => $v3) {
                        if (isBlankOrNull($v3)) {
                            unset($memoArr[$k3]);
                        }
                    }
                    $output[$k]['detail'][$k2]['user'] = count($memoArr);
                    $output[$k]['detail'][$k2]['memo'] = '[対象ユーザーID]' . "\n" . implode("\n",$memoArr);
                }
            }
        }

        //-----------------------------------------------
        //
        // ログインユーザー数チャート
        //
        //-----------------------------------------------
        $chartData = array();
        $row = array();
        $row[] = 'days';
        $row[] = '合計アクセスユーザー数';
        foreach ($typeList as $k => $v) {
            $row[] = $v;
        }
        $chartData[] = $row;
        foreach ($output as $k => $v) {
            $row = array();
            $row[] = $k;
            $row[] = $v['user'];
            foreach ($typeList as $k2 => $v2) {
                if (isset($output[$k]['detail'][$k2]['user'])) {
                    $row[] = $output[$k]['detail'][$k2]['user'];
                } else {
                    $row[] = 0;
                }
            }
            $chartData[] = $row;
        }

        $postData = array();
        $postData['data'] = $chartData;
        $postData['title'] = 'ログインユーザー数の変化　　（ユーザー数＝ログインID数）';
        $postData['left'] = 'ログインユーザー数';
        $postData['bottom'] = '日';
        $postData['width'] = '1000';
        $postData['height'] = '300';
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen(http_build_query($postData))
        );
        $options = array('http' => array(
            'method' => 'POST',
            'content' => http_build_query($postData),
            'header' => implode("\r\n", $headers),
        ));

        for ($loop = 5; $loop > 0; $loop--) {
            try {
                $res = file_get_contents('http://www.securepayment.jp/tool/google-combo-chart.php', false, stream_context_create($options));
                $actionForm->setUserChartHtml($res);
                break;
            } catch (Exception $e) {
                if ($loop === 1) {
                    throw new Exception($e);
                }
            }
        }

        //-----------------------------------------------
        //
        // アクセス数チャート
        //
        //-----------------------------------------------
        $chartData = array();
        $row = array();
        $row[] = 'days';
        $row[] = '合計アクセス数';
        foreach ($typeList as $k => $v) {
            $row[] = $v;
        }
        $chartData[] = $row;
        foreach ($output as $k => $v) {
            $row = array();
            $row[] = $k;
            $row[] = $v['count'];
            foreach ($typeList as $k2 => $v2) {
                if (isset($output[$k]['detail'][$k2]['count'])) {
                    $row[] = $output[$k]['detail'][$k2]['count'];
                } else {
                    $row[] = 0;
                }
            }
            $chartData[] = $row;
        }

        $postData = array();
        $postData['data'] = $chartData;
        $postData['title'] = 'アクセス数の変化';
        $postData['left'] = 'アクセス数';
        $postData['bottom'] = '日';
        $postData['width'] = '1000';
        $postData['height'] = '300';
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen(http_build_query($postData))
        );
        $options = array('http' => array(
            'method' => 'POST',
            'content' => http_build_query($postData),
            'header' => implode("\r\n", $headers),
        ));

        for ($loop = 5; $loop > 0; $loop--) {
            try {
                $res = file_get_contents('http://www.securepayment.jp/tool/google-combo-chart.php', false, stream_context_create($options));
                $actionForm->setAccessChartHtml($res);
                break;
            } catch (Exception $e) {
                if ($loop === 1) {
                    throw new Exception($e);
                }
            }
        }

        //-----------------------------------------------
        //
        // 時間別アクセス成功数チャート
        //
        //-----------------------------------------------
        $chartData = array();
        $row = array();
        $row[] = 'hour';
        $row[] = 'アクセス数';
        $row[] = 'ユーザー数';
        $chartData[] = $row;

        foreach ($timeAnalysisData as $k => $v) {
            $row = array();
            $row[] = $k;
            $row[] = $v['count'];
            $row[] = $v['user'];
            $chartData[] = $row;
        }

        $postData = array();
        $postData['data'] = $chartData;
        $postData['title'] = '時間別アクセス成功数の変化';
        $postData['left'] = 'アクセス数';
        $postData['bottom'] = '時';
        $postData['width'] = '1000';
        $postData['height'] = '300';
        // $postData['series'] = '{0: {type: "bars"},1: {type: "bars"}}';
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen(http_build_query($postData))
        );
        $options = array('http' => array(
            'method' => 'POST',
            'content' => http_build_query($postData),
            'header' => implode("\r\n", $headers),
        ));

        for ($loop = 5; $loop > 0; $loop--) {
            try {
                $res = file_get_contents('http://www.securepayment.jp/tool/google-combo-chart.php', false, stream_context_create($options));
                $actionForm->setTimeChartHtml($res);
                break;
            } catch (Exception $e) {
                if ($loop === 1) {
                    throw new Exception($e);
                }
            }
        }

        return $output;
    }

}
