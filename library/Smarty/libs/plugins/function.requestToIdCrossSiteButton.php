<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
 /**
 * Ajax で指定されたアクションへリクエストを行い、
 * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うボタンタグを返します。
 *
 * @param  array   $params
 *                      string name                  ボタンのvalue
 *                      string url                   アクション URL
 *                      string targetId              結果書き込み先(htmlタグのID)
 *                      string formId    null      | リクエスト時にpostするフォームのID
 *                      string append    OVERWRITE | 追記モード(FRONT/前方へ挿入/BACK:後方へ追記/OVERWRITE:上書き)
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.requestToIdCrossSiteButton.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_requestToIdCrossSiteButton($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['name']     = '';
    $default['url']      = '';
    $default['targetId'] = '';
    $default['formId']   = null;
    $default['append']   = 'OVERWRITE';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(isBlankOrNull($data['url']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 url に値がありません。');
    }
    if($data['append'] !== 'FRONT' && $data['append'] !== 'BACK' && $data['append'] !== 'OVERWRITE') {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】append の指定が誤っています。指定値=>' . $data['append']);
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once SMARTY_PLUGINS_DIR . 'function.requestToIdCrossSite.php';

    $script = '';
    $script = smarty_function_requestToIdCrossSite($params, $smarty);

    Mikoshiva_Logger::info(LOG_END);
    return '<input type="button" value="' . $data['name'] . '" onclick="this.disabled=true;' . $script . '">';
}


?>
