<?php


/**
 * キャンセル処理の Factory クラスです
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/24
 * @version SVN:$Id: Factory.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Alter_Cancel_Factory {


    /**
     * 引数の日付に応じたキャンセルクラスを返します
     *
     * GMO のマニュアル
     * PG カード決済サービス プロトコル・タイプ（インターフェース仕様）
     * ２．１２　決済の内容を取り消すより
     *
     * 仮売上に対する取消は『RETURN：返品』
     *
     * 即時売上・実売上は以下
     * 当月内『RETURN：返品』
     * 決済を実施した翌月以降『RETURNX：月跨り返品』
     *
     * 当日『取消：VOID』
     *
     * <test>
     * $dateList[] = '2010/03/24'; // 今日 Mikoshiva_Settlement_Execution_Gmo_Alter_Void
     * $dateList[] = '2010/02/04'; // 先月 Mikoshiva_Settlement_Execution_Gmo_Alter_ReturnX
     * $dateList[] = '2010/03/04'; // 今月 Mikoshiva_Settlement_Execution_Gmo_Alter_Return
     * $dateList[] = 'あかさたな'; // 形式 例外
     * $dateList[] = '2010/03/25'; // 明日 例外
     * $dateList[] = '2010/04/04'; // 来月 例外
     * </test>
     *
     * @return Mikoshiva_Settlement_Execution_Gmo_Alter_Abstract
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/24
     * @version SVN:$Id: Factory.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function getInstance($accessId) {
        //------------------------------------------------------------
        // ■アクセスIDが３２桁で無ければエラー
        //------------------------------------------------------------
        if (strlen($accessId) !== 32) {
            include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】不正なアクセス ID が検出されました： ”{$accessId}”";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }


        //------------------------------------------------------------
        // ■GMO 取引履歴テーブル（mst_gmo_transaction_history）から該当の返金対象の決済データを取得する
        //------------------------------------------------------------
        // table
        include_once 'Mikoshiva/Db/Ar/MstGmoTransactionHistory.php';
        $tableClass = null;
        $tableClass = new Mikoshiva_Db_Ar_MstGmoTransactionHistory();

        // select
        $selectClass = null;
        $selectClass = $tableClass->select();

        // where
        // オーダーＩＤは一意ではないので、最新の１件を取得し、
        // キャンセルされたかどうかを調査します
        $selectClass->where('mgh_access_id = ?', $accessId);
        $selectClass->order('mgh_tran_date DESC');
        $selectClass->limit(1);

        // 実行
        $rows = $tableClass->fetchAll($selectClass)->toArray();
        //------------------------------------------------------------
        // ■取得件数チェック
        //------------------------------------------------------------
        if (count($rows) === 0) {
            include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】アクセスＩＤ： ”{$accessId}”は DB に存在しません。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }
        $row = array();
        $row = $rows[0];

        //------------------------------------------------------------
        // ■ステータスチェック
        //------------------------------------------------------------
        $entryStatus = $row['mgh_entry_status'];

        if ($entryStatus === 'ENTRY_STATUS_VOID' || $entryStatus === 'ENTRY_STATUS_RETURN' || $entryStatus === 'ENTRY_STATUS_RETURNX') { // このステータスないのでは？
            include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】アクセスID：”{$accessId}”のデータはキャンセル済みです。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }

        //------------------------------------------------------------
        // ■送信データ作成
        //------------------------------------------------------------
        include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Alter.php';
        $alterInputPopo = null;
        $alterInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Alter();  // 実行クラスへ渡すための入れ物クラス（ POPO ）
        $alterInputPopo->setShopID($row['mgh_shop_id']);                 // ショップ ID
        $alterInputPopo->setAccessID($row['mgh_access_id']);             // アクセス ID
        $alterInputPopo->setAccessPass($row['mgh_access_pass']);         // アクセスパスワード

        $date = '';
        $date = $row['mgh_tran_date'];

        //------------------------------------------------------------
        // ■Mikoshiva_Date で初期化
        //------------------------------------------------------------
        $dateClass1 = new Mikoshiva_Date($date); // 与えられた日付による日付クラス
        $dateClass2 = new Mikoshiva_Date();      // 現在の日付クラス


        /****** 未来の日付は仮売上で
        //------------------------------------------------------------
        // ■未来の日付チェック
        //------------------------------------------------------------
        if ($dateClass1->getTimestamp() > $dateClass2->getTimestamp()) {
            include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】mgh_tran_date ”{$date}”は未来の日付であり不正です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }
        ****/




        //------------------------------------------------------------
        // ■日付に応じたオブジェクトを返却する
        //------------------------------------------------------------
        $class = null;
        switch (true) {

            // ▼amount=0 Mikoshiva_Settlement_Execution_Gmo_Alter_Check ----------------------------------------------------------------
            case $row['mgh_amount'] === '0':
                include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Check.php';
                $class = new Mikoshiva_Settlement_Execution_Gmo_Alter_Check($alterInputPopo);
                break;

            // ▼仮売上 Mikoshiva_Settlement_Execution_Gmo_Alter_Return ----------------------------------------------------------------
            case $row['mgh_entry_status'] === 'ENTRY_STATUS_AUTH':
                include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Return.php';
                $class = new Mikoshiva_Settlement_Execution_Gmo_Alter_Return($alterInputPopo);
                break;

            // ▼当日 Mikoshiva_Settlement_Execution_Gmo_Alter_Void ---------------------------------
            case $dateClass1->isToday():
                include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Void.php';
                $class = new Mikoshiva_Settlement_Execution_Gmo_Alter_Void($alterInputPopo);
                break;

            // ▼翌日以降月内 Mikoshiva_Settlement_Execution_Gmo_Alter_Return ------------------------
            case $dateClass1->toString('yyyy-MM') === $dateClass2->toString('yyyy-MM'):
                include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Return.php';
                $class = new Mikoshiva_Settlement_Execution_Gmo_Alter_Return($alterInputPopo);
                break;
                break;

            // ▼月跨返品 Mikoshiva_Settlement_Execution_Gmo_Alter_ReturnX ----------------------------
            //   今月の１日より、購入日が前（先月以降）であったら月跨返品
            case $dateClass1->getTimestamp() < $dateClass2->setDay(1)->setTime(0)->getTimestamp():
                include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/ReturnX.php';
                $class = new Mikoshiva_Settlement_Execution_Gmo_Alter_ReturnX($alterInputPopo);
                break;

            // ▼ Default -----------------------------------------------------------------------------
            Default:
                include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Exception.php';
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】”{$date}” に該当するキャンセルクラスが存在しません。";
                logDebug($message);
                throw new Mikoshiva_Settlement_Execution_Gmo_Alter_Exception($message);
        }

        return $class;
    }
}

































