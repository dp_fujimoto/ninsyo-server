<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ユーザーコンテンツ一括登録</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <span class="COM_fontRed">
                {$output|hd}
            </span>

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" enctype="multipart/form-data" method="post" onSubmit="return false;">

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            CSVファイル<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            <input type="file" name="addressFile" size="80">
                            <br>※1行に「メールアドレス,パスワード」のみ記載されているファイル。サイズは2Mバイトまで。
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            コンテンツID<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            <input type="text" name="contentId" value="{$form->getContentId()}" size="40">
                            　※カンマ区切りで複数可能
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            サーバー名<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td>
                            <select name="serverName">
                                <option value="free" {if $form->getServerName() === 'free'}selected{/if}>free（無料ＷＥＢ認証アカウント登録用）</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            会員開始日時
                        </td>
                        <td>
                            <input type="text" name="startDate" value="{$form->getStartDate()}" size="10" maxlength="10">
                            　※いつから会員だったことにするか 例：2013-09-12
                        </td>
                    </tr>
                </table>

                <!-- // ページ下部のボタン開始 -->
                {currentTabButton name='　　　　登録　　　　' url='/user/content/input-list' tabId=$tabId  formId='inputForm'}
                <!-- // ページ下部のボタン終了 -->

            </form>
            <!-- // フォーム終了 -->

            <div class="COM_displayNone">{$form->getDebug()}</div>

        </td>
        <!-- // ペイン終了 -->
    </tr>

</table>
<!-- // メインテーブル終了 -->