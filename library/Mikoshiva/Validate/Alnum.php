<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Alnum
 */
require_once 'Zend/Validate/Alnum.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Alnum.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Alnum extends Zend_Validate_Alnum
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_ALNUM    => "%COLUMN_NAME%にアルファベットと数字以外の文字が含まれています",
        self::STRING_EMPTY => "%COLUMN_NAME%は空の文字列です"
    );
}
