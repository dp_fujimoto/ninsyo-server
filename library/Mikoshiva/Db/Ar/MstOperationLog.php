<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstOperationLog
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstOperationLog extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_operation_log';
    protected $_primary  = 'mol_operation_log_id';
    protected $_metadata = array (
  'mol_operation_log_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_operation_log_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'mol_login_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_login_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_operation_url' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_operation_url',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '255',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_model_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_model_name',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '255',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_sql' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_sql',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'text',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_sql_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_sql_type',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '6',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_action_form_properties' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_action_form_properties',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'mediumtext',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_delete_flag',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_deletion_datetime',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_update_datetime',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_registration_datetime',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mol_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_operation_log',
    'COLUMN_NAME' => 'mol_update_timestamp',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

