<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Mapping.php';

/**
 *
 *
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/15
 * @version SVN:$Id: GmoMapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_GmoMapping extends Mikoshiva_View_Helper_Abstract {
    /**
     *
     *
     * @param  string $shopId ショップID
     * @return string $result ショップ名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/15
     * @version SVN:$Id: GmoMapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function gmoMapping($shopId) {
        logInfo(LOG_START);
        $result = '';
        try{
            $result = Mikoshiva_Utility_Mapping::gmoMapping($shopId);
        } catch (Mikoshiva_Utility_Exception $e){
            Mikoshiva_Logger::debug($e);
            $result = '';
        }
        logInfo(LOG_END);
        return $result;
    }
}
