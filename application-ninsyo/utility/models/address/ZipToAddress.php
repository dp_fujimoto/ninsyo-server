<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';


/**
 * 
 * 
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/22
 * @version SVN:$Id: ZipToAddress.php,v 1.1 2012/09/21 07:08:35 fujimoto Exp $
 */
class Utility_Models_Address_ZipToAddress
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定された条件で顧客検索を行い、検索結果を取得します
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                              Mikoshiva_Controller_Mapping_ActionContext $context) {
        $json = array();
        $zip1 = '';
        $zip2 = '';
        $zip1 = $actionForm->get('zip1');
        $zip2 = $actionForm->get('zip2');


        //DBからデータを1件取得
        $data = $this->selectZipToAddressOne($zip1 . $zip2);

        if($data['error'] === null) {
        // 住所2から、特定の記号・文言を削除する。
            $data['mzc_address2'] = preg_replace('/\(.*/i','', $data['mzc_address2']); // 半角のかっこの場合、それ以降を削除
            $data['mzc_address2'] = preg_replace('/（.*/i','', $data['mzc_address2']); // 全角のかっこの場合、それ以降を削除
            $data['mzc_address2'] = preg_replace('/、.*/i','', $data['mzc_address2']); // "、"を含む場合、それ以降を削除
            $data['mzc_address2'] = preg_replace('/～.*/i','', $data['mzc_address2']); // "～"を含む場合、それ以降を削除
            $data['mzc_address2'] = preg_replace('/以下に掲載がない場合.*/i', '', $data['mzc_address2']); //"以下に掲載がない場合"、それ以降を削除
        }
        
        //データをJSON形式に変換(data.カラム名でアクセス可能になる)
        $json = json_encode($data);
        echo $json;
        exit;
    }

        
     /**
     * 郵便番号から住所を検索し返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/10
     * @version SVN:$Id: ZipToAddress.php,v 1.1 2012/09/21 07:08:35 fujimoto Exp $
     *
     * @param $zip
     * @return array
     */
    public function selectZipToAddressOne($zip) {
        $_zip     = '';
        $zipClass = null;
        $select   = null;
        $row      = null;

        // 郵便番号を DB にチェックしに行きます
        $_zip = str_replace('-', '', $zip );

        $zipClass     = new Mikoshiva_Db_Ar_MstZipCode();
        $select       = $zipClass->select()->where('mzc_zip_code = ?', $_zip);
        $row          = $zipClass->fetchRow($select);

        if ($row === null) {
            return array('error' => '該当する住所はありません');
        }
        $result = $row->toArray();
        $result['error'] = null;
        return $result;
    }

    /**
     * 郵便番号から住所を検索し返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/10
     * @version SVN:$Id: ZipToAddress.php,v 1.1 2012/09/21 07:08:35 fujimoto Exp $
     *
     * @param $zip
     * @return array
     */
    function selectZipToAddressAll($zip) {

        // 郵便番号を DB にチェックしに行きます
        $_zip = str_replace('-', '', $zip );

        $zipClass = new Mikoshiva_Db_Ar_MstZipCode();
        $select   = $zipClass->select()->where('mzc_zip_code = ?', $_zip);
        $row      = $zipClass->fetchAll($select);

        if ($row === null) {
            return array();
        }
        return $row->toArray();
    }
}
