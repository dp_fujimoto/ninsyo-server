<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/RequestToId.php';


/**
 * 
 * 
 * @package Mikoshiva_View
 * @author admin
 * @since 2010/02/17
 * @version SVN:$Id: RequestToIdLink.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_RequestToIdLink extends Mikoshiva_View_Helper_Abstract {



    public function requestToIdLink($name, $url, $targetId, $formId = '', $append = '') {


        $a = new Mikoshiva_View_Helper_RequestToId();
        return '<a href="#" onclick="' . $a->RequestToId($url, $targetId, $formId, $append) . '">' . $name . '</a>';
    }
}
