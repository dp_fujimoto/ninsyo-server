<?php
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/String.php';
require_once 'Zend/Registry.php';



/**
 * ダンプを行い、変数情報を文字列で返します。
 * オブジェクトは未対応
 *
 * @param  array    $params    現在未使用
 * @param  Smarty   &$smarty
 * @return string              変数の情報
 */
function smarty_function_f5Former() {
    Mikoshiva_Logger::info(LOG_START);

    $input      = Zend_Registry::get('request')->getParams();
    $module     = $input['module'];
    $controller = $input['controller'];
    $action     = $input['action'];
    $key        = Mikoshiva_Utility_String::createRandomString(8);
    // タブ ID が無ければリターン
    if (!isset($input['tabId']) || $input['tabId'] === '') return;

    $tag  = '';
    $tag .= '<form style="display: inline;" action="" name="___f5Former_' . $input['tabId'] . '" id="___f5Former_' . $key . '">' . "\n";

    // 実行
    $retStr = _proccess($input);

    $options = array('url'    => '/' . $module . '/' . $controller . '/' . $action, $input['tabId'],
                     'tabId'  => $input['tabId'],
                     'formId' => '___f5Former_' . $key);

    require_once SMARTY_PLUGINS_DIR . 'function.currentTab.php';
    $tag .= $retStr;
    $tag .= '<input type="button" value="' . '/' . $module . '/' . $controller . '/' . $action . '" onclick="' . smarty_function_currentTab($options, $smarty) . '">';
    $tag .= '</form>';

    Mikoshiva_Logger::info(LOG_END);
    return $tag;
}

/**
 * 変数情報のダンプを作成します。
 *
 * @param $input String|Array ダンプを行いたい文字列または配列
 * @param $name String ダンプした際の接頭辞
 * @param $i
 * @param $k
 * @return void
 */
function _proccess( $input, $i = '0', $k = array('Error') ) {
    Mikoshiva_Logger::info(LOG_START);
    $retStr = '';
    if (is_object($input)) {
        Mikoshiva_Logger::info(LOG_END);
        return $retStr;
    }

    if( is_array( $input ) )
    {
        foreach( $input as $i => $value )
        {
            $temp = $k;
            $temp[] = $i;
            $retStr .= _proccess( $value, $i, $temp );
        }
    }
    else
    {
        $__name = '';

        foreach( $k as $i => $value)
        {
            if($value !== 'Error') {

                if( $__name !== '') {
                  $__name .= '[' . $value . ']';
                } else {
                    $__name .= $value;
                }
            }
        }
        $retStr .= '<input type="hidden" name="' . $__name . '" value="' . $input . '">' . "\n";
    }

    Mikoshiva_Logger::info(LOG_END);
    return $retStr;
}




