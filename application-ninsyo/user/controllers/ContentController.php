<?php
require_once 'Mikoshiva/Controller/Action/Admin/Abstract.php';

/**
 * ユーザーコンテンツを管理するコントローラークラスです。
 *
 * @author      K.fujimoto <fujimoto@d-publishing.jp>
 * @since       2012/06/25
 * @version     SVN: $Id: ContentController.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
 */
class User_ContentController
    extends Mikoshiva_Controller_Action_Admin_Abstract {

}

