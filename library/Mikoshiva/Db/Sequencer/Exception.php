<?php
/**
 * シーケンステーブル例外クラス
 *
 * @package Mikoshiva_Db
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/15
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Db_Sequencer_Exception extends Zend_Db_Table_Exception {

}