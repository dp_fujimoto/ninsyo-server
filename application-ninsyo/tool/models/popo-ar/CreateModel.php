<?php

require_once 'Mikoshiva/Controller/Model/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'MstStaff.php';



/**
 *
 *
 * @author K.Fujimoto <fujimoto@d-publishing.jp>
 * @since 2010/02/17
 * @version SVN:$Id: CreateModel.php,v 1.1 2012/09/21 07:08:32 fujimoto Exp $
 */
class Tool_Models_PopoAr_CreateModel extends Mikoshiva_Controller_Model_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_ActionForm $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $contex
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $contex) {


    include_once $_SERVER['DOCUMENT_ROOT'] . SUB_FOLDER_PATH . '/../tool/popo/create.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . SUB_FOLDER_PATH . '/../tool/ar/create-ninsyo.php';



        return $viewList;
    }
}

























