<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_charge
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxCharge extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tch';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'chargeId' => null,
        'divisionId' => null,
        'directionId' => null,
        'directionStatusId' => null,
        'customerId' => null,
        'shopId' => null,
        'orderId' => null,
        'accessId' => null,
        'accessPass' => null,
        'provisorySalesOrderId' => null,
        'provisorySalesAccessId' => null,
        'provisorySalesAccessPass' => null,
        'provisorySalesFlag' => null,
        'directInstallmentFlag' => null,
        'amount' => null,
        'shippingAmount' => null,
        'paymentDeliveryFee' => null,
        'addAmount' => null,
        'totalAmount' => null,
        'realChargeType' => null,
        'accountingFlag' => null,
        'chargeDate' => null,
        'chargeFlag' => null,
        'chargeErrorFlag' => null,
        'rechargeOrderId' => null,
        'rechargeType' => null,
        'rechargeDueDate' => null,
        'rechargeDate' => null,
        'rechargeErrorFlag' => null,
        'refundProcessingExecutedFlag' => null,
        'refundOrderId' => null,
        'requestStaffId' => null,
        'requestDatetime' => null,
        'requestFlag' => null,
        'refundStaffId' => null,
        'refundAmount' => null,
        'refundType' => null,
        'refundDatetime' => null,
        'refundFlag' => null,
        'refundErrorFlag' => null,
        'debitFlag' => null,
        'errorType' => null,
        'errorCode' => null,
        'errorInfo' => null,
        'serviceStartDate' => null,
        'serviceStopDate' => null,
        'stopFlag' => null,
        'stopDatetime' => null,
        'stopReasonType' => null,
        'memo' => null,
        'chargeCancelFlag' => null,
        'chargeCancelDate' => null,
        'chargeCancelReleaseDate' => null,
        'imperfectionFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 請求ID の値を返します
	 *
     * @return int 請求ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * 請求ID の値をセットします
	 *
	 * @param int $chargeId 請求ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 指示ID の値を返します
	 *
     * @return int 指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * 指示ID の値をセットします
	 *
	 * @param int $directionId 指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * オーダーID の値を返します
	 *
     * @return char オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderId() {
		return $this->_properties['orderId'];
    }


    /**
     * オーダーID の値をセットします
	 *
	 * @param char $orderId オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderId($orderId) {
		$this->_properties['orderId'] = $orderId;
    }


    /**
     * GMOアクセスID の値を返します
	 *
     * @return char GMOアクセスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessId() {
		return $this->_properties['accessId'];
    }


    /**
     * GMOアクセスID の値をセットします
	 *
	 * @param char $accessId GMOアクセスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessId($accessId) {
		$this->_properties['accessId'] = $accessId;
    }


    /**
     * GMOアクセスパス の値を返します
	 *
     * @return char GMOアクセスパス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessPass() {
		return $this->_properties['accessPass'];
    }


    /**
     * GMOアクセスパス の値をセットします
	 *
	 * @param char $accessPass GMOアクセスパス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessPass($accessPass) {
		$this->_properties['accessPass'] = $accessPass;
    }


    /**
     * 仮売上オーダーID の値を返します
	 *
     * @return char 仮売上オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesOrderId() {
		return $this->_properties['provisorySalesOrderId'];
    }


    /**
     * 仮売上オーダーID の値をセットします
	 *
	 * @param char $provisorySalesOrderId 仮売上オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesOrderId($provisorySalesOrderId) {
		$this->_properties['provisorySalesOrderId'] = $provisorySalesOrderId;
    }


    /**
     * 仮売上GMOアクセスID の値を返します
	 *
     * @return char 仮売上GMOアクセスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesAccessId() {
		return $this->_properties['provisorySalesAccessId'];
    }


    /**
     * 仮売上GMOアクセスID の値をセットします
	 *
	 * @param char $provisorySalesAccessId 仮売上GMOアクセスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesAccessId($provisorySalesAccessId) {
		$this->_properties['provisorySalesAccessId'] = $provisorySalesAccessId;
    }


    /**
     * 仮売上GMOアクセスパス の値を返します
	 *
     * @return char 仮売上GMOアクセスパス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesAccessPass() {
		return $this->_properties['provisorySalesAccessPass'];
    }


    /**
     * 仮売上GMOアクセスパス の値をセットします
	 *
	 * @param char $provisorySalesAccessPass 仮売上GMOアクセスパス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesAccessPass($provisorySalesAccessPass) {
		$this->_properties['provisorySalesAccessPass'] = $provisorySalesAccessPass;
    }


    /**
     * 仮売上フラグ の値を返します
	 *
     * @return char 仮売上フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesFlag() {
		return $this->_properties['provisorySalesFlag'];
    }


    /**
     * 仮売上フラグ の値をセットします
	 *
	 * @param char $provisorySalesFlag 仮売上フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesFlag($provisorySalesFlag) {
		$this->_properties['provisorySalesFlag'] = $provisorySalesFlag;
    }


    /**
     * ダイレクト分割フラグ の値を返します
	 *
     * @return char ダイレクト分割フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentFlag() {
		return $this->_properties['directInstallmentFlag'];
    }


    /**
     * ダイレクト分割フラグ の値をセットします
	 *
	 * @param char $directInstallmentFlag ダイレクト分割フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentFlag($directInstallmentFlag) {
		$this->_properties['directInstallmentFlag'] = $directInstallmentFlag;
    }


    /**
     * 金額 の値を返します
	 *
     * @return int 金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAmount() {
		return $this->_properties['amount'];
    }


    /**
     * 金額 の値をセットします
	 *
	 * @param int $amount 金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAmount($amount) {
		$this->_properties['amount'] = $amount;
    }


    /**
     * 発送金額 の値を返します
	 *
     * @return int 発送金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAmount() {
		return $this->_properties['shippingAmount'];
    }


    /**
     * 発送金額 の値をセットします
	 *
	 * @param int $shippingAmount 発送金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAmount($shippingAmount) {
		$this->_properties['shippingAmount'] = $shippingAmount;
    }


    /**
     * 代引き手数料 の値を返します
	 *
     * @return int 代引き手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryFee() {
		return $this->_properties['paymentDeliveryFee'];
    }


    /**
     * 代引き手数料 の値をセットします
	 *
	 * @param int $paymentDeliveryFee 代引き手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryFee($paymentDeliveryFee) {
		$this->_properties['paymentDeliveryFee'] = $paymentDeliveryFee;
    }


    /**
     * 追加金額 の値を返します
	 *
     * @return int 追加金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddAmount() {
		return $this->_properties['addAmount'];
    }


    /**
     * 追加金額 の値をセットします
	 *
	 * @param int $addAmount 追加金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddAmount($addAmount) {
		$this->_properties['addAmount'] = $addAmount;
    }


    /**
     * 合計金額 の値を返します
	 *
     * @return int 合計金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTotalAmount() {
		return $this->_properties['totalAmount'];
    }


    /**
     * 合計金額 の値をセットします
	 *
	 * @param int $totalAmount 合計金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTotalAmount($totalAmount) {
		$this->_properties['totalAmount'] = $totalAmount;
    }


    /**
     * 実請求タイプ の値を返します
	 *
     * @return varchar 実請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealChargeType() {
		return $this->_properties['realChargeType'];
    }


    /**
     * 実請求タイプ の値をセットします
	 *
	 * @param varchar $realChargeType 実請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealChargeType($realChargeType) {
		$this->_properties['realChargeType'] = $realChargeType;
    }


    /**
     * 課金済フラグ の値を返します
	 *
     * @return char 課金済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccountingFlag() {
		return $this->_properties['accountingFlag'];
    }


    /**
     * 課金済フラグ の値をセットします
	 *
	 * @param char $accountingFlag 課金済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccountingFlag($accountingFlag) {
		$this->_properties['accountingFlag'] = $accountingFlag;
    }


    /**
     * 請求日 の値を返します
	 *
     * @return date 請求日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeDate() {
		return $this->_properties['chargeDate'];
    }


    /**
     * 請求日 の値をセットします
	 *
	 * @param date $chargeDate 請求日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeDate($chargeDate) {
		$this->_properties['chargeDate'] = $chargeDate;
    }


    /**
     * 請求フラグ の値を返します
	 *
     * @return char 請求フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeFlag() {
		return $this->_properties['chargeFlag'];
    }


    /**
     * 請求フラグ の値をセットします
	 *
	 * @param char $chargeFlag 請求フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeFlag($chargeFlag) {
		$this->_properties['chargeFlag'] = $chargeFlag;
    }


    /**
     * 請求エラーフラグ の値を返します
	 *
     * @return char 請求エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeErrorFlag() {
		return $this->_properties['chargeErrorFlag'];
    }


    /**
     * 請求エラーフラグ の値をセットします
	 *
	 * @param char $chargeErrorFlag 請求エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeErrorFlag($chargeErrorFlag) {
		$this->_properties['chargeErrorFlag'] = $chargeErrorFlag;
    }


    /**
     * 再請求オーダーID の値を返します
	 *
     * @return char 再請求オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeOrderId() {
		return $this->_properties['rechargeOrderId'];
    }


    /**
     * 再請求オーダーID の値をセットします
	 *
	 * @param char $rechargeOrderId 再請求オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeOrderId($rechargeOrderId) {
		$this->_properties['rechargeOrderId'] = $rechargeOrderId;
    }


    /**
     * 再請求タイプ の値を返します
	 *
     * @return varchar 再請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeType() {
		return $this->_properties['rechargeType'];
    }


    /**
     * 再請求タイプ の値をセットします
	 *
	 * @param varchar $rechargeType 再請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeType($rechargeType) {
		$this->_properties['rechargeType'] = $rechargeType;
    }


    /**
     * 再請求予定日 の値を返します
	 *
     * @return date 再請求予定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeDueDate() {
		return $this->_properties['rechargeDueDate'];
    }


    /**
     * 再請求予定日 の値をセットします
	 *
	 * @param date $rechargeDueDate 再請求予定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeDueDate($rechargeDueDate) {
		$this->_properties['rechargeDueDate'] = $rechargeDueDate;
    }


    /**
     * 再請求日 の値を返します
	 *
     * @return date 再請求日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeDate() {
		return $this->_properties['rechargeDate'];
    }


    /**
     * 再請求日 の値をセットします
	 *
	 * @param date $rechargeDate 再請求日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeDate($rechargeDate) {
		$this->_properties['rechargeDate'] = $rechargeDate;
    }


    /**
     * 再請求エラーフラグ の値を返します
	 *
     * @return char 再請求エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeErrorFlag() {
		return $this->_properties['rechargeErrorFlag'];
    }


    /**
     * 再請求エラーフラグ の値をセットします
	 *
	 * @param char $rechargeErrorFlag 再請求エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeErrorFlag($rechargeErrorFlag) {
		$this->_properties['rechargeErrorFlag'] = $rechargeErrorFlag;
    }


    /**
     * 返金処理実行フラグ の値を返します
	 *
     * @return char 返金処理実行フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundProcessingExecutedFlag() {
		return $this->_properties['refundProcessingExecutedFlag'];
    }


    /**
     * 返金処理実行フラグ の値をセットします
	 *
	 * @param char $refundProcessingExecutedFlag 返金処理実行フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundProcessingExecutedFlag($refundProcessingExecutedFlag) {
		$this->_properties['refundProcessingExecutedFlag'] = $refundProcessingExecutedFlag;
    }


    /**
     * 返金オーダーID の値を返します
	 *
     * @return char 返金オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundOrderId() {
		return $this->_properties['refundOrderId'];
    }


    /**
     * 返金オーダーID の値をセットします
	 *
	 * @param char $refundOrderId 返金オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundOrderId($refundOrderId) {
		$this->_properties['refundOrderId'] = $refundOrderId;
    }


    /**
     * 返金依頼スタッフID の値を返します
	 *
     * @return int 返金依頼スタッフID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRequestStaffId() {
		return $this->_properties['requestStaffId'];
    }


    /**
     * 返金依頼スタッフID の値をセットします
	 *
	 * @param int $requestStaffId 返金依頼スタッフID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRequestStaffId($requestStaffId) {
		$this->_properties['requestStaffId'] = $requestStaffId;
    }


    /**
     * 返金依頼日時 の値を返します
	 *
     * @return datetime 返金依頼日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRequestDatetime() {
		return $this->_properties['requestDatetime'];
    }


    /**
     * 返金依頼日時 の値をセットします
	 *
	 * @param datetime $requestDatetime 返金依頼日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRequestDatetime($requestDatetime) {
		$this->_properties['requestDatetime'] = $requestDatetime;
    }


    /**
     * 返金依頼済フラグ の値を返します
	 *
     * @return char 返金依頼済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRequestFlag() {
		return $this->_properties['requestFlag'];
    }


    /**
     * 返金依頼済フラグ の値をセットします
	 *
	 * @param char $requestFlag 返金依頼済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRequestFlag($requestFlag) {
		$this->_properties['requestFlag'] = $requestFlag;
    }


    /**
     * 返金スタッフID の値を返します
	 *
     * @return int 返金スタッフID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundStaffId() {
		return $this->_properties['refundStaffId'];
    }


    /**
     * 返金スタッフID の値をセットします
	 *
	 * @param int $refundStaffId 返金スタッフID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundStaffId($refundStaffId) {
		$this->_properties['refundStaffId'] = $refundStaffId;
    }


    /**
     * 返金金額 の値を返します
	 *
     * @return int 返金金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundAmount() {
		return $this->_properties['refundAmount'];
    }


    /**
     * 返金金額 の値をセットします
	 *
	 * @param int $refundAmount 返金金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundAmount($refundAmount) {
		$this->_properties['refundAmount'] = $refundAmount;
    }


    /**
     * 返金タイプ の値を返します
	 *
     * @return varchar 返金タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundType() {
		return $this->_properties['refundType'];
    }


    /**
     * 返金タイプ の値をセットします
	 *
	 * @param varchar $refundType 返金タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundType($refundType) {
		$this->_properties['refundType'] = $refundType;
    }


    /**
     * 返金日時 の値を返します
	 *
     * @return datetime 返金日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundDatetime() {
		return $this->_properties['refundDatetime'];
    }


    /**
     * 返金日時 の値をセットします
	 *
	 * @param datetime $refundDatetime 返金日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundDatetime($refundDatetime) {
		$this->_properties['refundDatetime'] = $refundDatetime;
    }


    /**
     * 返金済フラグ の値を返します
	 *
     * @return char 返金済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundFlag() {
		return $this->_properties['refundFlag'];
    }


    /**
     * 返金済フラグ の値をセットします
	 *
	 * @param char $refundFlag 返金済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundFlag($refundFlag) {
		$this->_properties['refundFlag'] = $refundFlag;
    }


    /**
     * 返金エラーフラグ の値を返します
	 *
     * @return char 返金エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundErrorFlag() {
		return $this->_properties['refundErrorFlag'];
    }


    /**
     * 返金エラーフラグ の値をセットします
	 *
	 * @param char $refundErrorFlag 返金エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundErrorFlag($refundErrorFlag) {
		$this->_properties['refundErrorFlag'] = $refundErrorFlag;
    }


    /**
     * デビットフラグ の値を返します
	 *
     * @return char デビットフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDebitFlag() {
		return $this->_properties['debitFlag'];
    }


    /**
     * デビットフラグ の値をセットします
	 *
	 * @param char $debitFlag デビットフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDebitFlag($debitFlag) {
		$this->_properties['debitFlag'] = $debitFlag;
    }


    /**
     * エラータイプ の値を返します
	 *
     * @return char エラータイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorType() {
		return $this->_properties['errorType'];
    }


    /**
     * エラータイプ の値をセットします
	 *
	 * @param char $errorType エラータイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorType($errorType) {
		$this->_properties['errorType'] = $errorType;
    }


    /**
     * エラーコード の値を返します
	 *
     * @return char エラーコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorCode() {
		return $this->_properties['errorCode'];
    }


    /**
     * エラーコード の値をセットします
	 *
	 * @param char $errorCode エラーコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorCode($errorCode) {
		$this->_properties['errorCode'] = $errorCode;
    }


    /**
     * エラー情報 の値を返します
	 *
     * @return char エラー情報
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorInfo() {
		return $this->_properties['errorInfo'];
    }


    /**
     * エラー情報 の値をセットします
	 *
	 * @param char $errorInfo エラー情報
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorInfo($errorInfo) {
		$this->_properties['errorInfo'] = $errorInfo;
    }


    /**
     * サービス開始年月日 の値を返します
	 *
     * @return date サービス開始年月日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceStartDate() {
		return $this->_properties['serviceStartDate'];
    }


    /**
     * サービス開始年月日 の値をセットします
	 *
	 * @param date $serviceStartDate サービス開始年月日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceStartDate($serviceStartDate) {
		$this->_properties['serviceStartDate'] = $serviceStartDate;
    }


    /**
     * サービス終了年月日 の値を返します
	 *
     * @return date サービス終了年月日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceStopDate() {
		return $this->_properties['serviceStopDate'];
    }


    /**
     * サービス終了年月日 の値をセットします
	 *
	 * @param date $serviceStopDate サービス終了年月日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceStopDate($serviceStopDate) {
		$this->_properties['serviceStopDate'] = $serviceStopDate;
    }


    /**
     * 停止フラグ の値を返します
	 *
     * @return char 停止フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopFlag() {
		return $this->_properties['stopFlag'];
    }


    /**
     * 停止フラグ の値をセットします
	 *
	 * @param char $stopFlag 停止フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopFlag($stopFlag) {
		$this->_properties['stopFlag'] = $stopFlag;
    }


    /**
     * 停止日時 の値を返します
	 *
     * @return datetime 停止日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopDatetime() {
		return $this->_properties['stopDatetime'];
    }


    /**
     * 停止日時 の値をセットします
	 *
	 * @param datetime $stopDatetime 停止日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopDatetime($stopDatetime) {
		$this->_properties['stopDatetime'] = $stopDatetime;
    }


    /**
     * 停止理由 の値を返します
	 *
     * @return varchar 停止理由
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStopReasonType() {
		return $this->_properties['stopReasonType'];
    }


    /**
     * 停止理由 の値をセットします
	 *
	 * @param varchar $stopReasonType 停止理由
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStopReasonType($stopReasonType) {
		$this->_properties['stopReasonType'] = $stopReasonType;
    }


    /**
     * メモ の値を返します
	 *
     * @return varchar メモ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * メモ の値をセットします
	 *
	 * @param varchar $memo メモ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * 請求取り消しフラグ の値を返します
	 *
     * @return char 請求取り消しフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeCancelFlag() {
		return $this->_properties['chargeCancelFlag'];
    }


    /**
     * 請求取り消しフラグ の値をセットします
	 *
	 * @param char $chargeCancelFlag 請求取り消しフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeCancelFlag($chargeCancelFlag) {
		$this->_properties['chargeCancelFlag'] = $chargeCancelFlag;
    }


    /**
     * tch_charge_cancel_date の値を返します
	 *
     * @return date tch_charge_cancel_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeCancelDate() {
		return $this->_properties['chargeCancelDate'];
    }


    /**
     * tch_charge_cancel_date の値をセットします
	 *
	 * @param date $chargeCancelDate tch_charge_cancel_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeCancelDate($chargeCancelDate) {
		$this->_properties['chargeCancelDate'] = $chargeCancelDate;
    }


    /**
     * tch_charge_cancel_release_date の値を返します
	 *
     * @return date tch_charge_cancel_release_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeCancelReleaseDate() {
		return $this->_properties['chargeCancelReleaseDate'];
    }


    /**
     * tch_charge_cancel_release_date の値をセットします
	 *
	 * @param date $chargeCancelReleaseDate tch_charge_cancel_release_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeCancelReleaseDate($chargeCancelReleaseDate) {
		$this->_properties['chargeCancelReleaseDate'] = $chargeCancelReleaseDate;
    }


    /**
     * 欠陥フラグ の値を返します
	 *
     * @return char 欠陥フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * 欠陥フラグ の値をセットします
	 *
	 * @param char $imperfectionFlag 欠陥フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























