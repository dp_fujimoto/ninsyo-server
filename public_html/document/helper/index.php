<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 helper</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時  <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document">＜＜　戻る</a></p>

<p>以前のヘルパードキュメントは&ensp;＞＞&ensp;<a href="/document/helper/helper-all.php">こちら</a>&ensp;（もうメンテしません）</p>

        <ol>
          <li><strong style="color:#365F91;">タブ関連</strong>
            <ol>
              <li style="margin-top: 0.5em"><strong>newTab</strong> - 新規タブを開き画面を遷移する
                <ol>
                  <li><a href="/document/helper/newTab.php">newTab - 新規タブを開く</a></li>
                  <li><a href="/document/helper/newTabButton.php">newTabButton - 新規タブを開く（ボタン形式）</a></li>
                  <li><a href="/document/helper/newTabLink.php">newTabLink - 新規タブを開く（リンク形式）</a></li>
                </ol>
              </li>
              <li style="margin-top: 0.5em"><strong>currentTab</strong> - 現在のタブ内で画面を遷移する
                <ol>
                  <li><a href="/document/helper/currentTab.php">currentTab - 現在タブの遷移</a></li>
                  <li><a href="/document/helper/currentTabButton.php">currentTabButton - 現在タブの遷移（ボタン形式）</a></li>
                  <li><a href="/document/helper/currentTabLink.php">currentTabLink - 現在タブの遷移（リンク形式）</a></li>
                </ol>
              </li>
              <li style="margin-top: 0.5em"><strong>closeTabLocation</strong> - 実行されたタブ～指定されたタブまでのタブを閉じ、フォーカスを指定されたタブに移します
                <ol>
                  <li><a href="/document/helper/closeTabLocation.php">closeTabLocation - 指定されたタブにフォーカスを戻し、</a></li>
                  <li><a href="/document/helper/closeTabLocationButton.php">closeTabLocationButton - 現在タブの遷移（ボタン形式）</a></li>
                  <li><a href="/document/helper/closeTabLocationLink.php">closeTabLocationLink - 現在タブの遷移（リンク形式）</a></li>
                </ol>
              </li>
              <li style="margin-top: 0.5em"><strong>requestToIdCrossSite</strong> - 外部ドメインで使用するAjax用のJavascriptを返すヘルパー
                <ol>
                  <li><a href="/document/helper/requestToIdCrossSIte.php">requestToIdCrossSIte - 外部ドメインで使用するAjax用のJavascriptを返す</a></li>
                  <li><a href="/document/helper/requestToIdCrossSIteButton.php">requestToIdCrossSIteButton - 外部ドメインで使用するAjax用のJavascriptを返す(ボタン形式)</a></li>
                  <li><a href="/document/helper/requestToIdCrossSIteLink.php">requestToIdCrossSIteLink - 外部ドメインで使用するAjax用のJavascriptを返す(リンク形式)</a></li>
                </ol>
              </li>
            </ol>
          </li>
          <li>
            <strong style="color:#365F91;">リスター関連</strong>
            <ol>
              <li><a href="/document/helper/html_select_dateLister.php">html_select_dateDateLister - 年、月、日、それぞれのセレクトボックスのhtmlを返します</a></li>
              <li><a href="/document/helper/html_select_timeLister.php">html_select_dateTimeLister - 時、分、秒、それぞれのセレクトボックスのhtmlを返します</a></li>
              <li><a href="/document/helper/formSelectPopo.php">formSelectPopo - 同種のPopoの配列を元に、セレクトボックスを生成して返します。 </a></li>
            </ol>
          </li>
        </ol>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>