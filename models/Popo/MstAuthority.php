<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_authority
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstAuthority extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mau';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'authorityId' => null,
        'staffId' => null,
        'productAccessFlag' => null,
        'inquiryAccessFlag' => null,
        'customerAccessFlag' => null,
        'cancellationAccessFlag' => null,
        'chargeAccessFlag' => null,
        'shippingAccessFlag' => null,
        'orderAccessFlag' => null,
        'settlementAccessFlag' => null,
        'changeshippingaddressAccessFlag' => null,
        'receivedAccessFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 権限ID の値を返します
	 *
     * @return int 権限ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAuthorityId() {
		return $this->_properties['authorityId'];
    }


    /**
     * 権限ID の値をセットします
	 *
	 * @param int $authorityId 権限ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAuthorityId($authorityId) {
		$this->_properties['authorityId'] = $authorityId;
    }


    /**
     * スタッフID の値を返します
	 *
     * @return int スタッフID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStaffId() {
		return $this->_properties['staffId'];
    }


    /**
     * スタッフID の値をセットします
	 *
	 * @param int $staffId スタッフID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStaffId($staffId) {
		$this->_properties['staffId'] = $staffId;
    }


    /**
     * 商品アクセスフラグ の値を返します
	 *
     * @return char 商品アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductAccessFlag() {
		return $this->_properties['productAccessFlag'];
    }


    /**
     * 商品アクセスフラグ の値をセットします
	 *
	 * @param char $productAccessFlag 商品アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductAccessFlag($productAccessFlag) {
		$this->_properties['productAccessFlag'] = $productAccessFlag;
    }


    /**
     * 問い合わせアクセスフラグ の値を返します
	 *
     * @return char 問い合わせアクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInquiryAccessFlag() {
		return $this->_properties['inquiryAccessFlag'];
    }


    /**
     * 問い合わせアクセスフラグ の値をセットします
	 *
	 * @param char $inquiryAccessFlag 問い合わせアクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInquiryAccessFlag($inquiryAccessFlag) {
		$this->_properties['inquiryAccessFlag'] = $inquiryAccessFlag;
    }


    /**
     * 顧客対応アクセスフラグ の値を返します
	 *
     * @return char 顧客対応アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerAccessFlag() {
		return $this->_properties['customerAccessFlag'];
    }


    /**
     * 顧客対応アクセスフラグ の値をセットします
	 *
	 * @param char $customerAccessFlag 顧客対応アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerAccessFlag($customerAccessFlag) {
		$this->_properties['customerAccessFlag'] = $customerAccessFlag;
    }


    /**
     * 解約アクセスフラグ の値を返します
	 *
     * @return char 解約アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCancellationAccessFlag() {
		return $this->_properties['cancellationAccessFlag'];
    }


    /**
     * 解約アクセスフラグ の値をセットします
	 *
	 * @param char $cancellationAccessFlag 解約アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCancellationAccessFlag($cancellationAccessFlag) {
		$this->_properties['cancellationAccessFlag'] = $cancellationAccessFlag;
    }


    /**
     * 請求アクセスフラグ の値を返します
	 *
     * @return char 請求アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeAccessFlag() {
		return $this->_properties['chargeAccessFlag'];
    }


    /**
     * 請求アクセスフラグ の値をセットします
	 *
	 * @param char $chargeAccessFlag 請求アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeAccessFlag($chargeAccessFlag) {
		$this->_properties['chargeAccessFlag'] = $chargeAccessFlag;
    }


    /**
     * 発送アクセスフラグ の値を返します
	 *
     * @return char 発送アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAccessFlag() {
		return $this->_properties['shippingAccessFlag'];
    }


    /**
     * 発送アクセスフラグ の値をセットします
	 *
	 * @param char $shippingAccessFlag 発送アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAccessFlag($shippingAccessFlag) {
		$this->_properties['shippingAccessFlag'] = $shippingAccessFlag;
    }


    /**
     * 注文アクセスフラグ の値を返します
	 *
     * @return char 注文アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderAccessFlag() {
		return $this->_properties['orderAccessFlag'];
    }


    /**
     * 注文アクセスフラグ の値をセットします
	 *
	 * @param char $orderAccessFlag 注文アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderAccessFlag($orderAccessFlag) {
		$this->_properties['orderAccessFlag'] = $orderAccessFlag;
    }


    /**
     * 決済アクセスフラグ の値を返します
	 *
     * @return char 決済アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSettlementAccessFlag() {
		return $this->_properties['settlementAccessFlag'];
    }


    /**
     * 決済アクセスフラグ の値をセットします
	 *
	 * @param char $settlementAccessFlag 決済アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSettlementAccessFlag($settlementAccessFlag) {
		$this->_properties['settlementAccessFlag'] = $settlementAccessFlag;
    }


    /**
     * 配送方法変更アクセスフラグ の値を返します
	 *
     * @return char 配送方法変更アクセスフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChangeshippingaddressAccessFlag() {
		return $this->_properties['changeshippingaddressAccessFlag'];
    }


    /**
     * 配送方法変更アクセスフラグ の値をセットします
	 *
	 * @param char $changeshippingaddressAccessFlag 配送方法変更アクセスフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChangeshippingaddressAccessFlag($changeshippingaddressAccessFlag) {
		$this->_properties['changeshippingaddressAccessFlag'] = $changeshippingaddressAccessFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReceivedAccessFlag() {
		return $this->_properties['receivedAccessFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $receivedAccessFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReceivedAccessFlag($receivedAccessFlag) {
		$this->_properties['receivedAccessFlag'] = $receivedAccessFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return timestamp 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























