<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Mapping.php';
/**
 * ショップIDを元にショップ名を取得して返します。
 *
 * @param  array    $params
 *                          string shopId ショップID
 * @param  Smarty   $smarty
 * @param  Template $template
 * @return string   $result ショップ名
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.gmoMapping.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_gmoMapping($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default           = array();
    $default['shopId'] = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['shopId']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 shopId に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $result = '';
    try{
        $result = Mikoshiva_Utility_Mapping::gmoMapping($data['shopId']);
    } catch (Mikoshiva_Utility_Exception $e){
        Mikoshiva_Logger::debug($e);
        $result = '';
    }
    Mikoshiva_Logger::info(LOG_END);
    return $result;
}

