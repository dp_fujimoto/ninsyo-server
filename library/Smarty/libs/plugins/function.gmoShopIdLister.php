<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Mapping.php';

/**
 * ショップ ID のリストをselect or radio で表示します。
 *
 * @param array $params
 *                      string       name                       フォームのname, id
 *                      string       selected        空文字   | selectedにする値
 *                      boolean      addShopNameFlag ture     | ショップ名を追記
 *                      string       mode            select   | select/radio
 *                      array        attribs         array()  | selectタグに指定する追加の属性 '属性名' => 値
 *                      boolean      addEmpty        false    | 先頭項目に空項目を追加
 *                      boolean      addAllItem      false    | 先頭項目に【全て】の項目を追加
 *                      string       listsep         '<br>'   |  各エンティティを区切る文字列
 * @param Smarty   $smarty
 * @param Template $template
 *
 * @return string                    ショップIDのリスト(select/radio)
 *
 * @package Smarty_libs
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/04
 * @version SVN:$Id: function.gmoShopIdLister.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_gmoShopIdLister($params, &$smarty, &$template) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                    = array();
    $default['name']            = '';
    $default['selected']        = '';
    $default['addShopNameFlag'] = true;
    $default['mode']            = 'select';
    $default['attribs']         = array();
    $default['addEmpty']        = false;
    $default['addAllItem']      = false;
    $default['listsep']         = '<br>';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }
    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    // GMO のマッピングデータを取得
    $GMO_CONFIG = array();
    $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG'); //GMOのマッピングデータを取得


    $shopIdList = array();
    foreach ($GMO_CONFIG as $k => $v) {

        foreach ($v['SHOP'] as $k2 => $v2) {

            // ショップ名を追加する or しない
            if ($data['addShopNameFlag']) {
                $shopIdList[ (string)$k2 ] = $k2 . ' ' . $v2['NAME'];
            } else {
                $shopIdList[ (string)$k2 ] = $k2;
            }

        }
    }


    $data['options'] = $shopIdList;

        //==========================================
        // mode によって表示を切り替える
        // 不明な mode であれば空文字が返ります
        //==========================================

    if ( $data['mode'] === 'select' ) {
        // select -------------------------------
        require_once SMARTY_PLUGINS_DIR . 'function.formSelect.php';
        Mikoshiva_Logger::info(LOG_END);
        return smarty_function_formSelect($data, $smarty, $template);
    }

    if ( $data['mode'] === 'radio' ) {
        // radio -------------------------------
        require_once SMARTY_PLUGINS_DIR . 'function.formRadio.php';
        Mikoshiva_Logger::info(LOG_END);
        return smarty_function_formRadio($data, $smarty, $template);
    }

    return '';

}


?>
