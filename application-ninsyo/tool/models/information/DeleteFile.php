<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstInformationHistory.php';

/**
 * 指定した添付ファイルを削除する
 *
 * @author       K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/28
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Information_DeleteFile
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定した添付ファイルを削除する
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $db = new Mikoshiva_Db_Simple();
        $informationHistoryId = $actionForm->getInformationHistoryId();
        $hash = $actionForm->getHash();

        // データ削除
        if (file_exists(UPLOADS_DIR . '/tool/information/' . $hash)){
            $informationHistory = $db->simpleOneSelectById('Mikoshiva_Db_Ar_MstInformationHistory', $actionForm->getInformationHistoryId());

            $body = $informationHistory->getBody();
            $bodyArr = explode('___FILEDATA___',$body);

            $str = '';
            foreach ($bodyArr as $k => $v) {
                if (!stristr($v,$hash)) {
                    if ($k > 0) {
                        $str .= '___FILEDATA___';
                    }
                    $str .= $v;
                }
            }

            $informationHistory->setBody($str);

            // 更新（シンプルクラス）
            $result = $db->simpleOneCreateOrUpdateById( 'Mikoshiva_Db_Ar_MstInformationHistory', $informationHistory  );

            unlink(UPLOADS_DIR . '/tool/information/' . $hash);
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }

}
