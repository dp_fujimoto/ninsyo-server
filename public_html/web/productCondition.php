<?php
include '../api/define-api.php';

//------------------------------------------------------------------
// WEB認証
//------------------------------------------------------------------
$url = '/api/content/get-product-content-data';

$post = array();
$post['serverName'] = '';
$post['productCode'] = '';
$post['productShippingCode'] = '';
$post['packageNumber'] = '';
$post['campaignCode'] = '';
$post['campaignSalesCode'] = '';
$post['subscriptionType'] = '';
$post['productType'] = '';
if (isset($_REQUEST['serverName'])) {
    $post['serverName'] = $_REQUEST['serverName'];
}
if (isset($_REQUEST['productCode'])) {
    $post['productCode'] = $_REQUEST['productCode'];
}
if (isset($_REQUEST['productShippingCode'])) {
    $post['productShippingCode'] = $_REQUEST['productShippingCode'];
}
if (isset($_REQUEST['packageNumber'])) {
    $post['packageNumber'] = $_REQUEST['packageNumber'];
}
if (isset($_REQUEST['campaignCode'])) {
    $post['campaignCode'] = $_REQUEST['campaignCode'];
}
if (isset($_REQUEST['campaignSalesCode'])) {
    $post['campaignSalesCode'] = $_REQUEST['campaignSalesCode'];
}
if (isset($_REQUEST['subscriptionType'])) {
    $post['subscriptionType'] = $_REQUEST['subscriptionType'];
}
if (isset($_REQUEST['productType'])) {
    $post['productType'] = $_REQUEST['productType'];
}

echo send_socket($url, $post, $_SERVER);
