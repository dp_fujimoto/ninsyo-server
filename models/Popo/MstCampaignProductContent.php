<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_campaign_product_content
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstCampaignProductContent extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mco';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'campaignProductContentId' => null,
        'campaignProductId' => null,
        'number' => null,
        'creditCardFlag' => null,
        'paymentDeliveryFlag' => null,
        'bankTransferFlag' => null,
        'directInstallmentFlag' => null,
        'title' => null,
        'price' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * キャンペーン商品内容ID の値を返します
	 *
     * @return int キャンペーン商品内容ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductContentId() {
		return $this->_properties['campaignProductContentId'];
    }


    /**
     * キャンペーン商品内容ID の値をセットします
	 *
	 * @param int $campaignProductContentId キャンペーン商品内容ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductContentId($campaignProductContentId) {
		$this->_properties['campaignProductContentId'] = $campaignProductContentId;
    }


    /**
     * キャンペーン商品ID の値を返します
	 *
     * @return int キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductId() {
		return $this->_properties['campaignProductId'];
    }


    /**
     * キャンペーン商品ID の値をセットします
	 *
	 * @param int $campaignProductId キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductId($campaignProductId) {
		$this->_properties['campaignProductId'] = $campaignProductId;
    }


    /**
     * 順番 の値を返します
	 *
     * @return int 順番
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNumber() {
		return $this->_properties['number'];
    }


    /**
     * 順番 の値をセットします
	 *
	 * @param int $number 順番
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNumber($number) {
		$this->_properties['number'] = $number;
    }


    /**
     * クレジットカード決済フラグ の値を返します
	 *
     * @return char クレジットカード決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCreditCardFlag() {
		return $this->_properties['creditCardFlag'];
    }


    /**
     * クレジットカード決済フラグ の値をセットします
	 *
	 * @param char $creditCardFlag クレジットカード決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCreditCardFlag($creditCardFlag) {
		$this->_properties['creditCardFlag'] = $creditCardFlag;
    }


    /**
     * 代金引換決済フラグ の値を返します
	 *
     * @return char 代金引換決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDeliveryFlag() {
		return $this->_properties['paymentDeliveryFlag'];
    }


    /**
     * 代金引換決済フラグ の値をセットします
	 *
	 * @param char $paymentDeliveryFlag 代金引換決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDeliveryFlag($paymentDeliveryFlag) {
		$this->_properties['paymentDeliveryFlag'] = $paymentDeliveryFlag;
    }


    /**
     * 銀行振込決済フラグ の値を返します
	 *
     * @return char 銀行振込決済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBankTransferFlag() {
		return $this->_properties['bankTransferFlag'];
    }


    /**
     * 銀行振込決済フラグ の値をセットします
	 *
	 * @param char $bankTransferFlag 銀行振込決済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBankTransferFlag($bankTransferFlag) {
		$this->_properties['bankTransferFlag'] = $bankTransferFlag;
    }


    /**
     * ダイレクト分割フラグ の値を返します
	 *
     * @return char ダイレクト分割フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentFlag() {
		return $this->_properties['directInstallmentFlag'];
    }


    /**
     * ダイレクト分割フラグ の値をセットします
	 *
	 * @param char $directInstallmentFlag ダイレクト分割フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentFlag($directInstallmentFlag) {
		$this->_properties['directInstallmentFlag'] = $directInstallmentFlag;
    }


    /**
     * タイトル の値を返します
	 *
     * @return varchar タイトル
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTitle() {
		return $this->_properties['title'];
    }


    /**
     * タイトル の値をセットします
	 *
	 * @param varchar $title タイトル
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTitle($title) {
		$this->_properties['title'] = $title;
    }


    /**
     * 価格 の値を返します
	 *
     * @return int 価格
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrice() {
		return $this->_properties['price'];
    }


    /**
     * 価格 の値をセットします
	 *
	 * @param int $price 価格
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrice($price) {
		$this->_properties['price'] = $price;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























