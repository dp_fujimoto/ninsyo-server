<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Output_Payment_Change
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Gmo_Payment_Change extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'member' => null,
        'card' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * member
	 *
     * @return  Mikoshiva_Settlement_Gmo_Popo_Output_Member_Save
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMember() {
		return $this->_properties['member'];
    }


    /**
     * member
	 *
	 * @param  Mikoshiva_Settlement_Gmo_Popo_Output_Member_Save $member
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMember(Mikoshiva_Settlement_Gmo_Popo_Output_Member_Save $member) {
		$this->_properties['member'] = $member;
    }


    /**
     * card
	 *
     * @return Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCard() {
		return $this->_properties['card'];
    }


    /**
     * card
	 *
	 * @param Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save $card
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Change.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCard(Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save $card) {

        include_once 'Mikoshiva/Settlement/Gmo/Popo/Output/Card/Save.php';
        $card = new Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save();
        $card->setCardSeq('1'); // カード登録連番
        $card->setCardNo('41111111111');  // カード番号（下４桁伏字）
        $card->setForward('1234567'); // 仕向先コード
        $card->setErrCode('E01'); // エラーコード
        $card->setErrInfo('E01020001'); // エラー詳細コード
		$this->_properties['card'] = $card;
    }


}

























