#!/bin/bash
#
# ninsyo_log_analysis_edge_trade.sh loader
#
# @since  2012/3/27
# @author k.fujimoto
#

#include library classpath
. /usr/local/bin/miko_include_classpath.sh

##################################################
# define variables
##################################################
DATE=$(date +%Y%m%d)

BINDIR="/var/www/${SERVERMODEID}/www/private_bin/sh"
BINFILE="${BINDIR}/ninsyo_log_analysis_edge_trade_exec.sh"

LOGDIR="/var/www/${SERVERMODEID}/www/systems/sh_logs"
LOGFILE="${LOGDIR}/ninsyo_log_analysis_edge_trade_${DATE}.log"

##################################################
# check same process
##################################################
ps -ef | grep ${BINFILE} | grep -v grep
STATUS=$?
if [ ${STATUS} -eq 0 ]; then
  echo [$(date +%F_%T)] this script has already run. >> ${LOGFILE}
  exit ${STATUS}
fi

##################################################
# start main script
##################################################
exec ${BINFILE}
