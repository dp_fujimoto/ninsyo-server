<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/CloseTabCurrent.php';

/**
 * 新しいタブを開きます
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: CloseTabCurrentButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_CloseTabCurrentButton extends Mikoshiva_View_Helper_Abstract {

    /**
     * 新しいタブを開き、タブの保管を行います
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: CloseTabCurrentButton.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $url 新規タブの URL
     * @param boolean $storage タブの保存 trueなら保存
     * @return unknown_type
     */
    public function closeTabCurrentButton($name, $tabId) {
        $a = new Mikoshiva_View_Helper_CloseTabCurrent();
        return '<input type="button" value="' . $name . '" onclick="' . $a->closeTabCurrent($tabId) . '">';
    }
}
