<?php
require_once 'batch/models/classes/ReportManager.php';

/**
 * レポート情報を管理し処理するクラス
 *
 * @author fujimoto <fujimoto@d-publishing.jp>
 * @since  2012/06/29
 */
class Batch_Models_Classes_UserContent_InsertByTaskReportManager extends Batch_Models_Classes_ReportManager {

    /**
     * 自身のオブジェクト
     */
    private static $_instance = null;

    /**
     * 結果格納用
     */
    public $fileList = array();
    public $deleteUserNum = 0;
    public $deleteUserContentNum = 0;
    public $errorFlag = 0;

    /**
     * このクラスのインスタンスを取得する
     * ※singleton
     *
     */
    public static function getInstance() {

        if (isBlankOrNull(self::$_instance) === true) {
            self::$_instance = new self();
            self::$_instance->setStockProcessTimeStart('__TOTAL__');
        }

        return self::$_instance;
    }

    /**
     * レポートメールを送信します
     *
     *
     */
    public function sendReportMail($date) {

        //------------------------------------------------------------------
        // ■件名を生成
        //------------------------------------------------------------------
        if ($this->errorFlag !== '1') {
            $title = '[' . $date . '] ユーザーコンテンツ登録タスク処理バッチが完了しました。';
        } else {
            $title = '[' . $date . '] ユーザーコンテンツ登録タスク処理バッチの実行に失敗しました。';
        }

        //------------------------------------------------------------------
        // ■本文を生成
        //------------------------------------------------------------------
        $body   = "";
        $body  .= "[実行日]"          . "\n";
        $body  .= "{$date}"           . "\n";

        if ($this->errorFlag !== '1') {
            $body .= ""               . "\n";
            $body .= "[処理ファイル]" . "\n";
            foreach ($this->fileList as $k => $v) {
                $body .= $v      . "\n";
            }

            $body .= ""                      . "\n";
            $body .= "[削除ユーザー数]"      . "\n";
            $body .= $this->deleteUserNum    . "\n";
            $body .= "[削除ユーザーコンテンツ数]" . "\n";
            $body .= $this->deleteUserContentNum  . "\n";
        }


        //------------------------------------------------------------------
        // ■送信
        //------------------------------------------------------------------
        $this->sendReportMailBase($title,$body);
    }
}

















































