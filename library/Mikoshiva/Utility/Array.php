<?php
/**
 * @package Mikoshiva_Utility
 */



/**
 * 配列ユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: Array.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Array {


    /**
     * 配列のキー名が $prefix で始まるもだけを抽出し、返します。
     *
     * @param string $prefix
     * @param array $data
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/18
     * @version SVN:$Id: Array.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function extractBeginningOfPrefix($prefix, array $data ) {

        if($prefix === '' || $prefix === null) {
            include_once 'Mikoshiva/Exception.php';
            throw new Mikoshiva_Exception('【Mikoshiva_Utility_Array#extractBeginningOfPrefix】第一引数の値が空です。');
        }

        //-------------------------------------------------
        // ■プレフィックスがキーの者だけを抽出
        //-------------------------------------------------
        $targetParam = array();
        $param       = array();
        $param       = $data;
        foreach ($param as $k => $v) {
            if (preg_match('#^' . $prefix . '#', $k) === 1) {
                $targetParam[$k] = $v;
            }
        }

        return $targetParam;
    }


    /**
     * 引数の配列から重複値一つにまとめ、、空文字、NULLを除去し、添字配列で再初期化を行います
     * （注意）連想配列のキー名は維持されません
     *
     * @param array $data
     * @return array
     */
    public static function removeEmpty( array $data )
    {
        $newData = array();
        foreach ($data as $k => $v) {

            // 値が存在しなければ continue
            if ($v === null || $v === '') continue;

            $newData[$k] = $v;
        }

        return $newData;
    }


    /**
     * 配列の共通項目で配列を生成し直します
     *
     * 同一と判定される条件は配列のキーと値の２つが同一である場合です
     *
     *
     *
     *  // ■値を調べるもととなる配列
     *  $array1             = array();
     *  $array1["0"]        = "green";
     *  $array1["b"]        = 123;
     *  $array1["d"]        = "blue";
     *  $array1[1]          = "red";
     *  $array1['c']        = null;
     *  $array1['zip']      = '455-0014';
     *  $array1['lastName'] = '谷口';
     *
     *  // ■値を比較する対象となる配列
     *  $array2             = array();
     *  $array2["0"]        = "green";
     *  $array2[3]          = "yellow";
     *  $array2[1]          = "red";
     *  $array2['b']        = '123';
     *  $array2['c']        = false;
     *  $array2['zip1']     = '455';
     *  $array2['zip2']     = '0014';
     *  $array2['lastName'] = '';
     *
     *  // ■結果
     *  Array
     *  (
     *      [0] => green
     *      [1] => red
     *      [zip] => 455-0014
     *  )
     *
     *
     *
     * @param array $array1 値を調べるもととなる配列
     * @param array $array2 値を比較する対象となる配列
     * @param array $sourceConvertData
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/10
     * @version SVN:$Id: Array.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function intersectAssoc(array $targetArray,
                                          array $compareArray,
                                          array $sourceConvertData = array()) {


        // ログスタート
        logInfo(LOG_START);


        // コピー
        $_targetArray  = $targetArray;
        $_compareArray = $compareArray;


        //-----------------------------------
        // 共通項目判定から除外リストを作成
        //
        // 対象は以下の様な形
        // $popo[zip] $form[zip1]
        //            $form[zip2]
        //-----------------------------------
        $exclusionArray = array();
        foreach ($sourceConvertData as $k => $v) {

            $flag = true;

            // $_targetArray に zip が有り～
            if (array_key_exists($k, $_targetArray)) {

                // $_compareArray に zip1 zip2 が有れば～
                foreach ($v as $k2 => $v2) {

                    if (! array_key_exists($v2, $_compareArray)) {
                        $flag = false;
                        break;
                    }
                }

                if (! $flag) break;


                // ～新規配列にセット
                $exclusionArray[$k] = $_targetArray[$k];

                // $_targetArray から削除
                unset($_targetArray[$k]);
            }
        }

//dumper($exclusionArray);exit;
        //-----------------------------------
        // 共通項目リストのみの配列を生成
        //-----------------------------------
        $commonArray = array();
        // $commonArray = array_intersect_assoc($_targetArray, $_compareArray); // 厳格なチェックを行わない為仕様しない
        foreach ($_targetArray as $k => $v) {

            // キーと値が同一で有れば共通項目リストを生成（注１： array_key_exists は厳密な型チェックは行わない　注２：キーに関しては特に影響はないと考えています）
            if (array_key_exists($k, $_compareArray) && $_compareArray[$k] === $v) {
                $commonArray[$k] = $v;
            }
        }


        //-----------------------------------
        // 除外リストと共通項目リストを合成
        //-----------------------------------
        $newArray = array();
        $newArray = $commonArray + $exclusionArray;


        // ログ終了
        logInfo(LOG_END);


        // return
        return $newArray;
    }
}

































