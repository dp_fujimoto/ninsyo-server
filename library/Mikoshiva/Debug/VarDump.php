<?php
/**
 * @package Mikoshiva_Debug
 */

/**
 * 変数に関する情報見やすい形式でダンプします
 *
 * @package Mikoshiva_Debug
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/09
 * @version SVN:$Id: VarDump.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Debug_VarDump
{
    /**
     * 変数情報を格納します
     *
     * @var String
     */
    private static $retStr = '';

    /**
     * ダンプを行い、変数情報を文字列で返します
     * オブジェクトは未対応
     *
     * @param $input String|Array ダンプを行いたい文字列または配列
     * @param $name String ダンプした際の接頭辞
     * @return string 変数の情報
     */
    public static function exec( $input, $name = '$_DEBUG')
    {
        // 実行
        self::proccess($input, $name );

        // 取得
        $retStr = self::$retStr;

        // 初期化
        self::$retStr = '';

        return trim( $retStr );
    }

    /**
     * 変数情報のダンプを作成します
     *
     * @param $input String|Array ダンプを行いたい文字列または配列
     * @param $name String ダンプした際の接頭辞
     * @param $i
     * @param $k
     * @return void
     */
    private static function proccess( $input, $name, $i = '0', $k = array('Error') )
    {

        if( is_array( $input ) )
        {
            foreach( $input as $i => $value )
            {
                $temp = $k;
                $temp[] = $i;
                self::proccess( $value, $name, $i, $temp );
            }
        }
        else if (is_object($input)) {
        }
        else
        {
            self::$retStr .=  $name;
            foreach( $k as $i => $value)
            {
                if($value !== 'Error') {
                    self::$retStr .= '[' . $value . ']';
                }
            }
            self::$retStr .= ' = ' . $input . "\n";
        }
    }
}