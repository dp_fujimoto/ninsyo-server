<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/helper">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> ヘルパー</h1>


            <p>Ajax で指定されたURLへリクエスト(idで指定されたフォーム部品の情報をpost)を行い、<br>
               結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うjavascriptを返します。</p>
            <p style="color: #bb0000;">※外部ドメインで用いるページ内での遷移に使用することを想定しています。</p>
<!-- =================================================================
    ■requestToIdCrossSite
====================================================================== -->
          <table class="wiki">
            <tr>
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>アクション URL。"/shipping/dm-step-shipping/search" のような、Mikoshiva上でのパスを指定します。</td>
            </tr>
            <tr>
              <td>targetId</td>
              <td>string</td>
              <td>-</td>
              <td>-</td>
              <td>結果書き込み先(htmlタグのID)。アクションが出力したビューを反映する場所をしていします。</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>append</td>
              <td>string</td>
              <td>-</td>
              <td>OVERWRITE</td>
              <td>追記モード(FRONT/前方へ挿入/BACK:後方へ追記/OVERWRITE:上書き)</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string("<?php
{requestToIdCrossSite url='/sample/trader/confirm' targetId='content' formId='updateForm'}
"); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string("
requestToId('/sample/trader/confirm', 'content', '', 'OVERWRITE');
"); ?>
            </pre>


  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>


