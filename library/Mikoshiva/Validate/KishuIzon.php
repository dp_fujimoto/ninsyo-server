<?php
/**
 * @package Mikoshiva_Validate
 */


/**
 * require
 */
require_once 'Zend/Validate/Abstract.php';
require_once 'Mikoshiva/Utility/String.php';


/**
 * @category Mikoshiva_Validate
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/10
 * @version SVN:$Id: KishuIzon.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_KishuIzon extends Zend_Validate_Abstract {


    const INVALID = 'notUrl';


    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID => "【%COLUMN_NAME%】には機種依存文字&ensp;%__STR__%&ensp;を使用することは出来ません。"
    );


    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid IP address
     *
     * @param mixed $value
     * @return boolean
     */
    public function isValid($value) {

        // 文字列に含まれる機種依存文字を取得する
        $list = array();
        $list = Mikoshiva_Utility_String::searchPlatformDependentCharacters($value);

//dumper($list);exit;
        // 機種依存文字が含まれていたらエラー
        if (count($list) !== 0) {
            $this->_messageTemplates[self::INVALID] = str_replace('%__STR__%', implode('、',  $list), $this->_messageTemplates[self::INVALID]);
            $this->_error(self::INVALID);
            return false;
        }

        return true;
    }

}





































