<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * 指定した商品IDの情報を入力画面にプリセットする。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Product_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定した商品IDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        if (isBlankOrNull($actionForm->getDivisionId())) {
            $actionForm->setDivisionId($context->getStaff()->changeDivisionId);
        }

        $productId = $actionForm->getProductId();

        if (!isBlankOrNull($productId)) {

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $productData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstProduct', $productId);

            $actionForm = Mikoshiva_Utility_Copy::copyPopoToActionFormProperties($actionForm, $productData);

            if (isBlankOrNull($actionForm->getImageUpdateDatetime())) {
                $actionForm->setImageNoneFlag('1');
            }

            $brandData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstBrand', $productData->getBrandId());
            $actionForm->setDivisionId($brandData->getDivisionId());
        } else {
            $actionForm->setImageNoneFlag('1');
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }

}
