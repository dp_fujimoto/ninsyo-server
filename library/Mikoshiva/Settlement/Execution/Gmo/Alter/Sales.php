<?php
/** @package Mikoshiva_Gmo */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Abstract.php';


/**
 * 仮売上　→　実売上の処理を行います
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Sales.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Alter_Sales extends Mikoshiva_Settlement_Execution_Gmo_Alter_Abstract {

    /**
     * 処理区分 を保持します（必須）
     * @var string 処理区分 を保持します（必須）
     */
    protected $_jobCd = 'SALES';

}