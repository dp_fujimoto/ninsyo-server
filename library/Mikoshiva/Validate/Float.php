<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Float
 */
require_once 'Zend/Validate/Float.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Float.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Float extends Zend_Validate_Float
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_FLOAT => "%COLUMN_NAME%は数値ではないようです"
    );
}
