<?php


abstract class Popo_Abstract {


    protected $_properties = array();

    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:20 fujimoto Exp $
     */
    public function __constract(array $dataList = array()) {

        // プロパティを初期化
        $this->copyProperty($dataList);

    }


    /**
     * プロパティの一覧を返します
	 *
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Abstract.php,v 1.1 2012/09/21 07:08:20 fujimoto Exp $
     *
     */
    public function getProperties() {
		return $this->_properties;
    }


    public function copyProperty(array $dataList) {

        foreach ($dataList as $k => $v) {
            if (array_key_exists($k, $this->_propertys) ) {
                 $this->_propertys[$k] = $v;
            }
        }
    }
}