<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstBrand.php';
require_once 'tool/models/Operation.php';
require_once 'api/models/Operation.php';

/**
 * ブランド情報の登録を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Brand_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * ブランド情報の登録を行う。
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // ログ文
        $logText= '';

        // 空のデータはnullにするため、配列にして処理してからactionFormに詰める
        $data = Mikoshiva_Utility_Copy::copyToArray($actionForm);
        foreach ($data as $k => $v) {
            $data[$k] = Mikoshiva_Utility_Convert::blankToNull($v);
        }
        Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($actionForm,$data);

        //-----------------------------------------------
        // ブランド登録
        //-----------------------------------------------
        $brandId = $actionForm->getBrandId();
        if (!isBlankOrNull($brandId)) {

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $brandData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstBrand', $brandId);
            $logText .= $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName() . 'がブランドID:' . $brandId . 'のデータ更新。';

            $actionForm->setBrandCode($brandId);
            $actionForm->setLogoUpdateDatetime($brandData->getLogoUpdateDatetime());
            $actionForm->setBannerVerticalUpdateDatetime($brandData->getBannerVerticalUpdateDatetime());
            $actionForm->setBannerSidewaysUpdateDatetime($brandData->getBannerSidewaysUpdateDatetime());

        } else {
            $brandData = new Popo_MstBrand();
            $actionForm->setBrandCode('temporary');
            $logText .= $context->getStaff()->mstStaff->getLastName() . $context->getStaff()->mstStaff->getFirstName() . 'が' . $actionForm->getBrandName() . 'というブランド登録。';
        }

        Mikoshiva_Utility_Copy::copyToPopo($brandData, $actionForm);

        if (!isBlankOrNull($brandData->getLogoUrl()) && $actionForm->getLogoUpdateFlag() === '1') {
            $brandData->setLogoUpdateDatetime(Mikoshiva_date::mikoshivaNow());
        }
        if (!isBlankOrNull($brandData->getBannerVerticalUrl()) && $actionForm->getBannerVerticalUpdateFlag() === '1') {
            $brandData->setBannerVerticalUpdateDatetime(Mikoshiva_date::mikoshivaNow());
        }
        if (!isBlankOrNull($brandData->getBannerSidewaysUrl()) && $actionForm->getBannerSidewaysUpdateFlag() === '1') {
            $brandData->setBannerSidewaysUpdateDatetime(Mikoshiva_date::mikoshivaNow());
        }

        if (isBlankOrNull($brandData->getDeletionDatetime())) {
            if ($brandData->getDeleteFlag() === '1') {
                $brandData->setDeletionDatetime(Mikoshiva_date::mikoshivaNow());
            } else {
                $brandData->setDeleteFlag('0');
                $brandData->setDeletionDatetime(null);
            }
        } else {
            if ($brandData->getDeleteFlag() !== '1') {
                $brandData->setDeleteFlag('0');
                $brandData->setDeletionDatetime(null);
            }
        }

        // 更新（シンプルクラス）
        $db = null;
        $db = new Mikoshiva_Db_Simple();

        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstBrand', $brandData);

        if (isBlankOrNull($brandId)) {
            // 更新（シンプルクラス）
            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $result->setBrandCode($result->getBrandId());
            $result2 = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstBrand', $result);
        }

        //-----------------------------------------------
        // キャッシュファイル削除
        //-----------------------------------------------
        $operation = new Api_Models_Operation();
        $operation->deleteConditionCacheFile($actionForm, $context);

        file_get_contents(DELETE_CONDITION_CACHE_FILE_API);

        $operation->deleteUserCacheFile($actionForm, $context);

        file_get_contents(DELETE_USER_CACHE_FILE_API);

        //-----------------------------------------------
        // アクションログ保存
        //-----------------------------------------------
        $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

        $operation = new Tool_Models_Operation();
        $operation->insertActionLog($logText);

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $actionForm;
    }
}
