<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/View/Interface.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';

/**
 * データベースから値を取得し、返します。
 *
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2009/01/22
 * @version SVN:$Id: DbWrite.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_DbWrite extends Mikoshiva_View_Helper_Abstract {

    public $view;

    /**
     * データベースからプライマリーキーによって値を取得し、
     * 指定されたカラムの値を返します。
     * 複数のカラムを指定する場合は配列を用います。
     * 　　この時、セパレータを指定することが可能です。
     *
     * @param  string       $arClassName アクティブレコード名
     * @param  string       $pkey        プライマリーキーの値（複合プライマリーキー未対応）
     * @param  string|array $column      取得するカラム名
     * @param  string       $separater   複数のカラムを指定した際のセパレータ
     * @return string
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2009/01/22
     * @version SVN:$Id: DbWrite.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function dbWrite($arClassName, $pkey, $column, $separater = '') {
        // メソッド開始ログを出力
        logInfo(LOG_START);

        if(isBlankOrNull($arClassName) || isBlankOrNull($column) || isBlankOrNull($pkey)) {
            // メソッド終了ログを出力
            logInfo(LOG_END);
            return '';
        }

        /* @var $arClass Zend_Db_Table_Abstract */
        $arFileName = '';
        $arFileName = preg_replace('/_/', '/', $arClassName, 1) . '.php';
        require_once $arFileName;
        $arClass = new $arClassName();
        $select  = $arClass->select();

        $select->from($arClass, $column);

        //テーブル情報からプライマリーキーを取得
        $primaryList = $arClass->info('primary');
        //プライマリーキーがなかったら空文字を返す
        if(!isset($primaryList)) return '';
        $select = $arClass->select();
        $select->where($primaryList[1] . ' = ?', $pkey);

        // 取得
        $row = $arClass->fetchRow($select);

        // 取得件数が 0 件で有れば空文字を返す
        if (count($row) === 0) return '';

        // 配列から出力用の文字列を作成
        if(is_array($column)) {
            $d = array();
            foreach($column as $key) {
                $d[] = $row[$key];
            }
            $result = implode($separater, $d);
        } else {
            $result = $row[$column];
        }
        // メソッド終了ログを出力
        logInfo(LOG_END);
        return $result;
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
