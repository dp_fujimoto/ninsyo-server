<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_holiday
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstHoliday extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mho';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'holidayId' => null,
        'holidayDate' => null,
        'holidayName' => null,
        'holidayAttribute' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 祝日ID の値を返します
	 *
     * @return int 祝日ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHolidayId() {
		return $this->_properties['holidayId'];
    }


    /**
     * 祝日ID の値をセットします
	 *
	 * @param int $holidayId 祝日ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHolidayId($holidayId) {
		$this->_properties['holidayId'] = $holidayId;
    }


    /**
     * 祝日設定日 の値を返します
	 *
     * @return date 祝日設定日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHolidayDate() {
		return $this->_properties['holidayDate'];
    }


    /**
     * 祝日設定日 の値をセットします
	 *
	 * @param date $holidayDate 祝日設定日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHolidayDate($holidayDate) {
		$this->_properties['holidayDate'] = $holidayDate;
    }


    /**
     * 祝日名 の値を返します
	 *
     * @return varchar 祝日名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHolidayName() {
		return $this->_properties['holidayName'];
    }


    /**
     * 祝日名 の値をセットします
	 *
	 * @param varchar $holidayName 祝日名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHolidayName($holidayName) {
		$this->_properties['holidayName'] = $holidayName;
    }


    /**
     * 祝日属性 の値を返します
	 *
     * @return varchar 祝日属性
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHolidayAttribute() {
		return $this->_properties['holidayAttribute'];
    }


    /**
     * 祝日属性 の値をセットします
	 *
	 * @param varchar $holidayAttribute 祝日属性
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHolidayAttribute($holidayAttribute) {
		$this->_properties['holidayAttribute'] = $holidayAttribute;
    }


}

























