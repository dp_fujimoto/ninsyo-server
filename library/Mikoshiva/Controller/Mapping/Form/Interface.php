<?php
/** @package Mikoshiva_Controller */

/**
 * フォームインターフェース
 *
 * @package Mikoshiva_Controller
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/02/16
 * @version SVN:$Id: Interface.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 *
 */
interface Mikoshiva_Controller_Mapping_Form_Interface {

    /**
     * アクションフォームの初期化処理を行います。
     * 初期化処理を行う場合は、オーバーライドして実装してください。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request);

    /**
     * validation.ymlの定義に従ってバリデート処理を行います。
     * 独自バリデート処理を行う場合は、オーバーライドして実装してください。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request);

    /**
     * アクションフォームの値を取得します。
     *
     * @param string $name
     * @return object
     */
    public function get($name);

    /**
     * アクションフォームに値を保存します。
     *
     * @param string $name
     * @param mix $value
     */
    public function set($name, $value);

    /**
     * アクションフォームの内容を配列で返します。
     *
     * @return array $properties
     */
    public function toArrayProperty();

    /**
     * アクションフォームのプロパティ名を配列で返します。
     *
     * @return array $properties
     */
    public function getPropertyNames();

}