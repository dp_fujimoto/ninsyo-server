<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_invoice
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxInvoice extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tin';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'invoiceId' => null,
        'customerId' => null,
        'packagingDirectionId' => null,
        'packagingDirectionNumber' => null,
        'companyName' => null,
        'lastName' => null,
        'firstName' => null,
        'lastNameKana' => null,
        'firstNameKana' => null,
        'zip' => null,
        'prefecture' => null,
        'address1' => null,
        'address2' => null,
        'tel' => null,
        'invoiceType' => null,
        'invoicePaymentDeliveryPrice' => null,
        'shippingRequestCompanyType' => null,
        'productIdentificationCode' => null,
        'proposalDatetime' => null,
        'dataSendDate' => null,
        'suffix' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tin_invoice_id の値を返します
	 *
     * @return int tin_invoice_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInvoiceId() {
		return $this->_properties['invoiceId'];
    }


    /**
     * tin_invoice_id の値をセットします
	 *
	 * @param int $invoiceId tin_invoice_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInvoiceId($invoiceId) {
		$this->_properties['invoiceId'] = $invoiceId;
    }


    /**
     * tin_customer_id の値を返します
	 *
     * @return int tin_customer_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * tin_customer_id の値をセットします
	 *
	 * @param int $customerId tin_customer_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * tin_packaging_direction_id の値を返します
	 *
     * @return int tin_packaging_direction_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionId() {
		return $this->_properties['packagingDirectionId'];
    }


    /**
     * tin_packaging_direction_id の値をセットします
	 *
	 * @param int $packagingDirectionId tin_packaging_direction_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionId($packagingDirectionId) {
		$this->_properties['packagingDirectionId'] = $packagingDirectionId;
    }


    /**
     * tin_packaging_direction_number の値を返します
	 *
     * @return int tin_packaging_direction_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionNumber() {
		return $this->_properties['packagingDirectionNumber'];
    }


    /**
     * tin_packaging_direction_number の値をセットします
	 *
	 * @param int $packagingDirectionNumber tin_packaging_direction_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionNumber($packagingDirectionNumber) {
		$this->_properties['packagingDirectionNumber'] = $packagingDirectionNumber;
    }


    /**
     * tin_company_name の値を返します
	 *
     * @return varchar tin_company_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompanyName() {
		return $this->_properties['companyName'];
    }


    /**
     * tin_company_name の値をセットします
	 *
	 * @param varchar $companyName tin_company_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompanyName($companyName) {
		$this->_properties['companyName'] = $companyName;
    }


    /**
     * tin_last_name の値を返します
	 *
     * @return varchar tin_last_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastName() {
		return $this->_properties['lastName'];
    }


    /**
     * tin_last_name の値をセットします
	 *
	 * @param varchar $lastName tin_last_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastName($lastName) {
		$this->_properties['lastName'] = $lastName;
    }


    /**
     * tin_first_name の値を返します
	 *
     * @return varchar tin_first_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstName() {
		return $this->_properties['firstName'];
    }


    /**
     * tin_first_name の値をセットします
	 *
	 * @param varchar $firstName tin_first_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstName($firstName) {
		$this->_properties['firstName'] = $firstName;
    }


    /**
     * tin_last_name_kana の値を返します
	 *
     * @return varchar tin_last_name_kana
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLastNameKana() {
		return $this->_properties['lastNameKana'];
    }


    /**
     * tin_last_name_kana の値をセットします
	 *
	 * @param varchar $lastNameKana tin_last_name_kana
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLastNameKana($lastNameKana) {
		$this->_properties['lastNameKana'] = $lastNameKana;
    }


    /**
     * tin_first_name_kana の値を返します
	 *
     * @return varchar tin_first_name_kana
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstNameKana() {
		return $this->_properties['firstNameKana'];
    }


    /**
     * tin_first_name_kana の値をセットします
	 *
	 * @param varchar $firstNameKana tin_first_name_kana
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstNameKana($firstNameKana) {
		$this->_properties['firstNameKana'] = $firstNameKana;
    }


    /**
     * tin_zip の値を返します
	 *
     * @return char tin_zip
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZip() {
		return $this->_properties['zip'];
    }


    /**
     * tin_zip の値をセットします
	 *
	 * @param char $zip tin_zip
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZip($zip) {
		$this->_properties['zip'] = $zip;
    }


    /**
     * tin_prefecture の値を返します
	 *
     * @return varchar tin_prefecture
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefecture() {
		return $this->_properties['prefecture'];
    }


    /**
     * tin_prefecture の値をセットします
	 *
	 * @param varchar $prefecture tin_prefecture
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefecture($prefecture) {
		$this->_properties['prefecture'] = $prefecture;
    }


    /**
     * tin_address1 の値を返します
	 *
     * @return varchar tin_address1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * tin_address1 の値をセットします
	 *
	 * @param varchar $address1 tin_address1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * tin_address2 の値を返します
	 *
     * @return varchar tin_address2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * tin_address2 の値をセットします
	 *
	 * @param varchar $address2 tin_address2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


    /**
     * tin_tel の値を返します
	 *
     * @return varchar tin_tel
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTel() {
		return $this->_properties['tel'];
    }


    /**
     * tin_tel の値をセットします
	 *
	 * @param varchar $tel tin_tel
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTel($tel) {
		$this->_properties['tel'] = $tel;
    }


    /**
     * tin_invoice_type の値を返します
	 *
     * @return varchar tin_invoice_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInvoiceType() {
		return $this->_properties['invoiceType'];
    }


    /**
     * tin_invoice_type の値をセットします
	 *
	 * @param varchar $invoiceType tin_invoice_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInvoiceType($invoiceType) {
		$this->_properties['invoiceType'] = $invoiceType;
    }


    /**
     * tin_invoice_payment_delivery_price の値を返します
	 *
     * @return int tin_invoice_payment_delivery_price
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getInvoicePaymentDeliveryPrice() {
		return $this->_properties['invoicePaymentDeliveryPrice'];
    }


    /**
     * tin_invoice_payment_delivery_price の値をセットします
	 *
	 * @param int $invoicePaymentDeliveryPrice tin_invoice_payment_delivery_price
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setInvoicePaymentDeliveryPrice($invoicePaymentDeliveryPrice) {
		$this->_properties['invoicePaymentDeliveryPrice'] = $invoicePaymentDeliveryPrice;
    }


    /**
     * tin_shipping_request_company_type の値を返します
	 *
     * @return varchar tin_shipping_request_company_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingRequestCompanyType() {
		return $this->_properties['shippingRequestCompanyType'];
    }


    /**
     * tin_shipping_request_company_type の値をセットします
	 *
	 * @param varchar $shippingRequestCompanyType tin_shipping_request_company_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingRequestCompanyType($shippingRequestCompanyType) {
		$this->_properties['shippingRequestCompanyType'] = $shippingRequestCompanyType;
    }


    /**
     * tin_product_identification_code の値を返します
	 *
     * @return varchar tin_product_identification_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductIdentificationCode() {
		return $this->_properties['productIdentificationCode'];
    }


    /**
     * tin_product_identification_code の値をセットします
	 *
	 * @param varchar $productIdentificationCode tin_product_identification_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductIdentificationCode($productIdentificationCode) {
		$this->_properties['productIdentificationCode'] = $productIdentificationCode;
    }


    /**
     * tin_proposal_datetime の値を返します
	 *
     * @return datetime tin_proposal_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProposalDatetime() {
		return $this->_properties['proposalDatetime'];
    }


    /**
     * tin_proposal_datetime の値をセットします
	 *
	 * @param datetime $proposalDatetime tin_proposal_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProposalDatetime($proposalDatetime) {
		$this->_properties['proposalDatetime'] = $proposalDatetime;
    }


    /**
     * tin_data_send_date の値を返します
	 *
     * @return date tin_data_send_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDataSendDate() {
		return $this->_properties['dataSendDate'];
    }


    /**
     * tin_data_send_date の値をセットします
	 *
	 * @param date $dataSendDate tin_data_send_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDataSendDate($dataSendDate) {
		$this->_properties['dataSendDate'] = $dataSendDate;
    }


    /**
     * tin_suffix の値を返します
	 *
     * @return int tin_suffix
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSuffix() {
		return $this->_properties['suffix'];
    }


    /**
     * tin_suffix の値をセットします
	 *
	 * @param int $suffix tin_suffix
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSuffix($suffix) {
		$this->_properties['suffix'] = $suffix;
    }


    /**
     * tin_delete_flag の値を返します
	 *
     * @return char tin_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tin_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tin_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tin_deletion_datetime の値を返します
	 *
     * @return datetime tin_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tin_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tin_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tin_registration_datetime の値を返します
	 *
     * @return datetime tin_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tin_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tin_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tin_update_datetime の値を返します
	 *
     * @return datetime tin_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tin_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tin_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tin_update_timestamp の値を返します
	 *
     * @return timestamp tin_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tin_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tin_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























