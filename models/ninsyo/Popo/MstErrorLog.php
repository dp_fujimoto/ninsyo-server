<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_error_log
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstErrorLog extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mel';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'errorLogId' => null,
        'errorLevel' => null,
        'errorMsg' => null,
        'errorSql' => null,
        'errorInfo' => null,
        'executeFile' => null,
        'executePath' => null,
        'executeAllPath' => null,
        'referrer' => null,
        'ipAddress' => null,
        'userId' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * エラーログＩＤ の値を返します
	 *
     * @return int エラーログＩＤ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorLogId() {
		return $this->_properties['errorLogId'];
    }


    /**
     * エラーログＩＤ の値をセットします
	 *
	 * @param int $errorLogId エラーログＩＤ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorLogId($errorLogId) {
		$this->_properties['errorLogId'] = $errorLogId;
    }


    /**
     * エラーレベル の値を返します
	 *
     * @return varchar エラーレベル
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorLevel() {
		return $this->_properties['errorLevel'];
    }


    /**
     * エラーレベル の値をセットします
	 *
	 * @param varchar $errorLevel エラーレベル
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorLevel($errorLevel) {
		$this->_properties['errorLevel'] = $errorLevel;
    }


    /**
     * エラーメッセージ の値を返します
	 *
     * @return varchar エラーメッセージ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorMsg() {
		return $this->_properties['errorMsg'];
    }


    /**
     * エラーメッセージ の値をセットします
	 *
	 * @param varchar $errorMsg エラーメッセージ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorMsg($errorMsg) {
		$this->_properties['errorMsg'] = $errorMsg;
    }


    /**
     * エラーＳＱＬ の値を返します
	 *
     * @return text エラーＳＱＬ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorSql() {
		return $this->_properties['errorSql'];
    }


    /**
     * エラーＳＱＬ の値をセットします
	 *
	 * @param text $errorSql エラーＳＱＬ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorSql($errorSql) {
		$this->_properties['errorSql'] = $errorSql;
    }


    /**
     * エラー情報 の値を返します
	 *
     * @return varchar エラー情報
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorInfo() {
		return $this->_properties['errorInfo'];
    }


    /**
     * エラー情報 の値をセットします
	 *
	 * @param varchar $errorInfo エラー情報
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorInfo($errorInfo) {
		$this->_properties['errorInfo'] = $errorInfo;
    }


    /**
     * エラーファイル の値を返します
	 *
     * @return varchar エラーファイル
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecuteFile() {
		return $this->_properties['executeFile'];
    }


    /**
     * エラーファイル の値をセットします
	 *
	 * @param varchar $executeFile エラーファイル
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecuteFile($executeFile) {
		$this->_properties['executeFile'] = $executeFile;
    }


    /**
     * エラーファイル（フルパス） の値を返します
	 *
     * @return varchar エラーファイル（フルパス）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecutePath() {
		return $this->_properties['executePath'];
    }


    /**
     * エラーファイル（フルパス） の値をセットします
	 *
	 * @param varchar $executePath エラーファイル（フルパス）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecutePath($executePath) {
		$this->_properties['executePath'] = $executePath;
    }


    /**
     * エラーファイル（デバッグトレース） の値を返します
	 *
     * @return text エラーファイル（デバッグトレース）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getExecuteAllPath() {
		return $this->_properties['executeAllPath'];
    }


    /**
     * エラーファイル（デバッグトレース） の値をセットします
	 *
	 * @param text $executeAllPath エラーファイル（デバッグトレース）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setExecuteAllPath($executeAllPath) {
		$this->_properties['executeAllPath'] = $executeAllPath;
    }


    /**
     * リファラー の値を返します
	 *
     * @return varchar リファラー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getReferrer() {
		return $this->_properties['referrer'];
    }


    /**
     * リファラー の値をセットします
	 *
	 * @param varchar $referrer リファラー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setReferrer($referrer) {
		$this->_properties['referrer'] = $referrer;
    }


    /**
     * ＩＰアドレス の値を返します
	 *
     * @return varchar ＩＰアドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIpAddress() {
		return $this->_properties['ipAddress'];
    }


    /**
     * ＩＰアドレス の値をセットします
	 *
	 * @param varchar $ipAddress ＩＰアドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIpAddress($ipAddress) {
		$this->_properties['ipAddress'] = $ipAddress;
    }


    /**
     * ユーザーID（オペレーターID） の値を返します
	 *
     * @return varchar ユーザーID（オペレーターID）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserId() {
		return $this->_properties['userId'];
    }


    /**
     * ユーザーID（オペレーターID） の値をセットします
	 *
	 * @param varchar $userId ユーザーID（オペレーターID）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserId($userId) {
		$this->_properties['userId'] = $userId;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return timestamp 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























