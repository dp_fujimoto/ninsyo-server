<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_product_offered_detail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstProductOfferedDetail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mpd';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'productOfferedDetailId' => null,
        'productOfferedId' => null,
        'number' => null,
        'offeredDate' => null,
        'offeredPassageMonth' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mpd_product_offered_detail_id の値を返します
	 *
     * @return int mpd_product_offered_detail_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedDetailId() {
		return $this->_properties['productOfferedDetailId'];
    }


    /**
     * mpd_product_offered_detail_id の値をセットします
	 *
	 * @param int $productOfferedDetailId mpd_product_offered_detail_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedDetailId($productOfferedDetailId) {
		$this->_properties['productOfferedDetailId'] = $productOfferedDetailId;
    }


    /**
     * mpd_product_offered_id の値を返します
	 *
     * @return int mpd_product_offered_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductOfferedId() {
		return $this->_properties['productOfferedId'];
    }


    /**
     * mpd_product_offered_id の値をセットします
	 *
	 * @param int $productOfferedId mpd_product_offered_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductOfferedId($productOfferedId) {
		$this->_properties['productOfferedId'] = $productOfferedId;
    }


    /**
     * mpd_number の値を返します
	 *
     * @return int mpd_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getNumber() {
		return $this->_properties['number'];
    }


    /**
     * mpd_number の値をセットします
	 *
	 * @param int $number mpd_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setNumber($number) {
		$this->_properties['number'] = $number;
    }


    /**
     * mpd_offered_date の値を返します
	 *
     * @return date mpd_offered_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOfferedDate() {
		return $this->_properties['offeredDate'];
    }


    /**
     * mpd_offered_date の値をセットします
	 *
	 * @param date $offeredDate mpd_offered_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOfferedDate($offeredDate) {
		$this->_properties['offeredDate'] = $offeredDate;
    }


    /**
     * mpd_offered_passage_month の値を返します
	 *
     * @return int mpd_offered_passage_month
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOfferedPassageMonth() {
		return $this->_properties['offeredPassageMonth'];
    }


    /**
     * mpd_offered_passage_month の値をセットします
	 *
	 * @param int $offeredPassageMonth mpd_offered_passage_month
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOfferedPassageMonth($offeredPassageMonth) {
		$this->_properties['offeredPassageMonth'] = $offeredPassageMonth;
    }


    /**
     * mpd_delete_flag の値を返します
	 *
     * @return char mpd_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * mpd_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag mpd_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * mpd_deletion_datetime の値を返します
	 *
     * @return datetime mpd_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * mpd_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime mpd_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * mpd_update_datetime の値を返します
	 *
     * @return datetime mpd_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * mpd_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime mpd_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * mpd_registration_datetime の値を返します
	 *
     * @return datetime mpd_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * mpd_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime mpd_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * mpd_update_timestamp の値を返します
	 *
     * @return timestamp mpd_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * mpd_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp mpd_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























