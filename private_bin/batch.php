<?php




//-----------------------------------------------
//
// ■０１．ini_set
//
//-----------------------------------------------
error_reporting(E_ALL);
//error_reporting(E_ALL ^ E_NOTICE);
ini_set('magic_quotes_gpc', 'off'); // 自動でエスケープされるのを解除します
ini_set('output_buffering', 'off');
ini_set('default_charset',  '');    // 自動で右記が実行されますheader('Content-type: text/html; charset=EUC-JP');
// mb 系の設定
ini_set('mbstring.language',             'Japanese');
ini_set('mbstring.encoding_translation', 'off');
ini_set('mbstring.http_input',           'pass');  // mbstring.encoding_translationが On の時にここで指定した文字コードで リクエスト値の自動変換を行います
ini_set('mbstring.http_output',          'pass');  // 右記メソッドを使用した時の出力エンコード ob_start("mb_output_handler")
ini_set('mbstring.internal_encoding',    'utf-8'); // mb_* 系メソッドのデフォルトエンコード
ini_set('mbstring.substitute_character', '');      // 勝手に吐き出すキャラセット

header('Content-Type: text/html; charset:utf-8');
// タイムゾーンの設定
ini_set('date.timezone', 'Asia/Tokyo');



//-----------------------------------------------
//
// ■０２．設定ファイルの読み込み
//
//-----------------------------------------------
require_once dirname(__FILE__) . '/../configs/server-mode-config.php';
require_once dirname(__FILE__) . '/../configs/' . _SERVER_MODE_ID_ . '/define.php';
require_once dirname(__FILE__) . '/../configs/' . _SERVER_MODE_ID_ . '/test-define.php';
ini_set('include_path', MODELS_DIR . PATH_SEPARATOR
                      . LIBRARY_PATH . PATH_SEPARATOR
                      . LIBRARY_PATH . '/Smarty/libs' . PATH_SEPARATOR
                      . LIBRARY_PATH . '/PEAR' . PATH_SEPARATOR
                      . APPLICATION_PATH           . PATH_SEPARATOR
                      );


require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
require_once 'Mikoshiva/Log.php';
require_once 'Mikoshiva/Log/Formatter/Simple.php';
require_once 'Mikoshiva/Controller/Action/Admin/Abstract.php';
require_once 'Mikoshiva/Filter/Input/Request.php';
require_once 'Mikoshiva/Utility/Convert.php';
require_once LIBRARY_PATH . '/functions.php';
require_once 'spyc.php';

//-----------------------------------------------------------------------------
// ■バッチ用
//-----------------------------------------------------------------------------
require_once 'Zend/Controller/Front.php';
require_once 'Mikoshiva/Controller/Request/Simple.php';
require_once 'Zend/Controller/Request/Abstract.php';
require_once 'Zend/Controller/Response/Cli.php';
require_once 'Zend/Console/Getopt.php';
require_once 'Mikoshiva/Controller/Router/Cli.php';



//-----------------------------------------------------------------------------
// ■引数の取得
// php batch.php -m moduleName -c controllerName -a actionName -p "arg1=xxx,arg2=yyy,arg3=zzz" を解釈する
// 最終的に以下に同じ
// $options->zfm = moduleName
// $options->zfc = controllerName
// $options->zfa = actionName
// $params['arg1'] = 'xxx'
// $params['arg2'] = 'yyy'
// $params['arg3'] = 'zzz';
//-----------------------------------------------------------------------------
$options = new Zend_Console_Getopt(array(
    'zfm|m=s' => 'module',
    'zfc|c=s' => 'controller',
    'zfa|a=s' => 'action',
    'zfp|p=s' => 'parameters',
));
$options->parse();


// 引数のうち、p=aaa
$params = array();
if(isset($options->zfp)) {
    //$zfps = Mikoshiva_Utility_Convert::explodeEx(',', $options->zfp);
    if (!isBlankOrNull($options->zfp)) {
        $zfps = explode(',', $options->zfp);
        foreach( $zfps as $z ) {
            //list($name, $value) = Mikoshiva_Utility_Convert::explodeEx(':', $z);
            $tempArr = explode('=', $z);
            if (count($tempArr) === 2) {
                $params[$tempArr[0]] = $tempArr[1];
            } else if (count($tempArr) === 1) {
                $params[$tempArr[0]] = null;
            }
        }
    }

    /*
    echo 'Module: ' . $options->zfm . "\n";
    echo 'Controller: ' . $options->zfc . "\n";
    echo 'Action: ' . $options->zfa . "\n";
    echo "Parameters: \n";
    var_dump($params);
    */

}
if(isset($options->zfa) && isset($options->zfc) && isset($options->zfm)){
//        $request = new Zend_Controller_Request_Simple(
    $request = new Mikoshiva_Controller_Request_Simple(
        $options->zfa,
        $options->zfc,
        $options->zfm,
        $params
    );
    $request->setParam('module',     $options->zfm);
    $request->setParam('controller', $options->zfc);
    $request->setParam('action',     $options->zfa);

    //-----------------------------------------------------------------------------
    // ■ロガー
    //-----------------------------------------------------------------------------
    $logger      = new Mikoshiva_Log();
    $sqlLogger   = new Mikoshiva_Log();
    $develLogger = new Mikoshiva_Log();

    //-------------------------- フォーマットを定義
    $format      = new Mikoshiva_Log_Formatter_Simple(date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . " [%priorityName%]: %message%" . PHP_EOL);
    $formatDevel = new Mikoshiva_Log_Formatter_Simple(date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . " [DEVEL]: %message%" . PHP_EOL);

    //-------------------------- NULL


    $nullWriter = new Zend_Log_Writer_Null();
    $logger->addWriter($nullWriter);    // 通常ログにセット
    $sqlLogger->addWriter($nullWriter); // SQL ログにセット
    $develLogger->addWriter($nullWriter); // 開発 ログにセット


    // ロガーセット
    Zend_Registry::set('request',   $request);
    Zend_Registry::set('logger',    $logger);
    Zend_Registry::set('sqlLogger', $sqlLogger);
    Zend_Registry::set('develLogger', $develLogger);
     //------------------------ フィルログ
    if (LOG_LEVEL_FILE !== 99) {
        $writer = new Zend_Log_Writer_Stream(LOGS_DIR_BATCH . '/debug/' . $options->zfc . '_' . $options->zfa . '_' . LOG_LEVEL_FILE_FILE_NAME);
        $writer->setFormatter($format);
        $writer->addFilter(LOG_LEVEL_FILE);
        $logger->addWriter($writer); // 通常ログにセット
    }


    //------------------------ SQLログ
    if (LOG_LEVEL_SQL !== 99) {
        $sqlWriter = new Zend_Log_Writer_Stream(LOGS_DIR_BATCH . '/sql/' . $options->zfc . '_' . $options->zfa . '_' . LOG_LEVEL_SQL_FILENAME);
        $sqlWriter->setFormatter($format);
        $sqlWriter->addFilter(LOG_LEVEL_SQL);
        $sqlLogger->addWriter($sqlWriter);    // SQL ログにセット
    }


    //------------------------ 開発ログ
    if (LOG_LEVEL_DEVEL !== 99) {
        $develWriter = new Zend_Log_Writer_Stream(LOGS_DIR_BATCH . '/devel/' . $options->zfc . '_' . $options->zfa . '_' . LOG_LEVEL_DEVEL_FILENAME);
        $develWriter->setFormatter($formatDevel);
        $develWriter->addFilter(LOG_LEVEL_DEVEL);
        $develLogger->addWriter($develWriter);    // SQL ログにセット
    }

    //------------------------ エラーメール
    //if (LOG_LEVEL_MAIL !== 99) {
    //    include 'Mikoshiva/Mail.php';
    //    $mail = new Mikoshiva_Mail();
    //    $mail->setFrom('admin@mikoshiva.com', 'Mikoshivaシステムメール');
    //    $mail->setSubject('Mikoshivaでエラーが起きました');
    //    $mail->addTo('kaihatsuteam@googlegroups.com');
    //
    //    $mailWiter = new Zend_Log_Writer_Mail($mail);
    //    $mailWiter->addFilter(LOG_LEVEL_MAIL); // メールログ
    //    $mailWiter->setFormatter($format);
    //    $logger->addWriter($mailWiter);        // 通常ログにセット
    //}
    //------------------------ firebug
    if (LOG_LEVEL_FIREBUG !== 99) {
        $firebugWriter = new Zend_Log_Writer_Firebug();
        $firebugWriter->setFormatter($format);
        $firebugWriter->addFilter(LOG_LEVEL_FIREBUG); // firePHP ログ
        $logger->addWriter($firebugWriter);           // 通常ログにセット
        $sqlLogger->addWriter($firebugWriter);        // SQL ログにセット
    }

    //-----------------------------------------------------------------------------
    // ■キャッシュとZend_Registryの設定
    //-----------------------------------------------------------------------------
    $frontendOptions = array(
       'lifetime' => 7200, // キャッシュの有効期限を 2 時間とします
       'automatic_serialization' => true
    );

    $backendOptions = array(
        'cache_dir' => CACHES_DIR . '/private_bin' // キャッシュファイルを書き込むディレクトリ
    );

    $cache = Zend_Cache::factory('Core',
                                 'File',
                                 $frontendOptions,
                                 $backendOptions);
    Zend_Registry::set('cache', $cache);

    Zend_Registry::set('COLUMN_MAP_LIST',                $COLUMN_MAP_LIST);
    Zend_Registry::set('SHORT_CODE_MAP_LIST',            $SHORT_CODE_MAP_LIST);
    Zend_Registry::set('GMO_CONFIG',                     $GMO_CONFIG);
    Zend_Registry::set('DEBIT_CARD_LIST',                $DEBIT_CARD_LIST);
    Zend_Registry::set('ONE_TIME_DEBIT_CARD_LIST',       $ONE_TIME_DEBIT_CARD_LIST);
    Zend_Registry::set('ACD_MAP_LIST',                   $ACD_MAP_LIST);
    Zend_Registry::set('GMO_TEST_CARD_LIST',             $GMO_TEST_CARD_LIST);
    Zend_Registry::set('GMO_TEST_CARD_ALLOW_LIST',       $GMO_TEST_CARD_ALLOW_LIST);
    Zend_Registry::set('COLUMN_MAPPING_TABLE_NAME_LIST', $COLUMN_MAPPING_TABLE_NAME_LIST);
    Zend_Registry::set('COLUMN_MAPPING_POPO_NAME_LIST',  $COLUMN_MAPPING_POPO_NAME_LIST);
    Zend_Registry::set('API_CONFIG',                     $API_CONFIG);
    Zend_Registry::set('MAIL_CONFIG',                    $MAIL_CONFIG);
    Zend_Registry::set('FTP_CONFIG',                     $FTP_CONFIG);
    Zend_Registry::set('LOG_TEMPORARY',                  $LOG_TEMPORARY);
    Zend_Registry::set('NINSYO_APP_ID_LIST',             $NINSYO_APP_ID_LIST);
    Zend_Registry::set('NINSYO_SERVER_NAME_LIST',        $NINSYO_SERVER_NAME_LIST);

    $locale = new Zend_Locale('ja');
    $locale->setCache($cache);
    Zend_Registry::set('Zend_Locale', $locale);

    // ロケールキャッシュを無効（？）にします 2012-02-06
    // この設定をすると、
    // -- ロケールのキャッシュファイルが無ければ作成、有れば常に更新します
    // 設定をしなければ、
    // -- ロケールのキャッシュファイルが無ければ作成、有ればキャッシュ有効期限切れ後に削除し、新たに作成を行います
    Zend_Locale::disableCache(true);



    //-----------------------------------------------
    //
    // ◆DB の設定
    //   ここではまだ接続されない
    //   クエリを流すか getConnection() をして初めて接続されます
    //
    //-----------------------------------------------
    // データベースハンドルの取得
    /* @var $_db Zend_Db_Adapter_Abstract */
    // $db = Zend_Db_Table_Abstract::getDefaultAdapter();
    $db = Zend_Db::factory('Pdo_Mysql', Array(
        'host'             => DB_PAYMENT_HOST,
        'username'         => DB_PAYMENT_USER_NAME,
        'password'         => DB_PAYMENT_PASSWORD,
        'dbname'           => DB_PAYMENT_DB_NAME,
        'adapterNamespace' => 'Mikoshiva_Db_Adapter',
    ));
    // $db->query('SET NAMES utf8');
    Zend_Db_Table_Abstract::setDefaultAdapter($db);


    //-----------------------------------------------------------------------------
    // ■ルーティング設定
    //-----------------------------------------------------------------------------
    $m = new Zend_Controller_Plugin_ErrorHandler(
                    array(
                        'module'     => 'error',
                        'controller' => 'batch',
                        'action'     => 'error'
                    )
                );
                $m->setErrorHandlerModule('error');
                $m->setErrorHandlerController('batch');
                $m->setErrorHandlerAction('error');

    if (isset($request)) {
        // $frontController = Zend_Controller_Front::getInstance();
        $frontController = Zend_Controller_Front::getInstance()->registerPlugin($m);
        $frontController->setRequest($request);
        $frontController->setRouter(new Mikoshiva_Controller_Router_Cli());
        $frontController->setResponse(new Zend_Controller_Response_Cli());
        //$frontController->throwExceptions(true);
        $frontController->addModuleDirectory(APPLICATION_PATH);
        $frontController->dispatch();
    }
}


















































