<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';

/**
 * 改行コードを改行タグに変換します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/25
 * @version SVN:$Id: LineToBr.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_LineToBr extends Mikoshiva_View_Helper_Abstract {


    /**
     * 改行コードを改行タグに変換します
     *
     * @param unknown_type $str
     * @return string 改行コードを改行タグに変換した文字列
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/25
     * @version SVN:$Id: LineToBr.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function lineToBr($str) {

        $_str = $str;
        $_str = str_replace("\r\n", '<br>', $_str);
        $_str = str_replace("\r", '<br>', $_str);
        $_str = str_replace("\n", '<br>', $_str);


        return $_str;
    }
}
