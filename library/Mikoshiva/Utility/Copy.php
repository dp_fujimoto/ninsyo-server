<?php
/**
 * @package Mikoshiva_Utility
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Convert.php';
require_once 'Mikoshiva/Utility/Exception.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';


/**
 * COPYユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/12
 * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Copy {

// --
// Mikoshiva_Utility_Db.saveDirectionStatusHistory へ移しました。
// kawakami
// --

//    /**
//     * 与えられた指示ステータスIDのデータを、指示ステータス履歴テーブルにInsertします。
//     *
//     * @param  Int $directionStatusId
//     *
//     * @author M.Ikuma <ikumamasayuki@gmail.com>
//     * @since 2010/07/05
//     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
//     */
//    public static function copyToDirectionStatus($directionStatusId) {
//        include_once 'Mikoshiva/Db/Simple/Effective/DeleteFlag.php';
//        include_once 'Popo/TrxDirectionStatusHistory.php';
//        include_once 'Mikoshiva/Db/Ar/TrxDirectionStatusHistory.php';
//
//        $simple              = new Mikoshiva_Db_Simple_Effective_DeleteFlag();
//        $popoDirectionStatus = $simple->simpleOneSelectById('Mikoshiva_Db_Ar_TrxDirectionStatus', $directionStatusId);
//
//        $popo = new Popo_TrxDirectionStatusHistory();
//        foreach($popoDirectionStatus->getProperties() as $k => $v) {
//            $setter = '';
//            $setter = 'setTds' . ucfirst($k);
//            $popo->$setter($v);
//        }
//
//        $ar     = new Mikoshiva_Db_Ar_TrxDirectionStatusHistory();
//        $select = $ar->select();
//        $select->from('trx_direction_status_history', 'tdh_direction_status_history_number');
//        $select->where('tdh_tds_direction_status_id = ?', $directionStatusId);
//        $select->where('tdh_delete_flag = ?', '0');
//        $select->order('tdh_direction_status_history_number DESC');
//        $select->limit(1);
//        $data   = $ar->fetchAll($select);
//
//        if($data->count() === 1) {
//            $directionStatusHistoryNumber = (int)$data[0]['tdh_direction_status_history_number'] + 1;
//        } else {
//            $directionStatusHistoryNumber = 1;
//        }
//        $popo->setDirectionStatusHistoryNumber($directionStatusHistoryNumber);
//
//        $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_TrxDirectionStatusHistory', $popo);
//    }


    /**
     * アクションフォーム、Popo、配列を判別し、プロパティの値を配列にして返します。
     *
     * @param array|Popo_Abstract|Mikoshiva_Controller_Mapping_Form_Interface $data
     * @return array $result
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/25
     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function copyToArray($data) {
        if(is_array($data)) {
            return $data;
        }

        switch ($data) {
            case ($data instanceof Popo_Abstract)://Popo
                $result = $data->getProperties();
                break;
            case ($data instanceof Mikoshiva_Controller_Mapping_Form_Interface)://アクションフォーム
                $result = $data->toArrayProperty();
                break;
            default:
                throw new Mikoshiva_Utility_Exception('入力された値の形式が不正です');
        }
        return $result;
    }


    /**
     * アクションフォーム、Popo、配列を判別し、プロパティの値をPopoにコピーして返します。
     *
     * @param  Popo_Abstract $popo
     * @param  array|Popo_Abstract|Mikoshiva_Controller_Mapping_Form_Interface $data
     * @param  array $convertData
     * @return array $result array()|特殊な組み換えを指定
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/25
     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function copyToPopo($popo, $data, $convertData = array()) {
        try {
            $dataArray = Mikoshiva_Utility_Copy::copyToArray($data);
        } catch (Mikoshiva_Utility_Exception $e) {
            throw new Mikoshiva_Utility_Exception($e->getMessage());
            exit;
        }

        $result = Mikoshiva_Utility_Copy::copyProperties($popo, $dataArray, $convertData);
        return $result;
    }

    /**
     * 配列の値をPOPOにコピーします。
     *
     * 配列のキー名とPOPOのプロパティ名が一致した場合のみ、
     * 配列の値をPOPOのプロパティにコピーします。
     * 一致しないものについては無視します。
     *
     * デフォルトで以下の変換を行います
     * popoのプロパティtel = 配列['tel1']-配列['tel2']-配列['tel3']
     * popoのプロパティfax = 配列['fax1']-配列['fax2']-配列['fax3']
     * popoのプロパティzip = 配列['zip1']-配列['zip2']
     *
     * @param  Popo_Abstract $popo
     * @param  array         $data
     * @param  array         $convertData
     * @return Popo_Abstract
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/12
     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function copyProperties(Popo_Abstract $popo, array $source, array $sourceConvertData = array()) {
        //logInfo(LOG_START);

        $defaultConvertData = array();
        $defaultConvertData ['zip'] = array('zip1', 'zip2');
        $defaultConvertData ['tel'] = array('tel1', 'tel2', 'tel3');
        $defaultConvertData ['fax'] = array('fax1', 'fax2', 'fax3');

        $convertData = $sourceConvertData + $defaultConvertData ;
        $data = array();
        $data = $source;
        foreach($convertData as $keyPopo => $list) {
            $work = array();
            foreach($list as $v) {
                if(array_key_exists($v, $source) && !isBlankOrNull($source[$v])) {
                    $work[] = $source[$v];
                }
            }
            if(!array_key_exists($keyPopo, $data)) {
                $data[$keyPopo] = implode('-', $work);
            }
        }


        foreach($data as $key => $value) {
            $setter = '';
            $setter = 'set' . ucfirst($key);
            if(method_exists($popo, $setter)) {
                $popo->$setter($value);
            }
        }


        //logInfo(LOG_END);

        return $popo;

    }

    /**
     * DBから取得した配列の値をPOPOにコピーします。
     *
     * MikoshivaのDBのカラム名をキーとする配列から、POPOのプロパティ名をキーとする配列を作成します
     * 作成した配列を引数として、copyProperties()を呼び出し、配列の値をPOPOにコピーします
     *
     * @param Popo_Abstract $popo
     * @param array         $data 'DBのカラム名' => 値
     * @param array         $convertData
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/12
     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function copyDbProperties(Popo_Abstract $popo, array $data, $convertData = array()) {
        //logInfo(LOG_START);

        //--------------------------------------------------------------------------------------------
        // ■処理開始
        //--------------------------------------------------------------------------------------------
        $dataToPopo = array();
        foreach($data as $key => $value) {
            $list = array();
            $list = explode('_', $key);


            //テーブル毎のプレフィックスを削除
            array_shift($list);

            $propertyName = '';
            $propertyName = Mikoshiva_Utility_Convert::columnNameToVariableName($key);

            $dataToPopo[$propertyName] = $value;
        }


        $popo = Mikoshiva_Utility_Copy::copyProperties($popo, $dataToPopo, $convertData);

        //logInfo(LOG_END);

        return $popo;

    }

    /**
     * 配列の値をActionFormにコピーします。
     *
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param array                                       $data
     * @return Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     *
     * @author kawakami <kawakami@gmail.com>
     * @since 2010/02/12
     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function copyArrayToActionFormProperties(
        Mikoshiva_Controller_Mapping_Form_Interface $actionForm, array $data) {

        //logInfo(LOG_START);


        $properties = array();
        $properties = $actionForm->toArrayProperty();

        foreach($data as $key => $value) {
            if (array_key_exists($key, $properties)) {
                $actionForm->set($key, $value);
            }
        }

        //logInfo(LOG_END);

        return $actionForm;
    }

    /**
     * Popoの値をActionFormにコピーします。
     *
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Popo_Abstract                               $popo
     * @return Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     *
     * @author kawakami <kawakami@gmail.com>
     * @since 2010/02/12
     * @version SVN:$Id: Copy.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function copyPopoToActionFormProperties(
        Mikoshiva_Controller_Mapping_Form_Interface $actionForm, Popo_Abstract $popo) {

        //logInfo(LOG_START);


        $actionProperties = array();
        $actionProperties = $actionForm->toArrayProperty();

        $popoProperties = array();
        $popoProperties = $popo->getProperties();

        foreach($popoProperties as $key => $value) {
            if (array_key_exists($key, $actionProperties)) {
                $actionForm->set($key, $value);
            }
        }

        //logInfo(LOG_END);

        return $actionForm;
    }
}











