<?php
/** @package Mikoshiva_Controller */

/**
 * get/setメソッドの実装を簡略化する抽象クラス
 *
 * @package 	Mikoshiva_Controller
 * @author      kawakami <kawakami@kaihatsu.com>
 * @since       2010/02/15
 * @version     SVN: $Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Model_Bean_Abstract {

    /**
     * 動的なメンバ変数に対応するset/getメソッドの呼び出しに対応します。
     * @param string $name
     * @param array $value
     * @return unknown_type
     */
    public function __call($name, $value) {

        // get/setの接頭語 取得
        $prefix = '';
        $prefix = substr($name,0,3);

        // アクションマッピング配列のキー名
        $key = '';

        //-----------------------------------------------
        // getCustomerId / setCustomerId が呼ばれた場合
        //-----------------------------------------------
        if($prefix === 'get' || $prefix === 'set'){

            // 接頭語のget/setをを削除
            $name = substr_replace($name,'',0,3);

            // 最初の文字 取得
            $firstWord = '';
            $firstWord = substr($name,0,1);

            // それ以降の文字列 取得
            $lastWord = '';
            $lastWord = substr($name,1);

            // キー名を作成
            $key = strtolower($firstWord).$lastWord;

        //-----------------------------------------------
        // customerId が呼ばれた場合
        //-----------------------------------------------
        } else {
            // キー名にする
            $key = $name;
        }

		// セッターゲッターの処理を行います。
        if (property_exists($this, $key)) {
        	if ($prefix === 'set') {
        		// セッター
        		$this->$key = $this->getValue($value);
        	} else if ($prefix === 'get'){
        		// ゲッター
        		return $this->$key;
        	} else {
        		 // プロパティが存在しない場合、例外を投げます。
        		throw new Mikoshiva_Configuration_Exception(
					'【Mikoshiva_Controller_Model_Bean_Abstract】プロパティ[' . $key . ']が存在しません。');
        	}
        }

    }

    /**
     * valueが存在するれば値をセットします。
     */
    private function getValue(array $value) {
        $result = null;
        if (isset($value[0])) {
		    $result = $value[0];
		}
		return $result;
    }

}