<table class="COM_table COM_tableWidthLong">
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">ID</td>
        <td class="COM_nowrap">{if !isBlankOrNull($form->getBrandId())}{$form->getBrandId()}{else}新規登録中{/if}</td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">部署</td>
        <td class="COM_nowrap">
            {dbWrite arClassName='Mikoshiva_Db_NinsyoAr_MstDivision' pkey=$form->getDivisionId() column=array('divisionName')}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            ブランド名
        </td>
        <td class="COM_nowrap">
            {$form->getBrandName()}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            メモ
        </td>
        <td class="COM_nowrap">
            <pre>{$form->getMemo()}</pre>
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            状態
        </td>
        <td class="COM_nowrap">
            {if $form->getDeleteFlag() === '1'}削除{else}通常{/if}
        </td>
    </tr>
</table>

<br>
【用途：ライブラリー用】

<table class="COM_table COM_tableWidthLong">
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            ロゴURL
        </td>
        <td class="COM_nowrap">
            {$form->getLogoUrl()}
            {if !isBlankOrNull($form->getLogoUrl())}
                {if $form->getLogoNoneFlag() !== '1'}
                    {if $form->getLogoUpdateFlag() === '1'}<br><p class="COM_bold COM_fontRed">URLを変更、またはURLにある画像データを変更した</p>{/if}
                {/if}
                <br><img src="{$form->getLogoUrl()}" width="{$smarty.const.BLAND_LOGO_W}px" height="{$smarty.const.BLAND_LOGO_H}px">
            {/if}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            バナー縦画像
        </td>
        <td class="COM_nowrap">
            {$form->getBannerVerticalUrl()}
            {if !isBlankOrNull($form->getBannerVerticalUrl())}
                {if $form->getBannerVerticalNoneFlag() !== '1'}
                    {if $form->getBannerVerticalUpdateFlag() === '1'}<br><p class="COM_bold COM_fontRed">URLを変更、またはURLにある画像データを変更した</p>{/if}
                {/if}
                <br><img src="{$form->getBannerVerticalUrl()}" width="{$smarty.const.BLAND_BANNER_V_W}px" height="{$smarty.const.BLAND_BANNER_V_H}px">
            {/if}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            バナー横画像
        </td>
        <td class="COM_nowrap">
            {$form->getBannerSidewaysUrl()}
            {if !isBlankOrNull($form->getBannerSidewaysUrl())}
                {if $form->getBannerSidewaysNoneFlag() !== '1'}
                    {if $form->getBannerSidewaysUpdateFlag() === '1'}<br><p class="COM_bold COM_fontRed">URLを変更、またはURLにある画像データを変更した</p>{/if}
                {/if}
                <br><img src="{$form->getBannerSidewaysUrl()}" width="{$smarty.const.BLAND_BANNER_S_W}px" height="{$smarty.const.BLAND_BANNER_S_H}px">
            {/if}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            注釈
        </td>
        <td class="COM_nowrap">
            {$form->getDescription()}
        </td>
    </tr>
</table>