<table class="COM_table COM_tableWidthLong">
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">ID</td>
        <td class="COM_nowrap">{if !isBlankOrNull($form->getProductId())}{$form->getProductId()}{else}新規登録中{/if}</td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            ブランド名
        </td>
        <td class="COM_nowrap">
            {dbWrite arClassName='Mikoshiva_Db_NinsyoAr_MstBrand' pkey=$form->getBrandId() column=array('brandName')}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            商品名
        </td>
        <td class="COM_nowrap">
            {$form->getProductName()}
        </td>
    </tr>
    {if !isBlankOrNull($form->getProductCode())}
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            商品KEY<br>（自動発行で認証サーバー内で一意）
        </td>
        <td class="COM_nowrap">
            {$form->getProductCode()}
        </td>
    </tr>
    {/if}
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            メモ
        </td>
        <td class="COM_nowrap">
            <pre>{$form->getMemo()}</pre>
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            状態
        </td>
        <td class="COM_nowrap">
            {if $form->getDeleteFlag() === '1'}削除{else}通常{/if}
        </td>
    </tr>
</table>

<br>
<span class="COM_bold">【用途：ライブラリー用】</span>

<table class="COM_table COM_tableWidthLong">
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            商品画像URL
        </td>
        <td class="COM_nowrap">
            {$form->getImageUrl()}
            {if !isBlankOrNull($form->getImageUrl())}
                {if $form->getImageNoneFlag() !== '1'}
                    {if $form->getImageUpdateFlag() === '1'}<br><p class="COM_bold COM_fontRed">URLを変更、またはURLにある画像データを変更した</p>{/if}
                {/if}
                <br><img src="{$form->getImageUrl()}" width="{$smarty.const.PRODUCT_IMAGE_W}px" height="{$smarty.const.PRODUCT_IMAGE_H}px">
            {/if}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            注釈
        </td>
        <td class="COM_nowrap">
            {$form->getDescription()}
        </td>
    </tr>
</table>