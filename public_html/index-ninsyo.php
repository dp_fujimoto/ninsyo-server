<?php


/********************************
 *
 *
 * システム共通設定を行います
 *
 * TOC:
 * =1:  ini_set
 * =2:  インクルード
 * =3:  SESSION
 * =4:  Smarty
 * =5:  ログインチェック
 *
 **********************************/


//-----------------------------------------------
//
// ■０１．ini_set
//
//-----------------------------------------------
error_reporting(E_ALL);
ini_set('magic_quotes_gpc', 'off'); // 自動でエスケープされるのを解除します
ini_set('output_buffering', 'off');
ini_set('default_charset',  '');    // 自動で右記が実行されますheader('Content-type: text/html; charset=EUC-JP');
// mb 系の設定
ini_set('mbstring.language',             'Japanese');
ini_set('mbstring.encoding_translation', 'off');
ini_set('mbstring.http_input',           'pass');  // mbstring.encoding_translationが On の時にここで指定した文字コードで リクエスト値の自動変換を行います
ini_set('mbstring.http_output',          'pass');  // 右記メソッドを使用した時の出力エンコード ob_start("mb_output_handler")
ini_set('mbstring.internal_encoding',    'utf-8'); // mb_* 系メソッドのデフォルトエンコード
ini_set('mbstring.substitute_character', '');      // 勝手に吐き出すキャラセット
// セッション
ini_set('session.use_trans_sid'            ,'0'); // クッキー無効ブラウザによるクエリにPHPSESSIDを無効にする
ini_set('session.use_only_cookies'         ,'1'); // クエリによるセッションID生成を無効にする

// fgets()  file() などは [LF] までを一行と認識している。
// このため [CRLF] [CR] が行末である場合、期待する動作をしない。
// この値を１にすることで PHP が改行コードを自動解釈します
// ※ただし、ファイルの中に改行コードが混在している場合は期待した動作を得られません
//   これは、一番最初に登場した改行コードを基準に PHP が以降の処理を行う為です。
// http://www.php.net/manual/ja/filesystem.configuration.php#ini.auto-detect-line-endings
// http://blog.livedoor.jp/haruchaco/archives/1278445.html
ini_set('auto_detect_line_endings',           1);

// ▼セッションのガベージコレクションの設定
// session.gc_divisorと session.gc_probabilityの組み合わせで
// すべてのセッションの初期化過程でgc（ガーベッジコネクション）プロセス も始動する確率を制御します。
// 確率は gc_probability/gc_divisor で計算されます。例えば、1/100は各リクエスト毎に1%の確率でGCプロセスが 始動します。 session.gc_divisorのデフォルトは100です。
ini_set('session.gc_probability',  1);    // （1）
ini_set('session.gc_divisor',      100); // （100）
ini_set('session.gc_maxlifetime',  3600); // （1440） データが 'ごみ' とみなされ、消去されるまでの秒数を指定します。 （ファイルが作成されて指定時間以上経っているものが対象）

//ini_set('session.cookie_lifetime', 3600);  // （0 ブラウザを閉じるまで）クッキーの有効期間
// タイムゾーンの設定
ini_set('date.timezone', 'Asia/Tokyo');

ini_set('session.cache_limiter', 'public');

// ヘッダー
header('Content-type: text/html; charset=utf-8');
header('Cache-Control:no-cache');
header('Pragma: no-cache');

//------------------------------------------------------------------
//
// ■０２．設定ファイルの読み込み
//
//------------------------------------------------------------------
require_once dirname(__FILE__) . '/../configs/server-mode-config.php';                // サーバーモードを取得（必ず一番はじめに読み込む）
require_once dirname(__FILE__) . '/../configs/' . _SERVER_MODE_ID_ . '/define.php';      // 前定数を読み込む
require_once dirname(__FILE__) . '/../configs/' . _SERVER_MODE_ID_ . '/test-define.php'; // テスト定数
require_once LIBRARY_PATH . '/functions.php';
// include_path のセット
ini_set('include_path', MODELS_DIR . PATH_SEPARATOR
                      . AR_PATH . PATH_SEPARATOR
                      . LIBRARY_PATH . PATH_SEPARATOR
                      . LIBRARY_PATH . '/Smarty/libs' . PATH_SEPARATOR
                      . LIBRARY_PATH . '/PEAR' . PATH_SEPARATOR
                      . APPLICATION_PATH           . PATH_SEPARATOR
                      );

session_save_path(SESSION_DIR);



//------------------------------------------------------------------
//
// ◆ オートローダー有効化
//
//------------------------------------------------------------------
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
//$autoloader->setFallbackAutoloader( true );
require_once 'Smarty.class.php';
require_once 'Mikoshiva/View/Smarty.php';
require_once 'Mikoshiva/Log.php';
require_once 'Mikoshiva/Log/Formatter/Simple.php';
require_once 'Mikoshiva/Controller/Action/Admin/Abstract.php';
require_once 'Mikoshiva/Filter/Input/Request.php';
require_once 'spyc.php';
// ▼先（セッションが始まる前）に読み込んでおかないと
//   ログイン時にセッションに登録したクラスが復元出来ないためこの時点で読み込みを行います
require_once 'Popo/MstStaff.php';
require_once 'Popo/MstDivision.php';



//------------------------------------------------------------------
//
// ◆view の変更
// http://www.revulo.com/ZendFramework/Component/Phtmlc.html
//
//------------------------------------------------------------------
////$view = new Revulo_View_Phtmlc();
////$view->setEncoding('UTF-8');
////$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
////$viewRenderer->setView($view);
/*
$view = new Mikoshiva_View_Smarty();
$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
#
$viewRenderer->setView($view)
             //->setViewBasePathSpec($view->getEngine()->template_dir)
             ->setViewScriptPathSpec(':module/:controller/:action.:suffix')
             ->setViewScriptPathNoControllerSpec(':action.:suffix')
             ->setViewSuffix('tpl');
             */

$view = new Mikoshiva_View_Smarty();
$view->addHelperPath( dirname(__FILE__) . '/../library/Mikoshiva/View/Helper', 'Mikoshiva_View_Helper_');
//$view->set

// それを ViewRenderer に追加します
$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
$viewRenderer->setView($view)->setViewSuffix('tpl');
//->setViewBasePathSpec(APPLICATION_PATH)
//->setViewScriptPathSpec(APPLICATION_PATH . '/:module/views/scripts/:controller/:action.:suffix')
//->setViewScriptPathSpec('/:module/views/scripts/:controller/:action.:suffix')
//->setViewScriptPathNoControllerSpec(':action.:suffix')



//------------------------------------------------------------------
//
// ◆view helper の追加
//
//------------------------------------------------------------------
//$view = new Zend_View();
//$viewHelper = new Zend_Controller_Action_Helper_ViewRenderer( $view );
Zend_Controller_Action_HelperBroker::addHelper( $viewRenderer );



//------------------------------------------------------------------
//
// ◆Zend_Layout のパス設定
//
//------------------------------------------------------------------
Zend_Layout::startMvc(
    array('layout'     => 'layout',
          'layoutPath' => APPLICATION_PATH . '/layouts',
          'ViewSuffix' => 'tpl'
    )
);



//------------------------------------------------------------------
//
// ◆キャッシュの設定
//
//------------------------------------------------------------------
$frontendOptions = array(
   'lifetime' => 7200, // キャッシュの有効期限を 2 時間とします
   'automatic_serialization' => true
);

$backendOptions = array(
    'cache_dir' => CACHES_DIR // キャッシュファイルを書き込むディレクトリ
);



//------------------------------------------------------------------
//
// ◆DB の設定
//   ここではまだ接続されない
//   クエリを流すか getConnection() をして初めて接続されます
//
//------------------------------------------------------------------
// データベースハンドルの取得
/* @var $_db Zend_Db_Adapter_Abstract */
// $db = Zend_Db_Table_Abstract::getDefaultAdapter();
$db = Zend_Db::factory('Pdo_Mysql', Array(
    'host'             => DB_PAYMENT_HOST,
    'username'         => DB_PAYMENT_USER_NAME,
    'password'         => DB_PAYMENT_PASSWORD,
    'dbname'           => DB_PAYMENT_DB_NAME,
    'adapterNamespace' => 'Mikoshiva_Db_Adapter',
));
// $db->query('SET NAMES utf8');
Zend_Db_Table_Abstract::setDefaultAdapter($db);



//------------------------------------------------------------------
//
// ◆ロガー
//
//------------------------------------------------------------------
$logger    = new Mikoshiva_Log();
$sqlLogger = new Mikoshiva_Log();
$develLogger = new Mikoshiva_Log();

//-------------------------- フォーマットを定義
$format      = new Mikoshiva_Log_Formatter_Simple(date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . " [%priorityName%]: %message%" . PHP_EOL);
$formatDevel = new Mikoshiva_Log_Formatter_Simple(date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . " [DEVEL]: %message%" . PHP_EOL);

//-------------------------- NULL
$nullWriter = new Zend_Log_Writer_Null();
$logger->addWriter($nullWriter);    // 通常ログにセット
$sqlLogger->addWriter($nullWriter); // SQL ログにセット
$develLogger->addWriter($nullWriter); // 開発 ログにセット



// ロガーセット
Zend_Registry::set('logger',    $logger);
Zend_Registry::set('sqlLogger', $sqlLogger);
Zend_Registry::set('develLogger', $develLogger);

 //------------------------ フィルログ
if (LOG_LEVEL_FILE !== 99) {
    $writer = new Zend_Log_Writer_Stream(LOGS_DIR . '/debug/' . LOG_LEVEL_FILE_FILE_NAME);
    $writer->setFormatter($format);
    $writer->addFilter(LOG_LEVEL_FILE);
    $logger->addWriter($writer); // 通常ログにセット
}


//------------------------ SQLログ
if (LOG_LEVEL_SQL !== 99) {
    $sqlWriter = new Zend_Log_Writer_Stream(LOGS_DIR . '/sql/' . LOG_LEVEL_SQL_FILENAME);
    $sqlWriter->setFormatter($format);
    $sqlWriter->addFilter(LOG_LEVEL_SQL);
    $sqlLogger->addWriter($sqlWriter);    // SQL ログにセット
}

//------------------------ 開発ログ
if (LOG_LEVEL_DEVEL !== 99) {
    $develWriter = new Zend_Log_Writer_Stream(LOGS_DIR . '/devel/' . LOG_LEVEL_DEVEL_FILENAME);
    $develWriter->setFormatter($formatDevel);
    $develWriter->addFilter(LOG_LEVEL_DEVEL);
    $develLogger->addWriter($develWriter);
}

////------------------------ エラーメール
//if (LOG_LEVEL_MAIL !== 99) {
//    require_once 'Mikoshiva/Mail.php';
//    $mail = new Mikoshiva_Mail();
//    $mail->setFrom('admin@mikoshiva.com', 'Mikoshivaシステムメール');
//    $mail->setSubject('Mikoshivaでエラーが起きました');
//    $mail->addTo('kaihatsuteam@googlegroups.com');
//
//    $mailWiter = new Zend_Log_Writer_Mail($mail);
//    $mailWiter->addFilter(LOG_LEVEL_MAIL); // メールログ
//    $mailWiter->setFormatter($format);
//    $logger->addWriter($mailWiter);        // 通常ログにセット
//}
//------------------------ firebug
if (LOG_LEVEL_FIREBUG !== 99) {
    $firebugWriter = new Zend_Log_Writer_Firebug();
    $firebugWriter->setFormatter($format);
    $firebugWriter->addFilter(LOG_LEVEL_FIREBUG); // firePHP ログ
    $logger->addWriter($firebugWriter);           // 通常ログにセット
    $sqlLogger->addWriter($firebugWriter);        // SQL ログにセット
}



//------------------------------------------------------------------
//
// ◆Zend_Registry に登録
//
//------------------------------------------------------------------
// Zend_Cache_Core を Zend_Registry に 登録
$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
Zend_Registry::set('cache', $cache);

Zend_Registry::set('COLUMN_MAP_LIST',                $COLUMN_MAP_LIST);
Zend_Registry::set('SHORT_CODE_MAP_LIST',            $SHORT_CODE_MAP_LIST);
Zend_Registry::set('GMO_CONFIG',                     $GMO_CONFIG);
Zend_Registry::set('DEBIT_CARD_LIST',                $DEBIT_CARD_LIST);
Zend_Registry::set('ONE_TIME_DEBIT_CARD_LIST',       $ONE_TIME_DEBIT_CARD_LIST);
Zend_Registry::set('ACD_MAP_LIST',                   $ACD_MAP_LIST);
Zend_Registry::set('GMO_TEST_CARD_LIST',             $GMO_TEST_CARD_LIST);
Zend_Registry::set('GMO_TEST_CARD_ALLOW_LIST',       $GMO_TEST_CARD_ALLOW_LIST);
Zend_Registry::set('COLUMN_MAPPING_TABLE_NAME_LIST', $COLUMN_MAPPING_TABLE_NAME_LIST);
Zend_Registry::set('COLUMN_MAPPING_POPO_NAME_LIST',  $COLUMN_MAPPING_POPO_NAME_LIST);
Zend_Registry::set('API_CONFIG',                     $API_CONFIG);
Zend_Registry::set('MAIL_CONFIG',                    $MAIL_CONFIG);
Zend_Registry::set('FTP_CONFIG',                     $FTP_CONFIG);
Zend_Registry::set('NINSYO_APP_ID_LIST',             $NINSYO_APP_ID_LIST);
Zend_Registry::set('NINSYO_SERVER_NAME_LIST',        $NINSYO_SERVER_NAME_LIST);


Zend_Registry::set('LOG_TEMPORARY', $LOG_TEMPORARY);

$locale = new Zend_Locale('ja');
$locale->setCache($cache);
Zend_Registry::set('Zend_Locale', $locale);

//Zend_Locale::disableCache(true); // ロケールキャッシュを無効にします


//------------------------------------------------------------------
//
// ◆ルーティング設定
//
//------------------------------------------------------------------
$m = new Zend_Controller_Plugin_ErrorHandler(
                array(
                    'module'     => 'error',
                    'controller' => 'error',
                    'action'     => 'error'
                ),
                200
            );
            $m->setErrorHandlerModule('error');
            $m->setErrorHandlerController('error');
            $m->setErrorHandlerAction('error');



//------------------------------------------------------------------
//
// ◆MIKOSHIVAモードかマイページモードのでコントローラーディレクトリを変更
//
//------------------------------------------------------------------
$controllerDirectory = array();


//------------------------------------------------------------------
//
// ◆実行
//
//------------------------------------------------------------------
Zend_Controller_Front::getInstance()
                     ->registerPlugin($m)
                     ->setModuleControllerDirectoryName()
                     ->addModuleDirectory(APPLICATION_PATH)
                     //->setControllerDirectory($controllerDirectory)
                     ->dispatch();

