<?php
function errorHandler( $errno, $errstr, $errfile, $errline ) {
    if( E_STRICT !== $errno ) {
        include_once 'Mikoshiva/Error/Exception.php';
        throw new Mikoshiva_Error_Exception($errno, $errstr, $errfile, $errline);
    }
}
set_error_handler("errorHandler");

function fileChecker($classFile) {
    //--------------------------------------------
    // クラスファイルが存在していれば return
    //--------------------------------------------
    if (file_exists($classFile)) {
        return true;
    }


    //--------------------------------------------
    // include_path からクラスファイルを検索
    //---------------------------------------------
    $includePathList = explode(PATH_SEPARATOR, get_include_path());
    foreach ($includePathList as $k => $v) {

        // 空であればコンテニュー
        if (isBlankOrNull($v)) continue;

        if (file_exists($v . '/' . $classFile)) {
            return true;
        }
    }

    // 検索出来なければエラー
    return false;
}

if (!function_exists('get_called_class')){

    function get_called_class() {

        $bt = debug_backtrace();
        $lines = file($bt[1]['file']);
        preg_match('/([a-zA-Z0-9\_]+)::'.$bt[1]['function'].'/',
                   $lines[$bt[1]['line']-1],
                   $matches);
        return $matches[1];
    }
}


/**
 * 空文字かNULLのチェックを行います
 *
 * $mm = array();
 * $mm[]             = '';            // bool(true)
 * $mm[]             = '谷口さん';    // bool(false)
 * $mm[]             = NULL;          // bool(true)
 * $mm[]             = 0;             // bool(false)
 * $mm[]             = 1;             // bool(false)
 * $mm[]             = false;         // bool(false)
 * $mm[]             = true;          // bool(false)
 * $mm[]             = array();       // bool(false)
 * $mm[]             = array(0,1,2);  // bool(false)
 * $mm[]             = '0';           // bool(false)
 * $mm[]             = '1';           // bool(false)
 * $mm['a1']         = '';            // bool(true)
 * $mm['a2']         = '谷口さん';    // bool(false)
 * $mm['a3']         = NULL;          // bool(true)
 * $mm['a4']         = 0;             // bool(false)
 * $mm['a5']         = 1;             // bool(false)
 * $mm['a6']         = false;         // bool(false)
 * $mm['a7']         = true;          // bool(false)
 * $mm['a8']         = array();       // bool(false)
 * $mm['a9']         = array(0,1,2);  // bool(false)
 * $mm['a10']        = '0';           // bool(false)
 * $mm['a11']        = '1';           // bool(false)
 *
 * // ２次元配列は許容されないため、配列を isBlankOrNull にかける状態となります
 * // isBlankOrNull( array('b1' => '') ); としているのと同じ
 * $mm['a12']['b1']   = '';           // bool(false)
 * $mm['a13']['b2']   = '谷口さん';   // bool(false)
 * $mm['a14']['b3']   = NULL;         // bool(false)
 * $mm['a15']['b4']   = 0;            // bool(false)
 * $mm['a16']['b5']   = 1;            // bool(false)
 * $mm['a17']['b6']   = false;        // bool(false)
 * $mm['a18']['b7']   = true;         // bool(false)
 * $mm['a19']['b8']   = array();      // bool(false)
 * $mm['a20']['b9']   = array(0,1,2); // bool(false)
 * $mm['a21']['b10'] = '0';           // bool(false)
 * $mm['a22']['b11'] = '1';           // bool(false)
 *
 * foreach($mm as $k => $v) {
 *     var_dump(isBlankOrNull($mm, $k));
 * }
 * $mm2 = array();
 * $mm2['aaa']['b1']   = '';           // bool(true)
 * $mm2['aaa']['b2']   = '谷口さん';   // bool(false)
 * $mm2['aaa']['b3']   = NULL;         // bool(true)
 * $mm2['aaa']['b4']   = 0;            // bool(false)
 * $mm2['aaa']['b5']   = 1;            // bool(false)
 * $mm2['aaa']['b6']   = false;        // bool(false)
 * $mm2['aaa']['b7']   = true;         // bool(false)
 * $mm2['aaa']['b8']   = array();      // bool(false)
 * $mm2['aaa']['b9']   = array(0,1,2); // bool(false)
 * $mm2['aaa']['b10'] = '0';           // bool(false)
 * $mm2['aaa']['b11'] = '1';           // bool(false)
 *
 * foreach($mm2['aaa'] as $k => $v) {
 *     var_dump(isBlankOrNull($mm2['aaa'], $k));
 * }
 *
 * @param mixed  $str チェックする文字列
 * @param string $key チェックする文字列
 * @return boolean
 */
function isBlankOrNull($str, $key = null) {


    $_str = $str;


    // 配列であれば第二引数の値を持つキーが存在するかチェック
    if (is_array($_str) && $key !== null) {
        if (! array_key_exists($key, $_str)) {
            return true;
        }
        $_str = $_str[ $key ];
    }


    if (!isset($_str)  || ($_str === '')) return true;

    return false;
}



/**
 * 指定された値のキー名が配列に存在するか、NULLであるかをチェックし、
 * 存在すればその値を返します
 * 存在しなければ第 3 引数で指定された値を返します(初期値はNULLです)
 *
 * @param array  $array チェックする配列
 * @param string $key   配列のキー名
 * @param mixed  $def   初期値
 * @return mixed
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/21
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function getArrayParam(array $array, $key, $def = null) {

    if (isset($array[$key]) && ! isBlankOrNull($array[$key])) {
        return $array[$key];
    }

    return $def;
}




/**
 * 与えられた引数が,数字・数値文字列で有る場合に true を返します
 *
 * 11          // true
 * '11'        // true
 * '-1'        // false
 * '-0'        // false
 * -1          // false
 * -11111      // false
 * 11.11       // false
 * '11.11'     // false
 * 'a'         // false
 * '0'         // true
 * 0           // true
 * 0.5         // false
 * '0.5'       // false
 * true        // false
 * false       // false
 * ''          // false
 * null        // false
 * array()     // false
 * array(1,2,3)// false
 *
 * @return boolean
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/19
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function numberCheck($str) {

    if ((is_string($str) || is_int($str) || is_float($str)) && preg_match('/^[0-9]+$/', $str) === 1) {
        return true;
    }

    return false;
}



/**
 * Zend_Debug::dump のラップクラス
 *
 * @param $var
 * @param $label
 * @param $echo
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/02/08
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function dumper($var, $label = null, $echo = true) {
    include_once 'Zend/Debug.php';
    return Zend_Debug::dump($var, $label, $echo);
}


/**
 * dumper の処理結果を sjis に変換して返します
 *
 * @param $var
 * @param $label
 * @param $echo
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/06/07
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function dumperSjis($var, $label = null, $echo = true) {


    $v = dumper($var, $label, false);
    $s = mb_convert_encoding($v, 'SJIS', 'UTF-8');

    if ($echo) {
        echo $s;
    }

    return $s;
}


/**
 * 緊急(Emergency): 緊急メッセージ
 * Mikoshiva_Logger::emerg のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logEmerg($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::emerg($message, $addMessage);
}


/**
 * 危機(Critical): 危機メッセージ
 * Mikoshiva_Logger::crit のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logCrit($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::crit($message, $addMessage);
}


/**
 * エラー(Error): エラーメッセージ
 * Mikoshiva_Logger::err のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logErr($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::err($message, $addMessage);
}


/**
 * 警告(Warning): 警告メッセージ
 * Mikoshiva_Logger::warn のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logWarn($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::warn($message, $addMessage);
}


/**
 * 注意(Notice): 注意メッセージ
 * Mikoshiva_Logger::notice のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logNotice($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::notice($message, $addMessage);
}


/**
 * 開発 (Devel): ッセージ
 * Mikoshiva_Logger::debug のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logDevel($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::devel($message, $addMessage);
}


/**
 * 情報 (Informational): 情報メッセージ
 * Mikoshiva_Logger::info のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logInfo($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::info($message, $addMessage);
}



/**
 * デバッグ (Debug): デバッグメッセージ
 * Mikoshiva_Logger::debug のラップメソッドです
 *
 * @param $message
 * @param $addMessage
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/02
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function logDebug($message, $addMessage = '') {
    include_once 'Mikoshiva/Logger.php';
    Mikoshiva_Logger::debug($message, $addMessage);
}

/**
 * Module 毎にログを生成します
 *
 *
 * @param unknown_type $message
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/11/28
 * @version SVN:$Id:
 */
function logTemporary($message, $prefixFileName = '', $directoryNameIndicate = '', $calledClassNumber = 2) {

    // ▼リクエストの取得
    include_once 'Zend/Controller/Front.php';
    $front   = Zend_Controller_Front::getInstance();
    $request = $front->getRequest();

    // ▼ログ書き込みが有効であるかチェック
    $LOG_TEMPORARY = Zend_Registry::get('LOG_TEMPORARY');
    if (array_key_exists($request->getModuleName(), $LOG_TEMPORARY) === false || $LOG_TEMPORARY[$request->getModuleName()] === false) {
        return false;
    }



    // ▼Directory パスの生成
    // $moduleName       = $request->getModuleName();
    $moduleName = (isBlankOrNull($directoryNameIndicate) === false) // ログの書込みディレクトリを確定する（保存ディレクトリが指示されていればそっちに保存するようにする）
                ? $directoryNameIndicate
                : $request->getModuleName();
    $logDirectoryPath = LOGS_DIR . '/temporary/' . $moduleName . '/'; // ログ Directory パス
    if (file_exists($logDirectoryPath) === false) mkdir($logDirectoryPath);

    // ▼ファイル名の生成
    $_prefixFileName = $prefixFileName;
    if (isBlankOrNull($_prefixFileName) === false) $_prefixFileName = $_prefixFileName . '_';
    $logFilePath = "{$logDirectoryPath}{$_prefixFileName}" . date('Y-m-d', $_SERVER['REQUEST_TIME']) . '.txt';

    // ▼ログフォーマット
    $_message = $message;
    if (is_array($_message)) {
        $_message = "\n" . Mikoshiva_Debug_VarDump::exec($_message, '$_DEBUG');
    }

    $moduleName     = $request->getModuleName();
    $controllerName = $request->getControllerName();
    $actionName     = $request->getActionName();

    $requestUri = '';
    if(method_exists($request, 'getRequestUri')) {
        $requestUri = $request->getRequestUri();
    }

    $prefix  = '';
    $prefix = "[{$moduleName}_{$controllerName}_{$actionName}] ";

    if(!isBlankOrNull($requestUri)) {
        $prefix .= '['
                  . session_id()
                  . '] ';
    }

    // lib/Mikoshiva クラスは出力しない
    $logExeClass = Mikoshiva_Utility_Class::getCalledClass($calledClassNumber);
    if (LOG_MIKOSHIVA_CLASS === false) {
        if (preg_match('#^Mikoshiva_#', $logExeClass) === 1) {
            return;
        }
    }

    $_message = ''
              . date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . ': ' // リクエスト日時
              . $prefix                                              // リクエストパス
              . $logExeClass           // ログ書き込みクラス
              . ' '
              . $_message
              . "\n"
              . '';

    // ▼ログの書き出し
    error_log($_message, 3, $logFilePath);



    return true;
}



/**
 * DBのカラム名を、フォームのname・変数名に変換します。
 *
 * 出力される文字列は、以下の形式です。
 * 　　DBのカラム名からプレフィックスの3文字とアンダースコアを除外し、
 * 　　先頭の単語以外をキャピタライズした文字列
 *
 * @param string $columnName
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/18
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function convertToVariableName($columnName) {
    Mikoshiva_Logger::info(LOG_START);

    $list = explode('_', $columnName);
    //テーブル毎のプレフィックスを削除
    array_shift($list);

    //単語の先頭を大文字にして、再度連結
    foreach($list as $k => $v) {
        if ($k === 0) continue;
        $list[$k] = ucfirst($v);
    }

    $name = '';
    $name = implode('', $list);

    Mikoshiva_Logger::info(LOG_END);
    return $name;
}


/**
 * 配列が指定された件数で無ければ例外を投げます
 *
 * @param $array
 * @param $count
 * @return boolean
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/05/13
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function countCheck(array $array, $count) {

    if(count($array) !== $count) {
        include_once 'Mikoshiva/Exception.php';
        $message = "【"  . __FUNCTION__ . " （" . __LINE__ . "）】指定された配列は {$count} 件では有りませんでした。";
        logDebug($message);
        throw new Mikoshiva_Exception($message);
    }

    return true;
}


/**
 * 変数の値を再帰的に展開します
 *
 * @param $input
 * @param $name
 * @param $i
 * @param $k
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/08/05
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function dumper2($input, $name = "__DUMPER2_", $i = "0", $k = array("Error")) {

    switch (true) {

        case is_array($input):

            foreach ($input as $i => $value){
                $temp   = $k;
                $temp[] = $i;
                dumper2($value, $name, $i, $temp);
            }
            break;

        case is_object($input):

            $className = get_class($input);
            $_input = array();
            $_input[$className] = array();
            $_input[$className] = (array)$input;
            foreach ($_input as $i => $value) {
                $temp   = $k;
                $temp[] = $i;
                dumper2($value, $name, $i, $temp);
            }
            break;

        default:
            echo "$".$name;//[$k]
            $m = '';
            foreach ($k as $i => $value){

                if($value !=="Error") {
                    $value = str_replace("\x00", '', $value);
                    $value = str_replace("\x2A", '', $value);
                    if (preg_match('#^' . $m . '.+#', $value) === 1) {
                        $value = preg_replace('#^' . $m . '#', '', $value);
                    }
                    echo "[$value] ";
                    $m = $value;
                }
            }

            if ($input === null) {
                $input = 'NULL';
            }

            if ($input === '') {
                $input = "''";
            }

            if ($input === true) {
                $input = 'true';
            }

            if ($input === false) {
                $input = 'false';
            }

            if (PHP_SAPI === 'cli') {
                echo ' = ' . $input . PHP_EOL;
            } else {
                echo ' = ' . $input . '<br>';
            }
    }
}



/**
 * 再帰的にサニタイジングを行ないます
 *
 * @param unknown_type $mixed
 * @param unknown_type $quote_style
 * @param unknown_type $charset
 * @return unknown_type
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/08/27
 * @version SVN:$Id: functions.php,v 1.2 2012/12/19 03:45:20 fujimoto Exp $
 */
function htmlspecialchars2($mixed, $quoteStyle = ENT_QUOTES, $charset = 'UTF-8')
{
    // include_once 'Popo/Abstract.php';
    include_once 'Mikoshiva/Utility/Copy.php';
    switch (true) {

        case ($mixed instanceof Mikoshiva_Controller_Mapping_Form_Interface):
            $popProperties = array();
            foreach($mixed->toArrayProperty() as $key => $value) {
                $popProperties[$key] = htmlspecialchars2($value, $quoteStyle, $charset);
            }

            // 上書き
            $mixed = Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($mixed, $popProperties);
            break;

        case ($mixed instanceof Popo_Abstract):
            $popProperties = array();
            foreach($mixed->getProperties() as $key => $value) {
                $popProperties[$key] = htmlspecialchars2($value, $quoteStyle, $charset);
            }

            // 上書き
            $mixed->copyProperty($popProperties);
            break;

        case (is_array($mixed)):
            foreach($mixed as $key => $value) {
                $mixed[$key] = htmlspecialchars2($value, $quoteStyle, $charset);
            }
            break;

        case (is_string($mixed)):
            $mixed = htmlspecialchars(
                htmlspecialchars_decode($mixed, $quoteStyle),
                $quoteStyle,
                $charset
            );
            break;
    }
    return $mixed;
}


function errorMailTransporter($exceptionMessage, $displayTrace = false) {
    include_once 'Mikoshiva/Exception.php';
    errorMail(new Mikoshiva_Exception($exceptionMessage), '', $displayTrace);
}


/**
 * エラーメール共通メソッド
 *
 * @param Exception $e
 * @param 件名 $title
 */
function errorMail(Exception $e, $title = '', $displayTrace = false) {
    logInfo('▼------- エラーメール開始 -------▼');



        //------------------------------------------------------------------
        // ■リクエストの取得
        //------------------------------------------------------------------
        include_once 'Zend/Controller/Front.php';
        $front   = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $requestList = $request->getParams();
        if (isset($requestList['error_handler'])) {
            unset($requestList['error_handler']);
        }
        if (isset($requestList['cardNo'])) {
            $requestList['cardNo'] = '------------' . mb_substr($requestList['cardNo'], 12);
        }


        //------------------------------------------------------------------
        // ■件名
        //------------------------------------------------------------------
        if ($title === 'Mikoshivaでエラーが起きました') $title = '';
        $moduleName  = isset($requestList['module']) ? $requestList['module'] : '';
        $moduleName .= ($moduleName === 'batch' && isset($requestList['controller']))
                     ? '/' . $requestList['controller']
                     : '';
        $subject     = "[ERROR] {$moduleName}: {$title} " . preg_replace('#【.*?】#', '', $e->getMessage(), 1);
        if (_SERVER_MODE_ID_ === 'training.mikoshiva') $subject     = "[ERROR][training] {$moduleName}: {$title} " . preg_replace('#【.*?】#', '', $e->getMessage(), 1);
        //$subject = "【Mikoshiva:エラー】【{$moduleName}】{$title}";
        //$subject = mb_strimwidth($subject, 0, 70, '...');

        // ▼staff 情報が有れば取り出す
        $staffId = '';
        $staffName = '';
        $divisionName = '';
        include_once 'Zend/Session.php';
        include_once 'Zend/Session/Namespace.php';
        if (Zend_Session::namespaceIsset('staff')) {
            $staff = new Zend_Session_Namespace('staff');
            if (isset($staff->mstStaff) && $staff->mstStaff instanceof Popo_MstStaff) {
                $staffId = $staff->mstStaff->getStaffId();
                $staffName = $staff->mstStaff->getLastName() . ' ' . $staff->mstStaff->getFirstName();
                $divisionName = $staff->mstDivision->getDivisionName();
            }
        }


        //------------------------------------------------------------------
        // ■サーバー情報
        //------------------------------------------------------------------
        $_tmpSERVER = array();
        $_tmpSERVER['HTTPS']           = isset($_SERVER['HTTPS']           ) ? $_SERVER['HTTPS']           : '';
        $_tmpSERVER['HTTP_REFERER']    = isset($_SERVER['HTTP_REFERER']    ) ? $_SERVER['HTTP_REFERER']    : '';
        $_tmpSERVER['HTTP_USER_AGENT'] = isset($_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $_tmpSERVER['SCRIPT_FILENAME'] = isset($_SERVER['SCRIPT_FILENAME'] ) ? $_SERVER['SCRIPT_FILENAME'] : '';
        $_tmpSERVER['REQUEST_URI']     = isset($_SERVER['REQUEST_URI']     ) ? $_SERVER['REQUEST_URI']     : '';
        $_tmpSERVER['SCRIPT_NAME']     = isset($_SERVER['SCRIPT_NAME']     ) ? $_SERVER['SCRIPT_NAME']     : '';
        $_tmpSERVER['PHP_SELF']        = isset($_SERVER['PHP_SELF']        ) ? $_SERVER['PHP_SELF']        : '';
        $_tmpSERVER['REQUEST_TIME']    = isset($_SERVER['REQUEST_TIME']    ) ? $_SERVER['REQUEST_TIME']    : '';
        $_tmpSERVER['REMOTE_ADDR']     = isset($_SERVER['REMOTE_ADDR']     ) ? $_SERVER['REMOTE_ADDR']    : '';
        // softbank
        if (isset($_SERVER['x-s-bearer'])) {
            $_tmpSERVER['x-jphone-color']   = isset($_SERVER['x-jphone-color']   ) ? $_SERVER['x-jphone-color']   : ''; // メインディスプレイで表示可能な色
            $_tmpSERVER['x-jphone-display'] = isset($_SERVER['x-jphone-display'] ) ? $_SERVER['x-jphone-display'] : ''; // メインディスプレイの物理サイズ
            $_tmpSERVER['x-jphone-msname']  = isset($_SERVER['x-jphone-msname']  ) ? $_SERVER['x-jphone-msname']  : ''; // 端末の機種名
            $_tmpSERVER['x-jphone-region']  = isset($_SERVER['x-jphone-region']  ) ? $_SERVER['x-jphone-region']  : ''; // ユーザの利用地域(国内、国外)を表す
            $_tmpSERVER['x-jphone-smaf']    = isset($_SERVER['x-jphone-smaf']    ) ? $_SERVER['x-jphone-smaf']    : ''; // SMAF 種別
            $_tmpSERVER['x-jphone-uid']     = isset($_SERVER['x-jphone-uid']     ) ? $_SERVER['x-jphone-uid']     : ''; // ユーザID(UID)
            $_tmpSERVER['x-s-bearer']       = isset($_SERVER['x-s-bearer']       ) ? $_SERVER['x-s-bearer']       : ''; // 使用しているネットワーク種別
            $_tmpSERVER['x-s-display-info'] = isset($_SERVER['x-s-display-info'] ) ? $_SERVER['x-s-display-info'] : ''; // ブラウザのコンテンツ表示領域、半角表示文字数、テキストブラウズ設定
            $_tmpSERVER['x-s-unique-id']    = isset($_SERVER['x-s-unique-id']    ) ? $_SERVER['x-s-unique-id']    : ''; // 端末が特殊モデルの場合に指定
        }
        // docomo
        if (isset($_SERVER['X-DCMGUID'])) {
            $_tmpSERVER['X-DCMGUID'] = isset($_SERVER['X-DCMGUID']) ? $_SERVER['X-DCMGUID'] : ''; // 端末製造番号・iモードID
        }
        // au
        if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {
            $_tmpSERVER['HTTP_X_UP_SUBNO'] = isset($_SERVER['HTTP_X_UP_SUBNO']) ? $_SERVER['HTTP_X_UP_SUBNO'] : ''; // EZ番号
        }


        //------------------------------------------------------------------
        // ■トレース徐放から不必要な項目を削除する
        //   ※デフォルトは非表示です
        //------------------------------------------------------------------
        $t = array();
        if ($displayTrace === true) {
            $t = $e->getTrace();
            foreach ($t as $k => $v) {
                if (isset($v['args'])) {
                    foreach ($v['args'] as $k2 => $v2) {
                        if (is_object($v2)) {
                            if (($v2 instanceof Mikoshiva_Controller_Mapping_Form_Abstract)) {
                                $t[$k]['args'][$k2] = '【表示項目が長い為クラス名だけ表示】Mikoshiva_Controller_Mapping_Form_Abstract';
                                continue;
                            }
                            if (($v2 instanceof Mikoshiva_Controller_Mapping_ActionContext)) {
                                $t[$k]['args'][$k2] = '【表示項目が長い為クラス名だけ表示】Mikoshiva_Controller_Mapping_ActionContext';
                                continue;
                            }
                            if (($v2 instanceof Order_Models_Classes_InputSettlement)) {
                                if (method_exists($t[$k]['args'][$k2], 'getCardNo') && method_exists($t[$k]['args'][$k2], 'setCardNo')) {
                                    $_cardNo = '';
                                    $_cardNo = $t[$k]['args'][$k2]->getCardNo();
                                    $_cardNo = '------------' . mb_substr($_cardNo, 12);
                                    $t[$k]['args'][$k2]->setCardNo($_cardNo);
                                }
                                $t[$k]['args'][$k2] = '【表示項目が長い為クラス名だけ表示】Mikoshiva_Controller_Mapping_ActionContext';
                                continue;
                            }
                            $_className = get_class($v2);
                            if (preg_match('#^Smarty#', $_className) === 1) {
                                $t[$k]['args'][$k2] = "【表示項目が長い為クラス名だけ表示】{$_className}";
                                continue;
                            }
                        }
                    }
                }
                if (isset($v['file'])) {
                    if (preg_match('#Zend.Controller.Front\.php$#', $v['file']) === 1) {
                        $t[$k] = '【表示項目が長い為クラス名だけ表示】Zend_Controller_Front.php';
                    }
                }
            }
        }


        //------------------------------------------------------------------
        // ■本文
        //------------------------------------------------------------------
        $body  = ''
               . '********************************************************************************' . "\n"
               . '障害発生時刻: ' . Mikoshiva_Date::mikoshivaNow('yyyy-MM-dd HH:mm:ss')             . "\n"
               . $subject                                                                           . "\n"
               . '********************************************************************************' . "\n"
               . 'message:'                                                                         . "\n"
               . $e->getMessage()                                                                   . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'file:'                                                                            . "\n"
               . $e->getFile()                                                                      . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'line:'                                                                            . "\n"
               . $e->getLine()                                                                      . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'code:'                                                                            . "\n"
               . $e->getCode()                                                                      . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'スタッフ:'                                                                        . "\n"
               .  'ＩＤ：' . $staffId                                                               . "\n"
               .  '部署：' . $divisionName                                                          . "\n"
               .  '名前：' . $staffName                                                             . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'セッションＩＤ:'                                                                  . "\n"
               .  session_id()                                                                      . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'リクエスト:'                                                                      . "\n"
               . print_r($requestList, true)                                                        . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'SERVER:'                                                                          . "\n"
               . print_r($_tmpSERVER, true)                                                         . "\n"
               . "\n"
               . '--------------------------------------------------------------------------------' . "\n"
               . 'traceAsString:'                                                                   . "\n"
               . $e->getTraceAsString()                                                             . "\n"
               . "\n"
               . '';
       // トレース情報の表示制御
       // ※デフォルトは非表示
       if ($displayTrace === true) {
            $body  .= ''
                   . '--------------------------------------------------------------------------------' . "\n"
                   . '例外:'                                                                            . "\n"
                   . print_r($t, true)                                                                  . "\n"
                   . "\n"
                   . '';
        }
        $body  .= ''
               . '********************************************************************************' . "\n"
               . '';


        //------------------------------------------------------------------
        // ■メール送信
        //------------------------------------------------------------------
        try {
            include_once 'Mikoshiva/Mail.php';
            $o = null;
            $o = new Mikoshiva_Mail();
            $o->setSubject($subject);
            $o->setFrom(ERROR_MAIL_FROM_ADDRESS, ERROR_MAIL_FROM_NAME);
            $o->addTo(ERROR_MAIL_TO_ADDRESS, ERROR_MAIL_TO_NAME);
            $o->setBodyText($body);

            $o->send();
            /*
            reportMail('テストの件名です！！', 'テスト本文です
            ２行目
            ３行目
            ４行目');
            */

        } catch (Exception $e) {
            throw $e;
        }

    logInfo('▲------- エラーメール開始 -------▲');
}









/**
 * レポートメール
 *
 * @param Exception $e
 * @param 件名 $title
 */
function reportMail($title, $body) {
    logInfo('▼------- レポートメール開始 -------▼');
if (_SERVER_MODE_ID_ === 'training.mikoshiva') return;
        //------------------------------------------------------------------
        // ■リクエストの取得
        //------------------------------------------------------------------
        include_once 'Zend/Controller/Front.php';
        $front   = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $requestList = $request->getParams();


        //------------------------------------------------------------------
        // ■件名
        //------------------------------------------------------------------
        $moduleName = isset($requestList['module']) ? $requestList['module'] : '';
        $moduleName .= ($moduleName === 'batch' && isset($requestList['controller']))
                     ? '/' . $requestList['controller']
                     : '';
        $subject = "[INFO] {$moduleName}: {$title} ";


        //------------------------------------------------------------------
        // ■メール送信
        //------------------------------------------------------------------
        try {
            include_once 'Mikoshiva/Mail.php';
            $o = null;
            $o = new Mikoshiva_Mail();
            $o->setSubject($subject);
            $o->setFrom(ERROR_MAIL_FROM_ADDRESS, ERROR_MAIL_FROM_NAME);
            $o->addTo(ERROR_MAIL_TO_ADDRESS, ERROR_MAIL_TO_NAME);
            $o->setBodyText($body);

            $o->send();

        } catch (Exception $e) {
            throw $e;
        }

    logInfo('▲------- レポートメール終了 -------▲');
}


/**
 * クラス名からインスタンスを生成します
 * Enter description here ...
 * @param string $className
 */
function getInstance($className) {

    $path = '';
    $path = str_replace('_', '/', $className) . '.php';

    require_once $path;


    return new $className();
}



function mikoshivaTraceMode(Exception $e) {
    if (method_exists($e, 'getDisplayTrace') === true) {
        $e->setDisplayTrace(true);
    }
    return $e;
}

















