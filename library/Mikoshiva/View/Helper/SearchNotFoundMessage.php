<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';

/**
 * 検索結果が取得出来なかったときのメッセージ
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/15
 * @version SVN:$Id: SearchNotFoundMessage.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_SearchNotFoundMessage extends Mikoshiva_View_Helper_Abstract {


    /**
     * 検索結果が取得出来なかったときのメッセージ
     *
     * @return string
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/15
     * @version SVN:$Id: SearchNotFoundMessage.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function searchNotFoundMessage () {

        return '<p style="color: red; border: 1px solid red;">検索結果はありません</p>';
    }
}
