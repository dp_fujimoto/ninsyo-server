<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Validate - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/validate">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> バリデータ"><?php echo basename(__FILE__, '.php') ?> バリデータ</h1>

            <p>クレジットカード番号検証し、それがGMOテスト用のカードだった場合、リクエストを送ったクライアントのIPアドレスを検証します。 </p>
            <p>クライアントのIPアドレスが、許可されたネットワークのアドレスか、または存在しない(バッチ起動時)の場合、trueとなります。</p>
            <p>現在許可されているIPアドレス
              <ul>
                <?php 
                    $flag = true;
                    foreach(file(dirname(__FILE__) . '/../../../configs/define.php') as $line) {
                        if(preg_match('/\$GMO_TEST_CARD_ALLOW_LIST\[\]/', $line) !== 0) {
                            $line = str_replace('$GMO_TEST_CARD_ALLOW_LIST[] = ', '', $line);
                            echo "<li>{$line}</li>";
                            $flag = false;
                        }
                    }
                    if($flag) {
                        echo "<li>ダイレクト出版グローバルIPアドレス</li>\n";
                        echo "<li>ローカルホスト</li>\n";
                        echo "<li>社内LAN：大阪オフィス</li>\n";
                        echo "<li>社内LAN：東京オフィス</li>\n";
                    }
                ?>
              </ul>
            </p>
            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <h3>通常の検証方法</h3>
            <pre class="wiki">
            cardNo:
              0: TestCard
            </pre>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>