<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstStaff.php';

/**
 * スタッフ情報の更新を行う。
 *
 * @author         K.Fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/22
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Staff_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * スタッフ情報の更新を行う。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        //
        // スタッフ情報の更新を行う。
        //
        //-----------------------------------------------
        // 空のデータはnullにするため、配列にして処理してからactionFormに詰める
        $data = Mikoshiva_Utility_Copy::copyToArray($actionForm);
        logInfo('(' . __LINE__ . ')', 'foreach：ループ開始');
        foreach ($data as $k => $v) {
            logInfo('(' . __LINE__ . ')', 'loop：ループキー：' . $k);
            $data[$k] = Mikoshiva_Utility_Convert::blankToNull($v);
        }
        Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($actionForm,$data);

        $db = new Mikoshiva_Db_Simple();

        // popoの宣言とactionFormの値詰め替え
        $staff = new Popo_MstStaff();

        if ($actionForm->getLvFlag() === '1') {

            // 更新時は元のデータを取得し、更新内容を上書きしてアップデートを行う
            if ($actionForm->getStaffId() > 0) {
                logInfo('(' . __LINE__ . ')', 'if：スタッフIDあり');
                $staff = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstStaff', $actionForm->getStaffId());

            } else {
                logInfo('(' . __LINE__ . ')', 'if：スタッフIDあり');
                $staff->setLoginPassword( $actionForm->getLoginPassword() );
            }

            if ($actionForm->getSpFlag() === '1') {
                logInfo('(' . __LINE__ . ')', 'if：SPﾌﾗｸﾞあり');
                $staff->setLoginPassword( $actionForm->getLoginPassword() );
            }

            $staff->setDivisionId( $actionForm->getDivisionId() );
            $staff->setLastName( $actionForm->getLastName() );
            $staff->setFirstName( $actionForm->getFirstName() );
            $staff->setLastNameKana( $actionForm->getLastNameKana() );
            $staff->setFirstNameKana( $actionForm->getFirstNameKana() );
            $staff->setEmail( $actionForm->getEmail() );
            $staff->setValidFlag( $actionForm->getValidFlag() );
            $staff->setDeleteFlag( $actionForm->getDeleteFlag() );

            if ($actionForm->getDeleteFlag() === '1') {
                logInfo('(' . __LINE__ . ')', 'if：削除フラグが１');
                $staff->setDeletionDatetime( Mikoshiva_Date::mikoshivaNow() );
            } else {
                logInfo('(' . __LINE__ . ')', 'if：削除フラグが１以外');
                $staff->setDeletionDatetime(null);
            }
        } else {
            $staff = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstStaff', $context->getStaff()->mstStaff->getStaffId());

            $staff->setDivisionId( $actionForm->getDivisionId() );
            $staff->setLastName( $actionForm->getLastName() );
            $staff->setFirstName( $actionForm->getFirstName() );
            $staff->setLastNameKana( $actionForm->getLastNameKana() );
            $staff->setFirstNameKana( $actionForm->getFirstNameKana() );

        }

        // 更新（シンプルクラス）
        $result = $db->simpleOneCreateOrUpdateById( 'Mikoshiva_Db_NinsyoAr_MstStaff', $staff  );

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $actionForm;
    }
}
