<?php
require_once 'Mikoshiva/Db/Simple.php';
require_once 'Mikoshiva/Controller/Model/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Popo/MstCampaign.php';
require_once 'Mikoshiva/Mail.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Error_Models_Batch_ErrorFinder extends Mikoshiva_Controller_Model_Abstract {

    /**
     *
     * @var Mikoshiva_Db_Simple
     */
    private $_simple;

    public function __construct() {
        parent::__construct();
        $this->_simple = new Mikoshiva_Db_Simple();
    }
    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface &$actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $contex
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
        Mikoshiva_Controller_Mapping_ActionContext $context) {




        // ▼ロールバック
        try {
            Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
        } catch (Exception $e) {

        }





        // レスポンスオブジェクトを取得する
        $response = new Zend_Controller_Response_Http();
        // エラーハンドラを取得する
        $errors = $actionForm->get('error_handler');
        if(isBlankOrNull($errors)) {
            echo 'No Exception Found'."\n";
            return;
        }

        //--------------------------------------------------------------------
        // ■ ログ出力、DB更新、メール送信
        //--------------------------------------------------------------------
        // ErrorFinderのログを更新
        $this->_writeErrorLog($errors->exception, $context->getRequest());

        // エラーログテーブルを更新
        $this->_updateErrorLogTable($errors->exception, $context->getRequest());

        // エラーメールを送信
        $this->_sendMail($errors->exception, $context->getRequest());

        //--------------------------------------------------------------------
        // ■ 画面出力
        //--------------------------------------------------------------------
        // エラーの種別ごとにメッセージを表示する
        switch ($errors->type) {
            // コントローラが見つからない場合
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                //$response->setRawHeader('HTTP/1.1 404 Not Found');
                $this->_result = '404';
                break;
                // その他のエラーの場合
            default:
                // メッセージがセットされていればそれを表示する
                $exception = $errors->exception;
                if ($exception !== null) {
                    // デバッグモードならエラーメッセージをセットする
                    $debug  = nl2br(print_r($exception->getMessage(), true));
                    $debug .= '<br><br>';
                    $debug .= nl2br(print_r($exception->getTraceAsString() , true));


                    $message = $exception->getMessage();
                    $message = (strncmp(PHP_OS, 'WIN', 3) === 0)
                             ? mb_convert_encoding($message, 'SJIS-win',  'UTF-8')
                             : mb_convert_encoding($message, 'eucJP-win', 'UTF-8');

                    Zend_Debug::dump($message);
                    Zend_Debug::dump($exception->getTraceAsString());
                }
                break;
        }
    }

    /**
     *
     *
     * @param Exception $exception
     * @param Mikoshiva_Controller_Request_Simple $request
     * @return
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/05/07
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:33 fujimoto Exp $
     */
    private function _writeErrorLog($exception, $request) {
        $errorLevel     = '';
        $errorMessage   = '';
        $fullPath       = '';
        $debugTrace     = '';
        $message        = '';
        $match = array();


        if(preg_match('/^(.*?):(.*)$/', $exception->getMessage(), $match) > 0) {
            $errorLevel = trim($match[1]);
            $errorMessage   = trim($match[2]);
        } else {
            $errorLevel     = 'EMERG';
            $errorMessage   = $exception->getMessage();
        }

        $fullPath       = trim($exception->getFile());
        $debugTrace     = $exception->getTraceAsString();

        // ログへの出力(ここで設定する$messageはメールの内容と無関係)
        $message .= "\n";
        $message .= '**************************************************************' . "\n";
        $message .= 'エラーファイル:' . $fullPath . "\n";
        $message .= '**************************************************************' . "\n";
        $message .= 'メッセージ:' . $errorMessage . "\n";
        $message .= "\n";
        $message .= 'Debug Trace:' . "\n";
        $message .= $debugTrace . "\n";

        Mikoshiva_Logger::emerg($message);
    }



    /**
     * エラーログテーブルを更新します
     *
     * @param Exception $exception
     * @param Mikoshiva_Controller_Request_Simple $request
     * @return
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/05/07
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:33 fujimoto Exp $
     */
    private function _updateErrorLogTable($exception, $request) {
        $staff    = new Zend_Session_Namespace('staff');
        $staffId  = null;
        if($staff->mstStaff !== null) {
            /* @var $mstStaff Popo_MstStaff */
            $staffId  = $staff->mstStaff->getStaffId();
        }

        $match = array();
        if(preg_match('/^(.*?):(.*)$/', $exception->getMessage(), $match) > 0) {
            $errorLevel = trim($match[1]);
            $errorMsg   = trim($match[2]);
        } else {
            $errorLevel     = 'EMERG';
            $errorMsg   = $exception->getMessage();
        }


        $lastSql = null;
        if(Zend_Registry::getInstance()->isRegistered('LAST_EXECUTED_SQL') === true){
            $lastSql = Zend_Registry::get('LAST_EXECUTED_SQL');
        }

        $fullPath   = trim($exception->getFile());
        $fileName = basename($fullPath);

        //$data['errorLogId']       = ;                                  // エラーログＩＤ
        $data['errorLevel']       = $errorLevel;                         // エラーレベル
        $data['errorMsg']         = $errorMsg;                           // エラーメッセージ
        $data['errorSql']         = $lastSql;                            // エラーＳＱＬ
        $data['errorInfo']        = null;                                // エラー情報
        $data['executeFile']      = $fileName;                           // エラーファイル
        $data['executePath']      = $fullPath;                           // エラーファイル（フルパス）
        $data['executeAllPath']   = $exception->getTraceAsString();      // エラーファイル（デバッグトレース）
        $data['referrer']         = '';                                  // リファラーパス
        $data['ipAddress']        = '';                                  // IPアドレス
        $data['userId']           = $staffId;                            // オペレーター
        $data['deleteFlag']       = '0';

        try {
            $this->_simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstErrorLog', $data);
        } catch (Exception $e) {
            Mikoshiva_Logger::debug('{'.__CLASS__.'}#{'.__FUNCTION__.'} （'.__LINE__.'）】エラーログのDB登録に失敗しました');
        }
    }

    /**
     * エラーメールを送信します
     *
     * @param Exception $exception
     * @param Mikoshiva_Controller_Request_Simple $request
     * @return
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/23
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:33 fujimoto Exp $
     */
     private function _sendMail($exception, $request) {
         $mail = new Mikoshiva_Mail();
     // メールサーバ情報(ローカルのSMTPサーバを使用する場合は不要)
     //                $options = array(
     //                        'auth'     => 'login',
     //                        'ssl'      => 'ssl',
     //                        'port'     => '465',
     //                        'username' => 'mikoshivatest@gmail.com',
     //                        'password' => 'miko11112222'
     //                        );
     //        $tr = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $options);
     //        $mail->setDefaultTransport($tr);
        $errorLevel     = '';
        $errorMessage   = '';
        $fullPath       = '';
        $debugTrace     = '';
        $message        = '';
        $match = array();


        if(preg_match('/^(.*?):(.*)$/', $exception->getMessage(), $match) > 0) {
            $errorLevel = trim($match[1]);
            $errorMessage   = trim($match[2]);
        } else {
            $errorLevel     = 'EMERG';
            $errorMessage   = $exception->getMessage();
        }

        //$requestParams = array();
        //$requestParams = explode("\n", dumper($request->getParams(), null, false));
        //foreach($requestParams as $key => $value) {
        //    $requestParams[$key] = preg_replace('/^.*cardNo.*$/', '${1}*********カード番号***********', $value);
        //}

        // エラーがあった際のリクエストの取得
        $work        = $request->getParams();
        $requestOrg  = null;
        $requestOrg  = $work['error_handler']['request'];
        $requestUri  = '';
        $requestUri .= '/' . $requestOrg->getModuleName();
        $requestUri .= '/' . $requestOrg->getControllerName();
        $requestUri .= '/' . $requestOrg->getActionName();

        // エラーが起きたファイルを取得
        $fullPath     = trim($exception->getFile());

        $replacements = array();
        //$replacements['MODULE']         = $request->getModuleName();
        //$replacements['CONTROLLER']     = $request->getControllerName();
        //$replacements['ACTION']         = $request->getActionName();
        $replacements['FULL_PATH']      = $fullPath;
        $replacements['ERROR_MESSAGE']  = $errorMessage;
        $replacements['DEBUG_TRACE']    = $exception->getTraceAsString();
        $replacements['REQUEST_URI']    = $requestUri;


        $template = $this->getTemplate(1);
        $mail->setSubject($template['subject']);
        $mail->setBodyTextReplace($template['body'], $replacements);
        $mail->setFrom('admin@mikoshiva.com', 'Mikoshiva System');
        $mail->addTo('direct.errors@gmail.com', 'Mikoshivaエラー受信');
        //$mail->addTo('kaihatsuteam@googlegroups.com', '開発チームML');
        //$mail->addTo('ikumamasayuki@gmail.com', 'addTo');
        //$mail->addCc('elfaris@taupe.plala.or.jp', '谷口さん☆addCc');
        //$mail->addBcc('elfaris.iphone@gmail.com');

        try {
            if (method_exists($exception, 'getDisplayTrace') === true) {
                /* @var $exception Mikoshiva_Exception */
                errorMail($exception, 'Mikoshivaでエラーが起きました', $exception->getDisplayTrace());
            } else {
                /* @var $exception Exception */
                errorMail($exception, 'Mikoshivaでエラーが起きました');
            }
            // 2010-11-30 T.Taniguchi errorMailメソッドに変更
            // $mail->send();
        } catch (Exception $e) {
            Mikoshiva_Logger::debug('{'.__CLASS__.'}#{'.__FUNCTION__.'} （'.__LINE__.'）】エラーメールの送信に失敗しました');
        }
    }

    /**
     * エラーメールのテンプレートを返します
     *
     * @param  int $code エラーテンプレート番号
     * @return array
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/23
     * @version SVN:$Id: ErrorFinder.php,v 1.1 2012/09/21 07:08:33 fujimoto Exp $
     */
    public function getTemplate($code) {
        $template  = array();

        switch((string)$code) {
            case '1':
                $template['subject'] = '';
                $template['subject'] = 'Mikoshivaでエラーが起きました' . "\n";

                $template['body']  = '';
                $template['body'] .= 'Mikoshivaでエラーが起きました' . "\n";
                $template['body'] .= "\n";
                $template['body'] .= '**************************************************************' . "\n";
                $template['body'] .= 'エラーファイル: %FULL_PATH%' . "\n";
                $template['body'] .= '**************************************************************' . "\n";
                $template['body'] .= 'メッセージ: %ERROR_MESSAGE%' . "\n";
                $template['body'] .= 'リクエストURL: %REQUEST_URI%' . "\n";
                $template['body'] .= "\n";
                $template['body'] .= 'Debug Trace:' . "\n";
                $template['body'] .= '%DEBUG_TRACE%' . "\n";
                $template['body'] .= "\n";
                //$template['body'] .= 'Request Params:' . "\n";
                //$template['body'] .= '%REQUEST_PARAMS%' . "\n";
                //$template['body'] .= "\n";
                break;
            default:
        }

        return $template;
    }
}
