<?php

/** @package Mikoshiva_View*/

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';

/**
 * 部署リストを表示します
 * 
 * @package Mikoshiva_View
 * @author Tsukasa Taniguchi 2009-11-19
 *
 */
abstract class Mikoshiva_View_Helper_Db_Abstract extends Zend_View_Helper_Abstract
{
	public function __construct()
	{
        // データベースハンドルの取得
        $db = Zend_Db::factory('Pdo_Mysql', Array(
            'host'     => DB_PAYMENT_HOST,
            'username' => DB_PAYMENT_USER_NAME,
            'password' => DB_PAYMENT_PASSWORD,
            'dbname'   => DB_PAYMENT_DB_NAME
        ));
        $db->query('SET NAMES utf8');
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
	}
}
