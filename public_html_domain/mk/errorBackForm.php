<?php
	require_once './init.php';



	error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[case:ERROR_BACK_FORM]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

	$transactionNumber = $_POST['transactionNumber'];
	if ($transactionNumber + 1 == $_SESSION['transactionNumber']) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[if:セッションのトランザクションNo - 1と一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$campaignSalesCode = $_SESSION['campaignSalesCode'];
		$_SESSION['transactionNumber'] = $transactionNumber;
        if ($transactionNumber == '1') {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[if:セッションのトランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
            // パラメータを追加
            $params                      = $_POST;
            $params['campaignSalesCode'] = $campaignSalesCode;

            // mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'input', $params);

            echo $response;
		} else {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[else:セッションのトランザクションNoが１以外]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
            // mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'back', $_POST);

            echo $response;
		}
	} else if ($transactionNumber == $_SESSION['transactionNumber']) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[elseif:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$campaignSalesCode = $_SESSION['campaignSalesCode'];
		if ($transactionNumber === '1') {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[if:セッションのトランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
            // パラメータを追加
            $params                      = $_POST;
            $params['campaignSalesCode'] = $campaignSalesCode;

            // mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'input', $params);

            echo $response;
		} else {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[if:セッションのトランザクションNoが１以外]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
            // mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'back', $_POST);

            echo $response;
		}
	}
    error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
