<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">お知らせ情報登録　確認</h2>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                {hiddenLister hiddenList=$form}

                <!-- // お知らせマスタ情報入力テーブル開始 -->
                <table class="COM_table COM_tableWidthLong">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">ID</td>
                        <td>{if !isBlankOrNull($form->getInformationHistoryId())}{$form->getInformationHistoryId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">件名</td>
                        <td>
                            {$form->getSubject()}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">本文</td>
                        <td>
                            <pre>{$form->getBody()}</pre>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">削除フラグ</td>
                        <td>
                            {if $form->getDeleteFlag() === '1'}削除する　(<span class="COM_fontRed">削除フラグを立てるとお知らせ一覧に表示されません。再編集もできなくなります。</span>){else}削除しない{/if}
                        </td>
                    </tr>
                </table>
                <!-- // 商品マスタ編集フォーム終了 -->

                <br>
                <hr>
                <br>

                {currentTabButton name='　　　戻る　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/information/confirm-back' tabId=$tabId  formId='inputForm'}
                　　　
                {currentTabButton name='　　　　　　　登録　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/information/complete' tabId=$tabId  formId='inputForm'}

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>
</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->