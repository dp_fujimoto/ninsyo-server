<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Db/Ar/Exceptoin.php';
require_once 'Mikoshiva/Utility/String.php';

/**
 * SQL テーブルクラス
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Table_Abstract extends Zend_Db_Table_Abstract {


    protected $_rowClass    = 'Mikoshiva_Db_Table_Row';
    protected $_rowsetClass = 'Mikoshiva_Db_Table_Rowset';






    /**
     * DB から 1 件値を取得します
     * 1件以上取得出来た場合、Mikoshiva_Db_Ar_Exceptoin 投げます
     *
     * @param array $condition
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneSelect(array $condition) {

        // ログ書き出し
        logInfo(LOG_START);

        $select = $this->select();
        foreach ($condition as $k => $v) {
            $select->where($k . '= ?', $v);
        }


        $rows = array();
        $rows = $this->fetchAll($select);

        // 1件も取得出来なければ空の配列を返す
        if ($rows->count() === 0) {

            // ログ終了
            logInfo(LOG_END);
            return array();
        }

        // １件以上取得出来たらエラー
        if ($rows->count() > 1) {
            throw new Mikoshiva_Db_Ar_Exceptoin('【' . get_class($this) . '#simpleOneSelect】1件以上の値が取得されました。');
        }

        $row = $rows->toArray();


        // ログ終了
        logInfo(LOG_END);
        return $row[0];
    }


    /**
     * 指定された ID でにヒットしたデータを返します
     *
     * @param int $id
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneSelectById($id) {

        // ログ書き出し
        logInfo(LOG_START);


        $primaryList = $this->info('primary');


        $select = $this->select();
        $select->where($primaryList[1] . ' = ?', $id);

        $rows = array();
        $rows = $this->fetchAll($select);

        // 1件も取得出来なければ空の配列を返す
        if ($rows->count() === 0) {

            // ログ終了
            logInfo(LOG_END);
            return array();
        }

        if ($rows->count() > 1) {
            throw new Mikoshiva_Db_Ar_Exceptoin('【' . get_class($this) . '#simpleOneSelectById】1件以上の値が取得されました。');
        }


        $row = $rows->toArray();


        // ログ終了
        logInfo(LOG_END);
        return $row[0];
    }


    /**
     * 指定された ID でにヒットしたデータを返します
     *
     * @param int $id
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneSelectByIdFromForm(array $condition) {

        // ログ書き出し
        logInfo(LOG_START);


        $primaryList = $this->info('primary');

        if (!isset($condition[$primaryList[1]])) {
            throw new Mikoshiva_Db_Ar_Exceptoin('【' . get_class($this) . '#simpleOneSelectById】与えられた配列にプライマリーキー ' . $primaryList[1] . ' が存在しません。');
        }


        $select = $this->select();
        $select->where($primaryList[1] . ' = ?', $condition[$primaryList[1]]);

        $rows = array();
        $rows = $this->fetchAll($select);

        // 1件も取得出来なければ空の配列を返す
        if ($rows->count() === 0) {

            // ログ終了
            logInfo(LOG_END);
            return array();
        }

        if ($rows->count() > 1) {
            throw new Mikoshiva_Db_Ar_Exceptoin('【' . get_class($this) . '#simpleOneSelectById】1件以上の値が取得されました。');
        }


        $row = $rows->toArray();

        // ログ終了
        logInfo(LOG_END);

        return $row[0];
    }



    /**
     * 指定された条件にマッチしたデータを返します
     *
     * @param array $condition
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/24
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleManySelect(array $condition) {

        // ログ書き出し
        logInfo(LOG_START);


        $select = $this->select();
        foreach ($condition as $k => $v) {
            $select->where($k . ' = ?', $v);
        }


        $rows = array();
        $rows = $this->fetchAll($select);

        // 1件も取得出来なければ空の配列を返す
        if ($rows->count() === 0) {

            // ログ終了
            logInfo(LOG_END);
            return array();
        }

        // ログ終了
        logInfo(LOG_END);
        return $rows->toArray();
    }


    /**
     * プライマリキーを条件に登録・更新処理を行います。
     * プライマリキーが引数の値に存在しなければ登録処理を行います。
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/05
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function simpleOneCreateOrUpdateById(array $data) {

        // ログ書き出し
        logInfo(LOG_START);

        // プライマリキーを取得
        $primaryList = $this->info('primary');

        // 処理結果
        $resulet = null;


        // プライマリキーの有無で処理を振り分け
        // $data にプライマリキーが存在し、かつ、値が存在していれば更新
        if (isset($data[ $primaryList[1] ]) && ! isBlankOrNull($data[ $primaryList[1] ])) {


            //-----------------------------------------------
            // 更新
            //-----------------------------------------------
            Mikoshiva_Logger::debug('-- UPDATE START --');


            // プライマリキーが不正な値であればエラーにする
            // 10 進数でない
            // bool(false) 11
            // bool(false) '11'
            // bool(true) 11.11
            // bool(true) '11.11'
            // bool(true) 'a'
            // bool(true) '0'
            // bool(true) 0
            // bool(true) 0.5
            if (! ctype_digit((string)$data[ $primaryList[1] ]) || $data[ $primaryList[1] ] === 0) {
                throw new Mikoshiva_Exception('【Mikoshiva_Db_Table_Abstract#simpleOneCreateOrUpdateById】プライマリキーの値は 0 以上の数字文字列で入力して下さい・');
            }


            // WHERE
            $where = array();
            $where[ $primaryList[1] . ' = ?'] = $data[ $primaryList[1] ];

            // プライマリキーを除去 -- 更新対象に含めない（特に意味はないけど・・・）
            unset($data[ $primaryList[1] ]);

            // 実行
            $resulet = $this->update($data, $where);

            Mikoshiva_Logger::debug('-- UPDATE END --');

        } else {


            //-----------------------------------------------
            // 新規登録
            //-----------------------------------------------
            Mikoshiva_Logger::debug('-- INSERT START --');


            // プライマリキーが存在していたら除去
            if ( array_key_exists($primaryList[1], $data) ) {
                unset($data[ $primaryList[1] ]);
            }


            // 実行
            $resulet = $this->insert($data);
            Mikoshiva_Logger::debug('-- INSERT END --');
        }


        // ログ終了
        logInfo(LOG_END);

        return $resulet;
    }


    /**
     * カラム情報の一覧を返します
     *
     * @return array カラム情報の一覧
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getColumnList() {
        return $this->info('cols');
    }


    /**
     * カラム名一覧を取得し、それをキーとした配列をかえします
     *
     * @param $initData 配列の初期値
     * @return array
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function createArrayColumnNames($initData = null) {

        // ログスタート
        logInfo(LOG_START);


        // 配列の値を取得
        $cols = $this->getColumnList();


        // 配列の値をキーにした配列を生成
        $newArray = array();
        foreach ($cols as $k => $v) {
            $newArray[ $v ] = $initData;
        }


        // ログエンド
        logInfo(LOG_START);

        return $newArray;
    }


    /**
     * テーブル名を返します
     * Enter description here ...
     */
    public function getTableName() {
        return $this->_name;
    }

    /**
     * プライマリキーを返します
     * Enter description here ...
     */
    public function getPrimary() {
        return $this->_primary;
    }

    /**
     * Prefixを返します
     * Enter description here ...
     */
    public function getPrefix() {
        return Mikoshiva_Utility_String::getColumnPrefix($this->_primary);
    }


}




















