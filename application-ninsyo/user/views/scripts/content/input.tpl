<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ユーザーコンテンツ登録</h2>

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。
            <br>
            <span class="COM_fontRed">無条件閲覧コンテンツは自動的に登録されます。</span>

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}" >
                {$deleteList = array()}
                {$deleteList[] = 'tokenId'}
                {$deleteList[] = 'email'}
                {$deleteList[] = 'password'}
                {$deleteList[] = 'contentId'}
                {hiddenLister hiddenList=$form deleteList=$deleteList}

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            サーバー名<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <select name="serverName">
                                <option value="mikoshiva" {if $form->getServerName() === 'mikoshiva'}selected{/if}>mikoshiva</option>
                                <option value="shimant" {if $form->getServerName() === 'shimant'}selected{/if}>shimant</option>
                                <option value="gusiken" {if $form->getServerName() === 'gusiken'}selected{/if}>gusiken</option>
                                <option value="test.mikoshiva" {if $form->getServerName() === 'test.mikoshiva'}selected{/if}>test.mikoshiva</option>
                                <option value="free" {if $form->getServerName() === 'free'}selected{/if}>free（無料ＷＥＢ認証アカウント登録用）</option>
                            </select>
                            　※chopperのゲスト登録時は、freeを選択
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            メールアドレス<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="email" value="{$form->getEmail()}" size="80" maxlength="200"><br>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            パスワード<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="password" value="{$form->getPassword()}" size="20" maxlength="20">
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            コンテンツID<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="contentId" value="{$form->getContentId()}" size="80">
                            <br>
                            <span class="COM_fontGrey">カンマ区切りで複数可能 例:1,2,3</span>
                        </td>
                    </tr>
                    {*
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            取消設定
                        </td>
                        <td class="COM_nowrap">
                            <input type="checkbox" name="deleteFlag" value="1" {if $form->getDeleteFlag() === '1'}checked{/if}>上記で入力したコンテンツID以外の、閲覧許可確認ありのコンテンツを見れないようにする
                            <br>
                            <span class="COM_fontGrey">閲覧許可確認とは、条件に不適合になった人は見れなくなるようにする機能</span>
                        </td>
                    </tr>
                    *}
                </table>

                <br>
                <hr>
                <br>

                <!-- // ページ下部のボタン開始 -->
                <div>
                    {currentTabButton name='　　　　　　次へ　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/user/content/confirm" tabId=$tabId  formId='inputForm'}
                </div>
                <!-- // ページ下部のボタン終了 -->

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->