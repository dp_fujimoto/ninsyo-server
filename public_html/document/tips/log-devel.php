<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1>ログ関連 - 開発ログ出力</h1>


            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

              <p>
                以下のコードで、開発ログを出力できます。<br>
              </p>

            <pre class="wiki"><?php
highlight_string('<?php

        //----------------------------------------------
        // キャンセルに必要なデータ(オーダーIDのみ)をセットして実行
        //----------------------------------------------

        //----------------------------------------------
        // 実行
        //----------------------------------------------
        // 実行
        logDevel(\'ログメッセージ\');
'); ?>
            </pre>


            <!-- ========================================
              ■出力
            ==========================================-->
            <h2 id="出力例">出力例</h2>
            出力先<br>
            　　Web: /systems/logs/devel<br>
            　　バッチ: /systems/bin_logs/devel
            <pre class="wiki"><?php
highlight_string('
2010-05-18 13:04:18 [DEVEL]: [builder_builder_search]  Builder_Models_Builder_Search#execute（131） ログメッセージ
'); ?>
            </pre>
            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















