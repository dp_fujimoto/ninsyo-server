<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
include_once 'Mikoshiva/Utility/Copy.php';
require_once 'Popo/MstStaff.php';
require_once 'Popo/MstDivision.php';
require_once 'Zend/Registry.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Login_Models_Index_IndexModel extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface &$actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $contex
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $contex) {



        //------------------------------------------------------------------
        // ■セッション情報を削除
        //------------------------------------------------------------------
        Zend_Session::destroy();

        //------------------------------------------------------------------
        // ■許可 IP を取得
        //------------------------------------------------------------------
        $GMO_TEST_CARD_ALLOW_LIST = array();
        $GMO_TEST_CARD_ALLOW_LIST = Zend_Registry::get('GMO_TEST_CARD_ALLOW_LIST');

        //------------------------------------------------------------------
        // ■許可 IP であるか検証
        //------------------------------------------------------------------
        $ret = false;
        $userIpAddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''; // クライアントの IP アドレスを取得
        foreach($GMO_TEST_CARD_ALLOW_LIST as $chackIpAddress) {

            if (preg_match('#^192\.168#', $userIpAddress) === 1) {
                $ret = true;
                break;
            }

            if ($userIpAddress === $chackIpAddress) {
                $ret = true;
                break;
            }
        }

        //------------------------------------------------------------------
        // ■許可 IP で無ければ強制終了
        //------------------------------------------------------------------
        if ($ret === false) {
            echo 'アクセスできません。';
            exit;
        }

        return null;
    }

}

























