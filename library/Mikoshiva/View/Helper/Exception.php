<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/Exception.php';

/**
 * ヘルパーの例外クラス
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_Exception extends Mikoshiva_Exception {

}
