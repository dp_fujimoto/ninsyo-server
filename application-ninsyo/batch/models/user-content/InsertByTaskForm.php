<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * @author      fujimoto <fujimoto@d-publishing.jp>
 * @since       2012/06/29
 * @version     SVN: $Id: InsertByTaskForm.php,v 1.1 2012/09/21 07:08:29 fujimoto Exp $
 *
 */
class Batch_Models_UserContent_InsertByTaskForm extends Mikoshiva_Controller_Mapping_Form_Abstract {

}
