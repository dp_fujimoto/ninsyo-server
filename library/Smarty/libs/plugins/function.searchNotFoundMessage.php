<?php

require_once 'Mikoshiva/Logger.php';


/**
 * 検索結果が取得出来なかった時のメッセージを返します。
 *
 * @param  array   $params  現在未使用
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/15
 * @version SVN:$Id: function.searchNotFoundMessage.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_searchNotFoundMessage($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    Mikoshiva_Logger::info(LOG_END);
    return '<p style="color: red; border: 1px solid red;">検索結果はありません</p>';
}

