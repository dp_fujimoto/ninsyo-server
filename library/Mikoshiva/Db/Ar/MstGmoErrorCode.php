<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstGmoErrorCode
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstGmoErrorCode extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_gmo_error_code';
    protected $_primary  = 'mge_gmo_error_code_id';
    protected $_metadata = array (
  'mge_gmo_error_code_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_gmo_error_code_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'mge_error_code' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_error_code',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '3',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mge_error_code_detail' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_error_code_detail',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '9',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mge_settlement_status' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_settlement_status',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '2',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mge_settlement_target' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_settlement_target',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mge_error_content' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_error_content',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mge_workaround' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_workaround',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mge_error_message' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_gmo_error_code',
    'COLUMN_NAME' => 'mge_error_message',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

