<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_user_content
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstUserContent extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'muc';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'userContentId' => null,
        'userId' => null,
        'contentId' => null,
        'serviceStartDate' => null,
        'serviceEndDate' => null,
        'authFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * muc_user_content_id の値を返します
	 *
     * @return int muc_user_content_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserContentId() {
		return $this->_properties['userContentId'];
    }


    /**
     * muc_user_content_id の値をセットします
	 *
	 * @param int $userContentId muc_user_content_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserContentId($userContentId) {
		$this->_properties['userContentId'] = $userContentId;
    }


    /**
     * muc_user_id の値を返します
	 *
     * @return int muc_user_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserId() {
		return $this->_properties['userId'];
    }


    /**
     * muc_user_id の値をセットします
	 *
	 * @param int $userId muc_user_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserId($userId) {
		$this->_properties['userId'] = $userId;
    }


    /**
     * muc_content_id の値を返します
	 *
     * @return int muc_content_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContentId() {
		return $this->_properties['contentId'];
    }


    /**
     * muc_content_id の値をセットします
	 *
	 * @param int $contentId muc_content_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContentId($contentId) {
		$this->_properties['contentId'] = $contentId;
    }


    /**
     * muc_service_start_date の値を返します
	 *
     * @return date muc_service_start_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceStartDate() {
		return $this->_properties['serviceStartDate'];
    }


    /**
     * muc_service_start_date の値をセットします
	 *
	 * @param date $serviceStartDate muc_service_start_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceStartDate($serviceStartDate) {
		$this->_properties['serviceStartDate'] = $serviceStartDate;
    }


    /**
     * muc_service_end_date の値を返します
	 *
     * @return date muc_service_end_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getServiceEndDate() {
		return $this->_properties['serviceEndDate'];
    }


    /**
     * muc_service_end_date の値をセットします
	 *
	 * @param date $serviceEndDate muc_service_end_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setServiceEndDate($serviceEndDate) {
		$this->_properties['serviceEndDate'] = $serviceEndDate;
    }


    /**
     * muc_auth_flag の値を返します
	 *
     * @return char muc_auth_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAuthFlag() {
		return $this->_properties['authFlag'];
    }


    /**
     * muc_auth_flag の値をセットします
	 *
	 * @param char $authFlag muc_auth_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAuthFlag($authFlag) {
		$this->_properties['authFlag'] = $authFlag;
    }


    /**
     * muc_delete_flag の値を返します
	 *
     * @return char muc_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * muc_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag muc_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * muc_deletion_datetime の値を返します
	 *
     * @return datetime muc_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * muc_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime muc_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * muc_registration_datetime の値を返します
	 *
     * @return datetime muc_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * muc_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime muc_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * muc_update_datetime の値を返します
	 *
     * @return datetime muc_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * muc_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime muc_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * muc_update_timestamp の値を返します
	 *
     * @return timestamp muc_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * muc_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp muc_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























