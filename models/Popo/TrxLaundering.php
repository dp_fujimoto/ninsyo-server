<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_laundering
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxLaundering extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tla';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'launderingId' => null,
        'divisionId' => null,
        'directionId' => null,
        'directionStatusId' => null,
        'customerId' => null,
        'shopId' => null,
        'memberId' => null,
        'launderingFlag' => null,
        'launderingErrorFlag' => null,
        'launderingYearMonth' => null,
        'skipFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 洗替ID の値を返します
	 *
     * @return int 洗替ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLaunderingId() {
		return $this->_properties['launderingId'];
    }


    /**
     * 洗替ID の値をセットします
	 *
	 * @param int $launderingId 洗替ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLaunderingId($launderingId) {
		$this->_properties['launderingId'] = $launderingId;
    }


    /**
     * 部署ID の値を返します
	 *
     * @return int 部署ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDivisionId() {
		return $this->_properties['divisionId'];
    }


    /**
     * 部署ID の値をセットします
	 *
	 * @param int $divisionId 部署ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDivisionId($divisionId) {
		$this->_properties['divisionId'] = $divisionId;
    }


    /**
     * 指示ID の値を返します
	 *
     * @return int 指示ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionId() {
		return $this->_properties['directionId'];
    }


    /**
     * 指示ID の値をセットします
	 *
	 * @param int $directionId 指示ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionId($directionId) {
		$this->_properties['directionId'] = $directionId;
    }


    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * 会員ID の値を返します
	 *
     * @return char 会員ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemberId() {
		return $this->_properties['memberId'];
    }


    /**
     * 会員ID の値をセットします
	 *
	 * @param char $memberId 会員ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemberId($memberId) {
		$this->_properties['memberId'] = $memberId;
    }


    /**
     * 洗替済フラグ の値を返します
	 *
     * @return char 洗替済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLaunderingFlag() {
		return $this->_properties['launderingFlag'];
    }


    /**
     * 洗替済フラグ の値をセットします
	 *
	 * @param char $launderingFlag 洗替済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLaunderingFlag($launderingFlag) {
		$this->_properties['launderingFlag'] = $launderingFlag;
    }


    /**
     * 洗替エラーフラグ の値を返します
	 *
     * @return char 洗替エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLaunderingErrorFlag() {
		return $this->_properties['launderingErrorFlag'];
    }


    /**
     * 洗替エラーフラグ の値をセットします
	 *
	 * @param char $launderingErrorFlag 洗替エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLaunderingErrorFlag($launderingErrorFlag) {
		$this->_properties['launderingErrorFlag'] = $launderingErrorFlag;
    }


    /**
     * 洗替年月 の値を返します
	 *
     * @return char 洗替年月
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getLaunderingYearMonth() {
		return $this->_properties['launderingYearMonth'];
    }


    /**
     * 洗替年月 の値をセットします
	 *
	 * @param char $launderingYearMonth 洗替年月
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setLaunderingYearMonth($launderingYearMonth) {
		$this->_properties['launderingYearMonth'] = $launderingYearMonth;
    }


    /**
     * 洗替スキップフラグ の値を返します
	 *
     * @return char 洗替スキップフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSkipFlag() {
		return $this->_properties['skipFlag'];
    }


    /**
     * 洗替スキップフラグ の値をセットします
	 *
	 * @param char $skipFlag 洗替スキップフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSkipFlag($skipFlag) {
		$this->_properties['skipFlag'] = $skipFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























