<?php
/** @package Mikoshiva_Controller */


/**
 * Mikoshiva_Controller_Db_Action
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/03
 * @version SVN:$Id: Action.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Controller_Db_Action extends Zend_Controller_Action
{
    /**
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;
    /**
     * (non-PHPdoc)
     * @see library/Zend/Controller/Zend_Controller_Action#init()
     */
    public function init()
    {
        // データベースハンドルの取得
        /* @var $_db Zend_Db_Adapter_Abstract */
        $db = Zend_Db::factory('Pdo_Mysql', Array(
            'host'     => DB_PAYMENT_HOST,
            'username' => DB_PAYMENT_USER_NAME,
            'password' => DB_PAYMENT_PASSWORD,
            'dbname'   => DB_PAYMENT_DB_NAME
        ));
        $db->query('SET NAMES utf8');
        $this->_db = $db;
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
    }
}
