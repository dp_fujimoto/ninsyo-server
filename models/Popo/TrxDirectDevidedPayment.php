<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_direct_devided_payment
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxDirectDevidedPayment extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tdd';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'directDevidedPaymentId' => null,
        'chargeId' => null,
        'directDevidedPaymentNumber' => null,
        'orderId' => null,
        'accessId' => null,
        'accessPass' => null,
        'amount' => null,
        'realChargeType' => null,
        'accountingFlag' => null,
        'chargeDate' => null,
        'chargeFlag' => null,
        'chargeErrorFlag' => null,
        'rechargeOrderId' => null,
        'rechargeType' => null,
        'rechargeDueDate' => null,
        'rechargeDate' => null,
        'rechargeErrorFlag' => null,
        'refundFlag' => null,
        'errorType' => null,
        'errorCode' => null,
        'errorInfo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tdd_direct_devided_payment_id の値を返します
	 *
     * @return int tdd_direct_devided_payment_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectDevidedPaymentId() {
		return $this->_properties['directDevidedPaymentId'];
    }


    /**
     * tdd_direct_devided_payment_id の値をセットします
	 *
	 * @param int $directDevidedPaymentId tdd_direct_devided_payment_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectDevidedPaymentId($directDevidedPaymentId) {
		$this->_properties['directDevidedPaymentId'] = $directDevidedPaymentId;
    }


    /**
     * tdd_charge_id の値を返します
	 *
     * @return int tdd_charge_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeId() {
		return $this->_properties['chargeId'];
    }


    /**
     * tdd_charge_id の値をセットします
	 *
	 * @param int $chargeId tdd_charge_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeId($chargeId) {
		$this->_properties['chargeId'] = $chargeId;
    }


    /**
     * tdd_direct_devided_payment_number の値を返します
	 *
     * @return int tdd_direct_devided_payment_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectDevidedPaymentNumber() {
		return $this->_properties['directDevidedPaymentNumber'];
    }


    /**
     * tdd_direct_devided_payment_number の値をセットします
	 *
	 * @param int $directDevidedPaymentNumber tdd_direct_devided_payment_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectDevidedPaymentNumber($directDevidedPaymentNumber) {
		$this->_properties['directDevidedPaymentNumber'] = $directDevidedPaymentNumber;
    }


    /**
     * tdd_order_id の値を返します
	 *
     * @return char tdd_order_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderId() {
		return $this->_properties['orderId'];
    }


    /**
     * tdd_order_id の値をセットします
	 *
	 * @param char $orderId tdd_order_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderId($orderId) {
		$this->_properties['orderId'] = $orderId;
    }


    /**
     * tdd_access_id の値を返します
	 *
     * @return char tdd_access_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessId() {
		return $this->_properties['accessId'];
    }


    /**
     * tdd_access_id の値をセットします
	 *
	 * @param char $accessId tdd_access_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessId($accessId) {
		$this->_properties['accessId'] = $accessId;
    }


    /**
     * tdd_access_pass の値を返します
	 *
     * @return char tdd_access_pass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessPass() {
		return $this->_properties['accessPass'];
    }


    /**
     * tdd_access_pass の値をセットします
	 *
	 * @param char $accessPass tdd_access_pass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessPass($accessPass) {
		$this->_properties['accessPass'] = $accessPass;
    }


    /**
     * tdd_amount の値を返します
	 *
     * @return int tdd_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAmount() {
		return $this->_properties['amount'];
    }


    /**
     * tdd_amount の値をセットします
	 *
	 * @param int $amount tdd_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAmount($amount) {
		$this->_properties['amount'] = $amount;
    }


    /**
     * tdd_real_charge_type の値を返します
	 *
     * @return varchar tdd_real_charge_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealChargeType() {
		return $this->_properties['realChargeType'];
    }


    /**
     * tdd_real_charge_type の値をセットします
	 *
	 * @param varchar $realChargeType tdd_real_charge_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealChargeType($realChargeType) {
		$this->_properties['realChargeType'] = $realChargeType;
    }


    /**
     * tdd_accounting_flag の値を返します
	 *
     * @return char tdd_accounting_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccountingFlag() {
		return $this->_properties['accountingFlag'];
    }


    /**
     * tdd_accounting_flag の値をセットします
	 *
	 * @param char $accountingFlag tdd_accounting_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccountingFlag($accountingFlag) {
		$this->_properties['accountingFlag'] = $accountingFlag;
    }


    /**
     * tdd_charge_date の値を返します
	 *
     * @return date tdd_charge_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeDate() {
		return $this->_properties['chargeDate'];
    }


    /**
     * tdd_charge_date の値をセットします
	 *
	 * @param date $chargeDate tdd_charge_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeDate($chargeDate) {
		$this->_properties['chargeDate'] = $chargeDate;
    }


    /**
     * tdd_charge_flag の値を返します
	 *
     * @return char tdd_charge_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeFlag() {
		return $this->_properties['chargeFlag'];
    }


    /**
     * tdd_charge_flag の値をセットします
	 *
	 * @param char $chargeFlag tdd_charge_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeFlag($chargeFlag) {
		$this->_properties['chargeFlag'] = $chargeFlag;
    }


    /**
     * tdd_charge_error_flag の値を返します
	 *
     * @return char tdd_charge_error_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeErrorFlag() {
		return $this->_properties['chargeErrorFlag'];
    }


    /**
     * tdd_charge_error_flag の値をセットします
	 *
	 * @param char $chargeErrorFlag tdd_charge_error_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeErrorFlag($chargeErrorFlag) {
		$this->_properties['chargeErrorFlag'] = $chargeErrorFlag;
    }


    /**
     * tdd_recharge_order_id の値を返します
	 *
     * @return char tdd_recharge_order_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeOrderId() {
		return $this->_properties['rechargeOrderId'];
    }


    /**
     * tdd_recharge_order_id の値をセットします
	 *
	 * @param char $rechargeOrderId tdd_recharge_order_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeOrderId($rechargeOrderId) {
		$this->_properties['rechargeOrderId'] = $rechargeOrderId;
    }


    /**
     * tdd_recharge_type の値を返します
	 *
     * @return varchar tdd_recharge_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeType() {
		return $this->_properties['rechargeType'];
    }


    /**
     * tdd_recharge_type の値をセットします
	 *
	 * @param varchar $rechargeType tdd_recharge_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeType($rechargeType) {
		$this->_properties['rechargeType'] = $rechargeType;
    }


    /**
     * tdd_recharge_due_date の値を返します
	 *
     * @return date tdd_recharge_due_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeDueDate() {
		return $this->_properties['rechargeDueDate'];
    }


    /**
     * tdd_recharge_due_date の値をセットします
	 *
	 * @param date $rechargeDueDate tdd_recharge_due_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeDueDate($rechargeDueDate) {
		$this->_properties['rechargeDueDate'] = $rechargeDueDate;
    }


    /**
     * tdd_recharge_date の値を返します
	 *
     * @return date tdd_recharge_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeDate() {
		return $this->_properties['rechargeDate'];
    }


    /**
     * tdd_recharge_date の値をセットします
	 *
	 * @param date $rechargeDate tdd_recharge_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeDate($rechargeDate) {
		$this->_properties['rechargeDate'] = $rechargeDate;
    }


    /**
     * tdd_recharge_error_flag の値を返します
	 *
     * @return char tdd_recharge_error_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRechargeErrorFlag() {
		return $this->_properties['rechargeErrorFlag'];
    }


    /**
     * tdd_recharge_error_flag の値をセットします
	 *
	 * @param char $rechargeErrorFlag tdd_recharge_error_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRechargeErrorFlag($rechargeErrorFlag) {
		$this->_properties['rechargeErrorFlag'] = $rechargeErrorFlag;
    }


    /**
     * tdd_refund_flag の値を返します
	 *
     * @return char tdd_refund_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRefundFlag() {
		return $this->_properties['refundFlag'];
    }


    /**
     * tdd_refund_flag の値をセットします
	 *
	 * @param char $refundFlag tdd_refund_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRefundFlag($refundFlag) {
		$this->_properties['refundFlag'] = $refundFlag;
    }


    /**
     * tdd_error_type の値を返します
	 *
     * @return char tdd_error_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorType() {
		return $this->_properties['errorType'];
    }


    /**
     * tdd_error_type の値をセットします
	 *
	 * @param char $errorType tdd_error_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorType($errorType) {
		$this->_properties['errorType'] = $errorType;
    }


    /**
     * tdd_error_code の値を返します
	 *
     * @return char tdd_error_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorCode() {
		return $this->_properties['errorCode'];
    }


    /**
     * tdd_error_code の値をセットします
	 *
	 * @param char $errorCode tdd_error_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorCode($errorCode) {
		$this->_properties['errorCode'] = $errorCode;
    }


    /**
     * tdd_error_info の値を返します
	 *
     * @return char tdd_error_info
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorInfo() {
		return $this->_properties['errorInfo'];
    }


    /**
     * tdd_error_info の値をセットします
	 *
	 * @param char $errorInfo tdd_error_info
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorInfo($errorInfo) {
		$this->_properties['errorInfo'] = $errorInfo;
    }


    /**
     * tdd_delete_flag の値を返します
	 *
     * @return char tdd_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tdd_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tdd_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tdd_deletion_datetime の値を返します
	 *
     * @return datetime tdd_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tdd_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tdd_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tdd_registration_datetime の値を返します
	 *
     * @return datetime tdd_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tdd_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tdd_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tdd_update_datetime の値を返します
	 *
     * @return datetime tdd_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tdd_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tdd_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tdd_update_timestamp の値を返します
	 *
     * @return timestamp tdd_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tdd_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tdd_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























