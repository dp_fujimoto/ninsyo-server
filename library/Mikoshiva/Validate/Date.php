<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Date
 */
require_once 'Zend/Validate/Date.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 日付関連のメソッドを提供します。
 *
 * @category   Mikoshiva
 * @package    Mikoshiva_Validate
 */
class Mikoshiva_Validate_Date extends Zend_Validate_Date
{

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_YYYY_MM_DD => "%COLUMN_NAME%が YYYY-MM-DD 形式ではありません",
        self::FALSEFORMAT   => "%COLUMN_NAME%が 正しい形式ではありません",
        self::INVALID        => "%COLUMN_NAME%は正しい日付ではないようです",
    );

    /**
     * バリデーション用のフォーマットを設定します。
     *
     * @param string $dateFormat
     * @return void
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __construct($dateFormat = '') {
        logInfo(LOG_START);
        if(!isBlankOrNull($dateFormat)) {
            $this->setFormat($dateFormat);
        }
        logInfo(LOG_END);
        return;
    }

    /**
     * 入力された文字列に対し、Validator.ymlに設定されたフォーマットと一致するかのチェックを行います。
     * (Zend_Dateのチェックよりは厳密)
     * フォーマットと一致したものについては、Zend_Validate_Dateにチェックを依頼し、
     * 存在しない日付・時刻のチェックを行います。
     *
     * @param string $value
     * @return unknown_type
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Date#isValid($value)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: Date.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function isValid($value) {
        logInfo(LOG_START);
        if($this->_format !== null) {
            //--------------------------------------
            //■フォーマット判定用の正規表現を設定
            //--------------------------------------
            switch($this->_format) {
                case('yyyy-MM-dd'):
                    $formatRE='^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$';
                    break;
                case('yyyy/MM/dd'):
                    $formatRE='^[0-9]{4}\/[0-1][0-9]\/[0-3][0-9]$';
                    break;
                case('yyyy-MM-dd HH:mm:ss'):
                    $formatRE='^[0-9]{4}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$';
                    break;
                case('yyyy/MM/dd HH:mm:ss'):
                    $formatRE='^[0-9]{4}\/[0-1][0-9]\/[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$';
                    break;
                default:
                    $this->_error(self::NOT_YYYY_MM_DD);
                    logInfo(LOG_END);
                    return false;
            }
            //--------------------------------------
            //■正規表現でフォーマットチェックを行う
            //--------------------------------------
            if(!preg_match('/^' . $formatRE . '$/', $value)) {
                $this->_error(parent::FALSEFORMAT);
                logInfo(LOG_END);
                return false;
            }

        }
        logInfo(LOG_END);
        return parent::isValid($value);
    }
}
