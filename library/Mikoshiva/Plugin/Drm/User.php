<?php

require_once 'Zend/Http/Client.php';
require_once 'Zend/Http/Client/Adapter/Socket.php';
require_once 'Zend/Http/Client/Adapter/Exception.php';

require_once 'Mikoshiva/Plugin/Drm/Abstruct.php';

/**
 * DRMユーザー関連のDRMAPIを呼び出します。
 *
 * @author k.shimowaki
 * @since version - 2010/03/18
 */
class Mikoshiva_Plugin_Drm_User extends Mikoshiva_Plugin_Drm_Abstruct {

	/**
	 * DRMへユーザーのチェックを行います。
	 *
	 * @param Popo_MstCustomer $customer 顧客情報
	 * @param string $cardNo カード番号
	 * @param string $expirationDateMonth 有効期限（月）
	 * @param string $expirationDateYear 有効期限（年）
	 * @param array $productCd 商品コード（DRM,DRS）
	 * @return string
	 *
	 * @author k.shimowaki
	 * @since version - 2010/03/18
	 */
	public function checkUser(Popo_MstCustomer $customer, $cardNo, $expirationDateMonth, $expirationDateYear, array $productCd) {

		$c = new Popo_MstCustomer();
		$c = $customer;

		$approachPlan = array();

		foreach ($productCd as $key => $val) {
			if ($val === 'DRM') {
				$approachPlan[] = '0';
			} else if ($val === 'DRS') {
				$approachPlan[] = '1';
			} else {
				$approachPlan[] = '';
			}
		}

		$tel = array();
		$zip = array();

		if (!isBlankOrNull($c->getTel())) {
			$tel = split('-', $c->getTel());
		}
		if (!isBlankOrNull($c->getZip())) {
			$zip = split('-', $c->getZip());
		}

        if (isBlankOrNull($cardNo)) {
        	$cardNo = '0000000000000000';
        }
        if (isBlankOrNull($expirationDateMonth)) {
            $expirationDateMonth = '00';
        }
        if (isBlankOrNull($expirationDateYear)) {
            $expirationDateYear = '00';
        }


		// パラメータセット
		$this->client->setParameterPost('pathCode','USER_CHECK');										// パスコード

		$this->client->setParameterPost('email', $c->getEmail());										// メールアドレス
		$this->client->setParameterPost('lastName', $c->getLastName());									// 姓
		$this->client->setParameterPost('firstName', $c->getFirstName());								// 名
		$this->client->setParameterPost('lastNameKana', $c->getLastNameKana());							// 姓カナ
		$this->client->setParameterPost('firstNameKana', $c->getFirstNameKana());						// 名カナ
		if (count($tel) === 3) {
			$this->client->setParameterPost('tel1', $tel[0]);											// 電話番号１
			$this->client->setParameterPost('tel2', $tel[1]);											// 電話番号２
			$this->client->setParameterPost('tel3', $tel[2]);											// 電話番号３
		}
		if (count($zip) === 2) {
			$this->client->setParameterPost('zip1', $zip[0]);											// 郵便番号１
			$this->client->setParameterPost('zip2', $zip[1]);											// 郵便番号２
		}
		$this->client->setParameterPost('prefecture', $c->getPrefecture());								// 都道府県
		$this->client->setParameterPost('address1', $c->getAddress1());									// 住所１
		$this->client->setParameterPost('address2', $c->getAddress2());									// 住所２
		$this->client->setParameterPost('cardNo', $cardNo);												// カード番号
		$this->client->setParameterPost('expirationDateMonth', $expirationDateMonth);					// 有効期限（月）
		$this->client->setParameterPost('expirationDateYear', $expirationDateYear);						// 有効期限（年）
		$this->client->setParameterPost('productCd', $productCd);										// 商品コード
		$this->client->setParameterPost('validateFlag', '0');											// バリデートフラグ
		$this->client->setParameterPost('approachPlan', $approachPlan);									// アプローチプラン

		// 実行
		$response = $this->client->request('POST');
		return $this->retFormat($response->getBody());

	}

	/**
	 * DRMへユーザーの登録を行います。
	 *
	 * @param Popo_MstCustomer $customer 顧客情報
	 * @param string $cardNo カード番号
	 * @param string $expirationDateMonth 有効期限（月）
	 * @param string $expirationDateYear 有効期限（年）
	 * @param array $productCd 商品コード（DRM,DRS）
	 * @return string
	 *
	 * @author k.shimowaki
	 * @since version - 2010/03/18
	 */
	public function createUser(Popo_MstCustomer $customer, $cardNo, $expirationDateMonth, $expirationDateYear, array $productCd, $price,$subscription, $tranDate, $orderId, $freePeriodCreditCard) {

		$c = new Popo_MstCustomer();
		$c = $customer;

		$promotionId = array();
		$approachPlan = array();
		$freePeriodList = array();
		$priceList = array();
		$subscriptionList = array();

		$priceList[] = $price;
		$subscriptionList[] = $subscription;

		foreach ($productCd as $key => $val) {
			if ($val === 'DRM') {
				$promotionId[] = '3';
				$approachPlan[] = '0';
				$freePeriodList[] = $freePeriodCreditCard;
			} else if ($val === 'DRS') {
				$promotionId[] = '2';
				$approachPlan[] = '1';
				$freePeriodList[] = $freePeriodCreditCard;
			} else {
				$promotionId[] = '';
				$approachPlan[] = '';
				$freePeriodList[] = '';
			}
		}

		$tel = array();
		$zip = array();

		if (!isBlankOrNull($c->getTel())) {
			$tel = split('-', $c->getTel());
		}
		if (!isBlankOrNull($c->getZip())) {
			$zip = split('-', $c->getZip());
		}

        if (isBlankOrNull($cardNo)) {
            $cardNo = '0000000000000000';
        }
        if (isBlankOrNull($expirationDateMonth)) {
            $expirationDateMonth = '00';
        }
        if (isBlankOrNull($expirationDateYear)) {
            $expirationDateYear = '00';
        }

		// パラメータセット
		$this->client->setParameterPost('pathCode','USER_CREATE');										// パスコード

		$this->client->setParameterPost('email', $c->getEmail());										// メールアドレス
		$this->client->setParameterPost('lastName', $c->getLastName());									// 姓
		$this->client->setParameterPost('firstName', $c->getFirstName());								// 名
		$this->client->setParameterPost('lastNameKana', $c->getLastNameKana());							// 姓カナ
		$this->client->setParameterPost('firstNameKana', $c->getFirstNameKana());						// 名カナ
		if (count($tel) === 3) {
			$this->client->setParameterPost('tel1', $tel[0]);											// 電話番号１
			$this->client->setParameterPost('tel2', $tel[1]);											// 電話番号２
			$this->client->setParameterPost('tel3', $tel[2]);											// 電話番号３
		}
		if (count($zip) === 2) {
			$this->client->setParameterPost('zip1', $zip[0]);											// 郵便番号１
			$this->client->setParameterPost('zip2', $zip[1]);											// 郵便番号２
		}
		$this->client->setParameterPost('prefecture', $c->getPrefecture());								// 都道府県
		$this->client->setParameterPost('address1', $c->getAddress1());									// 住所１
		$this->client->setParameterPost('address2', $c->getAddress2());									// 住所２
		$this->client->setParameterPost('cardNo', $cardNo);												// カード番号
		$this->client->setParameterPost('expirationDateMonth', $expirationDateMonth);					// 有効期限（月）
		$this->client->setParameterPost('expirationDateYear', $expirationDateYear);						// 有効期限（年）
		$this->client->setParameterPost('productCd', $productCd);										// 商品コード
		$this->client->setParameterPost('validateFlag', '0');											// バリデートフラグ
		$this->client->setParameterPost('paymentStatus', '1');											// 決済ステータス
		$this->client->setParameterPost('price', $priceList);												// 金額
		$this->client->setParameterPost('tranDate', $tranDate);											// 決済日時
		$this->client->setParameterPost('promotionId', $promotionId);									// プロモーションID
		$this->client->setParameterPost('approachPlan', $approachPlan);									// アプローチプラン
		$this->client->setParameterPost('orderId', $orderId);											// オーダーID
		$this->client->setParameterPost('customerId', $customer->getCustomerId());						// 顧客ID
		$this->client->setParameterPost('number', $customer->getTotalPurchaseNumber());					// 決済回数
		$this->client->setParameterPost('freePeriod', $freePeriodList);									// お試し期間（月）
		$this->client->setParameterPost('subscriptionFee', $subscriptionList);									// お試し期間（月）

		// 実行
		$response = $this->client->request('POST');
		return $this->retFormat($response->getBody());

	}
}