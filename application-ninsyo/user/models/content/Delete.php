<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'tool/models/Operation.php';

/**
 * コンテンツ情報の削除を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class User_Models_Content_Delete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * コンテンツ情報の削除を行う。
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 更新（シンプルクラス）
        $db = null;
        $db = new Mikoshiva_Db_Simple();

        $where = array();
        $where['userContentId'] = $actionForm->getUserContentId();
        $where['deleteFlag'] = '1';
        $where['deletionDatetime'] = Mikoshiva_date::mikoshivaNow();

        $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);

        echo '<span class="COM_fontRed">削除完了</span>';

        //-----------------------------------------------
        // アクションログ保存
        //-----------------------------------------------
        $logText = '';
        $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

        $operation = new Tool_Models_Operation();
        $operation->insertActionLog($logText);

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
