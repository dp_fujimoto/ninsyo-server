<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_claim
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxClaim extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tcm';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'claimId' => null,
        'depositFlag' => null,
        'provisorySalesType' => null,
        'claimAmount' => null,
        'moneyReceivedPlanNumber' => null,
        'moneyReceivedNumber' => null,
        'moneyReceivedAmount' => null,
        'moneyReceivedCancelNumber' => null,
        'moneyReceivedCancelAmount' => null,
        'recoveredCompleteFlag' => null,
        'recoveredCompleteDate' => null,
        'cesserFlag' => null,
        'cesserDate' => null,
        'accrualDate' => null,
        'imperfectionFlag' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tcm_claim_id の値を返します
	 *
     * @return int tcm_claim_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClaimId() {
		return $this->_properties['claimId'];
    }


    /**
     * tcm_claim_id の値をセットします
	 *
	 * @param int $claimId tcm_claim_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClaimId($claimId) {
		$this->_properties['claimId'] = $claimId;
    }


    /**
     * tcm_deposit_flag の値を返します
	 *
     * @return char tcm_deposit_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDepositFlag() {
		return $this->_properties['depositFlag'];
    }


    /**
     * tcm_deposit_flag の値をセットします
	 *
	 * @param char $depositFlag tcm_deposit_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDepositFlag($depositFlag) {
		$this->_properties['depositFlag'] = $depositFlag;
    }


    /**
     * tcm_provisory_sales_type の値を返します
	 *
     * @return varchar tcm_provisory_sales_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesType() {
		return $this->_properties['provisorySalesType'];
    }


    /**
     * tcm_provisory_sales_type の値をセットします
	 *
	 * @param varchar $provisorySalesType tcm_provisory_sales_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesType($provisorySalesType) {
		$this->_properties['provisorySalesType'] = $provisorySalesType;
    }


    /**
     * tcm_claim_amount の値を返します
	 *
     * @return int tcm_claim_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getClaimAmount() {
		return $this->_properties['claimAmount'];
    }


    /**
     * tcm_claim_amount の値をセットします
	 *
	 * @param int $claimAmount tcm_claim_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setClaimAmount($claimAmount) {
		$this->_properties['claimAmount'] = $claimAmount;
    }


    /**
     * tcm_money_received_plan_number の値を返します
	 *
     * @return int tcm_money_received_plan_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedPlanNumber() {
		return $this->_properties['moneyReceivedPlanNumber'];
    }


    /**
     * tcm_money_received_plan_number の値をセットします
	 *
	 * @param int $moneyReceivedPlanNumber tcm_money_received_plan_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedPlanNumber($moneyReceivedPlanNumber) {
		$this->_properties['moneyReceivedPlanNumber'] = $moneyReceivedPlanNumber;
    }


    /**
     * tcm_money_received_number の値を返します
	 *
     * @return int tcm_money_received_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedNumber() {
		return $this->_properties['moneyReceivedNumber'];
    }


    /**
     * tcm_money_received_number の値をセットします
	 *
	 * @param int $moneyReceivedNumber tcm_money_received_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedNumber($moneyReceivedNumber) {
		$this->_properties['moneyReceivedNumber'] = $moneyReceivedNumber;
    }


    /**
     * tcm_money_received_amount の値を返します
	 *
     * @return int tcm_money_received_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedAmount() {
		return $this->_properties['moneyReceivedAmount'];
    }


    /**
     * tcm_money_received_amount の値をセットします
	 *
	 * @param int $moneyReceivedAmount tcm_money_received_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedAmount($moneyReceivedAmount) {
		$this->_properties['moneyReceivedAmount'] = $moneyReceivedAmount;
    }


    /**
     * tcm_money_received_cancel_number の値を返します
	 *
     * @return int tcm_money_received_cancel_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedCancelNumber() {
		return $this->_properties['moneyReceivedCancelNumber'];
    }


    /**
     * tcm_money_received_cancel_number の値をセットします
	 *
	 * @param int $moneyReceivedCancelNumber tcm_money_received_cancel_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedCancelNumber($moneyReceivedCancelNumber) {
		$this->_properties['moneyReceivedCancelNumber'] = $moneyReceivedCancelNumber;
    }


    /**
     * tcm_money_received_cancel_amount の値を返します
	 *
     * @return int tcm_money_received_cancel_amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMoneyReceivedCancelAmount() {
		return $this->_properties['moneyReceivedCancelAmount'];
    }


    /**
     * tcm_money_received_cancel_amount の値をセットします
	 *
	 * @param int $moneyReceivedCancelAmount tcm_money_received_cancel_amount
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMoneyReceivedCancelAmount($moneyReceivedCancelAmount) {
		$this->_properties['moneyReceivedCancelAmount'] = $moneyReceivedCancelAmount;
    }


    /**
     * tcm_recovered_complete_flag の値を返します
	 *
     * @return char tcm_recovered_complete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRecoveredCompleteFlag() {
		return $this->_properties['recoveredCompleteFlag'];
    }


    /**
     * tcm_recovered_complete_flag の値をセットします
	 *
	 * @param char $recoveredCompleteFlag tcm_recovered_complete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRecoveredCompleteFlag($recoveredCompleteFlag) {
		$this->_properties['recoveredCompleteFlag'] = $recoveredCompleteFlag;
    }


    /**
     * tcm_recovered_complete_date の値を返します
	 *
     * @return date tcm_recovered_complete_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRecoveredCompleteDate() {
		return $this->_properties['recoveredCompleteDate'];
    }


    /**
     * tcm_recovered_complete_date の値をセットします
	 *
	 * @param date $recoveredCompleteDate tcm_recovered_complete_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRecoveredCompleteDate($recoveredCompleteDate) {
		$this->_properties['recoveredCompleteDate'] = $recoveredCompleteDate;
    }


    /**
     * tcm_cesser_flag の値を返します
	 *
     * @return char tcm_cesser_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCesserFlag() {
		return $this->_properties['cesserFlag'];
    }


    /**
     * tcm_cesser_flag の値をセットします
	 *
	 * @param char $cesserFlag tcm_cesser_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCesserFlag($cesserFlag) {
		$this->_properties['cesserFlag'] = $cesserFlag;
    }


    /**
     * tcm_cesser_date の値を返します
	 *
     * @return date tcm_cesser_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCesserDate() {
		return $this->_properties['cesserDate'];
    }


    /**
     * tcm_cesser_date の値をセットします
	 *
	 * @param date $cesserDate tcm_cesser_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCesserDate($cesserDate) {
		$this->_properties['cesserDate'] = $cesserDate;
    }


    /**
     * tcm_accrual_date の値を返します
	 *
     * @return date tcm_accrual_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccrualDate() {
		return $this->_properties['accrualDate'];
    }


    /**
     * tcm_accrual_date の値をセットします
	 *
	 * @param date $accrualDate tcm_accrual_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccrualDate($accrualDate) {
		$this->_properties['accrualDate'] = $accrualDate;
    }


    /**
     * tcm_imperfection_flag の値を返します
	 *
     * @return char tcm_imperfection_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImperfectionFlag() {
		return $this->_properties['imperfectionFlag'];
    }


    /**
     * tcm_imperfection_flag の値をセットします
	 *
	 * @param char $imperfectionFlag tcm_imperfection_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImperfectionFlag($imperfectionFlag) {
		$this->_properties['imperfectionFlag'] = $imperfectionFlag;
    }


    /**
     * tcm_memo の値を返します
	 *
     * @return text tcm_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * tcm_memo の値をセットします
	 *
	 * @param text $memo tcm_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * tcm_delete_flag の値を返します
	 *
     * @return char tcm_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tcm_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tcm_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tcm_deletion_datetime の値を返します
	 *
     * @return datetime tcm_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tcm_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tcm_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tcm_update_datetime の値を返します
	 *
     * @return datetime tcm_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tcm_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tcm_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tcm_registration_datetime の値を返します
	 *
     * @return datetime tcm_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tcm_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tcm_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tcm_update_timestamp の値を返します
	 *
     * @return timestamp tcm_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tcm_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tcm_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























