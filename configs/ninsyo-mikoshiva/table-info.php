<?php

$COLUMN_MAPPING_TABLE_NAME_LIST = array (
  'mbr' => 'mst_brand',
  'mcd' => 'mst_condition',
  'mct' => 'mst_content',
  'mdi' => 'mst_division',
  'mel' => 'mst_error_log',
  'mih' => 'mst_information_history',
  'mol' => 'mst_operation_log',
  'mpr' => 'mst_product',
  'mst' => 'mst_staff',
  'mus' => 'mst_user',
  'muc' => 'mst_user_content',
  'mub' => 'mst_user_content_blank',
  'muf' => 'mst_user_content_force',
  'tan' => 'trx_access_log_analysis',
  'tad' => 'trx_access_log_analysis_detail',
  'tat' => 'trx_access_log_totalization',
  'taa' => 'trx_action_analysis',
  'tal' => 'trx_action_log',
);

$COLUMN_MAPPING_POPO_NAME_LIST = array (
  'mbr' => 'Popo_MstBrand',
  'mcd' => 'Popo_MstCondition',
  'mct' => 'Popo_MstContent',
  'mdi' => 'Popo_MstDivision',
  'mel' => 'Popo_MstErrorLog',
  'mih' => 'Popo_MstInformationHistory',
  'mol' => 'Popo_MstOperationLog',
  'mpr' => 'Popo_MstProduct',
  'mst' => 'Popo_MstStaff',
  'mus' => 'Popo_MstUser',
  'muc' => 'Popo_MstUserContent',
  'mub' => 'Popo_MstUserContentBlank',
  'muf' => 'Popo_MstUserContentForce',
  'tan' => 'Popo_TrxAccessLogAnalysis',
  'tad' => 'Popo_TrxAccessLogAnalysisDetail',
  'tat' => 'Popo_TrxAccessLogTotalization',
  'taa' => 'Popo_TrxActionAnalysis',
  'tal' => 'Popo_TrxActionLog',
);

