<?php
/** @package Popo */

require_once 'Popo/TrxCharge.php';
require_once 'Popo/TrxShipping.php';


/**
 * 請求リレーションPOPO
 * ・$_shipping: 発送POPO配列
 *
 * @package Popo
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: TrxCharge.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
 */
class Popo_Relation_TrxCharge extends Popo_TrxCharge {

     /**
     * 発送POPO配列 <発送POPO>
     * @var $_shippings array <Popo_TrxShipping>
     */
    protected $_shippings = array();


    /**
     * getShippings
     *
     * @return array <Popo_TrxShipping>
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: TrxCharge.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
     *
     */
    public function getShippings() {
        return $this->_shippings;
    }


    /**
     * setShippings
     *
     * @param array $_shippings <Popo_TrxShipping>
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: TrxCharge.php,v 1.1 2012/09/21 07:08:27 fujimoto Exp $
     *
     */
    public function setShippings(array $shippings) {
        $this->_shippings = $shippings;
    }

}

























