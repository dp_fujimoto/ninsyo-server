<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Validate - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/validate">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> バリデータ"><?php echo basename(__FILE__, '.php') ?> バリデータ</h1>

            <p>
              別々でリクエストされる【時・分・秒】が時間として正しい形式であるか検証を行う<br>
              （※注）このバリデータは最低限【時】のリクエストが必須となります。
            </p>




            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <h3>通常の検証方法</h3>
            <p>
              hour=11&amp;min=12&amp;sec=13 と言う形でリクエストが送信された場合を想定しています。
            </p>
            <pre class="wiki">
hour:
  0: [DateLister, min, sec]
            </pre>


            <h3>月を省いた場合の検証方法</h3>
            <p>
              hour=11&amp;min=12&amp;sec=13 と言う形でリクエストが送信された場合を想定しています。<br>
              この例では month の値は見ません。
            </p>
            <pre class="wiki">
hour:
  0: [DateLister, null, sec]
            </pre>


            <h3>日を省いた場合の検証方法</h3>
            <p>
              hour=11&amp;min=12&amp;sec=13 と言う形でリクエストが送信された場合を想定しています。<br>
              この例では day の値は見ません。
            </p>
            <pre class="wiki">
hour:
  0: [DateLister, min]

  または、

hour:
  0: [DateLister, min, null]

            </pre>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






























