<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Card
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Card extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'AccessID' => null,
        'AccessPass' => null,
        'OrderID' => null,
        'Method' => null,
        'PayTimes' => null,
        'CardNo' => null,
        'Expire' => null,
        'SecurityCode' => null,
        'ClientField1' => null,
        'ClientField2' => null,
        'ClientField3' => null,
        'ClientFieldFlag' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * AccessID
	 *
     * @return  AccessID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessID() {
		return $this->_properties['AccessID'];
    }


    /**
     * AccessID
	 *
	 * @param  $AccessID AccessID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessID($AccessID) {
		$this->_properties['AccessID'] = $AccessID;
    }


    /**
     * AccessPass
	 *
     * @return  AccessPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessPass() {
		return $this->_properties['AccessPass'];
    }


    /**
     * AccessPass
	 *
	 * @param  $AccessPass AccessPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessPass($AccessPass) {
		$this->_properties['AccessPass'] = $AccessPass;
    }


    /**
     * OrderID
	 *
     * @return  OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getOrderID() {
		return $this->_properties['OrderID'];
    }


    /**
     * OrderID
	 *
	 * @param  $OrderID OrderID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
		$this->_properties['OrderID'] = $OrderID;
    }


    /**
     * Method
	 *
     * @return  Method
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getMethod() {
		return $this->_properties['Method'];
    }


    /**
     * Method
	 *
	 * @param  $Method Method
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setMethod($Method) {
		$this->_properties['Method'] = $Method;
    }


    /**
     * PayTimes
	 *
     * @return  PayTimes
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getPayTimes() {
		return $this->_properties['PayTimes'];
    }


    /**
     * PayTimes
	 *
	 * @param  $PayTimes PayTimes
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setPayTimes($PayTimes) {
		$this->_properties['PayTimes'] = $PayTimes;
    }


    /**
     * CardNo
	 *
     * @return  CardNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getCardNo() {
		return $this->_properties['CardNo'];
    }


    /**
     * CardNo
	 *
	 * @param  $CardNo CardNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setCardNo($CardNo) {
		$this->_properties['CardNo'] = $CardNo;
    }


    /**
     * Expire
	 *
     * @return  Expire
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getExpire() {
		return $this->_properties['Expire'];
    }


    /**
     * Expire
	 *
	 * @param  $Expire Expire
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setExpire($Expire) {
		$this->_properties['Expire'] = $Expire;
    }


    /**
     * SecurityCode
	 *
     * @return  SecurityCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSecurityCode() {
		return $this->_properties['SecurityCode'];
    }


    /**
     * SecurityCode
	 *
	 * @param  $SecurityCode SecurityCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSecurityCode($SecurityCode) {
		$this->_properties['SecurityCode'] = $SecurityCode;
    }


    /**
     * ClientField1
	 *
     * @return  ClientField1
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientField1() {
		return $this->_properties['ClientField1'];
    }


    /**
     * ClientField1
	 *
	 * @param  $ClientField1 ClientField1
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientField1($ClientField1) {
		$this->_properties['ClientField1'] = $ClientField1;
    }


    /**
     * ClientField2
	 *
     * @return  ClientField2
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientField2() {
		return $this->_properties['ClientField2'];
    }


    /**
     * ClientField2
	 *
	 * @param  $ClientField2 ClientField2
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientField2($ClientField2) {
		$this->_properties['ClientField2'] = $ClientField2;
    }


    /**
     * ClientField3
	 *
     * @return  ClientField3
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientField3() {
		return $this->_properties['ClientField3'];
    }


    /**
     * ClientField3
	 *
	 * @param  $ClientField3 ClientField3
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientField3($ClientField3) {
		$this->_properties['ClientField3'] = $ClientField3;
    }


    /**
     * ClientFieldFlag
	 *
     * @return  ClientFieldFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getClientFieldFlag() {
		return $this->_properties['ClientFieldFlag'];
    }


    /**
     * ClientFieldFlag
	 *
	 * @param  $ClientFieldFlag ClientFieldFlag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Card.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setClientFieldFlag($ClientFieldFlag) {
		$this->_properties['ClientFieldFlag'] = $ClientFieldFlag;
    }


}

























