<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Validate - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/validate">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> バリデータ"><?php echo basename(__FILE__, '.php') ?> バリデータ</h1>

            <p>電話番号としての妥当性を検証します。</p>



            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <h3>通常の検証方法</h3>
            <p>
              fax=052-653-2432 と言う形でリクエストが送信された場合を想定しています。
            </p>
            <pre class="wiki">
            fax:
              0: Fax
            </pre>

            <!-- ======================================== -->

            <h3>郵便番号の項目が分かれている場合の検証方法</h3>
            <p>
              fax1=052&fax2=653&fax3=2432 と言う形でリクエストが送信される場合を想定してます。
            </p>
            <pre class="wiki">
            fax1:
              0: [Fax, fax2, fax3]
            </pre>


  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>