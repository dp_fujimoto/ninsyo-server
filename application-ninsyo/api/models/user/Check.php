<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/classes/CheckSessionId.php';
require_once 'api/models/classes/CreateUserHash.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_User_Check extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     *
     * 認証コードが違う場合はなにも返さない
     * 認証コードはあっているがユーザーが居ない場合、falseを返す
     * 認証コードがあっていてユーザーが存在する場合、trueを返す
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 検索結果
        $result = 'no';

        $class = new Api_Models_Classes_CheckSessionId();
        $class->execute($actionForm->getSessionId());

        $class = new Api_Models_Classes_CreateUserHash();
        $idHash = $class->execute($actionForm->getId());
        $passwordHash = $class->execute($actionForm->getPassword());

        // アプリケーションナンバーからタイプ取得
        $appNum = 1;
        if (!isBlankOrNull($actionForm->getAppNum()) && is_numeric($actionForm->getAppNum())) {
            $appNum = (int)$actionForm->getAppNum();
        }
        $config = $context->getConfig();
        $count = 0;
        $applicationType = '';
        foreach ($config['SHORT_CODE_MAP_LIST']['application_type'] as $k => $v) {
            $count++;
            if ($count === $appNum) {
                $applicationType = $k;
                break;
            }
        }

        //-----------------------------------------------
        // ユーザー確認
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT ";
        $sql .= "  mus_user_id  ";
        $sql .= " FROM mst_user";
        $sql .= " INNER JOIN mst_user_content ON mus_user_id  = muc_user_id";
        $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mus_delete_flag = '0'";
        $sql .= "   AND muc_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND mus_id_hash = ?";
        $sql .= "   AND mus_password_hash = ?";
        $sql .= "   AND mct_application_type = ?";

        $params   = array();
        $params[] = $idHash;
        $params[] = $passwordHash;
        $params[] = $applicationType;

        if (!isBlankOrNull($actionForm->getServerName())) {
            $sql .= "   AND mus_server_name = ?";
            $params[] = $actionForm->getServerName();
        }

        if (!isBlankOrNull($actionForm->getProductCode())) {
            $productCode = "'" . implode("','",explode(',',str_replace("'",'',str_replace('"','',$actionForm->getProductCode())))) . "'";
            $sql .= "   AND mpr_product_code IN ($productCode)";
        }

        $sql .= " LIMIT 1";

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $result = 'ok';
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $result;
    }
}
