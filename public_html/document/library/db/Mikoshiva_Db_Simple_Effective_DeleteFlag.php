<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <div class="system-message">
              <p>
                このクラスは Mikoshiva_Db_Simple_Abstract クラスを継承しています。<br>
                詳しい挙動は <a href="/document/library/db/Mikoshiva_Db_Simple.php">Mikoshiva_Db_Simple</a> を参照して下さい。
              </p>
            </div>


            <p>
                このクラスは <a href="/document/library/db/Mikoshiva_Db_Simple.php">Mikoshiva_Db_Simple</a> を拡張し、以下のメソッドの条件（WHERE）に強制的に delete_flag = 0 を追加します。<br>
                強制的に delete_flag = 0 を追加すること以外の挙動は <a href="/document/library/db/Mikoshiva_Db_Simple.php">Mikoshiva_Db_Simple</a> と同じですので動きの詳細は <a href="/document/library/db/Mikoshiva_Db_Simple.php">Mikoshiva_Db_Simple</a> を参照して下さい。
            </p>

            <h2>対象メソッド一覧</h2>
            <ol>
              <li><?php echo basename(__FILE__, '.php') ?>#Mikoshiva_Db_Simple#simpleOneSelect - 第二引数に与えられた条件を元に DB から値を 1 件取得します</a></li>
              <li><?php echo basename(__FILE__, '.php') ?>#Mikoshiva_Db_Simple#simpleOneSelectById - 指定された ID にヒットしたデータを返します</a></li>
              <li><?php echo basename(__FILE__, '.php') ?>#Mikoshiva_Db_Simple#simpleOneSelectByIdFromForm - 第二引数に与えられた配列・オブジェクトからプライマリーキーを見つけ出し、ヒットしたデータを POPO に詰めて返します</a></li>
              <li><?php echo basename(__FILE__, '.php') ?>#Mikoshiva_Db_Simple#simpleManySelect - 指定された条件にマッチしたデータを返します</a></li>
            </ol>





  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





























