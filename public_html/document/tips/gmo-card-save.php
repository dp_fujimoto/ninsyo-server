<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="GMO カード登録・更新">GMO カード登録・更新</h1>

            <p>GMO カード登録・更新</p>



            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <div class="system-message">
              <p>
                実行クラスは以下の場合をシステムエラーとみなして例外を投げます。<br>
                <span style="color: #bb0000">※GMO における決済の失敗（エラー）は false を返します。</span>
              </p>
              <ol>
                <li>必須の値がセットされていない場合</li>
                <li>GMO 決済 API への接続に失敗した場合</li>
                <li>何らかの理由でレスポンスが取得出来なかった場合</li>
              </ol>
            </div>

            <pre class="wiki"><?php
highlight_string('<?php

//-----------------------------------------------
//
// ■カード登録・更新サンプル
//
//------------------------------------------------
//----------------------------------------------
// 決済変更に必要な値を生成
//----------------------------------------------
// 入れ物クラスを生成
include_once \'Mikoshiva/Settlement/Popo/Input/Gmo/Card/Save.php\';
$inputPopo = null;
$inputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Card_Save(); // 実行クラスへ渡すための入れ物クラス（ POPO ）

// セット
$inputPopo->setMemberID(\'DP-0000053965-088\'); // 会員 ID
$inputPopo->setCardNo(\'4111111111111113\');    // カード番号
$inputPopo->setExpire(\'1112\');                // 有効期限




//----------------------------------------------
// 実行
//----------------------------------------------
// インスタンス生成
include_once \'Mikoshiva/Settlement/Execution/Gmo/Card/Save/Update/Run.php\';
$exec   = null;
$exec   = new Mikoshiva_Settlement_Execution_Gmo_Card_Save_Update_Run();
$ret    = $exec->execute($inputPopo);   // 実行
$output = $exec->getOutput(); // output オブジェクトが返る
dumper($ret);
dumper($output);
'); ?>
            </pre>


            <h2 id="Mikoshiva_Settlement_Popo_Input_Gmo_Card_Save">Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save（GMO へ送信する値を保持する POPO）</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>必須</th>
                <th>型</th>
                <th>桁</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>SiteID</td>
                <td>●</td>
                <td>CHAR</td>
                <td>13</td>
                <td>サイト ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>SitePass</td>
                <td>●</td>
                <td>CHAR</td>
                <td>20</td>
                <td>サイトパスワード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>MemberID</td>
                <td>●</td>
                <td>CHAR</td>
                <td>60</td>
                <td>サイト ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>SeqMode</td>
                <td><br></td>
                <td>CHAR</td>
                <td>1</td>
                <td>カード登録連番モード</td>
                <td>
                  以下のいずれかを設定<br>
                  ０：論理モード（デフォルト）<br>
                  １：物理モード
                </td>
              </tr>
              <tr>
                <td>CardSeq</td>
                <td>▲</td>
                <td>CHAR</td>
                <td>1</td>
                <td>カード登録連番</td>
                <td>
                  登録時は、何も入力しない。<br>
                  更新時は、ここに更新する値を設定。<br>
                  （※物理モードの場合は「最大4桁）
                </td>
              </tr>
              <tr>
                <td>DefaultFlag</td>
                <td><br></td>
                <td>CHAR</td>
                <td>1</td>
                <td>洗替・継続課金フラグ</td>
                <td>
                  以下のいずれかを設定<br>
                  ０：継続課金対象としない（デフォルト）<br>
                  １：継続課金対象とする
                </td>
              </tr>
              <tr>
                <td>CardName</td>
                <td><br></td>
                <td>CHAR</td>
                <td>10</td>
                <td>カード会社略称</td>
                <td><br></td>
              </tr>
              <tr>
                <td>CardNo</td>
                <td>●</td>
                <td>CHAR</td>
                <td>16</td>
                <td>カード番号</td>
                <td><br></td>
              </tr>
              <tr>
                <td>CardPass</td>
                <td><br></td>
                <td>CHAR</td>
                <td>20</td>
                <td>カードパスワード</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Expire</td>
                <td>●</td>
                <td>CHAR</td>
                <td>4</td>
                <td>有効期限</td>
                <td>（YYMM）形式</td>
              </tr>
              <tr>
                <td>HolderName</td>
                <td><br></td>
                <td>CHAR</td>
                <td>50</td>
                <td>名義人</td>
                <td><br></td>
              </tr>
            </table>





            <h2 id="Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save">Mikoshiva_Settlement_Gmo_Popo_Output_Card_Save（GMO 返却された値を保持する POPO）</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>桁</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>CardSeq</td>
                <td>CHAR</td>
                <td>1</td>
                <td>カード登録連番</td>
                <td>
                  モードにより返却内容が異なります。<br>
                  物理モードの場合は最大 4 桁
                </td>
              </tr>
              <tr>
                <td>CardNo</td>
                <td>CHAR</td>
                <td>16</td>
                <td>カード番号</td>
                <td>下 4 桁以外伏字</td>
              </tr>
              <tr>
                <td>Forward</td>
                <td>CHAR</td>
                <td>7</td>
                <td>仕向先コード</td>
                <td>有効性チェックを行った時の仕向先コード</td>
              </tr>
              <tr>
                <td>ErrCode</td>
                <td>CHAR</td>
                <td>3</td>
                <td>エラーコード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続された文字列
                </td>
              </tr>
              <tr>
                <td>ErrInfo</td>
                <td>CHAR</td>
                <td>9</td>
                <td>エラー詳細コード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続された文字列
                </td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















