<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * ステータス名のリストを作成します。
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2010/02/15
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Classes_StatusList
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
    * ステータス名のリストを作成します。
    *
    * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
    * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
    * @param Mikoshiva_Controller_Mapping_ActionContext $context
    * @return object
    */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {

        //-----------------------------------------------
        // ｽﾃｰﾀｽ名一覧作成
        //-----------------------------------------------
        $statusList = array();

        $temp = $context->getConfig();
        foreach ($temp['SHORT_CODE_MAP_LIST'] as $k => $v) {
            foreach ($v as $k2 => $v2) {
                $statusList[$k2] = $v2;
            }
        }

        return $statusList;
    }
}
