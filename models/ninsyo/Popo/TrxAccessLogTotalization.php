<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_access_log_totalization
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxAccessLogTotalization extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tat';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'accessLogTotalizationId' => null,
        'accessLogTotalizationType' => null,
        'accessDate' => null,
        'accessCount' => null,
        'userCount' => null,
        'memo' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tat_access_log_totalization_id の値を返します
	 *
     * @return int tat_access_log_totalization_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogTotalizationId() {
		return $this->_properties['accessLogTotalizationId'];
    }


    /**
     * tat_access_log_totalization_id の値をセットします
	 *
	 * @param int $accessLogTotalizationId tat_access_log_totalization_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogTotalizationId($accessLogTotalizationId) {
		$this->_properties['accessLogTotalizationId'] = $accessLogTotalizationId;
    }


    /**
     * tat_access_log_totalization_type の値を返します
	 *
     * @return varchar tat_access_log_totalization_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessLogTotalizationType() {
		return $this->_properties['accessLogTotalizationType'];
    }


    /**
     * tat_access_log_totalization_type の値をセットします
	 *
	 * @param varchar $accessLogTotalizationType tat_access_log_totalization_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessLogTotalizationType($accessLogTotalizationType) {
		$this->_properties['accessLogTotalizationType'] = $accessLogTotalizationType;
    }


    /**
     * tat_access_date の値を返します
	 *
     * @return date tat_access_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessDate() {
		return $this->_properties['accessDate'];
    }


    /**
     * tat_access_date の値をセットします
	 *
	 * @param date $accessDate tat_access_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessDate($accessDate) {
		$this->_properties['accessDate'] = $accessDate;
    }


    /**
     * tat_access_count の値を返します
	 *
     * @return int tat_access_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessCount() {
		return $this->_properties['accessCount'];
    }


    /**
     * tat_access_count の値をセットします
	 *
	 * @param int $accessCount tat_access_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessCount($accessCount) {
		$this->_properties['accessCount'] = $accessCount;
    }


    /**
     * tat_user_count の値を返します
	 *
     * @return int tat_user_count
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserCount() {
		return $this->_properties['userCount'];
    }


    /**
     * tat_user_count の値をセットします
	 *
	 * @param int $userCount tat_user_count
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserCount($userCount) {
		$this->_properties['userCount'] = $userCount;
    }


    /**
     * tat_memo の値を返します
	 *
     * @return text tat_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * tat_memo の値をセットします
	 *
	 * @param text $memo tat_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * tat_delete_flag の値を返します
	 *
     * @return char tat_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tat_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tat_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tat_deletion_datetime の値を返します
	 *
     * @return datetime tat_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tat_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tat_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tat_registration_datetime の値を返します
	 *
     * @return datetime tat_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tat_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tat_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tat_update_datetime の値を返します
	 *
     * @return datetime tat_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tat_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tat_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tat_update_timestamp の値を返します
	 *
     * @return timestamp tat_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tat_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tat_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























