<?php

/**
 * @see Zend_Registry
 */
require_once 'Zend/Registry.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/View/Helper/Exception.php';
Zend_Loader_Autoloader::autoload('Popo_MstAuthority');
 //'Popo/MstAuthority.php';

/**
 * 権限チェックを行います。
 *
 * @param array    $params
 *                      string  authName 権限名
 * @param Smarty   $smarty
 * @return boolean true|false
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: function.authCheck.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_authCheck($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['authName'] = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['authName']) ) {
        // メソッド終了ログを出力
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 authName に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $_authName = $data['authName'];
    //$_authName = preg_replace('#([A-Z])#', '_$1', $data['authName']);

    //$_authName = 'mau_' . $_authName . '_access_flag';
    $_authName = 'get' . ucfirst($_authName) . 'AccessFlag';
    $session   = new Zend_Session_Namespace('staff');
    //$_authName = strtolower($_authName);
    $mstAuthority = $session->mstAuthority;

    if (! method_exists($mstAuthority, $_authName)) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】与えられた引数の権限は存在しません。');
    }

    Mikoshiva_Logger::info(LOG_END);
    $ret = true;
    if ($mstAuthority->$_authName() !== '1') $rret = false;
    return $ret;

}

