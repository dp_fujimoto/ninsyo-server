<?php
$SHORT_CODE_MAP_LIST = array();

// application_typeは順番が重要なので、追加する場合は一番下に追加
$SHORT_CODE_MAP_LIST['application_type']['APPLICATION_TYPE_LIBRARY']                     = 'ライブラリー';
$SHORT_CODE_MAP_LIST['application_type']['APPLICATION_TYPE_EDGE_TRADER']                 = 'エッジトレーダー';
$SHORT_CODE_MAP_LIST['application_type']['APPLICATION_TYPE_WEB_PAGE_AUTH']               = 'WEBページ認証';

$SHORT_CODE_MAP_LIST['access_log_analysis_type']['ACCESS_LOG_ANALYSIS_TYPE_EDGE_TRADER']     = 'エッジトレーダー';

$SHORT_CODE_MAP_LIST['access_log_detail_type']['ACCESS_LOG_DETAIL_TYPE_COMSPREAD_CYCLE']     = '鞘取り＋サイクル理論';
$SHORT_CODE_MAP_LIST['access_log_detail_type']['ACCESS_LOG_DETAIL_TYPE_COMBASIC_CYCLE']      = 'サイクル理論';
$SHORT_CODE_MAP_LIST['access_log_detail_type']['ACCESS_LOG_DETAIL_TYPE_COMSPREAD']           = '鞘取り';
$SHORT_CODE_MAP_LIST['access_log_detail_type']['ACCESS_LOG_DETAIL_TYPE_COMBASIC']            = 'ベーシック';
$SHORT_CODE_MAP_LIST['access_log_detail_type']['ACCESS_LOG_DETAIL_TYPE_FAILURE']             = '失敗';