<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_shipping_mail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstShippingMail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'msm';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'shippingMailId' => null,
        'shippingId' => null,
        'directionStatusId' => null,
        'customerId' => null,
        'batchArgvDate' => null,
        'batchExecuteMikoshivaDatetime' => null,
        'batchExecuteRealDatetime' => null,
        'realSendDate' => null,
        'sendCompleteFlag' => null,
        'templateType' => null,
        'templateKey' => null,
        'mailType' => null,
        'fromName' => null,
        'fromEmail' => null,
        'toName' => null,
        'toEmail' => null,
        'subject' => null,
        'textBody' => null,
        'htmlBody' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 発送メールマスタID の値を返します
	 *
     * @return int 発送メールマスタID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingMailId() {
		return $this->_properties['shippingMailId'];
    }


    /**
     * 発送メールマスタID の値をセットします
	 *
	 * @param int $shippingMailId 発送メールマスタID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingMailId($shippingMailId) {
		$this->_properties['shippingMailId'] = $shippingMailId;
    }


    /**
     * 発送ID の値を返します
	 *
     * @return int 発送ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingId() {
		return $this->_properties['shippingId'];
    }


    /**
     * 発送ID の値をセットします
	 *
	 * @param int $shippingId 発送ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingId($shippingId) {
		$this->_properties['shippingId'] = $shippingId;
    }


    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * バッチ引数の日付 の値を返します
	 *
     * @return date バッチ引数の日付
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBatchArgvDate() {
		return $this->_properties['batchArgvDate'];
    }


    /**
     * バッチ引数の日付 の値をセットします
	 *
	 * @param date $batchArgvDate バッチ引数の日付
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBatchArgvDate($batchArgvDate) {
		$this->_properties['batchArgvDate'] = $batchArgvDate;
    }


    /**
     * msm_batch_execute_mikoshiva_datetime の値を返します
	 *
     * @return datetime msm_batch_execute_mikoshiva_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBatchExecuteMikoshivaDatetime() {
		return $this->_properties['batchExecuteMikoshivaDatetime'];
    }


    /**
     * msm_batch_execute_mikoshiva_datetime の値をセットします
	 *
	 * @param datetime $batchExecuteMikoshivaDatetime msm_batch_execute_mikoshiva_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBatchExecuteMikoshivaDatetime($batchExecuteMikoshivaDatetime) {
		$this->_properties['batchExecuteMikoshivaDatetime'] = $batchExecuteMikoshivaDatetime;
    }


    /**
     * バッチ実行日時（リアル日時） の値を返します
	 *
     * @return datetime バッチ実行日時（リアル日時）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getBatchExecuteRealDatetime() {
		return $this->_properties['batchExecuteRealDatetime'];
    }


    /**
     * バッチ実行日時（リアル日時） の値をセットします
	 *
	 * @param datetime $batchExecuteRealDatetime バッチ実行日時（リアル日時）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setBatchExecuteRealDatetime($batchExecuteRealDatetime) {
		$this->_properties['batchExecuteRealDatetime'] = $batchExecuteRealDatetime;
    }


    /**
     * 実メール送信日 の値を返します
	 *
     * @return date 実メール送信日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealSendDate() {
		return $this->_properties['realSendDate'];
    }


    /**
     * 実メール送信日 の値をセットします
	 *
	 * @param date $realSendDate 実メール送信日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealSendDate($realSendDate) {
		$this->_properties['realSendDate'] = $realSendDate;
    }


    /**
     * メール送信済フラグ の値を返します
	 *
     * @return char メール送信済フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSendCompleteFlag() {
		return $this->_properties['sendCompleteFlag'];
    }


    /**
     * メール送信済フラグ の値をセットします
	 *
	 * @param char $sendCompleteFlag メール送信済フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSendCompleteFlag($sendCompleteFlag) {
		$this->_properties['sendCompleteFlag'] = $sendCompleteFlag;
    }


    /**
     * テンプレートタイプ の値を返します
	 *
     * @return varchar テンプレートタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateType() {
		return $this->_properties['templateType'];
    }


    /**
     * テンプレートタイプ の値をセットします
	 *
	 * @param varchar $templateType テンプレートタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateType($templateType) {
		$this->_properties['templateType'] = $templateType;
    }


    /**
     * テンプレートキー の値を返します
	 *
     * @return varchar テンプレートキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateKey() {
		return $this->_properties['templateKey'];
    }


    /**
     * テンプレートキー の値をセットします
	 *
	 * @param varchar $templateKey テンプレートキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateKey($templateKey) {
		$this->_properties['templateKey'] = $templateKey;
    }


    /**
     * メールタイプ の値を返します
	 *
     * @return varchar メールタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMailType() {
		return $this->_properties['mailType'];
    }


    /**
     * メールタイプ の値をセットします
	 *
	 * @param varchar $mailType メールタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMailType($mailType) {
		$this->_properties['mailType'] = $mailType;
    }


    /**
     * 送信元名 (From) の値を返します
	 *
     * @return varchar 送信元名 (From)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromName() {
		return $this->_properties['fromName'];
    }


    /**
     * 送信元名 (From) の値をセットします
	 *
	 * @param varchar $fromName 送信元名 (From)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromName($fromName) {
		$this->_properties['fromName'] = $fromName;
    }


    /**
     * 送信元メールアドレス (From) の値を返します
	 *
     * @return varchar 送信元メールアドレス (From)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromEmail() {
		return $this->_properties['fromEmail'];
    }


    /**
     * 送信元メールアドレス (From) の値をセットします
	 *
	 * @param varchar $fromEmail 送信元メールアドレス (From)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromEmail($fromEmail) {
		$this->_properties['fromEmail'] = $fromEmail;
    }


    /**
     * 宛先名(To) の値を返します
	 *
     * @return varchar 宛先名(To)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToName() {
		return $this->_properties['toName'];
    }


    /**
     * 宛先名(To) の値をセットします
	 *
	 * @param varchar $toName 宛先名(To)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToName($toName) {
		$this->_properties['toName'] = $toName;
    }


    /**
     * 宛先メールアドレス (To) の値を返します
	 *
     * @return varchar 宛先メールアドレス (To)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToEmail() {
		return $this->_properties['toEmail'];
    }


    /**
     * 宛先メールアドレス (To) の値をセットします
	 *
	 * @param varchar $toEmail 宛先メールアドレス (To)
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToEmail($toEmail) {
		$this->_properties['toEmail'] = $toEmail;
    }


    /**
     * 件名 の値を返します
	 *
     * @return varchar 件名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubject() {
		return $this->_properties['subject'];
    }


    /**
     * 件名 の値をセットします
	 *
	 * @param varchar $subject 件名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubject($subject) {
		$this->_properties['subject'] = $subject;
    }


    /**
     * 本文（テキスト） の値を返します
	 *
     * @return mediumtext 本文（テキスト）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTextBody() {
		return $this->_properties['textBody'];
    }


    /**
     * 本文（テキスト） の値をセットします
	 *
	 * @param mediumtext $textBody 本文（テキスト）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTextBody($textBody) {
		$this->_properties['textBody'] = $textBody;
    }


    /**
     * 本文（HTML） の値を返します
	 *
     * @return mediumtext 本文（HTML）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHtmlBody() {
		return $this->_properties['htmlBody'];
    }


    /**
     * 本文（HTML） の値をセットします
	 *
	 * @param mediumtext $htmlBody 本文（HTML）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHtmlBody($htmlBody) {
		$this->_properties['htmlBody'] = $htmlBody;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























