<?php
/**
 * @package Mikoshiva_Validate
 */


/**
 * require
 */
require_once 'Zend/Validate/Abstract.php';


/**
 * @category Mikoshiva_Validate
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/10
 * @version SVN:$Id: Uri.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Uri extends Zend_Validate_Abstract
{

    const NOT_URL = 'notUrl';


    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_URL => "【%COLUMN_NAME%】 は URL として正しい形式ではありません。"
    );


    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid IP address
     *
     * @param mixed $value
     * @return boolean
     */
    public function isValid($value) {


        $valueString = (string) $value;

        $this->_setValue($valueString);

        if (!Zend_Uri::check($valueString)) {
            $this->_error(self::NOT_URL);
            return false;
        }

        return true;
    }

}