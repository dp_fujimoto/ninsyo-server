<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Regex
 */
require_once 'Zend/Validate/Regex.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Regex.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Regex extends Zend_Validate_Regex
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => "%COLUMN_NAME%はパターン%pattern%に合いません"
    );
}
