<?php
include '../api/define-api.php';

//------------------------------------------------------------------
// エッジトレーダー認証
//------------------------------------------------------------------
$url = '/api/user-content/get-edge-trade-url';
//reg_ID
//reg_PSW
//reg_KeyID

$logStr = date('Y-m-d H:i:s') . ' ';
foreach ($_POST as $k => $v) {
    $logStr .= $k . ':' . $v . ',';
}
$post = array();
$post['id'] = '';
if (isset($_POST['reg_ID'])) {
    $post['id'] = $_POST['reg_ID'];
}
$post['password'] = '';
if (isset($_POST['reg_PSW'])) {
    $post['password'] = $_POST['reg_PSW'];
}
$post['key'] = '';
if (isset($_POST['reg_KeyID'])) {
    $post['key'] = $_POST['reg_KeyID'];
}
try {
    file_put_contents(LOG_TEMPORARY_PATH . '/trade/authing_' . date('Y-m-d') . '.log',$logStr . "\n",FILE_APPEND);
} catch (Exception $e) {}
echo send_socket($url, $post, $_SERVER);
