<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_sequence
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstSequence extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mse';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'sequenceId' => null,
        'sequenceName' => null,
        'sequenceNumber' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * シーケンスマスタID の値を返します
	 *
     * @return int シーケンスマスタID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSequenceId() {
		return $this->_properties['sequenceId'];
    }


    /**
     * シーケンスマスタID の値をセットします
	 *
	 * @param int $sequenceId シーケンスマスタID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSequenceId($sequenceId) {
		$this->_properties['sequenceId'] = $sequenceId;
    }


    /**
     * シーケンス名 の値を返します
	 *
     * @return varchar シーケンス名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSequenceName() {
		return $this->_properties['sequenceName'];
    }


    /**
     * シーケンス名 の値をセットします
	 *
	 * @param varchar $sequenceName シーケンス名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSequenceName($sequenceName) {
		$this->_properties['sequenceName'] = $sequenceName;
    }


    /**
     * シーケンス番号 の値を返します
	 *
     * @return bigint シーケンス番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSequenceNumber() {
		return $this->_properties['sequenceNumber'];
    }


    /**
     * シーケンス番号 の値をセットします
	 *
	 * @param bigint $sequenceNumber シーケンス番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSequenceNumber($sequenceNumber) {
		$this->_properties['sequenceNumber'] = $sequenceNumber;
    }


}

























