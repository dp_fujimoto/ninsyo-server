<?php
/**
 * @package Mikoshiva_Utility
 */



/**
 * ブラウザ関連のユーティリティクラス
 *
 * 【方針】
 *     [2012-07-10]
 *          判別する OS は Android とiOS のみとします
 *          デバイスが携帯端末であるか、タブレット端末であるかをチェックします
 *
 * @package Mikoshiva_Utility
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/20
 * @version SVN:$Id: Device.php,v 1.2 2012/12/19 07:35:19 fujimoto Exp $
 */
class Mikoshiva_Utility_Device {

    /**
     * スマートデバイス全ての UserArgent の パターン配列
     * @var unknown_type
     */
    private static $_smartDeviceUserArgentPatternList = array(
                'iPod',    // Apple iPod touch
                'iPhone',  // Apple iPhone
                'iPad',    // Apple iPad touch
                'Android', // Android
            );

    /**
     * スマートデバイスの携帯端末のパターン配列
     * @var unknown_type
     */
    private static $_mobileDeviceUserArgentPatternList = array(
                'iPod',    // Apple iPod touch
                'iPhone',  // Apple iPhone
                'Android', // Android
            );

    /**
     * スマートデバイスのタブレット端末のパターン配列
     * @var unknown_type
     */
    private static $_tabletDeviceUserArgentPatternList = array(
                'iPad',    // Apple iPad touch
                'Android', // Android
            );


    /**
     * スマートデバイスであるかのチェックを行います
     *
     *
     */
    public static function isSmartDevice() {

        // スマートデバイスであるかをチェックします
        $pattern = '#' . implode('|', self::$_smartDeviceUserArgentPatternList) . '#i';
        if (!isset($_SERVER['HTTP_USER_AGENT']) || preg_match($pattern, $_SERVER['HTTP_USER_AGENT']) === 0) {
            return false;
        }
        return true;
    }

    /**
     * 携帯端末であるかのチェックを行います
     *
     *
     */
    public static function isMobileDevice() {

        // スマートデバイスであるかをチェックします
        $pattern = '#' . implode('|', self::$_mobileDeviceUserArgentPatternList) . '#i';
        if (!isset($_SERVER['HTTP_USER_AGENT']) || preg_match($pattern, $_SERVER['HTTP_USER_AGENT']) === 0) {
            return false;
        }

        // Android の場合だけ別途検証が必要
        if (preg_match('#Android#i', $_SERVER['HTTP_USER_AGENT']) === 1) {
            // Mobile が存在すれば携帯端末だと判定
            if (preg_match('#Mobile#i', $_SERVER['HTTP_USER_AGENT']) === 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * タブレット端末であるかのチェックを行います
     *
     *
     */
    public static function isTabletDevice() {

        // スマートデバイスであるかをチェックします
        $pattern = '#' . implode('|', self::$_tabletDeviceUserArgentPatternList) . '#i';
        if (!isset($_SERVER['HTTP_USER_AGENT']) || preg_match($pattern, $_SERVER['HTTP_USER_AGENT']) === 0) {
            return false;
        }

        // Android の場合だけ別途検証が必要
        if (preg_match('#Android#i', $_SERVER['HTTP_USER_AGENT']) === 1) {
            // Mobile が存在すれば携帯端末だと判定
            if (preg_match('#Mobile#i', $_SERVER['HTTP_USER_AGENT']) === 1) {
                return false;
            }
        }
        return true;
    }
}




































































