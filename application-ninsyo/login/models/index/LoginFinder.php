<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
include_once 'Mikoshiva/Utility/Copy.php';
require_once 'Popo/MstStaff.php';
require_once 'Popo/MstDivision.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Login_Models_Index_LoginFinder extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface &$actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $contex
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $contex) {


        // ここにビジネスロジック


        //モデル内で「$this->_result」に
        // action-mapping.ymlのforwardキー名を「セットすることにより
        // 明示的にモデルチェインを停止し
        // (このクラス後にチェインマッピングされているモデルクラスは実行されません)
        // forwardキー名の遷移先へ遷移することが出来ます。

        //$this->_result = 'failure';


        // リターンした値が自動的にviewにassignされます。
        // ※リターンは必ずオブジェクトかオブジェクト配列(連想は不可)にしてください。
        // nullを返した場合assignされません。

        //----------------------------------------------
        //
        // ■リクエストの値を取得
        //
        //----------------------------------------------
        $loginId         = $actionForm->get('loginId');
        $loginPassword   = $actionForm->get('loginPassword');



        //----------------------------------------------
        //
        // ■スタッフ情報を検索
        //
        //----------------------------------------------
        // SELECT
        $select = ''
                . 'SELECT * FROM mst_staff '
                . 'INNER JOIN mst_division '
                .         'ON mst_division_id = mdi_division_id '
                . 'WHERE mst_email = ? '
                . 'AND mst_login_password = ? '
                . 'AND mst_delete_flag = 0 '
                . '';

        // bind
        $bindList   = array();
        $bindList[0] = $actionForm->get('loginId');
        $bindList[1] = $actionForm->get('loginPassword');


        // 実行
        $rowList = array();
        $rowList = $this->_db->fetchAll($select, $bindList);


        // 取得出来なければエラー
        if (count($rowList) !== 1) {
            return null;
        }



        //----------------------------------------------
        //
        // ■スタッフ情報をセッションに詰める
        //
        //----------------------------------------------
        // セッションを生成
        $staff = new Zend_Session_Namespace('staff');


        $row   = $rowList[0];
        // ▼Popo_MstStaff -----------------------------------------------------------
        $mst             = array();
        $mst             = Mikoshiva_Utility_Array::extractBeginningOfPrefix('mst', $row);
        $staff->mstStaff = Mikoshiva_Utility_Copy::copyDbProperties(new Popo_MstStaff(), $mst);

        // パスワードをハッシュ化
        $staff->mstStaff->setLoginPassword(
            sha1($staff->mstStaff->getLoginPassword()
            . __FILE__)
        );


        // ▼Popo_MstDivision -----------------------------------------------------------
        $mdi                = array();
        $mdi                = Mikoshiva_Utility_Array::extractBeginningOfPrefix('mdi_', $row);
        $staff->mstDivision = Mikoshiva_Utility_Copy::copyDbProperties(new Popo_MstDivision(), $mdi);


        // ▼chageDivisionId -----------------------------------------------------------
        $staff->changeDivisionId = $staff->mstDivision->getDivisionId();


         // STAFF の有効期限を10時間に設定
         $staff->setExpirationSeconds( 60 * 60 * 10 );
         //$staff->setExpirationSeconds( 10 ); // 10 秒

        // STAFF を読み取り専用にする
        $staff->lock();

        //-------------------------------------
        // ■オペレーションログに書き込み
        //-------------------------------------
        /*****
        $mol = new Dao_MstOperationLog();
        $mol->staffLogRegister(
            Dao_MstOperationLog::NAME_LOGIN
        );
        *****/


        return null;
    }

}

























