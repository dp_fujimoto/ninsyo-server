<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1>デビット仮売上　→　実売上（即時売上）</h1>


            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <div class="system-message">
              <p>
                実行クラスは以下の場合をシステムエラーとみなして例外を投げます。<br>
                <span style="color: #bb0000">※GMO における決済の失敗（エラー）は false を返します。</span>
              </p>
              <ol>
                <li>必須の値がセットされていない場合</li>
                <li>GMO 決済 API への接続に失敗した場合</li>
                <li>何らかの理由でレスポンスが取得出来なかった場合</li>
              </ol>
            </div>

            <pre class="wiki"><?php
highlight_string('<?php

        //----------------------------------------------
        // 決済に必要なデータを作成
        //----------------------------------------------
        include_once \'Mikoshiva/Settlement/Popo/Input/Gmo/Alter/Debit.php\';
        $inputPopo = null;
        $inputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Alter_Debit();
        $inputPopo->setShopID(\'testpg0000153\');
        $inputPopo->setOrderID(\'DP-test173824-002-201006-19\');
        $inputPopo->setAmount(100);

        //----------------------------------------------
        // 実行
        //----------------------------------------------
        include_once \'Mikoshiva/Settlement/Execution/Gmo/Alter/Sales/Debit.php\';
        $alterSalesDebit = null;
        $alterSalesDebit = new Mikoshiva_Settlement_Execution_Gmo_Alter_Sales_Debit($inputPopo);

        $ret = null;
        $ret = $alterSalesDebit->execute(); // 実行

        dumper($ret);             // true: 決済成功|false: 決済失敗
        dumper($mm->getOutput()); // 決済結果 POPO ( Mikoshiva_Settlement_Gmo_Popo_Output_Alter が返る )
'); ?>
            </pre>


            <h2>Mikoshiva_Settlement_Popo_Input_Gmo_Alter_Debit（決済に必要な値を保持する POPO</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>仮売上　→　実売上処理に必須</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>ShopID</td>
                <td>●</td>
                <td>string</td>
                <td>ショップ ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>OrderID</td>
                <td>●</td>
                <td>string</td>
                <td>オーダー ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Amount</td>
                <td>●</td>
                <td>string</td>
                <td>金額</td>
                <td><br></td>
              </tr>
            </table>


            <h2>Mikoshiva_Settlement_Gmo_Popo_Output_Alter（処理結果を保持する POPO）</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>ErrCode</td>
                <td>string</td>
                <td>エラーコード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続さた文字列
              </td>
              </tr>
              <tr>
                <td>ErrInfo</td>
                <td>string</td>
                <td>エラー詳細コード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続さた文字列
              </td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















