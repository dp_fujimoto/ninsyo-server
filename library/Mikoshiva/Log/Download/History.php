<?php
require_once 'Mikoshiva/Date.php';
require_once 'Mikoshiva/Db/Simple.php';
require_once 'Mikoshiva/Utility/Integer.php';

require_once 'Popo/TrxFileDownloadHistory.php';
require_once 'Mikoshiva/Log/Download/Exception.php';
/**
 * Mikoshivaファイルダウンロード履歴
 *
 * @package Mikoshiva_File
 * @author fujimoto <fujimoto@d-publishing.jp>
 * @since 2011/03/16
 */
class Mikoshiva_Log_FileDownloadHistory {

    /**
     * メールテンプレートの情報を取得し、$fName を元にアラート or タスクの処理を行います
     *
     * @param string           $fileName            ファイル名
     * @param int|null(option) $dataNumber          データ数
     * @param string           $extractedCondition  抽出条件
     * @param string           $memo                メモ
     * @return true
     *
     * @author fujimoto <fujimoto@d-publishing.jp>
     * @since 2011/03/16
     */
    public function fileDownloadHistory(
            $fileName, $dataNumber = null, $extractedCondition = null, $memo = null) {

        // データの作成を確認します。
        if (isBlankOrNull($fileName)) {
            // 作成失敗
            $message = '【Mikoshiva_File_DownloadHistory#register】ファイル名エラー';
            throw new Mikoshiva_Log_FileDownloadHistory_Exception ($message);
        }
        if (isBlankOrNull($dataNumber)) {
            // 作成失敗
            $message = '【Mikoshiva_File_DownloadHistory#register】データ数エラー';
            throw new Mikoshiva_Log_FileDownloadHistory_Exception ($message);
        }


        //--------------------------------------------------------------------
        // レジストリ取得
        //--------------------------------------------------------------------
        $registry = null;
        $registry = Zend_Registry::getInstance();

        //--------------------------------------------------------------------
        // 初期化
        //--------------------------------------------------------------------
        // セッションの取得
        $staff = new Zend_Session_Namespace('staff');
        if($staff->mstStaff !== null) {
            /* @var $mstStaff Popo_MstStaff */
            $staffId  = $staff->mstStaff->getStaffId();
        } else {
            $staffId  = null;
        }
        $request    = $registry->get('request');
        $module     = $request->getModuleName();
        $controller = $request->getControllerName();
        $action     = $request->getActionName();
        $url        = "$module/$controller/$action";

        $modelName  = $registry->get('modelName');


        // Mikoshiva時間
        $now = Mikoshiva_Date::mikoshivaNow();


        //--------------------------------------------------------------------
        // DB オブジェクトを生成
        //--------------------------------------------------------------------
        $tfd = null;
        $tfd = new Popo_TrxFileDownloadHistory();

        $tfd->setFileDownloadHistoryId(null);                                   // ファイルダウンロード履歴ID
        $tfd->setStaffId($staffId);                                             // スタッフID（実行者）
        $tfd->setOperationUrl($url);                                            // オペレーションURL
        $tfd->setModelName($modelName);                                         // モデル名
        $tfd->setFileName($fileName);                                           // ファイル名
        $tfd->setDataNumber($dataNumber);                                       // データ数
        $tfd->setExtractedCondition($extractedCondition);                       // 抽出条件
        $tfd->setMemo($memo);                                                   // メモ
        $tfd->setDeleteFlag('0');                                               // 削除フラグ
        $tfd->setDeletionDatetime(null);                                        // 削除日時
        $tfd->setRegistrationDatetime($now);                                    // 登録日時
        $tfd->setUpdateDatetime($now);                                          // 更新日時
        $tfd->setUpdateTimestamp(null);                                         // システム更新日時


        $db = null;
        $db = new Mikoshiva_Db_Simple();
        $tfd = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_TrxFileDownloadHistory', $tfd);

        // データの作成を確認します。
        if (isBlankOrNull($tfd->getFileDownloadHistoryId())) {
            // 作成失敗
            $message = '【Mikoshiva_File_DownloadHistory#register】ファイルダウンロード履歴作成エラー';
            throw new Mikoshiva_Log_FileDownloadHistory_Exception ($message);
        }

        return true;
    }

}