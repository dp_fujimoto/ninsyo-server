<?php
/** @package Smarty */

/**
 * require
 */
require_once 'Zend/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';


/**
 * 文字列をハイフン（-）で分割し、指定した添字の値を返す。
 *
 * @param array     $params
 *                          string $str 文字列（電話番号・郵便番号など）
 *                          string $no  配列分割後の取得したい添字
 * @param $smarty
 * @return string
 * @author K.Sekiya <kurachi0223@gmail.com>
 * @since 2009/12/07
 * @version SVN:$Id:
 */
function smarty_function_haifunSplit($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);


    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['str'] = '';
    $default['no'] = '';
    
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;
    
    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['str']) ) {
        // メソッド終了ログを出力
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 str に値がありません。');
    }
    if(isBlankOrNull($data['no']) ) {
        // メソッド終了ログを出力
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 no に値がありません。');
    }
    
    $expArr = array();
    $expArr = explode('-',$data['str']);
    
    Mikoshiva_Logger::info(LOG_END);
    return $expArr[$data['no']];
}

?>
