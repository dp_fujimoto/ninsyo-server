<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_TrxDirectDevidedPayment
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_TrxDirectDevidedPayment extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'trx_direct_devided_payment';
    protected $_primary  = 'tdd_direct_devided_payment_id';
    protected $_metadata = array (
  'tdd_direct_devided_payment_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_direct_devided_payment_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'tdd_charge_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_charge_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_direct_devided_payment_number' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_direct_devided_payment_number',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_order_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_order_id',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '27',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_access_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_access_id',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '32',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_access_pass' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_access_pass',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '32',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_amount' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_amount',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_real_charge_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_real_charge_type',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_accounting_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_accounting_flag',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_charge_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_charge_date',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_charge_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_charge_flag',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_charge_error_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_charge_error_flag',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_recharge_order_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_recharge_order_id',
    'COLUMN_POSITION' => 13,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '27',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_recharge_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_recharge_type',
    'COLUMN_POSITION' => 14,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_recharge_due_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_recharge_due_date',
    'COLUMN_POSITION' => 15,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_recharge_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_recharge_date',
    'COLUMN_POSITION' => 16,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_recharge_error_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_recharge_error_flag',
    'COLUMN_POSITION' => 17,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_refund_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_refund_flag',
    'COLUMN_POSITION' => 18,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_error_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_error_type',
    'COLUMN_POSITION' => 19,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_error_code' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_error_code',
    'COLUMN_POSITION' => 20,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '3',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_error_info' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_error_info',
    'COLUMN_POSITION' => 21,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '9',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_delete_flag',
    'COLUMN_POSITION' => 22,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_deletion_datetime',
    'COLUMN_POSITION' => 23,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_registration_datetime',
    'COLUMN_POSITION' => 24,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_update_datetime',
    'COLUMN_POSITION' => 25,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tdd_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_direct_devided_payment',
    'COLUMN_NAME' => 'tdd_update_timestamp',
    'COLUMN_POSITION' => 26,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

