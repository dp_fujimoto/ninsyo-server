<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時  <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document">＜＜　戻る</a></p>

<?php
    // ▼new アイコン
    // <img src="/document/img/new.gif">
?>

        <ol>
          <?php
          //-----------------------------------------------------------------
          // ▼GMO 関連
          //-----------------------------------------------------------------
          ?>
          <li>▼GMO 関連
            <ol>
              <li><a href="/document/tips/gmo-cancel.php">GMO 決済変更関連 - 決済取キャンセル（取消・返品・月跨返品）</a></li>
              <li><a href="/document/tips/gmo-auth.php">仮売上　→　実売上</a></li>
              <li><a href="/document/tips/gmo-auth-debit.php">デビット仮売上　→　実売上（即時売上）</a></li>
              <li><a href="/document/tips/gmo-recharge.php">再請求（新たなクレジットカードで請求を掛ける）</a></li>
              <li><a href="/document/tips/gmo-card-save.php">GMO カード登録・更新</a></li>
              <li><a href="/document/tips/gmo-payment-change.php">支払い方法変更（会員登録～カード登録）</a></li>
              <li><a href="/document/tips/gmo-payment-order.php">注文（オーダー）</a></li>
              <li><a href="/document/tips/gmo-error-code.php">エラーコード・エラー詳細コードからエラーメッセージ（内容）を返す</a></li>
            </ol>
          </li>
          <?php
          //-----------------------------------------------------------------
          // ▼日付関連
          //-----------------------------------------------------------------
          ?>
          <li>▼日付関連
            <ol>
              <li><a href="/document/tips/date-add-sub.php">指定日時を取得する</a></li>
              <li><a href="/document/tips/date-card-compare.php">日付の比較</a></li>
              <li><a href="/document/tips/date-check.php">昨日・今日・明日・うるう年のチェック</a></li>
              <li><a href="/document/tips/date-end-of-month.php">指定された月末日を取得する</a></li>
            </ol>
          </li>
          <?php
          //-----------------------------------------------------------------
          // ▼エラー処理
          //-----------------------------------------------------------------
          ?>
          <li>▼エラー処理
            <ol>
              <li><a href="/document/tips/error-error-illegal.php">不正な処理が行われた際などにシステムエラー以外のシステム共通のエラー画面に繊維させたい。</a></li>
            </ol>
          </li>
          <?php
          //-----------------------------------------------------------------
          // ▼メール関連
          //-----------------------------------------------------------------
          ?>
          <li>▼メール関連
            <ol>
              <li><a href="/document/tips/mail-normal.php">普通にメールを送信したい</a> <img src="/document/img/new.gif"></li>
              <li><a href="/document/tips/mail-db-auto-set.php">DB からテンプレートを取得して自動でセットしたい</a> <img src="/document/img/new.gif"></li>
              <li><a href="/document/tips/mail-attachment.php">添付メールを送信したい</a> <img src="/document/img/new.gif"></li>
            </ol>
          </li>
          <?php
          //-----------------------------------------------------------------
          // ▼その他
          //-----------------------------------------------------------------
          ?>
          <li>▼ログ関連
            <ol>
              <li><a href="/document/tips/log-devel.php">運用ログとは別に、開発用のログを出力したい</a></li>
            </ol>
          </li>
          <?php
          //-----------------------------------------------------------------
          // ▼その他
          //-----------------------------------------------------------------
          ?>
          <li>▼その他
            <ol>
              <li><a href="/document/tips/etc-model-validate.php">モデルの中でバリデート処理を行いたい</a></li>
              <li><a href="/document/tips/zip-to-address.php">郵便番号から住所を検索して自動入力したい</a></li>
              <li><a href="/document/tips/format-encode.php">配列をCSVにしたい</a></li>
              <li><a href="/document/tips/format-decode.php">CSVを配列にしたい</a></li>
            </ol>
          </li>
        </ol>

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>




































