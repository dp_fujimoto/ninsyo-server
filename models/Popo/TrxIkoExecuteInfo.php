<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_iko_execute_info
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxIkoExecuteInfo extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tie';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'ikoExecuteInfoId' => null,
        'ikoType' => null,
        'ikoBeforTableName' => null,
        'ikoBeforTablePrimaryId' => null,
        'ikoAfterTableName' => null,
        'ikoAfterTablePrimaryId' => null,
        'ikoExecuteStatus' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'updateDatetime' => null,
        'registrationDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 移行実行情報ID の値を返します
	 *
     * @return int 移行実行情報ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoExecuteInfoId() {
		return $this->_properties['ikoExecuteInfoId'];
    }


    /**
     * 移行実行情報ID の値をセットします
	 *
	 * @param int $ikoExecuteInfoId 移行実行情報ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoExecuteInfoId($ikoExecuteInfoId) {
		$this->_properties['ikoExecuteInfoId'] = $ikoExecuteInfoId;
    }


    /**
     * 移行タイプ の値を返します
	 *
     * @return varchar 移行タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoType() {
		return $this->_properties['ikoType'];
    }


    /**
     * 移行タイプ の値をセットします
	 *
	 * @param varchar $ikoType 移行タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoType($ikoType) {
		$this->_properties['ikoType'] = $ikoType;
    }


    /**
     * 移行元テーブル名 の値を返します
	 *
     * @return varchar 移行元テーブル名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoBeforTableName() {
		return $this->_properties['ikoBeforTableName'];
    }


    /**
     * 移行元テーブル名 の値をセットします
	 *
	 * @param varchar $ikoBeforTableName 移行元テーブル名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoBeforTableName($ikoBeforTableName) {
		$this->_properties['ikoBeforTableName'] = $ikoBeforTableName;
    }


    /**
     * 移行元テーブルプライマリーID の値を返します
	 *
     * @return int 移行元テーブルプライマリーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoBeforTablePrimaryId() {
		return $this->_properties['ikoBeforTablePrimaryId'];
    }


    /**
     * 移行元テーブルプライマリーID の値をセットします
	 *
	 * @param int $ikoBeforTablePrimaryId 移行元テーブルプライマリーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoBeforTablePrimaryId($ikoBeforTablePrimaryId) {
		$this->_properties['ikoBeforTablePrimaryId'] = $ikoBeforTablePrimaryId;
    }


    /**
     * 移行先テーブル名 の値を返します
	 *
     * @return varchar 移行先テーブル名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoAfterTableName() {
		return $this->_properties['ikoAfterTableName'];
    }


    /**
     * 移行先テーブル名 の値をセットします
	 *
	 * @param varchar $ikoAfterTableName 移行先テーブル名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoAfterTableName($ikoAfterTableName) {
		$this->_properties['ikoAfterTableName'] = $ikoAfterTableName;
    }


    /**
     * 移行先テーブルプライマリーID の値を返します
	 *
     * @return int 移行先テーブルプライマリーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoAfterTablePrimaryId() {
		return $this->_properties['ikoAfterTablePrimaryId'];
    }


    /**
     * 移行先テーブルプライマリーID の値をセットします
	 *
	 * @param int $ikoAfterTablePrimaryId 移行先テーブルプライマリーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoAfterTablePrimaryId($ikoAfterTablePrimaryId) {
		$this->_properties['ikoAfterTablePrimaryId'] = $ikoAfterTablePrimaryId;
    }


    /**
     * 移行実行ステータス の値を返します
	 *
     * @return varchar 移行実行ステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIkoExecuteStatus() {
		return $this->_properties['ikoExecuteStatus'];
    }


    /**
     * 移行実行ステータス の値をセットします
	 *
	 * @param varchar $ikoExecuteStatus 移行実行ステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIkoExecuteStatus($ikoExecuteStatus) {
		$this->_properties['ikoExecuteStatus'] = $ikoExecuteStatus;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























