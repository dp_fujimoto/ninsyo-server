<?php
require_once 'Mikoshiva/Controller/Action/Admin/Abstract.php';

/**
 * 商品を管理するコントローラークラスです。
 *
 * @author      K.fujimoto <fujimoto@d-publishing.jp>
 * @since       2012/06/25
 * @version     SVN: $Id: ProductController.php,v 1.1 2012/09/21 07:08:31 fujimoto Exp $
 */
class Product_ProductController
    extends Mikoshiva_Controller_Action_Admin_Abstract {

}

