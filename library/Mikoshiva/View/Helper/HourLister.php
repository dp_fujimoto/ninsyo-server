<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';
require_once 'Zend/View/Interface.php';
require_once 'Zend/Validate/Date.php';

 /**
  * 年のリストを作成します
  *
  * @package Mikoshiva_View
  * @author T.Tsukasa <taniguchi@kaihatsu.com>
  * @since 2009/12/16
  * @version SVN:$Id: HourLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
  */
class Mikoshiva_View_Helper_HourLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;

    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function hourLister($name, $selected = null, $attribs = null)
    {
        $hourList = array();
        $hourList['']  = '';
        $hourList['01'] = '01';
        $hourList['02'] = '02';
        $hourList['03'] = '03';
        $hourList['04'] = '04';
        $hourList['05'] = '05';
        $hourList['06'] = '06';
        $hourList['07'] = '07';
        $hourList['08'] = '08';
        $hourList['09'] = '09';
        $hourList['10'] = '10';
        $hourList['11'] = '11';
        $hourList['12'] = '12';
        $hourList['13'] = '13';
        $hourList['14'] = '14';
        $hourList['15'] = '15';
        $hourList['16'] = '16';
        $hourList['17'] = '17';
        $hourList['18'] = '18';
        $hourList['19'] = '19';
        $hourList['20'] = '20';
        $hourList['21'] = '21';
        $hourList['22'] = '22';
        $hourList['23'] = '23';
        $hourList['24'] = '24';

        // ◆selected 2009/12/24 00:00:00 で来た場合にも対応
        $_selected = $selected;
        if (isset($_selected)) {
            $_selected = str_replace('/', '-', $_selected );
            $validator1 = new Zend_Validate_Date('Y-m-d H:i:s');
            if ($validator1->isValid($_selected)) {

                $matche = array();
                preg_match('#^\d{4}-\d{2}-\d{2} (\d{2}):\d{2}:\d{2}#', $_selected, $matche);
                $_selected = $matche[1];
            }
        }

        return $this->view->formSelect($name,
                                       $_selected,
                                       $attribs,
                                       $hourList);
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
