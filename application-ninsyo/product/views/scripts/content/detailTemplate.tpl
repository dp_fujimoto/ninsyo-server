<span class="COM_bold">【コンテンツについて】</span>
<br>

<table class="COM_table COM_tableWidthLong">
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            ID
        </td>
        <td class="COM_nowrap">{if !isBlankOrNull($form->getContentId())}{$form->getContentId()}{else}新規登録中{/if}</td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            商品名
        </td>
        <td class="COM_nowrap">
            {dbWrite arClassName='Mikoshiva_Db_NinsyoAr_MstProduct' pkey=$form->getProductId() column=array('productName')}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            用途
        </td>
        <td class="COM_nowrap">
            {statusMapping code=$form->getApplicationType()}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            コンテンツ名
        </td>
        <td class="COM_nowrap">
            {$form->getContentName()}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            公開日
        </td>
        <td class="COM_nowrap">
            {$form->getPublicationDate()}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            メモ
        </td>
        <td class="COM_nowrap">
            <pre>{$form->getMemo()}</pre>
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            テスト中フラグ
        </td>
        <td class="COM_nowrap">
            {if $form->getTestFlag() === '1'}テスト中であり、一般公開しない{else}一般公開中{/if}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
            状態
        </td>
        <td class="COM_nowrap">
            {if $form->getDeleteFlag() === '1'}削除{else}通常{/if}
        </td>
    </tr>
</table>

{if $form->getApplicationType() === 'APPLICATION_TYPE_LIBRARY'}
    <br>
    <span class="COM_bold">【用途：ライブラリー用】</span>

    <table class="COM_table COM_tableWidthLong">
        <tr>
            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                コンテンツ画像URL
            </td>
            <td class="COM_nowrap">
                {$form->getImageUrl()}
                {if !isBlankOrNull($form->getImageUrl())}
                    {if $form->getImageNoneFlag() !== '1'}
                        {if $form->getImageUpdateFlag() === '1'}<br><p class="COM_bold COM_fontRed">URLを変更、またはURLにある画像データを変更した</p>{/if}
                    {/if}
                    <br><img src="{$form->getImageUrl()}" width="{$smarty.const.CONTENT_IMAGE_W}px" height="{$smarty.const.CONTENT_IMAGE_H}px">
                {/if}
            </td>
        </tr>
        <tr>
            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                メディアタイプ
            </td>
            <td class="COM_nowrap">
                {$form->getMediaType()|strtoupper}
            </td>
        </tr>
        <tr>
            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                メディアURL
            </td>
            <td class="COM_nowrap">
                {$form->getMediaUrl()}
                {if !isBlankOrNull($form->getMediaUrl())}
                    {if $form->getMediaNoneFlag() !== '1'}
                        {if $form->getMediaUpdateFlag() === '1'}<br><p class="COM_bold COM_fontRed">URLを変更、またはURLにある画像データを変更した</p>{/if}
                    {/if}
                    <br><a href="{$form->getMediaUrl()}" target="_blank">メディア確認</a>
                {/if}
            </td>
        </tr>
        <tr>
            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                管理名
            </td>
            <td class="COM_nowrap">
                {$form->getConfirmName()}
            </td>
        </tr>
        <tr>
            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                注釈
            </td>
            <td class="COM_nowrap">
                {$form->getDescription()}
            </td>
        </tr>
    </table>
{/if}

<br>
<hr>
<br>

<span class="COM_bold">【閲覧条件について】</span>
<br>

{$conditionNo = $form->getConditionNo()}
{$productCode = $form->getProductCode()}
{$serverName = $form->getServerName()}
{$designatedDate = $form->getDesignatedDate()}
{$packageNumber = $form->getPackageNumber()}
{$productShippingCode = $form->getProductShippingCode()}
{$cancelOkFlag = $form->getCancelOkFlag()}
{$permissionMonth = $form->getPermissionMonth()}
{$permissionDay = $form->getPermissionDay()}
{$needPeriodMonth = $form->getNeedPeriodMonth()}
{$needPeriodDay = $form->getNeedPeriodDay()}
{$campaignCode = $form->getCampaignCode()}
{$campaignSalesCode = $form->getCampaignSalesCode()}
{$subscriptionType = $form->getSubscriptionType()}
{$productType = $form->getProductType()}
{$chargeType = $form->getChargeType()}


<table class="COM_table">
    <tr>
        <td class="COM_bgColorLight COM_nowrap">
            判定条件１
        </td>
        <td class="COM_nowrap">
            {if $form->getAllConditionFlag() !== '1'}設定した条件の<span class="COM_fontRed">どれか</span>に該当すれば、閲覧できる
            {elseif $form->getAllConditionFlag() === '1'}設定した条件の<span class="COM_fontRed">全て</span>に該当すれば、閲覧できる
            {/if}
        </td>
    </tr>
    {if $form->getApplicationType() === 'APPLICATION_TYPE_WEB_PAGE_AUTH'}
    <tr>
        <td class="COM_bgColorLight COM_nowrap">
            判定条件２
        </td>
        <td class="COM_nowrap">
            {if $form->getAvailableCheckFlag() !== '1'}指定なし
            {elseif $form->getAvailableCheckFlag() === '1'}請求取消分・商品金額以上の返金がある分はサービス対象外とする
            {/if}
        </td>
    </tr>
    <tr>
        <td class="COM_bgColorLight COM_nowrap">
            判定条件３
        </td>
        <td class="COM_nowrap">
            {if $form->getAuthFlag() !== '1'}サービス期間中のみ閲覧可能
            {elseif $form->getAuthFlag() === '1'}サービス期間終了後も閲覧可能
            {/if}
        </td>
    </tr>
    {/if}
</table>

<br>

<table class="COM_table">
    <tr>
        <td class="COM_bgColorLight COM_nowrap">
            NO
        </td>
        <td class="COM_bgColorLight COM_nowrap">
            パターン
        </td>
        <td class="COM_bgColorLight COM_nowrap">
            共通条件
        </td>
        <td class="COM_bgColorLight COM_nowrap">
            パターン別条件
        </td>
    </tr>

    {foreach from=$conditionNo key="k" item="v"}
        {if $v != 0}
        <tr>
            <td class="COM_nowrap">
                {$k + 1}
            </td>
            <td class="COM_nowrap">
                {if $v == 0}この条件を使用しない{/if}
                {if $v == 1}指定日以前に購入していること{/if}
                {if $v == 2}指定日にサービスINしていた(いる）こと{/if}
                {if $v == 3}購読中であること{/if}
                {if $v == 4}一括購読中であること{/if}
                {if $v == 5}購入したことがあること{/if}
                {if $v == 6}指定した発送物を持っていること{/if}
                {if $v == 9}無条件に見れる{/if}
            </td>
            <td class="COM_nowrap" >
                <div class="{if $v == 0 || $v == 9}COM_displayNone{/if}">
                    <table class="COM_tableBorderZero">
                        <tr>
                            <td class="COM_nowrap">
                                サーバー名：
                            </td>
                            <td class="COM_nowrap">
                                {$serverName[$k]}
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap" colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap">
                                商品コード：
                            </td>
                            <td class="COM_nowrap">
                                {$productCode[$k]}
                            </td>
                        </tr>
                        {if !isBlankOrNull($campaignCode[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    オーダーフォームコード：
                                </td>
                                <td class="COM_nowrap">
                                    {$campaignCode[$k]}
                                </td>
                            </tr>
                        {/if}
                        {if !isBlankOrNull($campaignSalesCode[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    キャンペーンコード：
                                </td>
                                <td class="COM_nowrap">
                                    {$campaignSalesCode[$k]}
                                </td>
                            </tr>
                        {/if}
                        {if !isBlankOrNull($productShippingCode[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    商品発送コード：
                                </td>
                                <td class="COM_nowrap">
                                    {$productShippingCode[$k]}
                                </td>
                            </tr>
                        {/if}
                        {if !isBlankOrNull($packageNumber[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    パッケージナンバー：
                                </td>
                                <td class="COM_nowrap">
                                    {$packageNumber[$k]}
                                </td>
                            </tr>
                        {/if}
                        {if !isBlankOrNull($subscriptionType[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    購読タイプ：
                                </td>
                                <td class="COM_nowrap">
                                    {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_NORMAL'}通常{/if}
                                    {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_YEAR'}年間{/if}
                                    {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_YEAR_CONTINUITY'}年間追加{/if}
                                    {if $subscriptionType[$k] === 'SUBSCRIPTION_TYPE_MICRO_CONTINUITY_UPGRADE'}マイクロアップグレード{/if}
                                    {if $subscriptionType[$k] === 'VIP'}VIP{/if}
                                </td>
                            </tr>
                        {/if}
                        {if !isBlankOrNull($productType[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    商品タイプ：
                                </td>
                                <td class="COM_nowrap">
                                    {if $productType[$k] === 'PRODUCT_TYPE_SINGLE'}単発{/if}
                                    {if $productType[$k] === 'PRODUCT_TYPE_MICRO_CONTINUITY'}マイクロコンティニュイティ{/if}
                                    {if $productType[$k] === 'PRODUCT_TYPE_CONTINUITY'}コンティニュイティ{/if}
                                </td>
                            </tr>
                        {/if}
                        {if !isBlankOrNull($chargeType[$k])}
                            <tr>
                                <td class="COM_nowrap">
                                    請求タイプ：
                                </td>
                                <td class="COM_nowrap">
                                    {if $chargeType[$k] === 'CHARGE_TYPE_CARD'}カード{/if}
                                    {if $chargeType[$k] === 'CHARGE_TYPE_CASH_ON_DELIVERY'}代引き{/if}
                                    {if $chargeType[$k] === 'CHARGE_TYPE_BANK_TRANSFER'}銀行振込{/if}
                                </td>
                            </tr>
                        {/if}
                    </table>
                </div>
            </td>
            <td class="COM_nowrap">
                <div class="{if $v == 1 || $v == 2}{else}COM_displayNone{/if}">
                    <table class="COM_tableBorderZero">
                        <tr>
                            <td class="COM_nowrap">
                                指定日：
                            </td>
                            <td class="COM_nowrap">
                                {$designatedDate[$k]}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="{if $v == 5}{else}COM_displayNone{/if}">
                    <table class="COM_tableBorderZero">
                        <tr>
                            <td class="COM_nowrap">
                                解約した時：
                            </td>
                            <td class="COM_nowrap">
                                {if $cancelOkFlag[$k] === '1'}サービス継続{else}サービス終了{/if}
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap" colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_nowrap">
                                サービス継続期間：
                            </td>
                            <td class="COM_nowrap">
                                {if !isBlankOrNull($permissionMonth[$k])}{$permissionMonth[$k]}ヶ月{/if}
                                {if !isBlankOrNull($permissionDay[$k])}{$permissionDay[$k]}日{/if}
                                {if isBlankOrNull($permissionMonth[$k]) && isBlankOrNull($permissionDay[$k])}期間なし{/if}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="{if $v == 5 || $v == 3}{else}COM_displayNone{/if}">
                    <table class="COM_tableBorderZero">
                        <tr>
                            <td class="COM_nowrap">
                                必要サービス期間：
                            </td>
                            <td class="COM_nowrap">
                                {if !isBlankOrNull($needPeriodMonth[$k])}{$needPeriodMonth[$k]}ヶ月{/if}
                                {if !isBlankOrNull($needPeriodDay[$k])}{$needPeriodDay[$k]}日{/if}
                                {if isBlankOrNull($needPeriodMonth[$k]) && isBlankOrNull($needPeriodDay[$k])}期間なし{/if}
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        {/if}
    {/foreach}
</table>