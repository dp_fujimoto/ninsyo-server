<?php
// プログレスバー関連クラス
require_once 'Zend/ProgressBar.php';
require_once 'Zend/ProgressBar/Adapter/Console.php';
require_once 'batch/models/classes/ReportManager.php';

/**
 * レポート情報を管理し処理するクラス
 *
 * @author fujimoto <fujimoto@d-publishing.jp>
 * @since  2012/01/18
 */
class Batch_Models_Classes_ReportManagerWithProgressBar extends Batch_Models_Classes_ReportManager {

    /**
     * 自身のオブジェクト
     */
    private static $_instance = null;

    /**
     * プログレスバー
     */
    private static $_progresBarAdapter = null;
    private $_nowProgres = '';

    /**
     * 結果格納用
     */
    private $_startProcessTime  = '';
    private $_beforeProcessTime = '';
    private $_nowProcessTime    = '';

    /**
     * このクラスのインスタンスを取得する
     * ※singleton
     *
     */
    public static function getInstance() {

        if (isBlankOrNull(self::$_instance) === true) {
            self::$_instance = new self();
            self::$_instance->setStockProcessTimeStart('__TOTAL__');

            // プログレスバーアダプタ
            self::$_progresBarAdapter = new Zend_ProgressBar_Adapter_Console(); // アダプタの作成
            self::$_progresBarAdapter->setTextWidth(40);
            self::$_progresBarAdapter->setElements(array(
                Zend_ProgressBar_Adapter_Console::ELEMENT_PERCENT,
                Zend_ProgressBar_Adapter_Console::ELEMENT_BAR,
                Zend_ProgressBar_Adapter_Console::ELEMENT_ETA,
                Zend_ProgressBar_Adapter_Console::ELEMENT_TEXT)
            );
        }

        return self::$_instance;
    }

    /**
     *
     *    処理時間計測用microTime取得
     *
     */
    private function _getMicrotime(){
        list($msec, $sec) = explode(" ", microtime());
        return ((float)$sec + (float)$msec);
    }

    /**
     *
     *    処理時間計測用microTime初期化
     *
     */
    public function resetMicrotime(){
        // 処理開始時間 計測用
        $this->_startProcessTime  = self::_getMicrotime();
        $this->_beforeProcessTime = $this->_startProcessTime;
        $this->_nowProcessTime    = $this->_beforeProcessTime;
    }

    /**
     *
     *    処理時間計測結果メッセージ取得
     *
     */
    public function getMicrotimeMessage(){
        $this->_beforeProcessTime = $this->_nowProcessTime;

        $this->_nowProcessTime = $this->_getMicrotime();
        return number_format($this->_nowProcessTime - $this->_startProcessTime,4) . '秒　（1つ前との差：' . number_format($this->_nowProcessTime - $this->_beforeProcessTime,4)  . '秒）';
    }


    /**
     *
     *    プログラスバー更新
     *
     */
    public function updateProgresBar($progressbar, $str){
        $this->_nowProgres += 1;
        $progressbar->update($this->_nowProgres, $this->_nowProgres . '_' . $str);
    }

    /**
     *
     *    プログレスバー作成
     *
     */
    public function setProgresBar($count, $str){
        // 現在値初期化
        $this->_nowProgres = 0;

        // プログレスバー作成 開始時に現在値を１にするため、最大値は+1してセットする
        $progressbar = new Zend_ProgressBar(self::$_progresBarAdapter, $this->_nowProgres, $count + 1);
        $this->updateProgresBar($progressbar, $str);

        return $progressbar;
    }
}



















































