<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_content
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstContent extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mct';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'contentId' => null,
        'productId' => null,
        'applicationType' => null,
        'contentName' => null,
        'confirmName' => null,
        'imageUrl' => null,
        'imageUpdateDatetime' => null,
        'mediaUrl' => null,
        'mediaType' => null,
        'mediaUpdateDatetime' => null,
        'publicationDate' => null,
        'description' => null,
        'memo' => null,
        'availableCheckFlag' => null,
        'allConditionFlag' => null,
        'authFlag' => null,
        'testFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * mct_content_id の値を返します
	 *
     * @return int mct_content_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContentId() {
		return $this->_properties['contentId'];
    }


    /**
     * mct_content_id の値をセットします
	 *
	 * @param int $contentId mct_content_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContentId($contentId) {
		$this->_properties['contentId'] = $contentId;
    }


    /**
     * mct_product_id の値を返します
	 *
     * @return int mct_product_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * mct_product_id の値をセットします
	 *
	 * @param int $productId mct_product_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * mct_application_type の値を返します
	 *
     * @return varchar mct_application_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApplicationType() {
		return $this->_properties['applicationType'];
    }


    /**
     * mct_application_type の値をセットします
	 *
	 * @param varchar $applicationType mct_application_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApplicationType($applicationType) {
		$this->_properties['applicationType'] = $applicationType;
    }


    /**
     * mct_content_name の値を返します
	 *
     * @return varchar mct_content_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContentName() {
		return $this->_properties['contentName'];
    }


    /**
     * mct_content_name の値をセットします
	 *
	 * @param varchar $contentName mct_content_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContentName($contentName) {
		$this->_properties['contentName'] = $contentName;
    }


    /**
     * mct_confirm_name の値を返します
	 *
     * @return varchar mct_confirm_name
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getConfirmName() {
		return $this->_properties['confirmName'];
    }


    /**
     * mct_confirm_name の値をセットします
	 *
	 * @param varchar $confirmName mct_confirm_name
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setConfirmName($confirmName) {
		$this->_properties['confirmName'] = $confirmName;
    }


    /**
     * mct_image_url の値を返します
	 *
     * @return varchar mct_image_url
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImageUrl() {
		return $this->_properties['imageUrl'];
    }


    /**
     * mct_image_url の値をセットします
	 *
	 * @param varchar $imageUrl mct_image_url
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImageUrl($imageUrl) {
		$this->_properties['imageUrl'] = $imageUrl;
    }


    /**
     * mct_image_update_datetime の値を返します
	 *
     * @return datetime mct_image_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getImageUpdateDatetime() {
		return $this->_properties['imageUpdateDatetime'];
    }


    /**
     * mct_image_update_datetime の値をセットします
	 *
	 * @param datetime $imageUpdateDatetime mct_image_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setImageUpdateDatetime($imageUpdateDatetime) {
		$this->_properties['imageUpdateDatetime'] = $imageUpdateDatetime;
    }


    /**
     * mct_media_url の値を返します
	 *
     * @return varchar mct_media_url
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMediaUrl() {
		return $this->_properties['mediaUrl'];
    }


    /**
     * mct_media_url の値をセットします
	 *
	 * @param varchar $mediaUrl mct_media_url
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMediaUrl($mediaUrl) {
		$this->_properties['mediaUrl'] = $mediaUrl;
    }


    /**
     * mct_media_type の値を返します
	 *
     * @return varchar mct_media_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMediaType() {
		return $this->_properties['mediaType'];
    }


    /**
     * mct_media_type の値をセットします
	 *
	 * @param varchar $mediaType mct_media_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMediaType($mediaType) {
		$this->_properties['mediaType'] = $mediaType;
    }


    /**
     * mct_media_update_datetime の値を返します
	 *
     * @return datetime mct_media_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMediaUpdateDatetime() {
		return $this->_properties['mediaUpdateDatetime'];
    }


    /**
     * mct_media_update_datetime の値をセットします
	 *
	 * @param datetime $mediaUpdateDatetime mct_media_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMediaUpdateDatetime($mediaUpdateDatetime) {
		$this->_properties['mediaUpdateDatetime'] = $mediaUpdateDatetime;
    }


    /**
     * mct_publication_date の値を返します
	 *
     * @return date mct_publication_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPublicationDate() {
		return $this->_properties['publicationDate'];
    }


    /**
     * mct_publication_date の値をセットします
	 *
	 * @param date $publicationDate mct_publication_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPublicationDate($publicationDate) {
		$this->_properties['publicationDate'] = $publicationDate;
    }


    /**
     * mct_description の値を返します
	 *
     * @return text mct_description
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDescription() {
		return $this->_properties['description'];
    }


    /**
     * mct_description の値をセットします
	 *
	 * @param text $description mct_description
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDescription($description) {
		$this->_properties['description'] = $description;
    }


    /**
     * mct_memo の値を返します
	 *
     * @return text mct_memo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemo() {
		return $this->_properties['memo'];
    }


    /**
     * mct_memo の値をセットします
	 *
	 * @param text $memo mct_memo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemo($memo) {
		$this->_properties['memo'] = $memo;
    }


    /**
     * mct_available_check_flag の値を返します
	 *
     * @return char mct_available_check_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAvailableCheckFlag() {
		return $this->_properties['availableCheckFlag'];
    }


    /**
     * mct_available_check_flag の値をセットします
	 *
	 * @param char $availableCheckFlag mct_available_check_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAvailableCheckFlag($availableCheckFlag) {
		$this->_properties['availableCheckFlag'] = $availableCheckFlag;
    }


    /**
     * mct_all_condition_flag の値を返します
	 *
     * @return char mct_all_condition_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAllConditionFlag() {
		return $this->_properties['allConditionFlag'];
    }


    /**
     * mct_all_condition_flag の値をセットします
	 *
	 * @param char $allConditionFlag mct_all_condition_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAllConditionFlag($allConditionFlag) {
		$this->_properties['allConditionFlag'] = $allConditionFlag;
    }


    /**
     * mct_auth_flag の値を返します
	 *
     * @return char mct_auth_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAuthFlag() {
		return $this->_properties['authFlag'];
    }


    /**
     * mct_auth_flag の値をセットします
	 *
	 * @param char $authFlag mct_auth_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAuthFlag($authFlag) {
		$this->_properties['authFlag'] = $authFlag;
    }


    /**
     * mct_test_flag の値を返します
	 *
     * @return char mct_test_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTestFlag() {
		return $this->_properties['testFlag'];
    }


    /**
     * mct_test_flag の値をセットします
	 *
	 * @param char $testFlag mct_test_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTestFlag($testFlag) {
		$this->_properties['testFlag'] = $testFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























