<?php
	require_once './init.php';



	error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[case:CONFIRM]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

	if ($_SESSION['email'] !== sha1($_POST['email'])) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:セッションのメールアドレスと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

		$campaignSalesCode = $_SESSION['campaignSalesCode'];
		// セッション初期化
		session_unset();
        header('Location: ../mikoshiva/'.$campaignSalesCode);
	} else {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[else:セッションのメールアドレスと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

		$_SESSION['confirmFlag'] = '1';
		$_POST['ipAddress'] = $_SERVER["REMOTE_ADDR"];
		// mikoshivaにpost
        $response = $agent->getResponseByPost($basePath.'confirm', $_POST);
        echo $response;
	}
    error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
