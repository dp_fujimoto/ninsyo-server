
/**
 * 返金方法をが選択された際に返金金額を変更します
 *
 * @param int     form_refundAmount ActionForm の金額
 * @param int     totalAmount       商品金額＋送料・手数料
 * @param int     productAmount     商品金額
 * @param string  refundType        返金タイプのステータスコード
 * @param boolean formFlag
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/06/14
 * @version SVN:$Id: cancellation.php,v 1.1 2012/09/21 07:08:02 fujimoto Exp $
 */
function CCL_RecommendedToRefund(form_refundAmount, totalAmount, productAmount, refundType, formFlag) {

    // ▼金額を初期化
    var formAmount  = form_refundAmount; // ActionForm の金額
    var totalAmount = totalAmount;       // 商品金額＋送料・手数料
    var amount      = productAmount;     // 商品金額
    var viewAmount  = 0;                 // 表示金額

    // ▼返金タイプによって金額を変更する
    switch(refundType) {
        case 'REFUND_TYPE_PAYMENTGATEWAY': // PG 返金
        case 'REFUND_TYPE_MONTH':          // 月次返金
        case 'REFUND_TYPE_GMO':          // 月次返金
            viewAmount = totalAmount;
            $('#refundAmount').attr('readonly', 'readonly');
            break;

        default:
            viewAmount = amount;
            $('#refundAmount').removeAttr('readonly');
    }

    if (formAmount !== '' && formAmount !== null && formFlag === true) {
        viewAmount = formAmount;
    }

    $('#refundAmount').val(viewAmount);
}