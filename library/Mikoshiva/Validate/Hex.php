<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Hex
 */
require_once 'Zend/Validate/Hex.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Hex.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Hex extends Zend_Validate_Hex
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_HEX => "%COLUMN_NAME%は16進文字列以外を含みます"
    );
}
