<?php
/**
 * @package Mikoshiva_Mail
 */

/**
 * require
 */
require_once 'Zend/Mail.php';
require_once 'Zend/Service/Amazon/S3.php';
require_once 'Zend/Mime.php';
require_once 'Mikoshiva/Utility/Mapping.php';
require_once 'Mikoshiva/Utility/Convert.php';


/**
 * メール送信クラス
 *
 * @package Mikoshiva_Mail
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/10
 * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Mail extends Zend_Mail {






    /**
     * コンストラクタ
     *
     * @param string $charset 文字コード
     */
    public function __construct($charset = 'ISO-2022-JP') {
        parent::__construct($charset);
        $this->setReturnPath('error@mikoshiva.com'); // add 谷口司 2010-12-27 緊急対応に付き固定で入れる
    }


    /**
     * 渡されたパスからファイルを読み込み添付ファイルをセットします
     *
     * <test>
     * $case   = array();
     * $case[] = 'C:\eclipse-miko2\workspace\mikoshiva\public_html\img\loading.gif';
     * $case[] = 'C:\eclipse-miko2\workspace\mikoshiva\public_html\img\loading.gi';
     * </test>
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/10
     * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param string $filePath
     * @return Zend_Mime_Part Newly created Zend_Mime_Part object (to allow
     * advanced settings)
     */
    public function readingAttachment($filePath, $_filename = null) {

        // ファイルの存在チェック
        if (!file_exists($filePath)) {
            include_once 'Mikoshiva/Mail/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】”{$filePath}” が見つかりません";
            logDebug($message);
            throw new Mikoshiva_Mail_Exception($message);
        }

        // ファイルの読み込み
        $fileData = '';
        $fileData = file_get_contents($filePath);

        // MIME タイプを取得
        $mimeType = Zend_Service_Amazon_S3::getMimeType($filePath);

        // ファイル名
        $fileName = (isBlankOrNull($_filename) === false)
                  ? $_filename
                  : basename($filePath);



        // createAttachment に委譲
        return $this->createAttachment($fileData,
                                       $mimeType,
                                       Zend_Mime::DISPOSITION_ATTACHMENT,
                                       Zend_Mime::ENCODING_BASE64,
                                       $fileName);
    }


    /**
     * ファイルを添付します
     *
     * @see Zend_Mail::createAttachment()
     */
    public function createAttachment($body,
                                     $mimeType    = Zend_Mime::TYPE_OCTETSTREAM,
                                     $disposition = Zend_Mime::DISPOSITION_ATTACHMENT,
                                     $encoding    = Zend_Mime::ENCODING_BASE64,
                                     $filename    = null) {

        $filename = mb_convert_encoding($filename, 'ISO-2022-JP', 'UTF-8');

        return parent::createAttachment($body, $mimeType, $disposition, $encoding, $filename);
    }

    /**
     * 件名を置換処理を行った後セットします
     *
     * @param   string    $subject
     * @return  Zend_Mail Provides fluent interface
     * @throws  Zend_Mail_Exception
     */
    public function setSubjectReplace($subject, array $replacementList = array()) {

        return $this->setSubject(
            Mikoshiva_Utility_Convert::templateReplacer($subject, $replacementList)
        );
    }



    /**
     * 本文を置換処理を行った後セットします（テキスト）
     *
     * <test>
     * 'あかさたな'
     * 'あかさたな', array('かさ', 'たな'), array('棚')
     * 'あかさたな', array('たな'),         array('傘', '棚')
     * 'あかさたな', array('かさ', 'たな'), array('傘', '棚')
     * </test>
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/10
     * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param string $body メール本文
     * @param array $serchList 置換前リスト
     * @param array $repaceList 置換後リスト
     * @return Mikoshiva_Mail
     */
    public function setBodyTextReplace(      $body,
                                       array $replacementList = array(),
                                             $charset         = null,
                                             $encoding        = Zend_Mime::ENCODING_QUOTEDPRINTABLE) {

        // セット
        return $this->setBodyText(
                   Mikoshiva_Utility_Convert::templateReplacer($body, $replacementList),
                   $charset,
                   $encoding
        );
    }



    /**
     * 本文を置換処理を行った後セットします（HTML）
     *
     * <test>
     * 'あかさたな'
     * 'あかさたな', array('かさ', 'たな'), array('棚')
     * 'あかさたな', array('たな'),         array('傘', '棚')
     * 'あかさたな', array('かさ', 'たな'), array('傘', '棚')
     * </test>
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/10
     * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     *
     * @param string $body メール本文
     * @param array $serchList 置換前リスト
     * @param array $repaceList 置換後リスト
     * @return Mikoshiva_Mail
     */
    public function setBodyHtmlReplace(      $body,
                                       array $replacementList = array(),
                                             $charset         = null,
                                             $encoding        = Zend_Mime::ENCODING_QUOTEDPRINTABLE) {

        // セット
        return $this->setBodyHtml(
                   Mikoshiva_Utility_Convert::templateReplacer($body, $replacementList),
                   $charset,
                   $encoding
        );
    }







    /**
     * ID を使用しメールテンプレートの情報を取得します
     *
     * @param string $tableName
     * @param int    $id
     * @param array  $replaceList
     * @return void
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/23
     * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setMailTemplateById($tableName, $id, array $replaceList = array()) {

        $this->_setMailTemplate('ID', $tableName, $id, $replaceList);
    }


    /**
     * メールキーを使用しメールテンプレートの情報を取得します
     *
     * @param string $tableName
     * @param string $key
     * @param array  $replaceList
     * @return void
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/23
     * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setMailTemplateByKey($tableName, $key, array $replaceList = array()) {

        $this->_setMailTemplate('KEY', $tableName, $key, $replaceList);
    }


    /**
     * 実行メソッド
     *
     * @param string     $mode
     * @param string     $tableName
     * @param int|string $value
     * @param array      $replaceList
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/23
     * @version SVN:$Id: Mail.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _setMailTemplate($mode, $tableName, $value, $replaceList) {


        //------------------------------------
        // ■Ar のクラス名を生成
        //------------------------------------
        $arClassName = 'Mikoshiva_Db_Ar_' .  $tableName;


        //------------------------------------
        // ■実行
        //------------------------------------
        $template = null;

        $simple = new Mikoshiva_Db_Simple();
        switch ($mode) {

            case 'ID':
                $template = $simple->simpleOneSelectById($arClassName, $value);
                break;

            case 'KEY':
                $template = $simple->simpleOneSelect($arClassName, array('templateKey' => $value));
                break;

            default:
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】指定されたメールテンプレートの取得方法”{$mode}”には対応していません。";
                logDebug($message);
                throw new Mikoshiva_Exception($message);
        }


        //------------------------------------
        // ■テンプレート取得の状態確認
        //------------------------------------
        // プライマリキーの値を確認します
        require_once str_replace('_', '/', $arClassName) . '.php';

        // ARのインスタンス生成
        $objAr = null;
        $objAr = new $arClassName();

        // プライマリキーを取得
        $primaryList = $objAr->info('primary');

        // プライマリキーのカラム名からプロパティ名を取得
        $primaryColumnName = Mikoshiva_Utility_Convert::columnNameToVariableName($primaryList[1]);

        // POPOの全プロパティを取得
        $popoProperties = $template->getProperties();

        if (! isset($popoProperties[$primaryColumnName])
                || isBlankOrNull($popoProperties[$primaryColumnName])) {
            include_once 'Mikoshiva/Mail/Exception.php';
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】指定されたメールテンプレートは存在していません。";
            throw new Mikoshiva_Mail_Exception($message);
        }


        //------------------------------------
        // ■POPO を生成
        //------------------------------------
        /* @var $popoClass Popo_MstMailTemplate */
        $popoClassName = 'Popo_' .  $tableName;
        $popoClass     = new $popoClassName();
        Mikoshiva_Utility_Copy::copyToPopo($popoClass, $template);


        //------------------------------------
        // ■POPO を生成
        //------------------------------------
        $replaceList[] = $popoClass;


        //------------------------------------
        // ■Mikoshiva_Mail にセット
        //------------------------------------
        // ▼FROM
        $this->setFrom($popoClass->getFromEmail(), $popoClass->getFromName());

        // ▼件名
        $this->setSubjectReplace($popoClass->getSubject(), $replaceList);

        // ▼本文
        switch ($popoClass->getMailType()) {

            case 'MAIL_TYPE_TEXT':
                $this->setBodyTextReplace($popoClass->getTextBody(), $replaceList);
                break;

            case 'MAIL_TYPE_HTML':
                $this->setBodyHtmlReplace($popoClass->getHtmlBody(), $replaceList);
                break;

            case 'MAIL_TYPE_MULTI':
                $this->setBodyTextReplace($popoClass->getTextBody(), $replaceList);
                $this->setBodyHtmlReplace($popoClass->getHtmlBody(), $replaceList);
                break;

            default:
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】指定された送信方法”" . $popoClass->getMailType() . "”には対応していません。";
                logDebug($message);
                throw new Mikoshiva_Exception($message);
        }
       // $this->set

    }
































//-------------------------------------------
//
// ◆override
//   主にエンコード処理して親のメソッドを呼び出しています
//
//-------------------------------------------

    /**
     * エンコード処理を行い本文をセットします（テキスト）
     *
     * @param  string $txt
     * @param  string $charset
     * @param  string $encoding
     * @return Zend_Mail Provides fluent interface
    */
    public function setBodyText($txt, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        // UTF-8 から 置換
        $_txt = $txt;
        mb_convert_variables($this->_charset, 'UTF-8', $_txt);


        return parent::setBodyText($_txt, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE);
    }


    /**
     * エンコード処理を行い本文をセットします（HTML）
     *
     * @param  string $txt
     * @param  string $charset
     * @param  string $encoding
     * @return Zend_Mail Provides fluent interface
    */
    public function setBodyHtml($html, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        // 文字コードを変換
        $_html = $html;
        mb_convert_variables($this->_charset, 'UTF-8', $_html);


        return parent::setBodyHtml($_html, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE);
    }


    /**
     * エンコード処理を行い件名をセットします
     *
     * @param   string    $subject
     * @return  Zend_Mail Provides fluent interface
     * @throws  Zend_Mail_Exception
     */
    public function setSubject($subject)
    {
        // 文字コードを変換
        $_subject = $subject;
        mb_convert_variables($this->_charset, 'UTF-8', $_subject);


        return parent::setSubject($_subject);
    }


    /**
     * エンコード処理行い送信先を追加します
     *
     * @param  string $email
     * @param  string $name
     * @return Zend_Mail Provides fluent interface
     */
    public function addTo($email, $name = '') {

        // 文字コードを変換
        $_name = $name;
        if (! isBlankOrNull($_name)) {
            $_name = mb_encode_mimeheader(mb_convert_encoding($_name, 'JIS', 'UTF-8'));
            //mb_convert_variables($this->_charset, 'UTF-8', $_name);
        }
        return parent::addTo($email, $_name);
    }


    /**
     * エンコード処理行い送信先を追加します
     *
     * @param  string $email
     * @param  string $name
     * @return Zend_Mail Provides fluent interface
     */
    public function addToMultiple($emails) {
        $emailArray = array();
        $emailArray = explode ('|', $emails);
        foreach ($emailArray as $email) {
            parent::addTo($email);
        }
    }


    /**
     * エンコード処理を行い Cc を追加します
     *
     * @param  string    $email
     * @param  string    $name
     * @return Zend_Mail Provides fluent interface
     */
    public function addCc($email, $name = '') {

        // 文字コードを変換
        $_name = $name;
        if (! isBlankOrNull($_name)) {
            $_name = mb_encode_mimeheader(mb_convert_encoding($_name, 'JIS', 'UTF-8'));
            //mb_convert_variables($this->_charset, 'UTF-8', $_name);
        }
        return parent::addCc($email, $_name);
    }


    /**
     * エンコード処理を行い From をセットします
     *
     * @param  string    $email
     * @param  string    $name
     * @return Zend_Mail Provides fluent interface
     * @throws Zend_Mail_Exception if called subsequent times
     */
    public function setFrom($email, $name = null) {

        // 文字コードを変換
        $_name = $name;
        if (! isBlankOrNull($_name)) {
            $_name = mb_encode_mimeheader(
                $_name,
                $this->_charset,
                'B',
                "\n"
            );
            //mb_convert_variables($this->_charset, 'UTF-8', $_name);
        }
        return parent::setFrom($email, $_name);
    }


    /**
     * エンコード処理を行い Reply-To をセットします
     *
     * @param string $email
     * @param string $name
     * @return Zend_Mail
     */
    public function setReplyTo($email, $name = null) {

        // 文字コードを変換
        $_name = $name;
        if (! isBlankOrNull($_name)) {
            $_name = mb_encode_mimeheader(mb_convert_encoding($_name, 'JIS', 'UTF-8'));
            //mb_convert_variables($this->_charset, 'UTF-8', $_name);
        }
        return parent::setReplyTo($email, $_name);
    }

	/**
	 * 送信メールをDB登録する際のmessageIdを作成します。
	 * ここで作成されたmessageIdをMailオブジェクトにセットしてください。
	 *
	 * 引数$arName,$fromEmailのいずれかがnullまたは空文字の場合
	 * Zend_MailのcreateMessageIdで作成したメッセージIDがセットされます。
	 *
	 * @param string $arName
	 * @param string $fromEmail
	 * @return string $messageId
	 * @author k.shimowaki
	 */
    public function registMessageId($arName = null,$fromEmail = null) {

    	$ret = '';

		if (isBlankOrNull($arName) || isBlankOrNull($fromEmail)) {

    		$this->setMessageId(parent::createMessageId());
			return $this->getMessageId();
		}
        // Ar のインスタンスを作成
        $ar = null;

        $path = '';
        $path = str_replace('_', '/', $arName) . '.php';

        require_once $path;

        $ar = new $arName();

        // プレフィックスを取得
        $primaryList = $ar->info('primary');
        $prefix      = '';
        $prefix      = preg_replace('/^(.*?)_.*$/', '${1}', $primaryList[1]);

    	$ret = time() . '_' . Mikoshiva_Utility_String::createRandomString(8) . '_' . $prefix . '_' .$fromEmail;

		$this->setMessageId($ret);

		return $this->getMessageId();
    }
}


















