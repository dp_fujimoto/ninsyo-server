<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';

/**
 * 新しいタブを開く javascript を返します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: NewTab.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_NewTab extends Mikoshiva_View_Helper_Abstract {

    /**
     * 新しいタブを開く javascript を返します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/07
     * @version SVN:$Id: NewTab.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     * @param string $url 遷移先 URL
     * @param string $storage タブ情報を記録するか
     * @param string $formId フォームの ID 名
     * @return string
     */
    public function newTab($url, $storage = false, $formId = null ) {

        $_storage = $storage ? 'true' : 'false';

        if ($formId === null ) {
            return 'KO_newTab(\'' . $url . '\', ' . $_storage . ');';
        } else {
            return 'KO_newTab(\'' . $url . '\', ' . $_storage . ', \''  . $formId . '\');';
        }
    }
}
