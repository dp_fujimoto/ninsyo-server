<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstUserContentForce.php';
require_once 'tool/models/Operation.php';

/**
 * コンテンツ情報の削除を行う。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class User_Models_Content_Update
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * コンテンツ情報の削除を行う。
     *
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        if (!isBlankOrNull($actionForm->getUserContentId()) && is_numeric($actionForm->getUserContentId())) {

            // 更新（シンプルクラス）
            $db = null;
            $db = new Mikoshiva_Db_Simple();

            if ($actionForm->getMode() === 'startDate' || $actionForm->getMode() === 'endDate') {

                $message = '';
                $where = array();

                if ($actionForm->getMode() === 'startDate') {
                    $where['userContentId'] = $actionForm->getUserContentId();
                    $where['serviceStartDate'] = $actionForm->getStartDate();

                    $dateArr = explode('-',$actionForm->getStartDate());
                    if (strlen($actionForm->getStartDate()) !== 10 && count($dateArr) !== 3 || checkdate($dateArr[1],$dateArr[2],$dateArr[0]) !== true) {
                        $message = '入力エラー';
                    }
                }
                if ($actionForm->getMode() === 'endDate') {

                    $where['userContentId'] = $actionForm->getUserContentId();
                    $where['serviceEndDate'] = $actionForm->getEndDate();
                    if (isBlankOrNull($actionForm->getEndDate())) {
                        $where['serviceEndDate'] = null;
                        $where['authFlag'] = '1';

                    } else {
                        $where['authFlag'] = '0';

                        $dateArr = explode('-',$actionForm->getEndDate());
                        if (strlen($actionForm->getEndDate()) !== 10 && count($dateArr) !== 3 || checkdate($dateArr[1],$dateArr[2],$dateArr[0]) !== true) {
                            $message = '入力エラー';
                        }
                    }
                }

                if (isBlankOrNull($message)) {
                    $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContent', $where);
                    $message = Mikoshiva_Date::mikoshivaNow() . ' 更新完了';

                    //-----------------------------------------------
                    // アクションログ保存
                    //-----------------------------------------------
                    $logText = '';
                    $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

                    $operation = new Tool_Models_Operation();
                    $operation->insertActionLog($logText);
                }

                echo '<br><span class="COM_fontRed">' . $message . '</span>';


            } else if ($actionForm->getMode() === 'forceStartDate' || $actionForm->getMode() === 'forceEndDate') {

                $message = '';
                $where = array();

                if ($actionForm->getMode() === 'forceStartDate') {
                    $where['userContentId'] = $actionForm->getUserContentId();
                    $where['startDate'] = $actionForm->getStartDate();

                    if (!isBlankOrNull($actionForm->getStartDate())) {
                        $dateArr = explode('-',$actionForm->getStartDate());
                        if (strlen($actionForm->getStartDate()) !== 10 && count($dateArr) !== 3 || checkdate($dateArr[1],$dateArr[2],$dateArr[0]) !== true) {
                            $message = '入力エラー';
                        }
                    } else {
                        $where['startDate'] = null;
                    }
                }
                if ($actionForm->getMode() === 'forceEndDate') {

                    $where['userContentId'] = $actionForm->getUserContentId();
                    $where['endDate'] = $actionForm->getEndDate();

                    if (!isBlankOrNull($actionForm->getEndDate())) {
                        $dateArr = explode('-',$actionForm->getEndDate());
                        if (strlen($actionForm->getEndDate()) !== 10 && count($dateArr) !== 3 || checkdate($dateArr[1],$dateArr[2],$dateArr[0]) !== true) {
                            $message = '入力エラー';
                        }
                    } else {
                        $where['endDate'] = null;
                    }
                }

                if (isBlankOrNull($message)) {
                    $popo = new Popo_MstUserContentForce();

                    $popo->setUserContentId($actionForm->getUserContentId());

                    $mucf = new Popo_MstUserContentForce();
                    $mucf = $db->simpleOneSelect('Mikoshiva_Db_NinsyoAr_MstUserContentForce', $popo);

                    if (!isBlankOrNull($mucf->getUserContentForceId())) {
                        $where['userContentForceId'] = $mucf->getUserContentForceId();
                    }


                    $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_MstUserContentForce', $where);
                    $message = Mikoshiva_Date::mikoshivaNow() . ' 更新完了';

                    //-----------------------------------------------
                    // アクションログ保存
                    //-----------------------------------------------
                    $logText = '';
                    $logText .= html_entity_decode(dumper($actionForm->toArrayProperty(),null,false));

                    $operation = new Tool_Models_Operation();
                    $operation->insertActionLog($logText);
                }

                echo '<br><span class="COM_fontRed">' . $message . '</span>';
            }
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
