<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_zip_code
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstZipCode extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mzc';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'zipCodeId' => null,
        'zipCode' => null,
        'prefectureNameKana' => null,
        'address1Kana' => null,
        'address2Kana' => null,
        'prefectureName' => null,
        'address1' => null,
        'address2' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 住所マスタID の値を返します
	 *
     * @return int 住所マスタID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZipCodeId() {
		return $this->_properties['zipCodeId'];
    }


    /**
     * 住所マスタID の値をセットします
	 *
	 * @param int $zipCodeId 住所マスタID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZipCodeId($zipCodeId) {
		$this->_properties['zipCodeId'] = $zipCodeId;
    }


    /**
     * 郵便番号 の値を返します
	 *
     * @return char 郵便番号
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getZipCode() {
		return $this->_properties['zipCode'];
    }


    /**
     * 郵便番号 の値をセットします
	 *
	 * @param char $zipCode 郵便番号
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setZipCode($zipCode) {
		$this->_properties['zipCode'] = $zipCode;
    }


    /**
     * 都道府県名（カナ） の値を返します
	 *
     * @return varchar 都道府県名（カナ）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefectureNameKana() {
		return $this->_properties['prefectureNameKana'];
    }


    /**
     * 都道府県名（カナ） の値をセットします
	 *
	 * @param varchar $prefectureNameKana 都道府県名（カナ）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefectureNameKana($prefectureNameKana) {
		$this->_properties['prefectureNameKana'] = $prefectureNameKana;
    }


    /**
     * 住所１（カナ） の値を返します
	 *
     * @return varchar 住所１（カナ）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1Kana() {
		return $this->_properties['address1Kana'];
    }


    /**
     * 住所１（カナ） の値をセットします
	 *
	 * @param varchar $address1Kana 住所１（カナ）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1Kana($address1Kana) {
		$this->_properties['address1Kana'] = $address1Kana;
    }


    /**
     * 住所２（カナ） の値を返します
	 *
     * @return varchar 住所２（カナ）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2Kana() {
		return $this->_properties['address2Kana'];
    }


    /**
     * 住所２（カナ） の値をセットします
	 *
	 * @param varchar $address2Kana 住所２（カナ）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2Kana($address2Kana) {
		$this->_properties['address2Kana'] = $address2Kana;
    }


    /**
     * 都道府県名 の値を返します
	 *
     * @return varchar 都道府県名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPrefectureName() {
		return $this->_properties['prefectureName'];
    }


    /**
     * 都道府県名 の値をセットします
	 *
	 * @param varchar $prefectureName 都道府県名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPrefectureName($prefectureName) {
		$this->_properties['prefectureName'] = $prefectureName;
    }


    /**
     * 住所１ の値を返します
	 *
     * @return varchar 住所１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress1() {
		return $this->_properties['address1'];
    }


    /**
     * 住所１ の値をセットします
	 *
	 * @param varchar $address1 住所１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress1($address1) {
		$this->_properties['address1'] = $address1;
    }


    /**
     * 住所２ の値を返します
	 *
     * @return varchar 住所２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAddress2() {
		return $this->_properties['address2'];
    }


    /**
     * 住所２ の値をセットします
	 *
	 * @param varchar $address2 住所２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAddress2($address2) {
		$this->_properties['address2'] = $address2;
    }


}

























