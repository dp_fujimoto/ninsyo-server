<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_api_result
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxApiResult extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tar';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'apiResultId' => null,
        'apiManagerId' => null,
        'directionStatusId' => null,
        'apiActionStatus' => null,
        'value' => null,
        'retryNumber' => null,
        'alertFlag' => null,
        'firstExecutionDatetime' => null,
        'retryExecutionDatetime' => null,
        'completeFlag' => null,
        'completeDatetime' => null,
        'successFlag' => null,
        'resultMessage' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * API結果ID の値を返します
	 *
     * @return int API結果ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApiResultId() {
		return $this->_properties['apiResultId'];
    }


    /**
     * API結果ID の値をセットします
	 *
	 * @param int $apiResultId API結果ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApiResultId($apiResultId) {
		$this->_properties['apiResultId'] = $apiResultId;
    }


    /**
     * APIマネージャーID の値を返します
	 *
     * @return int APIマネージャーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApiManagerId() {
		return $this->_properties['apiManagerId'];
    }


    /**
     * APIマネージャーID の値をセットします
	 *
	 * @param int $apiManagerId APIマネージャーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApiManagerId($apiManagerId) {
		$this->_properties['apiManagerId'] = $apiManagerId;
    }


    /**
     * 指示ステータスID の値を返します
	 *
     * @return int 指示ステータスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectionStatusId() {
		return $this->_properties['directionStatusId'];
    }


    /**
     * 指示ステータスID の値をセットします
	 *
	 * @param int $directionStatusId 指示ステータスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectionStatusId($directionStatusId) {
		$this->_properties['directionStatusId'] = $directionStatusId;
    }


    /**
     * APIアクションステータス の値を返します
	 *
     * @return varchar APIアクションステータス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApiActionStatus() {
		return $this->_properties['apiActionStatus'];
    }


    /**
     * APIアクションステータス の値をセットします
	 *
	 * @param varchar $apiActionStatus APIアクションステータス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApiActionStatus($apiActionStatus) {
		$this->_properties['apiActionStatus'] = $apiActionStatus;
    }


    /**
     * 値 の値を返します
	 *
     * @return varchar 値
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getValue() {
		return $this->_properties['value'];
    }


    /**
     * 値 の値をセットします
	 *
	 * @param varchar $value 値
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setValue($value) {
		$this->_properties['value'] = $value;
    }


    /**
     * 再試行回数 の値を返します
	 *
     * @return int 再試行回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRetryNumber() {
		return $this->_properties['retryNumber'];
    }


    /**
     * 再試行回数 の値をセットします
	 *
	 * @param int $retryNumber 再試行回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRetryNumber($retryNumber) {
		$this->_properties['retryNumber'] = $retryNumber;
    }


    /**
     * アラートフラグ の値を返します
	 *
     * @return char アラートフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAlertFlag() {
		return $this->_properties['alertFlag'];
    }


    /**
     * アラートフラグ の値をセットします
	 *
	 * @param char $alertFlag アラートフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAlertFlag($alertFlag) {
		$this->_properties['alertFlag'] = $alertFlag;
    }


    /**
     * 初回実行日時 の値を返します
	 *
     * @return datetime 初回実行日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstExecutionDatetime() {
		return $this->_properties['firstExecutionDatetime'];
    }


    /**
     * 初回実行日時 の値をセットします
	 *
	 * @param datetime $firstExecutionDatetime 初回実行日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstExecutionDatetime($firstExecutionDatetime) {
		$this->_properties['firstExecutionDatetime'] = $firstExecutionDatetime;
    }


    /**
     * 再実行日時 の値を返します
	 *
     * @return datetime 再実行日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRetryExecutionDatetime() {
		return $this->_properties['retryExecutionDatetime'];
    }


    /**
     * 再実行日時 の値をセットします
	 *
	 * @param datetime $retryExecutionDatetime 再実行日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRetryExecutionDatetime($retryExecutionDatetime) {
		$this->_properties['retryExecutionDatetime'] = $retryExecutionDatetime;
    }


    /**
     * 完了フラグ の値を返します
	 *
     * @return char 完了フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompleteFlag() {
		return $this->_properties['completeFlag'];
    }


    /**
     * 完了フラグ の値をセットします
	 *
	 * @param char $completeFlag 完了フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompleteFlag($completeFlag) {
		$this->_properties['completeFlag'] = $completeFlag;
    }


    /**
     * 完了日時 の値を返します
	 *
     * @return datetime 完了日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCompleteDatetime() {
		return $this->_properties['completeDatetime'];
    }


    /**
     * 完了日時 の値をセットします
	 *
	 * @param datetime $completeDatetime 完了日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCompleteDatetime($completeDatetime) {
		$this->_properties['completeDatetime'] = $completeDatetime;
    }


    /**
     * API実行結果フラグ の値を返します
	 *
     * @return char API実行結果フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSuccessFlag() {
		return $this->_properties['successFlag'];
    }


    /**
     * API実行結果フラグ の値をセットします
	 *
	 * @param char $successFlag API実行結果フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSuccessFlag($successFlag) {
		$this->_properties['successFlag'] = $successFlag;
    }


    /**
     * API実行結果メッセージ の値を返します
	 *
     * @return text API実行結果メッセージ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getResultMessage() {
		return $this->_properties['resultMessage'];
    }


    /**
     * API実行結果メッセージ の値をセットします
	 *
	 * @param text $resultMessage API実行結果メッセージ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setResultMessage($resultMessage) {
		$this->_properties['resultMessage'] = $resultMessage;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























