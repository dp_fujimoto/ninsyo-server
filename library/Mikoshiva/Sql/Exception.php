<?php
/** @package Mikoshiva_Sql */

/**
 * require
 */
require_once 'Mikoshiva/Exception.php';


/**
 * 
 * 
 * @package Mikoshiva_Sql
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/17
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Sql_Exception extends Mikoshiva_Exception {

}