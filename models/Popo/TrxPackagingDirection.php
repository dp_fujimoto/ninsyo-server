<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_packaging_direction
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxPackagingDirection extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tpk';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'packagingDirectionId' => null,
        'packagingDirectionCode' => null,
        'includedFlag' => null,
        'packagingNumber' => null,
        'shippingType' => null,
        'shippingSlipIncludedFlag' => null,
        'deliveryMethodType' => null,
        'dataSendDate' => null,
        'suffix' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * tpk_packaging_direction_id の値を返します
	 *
     * @return int tpk_packaging_direction_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionId() {
		return $this->_properties['packagingDirectionId'];
    }


    /**
     * tpk_packaging_direction_id の値をセットします
	 *
	 * @param int $packagingDirectionId tpk_packaging_direction_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionId($packagingDirectionId) {
		$this->_properties['packagingDirectionId'] = $packagingDirectionId;
    }


    /**
     * tpk_packaging_direction_code の値を返します
	 *
     * @return char tpk_packaging_direction_code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingDirectionCode() {
		return $this->_properties['packagingDirectionCode'];
    }


    /**
     * tpk_packaging_direction_code の値をセットします
	 *
	 * @param char $packagingDirectionCode tpk_packaging_direction_code
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingDirectionCode($packagingDirectionCode) {
		$this->_properties['packagingDirectionCode'] = $packagingDirectionCode;
    }


    /**
     * tpk_included_flag の値を返します
	 *
     * @return char tpk_included_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getIncludedFlag() {
		return $this->_properties['includedFlag'];
    }


    /**
     * tpk_included_flag の値をセットします
	 *
	 * @param char $includedFlag tpk_included_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setIncludedFlag($includedFlag) {
		$this->_properties['includedFlag'] = $includedFlag;
    }


    /**
     * tpk_packaging_number の値を返します
	 *
     * @return int tpk_packaging_number
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPackagingNumber() {
		return $this->_properties['packagingNumber'];
    }


    /**
     * tpk_packaging_number の値をセットします
	 *
	 * @param int $packagingNumber tpk_packaging_number
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPackagingNumber($packagingNumber) {
		$this->_properties['packagingNumber'] = $packagingNumber;
    }


    /**
     * tpk_shipping_type の値を返します
	 *
     * @return varchar tpk_shipping_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingType() {
		return $this->_properties['shippingType'];
    }


    /**
     * tpk_shipping_type の値をセットします
	 *
	 * @param varchar $shippingType tpk_shipping_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingType($shippingType) {
		$this->_properties['shippingType'] = $shippingType;
    }


    /**
     * tpk_shipping_slip_included_flag の値を返します
	 *
     * @return char tpk_shipping_slip_included_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingSlipIncludedFlag() {
		return $this->_properties['shippingSlipIncludedFlag'];
    }


    /**
     * tpk_shipping_slip_included_flag の値をセットします
	 *
	 * @param char $shippingSlipIncludedFlag tpk_shipping_slip_included_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingSlipIncludedFlag($shippingSlipIncludedFlag) {
		$this->_properties['shippingSlipIncludedFlag'] = $shippingSlipIncludedFlag;
    }


    /**
     * tpk_delivery_method_type の値を返します
	 *
     * @return varchar tpk_delivery_method_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeliveryMethodType() {
		return $this->_properties['deliveryMethodType'];
    }


    /**
     * tpk_delivery_method_type の値をセットします
	 *
	 * @param varchar $deliveryMethodType tpk_delivery_method_type
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeliveryMethodType($deliveryMethodType) {
		$this->_properties['deliveryMethodType'] = $deliveryMethodType;
    }


    /**
     * tpk_data_send_date の値を返します
	 *
     * @return date tpk_data_send_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDataSendDate() {
		return $this->_properties['dataSendDate'];
    }


    /**
     * tpk_data_send_date の値をセットします
	 *
	 * @param date $dataSendDate tpk_data_send_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDataSendDate($dataSendDate) {
		$this->_properties['dataSendDate'] = $dataSendDate;
    }


    /**
     * tpk_suffix の値を返します
	 *
     * @return int tpk_suffix
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSuffix() {
		return $this->_properties['suffix'];
    }


    /**
     * tpk_suffix の値をセットします
	 *
	 * @param int $suffix tpk_suffix
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSuffix($suffix) {
		$this->_properties['suffix'] = $suffix;
    }


    /**
     * tpk_delete_flag の値を返します
	 *
     * @return char tpk_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tpk_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tpk_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tpk_deletion_datetime の値を返します
	 *
     * @return datetime tpk_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tpk_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tpk_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tpk_registration_datetime の値を返します
	 *
     * @return datetime tpk_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tpk_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tpk_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tpk_update_datetime の値を返します
	 *
     * @return datetime tpk_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tpk_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tpk_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tpk_update_timestamp の値を返します
	 *
     * @return timestamp tpk_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tpk_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tpk_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























