<?php
/**
 * @package Mikoshiva_View
 */

/**
 * require
 */
require_once 'Zend/View/Helper/FormRadio.php';
require_once 'Zend/View/Abstract.php';
require_once 'Zend/Filter/Alnum.php';
require_once 'Mikoshiva/Logger.php';

/**
 * formRadio のラップクラス
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/17
 * @version SVN:$Id: FormRadio.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_FormRadio extends Zend_View_Helper_FormRadio {

    /**
     * formSelect のラップクラス
     *
     * @param string  $name       name 属性
     * @param string  $value      selected
     * @param string  $attribs    追加項目
     * @param array   $options    options
     * @param boolean $addAllItem true:【全て】の項目を追加
     * @param string  $listsep    区切文字
     * @return unknown_type
     * (non-PHPdoc)
     * @see Zend/View/Helper/Zend_View_Helper_FormSelect#formSelect($name, $value, $attribs, $options, $listsep)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: FormRadio.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function formRadio($name,
                              $value      = null,
                              $attribs    = null,
                              $options    = null,
                              $addAllItem = false,
                              $listsep    = "<br>\n") {


        // ログスタート
        logInfo(LOG_START);

       $addArray = array();
       if ($addAllItem) $addArray['0'] = '全て';

       if (is_array($options)) $options = $addArray + $options;
       else                    $options = $addArray;







    //--------------------------------------------------------------------
    // Zend_View_Helper_FormRadio のコピーです！！！
    // checked の in_array 部分が型チェックを行っていないためコピーしました
    //--------------------------------------------------------------------
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, value, attribs, options, listsep, disable

        // retrieve attributes for labels (prefixed with 'label_' or 'label')
        $label_attribs = array();
        foreach ($attribs as $key => $val) {
            $tmp    = false;
            $keyLen = strlen($key);
            if ((6 < $keyLen) && (substr($key, 0, 6) == 'label_')) {
                $tmp = substr($key, 6);
            } elseif ((5 < $keyLen) && (substr($key, 0, 5) == 'label')) {
                $tmp = substr($key, 5);
            }

            if ($tmp) {
                // make sure first char is lowercase
                $tmp[0] = strtolower($tmp[0]);
                $label_attribs[$tmp] = $val;
                unset($attribs[$key]);
            }
        }

        $labelPlacement = 'append';
        foreach ($label_attribs as $key => $val) {
            switch (strtolower($key)) {
                case 'placement':
                    unset($label_attribs[$key]);
                    $val = strtolower($val);
                    if (in_array($val, array('prepend', 'append'))) {
                        $labelPlacement = $val;
                    }
                    break;
            }
        }

        // the radio button values and labels
        $options = (array) $options;

        // build the element
        $xhtml = '';
        $list  = array();

        // should the name affect an array collection?
        $name = $this->view->escape($name);
        if ($this->_isArray && ('[]' != substr($name, -2))) {
            $name .= '[]';
        }

        // ensure value is an array to allow matching multiple times
        // $value = (array) $value;

        // 一度ストリングに変換してから配列にする
        // add 2009-12-28 T.T.aniguchi
        $value = (array) (string)$value;

        // XHTML or HTML end tag?
        $endTag = ' />';
        if (($this->view instanceof Zend_View_Abstract) && !$this->view->doctype()->isXhtml()) {
            $endTag= '>';
        }

        // add radio buttons to the list.
        require_once 'Zend/Filter/Alnum.php';
        $filter = new Zend_Filter_Alnum();
        foreach ($options as $opt_value => $opt_label) {


            // 勝手にキャストされるのでstringに統一
            // add 2009-12-28 T.T.aniguchi
            $opt_value = (string)$opt_value;



            // Should the label be escaped?
            if ($escape) {
                $opt_label = $this->view->escape($opt_label);
            }

            // is it disabled?
            $disabled = '';
            if (true === $disable) {
                $disabled = ' disabled="disabled"';
            } elseif (is_array($disable) && in_array($opt_value, $disable)) {
                $disabled = ' disabled="disabled"';
            }

            // is it checked?
            $checked = '';
            if (in_array($opt_value, $value, true)) {
                $checked = ' checked="checked"';
            }

            // generate ID
            $optId = $id . '-' . $filter->filter($opt_value);

            // Wrap the radios in labels
            $radio = '<label'
                    . $this->_htmlAttribs($label_attribs) . ' for="' . $optId . '">'
                    . (('prepend' == $labelPlacement) ? $opt_label : '')
                    . '<input type="' . $this->_inputType . '"'
                    . ' name="' . $name . '"'
                    . ' id="' . $optId . '"'
                    . ' value="' . $this->view->escape($opt_value) . '"'
                    . $checked
                    . $disabled
                    . $this->_htmlAttribs($attribs)
                    . $endTag
                    . (('append' == $labelPlacement) ? $opt_label : '')
                    . '</label>';

            // add to the array of radio buttons
            $list[] = $radio;
        }

        // done!
        $xhtml .= implode($listsep, $list);

        return $xhtml;













        // return parent::formRadio($name, $value, $attribs, $options, $listsep);
    }
}
