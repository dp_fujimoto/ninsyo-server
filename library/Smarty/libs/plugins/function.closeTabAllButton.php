<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';

//function.closeTabAll.phpが必須
 /**
 * 現在開いている全てのタブを閉じるjavascriptをボタンタグに実装した形で返します。
 *
 * @param  array   $params
 *                       string name  ボタンのvalue
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.closeTabAllButton.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_closeTabAllButton($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default            = array();
    $default['name']    = '';
    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        // メソッド終了ログを出力
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once dirname(__FILE__) . '/function.closeTabAll.php';

    $script = '';
    $script = smarty_function_closeTabAll($params, $smarty);

    Mikoshiva_Logger::info(LOG_END);
    return '<input type="button" value="' . $data['name'] . '" onclick="' . $script . '">';
}


?>
