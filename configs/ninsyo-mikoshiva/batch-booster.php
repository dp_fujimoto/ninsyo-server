<?php



//-----------------------------------------------
//
// ◆=1:  ini_set
//
//-----------------------------------------------
error_reporting(E_ALL);
ini_set('magic_quotes_gpc', 'off'); // 自動でエスケープされるのを解除します
ini_set('output_buffering', 'off');
ini_set('default_charset',  '');    // 自動で右記が実行されますheader('Content-type: text/html; charset=EUC-JP');
// mb 系の設定
ini_set('mbstring.language',             'Japanese');
ini_set('mbstring.encoding_translation', 'off');
ini_set('mbstring.http_input',           'pass');  // mbstring.encoding_translationが On の時にここで指定した文字コードで リクエスト値の自動変換を行います
ini_set('mbstring.http_output',          'pass');  // 右記メソッドを使用した時の出力エンコード ob_start("mb_output_handler")
ini_set('mbstring.internal_encoding',    'utf-8'); // mb_* 系メソッドのデフォルトエンコード
ini_set('mbstring.substitute_character', '');      // 勝手に吐き出すキャラセット

// タイムゾーンの設定
ini_set('date.timezone', 'Asia/Tokyo');

require_once dirname(__FILE__) . '/define.php';
require_once LIBRARY_PATH . '/functions.php';

// include_path のセット
ini_set('include_path', MODELS_DIR . PATH_SEPARATOR
                      . LIBRARY_PATH . PATH_SEPARATOR
                      . APPLICATION_PATH . DIRECTORY_SEPARATOR . 'batch' . PATH_SEPARATOR );


//-----------------------------------------------
//
// ◆ オートローダー有効化
//
//-----------------------------------------------
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader( true );



//-----------------------------------------------
//
// ◆ 設定ファイルの読み込み
//
//-----------------------------------------------
Zend_Registry::set('GMO_CONFIG', Spyc::YAMLLoad(CONFIGS_DIR . 'gmo-config.yml') );


//-----------------------------------------------
//
// ◆キャッシュの設定
//
//-----------------------------------------------
$frontendOptions = array(
   'lifetime' => 7200, // キャッシュの有効期限を 2 時間とします
   'automatic_serialization' => true
);

$backendOptions = array(
    'cache_dir' => CACHES_DIR // キャッシュファイルを書き込むディレクトリ
);


//-----------------------------------------------
//
// ◆DB の設定
//   ここではまだ接続されない
//   クエリを流すか getConnection() をして初めて接続されます
//
//-----------------------------------------------
// データベースハンドルの取得
/* @var $_db Zend_Db_Adapter_Abstract */
$db = Zend_Db_Table_Abstract::getDefaultAdapter();
$db = Zend_Db::factory('Pdo_Mysql', Array(
    'host'     => DB_PAYMENT_HOST,
    'username' => DB_PAYMENT_USER_NAME,
    'password' => DB_PAYMENT_PASSWORD,
    'dbname'   => DB_PAYMENT_DB_NAME
));
// $db->query('SET NAMES utf8');
Zend_Db_Table_Abstract::setDefaultAdapter($db);


//-----------------------------------------------
//
// ◆Zend_Registry に登録
//
//-----------------------------------------------
// Zend_Cache_Core を Zend_Registry に 登録
$cache = Zend_Cache::factory('Core',
                             'File',
                             $frontendOptions,
                             $backendOptions);
Zend_Registry::set('cache', $cache);