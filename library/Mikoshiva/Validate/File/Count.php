<?php
/** @package Mikoshiva_Validate */



/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/File/Count.php';
require_once 'Mikoshiva/Validate/File/Count.php';
/**
 * Validator for counting all given files
 *
 * @category  Zend
 * @package   Mikoshiva_Validate
 * @copyright Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 */
class Mikoshiva_Validate_File_Count extends Zend_Validate_File_Count
{
    /**
     * @var array Error message templates
     */
    protected $_messageTemplates = array(
    self::TOO_MUCH => "与えられたファイル数が、最大数 '%max%' 個を超えています。(ファイル数 '%count%' 個)",
    self::TOO_LESS => "与えられたファイル数が、最低数 '%min%' 個を下回っています。(ファイル数 '%count%' 個)"
    );


}
