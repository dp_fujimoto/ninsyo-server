<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Output_Order
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Order extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'chargeType' => null,
        'paymentType' => null,
        'debitCardFlag' => '0',
        'ErrCode' => null,
        'ErrInfo' => null,
        'ErrFlag' => '0',
        'settlementInfoList' => array(),
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param array $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }


    /**
     * chargeType
	 *
     * @return  chargeType
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * chargeType
	 *
	 * @param  $chargeType chargeType
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * paymentType
	 *
     * @return  paymentType
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getPaymentType() {
		return $this->_properties['paymentType'];
    }


    /**
     * paymentType
	 *
	 * @param  $paymentType paymentType
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setPaymentType($paymentType) {
		$this->_properties['paymentType'] = $paymentType;
    }


    /**
     * DebitCard
     *
     * @return boolean DebitCard
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getDebitCardFlag() {
        return $this->_properties['debitCardFlag'];
    }


    /**
     * DebitCard
     *
     * @param boolean $debitCard DebitCard
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setDebitCardFlag($debitCardFlag) {
        $this->_properties['debitCardFlag'] = $debitCardFlag;
    }



    /**
     * ErrCode
     *
     * @return string ErrCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrCode() {
        return $this->_properties['ErrCode'];
    }


    /**
     * Code
     *
     * @param string Code Code
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrCode($ErrCode) {
        $this->_properties['ErrCode'] = $ErrCode;
    }



    /**
     * ErrInfo
     *
     * @return string ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrInfo() {
        return $this->_properties['ErrInfo'];
    }


    /**
     * ErrInfo
     *
     * @param string ErrInfo ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrInfo($ErrInfo) {
        $this->_properties['ErrInfo'] = $ErrInfo;
    }



    /**
     * ErrFlag
     *
     * @return string ErrFlag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrFlag() {
        return $this->_properties['ErrFlag'];
    }


    /**
     * ErrFlag
     *
     * @param string ErrFlag DebitCard
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrFlag($ErrFlag) {
        $this->_properties['ErrFlag'] = $ErrFlag;
    }



    /**
     * 決済情報リスト
     *
     * @return array settlementInfoList
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getSettlementInfoList() {
        return $this->_properties['settlementInfoList'];
    }


    /**
     * 決済情報リスト
     *
     * @param array $settlementInfoList settlementInfoList
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Order.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setSettlementInfoList($settlementInfoList) {
        $this->_properties['settlementInfoList'] = $settlementInfoList;
    }

}

























