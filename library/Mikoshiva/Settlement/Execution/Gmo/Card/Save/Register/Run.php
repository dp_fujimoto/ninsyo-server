<?php


require_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Save/Register.php';


/**
 * カード更新処理を行い、成功した場合に【GMO 取引結果テーブル】に処理結果の登録を行います
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/19
 * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Card_Save_Register_Run {


    /**
     * Popo_Abstract
     * @var Popo_Abstract
     */
    protected $_outputPopo;


    /**
     * メイン処理
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/03
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function execute(Popo_Abstract $inputPopo) {

        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Card_Save */
        $inputPopo;


        //------------------------------------------------------------------
        // ■会員登録処理
        //------------------------------------------------------------------
        // ▼inputPopo から会員登録 POPO にコピー
        include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Member/Save.php';
        $memberInputPopo = null;
        $memberInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Member_Save();
        $memberInputPopo->setMemberID($inputPopo->getMemberID());

        // ▼会員登録実行
        include_once 'Mikoshiva/Settlement/Execution/Gmo/Member/Save.php';
        $memberExec = null;
        $memberExec = new Mikoshiva_Settlement_Execution_Gmo_Member_Save($memberInputPopo);
        $ret        = $memberExec->execute();

        // ▼結果を取得
        $this->_outputPopo = $memberExec->getOutput();

        // ◆エラー処理
        if (! $ret)  {
            return false;
        }


        //------------------------------------------------------------------
        // ■カード更新処理
        //------------------------------------------------------------------
        // ▼インスタンス生成
        $cardUpdateClass = null;
        $cardUpdateClass = new Mikoshiva_Settlement_Execution_Gmo_Card_Save_Register($inputPopo);

        // ▼実行
        $ret = null;
        $ret = $cardUpdateClass->execute();

        // ▼結果
        /* @var $output Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save */
        $output = null;
        $output = $cardUpdateClass->getOutput();
        $this->_outputPopo = $output;

        // ▼処理に失敗していたら false を投げる
        if (! $ret) {
            return false;
        }

        //------------------------------------------
        // ■DB 登録処理
        //------------------------------------------
        // ▼登録データを生成
        $data = array();
        $data = array(
            //'mgh_gmo_transaction_history_id' => '',
            //'mgh_order_id'                   => '',
            'memberId'                  => $inputPopo->getMemberID(),
            //'mgh_shop_id'                    => '',
            //'mgh_access_id'                  => '',
            //'mgh_access_pass'                => '',
            'cardNo'                    => Mikoshiva_Utility_Convert::creditCardSecret($inputPopo->getCardNo()),
            //'mgh_method'                     => '',
            //'mgh_pay_times'                  => '',
            'entryStatus'               => 'ENTRY_STATUS_CARD_REGISTER',
            //'mgh_amount'                     => '',
            //'mgh_tran_date'                  => '',
            // 'mgh_delete_flag'                => '',
            //'mgh_deletion_datetime'          => '',
            'registrationDatetime'      => Mikoshiva_Date::mikoshivaNow(),
            'updateDatetime'            => Mikoshiva_Date::mikoshivaNow(),
            //'mgh_update_timestamp'           => '',
        );

        // ▼登録
        $simple = null;
        $simple = new Mikoshiva_Db_Simple();
        $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);


        return true;
    }


    /**
     * GMO との通信結果を返します
     *
     * @return Mikoshiva_Settlement_Popo_Output_Gmo_Card_Save
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/18
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_outputPopo;
    }
}








































