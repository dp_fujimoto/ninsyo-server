<?php
/**
 * @package Mikoshiva_Db
 */


/**
 * 
 * 
 * @package Mikoshiva_Db
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Rowset.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Table_Rowset extends Zend_Db_Table_Rowset_Abstract {


    /**
     *
     * @return Ambigous <multitype:, unknown>
     */
    public function toArrayFormName() {

        $d = array();
        foreach ($this->toArray() as $k => $v) {
            $d2 = array();
            foreach ($v as $k2 => $v2) {
                $_k2 = preg_replace('#^.*?_#', '', $k2, 1);
                $_k2 = preg_replace('#_([a-z]+?)#e', "ucfirst('$1')", $_k2);
                $d2[$_k2] = $v2;
            }
            $d[$k] = $d2;
        }
        return $d;
    }
}
