<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_MstProductOffered
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_MstProductOffered extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'mst_product_offered';
    protected $_primary  = 'mpo_product_offered_id';
    protected $_metadata = array (
  'mpo_product_offered_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_product_offered_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'mpo_product_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_product_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_product_offered_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_product_offered_name',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_product_offered_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_product_offered_type',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_offered_number' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_offered_number',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_offered_start_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_offered_start_date',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_offered_end_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_offered_end_date',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_memo' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_memo',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'text',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_delete_flag',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_deletion_datetime',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_update_datetime',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_registration_datetime',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'mpo_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'mst_product_offered',
    'COLUMN_NAME' => 'mpo_update_timestamp',
    'COLUMN_POSITION' => 13,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

