<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                <input type="hidden" name="informationHistoryId" value="{$form->getInformationHistoryId()}">
                <input type="hidden" name="fileInfo" value="{$form->getFileInfo()}">

                <!-- // お知らせマスタ情報入力テーブル開始 -->
                <table class="COM_table COM_tableWidthLong">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">ID</td>
                        <td>{if !isBlankOrNull($form->getInformationHistoryId())}{$form->getInformationHistoryId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">件名<span class="COM_fontRed COM_bold">*</span></td>
                        <td class="COM_nowrap">
                            <input type="text" name="subject" value="{$form->getSubject()}" size="70" maxlength="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">本文<span class="COM_fontRed COM_bold">*</span></td>
                        <td class="COM_nowrap">
                            <textarea name="body" cols="80" rows="10" style="font-size:12px">{$form->getBody()}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">削除フラグ</td>
                        <td class="COM_nowrap">
                            <input type="checkbox" name="deleteFlag" value="1" {if $form->getDeleteFlag() === '1'}checked{/if}>
                        </td>
                    </tr>
                </table>
                <!-- // 商品マスタ編集フォーム終了 -->

                <br>
                <hr>
                <br>

                <!-- // ページ下部のボタン開始 -->
                <div>
                    {currentTabButton name='　　　　　　次へ　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:'/tool/information/confirm' tabId=$tabId  formId='inputForm'}
                </div>
                <!-- // ページ下部のボタン終了 -->

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->