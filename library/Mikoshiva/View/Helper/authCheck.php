<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/Session/Namespace.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/View/Helper/Exception.php';

/**
 * 権限チェックを行います
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/25
 * @version SVN:$Id: authCheck.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_AuthCheck extends Mikoshiva_View_Helper_Abstract {


    /**
     * 権限チェックを行います
     *
     * @param string $authName 権限名
     * @param string $flag フラグ
     * @return boolean true|false
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/25
     * @version SVN:$Id: authCheck.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function authCheck($authName) {
        logInfo(LOG_START);
        $_authName = $authName;
        $_authName = preg_replace('#([A-Z])#', '_$1', $authName);

        $_authName = 'mau_' . $_authName . '_access_flag';
        $session   = new Zend_Session_Namespace('staff');
        $_authName = strtolower($_authName);
        if (!isset($session->$_authName)) {
            throw new Mikoshiva_View_Helper_Exception('【Mikoshiva_View_Helper_AuthChecker#authChecker】与えられた引数の権限は存在しません。');
        }

        logInfo(LOG_END);
        return ($session->$_authName === '1');
    }
}
