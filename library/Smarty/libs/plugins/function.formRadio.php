<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 与えられた配列からラジオボタンのフォームを作成して返します。
 *
 * このヘルパーはビューからの使用が禁止されています。
 *
 * @param array   $params
 *                      string  name                   フォームのname, id
 *                      string  selected    空文字   | 選択された要素
 *                      array   options     array()  | キー名がvalue、値がラベル名となる配列
 *                      array   attribs     array()  | selectタグに指定する追加の属性 '属性名' => 値
 *                      boolean addAllItem  false    | リストに｢全て｣を追加(デフォルト：無効)
 *                      string  listsep     "<br>"   | 各エンティティを区切る文字列
 * @param Smarty  &$smarty
 * @return
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/03
 * @version SVN:$Id: function.formRadio.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_formRadio($params, &$smarty, &$template) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default               = array();
    $default['name']       = '';
    $default['selected']   = '';
    $default['options']    = array();
    $default['attribs']    = array();
    $default['addAllItem'] = false;
    $default['listsep']    = '<br>';

    //-------------------------------------------------------------------------------------------
    //■呼び出し元チェック smarty_function以外の関数から呼び出すとエラー
    //-------------------------------------------------------------------------------------------
    /*
    $trace = debug_backtrace();
    if(preg_match('/^smarty_function/', $trace[1]['function']) == 0) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】この関数はsmarty関数以外から呼び出してはなりません。');
    }
    */

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }
    if(!is_array($data['options'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 options は配列でなければなりません。');
    }
    if(!is_array($data['attribs'])) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】引数 attribs は配列でなければなりません。');
    }

    //-------------------------------------------------------------------------------------------
    //■条件設定にあれば、｢全て｣オプションを先頭に追加
    //-------------------------------------------------------------------------------------------
    $options = array();
    $options = $data['options'];

    if ($data['addAllItem']) {
        $options = array('0'=>'全て') + $options;
    }

    $data['options'] = $options;

    //-------------------------------------------------------------------------------------------
    //■optionタグを取得
    //-------------------------------------------------------------------------------------------
    require_once SMARTY_PLUGINS_DIR . 'function.html_radios.php';


    $entrustData = array();
    $entrustData['label_ids'] = true; // id 属性を追加します

    // ここで与えるnameは、idを付けさせるため。html_radios()任せにした場合、全ての[が_に変換される。
    // 【補足】
    // {html_radios name="chargeType[oopp]"} の場合、出力結果は、<id="chargeType_oopp__2" name="chargeType[oopp]"> となる
    // また、ここで chargeType-oopp とh変換した場合 <id="chargeType-oopp_2" name="chargeType-oopp"> となる
    // 今度は出力される name 属性が期待しているものでは無いので、後の処理で <id="chargeType-oopp_2" name="chargeType[oopp]"> となるようにしています。
    $entrustData['name'] = str_replace('[', '-', $data['name']);
    $entrustData['name'] = str_replace(']', '',  $entrustData['name']);

    $entrustData['selected'] = $data['selected'];
    $entrustData['options']  = $data['options'];
    $entrustData['separator'] = $data['listsep'];
    $str = '';
    $str = smarty_function_html_radios($entrustData, $smarty, $template);



    //-------------------------------------------------------------------------------------------
    //■追加属性を文字列に変換
    //-------------------------------------------------------------------------------------------
    $attribsStr = '';
    foreach($data['attribs'] as $k => $v) {
        //$attribs = '属性($k)="$v" ...'
        $attribsStr .= ' ' . $k . '="' . $v . '"';
    }


    //-------------------------------------------------------------------------------------------
    //■nameを修正し、追加属性をタグに追加
    //-------------------------------------------------------------------------------------------
    // <id="chargeType-oopp_2" name="chargeType[oopp]"> と成るように変換
    $str    = preg_replace('/name=".*?"/', 'name="' . $data['name'] . '"', $str);
    $result = preg_replace('/(<input)/', '${1} ' . $attribsStr, $str);


    Mikoshiva_Logger::info(LOG_END);
    return $result;

}


?>
