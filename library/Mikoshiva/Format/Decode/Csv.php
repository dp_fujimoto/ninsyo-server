<?php
/** @package Mikoshiva_Format */

/**
 * require
 */
require_once 'Mikoshiva/Format/Decode/Abstract.php';

/**
 *
 *
 * @package Mikoshiva_Format
 * @author admin
 * @since 2010/02/17
 * @version SVN:$Id: Csv.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Format_Decode_Csv extends Mikoshiva_Format_Decode_Abstract
{
    /**
     * フォーマット
     * @var unknown_type
     */
    protected $_format = 'ENCLOSED_IN_DOUBLE_QUOTES';

    /**
     *
     *
     * @param unknown_type $format ENCLOSED_IN_DOUBLE_QUOTES ダブルクォーテーション囲み　デフォルト
     *                             NOT_ENCLOSED_IN_DOUBLE_QUOTES ダブルクォーテーション無し
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/22
     * @version SVN:$Id: Csv.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFormat($format) {
        $this->_format = $format;
    }

    /**
     * CSV ファイル形式の文字列一行を配列にします
     *
     * 各データが"(ダブルクォート)で囲まれたCSVの一行を、データごとに切り分けて配列にします。
     * "(ダブルクォート)のエスケープは""(ダブルクォート2つ)で行われているものと想定します。
     * <test>
     *   $dataList[] = '"aa", "bb", "cc"';
     *   $dataList[] = '"aa", "bb", "cc",';
     *   $dataList[] = '"aa", "bb", "cc",';
     *   $dataList[] = '""",aa", ",""bb", "cc""",';
     *   $dataList[] = '';
     *   foreach($dataList as $k => $v) {
     *       var_dump(process($v));
     *   }
     * </test>
     *
     *
     * @param unknown_type $data
     * @return array
     */
    public function process( $data ) {

        $data = trim($data);

        $newArray = array();

        switch($this->_format) {
            case 'NOT_ENCLOSED_IN_DOUBLE_QUOTES':
                $newArray = explode(',', $data);
                break;
            case 'ENCLOSED_IN_DOUBLE_QUOTES':
                $match = array();
                $data  = preg_replace('/\n/', '', $data);
                preg_match_all('/"((?:""|.)*?)"(?:,|$)/', $data, $match);
                foreach($match[1] as $v) {
                    $newArray[] = str_replace('""', '"', $v);
                }
                break;
            default:
                throw new Mikoshiva_Exception();
        }
        return $newArray;
    }
}