<?php
require_once 'Mikoshiva/View/Helper/Exception.php';

require_once 'Mikoshiva/Logger.php';

 /**
 * ヘルパーに置き換えます
 *
 * @param  array   $params
 *                       string text     対象テキスト
 *                       string lineToBr true:テキストを出力する際に改行コードを改行タグに置き換える
 *                       string saveName
 *                       string  formId    null  |フォームの ID 名
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.helperReplacer.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_helperReplacer($params, &$smarty) {

    //-------------------------------------------------------------------------------------------
    //
    // ■デフォルト値を定義
    //
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['text']     = '';
    $default['lineToBr'] = false;
    $default['tabId']    = $smarty->get_template_vars('tabId');
    $default['formId']   = null;
    $default['append']   = 'OVERWRITE';
    $default['saveName'] = '';



    //-------------------------------------------------------------------------------------------
    //
    // ■入力値に存在しないキーをデフォルト値で置換
    //
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default; // 左勝ち



    //-------------------------------------------------------------------------------------------
    //
    // ■入力値チェック
    //
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['text']) ) {
        $message = "【" . __FUNCTION__ . " （" . __LINE__ . "）】必須引数 text に値がありません。";
        throw new Mikoshiva_View_Helper_Exception($message);
    }
    if(isBlankOrNull($data['text']) ) {
        $message = "【" . __FUNCTION__ . " （" . __LINE__ . "）】必須引数 text に値がありません。";
        throw new Mikoshiva_View_Helper_Exception($message);
    }



    //-------------------------------------------------------------------------------------------
    //
    // ■入力されたテキストから newTab currentTab を生成する
    //
    // <newTabLink url="/test1/test2/test3" name="次のページへ">
    // <currentTabLink url="/test1/test2/test3" name="次のページへ">
    //
    // array(2) {
    //   [0]=>
    //   array(2) {
    //     [0]=>
    //     string(63) "<newTabLink url="/test1/test2/test3" name="次のページへ">"
    //     [1]=>
    //     string(67) "<currentTabLink url="/test1/test2/test3" name="次のページへ">"
    //   }
    //   [1]=>
    //   array(2) {
    //     [0]=>
    //     string(10) "newTabLink"
    //     [1]=>
    //     string(14) "currentTabLink"
    //   }
    // }
    //-------------------------------------------------------------------------------------------
    $_text = $data['text']; // 本文取得

    // マッチしなかったらそのまま返す
    $matchList = array();
    if (preg_match_all('#(<|&lt;)((newTab|currentTab|requestToId)\w*?\s.*?)(>|&gt;)#s', $_text, $matchList) === 0) {

        if ($data['lineToBr'] === true) {
            include_once dirname(__FILE__) . '/function.lineToBr.php';
            $_text = smarty_function_lineToBr(array('str' => $_text), $smarty);
        }
        return $_text;
    }

    // マッチしたらリンクを生成
    $script = array();
    foreach ($matchList[2] as $matchIndex => $matchValue) {

        // ゴミを取り除く
        $_matchValue = $matchValue;                                       // コピー
        $_matchValue = htmlspecialchars_decode($_matchValue, ENT_QUOTES); // HTML エンティティを文字に戻します。
        $_matchValue = trim($_matchValue);                          // 前後の不要な文字列を削除
        $_matchValue = preg_replace('#\s{2,}#', ' ', $_matchValue); // ２つ以上のスペースを１つにする

        // 属性="値" を配列化
        $_helperMatchArray = array();
        preg_match_all('#(\w*?=".*?")#', $_matchValue, $_helperMatchArray); // スペースをセパレータとして配列を生成

        // メソッド名を取得
        $_methodNameMatchAray = array();
        preg_match('#(^\w+?)\s#', $_matchValue, $_methodNameMatchAray);

        // 0 番目の配列はヘルパー名となるため抽出
        $methodName       = $_methodNameMatchAray[1]; // 0 番目は抽出し配列の添字は採番し直されます
        $helperMethodName = 'smarty_function_' . $methodName;
        $helperFilePath   = dirname(__FILE__) . "/function.{$methodName}.php";

        // 属性をキー名とした配列を生成
        $helperParam = array();
        foreach ($_helperMatchArray[1] as $_helperIndex => $_helperValue) {

            // check
            $helperMatch = array();
            if (preg_match('#^(\w*?)="(.*?)"$#', $_helperValue, $helperMatch) !== 1) {
                dumper($_helperArray);exit;
                $message = "【" . __FUNCTION__ . " （" . __LINE__ . "）】記述方法が不正です。 ”{$_matchValue}”の”{$_helperValue}”";
                throw new Mikoshiva_View_Helper_Exception($message);
            }

            $helperParam[$helperMatch[1]] = $helperMatch[2];
        }

        include_once $helperFilePath;
        $scriptList[] = $helperMethodName(($data + $helperParam), $smarty);
    }
//dumper($scriptList);exit;
    foreach ($scriptList as $script) {
        $_text = preg_replace('#(<|&lt;)((newTab|currentTab|requestToId)\w*?\s.*?)(>|&gt;)#s', $script, $_text, 1);
    }

    if ($data['lineToBr'] === true) {
        include_once dirname(__FILE__) . '/function.lineToBr.php';
        $_text = smarty_function_lineToBr(array('str' => $_text), $smarty);
    }
    return $_text;
}





























