<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Db/Abstract.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Utility/Mapping.php';
require_once 'Zend/View/Interface.php';
require_once 'Mikoshiva/Logger.php';
/**
 * 短縮コードのリスト(select/radioフォーム)を作成します。
 *
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/14
 * @version SVN:$Id: StatusLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_statusLister extends Mikoshiva_View_Helper_Abstract {


    public $view;


    /**
     * 短縮コードのリスト(select/radioフォーム)を作成します。
     *
     * @param string  $name name  属性
     * @param string  $column     カラム名
     * @param string  $selected   selected
     * @param string  $mode       select/radio
     * @param array   $deleteList 削除リスト
     * @param array   $attribs    追加属性
     * @param boolean $addEmpty   true:一番目の配列に空項目を追加する
     * @param boolean $addAllItem true:【全て】の項目の追加
     * @param string  $listsep    区切文字
     * @return select/radio
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/15
     * @version SVN:$Id: StatusLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function statusLister($name,
                                 $column,
                                 $selected   = null,
                                 $mode       = 'select',
                                 $deleteList = null,
                                 $attribs    = null,
                                 $addEmpty   = false,
                                 $addAllItem = false,
                                 $listsep    = "<br>\n") {

        // ログスタート
        logInfo(LOG_START);


        // deleteList が null だったら殻の配列に上書き
        if (isBlankOrNull($deleteList)) $deleteList = array();


        // ステータスマッピングから値を取得
        // 2010/01/17 T.Taniguchi Mikoshiva_Utility_Mapping::statusMappingList から取得するように修正
        $d = array();
        $d = Mikoshiva_Utility_Mapping::statusMappingList($column, $deleteList);


        //==========================================
        // mode によって表示を切り替える
        // 不明な mode であれば空文字が返ります
        //==========================================

        if ( $mode === 'select' ) {
            // select -------------------------------
            //$formSelect = new Mikoshiva_View_Helper_FormSelect();
            logInfo(LOG_END);
            return $this->view->formSelect($name,
                                           $selected,
                                           $attribs,
                                           $d,
                                           $addEmpty,
                                           $addAllItem,
                                           $listsep);
        }

        if ( $mode === 'radio' ) {

            // radio -------------------------------
            //$formRadio = new Mikoshiva_View_Helper_FormRadio();
            return $this->view->formRadio($name,
                                          $selected,
                                          $attribs,
                                          $d,
                                          $addAllItem,
                                          $listsep);
        }


        // ログエンド
        logInfo(LOG_END);
        return '';
    }


    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}















