function dpal() {
    var lh = location.hostname;
    var lp = location.pathname;
    var ls = location.search.replace('?','');
    var cookie_name = lp+'-'+ls.replace('=','-');
    var cookie_value = '';
    var acl_url = 'https://www.mikoshiva.com/outside/js/accessLog.php';
    var url = acl_url+"?sn="+lh+"&sc="+lp+"&ru="+ls;
    
    if (document.cookie) {
        var cookies = document.cookie.split("; ");
        for (var i = 0; i < cookies.length; i++) {
            var str = cookies[i].split("=");
            if (str[0] == cookie_name) {
                var cookie_value = unescape(str[1]);
                break;
            }
        }
    }
    if (cookie_value == '') {
        var nowtime = new Date().getTime();
        var clear_time = new Date(nowtime + (60 * 60 * 24 * 30));
        var expires = clear_time.toGMTString();

        document.cookie = cookie_name + "=" + nowtime + "; expires=" + expires;
        var src = '<script type="text/javascript" src="'+url+'&ck=1" charset="utf-8"></script>'
        document.write(src);      
    } else {
        //var src = '<img src="'+url+'" width="1px" height="1px" style="display:none;">';
        var src = '<script type="text/javascript" src="'+url+'" charset="utf-8"></script>'
        document.write(src);
    }
}
