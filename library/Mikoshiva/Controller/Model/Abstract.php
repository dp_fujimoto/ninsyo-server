<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * Model_Abstract
 *
 * 単なるユーティリティメソッドの様なクラスを作成する際はこのクラスを継承すること
 * このクラスから DB に接続してはいけません
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/08
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Model_Abstract {

    /**
     * Modelの実行結果
     * @var string
     */
    protected $_result = null;


    /**
     * リクエストクラスと DB クラスを初期化します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     * @return unknown_type
     */
    public function __construct() {

    }

    /**
     * 実行クラス
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     * @return unknown_type
     */
    abstract public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                        Mikoshiva_Controller_Mapping_ActionContext $context);


    /**
     * モデルの実行結果を返します
     * @return string
     */
    public function getResult() {
        return $this->_result;
    }

}










