<?php
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Zend/Controller/Request/Abstract.php';

/**
 *
 * アクションフォーム
 *
 * -----------------------------------------------------------------------
 *    ワンポイント
 * -----------------------------------------------------------------------
 *    ■クラス名
 *    アクションフォームクラスのクラス名は、
 *    必ず「～Form」としてください。
 *
 *    ■resetメソッド
 *    任意の初期化処理を行う場合に実装します。
 *    (メソッド定義は省略可能です)
 *
 *    ■validateメソッド
 *    任意のバリデート処理を行う場合に実装します。(独自バリデート)
 *    validation.yml定義による固定バリデート処理を実行するには、
 *    スーパークラスのvalidateメソッドを呼び出してください。
 *    「parent::reset($config, $request);」
 *    その場合、スーパークラスのvalidateメソッドが返す
 *    固定バリデートの実行結果値は、適切に処理してください。
 *    (メソッド定義は省略可能です)
 * -----------------------------------------------------------------------
 *
 * @author        K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Product_InputForm
    extends Mikoshiva_Controller_Mapping_Form_Abstract {




    /**
     * アクションフォームの初期化を行います。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');
        parent::reset($config, $request);
        logInfo('(' . __LINE__ . ')', '終了');
    }

    /**
     * バリデートを行います。エラーMSGをリターンでかえす
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {
        logInfo('(' . __LINE__ . ')', '開始');

        $message = array();

        if ($request->getParam('action') === 'confirm' || $request->getParam('action') === 'complete') {
            // 商品名のチェック
            if (isBlankOrNull($this->getProductName())) {
                $message[] = '商品名が未記入です。';
            } else {
                //-----------------------------------------------
                // 商品名重複チェック
                //-----------------------------------------------
                $params = array();

                // SQL作成
                $sql = "";
                $sql .= "SELECT *";
                $sql .= " FROM mst_product";
                $sql .= " WHERE 1 = 1";

                $sql .= " AND mpr_product_name = ?";
                $params[] = $this->getProductName();

                if (!isBlankOrNull($this->getProductId())) {
                    $sql .= " AND mpr_product_id != ?";
                    $params[] = $this->getProductId();
                }

                $sql .= " AND mpr_brand_id = ?";
                $params[] = $this->getBrandId();

                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                    $message[] = 'ブランド内ですでに使われている商品名です。変更してください。';
                    break;
                }
            }

            // ブランドのチェック
            if (isBlankOrNull($this->getBrandId())) {
                $message[] = 'ブランドが選択されていません。';
            }

            // 商品画像URLのチェック
            if (!isBlankOrNull($this->getImageUrl())) {
                if (!preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/:\@+\$,%#]+)$/', $this->getImageUrl())) {
                    $message[] = '商品画像URLのフォーマットが異常、もしくは使えない文字が含まれています。';
                } else {
                    try {
                        $header = get_headers($this->getImageUrl());
                    } catch (Exception $e) {
                        $message[] = '商品画像URLに接続できません。URLを確認してください。';
                    }
                }
            }
        }

        //-----------------------------------------------
        // エラーメッセージを返す
        //-----------------------------------------------
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $message;
    }

}
