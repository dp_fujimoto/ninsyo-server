<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Date - MIKOSHIVA で設定された日付（mikoshiva 日付）を使用したい</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1>MIKOSHIVA で設定された日付（mikoshiva 日付）を使用したい</h1>

            <p>
                このメソッドは mikoshiva で設定された日付を返します。<br>
                設定されていない場合現在日付を返します。
            </p>

            <!-- ========================================
              ■MIKOSHIVA で設定された日付（mikoshiva 日付）を使用したい
            ==========================================-->
            <h2>MIKOSHIVA で設定された日付（mikoshiva 日付）を使用したい</h2>


            <pre class="wiki">
<?php
highlight_string('<?php

//-----------------------------------------------
//
// ■月末日を取得する
//
//------------------------------------------------
include_once \'Mikoshiva/Date.php\';

Mikoshiva_Date::mikoshivaNow();             // 2010-05-06 11:22:33
Mikoshiva_Date::mikoshivaNow(\'yyyy-MM-dd\'); // 2010-05-06
');?>
            </pre>




            <h2>Mikoshiva_Date::mikoshivaNow</h2>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$format</td>
                <td>string</td>
                <td>yyyy-MM-dd HH:mm:ss</td>
                <td>日付フォーマット
                </td>
                <td>
                  初期値に設定された日付から $month に与えられた月数の月末日を返します。<br>
                  ※時・分・秒は 00:00:00 でフォーマットされています。
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></br></td>
                <td>string</td>
                <td><br></td>
                <td>日付</td>
                <td><br></td>
              </tr>
            </table>

            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















