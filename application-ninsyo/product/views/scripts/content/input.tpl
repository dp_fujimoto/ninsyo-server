<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">コンテンツ登録1</h2>

            <span class="COM_bold">【コンテンツについて】</span><br><br>

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            {if isBlankOrNull($form->getContentId())}
                <form name="copyForm" id="copyForm" method="post" onSubmit="return false;">
                    ID：<input type="text" name="copyContentId" value="" maxlength="6" size="8"> (数字のみ）
                    　　
                    {currentTabButton name='左記のコンテンツの内容をコピーする' url='/product/content/input' tabId=$tabId formId='copyForm'}
                </form>
                <br>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}" >

                {$deleteList = array()}
                {$deleteList[] = 'tokenId'}
                {$deleteList[] = 'brandId'}
                {$deleteList[] = 'productId'}
                {$deleteList[] = 'applicationType'}
                {$deleteList[] = 'copyContentId'}
                {$deleteList[] = 'contentName'}
                {$deleteList[] = 'confirmName'}
                {$deleteList[] = 'imageUrl'}
                {$deleteList[] = 'imageUpdateFlag'}
                {$deleteList[] = 'mediaUrl'}
                {$deleteList[] = 'mediaUpdateFlag'}
                {$deleteList[] = 'mediaType'}
                {$deleteList[] = 'publicationDate'}
                {$deleteList[] = 'description'}
                {$deleteList[] = 'memo'}
                {$deleteList[] = 'testFlag'}
                {$deleteList[] = 'deleteFlag'}
                {$deleteList[] = 'maxConditionNum'}
                {hiddenLister hiddenList=$form deleteList=$deleteList}

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ID
                        </td>
                        <td class="COM_nowrap" colspan="2">{if !isBlankOrNull($form->getContentId())}{$form->getContentId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ブランド<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            {$where = array()}
                            {$where['mbr_delete_flag'] = '0'}
                            {$where['mbr_division_id'] = $form->getDivisionId()}

                            {$attribs = array()}
                            {$attribs['onChange'] = "KO_currentTab('/product/content/input-detail-back', '"|cat:$tabId|cat:"', 'inputForm', []);"}
                            {formSelectDb name='brandId' selected=$form->getBrandId() arClassName='Mikoshiva_Db_NinsyoAr_MstBrand' labelName='mbr_brand_name' valueName='mbr_brand_id' where=$where addEmpty=true attribs=$attribs}
                        </td>
                        <td class="COM_nowrap">
                            部署：
                            {$attribs = array()}
                            {$attribs['onChange'] = "KO_currentTab('/product/content/input-detail-back', '"|cat:$tabId|cat:"', 'inputForm', []);"}
                            {divisionLister name="divisionId" mode="select" selected=$form->getDivisionId() attribs=$attribs}
                            　　
                            検索文字：<input type="text" onChange="PDT_contentInputChgSelectBox{$tabId}('brandId',this.value);">
                        </td>
                    </tr>
                </table>

                <br>

                <div class="{if isBlankOrNull($form->getBrandId())}COM_displayNone{/if}">

                    <table class="COM_table">
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                商品名<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td class="COM_nowrap">
                                {$where = array()}
                                {$where['mpr_brand_id'] = $form->getBrandId()}
                                {$where['mpr_delete_flag'] = '0'}
                                {if isBlankOrNull($where['mpr_brand_id'])}{$where['mpr_brand_id'] = 0}{/if}
                                {formSelectDb name='productId' selected=$form->getProductId() arClassName='Mikoshiva_Db_NinsyoAr_MstProduct' labelName='mpr_product_name' valueName='mpr_product_id' where=$where addEmpty=false}
                                　　
                                検索文字：<input type="text" onChange="PDT_contentInputChgSelectBox{$tabId}('productId',this.value);">
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                用途<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td class="COM_nowrap">
                                {$attribs = array()}
                                {$attribs['onChange'] = 'PDT_contentInputChgApplicationType'|cat:$tabId|cat:'(this.options[this.selectedIndex].value);'}
                                {statusLister name="applicationType" selected=$form->getApplicationType() status="application_type" attribs=$attribs}
                                <span class="COM_fontGrey">何に使うためのコンテンツか。ライブラリーはiosアプリなどで利用</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                コンテンツ名<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="contentName" value="{$form->getContentName()}" size="80" maxlength="200"><br>
                                <span class="COM_fontGrey">お客様が目にすることを前提とした名前</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                公開日<span class="COM_fontRed COM_bold">*</span>
                            </td>
                            <td class="COM_nowrap">
                                <input type="text" name="publicationDate" value="{if !isBlankOrNull($form->getPublicationDate())}{$form->getPublicationDate()}{else}{date('Y-m-d')}{/if}" size="10" maxlength="10">
                                <br>
                                <span class="COM_fontGrey">コンテンツがリストに表示されるようになる日付 例：2012-06-12</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                テスト中フラグ
                            </td>
                            <td class="COM_nowrap">
                                <input type="checkbox" name="testFlag" value="1" {if $form->getTestFlag() === '1'}checked{/if}>テスト中であり、一般公開しない場合にチェックする
                                <br>
                                <span class="COM_fontGrey">テスト中でも@d-publishing.jpのメールアドレスのユーザーは、条件に該当すれば許可されるのでテストが可能</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                メモ
                            </td>
                            <td class="COM_nowrap">
                                <textarea name="memo" cols="70" rows="4">{$form->getMemo()}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                状態
                            </td>
                            <td class="COM_nowrap">
                                {if !isBlankOrNull($form->getContentId())}
                                    <input type="checkbox" name="deleteFlag" value="1" {if $form->getDeleteFlag() === '1'}checked{/if}>削除する
                                {else}
                                    <input type="hidden" name="deleteFlag" value="0">新規
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                設定する条件数
                            </td>
                            <td class="COM_nowrap">
                                <select name="maxConditionNum">
                                    <option value="3">3</option>
                                    <option value="4" {if $form->getMaxConditionNum() == 4}selected{/if}>4</option>
                                    <option value="5" {if $form->getMaxConditionNum() == 5}selected{/if}>5</option>
                                    <option value="6" {if $form->getMaxConditionNum() == 6}selected{/if}>6</option>
                                    <option value="7" {if $form->getMaxConditionNum() == 7}selected{/if}>7</option>
                                    <option value="8" {if $form->getMaxConditionNum() == 8}selected{/if}>8</option>
                                    <option value="9" {if $form->getMaxConditionNum() == 9}selected{/if}>9</option>
                                    <option value="10" {if $form->getMaxConditionNum() == 10}selected{/if}>10</option>
                                    <option value="11" {if $form->getMaxConditionNum() == 11}selected{/if}>11</option>
                                    <option value="12" {if $form->getMaxConditionNum() == 12}selected{/if}>12</option>
                                    <option value="13" {if $form->getMaxConditionNum() == 13}selected{/if}>13</option>
                                    <option value="14" {if $form->getMaxConditionNum() == 14}selected{/if}>14</option>
                                    <option value="15" {if $form->getMaxConditionNum() == 15}selected{/if}>15</option>
                                    <option value="16" {if $form->getMaxConditionNum() == 16}selected{/if}>16</option>
                                    <option value="17" {if $form->getMaxConditionNum() == 17}selected{/if}>17</option>
                                    <option value="18" {if $form->getMaxConditionNum() == 18}selected{/if}>18</option>
                                    <option value="19" {if $form->getMaxConditionNum() == 19}selected{/if}>19</option>
                                    <option value="20" {if $form->getMaxConditionNum() == 20}selected{/if}>20</option>
                                    <option value="21" {if $form->getMaxConditionNum() == 21}selected{/if}>21</option>
                                    <option value="22" {if $form->getMaxConditionNum() == 22}selected{/if}>22</option>
                                    <option value="23" {if $form->getMaxConditionNum() == 23}selected{/if}>23</option>
                                    <option value="24" {if $form->getMaxConditionNum() == 24}selected{/if}>24</option>
                                    <option value="25" {if $form->getMaxConditionNum() == 25}selected{/if}>25</option>
                                    <option value="26" {if $form->getMaxConditionNum() == 26}selected{/if}>26</option>
                                    <option value="27" {if $form->getMaxConditionNum() == 27}selected{/if}>27</option>
                                    <option value="28" {if $form->getMaxConditionNum() == 28}selected{/if}>28</option>
                                    <option value="29" {if $form->getMaxConditionNum() == 29}selected{/if}>29</option>
                                    <option value="30" {if $form->getMaxConditionNum() == 30}selected{/if}>30</option>
                                    <option value="31" {if $form->getMaxConditionNum() == 31}selected{/if}>31</option>
                                </select>
                                <br>
                                <span class="COM_fontGrey">次のページで設定する条件の数</span>
                            </td>
                        </tr>
                    </table>

                    <div id="div1" class="{if $form->getApplicationType() === 'APPLICATION_TYPE_LIBRARY' || isBlankOrNull($form->getApplicationType())}{else}COM_displayNone{/if}">
                        <br>
                        【用途がライブラリーの場合は下記の項目は入力推奨（一部必須）】

                        <table class="COM_table">
                            <tr>
                                <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                    コンテンツ画像URL
                                </td>
                                <td class="COM_nowrap">
                                    <input type="text" name="imageUrl" value="{$form->getImageUrl()}" size="100" maxlength="500">
                                    <br>
                                    <span class="COM_fontGrey">リスト表示に使用  横{$smarty.const.CONTENT_IMAGE_W}px 縦{$smarty.const.CONTENT_IMAGE_H}pxの縮尺で作成してください</span>
                                    {if $form->getImageNoneFlag() === '1'}
                                        <input type="hidden" name="imageUpdateFlag" value="1">
                                    {else}
                                        <hr style="border-style: dashed;">
                                        <input type="checkbox" name="imageUpdateFlag" value="1" {if $form->getImageUpdateFlag() === '1'}checked{/if}>
                                        ←URLを変更、またはURLにある画像データを変更したらチェック
                                        <br>
                                        <span class="COM_bold">最終更新日：{$form->getImageUpdateDatetime()}</span>
                                        <br>
                                        <span class="COM_fontRed">商品画像に関して変更があった場合、チェックボックスにチェックを入れないとユーザーに反映されません</span>
                                    {/if}
                                </td>
                            </tr>
                            <tr>
                                <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                    メディアタイプ<span class="COM_fontRed COM_bold">*</span>
                                </td>
                                <td class="COM_nowrap">
                                    <select name="mediaType">
                                        <option value="mp4" {if $form->getMediaType() === 'mp4'}selected{/if}>MP4</option>
                                        <option value="mp3" {if $form->getMediaType() === 'mp3'}selected{/if}>MP3</option>
                                        {*<option value="pdf" {if $form->getMediaType() === 'pdf'}selected{/if}>PDF</option>*}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                    メディアURL<span class="COM_fontRed COM_bold">*</span>
                                </td>
                                <td class="COM_nowrap">
                                    <input type="text" name="mediaUrl" value="{$form->getMediaUrl()}" size="100" maxlength="500">
                                    <br>
                                    <span class="COM_fontGrey">実際に動画などをおいてあるURL</span>
                                    {if $form->getMediaNoneFlag() === '1'}
                                        <input type="hidden" name="mediaUpdateFlag" value="1">
                                    {else}
                                        <hr style="border-style: dashed;">
                                        <input type="checkbox" name="mediaUpdateFlag" value="1" {if $form->getMediaUpdateFlag() === '1'}checked{/if}>
                                        ←URLを変更、またはURLにある画像データを変更したらチェック
                                        <br>
                                        <span class="COM_bold">最終更新日：{$form->getMediaUpdateDatetime()}</span>
                                        <br>
                                        <span class="COM_fontRed">商品画像に関して変更があった場合、チェックボックスにチェックを入れないとユーザーに反映されません</span>
                                    {/if}
                                </td>
                            </tr>
                            <tr>
                                <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                    メディア管理名
                                </td>
                                <td class="COM_nowrap">
                                    <input type="text" name="confirmName" value="{$form->getConfirmName()}" size="80" maxlength="200"><br>
                                    <span class="COM_fontGrey">管理用の名前。実際のファイル名など。入力なしOK</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                                    注釈
                                </td>
                                <td class="COM_nowrap">
                                    <input type="text" name="description" value="{$form->getDescription()}" size="80" maxlength="132">
                                    <br>
                                    <span class="COM_fontGrey">お客様が目にすることを前提としたこのコンテンツの説明文。改行不可</span>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <br>
                    <hr>
                    <br>

                    {currentTabButton name='　　　　　　次へ　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/input-detail" tabId=$tabId  formId='inputForm'}
                </div>

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 表示切替
    //-----------------------------------------------
    function PDT_contentInputChgApplicationType{$tabId}(val){

        var obj1 = document.getElementById('div1');

        if (val === 'APPLICATION_TYPE_LIBRARY') {
            obj1.className = '';
        } else {
            obj1.className = 'COM_displayNone';
        }
    }

    //-----------------------------------------------------
    // オーダーフォーム選択絞り
    //-----------------------------------------------------
    function PDT_contentInputChgSelectBox{$tabId}(id,val) {

        var obj = document.getElementById(id);
        if (obj) {
            var count = 0;
            for (var i = 1; i < obj.length; i++) {
                if (i !== obj.selectedIndex) {
                    if (obj.options[i].innerHTML.search(val) != -1) {
                        obj.options[i].style.display = '';
                        obj.options[i].disabled = false;
                        count++;
                    } else {
                        obj.options[i].style.display = 'none';
                        obj.options[i].disabled = true;
                    }
                }
            }
            //alert(count+'件該当しました');
        }
    }

</script>
<!-- // カレントJavascript終了 -->