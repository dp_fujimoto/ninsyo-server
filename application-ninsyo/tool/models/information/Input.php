<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstInformationHistory.php';

/**
 * 指定したスタッフIDの情報を入力画面にプリセットする。
 *
 * @author         fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/28
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Information_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したスタッフIDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $informationHistoryId   = $actionForm->getInformationHistoryId();
        $fileInfo = '';

        if (!isBlankOrNull($informationHistoryId)) {

            // SQL作成
            $sql = '';
            $sql .= 'SELECT';
            $sql .= ' *';
            $sql .= ' FROM mst_information_history ';
            $sql .= ' WHERE 1 = 1 ';

            // パラメータセット
            $params   = array();

            $sql .= ' AND mih_delete_flag = ? ';
            $params[] = '0';

            // 商品発送ID
            $sql .= ' AND mih_information_history_id =  ?';
            $params[] = $informationHistoryId;


            // SQL実行＋全件取得
            $res = array();
            $res = $this->_db->fetchAll($sql,$params);

            if (strstr($res[0]['mih_body'],'___FILEDATA___')) {
                $temp = explode('___FILEDATA___',$res[0]['mih_body']);
                $res[0]['mih_body'] = $temp[0];
                foreach ($temp as $k => $v) {
                    if ($k > 0) {
                        $fileInfo .= '___FILEDATA___' . $v;
                    }
                }
            }

            $row = Mikoshiva_Utility_Array::extractBeginningOfPrefix( 'mih_' , $res[0] );

            // Popoに一行ずつ入れていく
            $rowPopo = Mikoshiva_Utility_Copy::copyDbProperties( new Popo_MstInformationHistory() , $row );

            $actionForm = Mikoshiva_Utility_Copy::copyPopoToActionFormProperties($actionForm, $rowPopo);
            $actionForm->setFileInfo($fileInfo);
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return true;
    }

}
