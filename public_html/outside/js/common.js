// 画面
//document.write('<div class="COM_srcBorder1024"></div>');
//document.write('<div class="COM_srcBorder768"></div>');

function zipToAddress(idZip1, idZip2, idPrefecture, idAddress){
    //郵便番号が3桁+4桁の数字であることをチェックする。
    var zip1 = $('#' + idZip1).val();
    var zip2 = $('#' + idZip2).val();

    if(zip1.length != 3) {
      alert('郵便番号の形式が正しくありません。');
      return;
    }
    if(isNaN(zip1)){
        alert('郵便番号の形式が正しくありません。');
        return;
      }
    if(zip2.length != 4) {
      alert('郵便番号の形式が正しくありません。');
      return;
    }
    if(isNaN(zip2)){
      alert('郵便番号の形式が正しくありません。');
      return;
    }
    // ※数値チェックの追加 2012-01-25 谷口司
    if (zip1.match(/^[0-9]{3}$/) === null) {
        alert('郵便番号（左）は数字３文字で入力して下さい。');
        return;
    }
    // ※数値チェックの追加 2012-01-25 谷口司
    if (zip2.match(/^[0-9]{4}$/) === null) {
        alert('郵便番号（右）は数字４文字で入力して下さい。');
        return;
    }


	acd = window.name;
    //url = '/_SYSTEM_/tool/ajax.php?acd=' + acd;
    url = '/_SYSTEM_/transaction/controller.php?tcd=DUMMY&acd=utility_address_zip-to-address&namespace=' + window.name;

    //郵便番号をzipToAddressActionに送り、JSON形式でデータを取得する。
    //リクエスト先
    $.getJSON( url,
    { "actionUrl": "/utility/address/zip-to-address", "zip1": zip1, "zip2": zip2 },
      function(data) {
        //取得したデータにerrorが含まれていた場合、警告を出して終了する。
        if(data.error != null) {
            alert(data.error);
          return;
        }
        //取得したデータをフォームに反映する。
        $('#' + idPrefecture).val(data.mzc_prefecture_name);
        $('#' + idAddress).val(data.mzc_address1 + data.mzc_address2);
    }
    );
}

/**
 * Ajax で指定されたアクションへリクエストを行い、
 * 結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行います。
 *
 * @param url アクション先 URL
 * @param targetId リクエスト結果を追記無いし、上書きするID名
 * @param formId フォーム ID
 * @param mode 追記モード
 * @return
 */
function requestToId(url, targetId, formId, mode, reloadFlag) {
    // ◆form の値を一括取得
    var _form;
    _form += '&actionUrl=' + url;
    if (formId !== '') {
        _form += '&' + $('#' + formId).serialize();
    }
    if (reloadFlag === true) {
        _form += '&reload=true';
    }

    url = '/_SYSTEM_/transaction/controller.php?namespace=' + window.name;

    //------------------------------------------
    //■type="file" が存在するかをチェック
    //------------------------------------------
    var uploadFormCheck = false;
    if (formId !== null) {
        if ($('#' + formId + ' input[type="file"]').length > 0) {
            uploadFormCheck = true;
        }
    }

    //------------------------------------------
    //■POST、結果を新規コンテンツに埋める
    //------------------------------------------
    if (uploadFormCheck) {
        //alert('ファイルアップロードの場合');
        // ファイルアップロードの場合
        $('#' + formId).upload(url, _form, function(res) {
            if (mode === 'FRONT') {
                // 前方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(res + _html);
            } else if (mode === 'BACK') {
                // 後方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(_html + res);
            } else {
                // 上書き
                $('#' + targetId).html(res);
            }
        }, 'text');

    } else {
        //alert('ファイルアップロードじゃない場合');
        // ファイルアップロードじゃない場合
        $.post(url, _form, function(data, status){
            if (mode === 'FRONT') {
                // 前方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(data + _html);
            } else if (mode === 'BACK') {
                // 後方追記
                var _html = $('#' + targetId).html();
                $('#' + targetId).html(_html + data);
            } else {
                // 上書き
                $('#' + targetId).html(data);
            }
        });
    }


}



/**
 * 外部ドメインからMikoshivaに、Ajaxでアクセスしている場合、Mikoshiva外に出るときは必ずこれで移動します。
 *
 * @param url アクション先 URL
 * @param formId フォーム ID
 * @return
 */
/*
function leaveFromMikoshiva(url, formId) {
	namespace = window.name.slice(window.name.indexOf(":") + 1);
    $.get('/_SYSTEM_/transaction/leave.php' + '?namespace=' + namespace); // セッションを破棄

    document.location.href = url;
    window.name = '';
}
*/