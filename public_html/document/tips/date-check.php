<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Date - 日付チェック</span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1 id="日付チェック">日付チェック</h1>

            <p>日付のチェックを行います</p>



            <!-- ========================================
              ■指定された日付が昨日であるかのチェックする
            ==========================================-->
            <h2 id="指定された日付が昨日であるかのチェックする">指定された日付が昨日であるかのチェックする</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/08\');
var_dump($date->isYesterday()); // true


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/09\');
var_dump($date->isYesterday()); // false


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/10\');
var_dump($date->isYesterday()); // false

');
?>
            </pre>



            <!-- ========================================
              ■指定された日付が今日であるかのチェックする
            ==========================================-->
            <h2 id="指定された日付が今日であるかのチェックする">指定された日付が今日であるかのチェックする</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/08\');
var_dump($date->isToday()); // false


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/09\');
var_dump($date->isToday()); // true


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/10\');
var_dump($date->isToday()); // false

');
?>
            </pre>



            <!-- ========================================
              ■指定された日付が明日であるかのチェックする
            ==========================================-->
            <h2 id="指定された日付が明日であるかのチェックする">指定された日付が明日であるかのチェックする</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/08\');
var_dump($date->isToday()); // false


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/09\');
var_dump($date->isToday()); // false


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/10\');
var_dump($date->isToday()); // true

');
?>
            </pre>



            <!-- ========================================
              ■指定された日付がうるう年であるかのチェックする
            ==========================================-->
            <h2 id="指定された日付がうるう年であるかのチェックする">指定された日付がうるう年であるかのチェックする</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2004/03/08\');
var_dump($date->checkLeapYear()); // true


// 今日が 2010/03/09 である場合
$date = new Mikoshiva_Date(\'2010/03/09\');
var_dump($date->checkLeapYear()); // false

');
?>
            </pre>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















