<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>

        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">コンテンツ登録確認</h2>

            <!-- // 確認メッセージ -->
            <span>この内容で登録しますか？</span>
            <br>
            <br>

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                {hiddenLister hiddenList=$form}

                {include file='../application-ninsyo/product/views/scripts/content/detailTemplate.tpl' form=$form}

                <br>
                <hr>
                <br>

                {currentTabButton name='　　　戻る　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/confirm-back" tabId=$tabId  formId='inputForm'}
                　　　
                {currentTabButton name='　　　　　　　登録　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/content/complete" tabId=$tabId  formId='inputForm'}

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->