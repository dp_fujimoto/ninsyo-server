<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Zend/Validate/Date.php';
require_once 'Zend/View/Interface.php';
require_once 'Mikoshiva/View/Helper/FormSelect.php';
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Logger.php';

/**
 * 年のリストを作成します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/16
 * @version SVN:$Id: DayLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_DayLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;

    /**
     * 配列要素を指定された文字列により連結する
     *
     * @param $pieces 対象となる配列
     * @param string $glue 連結いする文字列
     * @return string 連結された文字列
     */
    public function dayLister($name, $selected, $attribs = null)
    {
        logInfo(LOG_START);
        $dayhList = array();
        $dayhList['']   = '';
        $dayhList['01'] = '01';
        $dayhList['02'] = '02';
        $dayhList['03'] = '03';
        $dayhList['04'] = '04';
        $dayhList['05'] = '05';
        $dayhList['06'] = '06';
        $dayhList['07'] = '07';
        $dayhList['08'] = '08';
        $dayhList['09'] = '09';
        $dayhList['10'] = '10';
        $dayhList['11'] = '11';
        $dayhList['12'] = '12';
        $dayhList['13'] = '13';
        $dayhList['14'] = '14';
        $dayhList['15'] = '15';
        $dayhList['16'] = '16';
        $dayhList['17'] = '17';
        $dayhList['18'] = '18';
        $dayhList['19'] = '19';
        $dayhList['20'] = '20';
        $dayhList['21'] = '21';
        $dayhList['22'] = '22';
        $dayhList['23'] = '23';
        $dayhList['24'] = '24';
        $dayhList['25'] = '25';
        $dayhList['26'] = '26';
        $dayhList['27'] = '27';
        $dayhList['28'] = '28';
        $dayhList['29'] = '29';
        $dayhList['30'] = '30';
        $dayhList['31'] = '31';

        // ◆selected 2009/12/24 00:00:00 で来た場合にも対応
        $_selected = $selected;
        if (isset($_selected)) {
            $_selected = str_replace('/', '-', $selected );
            $validator1 = new Zend_Validate_Date('Y-m-d H:i:s');
            $validator2 = new Zend_Validate_Date('Y-m-d');
            if ($validator1->isValid($_selected) || $validator2->isValid($_selected)) {

                $matche = array();
                preg_match('#^\d{4}-\d{2}-(\d{2})#', $_selected, $matche);
                $_selected = $matche[1];
            }
        }

        logInfo(LOG_END);

        return $this->view->formSelect($name,
                                       $_selected,
                                       $attribs,
                                       $dayhList);
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
