<?php
/** @package Mikoshiva_Controller */
require_once 'Zend/Controller/Router/Abstract.php';
require_once 'Zend/Controller/Router/Interface.php';



/**
 * 
 * 
 * @package Mikoshiva_Controller
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/22
 * @version SVN:$Id: Cli.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Controller_Router_Cli
    extends Zend_Controller_Router_Abstract
    implements Zend_Controller_Router_Interface {
    public function assemble($userParams, $name = null, $reset = false, $encode = true) {}
    public function route(Zend_Controller_Request_Abstract $dispatcher) {}
}