<?php
/**
 * @package Mikoshiva_Utility
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Exception.php';
/**
 * マッピングメソッドを持つクラスです。
 *
 * @package Mikoshiva_Utility
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/14
 * @version SVN:$Id: Mapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Mapping {
    /**
     * コードから、その示す「意味」を取得して返します。
     *
     * @param  string $code       コード
     * @return string $result     意味
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: Mapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function statusMapping($code) {



        // ログ開始
        logInfo(LOG_START);



        $result = '';
        $error  = '';

        //-----------------------------------------------------------
        // ■値が空であれば例外を投げる
        //-----------------------------------------------------------
        if(isBlankOrNull($code)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】入力された値が空です。";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }


        //-----------------------------------------------------------
        // ■値を取得
        //-----------------------------------------------------------
        $SHORT_CODE_MAP_LIST = array();
        $SHORT_CODE_MAP_LIST = Zend_Registry::get('SHORT_CODE_MAP_LIST'); //短縮コードのマッピングデータを取得

        foreach($SHORT_CODE_MAP_LIST as $value) {
            if (array_key_exists($code, $value) ) {
                $result = $value[$code];
            }

        }


        //-----------------------------------------------------------
        // ■マッピングデータの値が空であれば例外を投げる
        //-----------------------------------------------------------
        if(isBlankOrNull($result)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】”{$code}” は該当するステータス・タイプ、またはそれに対応する「意味」の記述がありません";
            logDebug($message);
            throw new Mikoshiva_Utility_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);
        return $result;
    }



    /**
     * カラム名からステータスのマッピングリストを取得します
     *
     * @param string $status     ステータス・タイプ
     * @param array  $deleteList 除外リスト
     * @return array 除外リストを除いたマッピングリスト
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: Mapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function statusMappingList($status, array $deleteList = array()) {

        // ログスタート
        logInfo(LOG_START);
        Mikoshiva_Logger::debug($status);


        // 引数のチェック
        if(isBlankOrNull($status)) {
            $error = 'ステータス・タイプが空です';
            throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Mapping#statusMappingList】' . $error);
        }


        // マッピングリストを取得
        $SHORT_CODE_MAP_LIST = array();
        $SHORT_CODE_MAP_LIST = Zend_Registry::get('SHORT_CODE_MAP_LIST');


        // マッピングの存在チェック
        if (!isset($SHORT_CODE_MAP_LIST[ $status ])) {
            $error = '指定された引数のマッピングリストは存在しません。';
            throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Mapping#statusMappingList】' . $error);
        }


        // マッピングリストを取得
        $map = array();
        $map = $SHORT_CODE_MAP_LIST[ $status ];


        // マッピングリストのキー名と除外リストの値がマッチするものを除去
        foreach ($deleteList as $v) {

            // マッピングリストに存在指定なれば新しい配列に詰める
            if (array_key_exists($v, $map) ) unset($map[ $v ]);
        }


        // ログエンド
        logInfo(LOG_END);

        return $map;
    }





    /**
     * ショップIDから、ショップ名を取得して返します。
     *
     * @param  string $shopId ショップID
     * @return string         ショップ名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/15
     * @version SVN:$Id: Mapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function gmoMapping($shopId) {
        logInfo(LOG_START);
        $error  = '';

        if(!isset($shopId)) {
            $error = 'ショップIDが空です';
            throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Mapping#gmoMapping】' . $error);
        }

        $GMO_CONFIG = array();
        $GMO_CONFIG = Zend_Registry::get('GMO_CONFIG'); //GMOのマッピングデータを取得

        if(isset($GMO_CONFIG['TEST']['SHOP'][$shopId]['NAME'])) {
            logInfo(LOG_END);
            return $GMO_CONFIG['TEST']['SHOP'][$shopId]['NAME'];
        }
        if(isset($GMO_CONFIG['REAL']['SHOP'][$shopId]['NAME'])) {
            logInfo(LOG_END);
            return $GMO_CONFIG['REAL']['SHOP'][$shopId]['NAME'];
        }

        $error  = 'ID該当するショップがありません';
        throw new Mikoshiva_Utility_Exception('【Mikoshiva_Utility_Mapping#gmoMapping】' . $error);

        logInfo(LOG_END);
        return;
    }

}
