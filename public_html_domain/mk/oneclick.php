<?php
	require_once './init.php';

	if (!isset($_SESSION['transactionNumber']) || !isset($_POST['transactionNumber']) || !isset($_SESSION['email'])) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:googleCheck:true]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$ret = oneclick($agent,$basePath,$errorLogTime);
		error_log($errorLogTime . '['  . session_id() . ']' . print_r($ret,true) . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		echo $ret;
		exit;
	}

	error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[case:COMPLETE]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
	if ($_SESSION['transactionNumber'] === $_POST['transactionNumber']) {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:セッションのトランザクションNoと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		if ($_SESSION['email'] !== sha1($_POST['email'])) {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:セッションのメールアドレスと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
	        header('Location: ../mikoshiva/'.$_SESSION['campaignSalesCode']);
	        break;
		} else {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[else:セッションのメールアドレスと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

            // キャンペーンタイプをセッションに保存
            $_SESSION['campaignType'] = 'CAMPAIGN_TYPE_UP_SELL';
            // プレビューキーをセッションに保存
            $_SESSION['previewKey'] = '';

            $response = $agent->getResponseByPost($basePath.'complete', $_POST);
            $f = explode("\n", $response);
			$ret = '';
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ .  '[foreach:ループ開始]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
			foreach ($f as $key => $val) {
				error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[foreach:ループキー:'.$key . ']' . '(' . __LINE__ . ')', 3, './logs/controller/'.date('Y-m-d').'.log');

				$search = array();

				// 値引き渡し用特殊タグ検索
				// 購入履歴ID
				if (preg_match('/^.*<miko.*id=\"pid\".*?>(.*)<\/miko>/',$val,$search) === 1) {
					error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:購入履歴ID用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
					if (!isset($_SESSION['purchaseId'])) {
						error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:セッションに購入履歴IDなし]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
						$_SESSION['purchaseId'] = $search[1];
					}
					error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
					continue;
				}
				// 顧客ID
				if (preg_match('/^.*<miko.*id=\"customerId\".*?>(.*)<\/miko>/',$val,$search) === 1) {
					error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:顧客ID用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
					if (!isset($_SESSION['customerId'])) {
						error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:セッションに顧客IDなし]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
						$_SESSION['customerId'] = $search[1];
					}
					error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
					continue;
				}
				// 発送先番号
				if (preg_match('/^.*<miko.*id=\"addressNumber\".*?>(.*)<\/miko>/',$val,$search) === 1) {
					error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:発送先番号用特殊タグと一致]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
					if (!isset($_SESSION['addressNumber'])) {
						error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:セッションに発送先番号なし]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
						$_SESSION['addressNumber'] = $search[1];
					}
					error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[continue:コンティニュー]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
					continue;
				}

				$ret .= $val ."\n";
			}
			echo $ret."\n";
	        $_SESSION['transactionNumber'] = '2';
		}
	} else {
		error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[else:セッションのトランザクションNoと一致しない]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
		$transactionNumber = $_SESSION['transactionNumber'];
		if ($transactionNumber !== '1') {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[if:トランザクションNoが１ではない]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');

			// 初回決済が完了しているのでアップセルに設定
//			$_POST['campaignType'] = 'CAMPAIGN_TYPE_UP_SELL';
			$_POST['campaignType'] = $_SESSION['campaignType'];
			$_POST['previewKey'] = $_SESSION['previewKey'];
			if (isset($_SESSION['addressNumber'])) {
				$_POST['addressNumber'] = $_SESSION['addressNumber'];
			}
			$_POST['transactionNumber'] = $_SESSION['transactionNumber'];
			if (isset($_SESSION['customerId'])) {
				$_POST['customerId'] = $_SESSION['customerId'];
			}

    		// mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'oneclick', $_POST);

            echo $response;
		} else {
			error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[else:トランザクションNoが１]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
    		// mikoshivaにpost
            $response = $agent->getResponseByPost($basePath.'error', $params);

            echo $response;
		}
	}
    error_log($errorLogTime . '['  . session_id() . ']' . __FILE__ . '[break:ブレイク]' . '(' . __LINE__ . ')' . "\n", 3, './logs/controller/'.date('Y-m-d').'.log');
