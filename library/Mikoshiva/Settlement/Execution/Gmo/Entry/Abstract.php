<?php
/** @package Mikoshiva_Gmo */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Entry/Register.php';

/**
 * 取引登録抽象クラス
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Settlement_Execution_Gmo_Entry_Abstract extends Mikoshiva_Settlement_Execution_Gmo_Abstract {


    /**
     * 処理区分
     * @var string 処理区分
     */
    protected $_jobCd;


    /**
     * 初期化処理
     *
     * @param Popo_Abstract $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Entry */
        $inputPopo;


        // 固定値を POPO に詰める
        $inputPopo->setShopPass($gmoConfig['SHOP'][$inputPopo->getShopID()]['SHOP_PASS']); // ショップパスワード
        $inputPopo->setJobCd($this->_jobCd);                                               // ジョブコード


        // GMO 結果 POPO クラスをセット
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['ENTRY_PGM'];

        // dumper($this->_gmoConnectioniUrl);
        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }


    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Exec/Mikoshiva_Settlement_Gmo_Exec_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/04
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Exception.php';


        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Entry */
        $inputPopo;

        // ショップ ID
        if(isBlankOrNull($inputPopo->getShopID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ショップ ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Entry_Exception($message);
        }


        // ショップパスワード
        if(isBlankOrNull($inputPopo->getShopPass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】ショップパスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Entry_Exception($message);
        }


        // オーダー ID
        if(isBlankOrNull($inputPopo->getOrderID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】オーダー ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Entry_Exception($message);
        }


        // 処理区分
        if(isBlankOrNull($inputPopo->getJobCd())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】処理区分 は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Entry_Exception($message);
        }


        // 利用金額
        if(isBlankOrNull($inputPopo->getAmount())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】利用金額 は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Entry_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);


        return true;
    }
}












