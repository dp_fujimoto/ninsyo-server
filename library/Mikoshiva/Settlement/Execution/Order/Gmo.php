<?php
/**
 * @package Mikoshiva_Settlement
 */

/**
 * require_once
 */
require_once 'Mikoshiva/Settlement/Execution/Order/Abstract.php';
require_once 'Mikoshiva/Utility/Gmo.php';
require_once 'Mikoshiva/Settlement/Popo/Input/Order/Work/List.php';
require_once 'Mikoshiva/Settlement/Popo/Input/Order/Work.php';
require_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Alter.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement/Result/List.php';


/**
 * 決済処理実行クラスの Abstract
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/08
 * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Order_Gmo extends Mikoshiva_Settlement_Execution_Order_Abstract {




    private $_isDebit = false;


    /**
     * 実行クラス
     *
     * 【メモ】
     * ０．仮売上の場合仮、２回決済処理を行う。１回目：【標準決済】、２回目：【仮売上決済】
     * １．デビットカードで仮売上商品を購入した場合、【標準決済】のみ。仮売上は３０日後とかに手動で決済する（バッチ？）
     * ２．仮売上商品をカード分割で購入した場合、１回目：一括決済、２回目：分割決済
     * ３．標準決済はすべて会員 ID を発行する。仮売上は標準 ID と同じものを使用する。
     * ４．仮売上のオーダー ID に実売上にする日を含める。 例： DP-0000090713-002-201003-00
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/08
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function execute() {


        $orderInputPopo = null;
        $orderInputPopo = $this->_orderInputPopo;



        //---------------------------------------------------------------------
        //
        // ■クレジットカードがデビットカードであるかどうかを判定する
        //
        //---------------------------------------------------------------------
        $this->_isDebit = Mikoshiva_Utility_Gmo::isDebitCard($orderInputPopo->getCardNo());


        //---------------------------------------------------------------------
        //
        // ■テストクレジットカードで有った場合にテストショップ ID にセットしなおす
        //
        //---------------------------------------------------------------------
        $ret = null;
        $ret = $this->_resetShopId($orderInputPopo);
//dumper($orderInputPopo);
//exit;

        //--------------------------------------------------------------------
        //
        // ■決済データを作成
        //
        //--------------------------------------------------------------------
        $settlementDataList = array();
        $settlementDataList = $this->_createSettelmentList($orderInputPopo);



        //--------------------------------------------------------------------
        //
        // ■決済処理を実行
        //
        // ※ GMO の決済に失敗した場合、キャンセル処理を行う
        // ※ システムエラーが発生した場合、キャンセル処理を行ってから再度例外を投げる
        //
        //--------------------------------------------------------------------
        try {

            // ◆決済
            $ret = $this->_settlementExecute($settlementDataList);
//dumper($settlementDataList);exit;
            // ◆決済処理に失敗したらキャンセル処理を行う
            if (! $ret) {
                $this->_settlementCancel($settlementDataList);
                //return false;
            }
//dumper($settlementDataList);exit;
            //throw new Exception('テストエラー');
        } catch (Exception $e) {

            // ◆キャンセル処理
            try {
                $this->_settlementCancel($settlementDataList);
            } catch (Exception $e2) {
                errorMail($e2, '決決済エラーが発生！キャンセル処理終了ＮＧ');
                throw $e;
            }

            // ◆例外を投げる
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】決済処理でシステムエラーが発生しました。" . $e->getMessage();
            logDebug($message);
            //throw new Exception($message);
            errorMail($e, '決済エラーが発生！キャンセル処理終了ＯＫ');
            throw $e;
        }

//dumper($settlementDataList);exit;


        //--------------------------------------------------------------------
        //
        // ■処理結果を詰め直す
        //
        //--------------------------------------------------------------------
        $output = null;
        $output = $this->_createSettlementOutput($orderInputPopo, $settlementDataList);
        $this->_orderOutputPopo = $output;


        //--------------------------------------------------------------------
        //
        // ■GMO 決済履歴テーブルに登録する
        //
        //--------------------------------------------------------------------
        if ($ret) {
            //$this->_createTransactionHistory($orderInputPopo, $output);
            $this->_createTransactionHistory2($orderInputPopo, $settlementDataList);
        }

        return $ret;
    }


    /**
     * テスト用のクレジットカード番号で購入された際に強制的にテスト用のショップ ID に切り替えます
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/07
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _resetShopId(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo) {


        // ▼テストクレジットカードであるかをチェック
        if (! Mikoshiva_Utility_Gmo::isTestCreditCard($orderInputPopo->getCardNo())) {

            // テストクレジットカードで無ければリターン
            return true;
        }

        // ▼productList
        $ProductDataList = array();
        $ProductDataList = $orderInputPopo->getProductDataList();


        // ▼ショップ ID をセットし直します
        /* @var $productData Mikoshiva_Settlement_Popo_Input_Order_ProductInfo */
        foreach ($ProductDataList as $key => $productData) {

            // ショップ ID を取り出す
            $shopId = '';
            $shopId = $productData->getMstProduct()->getShopId();

            // ショップ情報を取得
            $shopInfo = array();
            $shopInfo = Mikoshiva_Utility_Gmo::getShopInfo($shopId);

            // ショップ ID をセットし直します
            $productData->getMstProduct()->setShopId($shopInfo['TEST_SHOP_ID']);
        }
    }














    /**
     * 決済データリストを生成します
     *
     * １．標準決済はショップ毎の合計金額で決済を掛ける他の商品は有効性チェックで決済を掛ける。仮売上は商品毎に決済する。
     *
     * @param Mikoshiva_Settlement_Popo_Input_Order $orderPopo
     * @return array 決済データリスト
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/26
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _createSettelmentList(Mikoshiva_Settlement_Popo_Input_Order $orderPopo) {


        //--------------------------------------------------------------------
        //
        // ■初期化
        //
        //--------------------------------------------------------------------
        $settlementDataList       = array(); // 決済データリスト
        $shopNomalTotalAmountList = array(); // ショップ毎の合計金額（標準決済のみ、仮売上は商品毎に決済する）



        //--------------------------------------------------------------------
        //
        // ■GMO の決済で使用する決済データリストを生成します
        //
        //--------------------------------------------------------------------
        foreach ($orderPopo->getProductDataList() as $productInfoNumber => $productInfoClass) {

            /* @var $productInfoClass Mikoshiva_Settlement_Popo_Input_Order_ProductInfo */
            $productInfoClass;


            //--------------------------------------------------------------------
            //
            // ▼標準決済データの生成▼
            //
            //--------------------------------------------------------------------
            // ▼作業クラス ----------------------------------------------------------------
            $workInputPopo = null;
            $workInputPopo = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
            $workInputPopo->setNormal(new Mikoshiva_Settlement_Popo_Input_Order_Work());


            // ▼標準決済クラスを取得 ----------------------------------------------------------------
            $nomalClass = null;
            $nomalClass = $workInputPopo->getNormal();
            $nomalClass->setSettlementOutputPopoList(new Mikoshiva_Settlement_Popo_Output_Order_Settlement_Result_List()); // 処理結果リストクラス
            $nomalClass->setInputProductInfo($productInfoClass);


            // ▼ショップ ID 毎に金額を合計する --------------------------------------------------
            $shopId = null;
            $shopId = $productInfoClass->getMstProduct()->getShopId();
            if (! isset($shopTotalAmountList[$shopId])) {

                // ◆初期化
                $shopTotalAmountList[$shopId]      = 0;
                $shopNomalTotalAmountList[$shopId] = array();
            }


            // ▼会員 ID ----------------------------------------------------------------
            $memberId = null;
            $memberId = Mikoshiva_Utility_Gmo::createGmoMemberId($orderPopo->getCustomerId(), $productInfoClass->getPaymentNumber());
            $nomalClass->setMemberID($memberId);


            // ▼オーダー ID ----------------------------------------------------------------
            $orderId = null;
            $orderId = Mikoshiva_Utility_Gmo::createGmoOrderId($orderPopo->getCustomerId(), $productInfoClass->getPaymentNumber());
            $nomalClass->setOrderID($orderId);


            // ▼カード番号 ----------------------------------------------------------------
            $cardNo = null;
            $cardNo = $orderPopo->getCardNo();
            $nomalClass->setCardNo($cardNo);


            // ▼有効期限 ----------------------------------------------------------------
            $expire = null;
            $expire = $orderPopo->getExpire();
            $nomalClass->setExpire($expire);


            // ▼ショップ ID ----------------------------------------------------------------
            $shopId = null;
            $shopId = $productInfoClass->getMstProduct()->getShopId();
            $nomalClass->setShopID($shopId);


            // ▼金額 --------------------------------------------------------------------------------
            if ($productInfoClass->getProvisorySalesFlag() === '1') {

                // ◆仮売上商品で有れば標準決済（１回目）は送料のみをセット
                $shippingCost = 0;
                $shippingCost = $productInfoClass->getMstCampaignProduct()->getShippingCost();

                $shopTotalAmountList[$shopId] += $shippingCost; // 合計金額を足してゆく
                $nomalClass->setAmount(0);                      // 後で合計金額をセットするので取り敢えず 0 を入れる

            } else {

                // ◆仮売上商品で無ければ送料と初回価格（本体金額）の合計値をセット
                /****************************************************************
                $price        = 0; // 初回価格（本体金額）
                $shippingCost = 0; // 送料
                $amount       = 0; // 合計金額（初回価格（本体金額）+ 送料）

                $price        = $productInfoClass->getMstCampaignProduct()->getPrice();
                $shippingCost = $productInfoClass->getMstCampaignProduct()->getShippingCost();
                $amount       = $price + $shippingCost;
                ********************************************************************/

                // セット
                $shopTotalAmountList[$shopId] += $productInfoClass->getPaymentAmount(); // 合計金額を足してゆく
                $nomalClass->setAmount(0);         // 後で合計金額をセットするので取り敢えず 0 を入れる
            }

            // ▼アクセス ID (取引登録終了後にセット)
            //$nomalClass->setAccessID();

            // ▼クセスパスワード （取引登録後にセット）
            //$nomalClass->setAccessPass();


            // ▼支払い方法 ＆ 支払い回数 ---------------------------------------------------
            $provisorySalesFlag = null;
            $provisorySalesFlag = $productInfoClass->getProvisorySalesFlag();
            if (($orderPopo->getMethod() === 'CARD_DIVIDE_TYPE_SINGLE')
                || ($provisorySalesFlag === '1')
            ) {

                // ◆仮売上商品で有れば標準決済（１回目）は分割決済であっても一括決済とする
                $nomalClass->setMethod('1'); // 一括

            } else if ($orderPopo->getMethod() === 'CARD_DIVIDE_TYPE_MONTHLY') {

                // ◆分割決済
                $nomalClass->setMethod('2');                         // 分割
                $nomalClass->setPayTimes($orderPopo->getPayTimes()); // 支払回数

            } else {

                // ◆それ以外は規定外エラー（ここより前の処理でバリデータに引っかかるはず）
                include_once 'Mikoshiva/Settlement/Execution/Order/Exception.php';
                $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】規定外の支払い方法が設定されました。";
                logDebug($message);
                throw new Mikoshiva_Settlement_Execution_Order_Exception($message);
            }


            //--------------------------------------------------------------------
            //
            // ▼仮売上データの生成▼
            //
            //   売上商品でかつ、デビットカードでの購入で無ければ、仮売上の決済データを作成する
            //--------------------------------------------------------------------
            $provisorySalesFlag = null;  // 仮売上フラグ
            $isDebitCard          = false; // デビットカードであるか
            $provisorySalesFlag = $productInfoClass->getProvisorySalesFlag();
            $isDebitCard          = Mikoshiva_Utility_Gmo::isDebitCard($orderPopo->getCardNo());
            //if (($provisorySalesFlag === '1')
            //    && (Mikoshiva_Utility_Gmo::isDebitCard($orderPopo->getCardNo()) === false)
            if ($provisorySalesFlag === '1') {


                // ▼仮売上決済クラスを取得 ----------------------------------------------------------------
                $workInputPopo->setAuth(new Mikoshiva_Settlement_Popo_Input_Order_Work());
                $authClass = null;
                $authClass = $workInputPopo->getAuth();
                $authClass->setSettlementOutputPopoList(new Mikoshiva_Settlement_Popo_Output_Order_Settlement_Result_List()); // 処理結果リストクラス
                $authClass->setInputProductInfo($productInfoClass);


                // ▼会員 ID ---------------------------------------------------------------------------------------------------------------
                $memberId = null;
                $memberId = Mikoshiva_Utility_Gmo::createGmoMemberId($orderPopo->getCustomerId(), $productInfoClass->getPaymentNumber());
                $authClass->setMemberID($memberId);


                // ▼オーダー ID -------------------------------------------------------------------------------------------------------------
                $orderId = null;
                $orderId = Mikoshiva_Utility_Gmo::createGmoOrderId(
                                $orderPopo->getCustomerId(),
                                $productInfoClass->getPaymentNumber(),
                                $productInfoClass->getMstCampaignProduct()->getFreePeriodTypeCreditCard(),
                                $productInfoClass->getMstCampaignProduct()->getFreePeriodCreditCard()
                            );
                $authClass->setOrderID($orderId);


                // ▼カード番号 ---------------------------------------------------------------------------------------------------------------
                $cardNo = null;
                $cardNo = $orderPopo->getCardNo();
                $authClass->setCardNo($cardNo);


                // ▼有効期限 ----------------------------------------------------------------------------------------------------------------
                $expire = null;
                $expire = $orderPopo->getExpire();
                $authClass->setExpire($expire);


                // ▼ショップ ID -------------------------------------------------------------------------------------------------------------
                $shopId = null;
                $shopId = $productInfoClass->getMstProduct()->getShopId();
                $authClass->setShopID($shopId);


                // ▼金額 -------------------------------------------------------------------------------------------------------------------------
                //   ※仮売上は初回価格（本体金額）のみをセットする
                $price = 0; // 初回価格（本体金額）
                $price = $productInfoClass->getMstCampaignProduct()->getPrice();
                $authClass->setAmount($price);


                // ▼アクセス ID (取引登録終了後にセット) ------------------------------------------------------------------------------------------
                //$nomalClass->setAccessID();


                // ▼クセスパスワード （取引登録後にセット） ----------------------------------------------------------------------------------------
                //$nomalClass->setAccessPass();


                // ▼支払い方法 ＆ 支払い回数 -------------------------------------------------------------------------------------------------------
                if ($orderPopo->getMethod() === 'CARD_DIVIDE_TYPE_SINGLE') {

                    // ◆一括決済
                    $authClass->setMethod('1'); // 一括

                } else if($orderPopo->getMethod() === 'CARD_DIVIDE_TYPE_MONTHLY') {

                    // ◆クレジット分割決済
                    $authClass->setMethod('2');                         // 分割
                    $authClass->setPayTimes($orderPopo->getPayTimes()); // 支払回数

                } else {

                    // ◆それ以外は規定外エラー（ここより前の処理でバリデータに引っかかるはず）
                    include_once 'Mikoshiva/Settlement/Execution/Order/Exception.php';
                    $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】規定外の支払い方法が設定されました。";
                    logDebug($message);
                    throw new Mikoshiva_Settlement_Execution_Order_Exception($message);
                }
            }


            // ▼配列に詰める
            $shopId                        = null;
            $shopId                        = $productInfoClass->getMstProduct()->getShopId();
            $settlementDataList[$shopId][] = $workInputPopo;
        }



        //---------------------------------------------------------------------------
        //
        // ■ショップ毎の合計金額をセットする（標準決済のみ）
        //
        //---------------------------------------------------------------------------
        foreach ($settlementDataList as $shopId => $shopDataList) {


            // ショップの金額を取得
            $amount = 0;
            $amount = $shopTotalAmountList[$shopId];

            // セット
            /* @var $workInputPopo Mikoshiva_Settlement_Popo_Input_Order_Work_List */
            $workInputPopo = null;
            $workInputPopo = $shopDataList[0]; // 0 にのみセットする
            $workInputPopo->getNormal()->setAmount($amount);
        }


        // $settlementDataList[ショップID][0] = Mikoshiva_Settlement_Popo_Input_Order_Work_List
        return $settlementDataList;
    }


    /**
     * 決済処理を行います
     *
     * @param array $settlementDataList Mikoshiva_Settlement_Popo_Input_Order_Work_List[]
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/26
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _settlementExecute(array $settlementDataList) {


        // $settlementDataList['shopId_01'][0] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
        // $settlementDataList['shopId_02'][0] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
        foreach ($settlementDataList as $settlementDataShopId => $settlementDataShopDataList) { // foreach:01
//dumper('--------------------------------  ' . $settlementDataShopId);

            // $settlementDataShopDataList[0] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
            // $settlementDataShopDataList[1] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
            foreach ($settlementDataShopDataList as $index => $settlementDataList) { // foreach:02
//dumper('--------------------------------  ' . $index);
                /* @var $settlementDataList Mikoshiva_Settlement_Popo_Input_Order_Work_List */
                $settlementDataList;
                // $settlementDataList[normal] = new Mikoshiva_Settlement_Popo_Input_Order_Work();
                // $settlementDataList[auth]   = new Mikoshiva_Settlement_Popo_Input_Order_Work();
                foreach ($settlementDataList->getProperties() as $settlementKey => $settlementData) { // foreach:03

//dumper('--------------------------------  ' . $settlementKey);//exit;
                    /* @var $settlementData Mikoshiva_Settlement_Popo_Input_Order_Work */
                    $settlementData;
//dumper($settlementData);//exit;
                    //--------------------------------------------------------------------
                    //
                    // ▼normal or auth 意外はコンテニュー
                    //
                    //--------------------------------------------------------------------
                    if ($settlementKey !== 'normal' && $settlementKey !== 'auth') {
//dumper('--------------------------------  ▼normal or auth 意外はコンテニュー');
                        continue;
                    }


                    //--------------------------------------------------------------------
                    //
                    // ▼仮売上で有り、$settlementData（決済データ）が null または、デビットカードで有ればコンテニュー
                    //
                    //--------------------------------------------------------------------
                    if ($settlementKey === 'auth' && ($settlementData === null || $this->_isDebit === true)) {
//dumper('--------------------------------  ▼仮売上商品フラグが false で有ればコンテニュー');
                        continue;
                    }

//throw new Exception('エラーテスト');
                    //--------------------------------------------------------------------
                    //
                    // ▼キャンセル処理を初期化
//dumper('--------------------------------  ▼キャンセル処理を初期化');
                    //--------------------------------------------------------------------
                    include_once 'Mikoshiva/Settlement/Popo/Output/Order.php';
                    $cancelInputPopo = null;
                    $cancelInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Alter();
                    $cancelInputPopo->setShopID($settlementData->getShopID());        // 会員 ID をセット
                    $settlementData->setCancelInputPopo($cancelInputPopo);              // 決済データクラスにセット
//dumper($settlementData);//exit;
//throw new Exception('エラーテスト');
                    //--------------------------------------------------------------------
                    //
                    // ■処理開始
                    //
                    //------------------------------------------------------------------
                    logInfo('●会員ID：     ' . $settlementData->getMemberID());
                    logInfo('●オーダーID： ' . $settlementData->getOrderID());
                    logInfo('●ショップID： ' . $settlementData->getShopID());



                    //--------------------------------------------------------------------
                    //
                    // ▼会員登録
                    //   ※標準決済のみ
//dumper('--------------------------------  ▼会員登録');
                    //--------------------------------------------------------------------
                    if ($settlementKey === 'normal') { // if:01
                        logInfo('▼会員登録開始');


                        // ◆inputPopo から会員登録 POPO にコピー
                        logInfo('◆inputPopo から会員登録 POPO にコピー');
                        include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Member/Save.php';
                        $memberInputPopo = null;
                        $memberInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Member_Save();
                        Mikoshiva_Utility_Copy::copyToPopo($memberInputPopo, $settlementData);
//dumper($memberInputPopo);//exit;


                        // ◆会員登録実行
                        logInfo('◆会員登録実行');
                        include_once 'Mikoshiva/Settlement/Execution/Gmo/Member/Save.php';
                        $memberExec = null;
                        $memberExec = new Mikoshiva_Settlement_Execution_Gmo_Member_Save($memberInputPopo);
                        $ret        = $memberExec->execute();
//dumper($ret);//exit;


                        // ◆結果を取得
                        logInfo('◆結果を取得');
                        $memberOutputPopo = null;
                        $memberOutputPopo = $memberExec->getOutput();
                        $settlementData->getSettlementOutputPopoList()->setMember($memberOutputPopo);
//dumper($memberOutputPopo);//exit;


                        // ◆エラー処理
                        if (! $ret)  {
                            logInfo('◆エラー処理');
                            return false;
                        }



                        //--------------------------------------------------------------------
                        //
                        // ▼カード登録
                        //   ※標準決済のみ
//dumper('--------------------------------  ▼カード登録');
                        //--------------------------------------------------------------------
                        logInfo('▼カード登録');
                        // ◆inputPopo から会員登録 POPO にコピー
                        logInfo('◆inputPopo から会員登録 POPO にコピー');
                        include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Card/Save.php';
                        $cardInputPopo = null;
                        $cardInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Card_Save();
                        Mikoshiva_Utility_Copy::copyToPopo($cardInputPopo, $settlementData);
//dumper($cardInputPopo);//exit;
//if ($settlementDataShopId === 'testpg0000591') $cardInputPopo->setCardNo('4999000000000003');
                        // ◆会員登録実行クラス
                        logInfo('会員登録実行');
                        $cardExec = null;
                        include_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Save/Register.php';
                        $cardExec = new Mikoshiva_Settlement_Execution_Gmo_Card_Save_Register($cardInputPopo);
                        $ret      = $cardExec->execute();
//dumper($ret);//exit;
                        // ◆結果を取得
                        logInfo('◆結果を取得');
                        $cardOutputPopo = null;
                        $cardOutputPopo = $cardExec->getOutput();
                        $settlementData->getSettlementOutputPopoList()->setCard($cardOutputPopo);
//dumper($cardOutputPopo);//exit;
                        // ◆エラー処理
                        if (! $ret)  {
                        logInfo('◆エラー処理');
                            return false;
                        }
                    } // if:01



                    //--------------------------------------------------------------------
                    //
                    // ▼取引登録
//dumper('--------------------------------  ▼取引登録');
                    //--------------------------------------------------------------------
                    logInfo('▼取引登録');
                    // ◆inputPopo から会員登録 POPO にコピー
                    logInfo('◆inputPopo から会員登録 POPO にコピー');
                    include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Entry.php';
                    $entryInputPopo = null;
                    $entryInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Entry();
                    Mikoshiva_Utility_Copy::copyToPopo($entryInputPopo, $settlementData);
//dumper($entryInputPopo);//exit;

                    // ◆取引登録実行クラスを取得
                    logInfo('◆取引登録実行クラスを取得');
                    $entryExec = null;
                    switch (true) {

                        // 仮売上 ----------------------------------------------------
                        //case ($entryInputPopo->getAmount() === 0):
                        case ($settlementKey === 'auth'):
                            include_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Auth.php';
                            $entryExec = new Mikoshiva_Settlement_Execution_Gmo_Entry_Auth($entryInputPopo);
                            break;

                        // 0 円だったら有効性チェック ----------------------------------------------------
                        case ($entryInputPopo->getAmount() === 0):
                            include_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Check.php';
                            $entryExec = new Mikoshiva_Settlement_Execution_Gmo_Entry_Check($entryInputPopo);
                            break;

                        // 1 円以上だったら即時売上 ----------------------------------------------------
                        case ($entryInputPopo->getAmount() !== 0):
                            include_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Capture.php';
                            $entryExec = new Mikoshiva_Settlement_Execution_Gmo_Entry_Capture($entryInputPopo);
                            break;

                        default:
                            include_once 'Mikoshiva/Settlement/Execution/Order/Exception.php';
                            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】該当する GMO 取引登録実行クラスがありません。";
                            logDebug($message);
                            throw new Mikoshiva_Settlement_Execution_Order_Exception($message);
                    }

//dumper(get_class($entryExec));//exit;
                    // ◆実行
                    logInfo('◆実行');
                    $ret = $entryExec->execute();
//dumper($ret);//exit;
                    // ◆結果を取得
                    logInfo('◆結果を取得');
                    $entryOutputPopo = null;
                    $entryOutputPopo = $entryExec->getOutput();
                    $settlementData->getSettlementOutputPopoList()->setEntry($entryOutputPopo);
//dumper($entryOutputPopo);//exit;
                    // ◆エラー処理
                    if (! $ret)  {
                    logInfo('◆エラー処理');
                        return false;
                    }

                    // ▼取引 ID と取引パスワードをセット▼ ------------------------------------------------------------
                    // ◆決済データ
                    $settlementData->setAccessID($entryOutputPopo->getAccessID());
                    $settlementData->setAccessPass($entryOutputPopo->getAccessPass());

                    // ◆キャンセルデータ
                    $cancelInputPopo->setAccessID($entryOutputPopo->getAccessID());
                    $cancelInputPopo->setAccessPass($entryOutputPopo->getAccessPass());
//dumper($settlementData);//exit;

//if ($settlementDataShopId === 'testpg0000591') throw new Exception('エラーテスト');

// return false;
                    //--------------------------------------------------------------------
                    //
                    // ▼決済実行
//dumper('--------------------------------  ▼決済実行');
                    //--------------------------------------------------------------------
                    logInfo('◆決済実行');
                    // ◆inputPopo から会員登録 POPO にコピー
                    logInfo('◆inputPopo から会員登録 POPO にコピー');
                    include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Exec/Run.php';
                    $execInputPopo = null;
                    $execInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Run();
                    Mikoshiva_Utility_Copy::copyToPopo($execInputPopo, $settlementData);
//dumper($execInputPopo);//exit;

                    // ◆決済実行クラス
                    logInfo('◆決済実行クラス');
                    include_once 'Mikoshiva/Settlement/Execution/Gmo/Exec/Run.php';
                    $execExec = null;
                    $execExec = new Mikoshiva_Settlement_Execution_Gmo_Exec_Run($execInputPopo);
                    $ret      = $execExec->execute();

                    // ◆結果を取得
                    logInfo('◆結果を取得');
                    $execOutputPopo = null;
                    $execOutputPopo = $execExec->getOutput();
                    $settlementData->getSettlementOutputPopoList()->setExec($execOutputPopo);

                    // ◆エラー処理
                    if (! $ret)  {
                    logInfo('◆エラー処理');
                        return false;
                    }
//dumper($ret);//exit;
//dumper($entryOutputPopo);//exit;
                } // foreach:01
            } // foreach:02
        } // foreach:03

        return true;
    }


    /**
     *キャンセル処理を行います
     *
     * @param $outputIput
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/29
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _settlementCancel(array $settlementDataList) {


        // ログ開始
        //logInfo(LOG_START);



        // $settlementDataList['shopId_01'][0] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
        // $settlementDataList['shopId_02'][0] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
        foreach ($settlementDataList as $settlementDataShopId => $settlementDataShopDataList) { // foreach:01
//dumper('--------------------------------  ' . $settlementDataShopId);

            // $settlementDataShopDataList[0] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
            // $settlementDataShopDataList[1] = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
            foreach ($settlementDataShopDataList as $index => $settlementDataList) { // foreach:02
//dumper('--------------------------------  ' . $index);
                /* @var $settlementDataList Mikoshiva_Settlement_Popo_Input_Order_Work_List */
                $settlementDataList;

                // $settlementDataList[normal] = new Mikoshiva_Settlement_Popo_Input_Order_Work();
                // $settlementDataList[auth]   = new Mikoshiva_Settlement_Popo_Input_Order_Work();
                foreach ($settlementDataList->getProperties() as $settlementKey => $settlementData) { // foreach:03

                    /* @var $settlementData Mikoshiva_Settlement_Popo_Input_Order_Work */
                    $settlementData;


                    //--------------------------------------------------------------------
                    // ▼normal or auth 意外はコンテニュー
                    //--------------------------------------------------------------------
                    if ($settlementKey !== 'normal' && $settlementKey !== 'auth') {
//dumper('--------------------------------  ▼normal or auth 意外はコンテニュー（' . __LINE__ . '）');
                        continue;
                    }


                    //--------------------------------------------------------------------
                    // ▼仮売上で有り、$settlementData（決済データ）が null または、デビットカードで有ればコンテニュー
                    //--------------------------------------------------------------------
                    if ($settlementKey === 'auth' && ($settlementData === null || $this->_isDebit === true)) {
//dumper('--------------------------------  ▼仮売上商品フラグが false で有ればコンテニュー（' . __LINE__ . '）');
                        continue;
                    }
//dumper($settlementKey);
//dumper($settlementData->getCancelInputPopo());
                    //--------------------------------------------------------------------
                    // ▼取引 ID が無ければコンテニュー
                    //--------------------------------------------------------------------
                    if ($settlementData->getSettlementOutputPopoList()->getEntry()->getAccessID()  === null) {
//dumper('--------------------------------  ▼取引 ID が無ければコンテニュー（' . __LINE__ . '）');
                        continue;
                    }


                    //--------------------------------------------------------------------
                    // ▼金額が 0 円で有ればコンテニュー
                    //   このままキャンセル処理を行うとエラーになります。
                    //
                    //   E01050004 指定した処理区分の処理は実行出来ません。
                    //--------------------------------------------------------------------
                    if ($settlementData->getAmount() === 0) {
//dumper('--------------------------------  ▼金額が 0 円で有ればコンテニュー（' . __LINE__ . '）');
                        continue;
                    }



                    //------------------------------------------------------------------
                    //
                    // ▼処理開始
                    //
                    //------------------------------------------------------------------
                    logInfo('▼--------------処理データ--------------▼');
                    logInfo('●会員ID：     ' . $settlementData->getMemberID());
                    logInfo('●オーダーID： ' . $settlementData->getOrderID());
                    logInfo('●ショップID： ' . $settlementData->getShopID());



                    //--------------------------------------------------------------------
                    // ▼キャンセル処理
//dumper('--------------------------------  ▼キャンセル処理（' . __LINE__ . '）');
                    //--------------------------------------------------------------------
                    logInfo('▼キャンセル処理');
                    include_once 'Mikoshiva/Settlement/Execution/Gmo/Alter/Void.php';
                    $cancel = new Mikoshiva_Settlement_Execution_Gmo_Alter_Void($settlementData->getCancelInputPopo());
                    $ret = $cancel->execute();
                    //dumper($ret);


                    //--------------------------------------------------------------------
                    // ▼処理結果を取得
//dumper('--------------------------------  ▼処理結果を取得（' . __LINE__ . '）');
                    //--------------------------------------------------------------------
                    logInfo('▼処理結果を取得');
                    $output = null;
                    $output = $cancel->getOutput();
                    //dumper($output);
                    //dumper($settlementDataShopId);
                }
            }
        }
    }







    /**
     * 処理結果クラスを詰め直す
     *
     * メモ
     * １．上に返す値において、標準決済の取引 ID 取引パスはショップ毎に共通のものを使用する
     * ２．仮売上商品は標準決済は送料のみ、仮売上は本体金額のみで決済を行う
     *     ▼通常商品
     *       ●標準決済
     *         取引 ID PASS  --------- 同じ（ショップ ID 毎に共通のものを使用）
     *         オーダー ID   --------- 別
     *         本体金額      --------- 別
     *         送料          --------- 別
     *
     *     ▼仮売上商品
     *       ●標準決済
     *         取引 ID PASS  --------- 同じ（ショップ ID 毎に共通のものを使用）
     *         オーダー ID   --------- 別
     *         本体金額      --------- 別
     *         送料          --------- 0 でセット
     *
     *       ●仮売上
     *         取引 ID PASS  --------- 別
     *         オーダー ID   --------- 別
     *         本体金額      --------- 0 でセット
     *         送料          --------- 別
     *
     * @param array $outputIput
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/30
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    private function _createSettlementOutput(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo, array $settlementDataList) {

        include_once 'Mikoshiva/Settlement/Popo/Output/Order.php';
        include_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement/List.php';

        // ログ開始
        //logInfo(LOG_START);




        $outputOrder = null;
        $outputOrder = new Mikoshiva_Settlement_Popo_Output_Order();



        // ◆Mikoshiva_Settlement_Popo_Output_Order_Settlement_Info（決済情報クラス）を詰める配列
        $orderOutputSettlementInfoPopoList = array();





        // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
        // $settlementDataShopId       = testpg0000153
        // $settlementDataShopDataList = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
        foreach ($settlementDataList as $settlementDataShopId => $settlementDataShopDataList) { // {{{ foreach:01
        // ■■■


            // ▼取引 ID
            $normalAccessId   = null;
            $normalAccessId   = $settlementDataShopDataList[0]->getNormal()->getAccessID();

            // ▼取引パスワード
            $normalAccessPass = null;
            $normalAccessPass = $settlementDataShopDataList[0]->getNormal()->getAccessPass();

            // ▼マスターオーダー ID
            $masterOrderId = null;
            $masterOrderId = $settlementDataShopDataList[0]->getNormal()->getOrderID();

            // ▼マスター jobCd
            $masterJobCd = null;
            if ((string)$settlementDataShopDataList[0]->getNormal()->getAmount() === '0') {
                // 0 円だったら有効性チェック
                $masterJobCd = 'CHECK';
            } else {
                // 0 円以上だったら有効性チェック
                $masterJobCd = 'CAPTURE';
            }



            // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
            // $index              = 0
            // $settlementDataListWork = new Mikoshiva_Settlement_Popo_Input_Order_Work_List()
            foreach ($settlementDataShopDataList as $index => $settlementDataListWorkList) { // {{{ foreach:02
            // ■■■

                /* @var $settlementDataListWork Mikoshiva_Settlement_Popo_Input_Order_Work_List */
                $settlementDataListWork;



                $outputOrder->setChargeType($orderInputPopo->getChargeType());   // chargeType
                $outputOrder->setPaymentType($orderInputPopo->getPaymentType()); // PaymentType


                $outputSettlementDataList = null;
                $outputSettlementDataList = new Mikoshiva_Settlement_Popo_Output_Order_Settlement_List();



                include_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement/Info.php';
                $orderSettlementInfo = new Mikoshiva_Settlement_Popo_Output_Order_Settlement_Info();
                $orderSettlementInfo->setSettlementOutputList($outputSettlementDataList);






                // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
                // $settlementKey  = normal/auth
                // $settlementData = new Mikoshiva_Settlement_Popo_Input_Order_Work();
                foreach ($settlementDataListWorkList->getProperties() as $settlementKey => $settlementData) { // {{{ foreach:03
                // ■■■


                    //--------------------------------------------------------------------
                    //
                    // ▼normal or auth 意外はコンテニュー
                    //
                    //--------------------------------------------------------------------
                    if ($settlementKey !== 'normal' && $settlementKey !== 'auth') {
//dumper('--------------------------------  ▼normal or auth 意外はコンテニュー（' . __LINE__ . '）');
                        continue;
                    }


                    //--------------------------------------------------------------------
                    //
                    // ▼仮売上商品フラグが false で有ればコンテニュー
                    //
                    //--------------------------------------------------------------------
                    if ($settlementKey === 'auth' && $settlementData === null) {
//dumper('--------------------------------  ▼仮売上商品フラグが false で有ればコンテニュー（' . __LINE__ . '）');
                        continue;
                    }


                    //--------------------------------------------------------------------
                    //
                    // ▼Output クラスのインスタンスを生成
                    //
                    //--------------------------------------------------------------------
                    include_once 'Mikoshiva/Settlement/Popo/Output/Order/Settlement.php';
                    $orderSettlement = null;
                    $orderSettlement = new Mikoshiva_Settlement_Popo_Output_Order_Settlement();



                    //--------------------------------------------------------------------
                    //
                    // ▼Output クラスのインスタンスを生成
                    //   AccessID           アクセス ID
                    //   AccessPass         アクセスパスワード
                    //   OrderID            オーダー ID
                    //   ErrCode            エラーコード
                    //   ErrInfo            エラー詳細コード
                    //   ErrFlag            エラーフラグ 	０：成功　１：失敗
                    //   paymentAmount      支払い料金
                    //   paymenShippingCost 送料
                    //   paymentDatetime    支払い日時
                    //
                    //--------------------------------------------------------------------
                    // ▼取引 ID ---------------------------------------------------------
                    $accessId = null;
                    $accessId = ($settlementKey === 'auth')
                              ? $settlementData->getAccessID()
                              : $normalAccessId;
                    $orderSettlement->setAccessID($accessId);



                    // ▼取引パスワード ---------------------------------------------------------
                    $accessPass = null;
                    $accessPass = ($settlementKey === 'auth')
                                ? $settlementData->getAccessPass()
                                : $normalAccessPass;
                    $orderSettlement->setAccessPass($accessPass);



                    // ▼オーダー ID ---------------------------------------------------------
                    $orderSettlement->setOrderID($settlementData->getOrderID());


                    if ($settlementKey === 'normal') {

                        // ▼マスターオーダー ID ---------------------------------------------------------
                        $orderSettlement->setMasterOrderID($masterOrderId);

                        // ▼マスターjobCd ---------------------------------------------------------
                        $orderSettlement->setMasterJobCd($masterJobCd);
                    }





                    // ▼商品金額 ---------------------------------------------------------
                    $amount = 0;
                    if ($settlementData->getInputProductInfo()->getProvisorySalesFlag() === '1' && $settlementKey === 'normal') {


                        // ◆仮売上商品フラグが立っていれば標準決済は送料のみをセット
                        $orderSettlement->setPaymentAmount($settlementData->getInputProductInfo()->getMstCampaignProduct()->getShippingCost());

                    } else if($settlementKey === 'normal') {


                        // ◆仮売上商品フラグが立っていなければ送料と初回価格（本体金額）の合計値をセット
                        // --------------------------- 呼び出し元で計算した金額を使う
                        $orderSettlement->setPaymentAmount($settlementData->getInputProductInfo()->getPaymentAmount());

                    } else if ($settlementKey === 'auth') {


                        //   ※仮売上は初回価格（本体金額）のみをセットする
                        $orderSettlement->setPaymentAmount($settlementData->getInputProductInfo()->getMstCampaignProduct()->getPrice());

                        // ▼マスターオーダー ID ---------------------------------------------------------
                        $orderSettlement->setMasterOrderID($settlementData->getOrderID());

                        // ▼マスターjobCd ---------------------------------------------------------
                        $orderSettlement->setMasterJobCd('AUTH');
                    }

/**************
                    // ▼商品金額 （仮売上商品で有り、標準決済データで有れば 0 円でセット）---------------------------------------------------------
                    $amount = 0;
                    $amount = ($settlementData->getInputProductInfo()->getProvisorySalesFlag() === '1' && $settlementKey === 'normal')
                            ? 0
                            : $settlementData->getInputProductInfo()->getMstCampaignProduct()->getPrice();
                    $orderSettlement->setPaymentAmount($amount);



                    // ▼送料 （仮売上商品で有り、仮売上決済データで有れば 0 円でセット）---------------------------------------------------------
                    $shippingCost = 0;
                    $shippingCost = ($settlementData->getInputProductInfo()->getProvisorySalesFlag() === '1' && $settlementKey === 'auth')
                                  ? 0
                                  : $settlementData->getInputProductInfo()->getMstCampaignProduct()->getShippingCost();
                    $orderSettlement->setPaymentAmount($shippingCost);
******************/



                    // ▼支払い日時 ---------------------------------------------------------
                    $orderSettlement->setPaymentDatetime(
                        preg_replace('#(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})#',
                        			 '$1-$2-$3 $4:$5:$6',
                                     $settlementData->getSettlementOutputPopoList()->getExec()->getTranDate()
                         )
                     );



                    // ▼エラーコード & エラー詳細コード ---------------------------------------------------------
                    foreach ($settlementData->getSettlementOutputPopoList()->getProperties() as $popoKey => $popo) {

                        // ◆値が存在しなければループを抜ける
                        //if (isBlankOrNull($popo)) break;

                        // エラーフラグ
                        $orderSettlement->setErrFlag('0');

                        // ◆値が有れば、値を移してループ抜ける
                        if (! isBlankOrNull($popo->getErrCode()) || ! isBlankOrNull($popo->getErrInfo())) {

                            // エラーコード
                            $orderSettlement->setErrCode($popo->getErrCode());

                            // エラー詳細コード
                            $orderSettlement->setErrInfo($popo->getErrInfo());

                            // エラーフラグ
                            $orderSettlement->setErrFlag('1');
                            $outputOrder->setErrFlag('1');
                            $outputOrder->setErrCode($popo->getErrCode());
                            $outputOrder->setErrInfo($popo->getErrInfo());

                            break;
                        }
                    } // foreach:4


                    // ▼決済固有のセット --------------------------------------------------------------------------------------
                    if ($settlementKey === 'normal') {

                        // ◆標準決済の場合
                        $outputSettlementDataList->setNormal($orderSettlement);                    // 標準決済結果
                        $orderSettlementInfo->setMemberID($settlementData->getMemberID());                 // 会員 ID
                        $orderSettlementInfo->setShopID($settlementData->getShopID());                     // ショップ ID
                        $orderSettlementInfo->setInputProductInfo($settlementData->getInputProductInfo()); // 商品情報
                        $orderSettlementInfo->setAuthFlag(
                            $settlementData->getInputProductInfo()->getProvisorySalesFlag()
                        ); // 仮売上フラグ

                         // デビットカードフラグ
                        if (Mikoshiva_Utility_Gmo::isDebitCard($orderInputPopo->getCardNo())) {
                            $outputOrder->setDebitCardFlag('1');
                        }

                    } else {

                        // ◆仮売上
                        $outputSettlementDataList->setAuth($orderSettlement); // 仮売上決済結果
                    }

                } // foreach:3

                $orderOutputSettlementInfoPopoList[] = $orderSettlementInfo;
            } // foreach:2
        } // foreach:1

        $outputOrder->setSettlementInfoList($orderOutputSettlementInfoPopoList);
        return $outputOrder;
    }









    /**
     * 決済結果を【GMO 取引登録テーブル mst_gmo_transaction_history に登録します】
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/01
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function _createTransactionHistory(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo, Mikoshiva_Settlement_Popo_Output_Order $outputPopoList) {

include_once 'Mikoshiva/Date.php';
            // ログ開始
        //logInfo(LOG_START);


        /* @var $orderInputPopo Mikoshiva_Settlement_Popo_Input_Order */
        $orderInputPopo;


        /* @var $outputPopoValueList Mikoshiva_Settlement_Popo_Output_Order_Settlement_Info */
        foreach ($outputPopoList->getSettlementInfoList() as $outputPopoIndex => $outputPopoValueList) {


            /* @var $settlementOutputList Mikoshiva_Settlement_Popo_Output_Order_Settlement_List */
            /* @var $settlementOutputValue Mikoshiva_Settlement_Popo_Output_Order_Settlement */
            $settlementOutputList = null;
            $settlementOutputList = $outputPopoValueList->getSettlementOutputList();
            foreach ($settlementOutputList->getProperties() as $settlementOutputKey => $settlementOutputValue) {


                $settlementOutputKey; // normal or auth
                //$settlementOutputValue->getOrderID();
//dumper($settlementOutputValue->getPaymentDatetime());exit;

                //---------------------------------------------------------------------
                // ▼決済データが無ければコンテニュー
                //---------------------------------------------------------------------
                if ($settlementOutputValue === null) continue;



                //---------------------------------------------------------------------
                // ▼何の取引を行ったのか確定する
                //---------------------------------------------------------------------
                $entryStatus = null;//dumper($entryStatus);exit;
                switch (true) {

                    // 仮売上 ----------------------------------------------------
                    //case ($entryInputPopo->getAmount() === 0):
                    case ($settlementOutputKey === 'auth'):
                        $entryStatus = 'ENTRY_STATUS_AUTH';
                        break;

                    // 0 円だったら有効性チェック ----------------------------------------------------
                    case ($settlementOutputValue->getPaymentAmount() === 0):
                        $entryStatus = 'ENTRY_STATUS_CHECK';
                        break;

                    // 1 円以上だったら即時売上 ----------------------------------------------------
                    case ($settlementOutputValue->getPaymentAmount() !== 0):
                        $entryStatus = 'ENTRY_STATUS_CAPTURE';
                        break;

                    default:
                        include_once 'Mikoshiva/Settlement/Execution/Order/Exception.php';
                        $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引登録が確定出来ませんでした。";
                        logDebug($message);
                        throw new Mikoshiva_Settlement_Execution_Order_Exception($message);
                }





                //---------------------------------------------------------------------
                // ▼登録処理
                //---------------------------------------------------------------------
                // ◆登録データを整形
                $data = array();
                $data = array(
                    //'gmoTransactionHistoryId' => null,
                    'orderId'                 => $settlementOutputValue->getOrderID(),
                    'memberId'                => $outputPopoValueList->getMemberID(),
                    //'siteId'                  => $orderInputPopo->get,
                    'shopId'                  => $outputPopoValueList->getShopID(),
                    //'tranId'                  => $settlementOutputValue->getPaymentDatetime(),
                    //'checkString'             => $settlementOutputValue,
                    //'approve'                 => null,
                    //'transactionType'         => null,
                    'accessId'                => $settlementOutputValue->getAccessID(),
                    'accessPass'              => $settlementOutputValue->getAccessPass(),
                    'cardNo'                  => (isBlankOrNull($orderInputPopo->getCardNo()) ? null : 'XXXXXXXXXXXX' . substr($orderInputPopo->getCardNo(), -4)),
                    'method'                  => $orderInputPopo->getMethod(),
                    'payTimes'                => $orderInputPopo->getPayTimes(),
                    'amount'                  => $settlementOutputValue->getPaymentAmount(),
                    //'clientField1'            => null,
                    //'clientField2'            => null,
                    //'clientField3'            => null,
                    'tranDate'                => $settlementOutputValue->getPaymentDatetime(),
                    //'deletionDatetime'        => null,
                    'registrationDatetime'    => Mikoshiva_Date::mikoshivaNow(),
                    'updateDatetime'          => Mikoshiva_Date::mikoshivaNow(),
                    //'updateTimestamp'         => null,
                );
//dumper($settlementOutputValue);

                // ◆登録
                $simple = null;
                $simple = new Mikoshiva_Db_Simple();
                $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);
            }
        }
    }



















































    /**
     * 決済結果を【GMO 取引登録テーブル mst_gmo_transaction_history に登録します】
     *
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/04/01
     * @version SVN:$Id: Gmo.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function _createTransactionHistory2(Mikoshiva_Settlement_Popo_Input_Order $orderInputPopo, array $settlementDataList) {

//dumper($settlementDataList);exit;

        // ▼取引グループキーを生成
        $transactionGroupKey = Mikoshiva_Utility_String::createRandomString(40);

        // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
        // $settlementDataShopId       = testpg0000153
        // $settlementDataShopDataList = new Mikoshiva_Settlement_Popo_Input_Order_Work_List();
        foreach ($settlementDataList as $settlementDataShopId => $settlementDataShopDataList) { // {{{ foreach:01
        // ■■■



            // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
            // $index              = 0
            // $settlementDataListWork = new Mikoshiva_Settlement_Popo_Input_Order_Work_List()
            foreach ($settlementDataShopDataList as $index => $settlementDataListWorkList) { // {{{ foreach:02
            // ■■■

                // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
                // $settlementKey  = normal/auth
                /* @var $settlementData Mikoshiva_Settlement_Popo_Input_Order_Work */
                foreach ($settlementDataListWorkList->getProperties() as $settlementKey => $settlementData) { // {{{ foreach:03
                // ■■■





                $settlementOutputKey; // normal or auth

                //$settlementOutputValue->getOrderID();
//dumper($settlementOutputValue->getPaymentDatetime());exit;

                //---------------------------------------------------------------------
                // ▼決済データが無ければコンテニュー
                //---------------------------------------------------------------------
                if ($settlementData === null) continue;


                //---------------------------------------------------------------------
                // ▼仮売上でかつ、デビットカードで有ればコンテニュー
                //---------------------------------------------------------------------
                if ($settlementKey === 'auth' && $this->_isDebit === true) continue;


                //---------------------------------------------------------------------
                // ▼何の取引を行ったのか確定する
                //---------------------------------------------------------------------
                $entryStatus = null;
                switch (true) {

                    // 仮売上 ----------------------------------------------------
                    //case ($entryInputPopo->getAmount() === 0):
                    case ($settlementKey === 'auth'):
                        $entryStatus = 'ENTRY_STATUS_AUTH';
                        break;

                    // 0 円だったら有効性チェック ----------------------------------------------------
                    case ($settlementData->getAmount() === 0):
                        $entryStatus = 'ENTRY_STATUS_CHECK';
                        break;

                    // 1 円以上だったら即時売上 ----------------------------------------------------
                    case ($settlementData->getAmount() !== 0):
                        $entryStatus = 'ENTRY_STATUS_CAPTURE';
                        break;

                    default:
                        include_once 'Mikoshiva/Settlement/Execution/Order/Exception.php';
                        $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】取引登録が確定出来ませんでした。";
                        logDebug($message);
                        throw new Mikoshiva_Settlement_Execution_Order_Exception($message);
                }


                //---------------------------------------------------------------------
                // ▼登録処理
                //---------------------------------------------------------------------
                // ◆登録データを整形
                $data = array();
                $data = array(
                    //'gmoTransactionHistoryId' => null,
                    'transactionGroupKey'     => $transactionGroupKey,
                    'orderId'                 => $settlementData->getOrderID(),
                    'memberId'                => $settlementData->getMemberID(),
                    //'siteId'                  => $orderInputPopo->get,
                    'shopId'                  => $settlementData->getShopID(),
                    //'tranId'                  => $settlementOutputValue->getPaymentDatetime(),
                    //'checkString'             => $settlementOutputValue,
                    //'approve'                 => null,
                    //'transactionType'         => null,
                    'accessId'                => $settlementData->getAccessID(),
                    'accessPass'              => $settlementData->getAccessPass(),
                    'cardNo'                  => (isBlankOrNull($settlementData->getCardNo()) ? null : 'XXXXXXXXXXXX' . substr($settlementData->getCardNo(), -4)),
                    'method'                  => $orderInputPopo->getMethod(),
                    'payTimes'                => $orderInputPopo->getPayTimes(),
                    'entryStatus'             => $entryStatus,
                    'amount'                  => $settlementData->getAmount(),
                    //'clientField1'            => null,
                    //'clientField2'            => null,
                    //'clientField3'            => null,
                    'tranDate'                => $settlementData->getSettlementOutputPopoList()->getExec()->getTranDate(), //$settlementData->getPayTimes(),
                    //'deletionDatetime'        => null,
                    'registrationDatetime'    => Mikoshiva_Date::mikoshivaNow(),
                    'updateDatetime'          => Mikoshiva_Date::mikoshivaNow(),
                    //'updateTimestamp'         => null,
                );
//dumper($settlementOutputValue);

                // ◆登録
                $simple = null;
                $simple = new Mikoshiva_Db_Simple();
                $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);
            }
        }
    }
    }


















}


















