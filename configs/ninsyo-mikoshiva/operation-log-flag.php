<?php


/**
 * operationLog に関する定数定義
 *
 * 【説明】
 *
 *
 * 【概要】
 *
 * 【変更履歴】
 * 2011-03-23 谷口司 一時ログにsalesを追加
 */

//------------------------------------------------------------------
// ■SQL の mst_operation_log への書込みを制御します
//  0: 書き込まない
//  1: 書きこむ
//------------------------------------------------------------------
if (PHP_SAPI === 'cli') {
// ◆------------------◆
// ◆--- コンソール ---◆
// ◆------------------◆
    if (REAL_FLAG === 1) {
        // ◆----------------◆
        // ◆--- 本場環境 ---◆
        // ◆----------------◆
        define('OPERATION_LOG_INSERT_WRITE_FLAG', 0); // INSERT
        define('OPERATION_LOG_UPDATE_WRITE_FLAG', 0); // UPDATE
        define('OPERATION_LOG_DELETE_WRITE_FLAG', 0); // DELETE
        define('OPERATION_LOG_SELECT_WRITE_FLAG', 0); // SELECT
    } else {
        // ◆----------------◆
        // ◆--- ローカル ---◆
        // ◆----------------◆
        define('OPERATION_LOG_INSERT_WRITE_FLAG', 0); // INSERT
        define('OPERATION_LOG_UPDATE_WRITE_FLAG', 0); // UPDATE
        define('OPERATION_LOG_DELETE_WRITE_FLAG', 0); // DELETE
        define('OPERATION_LOG_SELECT_WRITE_FLAG', 0); // SELECT
    }
} else {
// ◆----------------◆
// ◆--- Web 画面 ---◆
// ◆----------------◆
    if (REAL_FLAG === 1) {
        // ◆----------------◆
        // ◆--- 本場環境 ---◆
        // ◆----------------◆
        define('OPERATION_LOG_INSERT_WRITE_FLAG', 0); // INSERT
        define('OPERATION_LOG_UPDATE_WRITE_FLAG', 0); // UPDATE
        define('OPERATION_LOG_DELETE_WRITE_FLAG', 0); // DELETE
        define('OPERATION_LOG_SELECT_WRITE_FLAG', 0); // SELECT
    } else {
        // ◆----------------◆
        // ◆--- ローカル ---◆
        // ◆----------------◆
        define('OPERATION_LOG_INSERT_WRITE_FLAG', 0); // INSERT
        define('OPERATION_LOG_UPDATE_WRITE_FLAG', 0); // UPDATE
        define('OPERATION_LOG_DELETE_WRITE_FLAG', 0); // DELETE
        define('OPERATION_LOG_SELECT_WRITE_FLAG', 0); // SELECT
    }
}