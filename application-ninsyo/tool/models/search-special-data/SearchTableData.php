<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'tool/models/classes/PrimaryKeyByTable.php';
require_once 'tool/models/classes/ColumnNameList.php';

/**
 * 指定したテーブルのデータを取得
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2011/09/09
 * @version        SVN: $Id:
 *
 */
class Tool_Models_SearchSpecialData_SearchTableData
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したテーブルのデータを取得
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // リターンする配列
        $output = array();

        $tableName = trim($actionForm->getTableName());
        $tableId   = trim($actionForm->getTableId());
        $keyId     = $actionForm->getKeyId();

        if (is_array($keyId)) {
            foreach ($keyId as $k => $v) {
                $keyId[$k] = trim($v);
            }
        } else {
            $keyId = array();
        }

        $keyColumn = $actionForm->getKeyColumn();
        if (is_array($keyColumn)) {
            foreach ($keyColumn as $k => $v) {
                $keyColumn[$k] = strtolower(trim($v));
            }
        } else {
            $keyColumn = array();
        }


        if (!isBlankOrNull($tableName)) {
            //-----------------------------------------------
            // カラム情報取得
            //-----------------------------------------------
            $columnData = array();

            // SQL作成
            $sql = "";
            $sql .= "SHOW COLUMNS ";
            $sql .= "FROM  ";
            $sql .= $tableName;

            // SQL実行
            $res = $this->_db->query($sql);

            /*
                array(6) {
                    ["Field"] => string(16) "mdr_direction_id"
                    ["Type"] => string(7) "int(11)"
                    ["Null"] => string(2) "NO"
                    ["Key"] => string(3) "PRI"
                    ["Default"] => NULL
                    ["Extra"] => string(14) "auto_increment"
                }
            */

            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                $columnData[$v['Field']] = $v;
            }
            ksort($columnData);
            $actionForm->setTableInfo($columnData);

            if ($actionForm->getViewOnlyFlag() === '1') {

                $class = null;
                $class = new Tool_Models_Classes_PrimaryKeyByTable();

                $primaryKey = $class->execute($tableName);
                $prefix = substr($primaryKey,0,3);

                //-----------------------------------------------
                //
                // 検索フォームからサブミットされたら、条件を追加して検索を行う。
                //
                //-----------------------------------------------
                // SQL作成
                $sql = "";
                $sql .= "SELECT";
                $sql .= " *";
                $sql .= " FROM " . $tableName;
                $sql .= " WHERE 1 = 1 ";

                $params   = array();

                if (!isBlankOrNull($tableId)) {
                    $sql .= " AND " . $primaryKey . " = ?";
                    $params[] = $tableId;
                }

                foreach ($keyColumn as $k => $v) {
                    if (!isBlankOrNull($v) && isset($keyId[$k]) && !isBlankOrNull($keyId[$k])) {
                        $keyIds = explode(',',str_replace("'",'',$keyId[$k]));
                        if (count($keyIds) > 1) {
                            $sql .= " AND " . $v . " IN ('" . implode("','",$keyIds) . "')";
                        } else {
                            $sql .= " AND " . $v . " = ?";
                            $params[] = $keyIds[0];
                        }
                    }
                }

                for ($i = 0; $i < count($params); $i++) {
                    $sql = $this->_db->quoteInto($sql, $params[$i], null, 1);
                }
                echo '<br>' . $sql . '<br>';

                $res = array();
                $res = $this->_db->query($sql);

                if ($res->rowCount() > 199) {
                    echo '<br><span class="COM_fontRed COM_bold">200件以上の結果は表示できません</span>';
                    die;
                }

                $res = array();

                $output[$prefix] = $this->_db->fetchAll($sql);

                //-----------------------------------------------
                // ｽﾃｰﾀｽ名一覧作成
                //-----------------------------------------------
                $statusArr = array();
                $statusList = array();
                $temp = $context->getConfig();
                foreach ($temp['SHORT_CODE_MAP_LIST'] as $k => $v) {
                    foreach ($v as $k2 => $v2) {
                        $statusArr[$k2] = $v2;
                    }
                    $statusList[$k] = '1';
                }
                $actionForm->setStatusName($statusArr);
                $actionForm->setStatusList($statusList);

                //-----------------------------------------------
                // 更新テーブル情報作成
                //-----------------------------------------------
                $tableList = array();
                $tableList[$prefix] = array();
                $tableList[$prefix]['name'] = $tableName;
                $tableList[$prefix]['table'] = $tableName;
                $tableList[$prefix]['primary'] = $primaryKey;
                $tableList[$prefix]['no'] = '1';

                $actionForm->setTableList($tableList);

                if (count($output[$prefix]) === 0) {
                    unset($output[$prefix]);
                } else {
                    $class = null;
                    $class = new Tool_Models_Classes_ColumnNameList();

                    $actionForm->setColumnName($class->execute());
                }
            }
        }

        return $output;
    }

}
