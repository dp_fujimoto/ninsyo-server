<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（ヘルパー） ドキュメント &gt; ヘルパー 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/helper">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> ヘルパー</h1>


            <p>年、月、日、それぞれのセレクトボックスのhtmlを返します。</p>
<!-- =================================================================
    ■<?php echo basename(__FILE__, '.php') ?>■
====================================================================== -->
          <table class="wiki">
            <tr>
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
<tr>
            <td id="html_select_dateLister" class="name">year_name
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>

            <td id="html_select_dateLister" class="default">空文字</td>
            <td id="html_select_dateLister" class="summary"> '年'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_name
</td>
            <td id="html_select_dateLister" class="type">string
</td>

            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">空文字</td>
            <td id="html_select_dateLister" class="summary"> '月'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_name
</td>

            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">空文字</td>
            <td id="html_select_dateLister" class="summary"> '日'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>

            <td id="html_select_dateLister" class="name">time
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">現在の時間</td>
            <td id="html_select_dateLister" class="summary"> 使用する日付/時間（timestamp/ YYYY-MM-DD）
</td>
          </tr>

                    <tr>
            <td id="html_select_dateLister" class="name">start_year
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">現在の年</td>
            <td id="html_select_dateLister" class="summary"> ドロップダウンリストの始めの年 (年を表す数字又は現在の年からの相対年数(+/- N))

</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">end_year
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">start_yearと同じ</td>

            <td id="html_select_dateLister" class="summary"> ドロップダウンリストの終わりの年 (年を表す数字又は現在の年からの相対年数(+/- N))
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">display_years
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>

            <td id="html_select_dateLister" class="default">TRUE</td>
            <td id="html_select_dateLister" class="summary"> 年のフォームを表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">display_months
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>

            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">TRUE</td>
            <td id="html_select_dateLister" class="summary"> 月のフォームを表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">display_days
</td>

            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">TRUE</td>
            <td id="html_select_dateLister" class="summary"> 日のフォームを表示するかどうか
</td>
          </tr>
                    <tr>

            <td id="html_select_dateLister" class="name">month_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%m</td>
            <td id="html_select_dateLister" class="summary"> 月の表示フォーマット(strftime)
</td>
          </tr>

                    <tr>
            <td id="html_select_dateLister" class="name">day_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%02d</td>
            <td id="html_select_dateLister" class="summary"> 日の表示のフォーマット(sprintf)

</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_value_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%m</td>

            <td id="html_select_dateLister" class="summary"> 月の値のフォーマット(strftime)
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_value_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>

            <td id="html_select_dateLister" class="default">%d</td>
            <td id="html_select_dateLister" class="summary"> 日の値のフォーマット (sprintf)
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">field_order
</td>
            <td id="html_select_dateLister" class="type">string
</td>

            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">YMD</td>
            <td id="html_select_dateLister" class="summary"> フィールドを表示する順序
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">field_separator
</td>

            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">\n</td>
            <td id="html_select_dateLister" class="summary"> フィールド間に表示する文字列
</td>
          </tr>
                    <tr>

            <td id="html_select_dateLister" class="name">year_as_text
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">FALSE</td>
            <td id="html_select_dateLister" class="summary"> 年をテキストとして表示するかどうか
</td>
          </tr>

                    <tr>
            <td id="html_select_dateLister" class="name">reverse_years
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">FALSE</td>
            <td id="html_select_dateLister" class="summary"> 年を逆順で表示する

</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">field_array
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>

            <td id="html_select_dateLister" class="summary"> name属性が与えられた場合、結果の値を name[Day],name[Month],name[Year]の形の連想配列にしてPHPに返す
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_size
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>

            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 年のselectタグにsize属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_size
</td>
            <td id="html_select_dateLister" class="type">string
</td>

            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 月のselectタグにsize属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_size
</td>

            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 日のselectタグにsize属性を追加
</td>
          </tr>
                    <tr>

            <td id="html_select_dateLister" class="name">all_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 全てのselect/inputタグに拡張属性を追加
</td>
          </tr>

                    <tr>
            <td id="html_select_dateLister" class="name">year_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 年のselect/inputタグに拡張属性を追加

</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>

            <td id="html_select_dateLister" class="summary"> 月のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>

            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 日のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_empty
</td>
            <td id="html_select_dateLister" class="type">string
</td>

            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 年のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。 例えば、セレクトボックスに "年を選択して下さい" と表示させる時に便利です。 年を選択しないことを示唆するのに、time属性に対して "-MM-DD" という値が指定できることに注意してください。
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_empty
</td>

            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 月のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。月を選択しないことを示唆するのに、 time属性に対して "YYYY--DD" という値が指定できることに注意してください。
</td>
          </tr>
                    <tr>

            <td id="html_select_dateLister" class="name">day_empty
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 日のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。日を選択しないことを示唆するのに、 time属性に対して "YYYY-MM-" という値が指定できることに注意してください。
</td>
          </tr>

          </table>

            <h2 id="使用例">サンプルソース</h2>

            <span class="wiki"><?php
highlight_string('
{html_select_dateLister year_name="yearFrom" month_name="monthFrom" day_name="dayFrom"
 start_year=$smarty.now|date_format:"%Y" end_year="-5" year_empty="" month_empty="" day_empty=""
 time=$form->getYearFrom()|cat:"-"|cat:$form->getMonthFrom()|cat:"-"|cat:$form->getDayFrom()}
'); ?>
            </span>

            <h2 id="使用例">結果</h2>

            <span class="wiki"><?php
highlight_string('
<select name="yearFrom" id="yearFrom">
<option label="" value="" selected="selected"></option>
<option label="2005" value="2005">2005</option>
<option label="2006" value="2006">2006</option>
<option label="2007" value="2007">2007</option>
<option label="2008" value="2008">2008</option>
<option label="2009" value="2009">2009</option>
<option label="2010" value="2010">2010</option>
</select> 年<select name="monthFrom" id="monthFrom">
<option label="" value="" selected="selected"></option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>
<option label="07" value="07">07</option>
<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12">12</option>
</select>月<select name="dayFrom" id="dayFrom">
<option label="" value="" selected="selected"></option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>
<option label="07" value="07">07</option>
<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12">12</option>
<option label="13" value="13">13</option>
<option label="14" value="14">14</option>
<option label="15" value="15">15</option>
<option label="16" value="16">16</option>
<option label="17" value="17">17</option>
<option label="18" value="18">18</option>
<option label="19" value="19">19</option>
<option label="20" value="20">20</option>
<option label="21" value="21">21</option>
<option label="22" value="22">22</option>
<option label="23" value="23">23</option>
<option label="24" value="24">24</option>
<option label="25" value="25">25</option>
<option label="26" value="26">26</option>
<option label="27" value="27">27</option>
<option label="28" value="28">28</option>
<option label="29" value="29">29</option>
<option label="30" value="30">30</option>
<option label="31" value="31">31</option>
</select>日                             

'); ?>
</span>
            <h2 id="使用例">結果(表示)</h2>

            
<select name="yearFrom" id="yearFrom">
<option label="" value="" selected="selected"></option>
<option label="2005" value="2005">2005</option>
<option label="2006" value="2006">2006</option>
<option label="2007" value="2007">2007</option>
<option label="2008" value="2008">2008</option>
<option label="2009" value="2009">2009</option>
<option label="2010" value="2010">2010</option>
</select> 年<select name="monthFrom" id="monthFrom">

<option label="" value="" selected="selected"></option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>
<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>
<option label="07" value="07">07</option>
<option label="08" value="08">08</option>

<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12">12</option>
</select>月<select name="dayFrom" id="dayFrom">
<option label="" value="" selected="selected"></option>
<option label="01" value="01">01</option>
<option label="02" value="02">02</option>
<option label="03" value="03">03</option>

<option label="04" value="04">04</option>
<option label="05" value="05">05</option>
<option label="06" value="06">06</option>
<option label="07" value="07">07</option>
<option label="08" value="08">08</option>
<option label="09" value="09">09</option>
<option label="10" value="10">10</option>
<option label="11" value="11">11</option>
<option label="12" value="12">12</option>

<option label="13" value="13">13</option>
<option label="14" value="14">14</option>
<option label="15" value="15">15</option>
<option label="16" value="16">16</option>
<option label="17" value="17">17</option>
<option label="18" value="18">18</option>
<option label="19" value="19">19</option>
<option label="20" value="20">20</option>
<option label="21" value="21">21</option>

<option label="22" value="22">22</option>
<option label="23" value="23">23</option>
<option label="24" value="24">24</option>
<option label="25" value="25">25</option>
<option label="26" value="26">26</option>
<option label="27" value="27">27</option>
<option label="28" value="28">28</option>
<option label="29" value="29">29</option>
<option label="30" value="30">30</option>

<option label="31" value="31">31</option>
</select>日                             

  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>


