<?php
require_once 'PHPUnit/Framework.php';

require_once('../DPMemberShip/DPMemberShip.php');


class MemberShipTest extends PHPUnit_Framework_TestCase
{
	var $memberShip;
	
	public function test01New(){
    	$this->memberShip = new MemberShip(AUTH_FILE_DIR);
    	$member = $this->memberShip;
    	
    	$this->assertEquals($member->authdir,AUTH_FILE_DIR);
    	$days = array('20110102'=>201012,'20110110'=>201012,'20110111'=>201101,'20110112'=>201101);
    	foreach ($days as $d=>$ym){
	    	$res = $member->add("hoge1@example$ym.jp",'MLT',$d);
	    	$res = $member->add("hoge2@example$ym.jp",'MLT',$d);
	    	$res = $member->add("hoge3@example$ym.jp",'MLT',$d);
	        $this->assertTrue(file_exists(AUTH_FILE_DIR."/MLT${ym}_$member->unique.csv"));
	    	$res = $member->add("hoge1@example$ym.jp",'MSC',$d);
	    	$res = $member->add("hoge2@example$ym.jp",'MSC',$d);
	    	$res = $member->add("hoge3@example$ym.jp",'MSC',$d);
	        $this->assertTrue(file_exists(AUTH_FILE_DIR."/MSC${ym}_$member->unique.csv"));
    	}
    	
    	$this->assertEquals( $member->dp_server_merge(),true);

    }
    
}
