<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_customer_thanks_mail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxCustomerThanksMail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tct';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'customerThanksMailId' => null,
        'messageId' => null,
        'customerId' => null,
        'purchaseId' => null,
        'transactionNumber' => null,
        'sendDate' => null,
        'realSendDatetime' => null,
        'templateType' => null,
        'templateKey' => null,
        'mailType' => null,
        'fromName' => null,
        'fromEmail' => null,
        'toName' => null,
        'toEmail' => null,
        'subject' => null,
        'textBody' => null,
        'htmlBody' => null,
        'sendCount' => null,
        'sendCompleteFlag' => null,
        'holdFlag' => null,
        'operatorFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 顧客サンキューメールID の値を返します
	 *
     * @return int 顧客サンキューメールID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerThanksMailId() {
		return $this->_properties['customerThanksMailId'];
    }


    /**
     * 顧客サンキューメールID の値をセットします
	 *
	 * @param int $customerThanksMailId 顧客サンキューメールID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerThanksMailId($customerThanksMailId) {
		$this->_properties['customerThanksMailId'] = $customerThanksMailId;
    }


    /**
     * メッセージID の値を返します
	 *
     * @return varchar メッセージID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMessageId() {
		return $this->_properties['messageId'];
    }


    /**
     * メッセージID の値をセットします
	 *
	 * @param varchar $messageId メッセージID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMessageId($messageId) {
		$this->_properties['messageId'] = $messageId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 購入履歴ID の値を返します
	 *
     * @return int 購入履歴ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseId() {
		return $this->_properties['purchaseId'];
    }


    /**
     * 購入履歴ID の値をセットします
	 *
	 * @param int $purchaseId 購入履歴ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseId($purchaseId) {
		$this->_properties['purchaseId'] = $purchaseId;
    }


    /**
     * トランザクションNo の値を返します
	 *
     * @return int トランザクションNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransactionNumber() {
		return $this->_properties['transactionNumber'];
    }


    /**
     * トランザクションNo の値をセットします
	 *
	 * @param int $transactionNumber トランザクションNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransactionNumber($transactionNumber) {
		$this->_properties['transactionNumber'] = $transactionNumber;
    }


    /**
     * 送信日 の値を返します
	 *
     * @return date 送信日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSendDate() {
		return $this->_properties['sendDate'];
    }


    /**
     * 送信日 の値をセットします
	 *
	 * @param date $sendDate 送信日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSendDate($sendDate) {
		$this->_properties['sendDate'] = $sendDate;
    }


    /**
     * 実送信日時 の値を返します
	 *
     * @return datetime 実送信日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRealSendDatetime() {
		return $this->_properties['realSendDatetime'];
    }


    /**
     * 実送信日時 の値をセットします
	 *
	 * @param datetime $realSendDatetime 実送信日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRealSendDatetime($realSendDatetime) {
		$this->_properties['realSendDatetime'] = $realSendDatetime;
    }


    /**
     * テンプレートタイプ の値を返します
	 *
     * @return varchar テンプレートタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateType() {
		return $this->_properties['templateType'];
    }


    /**
     * テンプレートタイプ の値をセットします
	 *
	 * @param varchar $templateType テンプレートタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateType($templateType) {
		$this->_properties['templateType'] = $templateType;
    }


    /**
     * tct_template_key の値を返します
	 *
     * @return varchar tct_template_key
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateKey() {
		return $this->_properties['templateKey'];
    }


    /**
     * tct_template_key の値をセットします
	 *
	 * @param varchar $templateKey tct_template_key
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateKey($templateKey) {
		$this->_properties['templateKey'] = $templateKey;
    }


    /**
     * メールタイプ の値を返します
	 *
     * @return varchar メールタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMailType() {
		return $this->_properties['mailType'];
    }


    /**
     * メールタイプ の値をセットします
	 *
	 * @param varchar $mailType メールタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMailType($mailType) {
		$this->_properties['mailType'] = $mailType;
    }


    /**
     * 送信元名 の値を返します
	 *
     * @return varchar 送信元名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromName() {
		return $this->_properties['fromName'];
    }


    /**
     * 送信元名 の値をセットします
	 *
	 * @param varchar $fromName 送信元名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromName($fromName) {
		$this->_properties['fromName'] = $fromName;
    }


    /**
     * 送信元メールアドレス の値を返します
	 *
     * @return varchar 送信元メールアドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromEmail() {
		return $this->_properties['fromEmail'];
    }


    /**
     * 送信元メールアドレス の値をセットします
	 *
	 * @param varchar $fromEmail 送信元メールアドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromEmail($fromEmail) {
		$this->_properties['fromEmail'] = $fromEmail;
    }


    /**
     * 送信先名 の値を返します
	 *
     * @return varchar 送信先名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToName() {
		return $this->_properties['toName'];
    }


    /**
     * 送信先名 の値をセットします
	 *
	 * @param varchar $toName 送信先名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToName($toName) {
		$this->_properties['toName'] = $toName;
    }


    /**
     * 送信先メールアドレス の値を返します
	 *
     * @return varchar 送信先メールアドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getToEmail() {
		return $this->_properties['toEmail'];
    }


    /**
     * 送信先メールアドレス の値をセットします
	 *
	 * @param varchar $toEmail 送信先メールアドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setToEmail($toEmail) {
		$this->_properties['toEmail'] = $toEmail;
    }


    /**
     * 件名 の値を返します
	 *
     * @return varchar 件名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubject() {
		return $this->_properties['subject'];
    }


    /**
     * 件名 の値をセットします
	 *
	 * @param varchar $subject 件名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubject($subject) {
		$this->_properties['subject'] = $subject;
    }


    /**
     * 本文（テキスト） の値を返します
	 *
     * @return mediumtext 本文（テキスト）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTextBody() {
		return $this->_properties['textBody'];
    }


    /**
     * 本文（テキスト） の値をセットします
	 *
	 * @param mediumtext $textBody 本文（テキスト）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTextBody($textBody) {
		$this->_properties['textBody'] = $textBody;
    }


    /**
     * 本文（HTML） の値を返します
	 *
     * @return mediumtext 本文（HTML）
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHtmlBody() {
		return $this->_properties['htmlBody'];
    }


    /**
     * 本文（HTML） の値をセットします
	 *
	 * @param mediumtext $htmlBody 本文（HTML）
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHtmlBody($htmlBody) {
		$this->_properties['htmlBody'] = $htmlBody;
    }


    /**
     * 送信回数 の値を返します
	 *
     * @return int 送信回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSendCount() {
		return $this->_properties['sendCount'];
    }


    /**
     * 送信回数 の値をセットします
	 *
	 * @param int $sendCount 送信回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSendCount($sendCount) {
		$this->_properties['sendCount'] = $sendCount;
    }


    /**
     * 送信完了フラグ の値を返します
	 *
     * @return char 送信完了フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSendCompleteFlag() {
		return $this->_properties['sendCompleteFlag'];
    }


    /**
     * 送信完了フラグ の値をセットします
	 *
	 * @param char $sendCompleteFlag 送信完了フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSendCompleteFlag($sendCompleteFlag) {
		$this->_properties['sendCompleteFlag'] = $sendCompleteFlag;
    }


    /**
     * 保留フラグ の値を返します
	 *
     * @return char 保留フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHoldFlag() {
		return $this->_properties['holdFlag'];
    }


    /**
     * 保留フラグ の値をセットします
	 *
	 * @param char $holdFlag 保留フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHoldFlag($holdFlag) {
		$this->_properties['holdFlag'] = $holdFlag;
    }


    /**
     * オペレーターフラグ の値を返します
	 *
     * @return char オペレーターフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOperatorFlag() {
		return $this->_properties['operatorFlag'];
    }


    /**
     * オペレーターフラグ の値をセットします
	 *
	 * @param char $operatorFlag オペレーターフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOperatorFlag($operatorFlag) {
		$this->_properties['operatorFlag'] = $operatorFlag;
    }


    /**
     * tct_delete_flag の値を返します
	 *
     * @return char tct_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * tct_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag tct_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * tct_deletion_datetime の値を返します
	 *
     * @return datetime tct_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * tct_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime tct_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * tct_registration_datetime の値を返します
	 *
     * @return datetime tct_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * tct_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime tct_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * tct_update_datetime の値を返します
	 *
     * @return datetime tct_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * tct_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime tct_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * tct_update_timestamp の値を返します
	 *
     * @return timestamp tct_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * tct_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp tct_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























