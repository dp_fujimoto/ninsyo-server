<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_purchase_detail
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxPurchaseDetail extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tpd';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'purchaseDetailId' => null,
        'purchaseId' => null,
        'customerId' => null,
        'productId' => null,
        'campaignProductId' => null,
        'productType' => null,
        'shopId' => null,
        'memberId' => null,
        'customerNumber' => null,
        'transactionNumber' => null,
        'orderId' => null,
        'accessId' => null,
        'accessPass' => null,
        'provisorySalesAccessId' => null,
        'provisorySalesAccessPass' => null,
        'provisorySalesOrderId' => null,
        'freePeriod' => null,
        'freePeriodType' => null,
        'guaranteePeriod' => null,
        'guaranteePeriodType' => null,
        'shippingStartDate' => null,
        'firstAmount' => null,
        'firstShippingAmount' => null,
        'firstDeliveryFee' => null,
        'continuityAmount' => null,
        'continuityShippingAmount' => null,
        'continuityDeliveryFee' => null,
        'chargeType' => null,
        'paymentType' => null,
        'cardDivideNumber' => null,
        'cardDivideType' => null,
        'shippingNumber' => null,
        'directInstallmentFlag' => null,
        'directInstallmentChargeNumber' => null,
        'directInstallmentPrice' => null,
        'provisorySalesFlag' => null,
        'debitFlag' => null,
        'shippingAtOrderFlag' => null,
        'paymentDatetime' => null,
        'errorType' => null,
        'errorCode' => null,
        'errorInfo' => null,
        'errorFlag' => null,
        'midtermCancellationFlag' => null,
        'applicationType' => null,
        'orderCancelFlag' => null,
        'orderCancelDatetime' => null,
        'dummyFlag' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 購入履歴詳細ID の値を返します
	 *
     * @return int 購入履歴詳細ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseDetailId() {
		return $this->_properties['purchaseDetailId'];
    }


    /**
     * 購入履歴詳細ID の値をセットします
	 *
	 * @param int $purchaseDetailId 購入履歴詳細ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseDetailId($purchaseDetailId) {
		$this->_properties['purchaseDetailId'] = $purchaseDetailId;
    }


    /**
     * 購入履歴ID の値を返します
	 *
     * @return int 購入履歴ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseId() {
		return $this->_properties['purchaseId'];
    }


    /**
     * 購入履歴ID の値をセットします
	 *
	 * @param int $purchaseId 購入履歴ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseId($purchaseId) {
		$this->_properties['purchaseId'] = $purchaseId;
    }


    /**
     * 顧客ID の値を返します
	 *
     * @return int 顧客ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerId() {
		return $this->_properties['customerId'];
    }


    /**
     * 顧客ID の値をセットします
	 *
	 * @param int $customerId 顧客ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerId($customerId) {
		$this->_properties['customerId'] = $customerId;
    }


    /**
     * 商品ID の値を返します
	 *
     * @return int 商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductId() {
		return $this->_properties['productId'];
    }


    /**
     * 商品ID の値をセットします
	 *
	 * @param int $productId 商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductId($productId) {
		$this->_properties['productId'] = $productId;
    }


    /**
     * キャンペーン商品ID の値を返します
	 *
     * @return int キャンペーン商品ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignProductId() {
		return $this->_properties['campaignProductId'];
    }


    /**
     * キャンペーン商品ID の値をセットします
	 *
	 * @param int $campaignProductId キャンペーン商品ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignProductId($campaignProductId) {
		$this->_properties['campaignProductId'] = $campaignProductId;
    }


    /**
     * 商品タイプ の値を返します
	 *
     * @return varchar 商品タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProductType() {
		return $this->_properties['productType'];
    }


    /**
     * 商品タイプ の値をセットします
	 *
	 * @param varchar $productType 商品タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProductType($productType) {
		$this->_properties['productType'] = $productType;
    }


    /**
     * ショップID の値を返します
	 *
     * @return char ショップID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShopId() {
		return $this->_properties['shopId'];
    }


    /**
     * ショップID の値をセットします
	 *
	 * @param char $shopId ショップID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShopId($shopId) {
		$this->_properties['shopId'] = $shopId;
    }


    /**
     * 会員ID の値を返します
	 *
     * @return char 会員ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMemberId() {
		return $this->_properties['memberId'];
    }


    /**
     * 会員ID の値をセットします
	 *
	 * @param char $memberId 会員ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMemberId($memberId) {
		$this->_properties['memberId'] = $memberId;
    }


    /**
     * 顧客決済No の値を返します
	 *
     * @return int 顧客決済No
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCustomerNumber() {
		return $this->_properties['customerNumber'];
    }


    /**
     * 顧客決済No の値をセットします
	 *
	 * @param int $customerNumber 顧客決済No
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCustomerNumber($customerNumber) {
		$this->_properties['customerNumber'] = $customerNumber;
    }


    /**
     * トランザクションNo の値を返します
	 *
     * @return int トランザクションNo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTransactionNumber() {
		return $this->_properties['transactionNumber'];
    }


    /**
     * トランザクションNo の値をセットします
	 *
	 * @param int $transactionNumber トランザクションNo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTransactionNumber($transactionNumber) {
		$this->_properties['transactionNumber'] = $transactionNumber;
    }


    /**
     * オーダーID の値を返します
	 *
     * @return char オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderId() {
		return $this->_properties['orderId'];
    }


    /**
     * オーダーID の値をセットします
	 *
	 * @param char $orderId オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderId($orderId) {
		$this->_properties['orderId'] = $orderId;
    }


    /**
     * GMOアクセスID の値を返します
	 *
     * @return char GMOアクセスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessId() {
		return $this->_properties['accessId'];
    }


    /**
     * GMOアクセスID の値をセットします
	 *
	 * @param char $accessId GMOアクセスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessId($accessId) {
		$this->_properties['accessId'] = $accessId;
    }


    /**
     * GMOアクセスパス の値を返します
	 *
     * @return char GMOアクセスパス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAccessPass() {
		return $this->_properties['accessPass'];
    }


    /**
     * GMOアクセスパス の値をセットします
	 *
	 * @param char $accessPass GMOアクセスパス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAccessPass($accessPass) {
		$this->_properties['accessPass'] = $accessPass;
    }


    /**
     * 仮売上GMOアクセスID の値を返します
	 *
     * @return char 仮売上GMOアクセスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesAccessId() {
		return $this->_properties['provisorySalesAccessId'];
    }


    /**
     * 仮売上GMOアクセスID の値をセットします
	 *
	 * @param char $provisorySalesAccessId 仮売上GMOアクセスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesAccessId($provisorySalesAccessId) {
		$this->_properties['provisorySalesAccessId'] = $provisorySalesAccessId;
    }


    /**
     * 仮売上GMOアクセスパス の値を返します
	 *
     * @return char 仮売上GMOアクセスパス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesAccessPass() {
		return $this->_properties['provisorySalesAccessPass'];
    }


    /**
     * 仮売上GMOアクセスパス の値をセットします
	 *
	 * @param char $provisorySalesAccessPass 仮売上GMOアクセスパス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesAccessPass($provisorySalesAccessPass) {
		$this->_properties['provisorySalesAccessPass'] = $provisorySalesAccessPass;
    }


    /**
     * 仮売上オーダーID の値を返します
	 *
     * @return char 仮売上オーダーID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesOrderId() {
		return $this->_properties['provisorySalesOrderId'];
    }


    /**
     * 仮売上オーダーID の値をセットします
	 *
	 * @param char $provisorySalesOrderId 仮売上オーダーID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesOrderId($provisorySalesOrderId) {
		$this->_properties['provisorySalesOrderId'] = $provisorySalesOrderId;
    }


    /**
     * お試し期間 の値を返します
	 *
     * @return int お試し期間
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriod() {
		return $this->_properties['freePeriod'];
    }


    /**
     * お試し期間 の値をセットします
	 *
	 * @param int $freePeriod お試し期間
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriod($freePeriod) {
		$this->_properties['freePeriod'] = $freePeriod;
    }


    /**
     * お試し期間タイプ の値を返します
	 *
     * @return varchar お試し期間タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFreePeriodType() {
		return $this->_properties['freePeriodType'];
    }


    /**
     * お試し期間タイプ の値をセットします
	 *
	 * @param varchar $freePeriodType お試し期間タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFreePeriodType($freePeriodType) {
		$this->_properties['freePeriodType'] = $freePeriodType;
    }


    /**
     * 返金保証期間 の値を返します
	 *
     * @return int 返金保証期間
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriod() {
		return $this->_properties['guaranteePeriod'];
    }


    /**
     * 返金保証期間 の値をセットします
	 *
	 * @param int $guaranteePeriod 返金保証期間
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriod($guaranteePeriod) {
		$this->_properties['guaranteePeriod'] = $guaranteePeriod;
    }


    /**
     * 返金保証期間タイプ の値を返します
	 *
     * @return varchar 返金保証期間タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getGuaranteePeriodType() {
		return $this->_properties['guaranteePeriodType'];
    }


    /**
     * 返金保証期間タイプ の値をセットします
	 *
	 * @param varchar $guaranteePeriodType 返金保証期間タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setGuaranteePeriodType($guaranteePeriodType) {
		$this->_properties['guaranteePeriodType'] = $guaranteePeriodType;
    }


    /**
     * 発送開始日 の値を返します
	 *
     * @return date 発送開始日
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingStartDate() {
		return $this->_properties['shippingStartDate'];
    }


    /**
     * 発送開始日 の値をセットします
	 *
	 * @param date $shippingStartDate 発送開始日
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingStartDate($shippingStartDate) {
		$this->_properties['shippingStartDate'] = $shippingStartDate;
    }


    /**
     * 初回商品金額 の値を返します
	 *
     * @return int 初回商品金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstAmount() {
		return $this->_properties['firstAmount'];
    }


    /**
     * 初回商品金額 の値をセットします
	 *
	 * @param int $firstAmount 初回商品金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstAmount($firstAmount) {
		$this->_properties['firstAmount'] = $firstAmount;
    }


    /**
     * 初回発送金額 の値を返します
	 *
     * @return int 初回発送金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstShippingAmount() {
		return $this->_properties['firstShippingAmount'];
    }


    /**
     * 初回発送金額 の値をセットします
	 *
	 * @param int $firstShippingAmount 初回発送金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstShippingAmount($firstShippingAmount) {
		$this->_properties['firstShippingAmount'] = $firstShippingAmount;
    }


    /**
     * 初回代引手数料 の値を返します
	 *
     * @return int 初回代引手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFirstDeliveryFee() {
		return $this->_properties['firstDeliveryFee'];
    }


    /**
     * 初回代引手数料 の値をセットします
	 *
	 * @param int $firstDeliveryFee 初回代引手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFirstDeliveryFee($firstDeliveryFee) {
		$this->_properties['firstDeliveryFee'] = $firstDeliveryFee;
    }


    /**
     * 継続金額 の値を返します
	 *
     * @return int 継続金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityAmount() {
		return $this->_properties['continuityAmount'];
    }


    /**
     * 継続金額 の値をセットします
	 *
	 * @param int $continuityAmount 継続金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityAmount($continuityAmount) {
		$this->_properties['continuityAmount'] = $continuityAmount;
    }


    /**
     * 継続発送金額 の値を返します
	 *
     * @return int 継続発送金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityShippingAmount() {
		return $this->_properties['continuityShippingAmount'];
    }


    /**
     * 継続発送金額 の値をセットします
	 *
	 * @param int $continuityShippingAmount 継続発送金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityShippingAmount($continuityShippingAmount) {
		$this->_properties['continuityShippingAmount'] = $continuityShippingAmount;
    }


    /**
     * 継続代引手数料 の値を返します
	 *
     * @return int 継続代引手数料
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getContinuityDeliveryFee() {
		return $this->_properties['continuityDeliveryFee'];
    }


    /**
     * 継続代引手数料 の値をセットします
	 *
	 * @param int $continuityDeliveryFee 継続代引手数料
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setContinuityDeliveryFee($continuityDeliveryFee) {
		$this->_properties['continuityDeliveryFee'] = $continuityDeliveryFee;
    }


    /**
     * 請求タイプ の値を返します
	 *
     * @return varchar 請求タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getChargeType() {
		return $this->_properties['chargeType'];
    }


    /**
     * 請求タイプ の値をセットします
	 *
	 * @param varchar $chargeType 請求タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setChargeType($chargeType) {
		$this->_properties['chargeType'] = $chargeType;
    }


    /**
     * 支払タイプ の値を返します
	 *
     * @return varchar 支払タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentType() {
		return $this->_properties['paymentType'];
    }


    /**
     * 支払タイプ の値をセットします
	 *
	 * @param varchar $paymentType 支払タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentType($paymentType) {
		$this->_properties['paymentType'] = $paymentType;
    }


    /**
     * カード分割回数 の値を返します
	 *
     * @return int カード分割回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCardDivideNumber() {
		return $this->_properties['cardDivideNumber'];
    }


    /**
     * カード分割回数 の値をセットします
	 *
	 * @param int $cardDivideNumber カード分割回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCardDivideNumber($cardDivideNumber) {
		$this->_properties['cardDivideNumber'] = $cardDivideNumber;
    }


    /**
     * カード分割タイプ の値を返します
	 *
     * @return varchar カード分割タイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCardDivideType() {
		return $this->_properties['cardDivideType'];
    }


    /**
     * カード分割タイプ の値をセットします
	 *
	 * @param varchar $cardDivideType カード分割タイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCardDivideType($cardDivideType) {
		$this->_properties['cardDivideType'] = $cardDivideType;
    }


    /**
     * 発送回数 の値を返します
	 *
     * @return int 発送回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingNumber() {
		return $this->_properties['shippingNumber'];
    }


    /**
     * 発送回数 の値をセットします
	 *
	 * @param int $shippingNumber 発送回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingNumber($shippingNumber) {
		$this->_properties['shippingNumber'] = $shippingNumber;
    }


    /**
     * ダイレクト分割フラグ の値を返します
	 *
     * @return char ダイレクト分割フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentFlag() {
		return $this->_properties['directInstallmentFlag'];
    }


    /**
     * ダイレクト分割フラグ の値をセットします
	 *
	 * @param char $directInstallmentFlag ダイレクト分割フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentFlag($directInstallmentFlag) {
		$this->_properties['directInstallmentFlag'] = $directInstallmentFlag;
    }


    /**
     * ダイレクト分割請求回数 の値を返します
	 *
     * @return int ダイレクト分割請求回数
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentChargeNumber() {
		return $this->_properties['directInstallmentChargeNumber'];
    }


    /**
     * ダイレクト分割請求回数 の値をセットします
	 *
	 * @param int $directInstallmentChargeNumber ダイレクト分割請求回数
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentChargeNumber($directInstallmentChargeNumber) {
		$this->_properties['directInstallmentChargeNumber'] = $directInstallmentChargeNumber;
    }


    /**
     * ダイレクト分割初回金額 の値を返します
	 *
     * @return int ダイレクト分割初回金額
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDirectInstallmentPrice() {
		return $this->_properties['directInstallmentPrice'];
    }


    /**
     * ダイレクト分割初回金額 の値をセットします
	 *
	 * @param int $directInstallmentPrice ダイレクト分割初回金額
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDirectInstallmentPrice($directInstallmentPrice) {
		$this->_properties['directInstallmentPrice'] = $directInstallmentPrice;
    }


    /**
     * 仮売上フラグ の値を返します
	 *
     * @return char 仮売上フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getProvisorySalesFlag() {
		return $this->_properties['provisorySalesFlag'];
    }


    /**
     * 仮売上フラグ の値をセットします
	 *
	 * @param char $provisorySalesFlag 仮売上フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setProvisorySalesFlag($provisorySalesFlag) {
		$this->_properties['provisorySalesFlag'] = $provisorySalesFlag;
    }


    /**
     * デビットフラグ の値を返します
	 *
     * @return char デビットフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDebitFlag() {
		return $this->_properties['debitFlag'];
    }


    /**
     * デビットフラグ の値をセットします
	 *
	 * @param char $debitFlag デビットフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDebitFlag($debitFlag) {
		$this->_properties['debitFlag'] = $debitFlag;
    }


    /**
     * 購入時発送フラグ の値を返します
	 *
     * @return char 購入時発送フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getShippingAtOrderFlag() {
		return $this->_properties['shippingAtOrderFlag'];
    }


    /**
     * 購入時発送フラグ の値をセットします
	 *
	 * @param char $shippingAtOrderFlag 購入時発送フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setShippingAtOrderFlag($shippingAtOrderFlag) {
		$this->_properties['shippingAtOrderFlag'] = $shippingAtOrderFlag;
    }


    /**
     * 決済日時 の値を返します
	 *
     * @return datetime 決済日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPaymentDatetime() {
		return $this->_properties['paymentDatetime'];
    }


    /**
     * 決済日時 の値をセットします
	 *
	 * @param datetime $paymentDatetime 決済日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPaymentDatetime($paymentDatetime) {
		$this->_properties['paymentDatetime'] = $paymentDatetime;
    }


    /**
     * エラータイプ の値を返します
	 *
     * @return char エラータイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorType() {
		return $this->_properties['errorType'];
    }


    /**
     * エラータイプ の値をセットします
	 *
	 * @param char $errorType エラータイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorType($errorType) {
		$this->_properties['errorType'] = $errorType;
    }


    /**
     * エラーコード の値を返します
	 *
     * @return char エラーコード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorCode() {
		return $this->_properties['errorCode'];
    }


    /**
     * エラーコード の値をセットします
	 *
	 * @param char $errorCode エラーコード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorCode($errorCode) {
		$this->_properties['errorCode'] = $errorCode;
    }


    /**
     * エラー情報 の値を返します
	 *
     * @return char エラー情報
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorInfo() {
		return $this->_properties['errorInfo'];
    }


    /**
     * エラー情報 の値をセットします
	 *
	 * @param char $errorInfo エラー情報
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorInfo($errorInfo) {
		$this->_properties['errorInfo'] = $errorInfo;
    }


    /**
     * エラーフラグ の値を返します
	 *
     * @return char エラーフラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getErrorFlag() {
		return $this->_properties['errorFlag'];
    }


    /**
     * エラーフラグ の値をセットします
	 *
	 * @param char $errorFlag エラーフラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setErrorFlag($errorFlag) {
		$this->_properties['errorFlag'] = $errorFlag;
    }


    /**
     * 途中解約フラグ の値を返します
	 *
     * @return char 途中解約フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMidtermCancellationFlag() {
		return $this->_properties['midtermCancellationFlag'];
    }


    /**
     * 途中解約フラグ の値をセットします
	 *
	 * @param char $midtermCancellationFlag 途中解約フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMidtermCancellationFlag($midtermCancellationFlag) {
		$this->_properties['midtermCancellationFlag'] = $midtermCancellationFlag;
    }


    /**
     * 申込方法 の値を返します
	 *
     * @return varchar 申込方法
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getApplicationType() {
		return $this->_properties['applicationType'];
    }


    /**
     * 申込方法 の値をセットします
	 *
	 * @param varchar $applicationType 申込方法
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setApplicationType($applicationType) {
		$this->_properties['applicationType'] = $applicationType;
    }


    /**
     * 注文取消フラグ の値を返します
	 *
     * @return char 注文取消フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderCancelFlag() {
		return $this->_properties['orderCancelFlag'];
    }


    /**
     * 注文取消フラグ の値をセットします
	 *
	 * @param char $orderCancelFlag 注文取消フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderCancelFlag($orderCancelFlag) {
		$this->_properties['orderCancelFlag'] = $orderCancelFlag;
    }


    /**
     * 注文取消日時 の値を返します
	 *
     * @return datetime 注文取消日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getOrderCancelDatetime() {
		return $this->_properties['orderCancelDatetime'];
    }


    /**
     * 注文取消日時 の値をセットします
	 *
	 * @param datetime $orderCancelDatetime 注文取消日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setOrderCancelDatetime($orderCancelDatetime) {
		$this->_properties['orderCancelDatetime'] = $orderCancelDatetime;
    }


    /**
     * tpd_dummy_flag の値を返します
	 *
     * @return char tpd_dummy_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDummyFlag() {
		return $this->_properties['dummyFlag'];
    }


    /**
     * tpd_dummy_flag の値をセットします
	 *
	 * @param char $dummyFlag tpd_dummy_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDummyFlag($dummyFlag) {
		$this->_properties['dummyFlag'] = $dummyFlag;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























