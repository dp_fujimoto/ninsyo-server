<?php
/**
 * @package Mikoshiva_Validate
 */


/**
 * 電話番号を検証します
 *
 * @package Mikoshiva_Validate
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/28
 * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Tel extends Zend_Validate_Abstract {


    const TEL_EMPTY = 'telEmpty';
    const STRING_EMPTY = 'telEmpty';
    const INVALID   = 'invalid';


    /**
     * Digits filter used for validation
     *
     * @var Zend_Filter_Digits
     */
    protected static $_filter = null;


    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::STRING_EMPTY => "【%COLUMN_NAME%】 は必須項目です。",
        self::INVALID      => "【%COLUMN_NAME%】 の形式が正しく有りません。",
    );

    /**
     * 電話番号を保持します
     * @var Mikoshiva_Property_Tel 電話番号を保持します
     */
    protected $_tel;


    /**
     * 電話番号市外局番 を保持します
     * @var string 電話番号市外局番 を保持します
     */
    protected $_tel2Name;

    /**
     * 電話番号契約者番号 を保持します
     * @var string 電話番号契約者番号 を保持します
     */
    protected $_tel3Name;
    
    /**
     * リクエストデータ を保持します
     * @var array リクエストデータ を保持します
     */
    protected $_data;

    /**
     * 電話番号をセットします
     *
     * @param string|array $tel 電話番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setTel($tel) {
        logInfo(LOG_START);
        if(!isBlankOrNull($this->_tel2Name) || !isBlankOrNull($this->_tel3Name)) {
            $data = array();
            $data = $this->getData();
            if(isBlankOrNull($this->_tel2Name)) {
                throw new Mikoshiva_Validate_Exception('tel2のnameが空です');
            }
            if(isBlankOrNull($this->_tel3Name)) {
                throw new Mikoshiva_Validate_Exception('tel3のnameが空です');
            }
            $tel2 = '';
            $tel2 = $data[$this->_tel2Name];
            $tel3 = '';
            $tel3 = $data[$this->_tel3Name];
            $this->_tel = $tel . '-' . $tel2 . '-' . $tel3;

        } else {
            $this->_tel = $tel;
        }
        logInfo(LOG_END);
    }

    /**
     * 電話番号市外局番 をセットします
     *
     * @param string $tel2Name 電話番号市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setTel2Name($tel2Name) {
        logInfo(LOG_START);
        $this->_tel2Name = $tel2Name;
        logInfo(LOG_END);
    }

    /**
     * 電話番号契約者番号 をセットします
     *
     * @param string $tel3Name 電話番号契約者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setTel3Name($tel3Name) {
        logInfo(LOG_START);
        $this->_tel3Name = $tel3Name;
        logInfo(LOG_END);
    }

    /**
     * リクエストデータ をセットします
     *
     * @param array $data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {
        logInfo(LOG_START);
        $this->_data = $data;
        logInfo(LOG_END);
    }

    /**
     * 電話番号を返します
     *
     * @return Mikoshiva_Property_Tel $tel 電話番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getTel() {
        logInfo(LOG_START);
        return $this->_tel;
        logInfo(LOG_END);
    }

    /**
     * 電話番号市外局番 を返します
     *
     * @return string $this->_tel2Name 電話番号市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getTel2Name() {
        logInfo(LOG_START);
        return $this->_tel2Name;
        logInfo(LOG_END);
    }

    /**
     * 電話番号契約者番号 を返します
     *
     * @return string $this->_tel3Name 電話番号契約者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getTel3Name() {
        logInfo(LOG_START);
        return $this->_tel3Name;
        logInfo(LOG_END);
    }

    

    /**
     * リクエストデータ を返します
     *
     * @return array $this->_data リクエストデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/23
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getData() {
        logInfo(LOG_START);
        return $this->_data;
        logInfo(LOG_END);
    }



    public function __construct($tel2 = '', $tel3 = '') {
        $this->setTel2Name($tel2);
        $this->setTel3Name($tel3);
    }

    /**
     * 電話番号チェック
     *
     * @param string|array $tel 電話番号
     * @return boolean
     * (non-PHPdoc)
     * @see library/Zend/Validate/Zend_Validate_Interface#isValid($value)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/28
     * @version SVN:$Id: Tel.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function isValid($value) {
        logInfo(LOG_START);

        $this->setTel($value);
        $telNo = '';
        $telNo = $this->_tel;

        if(isBlankOrNull($telNo)) {
            $this->_error(self::TEL_EMPTY);
            logInfo(LOG_END);
            return false;
        }


        // 長さチェック
        if(strlen($telNo) < 12 || strlen($telNo) > 13) {
            $this->_error(self::INVALID);

            logInfo(LOG_END);
            return false;

        }

        // 形式チェック
        if (preg_match('#^[0-9]{2,4}-[0-9]{2,4}-[0-9]{4}$#', $telNo) !== 1) {
            $this->_error(self::INVALID);

            logInfo(LOG_END);
            return false;
        }


        logInfo(LOG_END);
        return true;
    }

}
