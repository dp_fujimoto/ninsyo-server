<?php


/**
 * DB のテーブル情報を取得して popo クラスを自動で作成します
 *
 * @var unknown_type
 */



$template = file_get_contents(dirname(__FILE__) . '/Template.txt');


include_once dirname(__FILE__) . '/config.php';





//-----------------------------------
// ■クラス名
//------------------------------------
        $_template = $template;
        $_template  = str_replace('%CLASS_NAME%', $className, $_template);




        //-----------------------------------
        // ■プロパティ名
        //------------------------------------
        $match = array();
        preg_match('#%PRO_START%(.*?)%PRO_END%#s', $_template, $match);
        $pro = $match[1];
        $proTemplate = '';
        foreach ($properties as $col => $data) {

            $propertyName = $col;
            $_data = is_null($data) ? 'null' : "'{$data}'";
            $proTemplate .= "{$pro}'{$propertyName}' => {$_data},\n";
        }

        $_template = preg_replace('#%PRO_START%.*?%PRO_END%#', $proTemplate, $_template);


        //-----------------------------------
        // ■getter setter
        //------------------------------------
        $match = array();
        preg_match('#%METHOD_START%(.*?)%METHOD_END%#s', $template, $match);//Zend_Debug::dump($match[1]);exit;
        $methodTemplate = $match[1];
        $_methodTemplate = '';
        foreach ($properties as $col => $data) {

            $methodName       = '';
            $methodName       = ucfirst($col);

            $propertyName = '';
            $propertyName = preg_replace('#^.*?_#', '', $col, 1);
            $propertyName = preg_replace('#_([a-z]+?)#e', "ucfirst('$1')", $propertyName);

            $_methodTemplate .= str_replace('%METHOD_NAME%', $methodName, $methodTemplate);
            $_methodTemplate  = str_replace('%COL_NAME%', $propertyName, $_methodTemplate);
            $_methodTemplate  = str_replace('%PROPERTY_NAME%', $propertyName, $_methodTemplate);
            $_methodTemplate  = str_replace('%DATA_TYPE%', $data['DATA_TYPE'], $_methodTemplate);
        }

        $_template = preg_replace('#%METHOD_START%.*?%METHOD_END%#s', $_methodTemplate, $_template);



        //-----------------------------------
        // ■file create
        //------------------------------------
        //Zend_Debug::dump($template);
        file_put_contents($createPath, $_template);



















