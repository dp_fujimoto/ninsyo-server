<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';


/**
 * ﾊﾞｯﾁ終了のメッセージを作成
 *
 * @author       K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2011/04/01
 * @version        SVN: $Id:
 *
 */
class Tool_Models_BatchTest_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * ﾊﾞｯﾁ終了のメッセージを作成
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $str = '';
        $downloadFlag = '0';

        switch($actionForm->getMode()) {
            case '1': $str  .= 'ユーザーコンテンツ登録タスク処理を実行しました。';
                    break;
            case '2': $str  .= 'エッジトレーダーアクセス解析処理を実行しました。';
                    break;
            default:$str .= '想定外の処理のため実行に失敗しました。';
                    break;
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $str;
    }

}
