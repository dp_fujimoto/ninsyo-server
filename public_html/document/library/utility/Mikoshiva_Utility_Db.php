
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; Task 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#saveDirectionStatusHistory">Mikoshiva_Utility_Db#saveDirectionStatusHistory - 指示ステータス履歴テーブルへデータ登録を行います。</a></li>
            </ol>



            <!-- ==============================================
            ■Mikoshiva_Utility_Db#saveDirectionStatusHistory
            ============================================== -->
            <h2 id="Mikoshiva_Utility_Db#saveDirectionStatusHistory"><a name="saveDirectionStatusHistory">Mikoshiva_Utility_Db#saveDirectionStatusHistory</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              指示ステータス履歴テーブルへデータ登録を行います。<br>
            </p>
			<p>
              引数に与えられた「指示ステータス」データを履歴として登録し、<br>
              登録成功した「指示ステータス履歴」データを戻り値として返します。<br>
              万が一、登録に失敗した場合は、例外がスローされます。<br>
            </p>
            <p>
              <font color="red"><b>※注意</b></font><br>
              このメソッドは内部で指示ステータス履歴テーブルへインサートを行っておりますが、<br>
              トランザクション制御は行われておりません。<br>			  
              必ず呼び出し側でトランザクション制御を行うように実装して下さい。<br>
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$db</td>
                <td>Zend_Db_Adapter_Abstract</td>
                <td><br></td>
                <td>Object</td>
                <td>
                  データベースアダプタオブジェクト
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>$tds</td>
                <td>Popo_TrxDirectionStatus</td>
                <td><br></td>
                <td>POPO</td>
				<td>
                  「指示ステータス履歴テーブル」へ記録する「指示ステータス」のPOPO
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td>$resultTdh</td>
                <td>Popo_TrxDirectionStatusHistory</td>
                <td><br></td>
                <td>POPO</td>
                <td>登録成功した「指示ステータス履歴」のPOPO</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

require_once \'Mikoshiva/Utility/Db.php\';

require_once \'Popo/TrxDirectionStatus.php\';
require_once \'Popo/TrxDirectionStatusHistory.php\';


// 指示ステータス取得
$simple = null;
$simple = new Mikoshiva_Db_Simple();

/* @var $tds Popo_TrxDirectionStatus */
$tds = null;
$tds = $s->simpleOneSelectById(\'Mikoshiva_Db_Ar_TrxDirectionStatus\', 32);


// 指示ステータス履歴登録
$utilDb = null;
$utilDb = new Mikoshiva_Utility_Db();

/* @var $resultTdh Popo_TrxDirectionStatusHistory */
$resultTdh = null;
$resultTdh = $utilDb->saveDirectionStatusHistory($this->_db, $tds);


dumper($resultTdh);

');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">

object(Popo_TrxDirectionStatusHistory)#34 (2) {
  ["_columnPrefix:protected"] => string(3) "tdh"
  ["_properties:protected"] => array(58) {
    ["directionStatusHistoryId"] => string(1) "9"
    ["directionStatusHistoryNumber"] => string(1) "2"
    ["tdsDirectionStatusId"] => string(2) "32"
    ["tdsDirectionId"] => string(2) "29"
    ["tdsStatusNumber"] => string(1) "2"
    ["tdsCampaignProductId"] => string(1) "2"
    ["tdsPurchaseDetailId"] => string(2) "34"
    ["tdsMemberId"] => string(17) "DP-0000000011-659"
    ["tdsAddressNumber"] => string(1) "1"
    ["tdsChargeType"] => string(16) "CHARGE_TYPE_CARD"
    ["tdsProposalDatetime"] => string(19) "2010-07-21 21:03:29"
    ["tdsFreePeriod"] => string(1) "0"
    ["tdsFreePeriodType"] => string(22) "FREE_PERIOD_TYPE_MONTH"
    ["tdsGuaranteePeriod"] => string(1) "2"
    ["tdsGuaranteePeriodType"] => string(27) "GUARANTEE_PERIOD_TYPE_MONTH"
    ["tdsFirstOrderId"] => string(27) "DP-0000000011-659-000000-00"
    ["tdsFirstAmount"] => string(5) "29800"
    ["tdsFirstShippingAmount"] => string(3) "640"
    ["tdsFirstDeliveryFee"] => string(3) "420"
    ["tdsProvisorySalesOrderId"] => NULL
    ["tdsContinuityAmount"] => string(1) "0"
    ["tdsContinuityShippingAmount"] => string(1) "0"
    ["tdsContinuityDeliveryFee"] => string(1) "0"
    ["tdsCardDivideNumber"] => string(1) "1"
    ["tdsCardDivideType"] => string(23) "CARD_DIVIDE_TYPE_SINGLE"
    ["tdsSubscriptionPackageFlag"] => string(1) "1"
    ["tdsPackageShippingNumber"] => string(2) "12"
    ["tdsPackageShippingPlanNumber"] => string(2) "13"
    ["tdsShippingNumber"] => string(1) "1"
    ["tdsChargeNumber"] => string(1) "1"
    ["tdsDirectInstallmentFlag"] => string(1) "0"
    ["tdsDirectInstallmentChargeNumber"] => string(1) "0"
    ["tdsDirectInstallmentPrice"] => string(1) "0"
    ["tdsDebitFlag"] => string(1) "0"
    ["tdsActiveFlag"] => string(1) "1"
    ["tdsMidtermCancellationFlag"] => string(1) "0"
    ["tdsApplicationType"] => string(20) "APPLICATION_TYPE_FAX"
    ["tdsShippingAtOrderFlag"] => string(1) "1"
    ["tdsStopFlag"] => string(1) "0"
    ["tdsStopDatetime"] => NULL
    ["tdsStopReasonType"] => NULL
    ["tdsSubscriptionRestartDate"] => NULL
    ["tdsSubscriptionCancelFlag"] => string(1) "0"
    ["tdsSubscriptionCancelDatetime"] => NULL
    ["tdsReturnCancelFlag"] => string(1) "0"
    ["tdsCancelDatetime"] => NULL
    ["tdsOrderCancelFlag"] => string(1) "0"
    ["tdsOrderCancelDatetime"] => NULL
    ["tdsDeleteFlag"] => string(1) "0"
    ["tdsDeletionDatetime"] => NULL
    ["tdsRegistrationDatetime"] => string(19) "2010-07-21 21:03:29"
    ["tdsUpdateDatetime"] => string(19) "2010-07-21 21:03:29"
    ["tdsUpdateTimestamp"] => string(19) "2010-07-21 21:03:34"
    ["deleteFlag"] => string(1) "0"
    ["deletionDatetime"] => NULL
    ["registrationDatetime"] => string(19) "2010-07-23 12:03:01"
    ["updateDatetime"] => string(19) "2010-07-23 12:03:01"
    ["updateTimestamp"] => string(19) "2010-07-23 12:03:02"
  }
}

</pre>


            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>



  </div>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>





























