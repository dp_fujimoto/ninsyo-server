<?php
/**
 * @package Mikoshiva_Property
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';

/**
 * 郵便番号を保持します
 * 
 * @package Mikoshiva_Property
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/09
 * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Property_Zip {
        /**
     * 郵便番号上4桁 を保持します
     * @var string 郵便番号上4桁 を保持します
     */
    protected $_zip1;

    /**
     * 郵便番号下4桁 を保持します
     * @var string 郵便番号下4桁 を保持します
     */
    protected $_zip2;

    /**
     * 郵便番号上4桁 をセットします
     *
     * @param string $zip1 郵便番号上4桁
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setZip1($zip1) {
        logInfo(LOG_START);
        $this->_zip1 = $zip1;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号下4桁 をセットします
     *
     * @param string $zip2 郵便番号下4桁
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setZip2($zip2) {
        logInfo(LOG_START);
        $this->_zip2 = $zip2;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号上4桁 を返します
     *
     * @return string $zip1 郵便番号上4桁
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getZip1() {
        logInfo(LOG_START);
        return $this->_zip1;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号下4桁 を返します
     *
     * @return string $zip2 郵便番号下4桁
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getZip2() {
        logInfo(LOG_START);
        return $this->_zip2;
        logInfo(LOG_END);
    }

    
    /**
     * 郵便番号をstring(xxx-yyyy)で返します。
     *
     * @return string
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function toString() {
        return $this->_zip1 . '-' . $this->_zip2;
    }
    
    
    /**
     * 初期設定を行います。
     *
     * @param string $zip1
     * @param string $zip2
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Zip.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct($zip1 = '', $zip2 = '') {
        $this->setZip1($zip1);
        $this->setZip2($zip2);
    }
}