<?php
require_once 'Mikoshiva/Controller/Model/Class/Db/Abstract.php';

/**
 * アクションログを保存します
 *
 * @author K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Classes_InsertActionLog
    extends Mikoshiva_Controller_Model_Class_Db_Abstract {

    /**
    * アクションログを保存します
    *
    */
    public function execute($text) {

        if (!isBlankOrNull($text)) {
            $where = array();
            $where['content'] = $text;

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            // 更新（シンプルクラス）
            $result = $db->simpleOneCreateOrUpdateById('Mikoshiva_Db_NinsyoAr_TrxActionLog', $where);
        }

        return null;
    }
}
