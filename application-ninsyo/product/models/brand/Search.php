<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstProduct.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Product_Models_Brand_Search extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 出力
        $output = array();

        //-----------------------------------------------
        //
        // ブランド検索
        //
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT";
        $sql .= " *";
        $sql .= " FROM mst_brand";
        $sql .= " WHERE 1 = 1 ";

        // パラメータセット
        $params = array();

        if ($actionForm->getMode() !== 'search') {
            // 部署が未入力であれば、初期値を代入
            if(isBlankOrNull($actionForm->getDivisionId())) {
                $actionForm->setDivisionId($context->getStaff()->changeDivisionId);
            }
        }

        // 部署ID
        if (!isBlankOrNull($actionForm->getDivisionId())) {
            $sql .= " AND mbr_division_id = ?";
            $params[] = $actionForm->getDivisionId();
        }

        // ブランド名
        if (!isBlankOrNull( $actionForm->getBrandName())) {
            $sql .= " AND mbr_brand_name like ?";
            $params[] = '%' . $actionForm->getBrandName() . '%';
        }

        // 状態
        if (!isBlankOrNull($actionForm->getDeleteFlag())) {
            $sql .= " AND mbr_delete_flag = ?";
            $params[] = $actionForm->getDeleteFlag();
        } else {
            $sql .= " AND mbr_delete_flag = '0'";
        }

        $sql .= " ORDER BY mbr_brand_name";

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $output[] = $v;
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;

    }
}
