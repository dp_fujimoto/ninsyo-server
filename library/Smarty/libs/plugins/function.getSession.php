<?php

require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Zend/Session/Namespace.php';

/**
 * セッションの値を返します。
 * @param array  $params
 *                  string name      取り出したいキー名前
 *                  string namespace 名前空間の名前
 * @param Smarty &$smarty
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: function.getSession.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $

 * @return mixed
 */
function smarty_function_getSession( $params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['name']         = '';
    $default['namespace']    = null;

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 name に値がありません。');
    }

    if(isBlankOrNull($data['namespace']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 namespace に値がありません。');
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $name       = '';
    $namespace  = '';

    $name      = $data['name'];
    $namespace = $data['namespace'];
    $session = null;
    if ( isset( $namespace ) ) {
        $session = new Zend_Session_Namespace( $namespace );
    } else {
        $session = new Zend_Session_Namespace();
    }

    if ( ! isset( $name ) ) {
        Mikoshiva_Logger::info(LOG_END);
        return $session;
    } else if( is_array( $session ) ) {
        Mikoshiva_Logger::info(LOG_END);
        return $session[ $name ];
    } else {
        Mikoshiva_Logger::info(LOG_END);
        return $session->$name;
    }
}
