<?php

require_once('DPConfig.php');
class DPAuthzMerge{
	
	var $lockfp;
	var $pcode;
	var $yyyymm;
	var $authfile;
	var $savefile;
	
	function DPAuthzMerge($key,$pcode,$yyyymm){
		$this->pcode = $pcode;
		$this->yyyymm = $yyyymm;
		// 更新キーの確認
		if(MERGE_KEY != $key){
			throw new Exception("DPAuthzMerge Permition denied.");
		}
//		$yyyymm = substr($date,0,6);
		$this->authfile = AUTH_FILE_SERVER_DIR."/".$pcode.$yyyymm.'.csv';
		$this->savefile = AUTH_FILE_SERVER_SAVE_DIR."/".$pcode.$yyyymm.'.csv';
		
	}
	// 設定済みデータの読み込み
	function _loadAuthz($fpr){
		$pcode = $this->pcode;
		$authfile = $this->authfile;
		$authz = array();
		if($fpr){
			flock($fpr,LOCK_EX);
		}else{
			if(!file_exists($authfile)) return $authz;
			$fpr = fopen($authfile,"r");
			if(!$fpr){
				throw new Exception("Can't open file. authfile=$authfile");
			}
		}
		// 登録済みユーザを配列に詰める
		$authz = array();
		
		while(feof($fpr) == false) {
		    $line = fgets($fpr);
		    if(preg_match("/^\s*#.*$/",$line)) continue;
		    if(preg_match("/^\s*$/",$line)) continue;
		    $line  =preg_replace("/\r?\n/","",$line);
		    array_push($authz,$line);
		}
		
		return $authz;
	}
	// プロダクトコード別CSVファイルのマージ処理
	function merge($data){
		$pcode = $this->pcode;
		$savefile = $this->savefile;
		$authfile = $this->authfile;
		try{
			$users = array();
			if(file_exists($this->authfile)){
				$fpr = fopen($this->authfile,"r");
			}
			$users = $this->_loadAuthz($fpr);
			
			// リクエストパラメータから追加ユーザ配列を生成
			// 登録済みユーザにマージする
			$addusers = preg_split("/\r?\n/",$data);
			$users = array_merge($users,$addusers);
			$users = array_unique($users);
			
			$tmpfile = $savefile.date('Ymd_His_u').'_'.getmypid();
			$fp = fopen($tmpfile, "w") ;
			if(!$fp){
				throw new Exception("Can't open file. tmpfile=$tmpfile");
			}
			fwrite($fp, "# PRODUCT_CD: $pcode\n");
			fwrite($fp, "# MONTH     : $this->yyyymm\n");
			fwrite($fp, "# MODIFY    : ".date('Y.m.d H:i:s')."\n");
			fwrite($fp, implode("\n",$users));
			fclose($fp);
			$this->_unlock($fpr);
			
			if(!copy($tmpfile,$authfile)){
				throw new Exception("Error at rename. tmpfile=$tmpfile");
			}
		}catch(Exception $e){
			error_log($e->getMessage());
			print_r($e->getMessage());
			exit ;
		}
		return true;
	}
	// ファイルロック解除
	function _unlock($fpr){
		if($fpr){
			flock($fpr,LOCK_UN);
			fclose($fpr);
		}
	}
}
?>