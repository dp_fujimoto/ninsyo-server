<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_TrxCustomerLoginCookie
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_TrxCustomerLoginCookie extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'trx_customer_login_cookie';
    protected $_primary  = 'tcl_customer_login_cookie_id';
    protected $_metadata = array (
  'tcl_customer_login_cookie_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_customer_login_cookie_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'tcl_customer_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_customer_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_login_cookie_value' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_login_cookie_value',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '40',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_login_cookie_key' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_login_cookie_key',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_ip_address' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_ip_address',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '15',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_output_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_output_flag',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_delete_flag',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_deletion_datetime',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_update_datetime',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_registration_datetime',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tcl_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'trx_customer_login_cookie',
    'COLUMN_NAME' => 'tcl_update_timestamp',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

