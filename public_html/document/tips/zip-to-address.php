<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'><?php echo $title = '郵便番号を元に住所を入力したい'; ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1><?php echo $title; ?></h1>



            <!-- ========================================
              ■ファイルを添付したい
            ==========================================-->
            <h2>サンプル</h2>

            <pre class="wiki">
<?php
highlight_string('
<?php
<form id="Address_form" name="Address_form">
郵便番号
<input type="text" id="zip1" name="Address[zip1]"> -
<input type="text" id="zip2" name="Address_form[zip2]">
<input type="button" id="zipSubmit" value="郵便番号から住所を取得" onClick="zipToAddress(\'zip1\',\'zip2\',\'prefecture\',\'address1\')"><br>
{prefectureLister name="prefecture"}<br>
<input type="text" id="address1" name="Address_form[address1]" style="width:30em;"><br>
<input type="text" id="address2" name="Address_form[address2]" style="width:30em;">
</form>
');
?>
            </pre>
            <h2>メソッド内容</h2>
zipToAddress(zip1, zip2, prefecture, address)
            <pre class="wiki">
<table>
<tr>
<td class="COM_nowrap">zip1</td><td class="COM_nowrap">郵便番号上3桁を入力するフォーム部品のID</td>
</tr>
<tr>
<td class="COM_nowrap">zip2</td><td class="COM_nowrap">郵便番号下4桁を入力するフォーム部品のID</td>
</tr>
<tr>
<td class="COM_nowrap">prefecture</td><td class="COM_nowrap">取得した都道府県名を入力するフォーム部品のID</td>
</tr>
<tr>
<td class="COM_nowrap">address</td><td class="COM_nowrap">取得した住所を入力するフォーム部品のID</td>
</tr>
</table></pre>

  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















