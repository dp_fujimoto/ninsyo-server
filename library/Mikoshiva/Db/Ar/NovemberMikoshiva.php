<?php
/** @package Mikoshiva_Db */

/**
 * require
 */
require_once 'Mikoshiva/Db/Table/Abstract.php';

/**
 * Mikoshiva_Db_Ar_NovemberMikoshiva
 *
 * @package Mikoshiva_Db
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/01
 */
class Mikoshiva_Db_Ar_NovemberMikoshiva extends Mikoshiva_Db_Table_Abstract
{

    protected $_name     = 'november_mikoshiva';
    protected $_primary  = 'tsh_shipping_id';
    protected $_metadata = array (
  'tsh_shipping_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_id',
    'COLUMN_POSITION' => 1,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => true,
    'PRIMARY_POSITION' => 1,
    'IDENTITY' => true,
  ),
  'tsh_customer_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_customer_id',
    'COLUMN_POSITION' => 2,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_direction_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_direction_id',
    'COLUMN_POSITION' => 3,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_direction_status_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_direction_status_id',
    'COLUMN_POSITION' => 4,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_charge_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_charge_id',
    'COLUMN_POSITION' => 5,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_division_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_division_id',
    'COLUMN_POSITION' => 6,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_relative_shipping_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_relative_shipping_id',
    'COLUMN_POSITION' => 7,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_campaign_sales_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_campaign_sales_id',
    'COLUMN_POSITION' => 8,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_campaign_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_campaign_id',
    'COLUMN_POSITION' => 9,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_product_shipping_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_product_shipping_id',
    'COLUMN_POSITION' => 10,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_product_package_id' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_product_package_id',
    'COLUMN_POSITION' => 11,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_package_number' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_package_number',
    'COLUMN_POSITION' => 12,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_package_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_package_type',
    'COLUMN_POSITION' => 13,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_charge_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_charge_type',
    'COLUMN_POSITION' => 14,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_step_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_step_type',
    'COLUMN_POSITION' => 15,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_step_count' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_step_count',
    'COLUMN_POSITION' => 16,
    'DATA_TYPE' => 'int',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_shipping_group_label' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_group_label',
    'COLUMN_POSITION' => 17,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '1000',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_entity_shipping_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_entity_shipping_flag',
    'COLUMN_POSITION' => 18,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '1',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_company_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_company_name',
    'COLUMN_POSITION' => 19,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_last_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_last_name',
    'COLUMN_POSITION' => 20,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '20',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_first_name' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_first_name',
    'COLUMN_POSITION' => 21,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '20',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_last_name_kana' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_last_name_kana',
    'COLUMN_POSITION' => 22,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '40',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_first_name_kana' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_first_name_kana',
    'COLUMN_POSITION' => 23,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '40',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_zip' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_zip',
    'COLUMN_POSITION' => 24,
    'DATA_TYPE' => 'char',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '8',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_prefecture' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_prefecture',
    'COLUMN_POSITION' => 25,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '4',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_address1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_address1',
    'COLUMN_POSITION' => 26,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_address2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_address2',
    'COLUMN_POSITION' => 27,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '200',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_tel' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_tel',
    'COLUMN_POSITION' => 28,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '13',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_shipping_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_type',
    'COLUMN_POSITION' => 29,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_expected_shipping_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_expected_shipping_date',
    'COLUMN_POSITION' => 30,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_payment_delivery_price' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_payment_delivery_price',
    'COLUMN_POSITION' => 31,
    'DATA_TYPE' => 'int',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_shipping_assign' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_assign',
    'COLUMN_POSITION' => 32,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_builder_id1' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_builder_id1',
    'COLUMN_POSITION' => 33,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_builder_id2' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_builder_id2',
    'COLUMN_POSITION' => 34,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_builder_id3' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_builder_id3',
    'COLUMN_POSITION' => 35,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_builder_id4' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_builder_id4',
    'COLUMN_POSITION' => 36,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_builder_id5' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_builder_id5',
    'COLUMN_POSITION' => 37,
    'DATA_TYPE' => 'int',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_shipping_status' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_status',
    'COLUMN_POSITION' => 38,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_shipping_stop_status' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_stop_status',
    'COLUMN_POSITION' => 39,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_support_status' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_support_status',
    'COLUMN_POSITION' => 40,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_return_status' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_return_status',
    'COLUMN_POSITION' => 41,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_return_support_status' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_return_support_status',
    'COLUMN_POSITION' => 42,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_return_reason_type' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_return_reason_type',
    'COLUMN_POSITION' => 43,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => '100',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_return_request_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_return_request_date',
    'COLUMN_POSITION' => 44,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_return_complete_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_return_complete_date',
    'COLUMN_POSITION' => 45,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_return_limit_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_return_limit_date',
    'COLUMN_POSITION' => 46,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_responase_limit_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_responase_limit_date',
    'COLUMN_POSITION' => 47,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_cancel_after_shipping_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_cancel_after_shipping_date',
    'COLUMN_POSITION' => 48,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_real_shipping_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_real_shipping_date',
    'COLUMN_POSITION' => 49,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_inquiry_number' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_inquiry_number',
    'COLUMN_POSITION' => 50,
    'DATA_TYPE' => 'varchar',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => '20',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_refund_possibility_date' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_refund_possibility_date',
    'COLUMN_POSITION' => 51,
    'DATA_TYPE' => 'date',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_shipping_complete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_shipping_complete_flag',
    'COLUMN_POSITION' => 52,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_imperfection_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_imperfection_flag',
    'COLUMN_POSITION' => 53,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_delete_flag' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_delete_flag',
    'COLUMN_POSITION' => 54,
    'DATA_TYPE' => 'char',
    'DEFAULT' => '0',
    'NULLABLE' => false,
    'LENGTH' => '1',
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_deletion_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_deletion_datetime',
    'COLUMN_POSITION' => 55,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => true,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_registration_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_registration_datetime',
    'COLUMN_POSITION' => 56,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_update_datetime' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_update_datetime',
    'COLUMN_POSITION' => 57,
    'DATA_TYPE' => 'datetime',
    'DEFAULT' => NULL,
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
  'tsh_update_timestamp' => 
  array (
    'SCHEMA_NAME' => NULL,
    'TABLE_NAME' => 'november_mikoshiva',
    'COLUMN_NAME' => 'tsh_update_timestamp',
    'COLUMN_POSITION' => 58,
    'DATA_TYPE' => 'timestamp',
    'DEFAULT' => 'CURRENT_TIMESTAMP',
    'NULLABLE' => false,
    'LENGTH' => NULL,
    'SCALE' => NULL,
    'PRECISION' => NULL,
    'UNSIGNED' => NULL,
    'PRIMARY' => false,
    'PRIMARY_POSITION' => NULL,
    'IDENTITY' => false,
  ),
);


}

