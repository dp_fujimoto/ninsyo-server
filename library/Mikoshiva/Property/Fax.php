<?php
/**
 * @package Mikoshiva_Property
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';

/**
 * Fax番号を保持します
 * 
 * 
 * @package Mikoshiva_Property
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/09
 * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Property_Fax {
    
    /**
     * 市外局番 を保持します
     * @var string 市外局番 を保持します
     */
    protected $_fax1;

    /**
     * 市内局番 を保持します
     * @var string 市内局番 を保持します
     */
    protected $_fax2;

    /**
     * 加入者番号 を保持します
     * @var string 加入者番号 を保持します
     */
    protected $_fax3;

    /**
     * 市外局番 をセットします
     *
     * @param string $fax1 市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setFax1($fax1) {
        logInfo(LOG_START);
        $this->_fax1 = $fax1;
        logInfo(LOG_END);
    }

    /**
     * 市内局番 をセットします
     *
     * @param string $fax2 市内局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setFax2($fax2) {
        logInfo(LOG_START);
        $this->_fax2 = $fax2;
        logInfo(LOG_END);
    }

    /**
     * 加入者番号 をセットします
     *
     * @param string $fax3 加入者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setFax3($fax3) {
        logInfo(LOG_START);
        $this->_fax3 = $fax3;
        logInfo(LOG_END);
    }

    /**
     * 市外局番 を返します
     *
     * @return string $fax1 市外局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getFax1() {
        logInfo(LOG_START);
        return $this->_fax1;
        logInfo(LOG_END);
    }

    /**
     * 市内局番 を返します
     *
     * @return string $fax2 市内局番
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getFax2() {
        logInfo(LOG_START);
        return $this->_fax2;
        logInfo(LOG_END);
    }

    /**
     * 加入者番号 を返します
     *
     * @return string $fax3 加入者番号
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getFax3() {
        logInfo(LOG_START);
        return $this->_fax3;
        logInfo(LOG_END);
    }

    
    /**
     * Fax番号をstring(ハイフンを含む)で返します。
     *
     * @return string
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function toString() {
        return $this->_fax1 . '-' . $this->_fax2 . '-' . $this->_fax3;
    }
    
    /**
     * 初期設定を行います。
     *
     * @param string $fax1
     * @param string $fax2
     * @param string $fax3
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/09
     * @version SVN:$Id: Fax.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct($fax1 = '', $fax2 = '', $fax3 = '') {
        $this->setFax1($fax1);
        $this->setFax2($fax2);
        $this->setFax3($fax3);
    }
    
}