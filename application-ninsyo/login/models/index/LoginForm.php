<?php

include_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
include_once 'MstStaff.php';

class Login_Models_Index_LoginForm extends Mikoshiva_Controller_Mapping_Form_Abstract {


    public function validate(array $config, Zend_Controller_Request_Abstract $request) {

        $message = parent::validate($config, $request);

        if (is_array($message) && count($message) !== 0) {
            return $message;
        }

        //----------------------------------------------
        //
        // ■リクエストの値を取得
        //
        //----------------------------------------------
        $loginId         = $this->get('loginId');
        $loginPassword   = $this->get('loginPassword');

        $staffObj = null;
        $staffObj = new Mikoshiva_Db_NinsyoAr_MstStaff();

        // SELECT 文を作成
        $select = $staffObj->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
        //$select->setIntegrityCheck(false);
        //$select->join('mst_authority', 'mst_staff_id = mau_staff_id');
        $select->where('mst_email = ?', $loginId );
        $select->where('mst_login_password = ?', $loginPassword );

        // 実行
        $row = null;
        $row = $staffObj->fetchRow($select);

        if ($row === null) {
            return array('ログインIDかログインパスワードが間違っています。');
        }


        return array();
    }
}