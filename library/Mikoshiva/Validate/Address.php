<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * require
 */
require_once 'Mikoshiva/Validate/Exception.php';
require_once 'Mikoshiva/Validate/Abstract.php';
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Db/Ar/MstZipCode.php';

/**
 * メソッド isValidに与えられた引数$address(住所)が、
 * テーブルmst_zip_codeの住所(DB住所)と一致するか判定するバリデータ。
 * 住所の文字列が、DB住所の文字列を全て含めばTrueとなる。
 *
 * @package Mikoshiva_Validate
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/10
 * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Address extends Mikoshiva_Validate_Abstract{

    const NO_FORM_NAME_ZIP1       = 'noFormNameZip1';
    const NO_FORM_NAME_ZIP2       = 'noFormNameZip2';
    const NO_FORM_NAME_PREFECTURE = 'noFormNamePrefecture';
    const NO_FORM_NAME_ADDRESS2   = 'noFormNameAddress2';
    const NO_DATA_ZIP1            = 'noZip1';
    const NO_DATA_ZIP2            = 'noZip2';
    const NO_DATA_PREFECTURE      = 'noPrefecture';
    const NO_DATA_ADDRESS         = 'noAddress';
    const NO_DATA_ON_DB           = 'noData';
    const INVALID_DATA            = 'invalidData';
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NO_FORM_NAME_ZIP1         => '郵便番号(上3桁)のフォーム名が未入力です',
        self::NO_FORM_NAME_ZIP2         => '郵便番号(下4桁)のフォーム名が未入力です',
        self::NO_FORM_NAME_PREFECTURE   => '都道府県名のフォーム名が未入力です',
        self::NO_FORM_NAME_ADDRESS2     => '住所2のフォーム名が未入力です',
        self::NO_DATA_ZIP1              => '郵便番号(上3桁)が未入力です',
        self::NO_DATA_ZIP2              => '郵便番号(下4桁)が未入力です',
        self::NO_DATA_PREFECTURE        => '都道府県名が未入力です',
        self::NO_DATA_ADDRESS           => '住所が未入力です',
        self::NO_DATA_ON_DB             => '郵便番号が存在しません', // DBに郵便番号自体が存在しない
        self::INVALID_DATA              => '郵便番号と住所が一致しませんでした',
    );

    /**
     * フォームのデータ を保持します
     * @var array フォームのデータ を保持します
     */
    protected $_data;

    /**
     * setDataされた配列内で郵便番号(上3桁)を格納しているキーの名前 を保持します
     * @var string setDataされた配列内で郵便番号(上3桁)を格納しているキーの名前 を保持します
     */
    protected $_formNameZip1;

    /**
     * setDataされた配列内で郵便番号(下4桁)を格納しているキーの名前 を保持します
     * @var string setDataされた配列内で郵便番号(下4桁)を格納しているキーの名前 を保持します
     */
    protected $_formNameZip2;

    /**
     * setDataされた配列内で都道府県名を格納しているキーの名前 を保持します
     * @var string setDataされた配列内で都道府県名を格納しているキーの名前 を保持します
     */
    protected $_formNamePrefecture;

    /**
     * setDataされた配列内で住所2を格納しているキーの名前 を保持します
     * @var string setDataされた配列内で住所2を格納しているキーの名前 を保持します
     */
    protected $_formNameAddress2;

    /**
     * 郵便番号(上3桁) を保持します
     * @var string 郵便番号(上3桁) を保持します
     */
    protected $_zip1;

    /**
     * 郵便番号(下3桁) を保持します
     * @var string 郵便番号(下3桁) を保持します
     */
    protected $_zip2;

    /**
     * 都道府県名 を保持します
     * @var string 都道府県名 を保持します
     */
    protected $_prefecture;

    /**
     * 住所2 を保持します
     * @var string 住所2 を保持します
     */
    protected $_address2;

    /**
     * setDataされた配列内で郵便番号(上3桁)を格納しているキーの名前 をセットします
     *
     * @param string $formNameZip1 setDataされた配列内で郵便番号(上3桁)を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFormNameZip1($formNameZip1) {
        logInfo(LOG_START);
        $this->_formNameZip1 = $formNameZip1;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で郵便番号(下4桁)を格納しているキーの名前 をセットします
     *
     * @param string $formNameZip2 setDataされた配列内で郵便番号(下4桁)を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFormNameZip2($formNameZip2) {
        logInfo(LOG_START);
        $this->_formNameZip2 = $formNameZip2;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で都道府県名を格納しているキーの名前 をセットします
     *
     * @param string $formNamePrefecture setDataされた配列内で都道府県名を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFormNamePrefecture($formNamePrefecture) {
        logInfo(LOG_START);
        $this->_formNamePrefecture = $formNamePrefecture;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で住所2を格納しているキーの名前 をセットします
     *
     * @param string $formNameAddress2 setDataされた配列内で住所2を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setFormNameAddress2($formNameAddress2) {
        logInfo(LOG_START);
        $this->_formNameAddress2 = $formNameAddress2;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号(上3桁) をセットします
     *
     * @param string $zip1 郵便番号(上3桁)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setZip1($zip1) {
        logInfo(LOG_START);
        $this->_zip1 = $zip1;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号(下3桁) をセットします
     *
     * @param string $zip2 郵便番号(下3桁)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setZip2($zip2) {
        logInfo(LOG_START);
        $this->_zip2 = $zip2;
        logInfo(LOG_END);
    }

    /**
     * 都道府県名 をセットします
     *
     * @param string $prefecture 都道府県名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setPrefecture($prefecture) {
        logInfo(LOG_START);
        $this->_prefecture = $prefecture;
        logInfo(LOG_END);
    }

    /**
     * 住所1 をセットします
     *
     * @param string $address1 住所2
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setAddress1($address1) {
        logInfo(LOG_START);
        $this->_address1 = $address1;
        logInfo(LOG_END);
    }

    /**
     * 住所2 をセットします
     *
     * @param string $address2 住所2
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setAddress2($address2) {
        logInfo(LOG_START);
        $this->_address2 = $address2;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で郵便番号(上3桁)を格納しているキーの名前 を返します
     *
     * @return string $this->_formNameZip1 setDataされた配列内で郵便番号(上3桁)を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFormNameZip1() {
        logInfo(LOG_START);
        return $this->_formNameZip1;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で郵便番号(下4桁)を格納しているキーの名前 を返します
     *
     * @return string $this->_formNameZip2 setDataされた配列内で郵便番号(下4桁)を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFormNameZip2() {
        logInfo(LOG_START);
        return $this->_formNameZip2;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で都道府県名を格納しているキーの名前 を返します
     *
     * @return string $this->_formNamePrefecture setDataされた配列内で都道府県名を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFormNamePrefecture() {
        logInfo(LOG_START);
        return $this->_formNamePrefecture;
        logInfo(LOG_END);
    }

    /**
     * setDataされた配列内で住所2を格納しているキーの名前 を返します
     *
     * @return string $this->_formNameAddress2 setDataされた配列内で住所2を格納しているキーの名前
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getFormNameAddress2() {
        logInfo(LOG_START);
        return $this->_formNameAddress2;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号(上3桁) を返します
     *
     * @return string $this->_zip1 郵便番号(上3桁)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getZip1() {
        logInfo(LOG_START);
        return $this->_zip1;
        logInfo(LOG_END);
    }

    /**
     * 郵便番号(下3桁) を返します
     *
     * @return string $this->_zip2 郵便番号(下3桁)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getZip2() {
        logInfo(LOG_START);
        return $this->_zip2;
        logInfo(LOG_END);
    }

    /**
     * 都道府県名 を返します
     *
     * @return string $this->_prefecture 都道府県名
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getPrefecture() {
        logInfo(LOG_START);
        return $this->_prefecture;
        logInfo(LOG_END);
    }

    /**
     * 住所1 を返します
     *
     * @return string $this->_address1 住所1
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getAddress1() {
        logInfo(LOG_START);
        return $this->_address1;
        logInfo(LOG_END);
    }

    /**
     * 住所2 を返します
     *
     * @return string $this->_address2 住所2
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function getAddress2() {
        logInfo(LOG_START);
        return $this->_address2;
        logInfo(LOG_END);
    }


    /**
     * フォームのデータ をセットします
     *
     * @param array $data フォームのデータ
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/15
     * @version SVN:$Id: Address.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function setData($data) {
        logInfo(LOG_START);
        $this->_data = $data;
        logInfo(LOG_END);
    }


    /**
     * クラスを初期化します。
     * フォームデータでzipを示すキーの名前を取得します。
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @param  string $formNameZip
     */
    public function __construct($formNameZip1, $formNameZip2, $formNamePrefecture, $formNameAddress2) {
        $this->setFormNameZip1($formNameZip1);
        $this->setFormNameZip2($formNameZip2);
        $this->setFormNamePrefecture($formNamePrefecture);
        $this->setFormNameAddress2($formNameAddress2);
    }
    /**
     * Defined by Zend_Validate_Interface
     * 入力値チェック=>false =>return false
     * DB値チェック  =>false =>return false
     * 処理後チェック=>false =>return false
     * 処理後チェック=>true  =>return true
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @param  string $value
     * @return boolean
     */
    public function isValid($address1) {
        //-----------------------------------------------
        // ■プロパティチェック
        //-----------------------------------------------
        if(isBlankOrNull($this->_formNameZip1)) {
            $this->_error(self::NO_FORM_NAME_ZIP1);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_FORM_NAME_ZIP1);
            return false;
        }
        if(isBlankOrNull($this->_formNameZip2)) {
            $this->_error(self::NO_FORM_NAME_ZIP2);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_FORM_NAME_ZIP2);
            return false;
        }
        if(isBlankOrNull($this->_formNameAddress2)) {
            $this->_error(self::NO_FORM_NAME_ADDRESS2);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_FORM_NAME_ADDRESS2);
            return false;
        }
        if(isBlankOrNull($this->_formNamePrefecture)) {
            $this->_error(self::NO_FORM_NAME_PREFECTURE);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_FORM_NAME_PREFECTURE);
            return false;
        }

        if(isBlankOrNull($this->_data[$this->_formNameZip1])) {
            $this->_error(self::NO_DATA_ZIP1);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_DATA_ZIP1);
            return false;
        }
        if(isBlankOrNull($this->_data[$this->_formNameZip2])) {
            $this->_error(self::NO_DATA_ZIP2);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_DATA_ZIP2);
            return false;
        }
        if(isBlankOrNull($this->_data[$this->_formNamePrefecture])) {
            $this->_error(self::NO_DATA_PREFECTURE);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_DATA_PREFECTURE);
            return false;
        }

        $zip = '';
        // 郵便番号は -(ハイフン) 無し
        $zip = $this->_data[$this->_formNameZip1] . $this->_data[$this->_formNameZip2];
        Mikoshiva_Logger::debug('郵便番号:' . $zip);

        $address = '';
        $address = $this->_data[$this->_formNamePrefecture] . $address1 . $this->_data[$this->_formNameAddress2];
        Mikoshiva_Logger::debug('住所:' . $address);

        //-----------------------------------------------
        // ■入力値チェック
        //-----------------------------------------------

        if(isBlankOrNull($address)) {
            $this->_error(self::NO_DATA_ADDRESS);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_DATA_ADDRESS);
            return false;
        }

        //-----------------------------------------------
        // 住所マスタから、郵便番号1・2、で検索
        //-----------------------------------------------
        $db     = new Mikoshiva_Db_Ar_MstZipCode();
        $select = $db->select()->where('mzc_zip_code = ?', $zip);
        $rowsObj   = $db->fetchAll($select);

        //-----------------------------------------------
        // DB値チェック
        //-----------------------------------------------
        if ($rowsObj->count() === 0) {
            $this->_error(self::NO_DATA_ON_DB);
            Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . self::NO_DATA_ON_DB);
            return false;
        }

        //比較開始
        //  同じ郵便番号で複数の住所を持つ場合があるため、取得できたレコード数だけループする
        foreach($rowsObj->toArray() as $row) {
            // 都道府県・住所１・住所２を結合
            $dbFullAddress = $row['mzc_prefecture_name'] . $row['mzc_address1'] . $row['mzc_address2'];
            $dbFullAddress = mb_convert_kana($dbFullAddress, 'rnK');
            // 住所２の　（１丁目１0７７番地）　などは削除しておく
            $dbFullAddress = preg_replace('/\(.*/i','', $dbFullAddress); // 半角のかっこの場合
            $dbFullAddress = preg_replace('/（.*/i','', $dbFullAddress); // 全角のかっこの場合
            // 入力された郵便番号に該当する住所
            $hitAddress = $dbFullAddress;
            Mikoshiva_Logger::debug('DB住所(変換前) : ' . $hitAddress);
            Mikoshiva_Logger::debug('チェック対象住所 : ' . $address);
            //======================================================================
            // 検索結果があれば、入力住所とDB住所を照合する
            //======================================================================
            if(empty($hitAddress)) {
                $checkError[] = '存在しない郵便番号です。正しく入力してください。';
            } else {
                 // 「以下に掲載がない場合」は削除しておく。
                $hitAddress = str_replace('以下に掲載がない場合', '', $hitAddress);

                // 全角「（」以下の住所は削除
                $expHitAddress = explode('（',$hitAddress);
                $hitAddress     = $expHitAddress[0];

                // 半角「(」以下の住所は削除
                $expHitAddress2 = explode('(', $hitAddress);
                $hitAddress      = $expHitAddress2[0];

                // 数字 以下の住所は削除
                $expHitAddress3 = preg_split('/[0-9]/', $hitAddress);
                $hitAddress      = $expHitAddress3[0];

                //改行コード・スペースを削除
                $hitAddress      = trim($hitAddress);

                // DB住所の最後の文字 取得
                $hitLastStrng    = mb_substr($hitAddress, -1, 1);

                // DB住所が入力住所に含まれていたら、OK
                if(preg_match('/^'.$hitAddress.'/i', $address) == 1 ){
                    Mikoshiva_Logger::debug('"町"削除前のデータで一致');
                    return true;
                }
                Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . '郵便番号と住所が一致しませんでした' . 'ＤＢ住所：' . $hitAddress);

                // DB住所の最後の文字が「町」なら、「町」なしでの比較を行う。それ以外はDBの住所と一致しなかったため終了。
                $hitAddressWithoutMachi = '';
                if($hitLastStrng === '町'){
                    $hitAddressWithoutMachi = mb_substr($hitAddress, 0, -1);
                    Mikoshiva_Logger::debug('最後の文字が"町"だったため、$hitAddressWithoutMachiを作成=>' . $hitAddressWithoutMachi);
                } else {
                    continue;
                }

                // 町なしDB住所に、入力住所に含まれていたら、OK
                if(preg_match('/^' . $hitAddressWithoutMachi . '/i',$address) == 1 ) {
                    Mikoshiva_Logger::debug('"町"削除後のデータで一致');
                    return true;
                } else {
                    Mikoshiva_Logger::debug(__FILE__ . ' --- ' . __LINE__ . '郵便番号と住所が一致しませんでした(町抜きでも×）' . 'ＤＢ住所：' . $hitAddress);
                }
//                $checkError[] = '郵便番号と住所が一致しませんでした。自動住所入力をご利用の上、正しく入力してください。';
            }
        }//end foreach
        $this->_messageTemplates[self::INVALID_DATA] = $this->_messageTemplates[self::INVALID_DATA] . "<br>入力された住所：{$address}<br>ヒットした住所：{$hitAddress}";
        $this->_error(self::INVALID_DATA);
        Mikoshiva_Logger::debug(self::INVALID_DATA);
        return false;
    }
}
