<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstStaff.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class Tool_Models_Staff_Search extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // リターンするクラス配列
        $arrayClass = array();

        //-----------------------------------------------
        //
        // 検索フォームからサブミットされたら、条件を追加して検索を行う。
        //
        //-----------------------------------------------
        // SQL作成
        $sql = '';
        $sql .= 'SELECT DISTINCT';
        $sql .= ' *';
        $sql .= ' FROM mst_staff';
        $sql .= ' WHERE 1 = 1 ';

        // パラメータセット
        $params = array();

        // 部署ID
        if (!isBlankOrNull( $actionForm->getDivisionId() )) {
            logInfo('(' . __LINE__ . ')', 'if：部署IDあり');
            $sql .= ' AND mst_division_id = ?';
            $params[] = $actionForm->getDivisionId();
        }
        // メールアドレス
        if (!isBlankOrNull( $actionForm->getEmail() )) {
            logInfo('(' . __LINE__ . ')', 'if：メールアドレスあり');
            $sql .= ' AND mst_email like ?';
            $params[] = $actionForm->getEmail() . '%';
        }
        // 削除フラグ
        if (!isBlankOrNull( $actionForm->getDeleteFlag() )) {
            logInfo('(' . __LINE__ . ')', 'if：削除フラグあり');
            $sql .= ' AND mst_delete_flag = ?';
            $params[] = $actionForm->getDeleteFlag();
        }
        // 特殊パス
        if (!isBlankOrNull( $actionForm->getPass() ) && $actionForm->getPass() === 'password') {
            logInfo('(' . __LINE__ . ')', 'if：特殊パスあり');
            $actionForm->setSpFlag('1');
        }

        $sql .= ' ORDER BY mst_last_name_kana, mst_first_name_kana';


        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->fetchAll($sql,$params);

        //-----------------------------------------------
        // オブジェクトへの詰め替え処理
        //-----------------------------------------------
        foreach ($res as $k => $v) {

            // 配列から接頭語指定した要素だけ、取得
            $row = array();
            $row = Mikoshiva_Utility_Array::extractBeginningOfPrefix( 'mst_' , $v );

            $rowPopo = array();
            $rowPopo['mst'] = Mikoshiva_Utility_Copy::copyDbProperties( new Popo_MstStaff() , $row );

            // クラス配列 作成
            $arrayClass[] = $rowPopo;
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $arrayClass;

    }
}
