<?php
/**
 * @package Mikoshiva_Utility
 */
require_once 'Mikoshiva/Utility/Exception.php';

require_once 'Mikoshiva/Db/Simple/Effective/DeleteFlag.php';

require_once 'Mikoshiva/Db/Ar/TrxDirectionStatusHistory.php';

require_once 'Popo/TrxDirectionStatus.php';
require_once 'Popo/TrxDirectionStatusHistory.php';

/**
 * DB ユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: Db.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Utility_Db {

    /**
     * 指示ステータス履歴テーブルに指示ステータスデータを登録します。
     * インサートされた指示ステータス履歴POPOを戻り値として返します。
     *
     * ※このメソッドは指示ステータス履歴テーブルにインサートを行います。
     * ※必ず呼び出し側でトランザクション制御を行って下さい。
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     *
     * @param Zend_Db_Adapter_Abstract $db
     * @param Popo_TrxDirectionStatus $tds
     * @return Popo_TrxDirectionStatusHistory $resultTdh
     */
    public function saveDirectionStatusHistory(Popo_TrxDirectionStatus $tds) {
        logInfo(LOG_START);
        // =============================================================================
        //  履歴登録可能な指示ステータスであるかチェック
        // =============================================================================
        if (isBlankOrNull($tds->getDirectionStatusId())) {
            // エラー
            $message = '';
            $message = '指示ステータスIDが不正です。';
            throw new Mikoshiva_Utility_Exception($message);
        }


        // =============================================================================
        //  指示ステータス履歴登録
        // =============================================================================
        $popo = null;
        $popo = new Popo_TrxDirectionStatusHistory();
        foreach($tds->getProperties() as $k => $v) {
            $setter = '';
            $setter = 'setTds' . ucfirst($k);
            $popo->$setter($v);
        }

        $ar     = null;
        $ar     = new Mikoshiva_Db_Ar_TrxDirectionStatusHistory();
        $select = $ar->select();
        $select->from('trx_direction_status_history', 'tdh_direction_status_history_number');
        $select->where('tdh_tds_direction_status_id = ?', $tds->getDirectionStatusId());
        $select->where('tdh_delete_flag = ?', '0');
        $select->order('tdh_direction_status_history_number DESC');
        $select->limit(1);
        $data   = $ar->fetchAll($select);

        if($data->count() === 1) {
            $directionStatusHistoryNumber = (int)$data[0]['tdh_direction_status_history_number'] + 1;
        } else {
            $directionStatusHistoryNumber = 1;
        }
        $popo->setDirectionStatusHistoryNumber($directionStatusHistoryNumber);

        $simple = null;
        $simple = new Mikoshiva_Db_Simple_Effective_DeleteFlag();

        /* @var $resultTdh Popo_TrxDirectionStatusHistory */
        $resultTdh = null;
        $resultTdh = $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_TrxDirectionStatusHistory', $popo);


        // =============================================================================
        //  登録チェック
        // =============================================================================
        if (isBlankOrNull($resultTdh->getDirectionStatusHistoryId())) {
            // エラー
            $message = '';
            $message = '指示ステータス履歴の登録に失敗しました。';
            throw new Mikoshiva_Utility_Exception($message);
        }


        logInfo(LOG_END);
        return $resultTdh;
    }

}