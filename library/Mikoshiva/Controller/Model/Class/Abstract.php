<?php
/** @package Mikoshiva_Controller */

/**
 * Model_Class_Abstract
 *
 * Models/Classes に置かれる共通処理クラス、業務処理クラスはこのクラスを継承して下さい。
 * このクラスから DB に接続してはいけません。
 *
 * @package Mikoshiva_Controller
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/01/08
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Model_Class_Abstract {

    /**
     * リクエストクラスと DB クラスを初期化します
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/02
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     * @return unknown_type
     */
    public function __construct() {

    }

}










