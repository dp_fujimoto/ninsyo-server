<?php
require_once 'Mikoshiva/Controller/Model/Class/Db/Abstract.php';

/**
 * メールアドレスとパスワードからハッシュ値を作成します
 *
 * @since 2012/06/25
 * @author fujimoto
 */
class Api_Models_Classes_CreateUserHash
    extends Mikoshiva_Controller_Model_Class_Db_Abstract {

    /**
     * execute
     *
     */
    public function execute($string) {
        logInfo('(' . __LINE__ . ')', '開始');

        $hash = '';
        $hash =  hash(HASH_ALGO, $string, false);


        logInfo('(' . __LINE__ . ')', '終了');
        return $hash;
    }
}



