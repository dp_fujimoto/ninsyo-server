<?php


/**
 * {html_select_date}拡張したものです。
 * 基本的な仕様は同プラグインに従います。
 * ＜相違点＞
 * ■selectタグのname/idを個別に指定できます。
 * 　指定しなければname/idは空文字になります。
 *
 * @param    array    $params
 *                           string year_name            空文字           | '年'のselectタグのname, idを指定します
 *                           string month_name           空文字           | '月'のselectタグのname, idを指定します
 *                           string day_name             空文字           | '日'のselectタグのname, idを指定します
 *                           string  time                現在の時間       | 使用する日付/時間（timestamp/ YYYY-MM-DD）
 *                           string  start_year          現在の年         | ドロップダウンリストの始めの年 (年を表す数字又は現在の年からの相対年数(+/- N))
 *                           string  end_year            start_yearと同じ | ドロップダウンリストの終わりの年 (年を表す数字又は現在の年からの相対年数(+/- N))
 *                           boolean display_years       TRUE             | 年のフォームを表示するかどうか
 *                           boolean display_months      TRUE             | 月のフォームを表示するかどうか
 *                           boolean display_days        TRUE             | 日のフォームを表示するかどうか
 *                           string  month_format        %m               | 月の表示フォーマット(strftime)
 *                           string  day_format          %02d             | 日の表示のフォーマット(sprintf)
 *                           string  month_value_format  %m               | 月の値のフォーマット(strftime)
 *                           string  day_value_format    %02d             | 日の値のフォーマット (sprintf)
 *                           string  field_order         YMD              | フィールドを表示する順序
 *         20100421  廃止    string  field_separator     \n               | フィールド間に表示する文字列
 *                           string  year_separator      年               | '年'の区切り文字を指定します
 *                           string  month_separator     月               | '月'の区切り文字を指定します
 *                           string  day_separator       日               | '日'の区切り文字を指定します
 *                           boolean year_as_text        FALSE            | 年をテキストとして表示するかどうか
 *                           boolean reverse_years       FALSE            | 年を逆順で表示する
 *                           string  field_array         null             | name属性が与えられた場合、結果の値を name[Day],name[Month],name[Year]の形の連想配列にしてPHPに返す
 *                           string  year_size           null             | 年のselectタグにsize属性を追加
 *                           string  month_size          null             | 月のselectタグにsize属性を追加
 *                           string  day_size            null             | 日のselectタグにsize属性を追加
 *                           string  all_extra           null             | 全てのselect/inputタグに拡張属性を追加
 *                           string  year_extra          null             | 年のselect/inputタグに拡張属性を追加
 *                           string  month_extra         null             | 月のselect/inputタグに拡張属性を追加
 *                           string  day_extra           null             | 日のselect/inputタグに拡張属性を追加
 *                           string  year_empty          null             | 年のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。 例えば、セレクトボックスに "年を選択して下さい" と表示させる時に便利です。 年を選択しないことを示唆するのに、time属性に対して "-MM-DD" という値が指定できることに注意してください。
 *                           string  month_empty         null             | 月のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。月を選択しないことを示唆するのに、 time属性に対して "YYYY--DD" という値が指定できることに注意してください。
 *                           string  day_empty           null             | 日のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。日を選択しないことを示唆するのに、 time属性に対して "YYYY-MM-" という値が指定できることに注意してください。
 * @param    Smarty   &$smarty
 * @param    template &$template
 *
 */
function smarty_function_html_select_dateLister($params, &$smarty, &$template)
{

    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    require_once 'Mikoshiva/Logger.php';
    require_once 'Mikoshiva/View/Helper/Exception.php';
    $default = array();
    $default['year_name']          = '';
    $default['month_name']         = '';
    $default['day_name']           = '';
    $default['year_separator']     = '年';
    $default['month_separator']    = '月';
    $default['day_separator']      = '日';
    $default['field_order_format'] = '';
    $data    = $params + $default;

    $year_name       = $data['year_name'];
    $month_name      = $data['month_name'];
    $day_name        = $data['day_name'];
    $year_separator  = $data['year_separator'];
    $month_separator = $data['month_separator'];
    $day_separator   = $data['day_separator'];

    $year_id    = str_replace('[', '-', $year_name);
    $year_id    = str_replace(']', '', $year_id);
    $month_id   = str_replace('[', '-', $month_name);
    $month_id   = str_replace(']', '', $month_id);
    $day_id     = str_replace('[', '-', $day_name);
    $day_id     = str_replace(']', '', $day_id);
    $fieldOrderFormat = $data['field_order_format'];
    unset($params['field_order_format']);
    unset($params['year_name']);
    unset($params['month_name']);
    unset($params['day_name']);
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------

    require_once(SMARTY_PLUGINS_DIR . 'shared.escape_special_chars.php');
    require_once(SMARTY_PLUGINS_DIR . 'shared.make_timestamp.php');
    require_once(SMARTY_PLUGINS_DIR . 'function.html_options.php');
    //$smarty->loadPlugin('Smarty_shared_escape_special_chars');
    //$smarty->loadPlugin('Smarty_shared_make_timestamp');
    //$smarty->loadPlugin('Smarty_function_html_options');

    /* Default values. */
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    //$prefix = "Date_";
    $prefix = '';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
    $start_year = strftime("%Y");
    $end_year = $start_year;
    $display_days = true;
    $display_months = true;
    $display_years = true;
    $year_format  = "%04d"; // 追加
    $month_format = "%m";
    /* Write months as numbers by default  GL */
    $month_value_format = "%m";
    $day_format = "%02d";
    /* Write day values using this format MB */
    $day_value_format = "%02d";
    $year_as_text = false;
    /* Display years in reverse order? Ie. 2000,1999,.... */
    $reverse_years = false;
    /* Should the select boxes be part of an array when returned from PHP?
       e.g. setting it to "birthday", would create "birthday[Day]",
       "birthday[Month]" & "birthday[Year]". Can be combined with prefix */
    $field_array = null;
    /* <select size>'s of the different <select> tags.
       If not set, uses default dropdown. */
    $day_size = null;
    $month_size = null;
    $year_size = null;
    /* Unparsed attributes common to *ALL* the <select>/<input> tags.
       An example might be in the template: all_extra ='class ="foo"'. */
    $all_extra = null;
    /* Separate attributes for the tags. */
    $day_extra = null;
    $month_extra = null;
    $year_extra = null;
    /* Order in which to display the fields.
       "D" -> day, "M" -> month, "Y" -> year. */
    $field_order = 'YMD';
    /* String printed between the different fields. */
    //$field_separator = "\n";
    //$time = time();
    $time = strtotime(Mikoshiva_Date::mikoshivaNow('yyyy-MM-dd HH:mm:dd'));
    $all_empty = null;
    $day_empty = null;
    $month_empty = null;
    $year_empty = null;
    $extra_attrs = '';

    foreach ($params as $_key => $_value) {
        switch ($_key) {
            case 'prefix':
            case 'time':
            case 'start_year':
            case 'end_year':
            case 'year_format': // 追加
            case 'month_format':
            case 'day_format':
            case 'day_value_format':
            case 'field_array':
            case 'day_size':
            case 'month_size':
            case 'year_size':
            case 'all_extra':
            case 'day_extra':
            case 'month_extra':
            case 'year_extra':
            case 'field_order':
            //case 'field_separator':
            case 'month_value_format':
            case 'month_empty':
            case 'day_empty':
            case 'year_empty':
                $$_key = (string)$_value;
                break;

            case 'all_empty':
                $$_key = (string)$_value;
                $day_empty = $month_empty = $year_empty = $all_empty;
                break;

            case 'display_days':
            case 'display_months':
            case 'display_years':
            case 'year_as_text':
            case 'reverse_years':
                $$_key = (bool)$_value;
                break;

            default:
                if (!is_array($_value)) {
                    $extra_attrs .= ' ' . $_key . '="' . smarty_function_escape_special_chars($_value) . '"';
                } else {
                    trigger_error("html_select_date: extra attribute '$_key' cannot be an array", E_USER_NOTICE);
                }
                break;
        }
    }

    if (preg_match('!^-\d+$!', $time)) {
        // negative timestamp, use date()
        $time = date('Y-m-d', $time);
    }
    // If $time is not in format yyyy-mm-dd
    if (preg_match('/^(\d{0,4}-\d{0,2}-\d{0,2})/', $time, $found)) {
        $time = $found[1];
    } else {
        // use smarty_make_timestamp to get an unix timestamp and
        // strftime to make yyyy-mm-dd
        $time = strftime('%Y-%m-%d', smarty_make_timestamp($time));
    }
    // Now split this in pieces, which later can be used to set the select
    $time = explode("-", $time);
    // make syntax "+N" or "-N" work with start_year and end_year
    if (preg_match('!^(\+|\-)\s*(\d+)$!', $end_year, $match)) {
        if ($match[1] == '+') {
            $end_year = strftime('%Y') + $match[2];
        } else {
            $end_year = strftime('%Y') - $match[2];
        }
    }
    if (preg_match('!^(\+|\-)\s*(\d+)$!', $start_year, $match)) {
        if ($match[1] == '+') {
            $start_year = strftime('%Y') + $match[2];
        } else {
            $start_year = strftime('%Y') - $match[2];
        }
    }
    if (strlen($time[0]) > 0) {
        if ($start_year > $time[0] && !isset($params['start_year'])) {
            // force start year to include given date if not explicitly set
            $start_year = $time[0];
        }
        if ($end_year < $time[0] && !isset($params['end_year'])) {
            // force end year to include given date if not explicitly set
            $end_year = $time[0];
        }
    }

    $field_order = strtoupper($field_order);

    $html_result = $month_result = $day_result = $year_result = "";

    $field_separator_count = -1;
    if ($display_months) {
        $field_separator_count++;
        $month_names = array();
        $month_values = array();
        if (isset($month_empty)) {
            $month_names[''] = $month_empty;
            $month_values[''] = '';
        }
        for ($i = 1; $i <= 12; $i++) {
            $month_names[$i] = strftime($month_format, mktime(0, 0, 0, $i, 1, 2000));
            $month_values[$i] = strftime($month_value_format, mktime(0, 0, 0, $i, 1, 2000));
        }

        $month_result .= '<select name=';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
            $month_result .= '"' . $field_array . '[' . $prefix . $month_name . ']"';
//            $month_result .= '"' . $field_array . '[' . $prefix . 'Month]"';
        } else {
            $month_result .= '"' . $prefix . $month_name . '"';
//            $month_result .= '"' . $prefix . 'Month"';
        }
        $month_result .= ' id="' . $month_id . '"';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
        if (null !== $month_size) {
            $month_result .= ' size="' . $month_size . '"';
        }
        if (null !== $month_extra) {
            $month_result .= ' ' . $month_extra;
        }
        if (null !== $all_extra) {
            $month_result .= ' ' . $all_extra;
        }
        $month_result .= $extra_attrs . '>' . "\n";

        $month_result .= smarty_function_html_options(array('output' => $month_names,
                'values' => $month_values,
                'selected' => (int)$time[1] ? strftime($month_value_format, mktime(0, 0, 0, (int)$time[1], 1, 2000)) : '',
                'print_result' => false),
            $smarty, $template);
        $month_result .= '</select>';
    }

    if ($display_days) {
        $field_separator_count++;
        $days = array();
        if (isset($day_empty)) {
            $days[''] = $day_empty;
            $day_values[''] = '';
        }
        for ($i = 1; $i <= 31; $i++) {
            $days[] = sprintf($day_format, $i);
            $day_values[] = sprintf($day_value_format, $i);
        }

        $day_result .= '<select name=';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
            $day_result .= '"' . $field_array . '[' . $prefix . $day_name . ']"';
//            $day_result .= '"' . $field_array . '[' . $prefix . 'Day]"';
        } else {
            $day_result .= '"' . $prefix . $day_name . '"';
//            $day_result .= '"' . $prefix . 'Day"';
        }
        $day_result .= ' id="' . $day_id . '"';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
        if (null !== $day_size) {
            $day_result .= ' size="' . $day_size . '"';
        }
        if (null !== $all_extra) {
            $day_result .= ' ' . $all_extra;
        }
        if (null !== $day_extra) {
            $day_result .= ' ' . $day_extra;
        }
        $day_result .= $extra_attrs . '>' . "\n";
        $day_result .= smarty_function_html_options(array('output' => $days,
                'values' => $day_values,
                'selected' => $time[2],
                'print_result' => false),
            $smarty, $template);
        $day_result .= '</select>';
    }

    if ($display_years) {
        $field_separator_count++;
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
        if (null !== $field_array) {
            $year_name = $field_array . '[' . $prefix . $year_name . ']';
//            $year_name = $field_array . '[' . $prefix . 'Year]';
        } else {
            $year_name = $prefix . $year_name;
//            $year_name = $prefix . 'Year';
        }
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------
        if ($year_as_text) {
            $year_result .= '<input type="text" name="' . $year_name . '" value="' . $time[0] . '" size="4" maxlength="4"';
            if (null !== $all_extra) {
                $year_result .= ' ' . $all_extra;
            }
            if (null !== $year_extra) {
                $year_result .= ' ' . $year_extra;
            }
            $year_result .= ' />';
        } else {
            $years = range((int)$start_year, (int)$end_year);
            if ($reverse_years) {
                rsort($years, SORT_NUMERIC);
            } else {
                sort($years, SORT_NUMERIC);
            }
            //$yearvals = $years;
            foreach($years as $val) $tmp_years[] = sprintf($year_format, $val); // 追加
            $years = $tmp_years; // 追加
            $yearvals = $years; // 追加
            if (isset($year_empty)) {
                array_unshift($years, $year_empty);
                array_unshift($yearvals, '');
            }
            $year_result .= '<select name="' . $year_name . '"';
            if (null !== $year_size) {
                $year_result .= ' size="' . $year_size . '"';
            }
            if (null !== $all_extra) {
                $year_result .= ' ' . $all_extra;
            }
            if (null !== $year_extra) {
                $year_result .= ' ' . $year_extra;
            }
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
            $year_result .= ' id="' . $year_id . '"';
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //----------------------------------------------------------------------------------------------------

            $year_result .= $extra_attrs . '>' . "\n";
            $year_result .= smarty_function_html_options(array('output' => $years,
                    'values' => $yearvals,
                    'selected' => $time[0],
                    'print_result' => false),
                $smarty, $template);
            $year_result .= '</select>';
        }
    }
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    if(!isBlankOrNull($fieldOrderFormat)) {
        $html_result .= str_replace('|Y|', $year_result, $fieldOrderFormat);
        $html_result = str_replace('|m|', $month_result, $html_result);
        $html_result = str_replace('|d|', $day_result, $html_result);
        return $html_result;
    }

    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    // Loop thru the field_order field
    for ($i = 0; $i <= 2; $i++) {
        $c = substr($field_order, $i, 1);
        switch ($c) {
            case 'D':
                $html_result .= $day_result;
                $html_result .= $day_separator;
                break;

            case 'M':
                $html_result .= $month_result;
                $html_result .= $month_separator;
                break;

            case 'Y':
                $html_result .= $year_result;
                $html_result .= $year_separator;
                break;
        }
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code START
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------

        // Add the field seperator
    //    if ($i < $field_separator_count) {
    //        $html_result .= $field_separator;
    //    }
    //----------------------------------------------------------------------------------------------------
    //■Mikoshiva_Code END
    //@authr M.Ikuma <ikumamasayuki@gmail.com>
    //----------------------------------------------------------------------------------------------------
    }

    return $html_result;
}
?>
