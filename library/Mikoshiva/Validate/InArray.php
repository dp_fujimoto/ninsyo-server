<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_InArray
 */
require_once 'Zend/Validate/InArray.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: InArray.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_InArray extends Zend_Validate_InArray
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_IN_ARRAY => "%COLUMN_NAME%は候補の中にありません"
    );
}
