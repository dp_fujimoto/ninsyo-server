<?php
require_once('DPConfig.php');

class MemberShip{
	// 認証DBファイルのパスを指定
	var $authdir;
	var $unique;

	function MemberShip(){
		$this->authdir = AUTH_FILE_DIR;
		$this->unique  = date('Ymd_His_u').'_'.getmypid();
	}

	// 認証情報の追加。
	function add($email, $product_cd, $date){
		if ($product_cd == "MLT" || $product_cd == "MSC") {
			// 2011.01.9 の場合、AUTHZ_START_DAYが11なので先月のデータに投入する
			$yyyymm = substr($date,0,6);
			if($date < $yyyymm.AUTHZ_START_DAY){
				$yyyy = substr($date,0,4);
				$mm   = substr($date,4,2);
				$dd   = substr($date,6,2);
				$epoch = mktime(0,0,0,$mm,1,$yyyy);
				$epoch -= 60 * 60 * 24;
				$yyyymm = strftime("%Y%m",$epoch);
			}
			$file = $this->authdir."/${product_cd}${yyyymm}_$this->unique.csv";
			$fp = fopen($file, "a");
			if(flock($fp,LOCK_EX)){
				fwrite($fp, "$email,$product_cd\n");
				flock($fp,LOCK_UN);
			}
			fclose($fp);
		}
	}
	// CSVファイルのサーバ転送（転送先でマージ）
	function dp_server_merge(){
		$files = array();
		// 転送対象認証ファイルを取得
		$dir = opendir( $this->authdir );
		while( $file = readdir( $dir ) ){
			if(preg_match("/$this->unique\.csv/",$file)){
				array_push($files,$file);
			}
		}
		closedir( $dir );

		$is_sucess = true;

		// 転送処理（プロダクトコード分）
		foreach($files as $f){
			$file = $this->authdir."/".$f;
			preg_match("/([a-zA-Z\d]+)(\d{6})_/",$f,$m);//substr(basename($file),0,strpos(basename($file),'_')),
			$pcode  = $m[1];
			$yyyymm = $m[2];
			$data = array(
				'key'        => MERGE_KEY,
				'product_cd' => $pcode,
				'yyyymm'     => $yyyymm,
				'data'       => file_get_contents( $file )
			);
			$options = array(
				'http'    => array(
				'method'  => 'POST',
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'content' => http_build_query($data,'','&')
			));

			if ($pcode == "MLT" || $pcode == "MSC") {
				// プロダクトコードから転送先を取得
				// 対象ファイルのファイル名からプロダクトコードを取得して、該当する転送先設定がないとエラー
				$constMergeUrl = 'MERGE_URL_'.strtoupper($pcode);
				$mergeUrl = constant($constMergeUrl);
				if(!$mergeUrl) throw new Exception("Not defined $constMergeUrl. Check DPConfig.php MERGE_URL_*** or $file");

				$res = file_get_contents($mergeUrl, false, stream_context_create($options));
				if(!$res){
					throw new Exception("Can't transfer file. file: $file");
				}
				print $res;
			} else {
				$is_sucess = false;
			}
		}
		if ($is_sucess == true) {
			return true;
		} else {
			throw new Exception("Find illegal file. not MLT/MSC file.");
		}
	}
}
?>