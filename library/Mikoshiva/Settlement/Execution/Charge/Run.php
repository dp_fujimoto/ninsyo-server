<?php

// ユーティリティ
include_once 'Mikoshiva/Utility/Gmo.php';

// アウトプット
include_once 'Mikoshiva/Settlement/Popo/Output/Charge/Run.php';

// 取引登録
include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Entry.php';
include_once 'Mikoshiva/Settlement/Execution/Gmo/Entry/Capture.php';

// 決済実行
include_once 'Mikoshiva/Settlement/Popo/Input/Gmo/Exec/Card.php';
include_once 'Mikoshiva/Settlement/Execution/Gmo/Exec/Card.php';


/**
 * カード登録・更新処理を行わずに再請求処理を行ないます
 *
 * 【フロー】
 * １．取引登録
 * ２．決済実行
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/07/07
 * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Charge_Run {


    /**
     * 入力 POPO
     * @var Mikoshiva_Settlement_Popo_Input_Charge_Run 入力 POPO
     */
    protected $_inputPopo;


    /**
     * 出力 POPO
     * @var Mikoshiva_Settlement_Popo_Output_Charge_Run 入力 POPO
     */
    protected $_outputPopo;


    /**
     * 処理区分（即時売上：※固定）
     * @var string 処理区分
     */
    protected $_JobCd = 'CAPTURE';


    /**
     * 支払方法（再請求：※固定）
     * @var string 支払方法
     */
    protected $_entryStatus = 'ENTRY_STATUS_CHARGE'; // 再請求


    /**
     * 支払方法（一括払い：※固定）
     * @var string 支払方法
     */
    protected $_method = '1'; // GMO Status
    protected $_methodStatus = 'PAYMENT_TYPE_SINGLE'; // MIKOSHIVA Status


    /**
     * コンストラクタ
     *
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/07/08
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct() {

        // アウトプットクラスのインスタンスを生成
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Charge_Run();
    }



    /**
     * 実行メソッド
     *
     * @return boolean 成功：true|失敗：false
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/07/07
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function execute(Mikoshiva_Settlement_Popo_Input_Charge_Run $inputPopo) {


        //--------------------------------------------------------------------
        //
        // ▼念のため必要なもののみ入力チェックを行う
        //   ここで入力していないものは先の処理でチェックしているはず・・・
        //
        //--------------------------------------------------------------------
        // 金額
        if (! numberCheck($inputPopo->getAmount()) || $inputPopo->getAmount() <= 0) {
            // 数値じゃなく、０円以下で有ればエラーとする
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】再請求の金額は０円以上でセットして下さい。";
            logDebug($message);
            include_once 'Mikoshiva/Settlement/Execution/Charge/Exception.php';
            throw new Mikoshiva_Settlement_Execution_Charge_Exception($message);
        }


        //--------------------------------------------------------------------
        //
        // ▼必要な値を初期化する
        //
        //--------------------------------------------------------------------
        // ショップ情報を取得する
        $shopInfo = array();
        $shopInfo = Mikoshiva_Utility_Gmo::getShopInfo($inputPopo->getShopID());

        // オーダー ID から会員 ID を取得
        $memberId = '';
        $memberId = Mikoshiva_Utility_Gmo::getFromOrderIdToMemberId($inputPopo->getOrderID());


        //--------------------------------------------------------------------
        //
        // ▼取引登録
        //
        //--------------------------------------------------------------------
        // 入力 POPO を登録
        $entryInputPopo = null;
        $entryInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Entry();
        $entryInputPopo->setShopID($inputPopo->getShopID());   // ショップ ID
        $entryInputPopo->setShopPass($shopInfo['SHOP_PASS']);  // ショップパスワード
        $entryInputPopo->setOrderID($inputPopo->getOrderID()); // オーダー ID
        $entryInputPopo->setJobCd($this->_JobCd);              // 処理区分
        $entryInputPopo->setAmount($inputPopo->getAmount());   // 処理区分
//dumper($entryInputPopo);//exit;
        // 実行
        $entryExecClass = null;
        $entryExecClass = new Mikoshiva_Settlement_Execution_Gmo_Entry_Capture($entryInputPopo);
        $ret            = null;
        $ret            = $entryExecClass->execute();

        // 処理結果を取得
        /* @var $entryOutputPopo Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register */
        $entryOutputPopo = null;
        $entryOutputPopo = $entryExecClass->getOutput();

        // 処理に失敗していたらリターン
        if (! $ret) {
            // アウトプットの POPO に詰めて失敗（false）を返す
            Mikoshiva_Utility_Copy::copyToPopo($this->_outputPopo, $entryOutputPopo);
            return false;
        }
//dumper($entryOutputPopo);//exit;

        //--------------------------------------------------------------------
        //
        // ▼決済実行
        //
        //--------------------------------------------------------------------
        // inputPopo から会員登録 POPO にコピー
        $execInputPopo = null;
        $execInputPopo = new Mikoshiva_Settlement_Popo_Input_Gmo_Exec_Card();
        $execInputPopo->setAccessID($entryOutputPopo->getAccessID());     // 取引 ID
        $execInputPopo->setAccessPass($entryOutputPopo->getAccessPass()); // 取引パスワード
        $execInputPopo->setOrderID($inputPopo->getOrderID());             // オーダー ID
        $execInputPopo->setMethod($this->_method);                        // 支払方法
////////////$execInputPopo->setPayTimes();                                // 支払回数
        $execInputPopo->setCardNo($inputPopo->getCardNo());               // クレジットカード番号
        $execInputPopo->setExpire($inputPopo->getExpire());               // クレジットカード有効期間
        //$execInputPopo->setMemberID($memberId);                           // 会員 ID
        //$execInputPopo->setCardSeq('0');                                  // カード登録連番
//dumper($execInputPopo);//exit;
        // 決済実行クラス
        $execExec = null;
        $execExec = new Mikoshiva_Settlement_Execution_Gmo_Exec_Card($execInputPopo);
        $ret      = null;
        $ret      = $execExec->execute();

        // 結果を取得
        /* @var $execOutputPopo Mikoshiva_Settlement_Popo_Output_Gmo_Exec_Card */
        $execOutputPopo = null;
        $execOutputPopo = $execExec->getOutput();
//dumper($execOutputPopo);exit;
        // 処理に失敗していたらリターン
        if (! $ret)  {
            // アウトプットの POPO に詰めて失敗（false）を返す
            Mikoshiva_Utility_Copy::copyToPopo($this->_outputPopo, $execOutputPopo);
            return false;
        }


        //--------------------------------------------------------------------
        //
        // ■GMO 取引履歴テーブル登録処理
        //
        //--------------------------------------------------------------------
        // 登録データを作成
        $data = array();
        $data = array(
            //'mgh_gmo_transaction_history_id' => null,                                        // ＩＤ
            'orderId'                 => $inputPopo->getOrderID(),                             // オーダーＩＤ
            'memberId'                => $memberId,                                            // 会員ＩＤ
            'shopId'                  => $inputPopo->getShopID(),                              // ショップＩＤ
            'accessId'                => $entryOutputPopo->getAccessID(),                      // アクセスＩＤ
            'accessPass'              => $entryOutputPopo->getAccessPass(),                    // アクセスパス
            'cardNo'                  => 'XXXXXXXXXXXX' . substr($inputPopo->getCardNo(), -4), // カード番号
            'method'                  => $this->_methodStatus,                                 // 支払方法
            'payTimes'                => '1',                                                  // 支払回数
            'entryStatus'             => $this->_entryStatus,                                  // 取引方法
            'amount'                  => $inputPopo->getAmount(),                              // 金額
            'mgh_tran_date datetime'  => $execOutputPopo->getTranDate(),                       // 決済日時
            //    mgh_delete_flag char(1) not null default 0,                                  // 削除フラグ
            //    mgh_deletion_datetime datetime,                                              // 削除日時
            'registrationDatetime'    => Mikoshiva_Date::mikoshivaNow(),                       // 登録日時
            'updateDatetime'          => Mikoshiva_Date::mikoshivaNow(),                       // 更新日時（datetime）
            //'updateTimestamp'         => null,                                               // 更新日時（timestamp）
        );

        // 更新
        $simple = null;
        $simple = new Mikoshiva_Db_Simple();

        $ret = null;
        $ret = $simple->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_MstGmoTransactionHistory', $data);



        // ---------------------- ▲ログ終了▲ ---------------------- //
        logInfo("【" . __CLASS__ . "#" . __FUNCTION__ . "】終了");
        return true;
    }



    /**
     * 処理結果を返します
     *
     * @return Mikoshiva_Settlement_Popo_Output_Charge_Run
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/05/31
     * @version SVN:$Id: Run.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getOutput() {
        return $this->_outputPopo;
    }
}



















































