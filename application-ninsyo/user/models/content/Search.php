<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/Operation.php';
require_once 'Mikoshiva/Db/Pager.php';

/**
 * モデルクラス
 * @author kawakami
 *
 */
class User_Models_Content_Search extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        // 出力
        $output = array();

        if ($actionForm->getMode() === 'search') {

            $email = $actionForm->getEmail();
            $password = $actionForm->getPassword();
            $serverName = $actionForm->getServerName();
            $contentId = $actionForm->getContentId();
            $deleteFlag = $actionForm->getDeleteFlag();

            // 共通クラス
            $class = new Api_Models_Operation();

            //-----------------------------------------------
            //
            // コンテンツ検索
            //
            //-----------------------------------------------
            $params = array();
            $exeFlag = '0';
            $order = " ORDER BY mus_user_id, mbr_brand_name, mpr_product_name, mct_content_name";

            // SQL作成
            $sql = "";
            $sql .= "SELECT";
            $sql .= "    mst_user_content.*";
            $sql .= "   ,mus_user_id";
            $sql .= "   ,mbr_brand_name";
            $sql .= "   ,mpr_product_name";
            $sql .= "   ,mct_content_id";
            $sql .= "   ,mct_content_name";
            $sql .= "   ,mct_confirm_name";
            $sql .= "   ,mct_application_type";
            $sql .= "   ,muf_start_date";
            $sql .= "   ,muf_end_date";
            $sql .= " FROM mst_user";
            $sql .= " INNER JOIN mst_user_content ON mus_user_id = muc_user_id";
            $sql .= " INNER JOIN mst_content ON muc_content_id = mct_content_id";
            $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
            $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
            $sql .= " LEFT JOIN mst_user_content_force ON muc_user_content_id = muf_user_content_id";
            $sql .= " WHERE 1 = 1 ";
            $sql .= " AND mus_delete_flag = '0'";
            $sql .= " AND mct_delete_flag = '0'";
            $sql .= " AND mbr_delete_flag = '0'";
            $sql .= " AND mpr_delete_flag = '0'";

            // IDハッシュ
            if (!isBlankOrNull($email)) {
                $sql .= " AND mus_id_hash = ?";
                $params[] = $class->createUserHash($email);
                $exeFlag = '1';
                $order = " ORDER BY mct_application_type , mbr_brand_name, mpr_product_name, mct_content_id";
            }

            // パスワードハッシュ
            if (!isBlankOrNull($password)) {
                $sql .= " AND mus_password_hash = ?";
                $params[] = $class->createUserHash($password);
            }

            // サーバー名
            if (!isBlankOrNull($serverName)) {
                $sql .= " AND mus_server_name = ?";
                $params[] = $serverName;
            }

            // コンテンツID
            if (!isBlankOrNull($contentId)) {
                $sql .= " AND mct_content_id = ?";
                $params[] = $contentId;
                $exeFlag = '1';
            }

            // 用途
            if (!isBlankOrNull( $actionForm->getApplicationType())) {
                $sql .= " AND mct_application_type = ?";
                $params[] = $actionForm->getApplicationType();
            }

            // 権限
            if (!isBlankOrNull($actionForm->getAuthFlag())) {
                $sql .= " AND muc_auth_flag = ?";
                $params[] = $actionForm->getAuthFlag();
            }

            // 削除
            if (!isBlankOrNull($actionForm->getDeleteFlag())) {
                $sql .= " AND muc_delete_flag = ?";
                $params[] = $actionForm->getDeleteFlag();
            }

            $sql .= $order;

            if ($exeFlag === '1') {
                // SQL実行＋全件取得
                $res = array();
                $res = $this->_db->query($sql,$params);

                $pager = new Mikoshiva_Db_Pager();
                $output  = $pager->fetchAllLimit($actionForm, 100, $sql, $params);

                $ids = array();
                foreach ($output as $k => $v) {
                    $output[$k]['blank'] = '';
                    $ids[$v['muc_user_content_id']] = $v['muc_user_content_id'];
                }

                if (count($ids) > 0) {
                    // ブランク期間取得

                    // SQL作成
                    $sql = "";
                    $sql .= "SELECT";
                    $sql .= "    *";
                    $sql .= " FROM mst_user_content_blank";
                    $sql .= " WHERE 1 = 1 ";
                    $sql .= " AND mub_delete_flag = '0'";
                    $sql .= " AND mub_user_content_id IN (" . implode(',',$ids) . ")";

                    $res = array();
                    $res = $this->_db->query($sql);

                    $list = array();
                    while($row = $res->fetch(Zend_Db::FETCH_ASSOC)){
                        if (!isset($list[$row['mub_user_content_id']])) {
                            $list[$row['mub_user_content_id']] = array();
                        }
                        $list[$row['mub_user_content_id']][] = $row['mub_start_date'] . '～' . $row['mub_end_date'];
                    }
                    foreach ($output as $k => $v) {
                        if (isset($list[$v['muc_user_content_id']])) {
                            $output[$k]['blank'] = implode('<br>',$list[$v['muc_user_content_id']]);
                        }
                    }
                }
            }
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $output;

    }
}
