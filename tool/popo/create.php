<?php


/**
 * DB のテーブル情報を取得して popo クラスを自動で作成します
 *
 * @var unknown_type
 */


include_once dirname(__FILE__) . '/ColumnJpMapping.php';
$template = file_get_contents(dirname(__FILE__) . '/Template.txt');
$template = str_replace("\r\n", "\n", $template);




//-----------------------------------
// ■クラス名
//------------------------------------
$columnMappingTableName = array();
$columnMappingPopoName  = array();
$viewList = array();
$i = 0;
foreach (Zend_Db_Table_Abstract::getDefaultAdapter()->listTables() as $tableName) {

    $viewList[$i]['tableName'] = $tableName;




//Zend_Debug::dump(Zend_Db_Table_Abstract::getDefaultAdapter()->describeTable($tableName));exit;
        $_template = $template;

        $colsData = array();
        $colsData = Zend_Db_Table_Abstract::getDefaultAdapter()->describeTable($tableName);

        $className = '';
        $className = 'Popo_' . ucwords( str_replace('_', ' ', $tableName) );
        $className = str_replace(' ', '', $className);
        $_template  = str_replace('%CLASS_NAME%', $className, $_template);
        $_template  = str_replace('%TABLE_NAME%', $tableName, $_template);



        //-----------------------------------
        // ■columnPrefix
        //-----------------------------------
        $currentKey = '';
        $currentKey = key($colsData);

        $prefix = '';
        $prefix = preg_replace('#^(.*?)_.*$#', '${1}', $currentKey);
        $_template  = str_replace('%COLUMN_PREFIX%', $prefix, $_template);



        //-----------------------------------
        // ■columnMapping
        //-----------------------------------
        $columnMappingTableName[$prefix] = $tableName;
        //-----------------------------------
        // ■columnMappingPopoName
        //-----------------------------------
        $columnMappingPopoName[$prefix] = $className;




        //-----------------------------------
        // ■プロパティ名
        //------------------------------------
        $match = array();
        preg_match('#%PRO_START%(.*?)%PRO_END%#s', $_template, $match);
        $pro = $match[1];
        $proTemplate = '';
        foreach ($colsData as $col => $data) {

            $propertyName = $col;
            $propertyName = preg_replace('#^.*?_#', '', $propertyName, 1);
            $propertyName = preg_replace('#_([a-z]+?)#e', "ucfirst('$1')", $propertyName);
            $proTemplate .= $pro . '\'' . $propertyName . '\' => null,' . "\n";
        }

        $_template = preg_replace('#%PRO_START%.*?%PRO_END%#', $proTemplate, $_template);


        //-----------------------------------
        // ■getter setter
        //------------------------------------
        $match = array();
        preg_match('#%METHOD_START%(.*?)%METHOD_END%#s', $template, $match);//Zend_Debug::dump($match[1]);exit;
        $methodTemplate = $match[1];
        $_methodTemplate = '';
        foreach ($colsData as $col => $data) {

            $methodName       = '';
            $methodName       = preg_replace('#^.*?_#', '', $col, 1);
            $methodName       = ucwords( str_replace('_', ' ', $methodName) );
            $methodName       = str_replace(' ',            '', $methodName);

            $propertyName = '';
            $propertyName = preg_replace('#^.*?_#', '', $col, 1);
            $propertyName = preg_replace('#_([a-z]+?)#e', "ucfirst('$1')", $propertyName);

            $_methodTemplate .= str_replace('%METHOD_NAME%', $methodName, $methodTemplate);
            $_methodTemplate  = str_replace('%COL_NAME_IMI%', (isset($aryMap[$col]) ? $aryMap[$col] : $col), $_methodTemplate);
            $_methodTemplate  = str_replace('%COL_NAME%', $propertyName, $_methodTemplate);
            $_methodTemplate  = str_replace('%PROPERTY_NAME%', $propertyName, $_methodTemplate);
            $_methodTemplate  = str_replace('%DATA_TYPE%', $data['DATA_TYPE'], $_methodTemplate);
        }

        $_template = preg_replace('#%METHOD_START%.*?%METHOD_END%#s', $_methodTemplate, $_template);



        //-----------------------------------
        // ■file create
        //------------------------------------
        //Zend_Debug::dump($template);
        $fileNmae = '';
        $fileNmae = ucwords( str_replace('_', ' ', $tableName) );
        $fileNmae = str_replace(' ', '', $fileNmae);
        if (! file_exists(MODELS_DIR . '/Popo/' . $fileNmae . '.php')) {
            file_put_contents(MODELS_DIR . '/Popo/' . $fileNmae . '.php', '');
        }
        $__p = file_get_contents(MODELS_DIR . '/Popo/' . $fileNmae . '.php');
        $__p = str_replace("\r\n", "\n", $__p);
        if ($__p !== $_template) {
            $viewList[$i]['create'] = '○';
            file_put_contents(MODELS_DIR . '/Popo/' . $fileNmae . '.php', $_template);
        } else {
            $viewList[$i]['create'] = '×';
            // echo MODELS_DIR . '/Popo/' . $fileNmae . '.php<br>';
        }
        ++$i;
}

        //-----------------------------------
        // ■file create
        //------------------------------------
        $a  = '';
        $a .= '<?php';
        $a .= "\n\n";
        $a .= '$COLUMN_MAPPING_TABLE_NAME_LIST = ' . var_export($columnMappingTableName, true) . ';';
        $a .= "\n\n";
        $a .= '$COLUMN_MAPPING_POPO_NAME_LIST = ' .  var_export($columnMappingPopoName, true) . ';';
        $a .= "\n\n";
        file_put_contents(CONFIGS_DIR . '/table-info.php', $a);

        $tableList = array();
        foreach ($columnMappingTableName as $k => $v) {
            $tableList[] = $v;
        }

        file_put_contents(CONFIGS_DIR . '/table-name-list.php', implode("\n",$tableList));
















