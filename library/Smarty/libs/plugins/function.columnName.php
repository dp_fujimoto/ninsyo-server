<?php
require_once 'Mikoshiva/View/Helper/Exception.php';
require_once 'Zend/Registry.php';
require_once 'Mikoshiva/Logger.php';

/**
 * カラムリストからマッピングされた値を取得します。
 *
 * @param array    $params
 *                      string  name フォームのname, id
 * @param Smarty   $smarty
 * @param Template $template
 * @return string
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/07
 * @version SVN:$Id: function.columnName.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_columnName($params, &$smarty) {
    Mikoshiva_Logger::info(LOG_START);
    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default                 = array();
    $default['column']         = '';

    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;

    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['column']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】必須引数 column に値がありません。');
    }

    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    $column = $data['column'];

    $COLUMN_MAP_LIST = Zend_Registry::get('COLUMN_MAP_LIST');

    if (!isset($COLUMN_MAP_LIST[$column])) {
        Mikoshiva_Logger::info(LOG_END);
        return '項目リストに【 ' . $column . ' 】 は存在しません。 ';
    }

    Mikoshiva_Logger::info(LOG_END);
    return $COLUMN_MAP_LIST[$column];
}

?>