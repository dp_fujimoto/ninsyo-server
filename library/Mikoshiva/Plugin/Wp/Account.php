<?php
require_once 'Mikoshiva/Plugin/Wp/Abstruct.php';

/**
 * ワードプレスアカウント関連のAPIを呼び出します。
 *
 * @author k.shimowaki
 * @since version - 2010/03/19
 */
class Mikoshiva_Plugin_Wp_Account extends Mikoshiva_Plugin_Wp_Abstruct {

	public function createAccount(Popo_MstCustomer $customer, $wpmId) {

		$c = new Popo_MstCustomer();
		$c = $customer;

		$password = Mikoshiva_Utility_String::createRandomString(8);

		// パラメータセット
		$this->client->setParameterPost('username', $c->getEmail());								// メールアドレス
		$this->client->setParameterPost('firstname', $c->getLastName());							// 姓と名を反対に登録する でないと漢字の名前のとき逆になる
		$this->client->setParameterPost('lastname', $c->getFirstName());							// 姓と名を反対に登録する でないと漢字の名前のとき逆になる
		$this->client->setParameterPost('email', $c->getEmail());									// メールアドレス
		$this->client->setParameterPost('password1', $password);									// TODO パスワード１
		$this->client->setParameterPost('password2', $password);									// TODO パスワード２
		$this->client->setParameterPost('wpm_id', $wpmId);											// wpmId
		$this->client->setParameterPost('WishListMemberAction', 'WPMRegister');						// 固定値
		$this->client->setParameterPost('wpmtheadminkhas78ajd',
											'kjhad7kjghadjjhgas72jga878uayjahsd72uy3yg*&^&%@J$');	// 固定値



		try{

			logDebug($this->client->getParameterRequest());
			logDebug($this->client->getUri());

			// 実行
			$response = $this->client->request('POST');
		} catch (Exception $e) {

			// エラー処理
			$this->error($e);
			return null;
		}
		logDebug($this->client->getLastRequest());
		logDebug($this->client->getLastResponse());

		return $response->getBody();

	}

}