<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Mapping/Form/Abstract.php';
require_once 'Mikoshiva/Validate/Auto/Checker.php';

/**
 * アクションフォームの抽象クラス
 *
 * @package Mikoshiva_Controller
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/02/16
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 *
 */
abstract class Mikoshiva_Controller_Mapping_Form_File_Abstract
    extends Mikoshiva_Controller_Mapping_Form_Abstract {

    /* @val $_file Zend_File_Transfer_Adapter_Http */
    public $_file = null;

    public $_filePath = '';

    /**
     * コンストラクタ
     *
     * @param array $confProperties
     * @param array $param
     */
    public function __construct(array $confProperties, array $param) {
        // スーパークラスのコンストラクタ呼び出し
        parent::__construct($confProperties, $param);

        $file = null;
        $file = new Zend_File_Transfer_Adapter_Http();

        $filePath = '';
        $filePath = UPLOADS_DIR . '/' . date('YmdHis') . '_' . Mikoshiva_Utility_String::createRandomString(8) . '.csv';

        $file->addFilter('Rename', $filePath); // ファイルの保存先、ファイル名を指定
        //$file->addValidator('Extension', false, array('csv') ); // ファイル形式チェック

        $message = array();

        // ファイルを保存
        if ( $file->receive() === false ) {
            // ファイルの保存に失敗したらエラーメッセージを取得
            // エラーメッセージを取得
            $message[] = $file->getMessages();
            $file = null;

            Mikoshiva_Logger::debug('ファイル保存失敗');

        } else {
            $this->_file = $file;
            $this->_filePath = $filePath;

            Mikoshiva_Logger::debug($this->_filePath, 'ファイル保存成功');
        }

    }

    /**
     * アップロードされたファイルを利用可能な状態に準備します。
     * @return array $masseage:
     */
    private function preUploadFile() {

        //=====================================
        // ファイル形式をチェック
        //=====================================
        $file = null;
        $file = new Zend_File_Transfer_Adapter_Http();

        $filePath = '';
        $filePath = UPLOADS_DIR . '/' . date('YmdHis') . '_' . Mikoshiva_Utility_String::createRandomString(8) . '.csv';

        $file->addFilter('Rename', $filePath); // ファイルの保存先、ファイル名を指定
        //$file->addValidator('Extension', false, array('csv') ); // ファイル形式チェック

        $message = array();

        // ファイルを保存
        if ( $file->receive() === false ) {
            // ファイルの保存に失敗したらエラーメッセージを取得
            // エラーメッセージを取得
            $message[] = $file->getMessages();

            $file = null;
        } else {

            $this->_file = $file;
            $this->_filePath = $filePath;
        }

        return $message;
    }

    /**
     * アップロードされたファイルを削除します。
     */
    public function termUploadFile() {
        if (! isBlankOrNull($this->_filePath)) {
            unlink($this->_filePath);
        }
    }

    /**
     * アップロードされたファイルのパスを返します。
     * @see library/Mikoshiva/Controller/Mapping/Form/File/Mikoshiva_Controller_Mapping_Form_File_Abstract#getFilePath()
     */
    public function getFilePath() {
        return $this->_filePath;
    }

}