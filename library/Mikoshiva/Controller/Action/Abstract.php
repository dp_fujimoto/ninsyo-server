<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Exception.php';
require_once 'Mikoshiva/Controller/Mapping/ConfigLoader.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';
require_once 'Mikoshiva/Controller/Mapping/Form/File/Abstract.php';

require_once 'Mikoshiva/Filter/Input/Request.php';
include_once 'Zend/Controller/Action.php';

require_once 'Mikoshiva/Configuration/Exception.php';

require_once 'Mikoshiva/Session/Token.php';
require_once 'Mikoshiva/Session/Token/Exception.php';

require_once 'Mikoshiva/Utility/Device.php';

/**
 * Mikoshiva_Controller_Action_Abstract
 *
 * @package Mikoshiva_Controller
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/03
 * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Controller_Action_Abstract extends Zend_Controller_Action {

    /**
     * Zend_Db_Table_Abstract
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    /**
     * 初期化を行います
     *
     * 必要と思われるコンポーネントを Zend_Registry にセットしています
     * また、module レベルの models パス include_path パスにセットします
     *
     * init → preDispatch → reset
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/03
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     * (non-PHPdoc)
     * @see library/Zend/Controller/Zend_Controller_Action#init()
     */
    public function init() {
        //-------------------------------------
        // ■必要コンポーネントをセット
        //-------------------------------------
        Zend_Session::start();

        Zend_Registry::set('request',          $this->_request);  // リクエスト
        Zend_Registry::set('response',         $this->_response); // レスポンス
        Zend_Registry::set('view',             $this->view);      // view
        Zend_Registry::set('actionController', $this);            // アクションコントローラ


        //-------------------------------------
        // ■DB アダプタクラスをセット
        //-------------------------------------
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();


        //-------------------------------------
        // ■module レベルの models のパスを include_path にセット
        //-------------------------------------
        // module の models パスを解決
        $moduleName = null;
        $moduleName = $this->getRequest()->getModuleName();
        $moduleModelsPath = null;
        $moduleModelsPath = APPLICATION_PATH . '/' . $moduleName . '/models';

        $moduleValidatesPath = APPLICATION_PATH . '/' . $moduleName . '/validates';

        // include_path を追加
        // $includePathList = array();
        // $includePathList = ini_get('include_path');
        // ini_set('include_path', $includePathList . $moduleModelsPath . PATH_SEPARATOR . $moduleValidatesPath . PATH_SEPARATOR);


        //-------------------------------------
        // ■tabId tokenId を view にセット
        //-------------------------------------
        $this->view->tabId   = $this->getRequest()->getParam('tabId');
        $this->view->tokenId = $this->getRequest()->getParam('tokenId');


        //-------------------------------------
        // ■レイアウトとの非表示処理
        //-------------------------------------
         // デフォルトページ以外レイアウトは非表示にする
        if( $this->_request->getModuleName() === 'login' || $this->_request->getModuleName() === 'error') {
            $this->_helper->layout->disableLayout(); // layout
        }


        //-------------------------------------
        // ■デバッグ用
        //-------------------------------------
        //Zend_Debug::dump($this->getRequest()->getParams());
        //print_r(Zend_Debug::dump($this->getRequest()->getParams(), null, false));
        if (! $this->getRequest()->has('error_handler')) {
            $this->view->assign('MIKOSHIVA___REQUEST', Zend_Debug::dump($this->getRequest()->getParams(), null, false)); // リクエスト
        }

        //print_r($this->view->MIKOSHIVA___REQUEST);
    }

    /**
     * アクションが叩かれる前に実行されるメソッド
     *
     * init → preDispatch → reset
     *
     * (non-PHPdoc)
     * @see library/Zend/Controller/Zend_Controller_Action#preDispatch()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/16
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function preDispatch() {
        $this->filter();

        // アクションマッピングを処理します。
        $this->mapping();
    }


    public function filter() {
        //--------------------------------------
        // ■フィルター
        //--------------------------------------
        $newRequest = array();
        $newRequest = Mikoshiva_Filter_Input_Request::exec($this->getRequest()->getParams());

        $this->getRequest()->setParams($newRequest);
    }



    /**
     * Action実行数をカウントする
     *
     * @param $simpleDb       Mikoshiva_Db_Simple_Abstract
     * @param $moduleName     string
     * @param $controllerName string
     * @param $actionName     string
     */
    protected function _actionAnalyzer(Mikoshiva_Db_Simple_Abstract $simpleDb, $moduleName, $controllerName, $actionName) {


        //------------------------------------------------------------------
        // ■データを取得
        //------------------------------------------------------------------
        $taaPopoWhere = null;
        $taaPopoWhere = new Popo_TrxActionAnalysis();
        $taaPopoWhere->setModuleName($moduleName);
        $taaPopoWhere->setControllerName($controllerName);
        $taaPopoWhere->setActionName($actionName);

        /* @var $taaPopoSelect Popo_TrxActionAnalysis */
        $taaPopoSelect = null;
        $taaPopoSelect = $simpleDb->simpleOneSelect('Mikoshiva_Db_Ar_TrxActionAnalysis', $taaPopoWhere);


        //------------------------------------------------------------------
        // ■取得に失敗していたら検索条件で上書きする
        //------------------------------------------------------------------
        if (isBlankOrNull($taaPopoSelect->getActionAnalysisId()) === true) {
            $taaPopoSelect = $taaPopoWhere;
            $taaPopoSelect->setExecutionCount(0);
        }


        //------------------------------------------------------------------
        // ■カウントアップ
        //------------------------------------------------------------------
        $countUp = $taaPopoSelect->getExecutionCount() + 1;
        $taaPopoSelect->setExecutionCount($countUp);


        //------------------------------------------------------------------
        // ■登録・更新
        //------------------------------------------------------------------
        $resultTaaPopo = null;
        $resultTaaPopo = $simpleDb->simpleOneCreateOrUpdateById('Mikoshiva_Db_Ar_TrxActionAnalysis', $taaPopoSelect);



        return $resultTaaPopo;
    }


    /**
     * アクションマッピング
     *
     *
     * 1. new ActionFrom
     * 2. ActionFrom#setPost
     * 3. ActionFrom#reset
     * 4. ActionFrom#validate
     * 5. model
     *
     *
     *
     *
     */
    public function mapping() {


        // ログスタート
        logInfo(LOG_START);


        //-------------------------------------------------------
        //
        // ■リクエストから設定ファイル読み込み用情報を取得
        //
        //-------------------------------------------------------
        $req            = $this->getRequest();                      // リクエストクラス
        $moduleName     = $this->getRequest()->getModuleName();     // モジュール名
        $controllerName = $this->getRequest()->getControllerName(); // コントローラー名
        $actionName     = $this->getRequest()->getActionName();     // アクション名


        // コンフィグディレクトリへのパスを生成
        $configsPath    = APPLICATION_PATH . '/' . $moduleName . '/configs/' . $controllerName;

        // ▼ログ
        Mikoshiva_Logger::debug($configsPath);



        //------------------------------------------------------------------
        //
        // ■Action分析クラスにデータの登録を行う
        //
        //------------------------------------------------------------------
        require_once 'Mikoshiva/Db/Simple.php';
        require_once 'Popo/TrxActionAnalysis.php';
        $retTaaPopo = null;
        $retTaaPopo = $this->_actionAnalyzer(new Mikoshiva_Db_Simple(), $moduleName, $controllerName, $actionName);


        //----------------------------------------------------
        // コンフィグディレクトリの存在チェック
        //----------------------------------------------------
        if (! file_exists($configsPath)) {

            logInfo(LOG_IF_START,'case: コンフィグファイル存在なし');
            Mikoshiva_Logger::debug("return");

            // Zendフレームワークによるアクションの実行を阻止します。
            $this->getRequest()->setDispatched(true);
            // require_once 'Mikoshiva/Configuration/Exception.php';
            // throw new Mikoshiva_Configuration_Exception();
            logInfo(LOG_IF_END,'case: コンフィグファイル存在なし');
            return;
        }


        //----------------------------------------------------
        // コンフィグファイルを取得
        //----------------------------------------------------
        Mikoshiva_Logger::debug('設定ファイルロード開始');

        // アクションフォームとアクションマッピングの設定ファイルを取得します。
        // ファイルの存在チェックもこのクラスの中で行っています
        $configLoader = new Mikoshiva_Controller_Mapping_ConfigLoader($configsPath);

        $configs             = $configLoader->get();
        $actionFormConfig    = $configs['action-form'];
        $validateConfig      = $configs['validation'];

        // アクションマッピングにアクション名が存在しなければエラー
        if (! isset($configs['action-mapping'][$actionName])) {
            $logMessage = '【Mikoshiva_Controller_Action_Abstract#mapping】action-mapping に ”' . $actionName . '”セクションが存在しません。';
            Mikoshiva_Logger::debug($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
        }
        $actionMappingConfig = $configs['action-mapping'][$actionName];

        // ▼ログ
        Mikoshiva_Logger::debug($actionFormConfig,    'アクションフォーム設定内容');
        Mikoshiva_Logger::debug($actionMappingConfig, 'アクションマッピング設定内容');
        Mikoshiva_Logger::debug($validateConfig,      'バリデート設定内容');

        //------------------------------------------------------------------
        // ■view の forward セクションの存在をチェックします
        //   Model が動いて view が動かない状態を防ぎます（Model のみが動くことを防ぐ）
        //   → Model のみが動くと setcookie などの処理が実行されてしまうのを防ぎます
        //------------------------------------------------------------------
        $this->_getForwardSection($configs['action-mapping'][$actionName], $this->_request, false);





        //------------------------------------------------------------------------
        //
        // ■アクションフォーム生成
        //
        //------------------------------------------------------------------------
        Mikoshiva_Logger::debug('アクションフォーム生成開始');


        // action-form セクションの存在チェック
        if (! isset($actionMappingConfig['action-form'])) {
            $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】action-mapping に ”{$actionName}: action-form”セクションが存在しません。";
            Mikoshiva_Logger::debug($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
        }


        // ◆アクションフォーム名を取得
        $actionFormName = $actionMappingConfig['action-form'];


        // useClass セクションの存在チェック
        if (! isset($actionFormConfig[$actionFormName]['useClass'])) {
            $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】action-form.yml に”{$actionName}: action-form: useClass”セクションが存在しません。";
            Mikoshiva_Logger::debug($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
        }


        // ◆アクションフォームのクラス名を取得
        $formClassName = $actionFormConfig[$actionFormName]['useClass'];

        // モデル名をZend_Registryに格納します
        Zend_Registry::set('actionFormName', $formClassName);

        //-------------------------------------------------------------
        // クラス名からクラスファイルまでのパスを生成
        //
        // 最後の Test3Form 以外は小文字に変換する
        // クラス名:   Test1_Models_Test2_Test3Form
        //          ↓
        // ファイル名: test1/models/test2/Test3Form
        //-------------------------------------------------------------
        $requirePath = '';                                       // ファイル名
        $requirePathList         = explode('_', $formClassName); // クラス名を _ で区切って配列化
        $requirePathListMaxCount = count($requirePathList);      // 配列の要素数

        // ファイルパスを生成
        foreach ($requirePathList as $k => $v) {

            // 最後の要素でなければ小文字にする
            if ($k !== ($requirePathListMaxCount -1)) {

                // クラス名の単語が連なる場合ハイフンで区切る
                // Doki_Models_Test2Test2_Test3Test3Form → Doki Test2-Test2 Test2-Test2
                $v = preg_replace('#([0-9a-zA-Z])([A-Z])#', '$1-$2', $v);

                // 大文字を小文字にする
                $requirePath .= strtolower($v) . '/';
            } else {
                // 最後の要素であれば拡張子を付ける
                $requirePath .= $v . '.php';
            }
        }
        /*****************
        $sp = split('_', $formClassName);
        $requirePath = '';
        Mikoshiva_Logger::debug($sp, 'sp');
        for ($i = 2;$i<count($sp);$i++) {
            logInfo(LOG_LOOP_START,'$i='.$i);

            if (count($sp) -1 !== $i) {
                logInfo(LOG_IF_START,'case: count($sp) -1 !== $i');

                // クラス名の単語が連なる場合ハイフンで区切る
                        // Doki_Models_Test2Test2_Test3Test3Form → Doki Test2-Test2 Test2-Test2
                $sp[$i] = preg_replace('#([0-9a-zA-Z])([A-Z])#', '$1-$2', $sp[$i]);

                $requirePath .= strtolower($sp[$i]).'/';

                logInfo(LOG_IF_END,'case: count($sp) -1 !== $i');
            } else if (count($sp) -1 === $i) {
                logInfo(LOG_IF_START,'case: count($sp) -1 === $i');

                $requirePath .= $sp[$i].'.php';

                logInfo(LOG_IF_END,'case: count($sp) -1 === $i');
            }
        }
        logInfo(LOG_LOOP_END,'$i='.$i);
        ********************/

// dumper($formClassName);
// dumper($requirePath);
// exit;
        //-----------------------------------------------
        // モデルクラスの存在チェック
        //-----------------------------------------------
        Mikoshiva_Logger::debug($requirePath,'フォームクラスインクルードパス');
        if (! fileChecker($requirePath)) {
            $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward セクションに ”{$requirePath}” が存在しません。'";
            Mikoshiva_Logger::debug($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
        }


        require_once $requirePath;

        Mikoshiva_Logger::debug($formClassName,'class名');


        //-------------------------------------------------------------
        // ▼ActionForm にセットするためにリクエスト値を取得
        //   ※ forwrad でリクエストされた場合で、ActionForm が存在していたら ActionForm を利用します
        //-------------------------------------------------------------
        $param = array();
        if (! isBlankOrNull($req->getParam('_actionForm'))) {

            // ActionForm をセット
            $param = $req->getParam('_actionForm')->toArrayProperty();

            // 削除
            $req->setParam('_actionForm', null);

        } else {

            // リクエスト値をセット
            $param = $req->getParams();
        }

        if ($param == null) {
            logInfo(LOG_IF_START,'case: $param == null');
            $param = array();
            logInfo(LOG_IF_END,'case: $param == null');
        }

        //-------------------------------------------------------------
        // 設定により動的にフォームクラスを生成
        //-------------------------------------------------------------
        $actionForm = null;
        if (isset($actionFormConfig[$actionFormName]['properties'])) {

            // ▼ログ
            Mikoshiva_Logger::debug($actionFormConfig[$actionFormName]['properties']);
            Mikoshiva_Logger::debug($param);

            // 生成
            $actionForm = new $formClassName($actionFormConfig[$actionFormName]['properties'], $param);
        } else {

            // ▼ログ
            Mikoshiva_Logger::debug($param);

            // 生成
            $actionForm = new $formClassName(array(), $param);
        }



//dumper('//------------------------------------------------------------------');
//dumper($actionForm->toArrayProperty());
//dumper('//------------------------------------------------------------------');
        //-------------------------------------------------------------
        // ActionForm を view にセット
        //-------------------------------------------------------------
        // ◆アクションフォームを view にセットする
        // 2011-04-21 $this->view->assign('form', $actionForm);




        Mikoshiva_Logger::debug('フォーム作成完了');
        Mikoshiva_Logger::debug('class名',get_class($actionForm));

        // 初期化
        $actionForm->reset($actionFormConfig[$actionFormName], $req);


        //------------------------------------------------------------
        //
        // ■バリデート用コンフィグファイル加工
        //
        //------------------------------------------------------------
        // マッピングファイルに validate: check セクションが有れば
        if (isset($actionMappingConfig['validate']) && isset($actionMappingConfig['validate']['check'])) {

            // ▼ログ
            Mikoshiva_Logger::debug($actionMappingConfig['validate']['check'],'validateキー');


            // validation.yml にマッピングファイルで指定されたセクションが存在していなければエラー
            // 値が NULL でも独自バリデータを実行させるため通します
            if (! array_key_exists($actionMappingConfig['validate']['check'], $validateConfig)) {
                $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】validation.yml に {$actionMappingConfig['validate']['check']} セクションは存在しません。'";
                Mikoshiva_Logger::debug($logMessage); // ▼ログ
                throw new Mikoshiva_Configuration_Exception($logMessage);
            }


            //---------------------------------------------------
            // 検証定義実行
            //---------------------------------------------------
            // 検証定義を取得
            // NULL で有れば空配列にする（バリデータメソッドの引数が配列で来ることを期待しているため）
            $validateConfigDetail = $validateConfig[$actionMappingConfig['validate']['check']] !== null
                                  ? $validateConfig[$actionMappingConfig['validate']['check']]
                                  : array();
            Mikoshiva_Logger::debug($validateConfigDetail);

            // 実行
            $actionMessage = $actionForm->validate($validateConfigDetail, $req);
            Mikoshiva_Logger::debug($actionMessage,'エラー内容');


            //---------------------------------------------------
            // 検証に引っかかった場合の処理
            //---------------------------------------------------
            if (is_array($actionMessage) && count($actionMessage) !== 0) {


                // ▼ログ：バリデートエラー時の処理
                //------------------------------------------------------------------
                Mikoshiva_Logger::debug($actionMessage,'バリデートエラー処理開始');


                // エラーメッセージを view へセット
                //------------------------------------------------------------------
                $this->view->assign('message', $actionMessage);


                // error セクションがなければエラー
                //------------------------------------------------------------------
                if (! isset($actionMappingConfig['validate']['error'])) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】validation.yml に check セクションが存在しません。'";
                    Mikoshiva_Logger::debug($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }


                // 検証に引っかかった場合の遷移先を取得
                //------------------------------------------------------------------
                $errorKey = $actionMappingConfig['validate']['error'];


                // アクションマッピングの forward セクションの存在チェック
                //------------------------------------------------------------------
                if (isset($actionMappingConfig['forward']) === false && isset($actionMappingConfig['forward-mobile']) === false && isset($actionMappingConfig['forward-tablet']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward セクションが存在しません。";
                    Mikoshiva_Logger::debug($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                /*
                if (! isset($actionMappingConfig['forward'])) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward セクションが存在しません。'";
                    Mikoshiva_Logger::debug($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                */

                // forward セクションを取得します
                //------------------------------------------------------------------
                $forwardSection = array();
                $forwardSection = $this->_getForwardSection($actionMappingConfig, $this->_request);


                // アクションマッピングの forward セクションに検証失敗時の遷移先が定義されていなければエラー
                //------------------------------------------------------------------
                if (isset($forwardSection[$errorKey]) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward セクションに ”{$errorKey}” が存在しません。";
                    Mikoshiva_Logger::debug($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                /*
                if (! isset($actionMappingConfig['forward'][$errorKey])) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward セクションに ”{$errorKey}” が存在しません。'";
                    Mikoshiva_Logger::debug($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                */


                // forward 先を取得
                //------------------------------------------------------------------
                $forward = $forwardSection[$errorKey];



                //------------------------------------------------------------------
                // ▼遷移
                //------------------------------------------------------------------
                switch (true) {

                    // ◆[リダイレクト] ----------------------------------------------------
                    case (preg_match('/-r\s(.+)/',$forward, $redirect) === 1):
                        Mikoshiva_Logger::debug($redirect[1], 'リダイレクト');
                        $this->_redirect($redirect[1]);
                        break;


                    // ◆[フォーワード] ----------------------------------------------------
                    case (preg_match('#\/#', $forward) === 1):
                        Mikoshiva_Logger::debug($forward, 'フォーワード');
                        $sp = explode('/',$forward);
                        if (count($sp) < 4) {
                            throw new Mikoshiva_Configuration_Exception( '【Mikoshiva_Controller_Action_Abstract#mapping】フォワード処理に失敗しました。');
                        }
                        Mikoshiva_Logger::debug($sp, 'forward $sp');
                        $this->getRequest()->setParam('_actionForm', $actionForm); // ActionFormをセット
                        $this->_forward($sp[3], $sp[2], $sp[1]);
                        return;
                        break;


                    // ◆[noRender] ----------------------------------------------------
                    case ($forward === 'none'):
                        Mikoshiva_Logger::debug($forward, 'noRender');
                        $this->_helper->viewRenderer->setNoRender();
                        break;


                    // ◆[レンダリング] ----------------------------------------------------
                    default:
                        // ◆アクションフォームを view にセットする
                        $this->view->assign('form', $actionForm);
                        $this->_helper->viewRenderer->setNoRender();
                        Mikoshiva_Logger::debug($forward, 'レンダリング');
                        echo $this->view->render($controllerName . '/' . $forward);
                }

/**********
                // ▼ view forward redirect 処理
                Mikoshiva_Logger::debug('render:' . $controllerName . '/' . $forward);
*************/
                // ◆アクションフォームを view にセットする
                //////////// 2011-04-21 $this->view->assign('form', $actionForm);
/**********
                // レンダー
                $this->_helper->viewRenderer->setNoRender();
                echo $this->view->render($controllerName . '/' . $forward);
*************/

                // Zendフレームワークによるアクションの実行を阻止します。
                $this->getRequest()->setDispatched(true);
                Mikoshiva_Logger::debug('validateError');
                return;
            }
        }


        //------------------------------------------------------------------
        //
        // ■コンテキストの準備をします
        //
        //------------------------------------------------------------------
        $context = null;
        if ($this->getRequest()->has('Mikoshiva_Controller_Mapping_ActionContext')) {
            $context = $this->getRequest()->getParam('Mikoshiva_Controller_Mapping_ActionContext');
        } else {
            // ログインスタッフの情報取得
            $loginInfo = null;
            switch (true) {

                // MIKOSHIVA システムの場合 Staff 情報を取得
                case _APPLICATION_TYPE_ID_ === 'mikoshiva':
                    $loginInfo = new Zend_Session_Namespace('staff');
                    break;

                // MYPAGE システムの場合 Customer 情報を取得
                case _APPLICATION_TYPE_ID_ === 'mypage':
                    $loginInfo = new Zend_Session_Namespace('customer');
                    break;

                // NINSYO システムの場合 Staff 情報を取得
                case _APPLICATION_TYPE_ID_ === 'ninsyo':
                    $loginInfo = new Zend_Session_Namespace('staff');
                    break;

                default:
                    throw new Mikoshiva_Configuration_Exception('_SYSTEM_MODE_ID_ の値が不正です');
            }

            // コンテキストにセット
            $context = new Mikoshiva_Controller_Mapping_ActionContext($loginInfo);

            // 最低限のモノだけコンテキストに詰める
            $context->setRequest($this->getRequest());   // request
            $context->setResponse($this->getResponse()); // response

            // ▼コンフィグ
            $_configList       = Zend_Registry::getInstance()->getArrayCopy();
            $_defineConfigList = array();
            foreach ($_configList as $configKey => $configValue)  {

                // 大文字から始まるものだけをセットします
                if(preg_match('#^[A-Z_]+$#', $configKey) === 1) {
                    $_defineConfigList[$configKey] = $configValue;
                }
            }
            $context->setConfig($_defineConfigList);



            // backup 20100302 kawakami
            // $context->setResult('request', $this->getRequest());   // request
            // $context->setResult('response', $this->getResponse()); // response
        }
//        Mikoshiva_Logger::debug(dumper($this->getRequest()->getParam('Mikoshiva_Controller_Mapping_ActionContext')),'Mikoshiva_Controller_Mapping_ActionContext');



        //-----------------------------------------------------------------------
        //
        // ■トークン処理
        //
        //------------------------------------------------------------------------
        // dumper($actionMappingConfig);exit;
        if (isset($actionMappingConfig['token']) && isset($actionMappingConfig['token']['type'])) {


            Mikoshiva_Logger::debug('トークン処理：理開始');
            switch ($actionMappingConfig['token']['type']) {


                // ◆トークン処理（save） ----------------------------------------
                case 'save':
                    Mikoshiva_Logger::debug('トークン処理（save）：開始');
                    Mikoshiva_Session_Token::createToken($this->view);
                    Mikoshiva_Logger::debug('トークン処理（save）：終了');
                    break;


                // ◆トークン処理（complete） ----------------------------------------
                case 'complete':
                    Mikoshiva_Logger::debug('トークン処理（complete）：開始');

                    // チェック
                    $ret = Mikoshiva_Session_Token::checkToken($this->getRequest());

                    // マッチしなければエラー
                    if (! $ret) {
                        Mikoshiva_Logger::debug('【Mikoshiva_Controller_Action_Abstract::mapping】不正なトークンＩＤです');
                        throw new Mikoshiva_Session_Token_Exception('【Mikoshiva_Controller_Action_Abstract::mapping】不正なトークンＩＤです');
                    }

                    // チェックが通ったら削除
                    Mikoshiva_Session_Token::deleteToken();

                    Mikoshiva_Logger::debug('トークン処理（complete）：終了');
                    break;


                // ◆トークン処理（check）----------------------------------------
                case 'check':
                    Mikoshiva_Logger::debug('トークン処理（check）：開始');

                    // チェック
                    $ret = Mikoshiva_Session_Token::checkToken($this->getRequest());

                    // マッチしなければエラー
                    if (! $ret) {
                        Mikoshiva_Logger::debug('【Mikoshiva_Controller_Action_Abstract::mapping】不正なトークンＩＤです');
                        throw new Mikoshiva_Session_Token_Exception('【Mikoshiva_Controller_Action_Abstract::mapping】不正なトークンＩＤです');
                    }

                    Mikoshiva_Logger::debug('トークン処理（check）：終了');
                    break;


                 // ◆何れのケースにもマッチしなければエラー）----------------------------------------
                default:
                    Mikoshiva_Logger::debug('【Mikoshiva_Controller_Action_Abstract::mapping】不正なトークンＩＤです');
                    throw new Mikoshiva_Session_Token_Exception('【Mikoshiva_Controller_Action_Abstract::mapping】token-type の値が不正です');
            }
            Mikoshiva_Logger::debug('トークン処理：理終了');
        }




        //-----------------------------------------------------------------------
        //
        // ■モデルチェイン
        //
        //------------------------------------------------------------------------
        Mikoshiva_Logger::debug('モデルチェイン開始');

        // モデルチェインを行います。
        $i = 0;
        $result = null;


        if (isset($actionMappingConfig['models-chain'])) {
            Mikoshiva_Logger::debug('モデル名', $actionMappingConfig['models-chain']);

            $modelsChain = $actionMappingConfig['models-chain'];
            $beginFlag = false;

            $modelCount = 0;
            // [foreach] -----------------------------------------------
            foreach ($modelsChain as $i => $values) {

                Mikoshiva_Logger::debug($i,'キー名');
                Mikoshiva_Logger::debug($values,'バリュー');
                // モデルクラス名を取得します。
                if (isset($values['model'])) {
                    $modelName = $values['model'];
                } else if (isset($values['begin'])) {
                    // トランザクション処理
                    if ($values['begin'] === true) {
                        if ($beginFlag === false) {
                            Mikoshiva_Logger::debug('トランザクションbegin');
                            $this->_db->beginTransaction();
                            $beginFlag = true;
                        } else {
                            throw new Mikoshiva_Configuration_Exception(
                                '【Mikoshiva_Controller_Action_Abstract】transaction すでにbeginされています。');
                        }
                    }
                    continue;
                } else if (isset($values['commit'])) {
                    if ($values['commit'] === true) {
                        if ($beginFlag === true) {
                            Mikoshiva_Logger::debug('トランザクションcommit');
                            $this->_db->commit();
                            $beginFlag = false;
                        } else {
                            throw new Mikoshiva_Configuration_Exception(
                                '【Mikoshiva_Controller_Action_Abstract】transaction beginされていない為commitできません。');
                        }
                    }
                    continue;
                } else {
                    throw new Mikoshiva_Exception(
                        '【Mikoshiva_Controller_Action_Abstract】models-chain [' . $i .'] に model は存在しません。');
                }
                // アサイン名を取得します。
                if (isset($values['name'])) {
                    $assignName = $values['name'];
                } else {
                    $assignName = $moduleName . '/' . $controllerName . '/' . $actionName . ':' . $modelCount;
                }
                $modelCount++;

                // パス名(クラス名)要検討
                Mikoshiva_Logger::debug($modelName,'モデルクラスのインスタンス生成');
                //-------------------------------------------------------------
                // クラス名からクラスファイルまでのパスを生成
                //
                // 最後の Test3Form 以外は小文字に変換する
                // クラス名:   Test1_Models_Test2_Test3Form
                //          ↓
                // ファイル名: test1/models/test2/Test3Form
                //-------------------------------------------------------------
                $requirePath = '';                                       // ファイル名
                $requirePathList         = explode('_', $modelName); // クラス名を _ で区切って配列化
                $requirePathListMaxCount = count($requirePathList);      // 配列の要素数

                // ファイルパスを生成
                foreach ($requirePathList as $k => $v) {

                    // 最後の要素でなければ小文字にする
                    if ($k !== ($requirePathListMaxCount -1)) {

                        // クラス名の単語が連なる場合ハイフンで区切る
                        // Doki_Models_Test2Test2_Test3Test3Form → Doki Test2-Test2 Test2-Test2
                        $v = preg_replace('#([0-9a-zA-Z])([A-Z])#', '$1-$2', $v);

                        // 大文字を小文字にする
                        $requirePath .= strtolower($v) . '/';
                    } else {
                        // 最後の要素であれば拡張子を付ける
                        $requirePath .= $v . '.php';
                    }
                }
                // modelクラスへのインクルードパス生成
                /***********************************************
                $sp = split('_',$modelName);
                $requirePath = '';
                Mikoshiva_Logger::debug($sp,'sp');
                for ($i = 2;$i<count($sp);$i++) {
                    if (count($sp) -1 !== $i) {

                        // クラス名の単語が連なる場合ハイフンで区切る
                        // Doki_Models_Test2Test2_Test3Test3Model → Doki Test2-Test2 Test2-Test2
                        $sp[$i] = preg_replace('#([0-9a-zA-Z])([A-Z])#', '$1-$2', $sp[$i]);

                        $requirePath .= strtolower($sp[$i]).'/';
                    } else if (count($sp) -1 === $i) {
                        $requirePath .= $sp[$i].'.php';
                    }
                }
                *****************************************************/
// dumper($modelName);
// dumper($requirePath);
// exit;
                // ◆モデルクラス読み込み
                Mikoshiva_Logger::debug($requirePath);


                //-----------------------------------------------
                // モデルクラスの存在チェック
                //-----------------------------------------------
                if (! fileChecker($requirePath)) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward セクションに ”{$requirePath}” が存在しません。'";
                    Mikoshiva_Logger::debug($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }

                // 読み込み
                require_once $requirePath;


                // モデル名をZend_Registryに格納します
                Zend_Registry::set('modelName', $modelName);
                // ◆モデルクラスを実行します。
                $modelClass = new $modelName();
                $rs = $modelClass->execute($actionForm, $context);

                if (isset($rs)) {
                    Mikoshiva_Logger::debug('リザルト結果のアサイン');
                    Mikoshiva_Logger::debug($assignName,'アサイン名');

                    // モデルのリザルト結果をビューにアサインします。
                    $this->view->assign($assignName, $rs);
                    // モデルのリザルト結果をコンテキストに登録します。
                    $context->setResult($assignName, $rs);
                }

                $result = $modelClass->getResult();

                if ($result !== null) {
                    break;
                }
            } // ----------------------------------------------- [/foreach]

            if ($result === null) {

                $result = 'success';

                if ($beginFlag === true) {

                    $this->_db->rollBack();

                    throw new Mikoshiva_Configuration_Exception(
                        '【Mikoshiva_Controller_Action_Abstract】transaction commitされていません。');
                }
            } else {
                if ($beginFlag === true) {
                    $this->_db->rollBack();
                }
            }

        // モデルチェインが設定されていない場合は強制的にsuccessを入れる
        } else {
            $result = 'success';
        }

        // Mikoshiva_Controller_Mapping_Form_File_Abstract のActionFormを利用している場合
        // アップロードされたファイルの削除を行います。
        if ($actionForm instanceof Mikoshiva_Controller_Mapping_Form_File_Abstract) {
            Mikoshiva_Logger::debug('Mikoshiva_Controller_Mapping_Form_File_Abstract', 'ActionForm型');
            $actionForm->termUploadFile();
        }

        // ◆アクションフォームを view にセットする
        //2011-04-21$this->view->assign('form', $actionForm);



        //-------------------------------------------------------
        //
        // ◆遷移処理
        //
        //-------------------------------------------------------
        Mikoshiva_Logger::debug('Forward条件判定開始');

        //-----------------------------------------------------------
        // 定義チェック
        //-----------------------------------------------------------
        // forward を確定
        $forwardSection = array();
        $forwardSection = $this->_getForwardSection($configs['action-mapping'][$actionName], $this->_request);
        if (isset($forwardSection[$result]) === false) {
            $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward:['{$result}']セクションが存在しません。'";
            Mikoshiva_Logger::debug($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
        }


        //-----------------------------------------------------------
        // 遷移
        //-----------------------------------------------------------
        // 使用する view を確定します
        $forward = $forwardSection[$result];
        Mikoshiva_Logger::debug('render:' . $controllerName . '/' . $forward);

        $redirect = array();
        switch (true) {

            // ◆[リダイレクト] ----------------------------------------------------
            case (preg_match('/-r\s(.+)/',$forward, $redirect) === 1):
                Mikoshiva_Logger::debug($redirect[1], 'リダイレクト');
                $this->_redirect($redirect[1]);
                break;


            // ◆[フォーワード] ----------------------------------------------------
            case (preg_match('#^/#',$forward) === 1):
                Mikoshiva_Logger::debug($forward, 'フォーワード');
                $sp = explode('/',$forward);
                if (count($sp) < 4) {
                    throw new Mikoshiva_Configuration_Exception('【Mikoshiva_Controller_Action_Abstract#mapping】フォワード処理に失敗しました。');
                }
                Mikoshiva_Logger::debug($sp, 'forward $sp');
                $this->getRequest()->setParam('_actionForm', $actionForm); // ActionFormをセット
                $this->_forward($sp[3], $sp[2], $sp[1], array('Mikoshiva_Controller_Mapping_ActionContext' => $context));
                return;
                break;


            // ◆[noRender] ----------------------------------------------------
            case ($forward === 'none'):
                Mikoshiva_Logger::debug($forward, 'noRender');
                $this->_helper->viewRenderer->setNoRender();
                break;


            // ◆[レンダリング] ----------------------------------------------------
            default:
                // ◆アクションフォームを view にセットする
                $this->view->assign('form', $actionForm);
                $this->_helper->viewRenderer->setNoRender();
                Mikoshiva_Logger::debug($forward, 'レンダリング');
                echo $this->view->render($controllerName . '/' . $forward);
        }


        Mikoshiva_Logger::debug('mapping end');

        // Zendフレームワークによるアクションの実行を阻止します。
        $this->getRequest()->setDispatched(true);

//        Mikoshiva_Logger::debug(get_defined_vars());
        logInfo(LOG_END);
    }


    /**
     * 遷移処理
     *
     * @param string $forward
     * @return unknown_type
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/01
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function transitional($forward) {

        switch (true) {

            // ◆[リダイレクト] ----------------------------------------------------
            case (preg_match('/-r\s(.+)/', $forward, $redirect) === 1):

                Mikoshiva_Logger::debug($redirect[1], 'リダイレクト');
                $this->_redirect($redirect[1]);
                break;


            // ◆[フォーワード] ----------------------------------------------------
            case (preg_match('#\/#',$forward) === 1):

                Mikoshiva_Logger::debug($forward, 'フォーワード');

                $sp = explode('/',$forward);
                if (count($sp) < 4) {
                    throw new Mikoshiva_Configuration_Exception(
                       '【Mikoshiva_Controller_Action_Abstract#transitional】Forwardできません。');
                }
                Mikoshiva_Logger::debug($sp, 'forward $sp');
                $this->_forward($sp[3], $sp[2], $sp[1], array('Mikoshiva_Controller_Mapping_ActionContext' => $context));
                return;
                break;


            // ◆[noRender] ----------------------------------------------------
            case ($forward === 'none'):

                Mikoshiva_Logger::debug($forward, 'noRender');
                $this->_helper->viewRenderer->setNoRender();
                return;
                break;


            // ◆[レンダリング] ----------------------------------------------------
            default:
                $this->_helper->viewRenderer->setNoRender();
                Mikoshiva_Logger::debug($forward, 'レンダリング');
                echo $this->view->render($controllerName . '/' . $forward);
        }
    }


    /**
     * forward セクションを確定し、返します
     *
     *
     * @param array $forwardSection
     * @return array
     */
    protected function _getForwardSection(array $actionMapping, Zend_Controller_Request_Http $request, $setCookieFlag = true) {


        // forward セクション
        $forwardSection = array();

        //------------------------------------------------------------------
        //
        // ■【端末情報】で forward を決定する
        //   ※クッキーを発行しません
        //
        //------------------------------------------------------------------

            // 携帯端末の場合
            //------------------------------------------------------------------
            $isMobile = Mikoshiva_Utility_Device::isMobileDevice();
            if ($isMobile === true && isset($actionMapping['forward-mobile']) === true) {
                $forwardSection = $actionMapping['forward-mobile'];
            }

            // タブレット端末の場合
            //------------------------------------------------------------------
            $isTablet = Mikoshiva_Utility_Device::isTabletDevice();
            if ($isTablet === true && isset($actionMapping['forward-tablet']) === true) {
                $forwardSection = $actionMapping['forward-tablet'];
            }

            // どれにも当てはまらなければ forward セクションをセット
            //------------------------------------------------------------------
            if (count($forwardSection) === 0) {
                // ▼forward が存在しなければエラー
                if (isset($actionMapping['forward']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward'];
            }
            logInfo('■【端末情報】で forward を決定する');
            logInfo($forwardSection);
            logInfo($_SERVER);


        //------------------------------------------------------------------
        //
        // ■【クッキー情報】が有れば forward を上書きする
        //   ※クッキーを発行します
        //   ※端末 ＜ クッキー ＜ リクエスト
        //
        //------------------------------------------------------------------

            // 表示方法が PC （view-mode）で有ればデバイスに限らずデフォルトの forward を使用する
            //------------------------------------------------------------------
            if ($request->getCookie('view-mode') === 'pc') {
                // ▼forward が存在しなければエラー
                if (isset($actionMapping['forward']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward']; // $forwardSection にすでに値が有っても上書きする
                // ▼セットクッキー
                if ($setCookieFlag === true) $this->_setCookieViewMode($actionMapping, 'pc');
            }

            // 表示方法が PC （view-mode）で有ればデバイスに限らずデフォルトの forward を使用する
            //------------------------------------------------------------------
            if ($request->getCookie('view-mode') === 'mobile') {
                // ▼forward-mobile が存在しなければエラー
                if (isset($actionMapping['forward-mobile']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward-mobile:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward-mobile']; // $forwardSection にすでに値が有っても上書きする
                // ▼セットクッキー
                if ($setCookieFlag === true) $this->_setCookieViewMode($actionMapping, 'mobile');
            }

            // 表示方法が PC （view-mode）で有ればデバイスに限らずデフォルトの forward を使用する
            //------------------------------------------------------------------
            if ($request->getCookie('view-mode') === 'tablet') {
                // ▼forward-tablet が存在しなければエラー
                if (isset($actionMapping['forward-tablet']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward-tablet:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward-tablet']; // $forwardSection にすでに値が有っても上書きする
                // ▼セットクッキー
                if ($setCookieFlag === true) $this->_setCookieViewMode($actionMapping, 'tablet');
            }
            logInfo('■【クッキー情報】が有れば forward を上書きする');
            logInfo($forwardSection);
            logInfo($_SERVER);


        //------------------------------------------------------------------
        //
        // ■【リクエスト情報】が有れば forward を上書きする
        //   ※クッキーを発行します
        //   ※端末 ＜ クッキー ＜ リクエスト
        //
        //------------------------------------------------------------------

            // 表示方法が PC （view-mode）で有ればデバイスに限らずデフォルトの forward を使用する
            //------------------------------------------------------------------
            if ($request->getParam('view-mode') === 'pc') {
                // ▼forward が存在しなければエラー
                if (isset($actionMapping['forward']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward']; // $forwardSection にすでに値が有っても上書きする
                // ▼セットクッキー
                if ($setCookieFlag === true) $this->_setCookieViewMode($actionMapping, 'pc');
            }

            // 表示方法が PC （view-mode）で有ればデバイスに限らずデフォルトの forward を使用する
            //------------------------------------------------------------------
            if ($request->getParam('view-mode') === 'mobile') {
                // ▼forward-mobile が存在しなければエラー
                if (isset($actionMapping['forward-mobile']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward-mobile:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward-mobile']; // $forwardSection にすでに値が有っても上書きする
                // ▼セットクッキー
                if ($setCookieFlag === true) $this->_setCookieViewMode($actionMapping, 'mobile');
            }

            // 表示方法が PC （view-mode）で有ればデバイスに限らずデフォルトの forward を使用する
            //------------------------------------------------------------------
            if ($request->getParam('view-mode') === 'tablet') {
                // ▼forward-tablet が存在しなければエラー
                if (isset($actionMapping['forward-tablet']) === false) {
                    $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】forward-tablet:セクションが存在しません。'";
                    logInfo($logMessage); // ▼ログ
                    throw new Mikoshiva_Configuration_Exception($logMessage);
                }
                // ▼セット
                $forwardSection = $actionMapping['forward-tablet']; // $forwardSection にすでに値が有っても上書きする
                // ▼セットクッキー
                if ($setCookieFlag === true) $this->_setCookieViewMode($actionMapping, 'tablet');
            }
            logInfo('■【リクエスト情報】が有れば forward を上書きする');
            logInfo($forwardSection);
            logInfo($_SERVER);

        return $forwardSection;
    }


    /**
     * view mode のクッキーを表示します
     *
     *
     * @param array $actionMapping
     * @param unknown_type $cookieValue
     */
    private function _setCookieViewMode(array $actionMapping, $cookieValue) {

        // cookie-view-mode の存在を確認
        if (isset($actionMapping['cookie-view-mode']) === false) {
            return;
        }

        // expire の存在を確認
        if (isset($actionMapping['cookie-view-mode']['expire']) === false) {
            $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】['cookie-view-mode']['expire']:が存在しません。'";
            logInfo($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
            return;
        }

        // pathe の存在を確認
        if (isset($actionMapping['cookie-view-mode']['path']) === false) {
            $logMessage = "【Mikoshiva_Controller_Action_Abstract#mapping】['cookie-view-mode']['path']:が存在しません。'";
            logInfo($logMessage); // ▼ログ
            throw new Mikoshiva_Configuration_Exception($logMessage);
            return;
        }

        // cookie-view-mode セクションを取得
        $cookieExpire   = $actionMapping['cookie-view-mode']['expire'];
        $cookiePath     = $actionMapping['cookie-view-mode']['path'];

        eval('$cookieExpire = ' . $cookieExpire . ';');

        setcookie('view-mode', $cookieValue, time() + $cookieExpire, $cookiePath);

    }


    /**
     * Zend_Controller_Action#_forward のラップクラス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/03
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     * @param $action
     * @param $controller
     * @param $module
     * @param $params
     */
    public function forward($action, $controller = null, $module = null, array $params = null) {
        $this->_forward($action, $controller, $module, $params);
    }


    /**
     * Zend_Controller_Action#_redirect のラップクラス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/03
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     * @param $url
     * @param $options
     */
    public function redirect($url, array $options = array()) {
        $this->_redirect($url, $options);
    }



    /**
     * コントローラーのアクションメソッドを一手に引き受けるコールメソッド
     * このメソッドによりコントローラーにアクションを記述する必要が無くなります。
     * @see library/Zend/Controller/Zend_Controller_Action#__call($methodName, $args)
     *
     * @author kawakami <kawakami@kaihatsu.com>
     * @since 2009/12/04
     * @version SVN:$Id: Abstract.php,v 1.3 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function __call($methodName, $args) {

    }
}





















