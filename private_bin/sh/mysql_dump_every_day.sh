#!/bin/bash
#
# mikoshiva bkup data create
# k.sekiya create 2010/12/01

#include library classpath
. /usr/local/bin/miko_include_classpath.sh

##################################################
# define variables
##################################################
LOGDIR=/bkup2/mysql_dump_every_day
LOGFILE=$LOGDIR/mysql_dump_every_day.log
BKUPDIR=/bkup2/mysql_dump_every_day
NOW=`date`
TODAY=`date +%Y-%m-%d`
TWO_DAYS_AGO=`date -d -2day +%Y-%m-%d`

##################################################
# start message
##################################################
echo [$NOW]:batch start
echo [$NOW]:batch start > $LOGFILE

##################################################
# check & make directory
##################################################
ls $BKUPDIR
if [ $? -ne 0 ]; then
  mkdir $BKUPDIR
fi

##################################################
# check & delete old dump file
##################################################
ls $BKUPDIR/mikoshiva_bkup_$TWO_DAYS_AGO.dump.gz
if [ $? -eq 0 ]; then
  rm -f $BKUPDIR/mikoshiva_bkup_$TWO_DAYS_AGO.dump.gz
fi
ls $BKUPDIR/mikoshiva_bkup_oplog_$TWO_DAYS_AGO.dump.gz
if [ $? -eq 0 ]; then
  rm -f $BKUPDIR/mikoshiva_bkup_oplog_$TWO_DAYS_AGO.dump.gz
fils $BKUPDIR/mikoshiva_bkup_$TWO_DAYS_AGO.dump
if [ $? -eq 0 ]; then
  rm -f $BKUPDIR/mikoshiva_bkup_$TWO_DAYS_AGO.dump
fi

##################################################
# dump database
##################################################
su - mysql -c "mysqldump -u root --single-transaction --master-data=2 --hex-blob --ignore-table=mikoshiva.mst_operation_log mikoshiva | gzip > $BKUPDIR/mikoshiva_bkup_$TODAY.dump.gz"

if [ $? -ne 0 ]; then
  echo [`date`]:[dump failed]
  echo [`date`]:[dump failed] >> $LOGFILE
  exit 1
fi

sleep 5

su - mysql -c "mysqldump -u root --single-transaction --master-data=2 --hex-blob mikoshiva mst_operation_log | gzip > $BKUPDIR/mikoshiva_bkup_oplog_$TODAY.dump.gz"su - mysql -c "mysqldump -u root --single-transaction --master-data=2 --hex-blob mikoshiva > $BKUPDIR/mikoshiva_bkup_$TODAY.dump"
if [ $? -ne 0 ]; then
  echo [`date`]:[dump failed]
  echo [`date`]:[dump failed] >> $LOGFILE
  exit 1
fi

##################################################
# end message
##################################################
echo [`date`]:batch succeed
echo [`date`]:batch succeed >> $LOGFILE