<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Gmo_Popo_Output_Entry_Register
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Gmo_Entry_Register extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'AccessID' => null,
        'AccessPass' => null,
        'ErrCode' => null,
        'ErrInfo' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * AccessID
	 *
     * @return  AccessID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessID() {
		return $this->_properties['AccessID'];
    }


    /**
     * AccessID
	 *
	 * @param  $AccessID AccessID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessID($AccessID) {
		$this->_properties['AccessID'] = $AccessID;
    }


    /**
     * AccessPass
	 *
     * @return  AccessPass
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAccessPass() {
		return $this->_properties['AccessPass'];
    }


    /**
     * AccessPass
	 *
	 * @param  $AccessPass AccessPass
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAccessPass($AccessPass) {
		$this->_properties['AccessPass'] = $AccessPass;
    }


    /**
     * ErrCode
	 *
     * @return  ErrCode
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrCode() {
		return $this->_properties['ErrCode'];
    }


    /**
     * ErrCode
	 *
	 * @param  $ErrCode ErrCode
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrCode($ErrCode) {
		$this->_properties['ErrCode'] = $ErrCode;
    }


    /**
     * ErrInfo
	 *
     * @return  ErrInfo
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getErrInfo() {
		return $this->_properties['ErrInfo'];
    }


    /**
     * ErrInfo
	 *
	 * @param  $ErrInfo ErrInfo
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Register.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setErrInfo($ErrInfo) {
		$this->_properties['ErrInfo'] = $ErrInfo;
    }


}

























