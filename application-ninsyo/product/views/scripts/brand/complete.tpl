<!-- // メインテーブル開始 -->
<table id="COM_painSingle">

    <tr>

        <!-- // ペイン開始 -->
        <td class="COM_nowrap">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ブランド登録完了</h2>

            <span>登録完了しました。</span>
            <br>
            <br>
            <br>
            {closeTabButton name='タブを閉じる' tabId=$tabId}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->