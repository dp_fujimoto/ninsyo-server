<?php
/** @package Mikoshiva_Controller */

/**
 * コンテキストオブジェクト
 * アクションマッピングにてモデルクラスへの引数として渡されるクラスです。
 *
 * @package Mikoshiva_Controller
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/02/16
 * @version SVN:$Id: ActionContext.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 *
 */
class Mikoshiva_Controller_Mapping_ActionContext {

    /**
     * スタッフ情報
     */
    private $_staff = array();

    /**
     * 顧客情報
     */
    private $_customer = array();

    /**
     * レスポンス
     */
    private $_response = null;

    /**
     * リクエスト
     */
    private $_request = null;

    /**
     * コンフィグ
     */
    private $_config = array();

    /**
     * リザルトメッセージ
     * メッセージがセットされた場合、モデルチェインは停止します。
     * トランザクション下にいる場合、ロールバック対象となります。
     * Action-Mapping.yml の forward 先となります。
     */
    private $_results = array();


    /**
     * コンストラクタ
     *
     * @param unknown_type $staff
     */
    public function __construct(Zend_Session_Namespace $loginInfo) {

        switch (true) {

            // MIKOSHIVA システムの場合 Staff 情報を取得
            case _APPLICATION_TYPE_ID_ === 'mikoshiva':
                $this->_staff = $loginInfo;
                break;

            // MYPAGE システムの場合 Customer 情報を取得
            case _APPLICATION_TYPE_ID_ === 'mypage':
                $this->_customer = $loginInfo;
                break;
                
            // NINSYO システムの場合 Staff 情報を取得
            case _APPLICATION_TYPE_ID_ === 'ninsyo':
                $this->_staff = $loginInfo;
                break;

            default:
                throw new Mikoshiva_Configuration_Exception('_SYSTEM_MODE_ID_ の値が不正です');
        }
    }

    /**
     * スタッフオブジェトを返します
     *
     * @return unknown_type $staff
     */
    public function getStaff() {
        return $this->_staff;
    }

    /**
     * 顧客オブジェトを返します
     *
     * @return unknown_type $staff
     */
    public function getCustomer() {
        return $this->_customer;
    }

    /**
     * レスポンスを返します。
     * @return Zend_Controller_Response_Abstract
     */
    public function getResponse() {
        return $this->_response;
    }

    /**
     * レスポンスをセットします。
     * @param Zend_Controller_Response_Abstract $response
     */
    public function setResponse(Zend_Controller_Response_Abstract $response) {
        $this->_response = $response;
    }

     /**
     * リクエストを返します。
     * @return Zend_Controller_Request_Abstract
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * リクエストをセットします。
     * @param Zend_Controller_Request_Abstract $response
     */
    public function setRequest(Zend_Controller_Request_Abstract $request) {
        $this->_request = $request;
    }


    /**
     *  すべてのリザルトネームを配列で返します
     *
     * @return array $keys
     */
    public function getResultNames() {
        $names = array();
        foreach ($this->_results as $k => $v) {
            $names[] = $k;
        }
        return $names;
    }

    /**
     * リザルトオブジェクトを返します。
     *
     * @param $name
     * @return object | null
     */
    public function getResult($name) {
        if (isset($this->_results[$name])) {
            return $this->_results[$name];
        } else {
            return null;
        }
    }

    /**
     * リザルト結果を保存します。
     *
     * @param string $name
     * @param object $result
     */
    public function setResult($name, $result) {
        $this->_results[$name] = $result;
    }



    /**
     * コンフィグを返します
     *
     * @return array
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/11
     * @version SVN:$Id: ActionContext.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getConfig() {
        return $this->_config;
    }



    /**
     * コンフィグをセットします
     *
     * @return void
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/06/11
     * @version SVN:$Id: ActionContext.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function setConfig($config) {
        $this->_config = $config;
    }

}



































