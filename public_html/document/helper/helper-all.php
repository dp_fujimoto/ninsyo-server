<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="/document/css/pice.css" type="text/css">
<title>mikoshiva smarty helper</title>


<style>
  body {
            background-color : #ffefcf;
  }

  table.menu {
            border           : 1px solid #C0C0C0;
            border-collapse  : collapse;
            background-color : #ffffff;
            margin-bottom    : 3em;
  }
  table.menu td {
            border           : 1px solid #C0C0C0;
            padding          : 0.5em;
            vertical-align   : top;
          }
  table.menu th {
            border           : 1px solid #C0C0C0;
            font-weight      : bold;
            background-color : #8484EE;
            color            : #ffffff;
  }

  div.contents {
            width:1000px;
  }

  table.container {
            border-collapse  : collapse;
            width            : 995px;
            border           : 1px solid #C0C0C0;
            margin-bottom    : 3em;
  }
  table.container th {
            font-weight      : bold;
            text-align       : left;
            padding-left     : 0.5em;
            background-color : #8484EE;
            color            : #ffffff;
            border           : 1px solid #C0C0C0;
            margin-bottom    : 3em;
  }
  table.container td {
            border : 1px solid #C0C0C0;
  }
  td.funcname {
            font-weight      : bold;
            background-color : #8484EE;
            color            : #ffffff;
  }

  table.details {
            background-color : #ffffff;
            border-collapse  : collapse;
            border           : 1px solid #C0C0C0;
  }
  table.details th {
            text-align       : center;
            background-color : #ccccff;
            color            : #000000;
            border           : 1px solid #C0C0C0;
  }
  table.details td {
            border           : 1px solid #C0C0C0;
            padding-top      : 0.3em;
            padding-left     : 0.5em;
            padding-right    : 0.5em;
            padding-bottom   : 0.2em;
  }
  td.default {
            text-align       : center;
  }
  td.indispensable {
            text-align       : center;
  }
  td.nodata {
            text-align       : center;
  }

</style>

</head>
<body>
<h1>ヘルパー一覧</h1>
<div style="width:800px;">
<table class="menu">
  <tr>
    <th class="menu th">Tab</th>
    <th class="menu th">Ajax</th>
    <th class="menu th">Lister</th>
    <th class="menu th">Session</th>
    <th class="menu th">Other</th>
  </tr>

  <tr>
    <td class="menu tab" >
          <a href="#closeTab_title" title="指定されたIDを持つタブを閉じるjavascriptを返します。">closeTab</a><br>
          <a href="#closeTabAll_title" title="現在開いている全てのタブを閉じるjavascriptを返します。">closeTabAll</a><br>
          <a href="#closeTabAllButton_title" title="現在開いている全てのタブを閉じるjavascriptをボタンタグに実装した形で返します。">closeTabAllButton</a><br>
          <a href="#closeTabAllLink_title" title="現在開いている全てのタブを閉じるjavascriptをリンク(aタグ)に実装した形で返します。">closeTabAllLink</a><br>
          <a href="#closeTabButton_title" title="指定されたIDを持つタブを閉じるjavascript をボタンタグに実装した形で返します。">closeTabButton</a><br>
          <a href="#closeTabLink_title" title="指定されたIDを持つタブを閉じるjavascript をリンク(aタグ)に実装した形で返します。">closeTabLink</a><br>
          <a href="#closeTabUnprotected_title" title="保護されたタブ以外の全てのタブを閉じるjavascriptを返します。">closeTabUnprotected</a><br>
          <a href="#closeTabUnprotectedButton_title" title="保護されたタブ以外の全てのタブを閉じるjavascriptをボタンタグに実装した形で返します。">closeTabUnprotectedButton</a><br>
          <a href="#closeTabUnprotectedLink_title" title="保護されたタブ以外の全てのタブを閉じるjavascriptをリンク(aタグ)に実装した形で返します。">closeTabUnprotectedLink</a><br>
          <a href="#currentTab_title" title="画面遷移を現在開いているタブに対して行うjavascriptを返します。">currentTab</a><br>
          <a href="#currentTabButton_title" title="画面遷移を現在開いているタブに対して行うjavascriptをボタンタグに実装した形で返します。">currentTabButton</a><br>
          <a href="#currentTabLink_title" title="画面遷移を現在開いているタブに対して行うjavascriptをリンク(aタグ)に実装した形で返します。">currentTabLink</a><br>
          <a href="#newTab_title" title="新しいタブを開くjavascriptを返します。">newTab</a><br>
          <a href="#newTabButton_title" title="新しいタブを開くjavascriptをボタンタグに実装した形で返します。">newTabButton</a><br>
          <a href="#newTabLink_title" title="新しいタブを開くjavascriptをリンク(aタグ)に実装した形で返します。">newTabLink</a><br>
        </td>

    <td class="menu ajax" >
          <a href="#f5Former_title" title="ダンプを行い、変数情報を文字列で返します。
オブジェクトは未対応">f5Former</a><br>
          <a href="#requestToId_title" title="Ajax で指定されたURLへリクエスト(idで指定されたフォーム部品の情報をpost)を行い、
結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うjavascriptを返します。">requestToId</a><br>
          <a href="#requestToIdButton_title" title="Ajax で指定されたアクションへリクエストを行い、
結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うボタンタグを返します。">requestToIdButton</a><br>
          <a href="#requestToIdLink_title" title="Ajax で指定されたアクションへリクエストを行い、
結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うjavascriptを
リンク(aタグ)に実装した形で返します。">requestToIdLink</a><br>
        </td>

    <td class="menu lisster" >
          <a href="#divisionLister_title" title="部署データを返します。">divisionLister</a><br>
          <a href="#gmoShopIdLister_title" title="ショップ ID のリストをselect or radio で表示します。">gmoShopIdLister</a><br>
          <a href="#hiddenLister_title" title="与えられた配列から hidden タグを書き出します。">hiddenLister</a><br>
          <a href="#html_select_dateLister_title" title="{html_select_date}拡張したものです。
基本的な仕様は同プラグインに従います。
＜相違点＞
■selectタグのname/idを個別に指定できます。
　指定しなければname/idは空文字になります。">html_select_dateLister</a><br>
          <a href="#html_select_timeLister_title" title="{html_select_time}を拡張したものです。
基本的な仕様は同プラグインに従います。
＜相違点＞
■selectタグのname/idを個別に指定できます。
　指定しなければname/idは空文字になります。
■セパレータを指定できます
　html_select_dateと同様に、field_separatorが指定できます。">html_select_timeLister</a><br>
          <a href="#prefectureLister_title" title="都道府県のリスト(select/radioタグ)を作成します。">prefectureLister</a><br>
          <a href="#statusLister_title" title="ステータス・タイプを元に、それが含む意味のリスト(select/radioフォーム)を作成します。">statusLister</a><br>
        </td>

    <td class="menu session">
          <a href="#authCheck_title" title="権限チェックを行います。">authCheck</a><br>
          <a href="#divisionCheck_title" title="セッションにセットされた部門IDを返します。
ただし、部門変更が行われていて、かつフラグとしてtrueが渡された場合、
変更後の部門IDを返します。">divisionCheck</a><br>
          <a href="#getSession_title" title="セッションの値を返します。">getSession</a><br>
        </td>

    <td class="menu other" >
          <a href="#columnName_title" title="カラムリストからマッピングされた値を取得します。">columnName</a><br>
          <a href="#dbWrite_title" title="データベースからプライマリーキーによって値を取得し、
指定されたカラムの値を返します。
複数のカラムを指定する場合は配列を用います。
　　この時、セパレータを指定することが可能です。">dbWrite</a><br>
          <a href="#formSelect_title" title="与えられた配列からセレクトボックスのフォームを作成して返します。">formSelect</a><br>
          <a href="#formSelectDb_title" title="データベベースから値を取得し、セレクトボックスを生成して返します。">formSelectDb</a><br>
          <a href="#gmoMapping_title" title="ショップIDを元にショップ名を取得して返します。">gmoMapping</a><br>
          <a href="#haifunSplit_title" title="文字列をハイフン（-）で分割し、指定した添字の値を返す。">haifunSplit</a><br>
          <a href="#imploader_title" title="配列要素を指定された文字列により連結して返します
要素が配列でない場合はそのまま返します">imploader</a><br>
          <a href="#lineToBr_title" title="改行コードを改行タグに変換します">lineToBr</a><br>
          <a href="#searchNotFoundMessage_title" title="検索結果が取得出来なかった時のメッセージを返します。">searchNotFoundMessage</a><br>
          <a href="#staffInfo_title" title="スタッフID及び、カラム名を元に、スタッフ情報を返します。
カラム名を複数指定した場合、それらを連結して返します。">staffInfo</a><br>
          <a href="#statusMapping_title" title="コードから、その意味を取得して返します。">statusMapping</a><br>
        </td>
  </tr>

</table></div>
<div class="contents" >
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="authCheck_title"></a>
        authCheck
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">権限チェックを行います。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="authCheck" class="name">authName
</td>
            <td id="authCheck" class="type">string
</td>
            <td id="authCheck" class="indispensable">○</td>
            <td id="authCheck" class="default">-</td>
            <td id="authCheck" class="summary">権限名
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTab_title"></a>
        closeTab
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">指定されたIDを持つタブを閉じるjavascriptを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTab" class="name">tabId
</td>
            <td id="closeTab" class="type">string
</td>
            <td id="closeTab" class="indispensable">○</td>
            <td id="closeTab" class="default">-</td>
            <td id="closeTab" class="summary">閉じるタブのタブID
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabAll_title"></a>
        closeTabAll
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">現在開いている全てのタブを閉じるjavascriptを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td class="nodata" > - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabAllButton_title"></a>
        closeTabAllButton
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">現在開いている全てのタブを閉じるjavascriptをボタンタグに実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTabAllButton" class="name">name
</td>
            <td id="closeTabAllButton" class="type">string
</td>
            <td id="closeTabAllButton" class="indispensable">○</td>
            <td id="closeTabAllButton" class="default">-</td>
            <td id="closeTabAllButton" class="summary">ボタンのvalue
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabAllLink_title"></a>
        closeTabAllLink
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">現在開いている全てのタブを閉じるjavascriptをリンク(aタグ)に実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTabAllLink" class="name">name
</td>
            <td id="closeTabAllLink" class="type">string
</td>
            <td id="closeTabAllLink" class="indispensable">○</td>
            <td id="closeTabAllLink" class="default">-</td>
            <td id="closeTabAllLink" class="summary">リンクテキスト
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabButton_title"></a>
        closeTabButton
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">指定されたIDを持つタブを閉じるjavascript をボタンタグに実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTabButton" class="name">name
</td>
            <td id="closeTabButton" class="type">string
</td>
            <td id="closeTabButton" class="indispensable">○</td>
            <td id="closeTabButton" class="default">-</td>
            <td id="closeTabButton" class="summary">ボタンのvalue
</td>
          </tr>
                    <tr>
            <td id="closeTabButton" class="name">tabId
</td>
            <td id="closeTabButton" class="type">string
</td>
            <td id="closeTabButton" class="indispensable">○</td>
            <td id="closeTabButton" class="default">-</td>
            <td id="closeTabButton" class="summary">閉じるタブのタブID
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabLink_title"></a>
        closeTabLink
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">指定されたIDを持つタブを閉じるjavascript をリンク(aタグ)に実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTabLink" class="name">name
</td>
            <td id="closeTabLink" class="type">string
</td>
            <td id="closeTabLink" class="indispensable">○</td>
            <td id="closeTabLink" class="default">-</td>
            <td id="closeTabLink" class="summary">リンクテキスト
</td>
          </tr>
                    <tr>
            <td id="closeTabLink" class="name">tabId
</td>
            <td id="closeTabLink" class="type">string
</td>
            <td id="closeTabLink" class="indispensable">○</td>
            <td id="closeTabLink" class="default">-</td>
            <td id="closeTabLink" class="summary">閉じるタブのタブID
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabUnprotected_title"></a>
        closeTabUnprotected
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">保護されたタブ以外の全てのタブを閉じるjavascriptを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td class="nodata" > - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabUnprotectedButton_title"></a>
        closeTabUnprotectedButton
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">保護されたタブ以外の全てのタブを閉じるjavascriptをボタンタグに実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTabUnprotectedButton" class="name">name
</td>
            <td id="closeTabUnprotectedButton" class="type">string
</td>
            <td id="closeTabUnprotectedButton" class="indispensable">○</td>
            <td id="closeTabUnprotectedButton" class="default">-</td>
            <td id="closeTabUnprotectedButton" class="summary">ボタンのvalue
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="closeTabUnprotectedLink_title"></a>
        closeTabUnprotectedLink
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">保護されたタブ以外の全てのタブを閉じるjavascriptをリンク(aタグ)に実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="closeTabUnprotectedLink" class="name">name
</td>
            <td id="closeTabUnprotectedLink" class="type">string
</td>
            <td id="closeTabUnprotectedLink" class="indispensable">○</td>
            <td id="closeTabUnprotectedLink" class="default">-</td>
            <td id="closeTabUnprotectedLink" class="summary">この文字列を&lt;a&gt;～&lt;/a&gt;で囲って出力
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="columnName_title"></a>
        columnName
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">カラムリストからマッピングされた値を取得します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="columnName" class="name">name
</td>
            <td id="columnName" class="type">string
</td>
            <td id="columnName" class="indispensable">○</td>
            <td id="columnName" class="default">-</td>
            <td id="columnName" class="summary">フォームのname, id
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>






<!-- =================================================================
    ■currentTab■
====================================================================== -->
    <table class="container" >
      <tr>
        <th class="funcname" >
          <a name="currentTab_title"></a>
          currentTab
        </th>
      </tr>
      <tr>
        <td style="font-family: MS Gothic">画面遷移を現在開いているタブに対して行うjavascriptを返します。

          <br>
          <br>
          <table class="details">
            <tr class="table_header">
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>tabId</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>タブ ID</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>onloadList</td>
              <td>array</td>
              <td>-</td>
              <td>[]</td>
              <td>リンクを押下された際に実行したいメソッド名を記述した配列</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{currentTab tabId="$tabId" formId="tab3From" url="/test1/test2/test4" onloadList="[\'alert(123)\', \'alert(456)\']"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
KO_currentTab(\'/test1/test2/test4\', \'1\', \'tab3From\', [\'alert(123)\', \'alert(456)\']);
'); ?>
            </pre>

          <a href="#" style="text-align:right">TOP</a>

        </td>
      </tr>
    </table>






<!-- =================================================================
    ■currentTabButton■
====================================================================== -->
    <table class="container" >
      <tr>
        <th class="funcname" >
          <a name="currentTabButton_title"></a>
          currentTabButton
        </th>
      </tr>
      <tr>
        <td style="font-family: MS Gothic">画面遷移を現在開いているタブに対して行うjavascriptをボタンタグに実装した形で返します。

          <br>
          <br>
          <table class="details">
            <tr class="table_header">
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>name</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>ボタン名</td>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>tabId</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>タブ ID</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>onloadList</td>
              <td>array</td>
              <td>-</td>
              <td>[]</td>
              <td>リンクを押下された際に実行したいメソッド名を記述した配列</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{currentTabButton name="現在タブボタン" tabId="$tabId" formId="tab3From" url="/test1/test2/test4" onloadList="[\'alert(123)\', \'alert(456)\']"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
<input type="button" onclick="KO_currentTab(\'/test1/test2/test4\', \'1\', \'tab3From\', [\'alert(123)\', \'alert(456)\']);" value="現在タブボタン">
'); ?>
            </pre>

          <a href="#" style="text-align:right">TOP</a>

        </td>
      </tr>
    </table>






<!-- =================================================================
    ■currentTabLink■
====================================================================== -->
    <table class="container" >
      <tr>
        <th class="funcname" >
          <a name="currentTabLink_title"></a>
          currentTabLink
        </th>
      </tr>
      <tr>
        <td style="font-family: MS Gothic">画面遷移を現在開いているタブに対して行うjavascriptをリンク(aタグ)に実装した形で返します。

          <br>
          <br>
          <table class="details">
            <tr class="table_header">
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>name</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>リンク名</td>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>tabId</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>タブ ID</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>onloadList</td>
              <td>array</td>
              <td>-</td>
              <td>[]</td>
              <td>リンクを押下された際に実行したいメソッド名を記述した配列</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{currentTabLink name="現在タブリンク" tabId="$tabId" formId="tab3From" url="/test1/test2/test4" onloadList="[\'alert(123)\', \'alert(456)\']"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
<a onclick="KO_currentTab(\'/test1/test2/test4\', \'1\', \'tab3From\', [\'alert(123)\', \'alert(456)\']);" href="#">現在タブリンク</a>
'); ?>
            </pre>

          <a href="#" style="text-align:right">TOP</a>

        </td>
      </tr>
    </table>













    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="dbWrite_title"></a>
        dbWrite
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">データベースからプライマリーキーによって値を取得し、
<br>
指定されたカラムの値を返します。
<br>
複数のカラムを指定する場合は配列を用います。
<br>
　　この時、セパレータを指定することが可能です。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="dbWrite" class="name">arClassName
</td>
            <td id="dbWrite" class="type">string
</td>
            <td id="dbWrite" class="indispensable">○</td>
            <td id="dbWrite" class="default">-</td>
            <td id="dbWrite" class="summary">アクティブレコード名
</td>
          </tr>
                    <tr>
            <td id="dbWrite" class="name">pkey
</td>
            <td id="dbWrite" class="type">string
</td>
            <td id="dbWrite" class="indispensable">○</td>
            <td id="dbWrite" class="default">-</td>
            <td id="dbWrite" class="summary">プライマリーキーの値（複合プライマリーキー未対応）
</td>
          </tr>
                    <tr>
            <td id="dbWrite" class="name">column
</td>
            <td id="dbWrite" class="type">string
</td>
            <td id="dbWrite" class="indispensable">○</td>
            <td id="dbWrite" class="default">-</td>
            <td id="dbWrite" class="summary">カラム名、またはカラム名の配列(array(column1, column2, ...)
</td>
          </tr>
                    <tr>
            <td id="dbWrite" class="name">separater
</td>
            <td id="dbWrite" class="type">string
</td>
            <td id="dbWrite" class="indispensable">-</td>
            <td id="dbWrite" class="default">空文字</td>
            <td id="dbWrite" class="summary"> 戻り値のセパレータ(column1_data[separater]column2_data[separater]...columnN_data)
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="divisionCheck_title"></a>
        divisionCheck
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">セッションにセットされた部門IDを返します。
<br>
ただし、部門変更が行われていて、かつフラグとしてtrueが渡された場合、
<br>
変更後の部門IDを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="divisionCheck" class="name">flagChangeDivision
</td>
            <td id="divisionCheck" class="type">boolean
</td>
            <td id="divisionCheck" class="indispensable">-</td>
            <td id="divisionCheck" class="default">false</td>
            <td id="divisionCheck" class="summary">フラグ
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="divisionLister_title"></a>
        divisionLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">部署データを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="divisionLister" class="name">name
</td>
            <td id="divisionLister" class="type">string
</td>
            <td id="divisionLister" class="indispensable">○</td>
            <td id="divisionLister" class="default">-</td>
            <td id="divisionLister" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="divisionLister" class="name">selected
</td>
            <td id="divisionLister" class="type">string
</td>
            <td id="divisionLister" class="indispensable">-</td>
            <td id="divisionLister" class="default">空文字</td>
            <td id="divisionLister" class="summary">選択された要素
</td>
          </tr>
                    <tr>
            <td id="divisionLister" class="name">mode
</td>
            <td id="divisionLister" class="type">string
</td>
            <td id="divisionLister" class="indispensable">-</td>
            <td id="divisionLister" class="default">select</td>
            <td id="divisionLister" class="summary">select/radio
</td>
          </tr>
                    <tr>
            <td id="divisionLister" class="name">attribs
</td>
            <td id="divisionLister" class="type">array
</td>
            <td id="divisionLister" class="indispensable">-</td>
            <td id="divisionLister" class="default">array()</td>
            <td id="divisionLister" class="summary">selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="divisionLister" class="name">addEmpty
</td>
            <td id="divisionLister" class="type">boolean
</td>
            <td id="divisionLister" class="indispensable">-</td>
            <td id="divisionLister" class="default">false</td>
            <td id="divisionLister" class="summary">リストに空白を追加
</td>
          </tr>
                    <tr>
            <td id="divisionLister" class="name">addAllItem
</td>
            <td id="divisionLister" class="type">boolean
</td>
            <td id="divisionLister" class="indispensable">-</td>
            <td id="divisionLister" class="default">false</td>
            <td id="divisionLister" class="summary">リストに｢全て｣を追加
</td>
          </tr>
                    <tr>
            <td id="divisionLister" class="name">listsep
</td>
            <td id="divisionLister" class="type">string
</td>
            <td id="divisionLister" class="indispensable">-</td>
            <td id="divisionLister" class="default">'&lt;br&gt;'</td>
            <td id="divisionLister" class="summary">各エンティティを区切る文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="f5Former_title"></a>
        f5Former
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">ダンプを行い、変数情報を文字列で返します。
<br>
オブジェクトは未対応
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td class="nodata" > - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="formSelect_title"></a>
        formSelect
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">与えられた配列からセレクトボックスのフォームを作成して返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="formSelect" class="name">name
</td>
            <td id="formSelect" class="type">string
</td>
            <td id="formSelect" class="indispensable">○</td>
            <td id="formSelect" class="default">-</td>
            <td id="formSelect" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="formSelect" class="name">selected
</td>
            <td id="formSelect" class="type">string
</td>
            <td id="formSelect" class="indispensable">-</td>
            <td id="formSelect" class="default">空文字</td>
            <td id="formSelect" class="summary"> selectedにする値
</td>
          </tr>
                    <tr>
            <td id="formSelect" class="name">options
</td>
            <td id="formSelect" class="type">array
</td>
            <td id="formSelect" class="indispensable">-</td>
            <td id="formSelect" class="default">array()</td>
            <td id="formSelect" class="summary"> キー名がvalue、値がラベル名となる配列
</td>
          </tr>
                    <tr>
            <td id="formSelect" class="name">attribs
</td>
            <td id="formSelect" class="type">array
</td>
            <td id="formSelect" class="indispensable">-</td>
            <td id="formSelect" class="default">array()</td>
            <td id="formSelect" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="formSelect" class="name">addEmpty
</td>
            <td id="formSelect" class="type">boolean
</td>
            <td id="formSelect" class="indispensable">-</td>
            <td id="formSelect" class="default">false</td>
            <td id="formSelect" class="summary"> リストに空白を追加(デフォルト：無効)
</td>
          </tr>
                    <tr>
            <td id="formSelect" class="name">addAllItem
</td>
            <td id="formSelect" class="type">boolean
</td>
            <td id="formSelect" class="indispensable">-</td>
            <td id="formSelect" class="default">false</td>
            <td id="formSelect" class="summary"> リストに｢全て｣を追加(デフォルト：無効)
</td>
          </tr>
                    <tr>
            <td id="formSelect" class="name">listsep
</td>
            <td id="formSelect" class="type">string
</td>
            <td id="formSelect" class="indispensable">-</td>
            <td id="formSelect" class="default">'&lt;br&gt;'</td>
            <td id="formSelect" class="summary"> 各エンティティを区切る文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="formSelectDb_title"></a>
        formSelectDb
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">データベベースから値を取得し、セレクトボックスを生成して返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="formSelectDb" class="name">name
</td>
            <td id="formSelectDb" class="type">string
</td>
            <td id="formSelectDb" class="indispensable">○</td>
            <td id="formSelectDb" class="default">-</td>
            <td id="formSelectDb" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">selected
</td>
            <td id="formSelectDb" class="type">string
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">空文字</td>
            <td id="formSelectDb" class="summary"> selectedにする値
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">arClassName
</td>
            <td id="formSelectDb" class="type">string
</td>
            <td id="formSelectDb" class="indispensable">○</td>
            <td id="formSelectDb" class="default">-</td>
            <td id="formSelectDb" class="summary">アクティブレコード名
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">where
</td>
            <td id="formSelectDb" class="type">array
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">array()</td>
            <td id="formSelectDb" class="summary"> 検索条件
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">valueName
</td>
            <td id="formSelectDb" class="type">string
</td>
            <td id="formSelectDb" class="indispensable">○</td>
            <td id="formSelectDb" class="default">-</td>
            <td id="formSelectDb" class="summary">value に入力させたいカラム名
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">labelName
</td>
            <td id="formSelectDb" class="type">string|array
</td>
            <td id="formSelectDb" class="indispensable">○</td>
            <td id="formSelectDb" class="default">-</td>
            <td id="formSelectDb" class="summary">ラベルに表示されたいカラム名
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">separater
</td>
            <td id="formSelectDb" class="type">string
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">空文字</td>
            <td id="formSelectDb" class="summary"> ラベルが複数指定された時の区切り文字
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">attribs
</td>
            <td id="formSelectDb" class="type">array
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">array()</td>
            <td id="formSelectDb" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">addEmpty
</td>
            <td id="formSelectDb" class="type">boolean
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">false</td>
            <td id="formSelectDb" class="summary"> リストに空白を追加(デフォルト：無効)
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">addAllItem
</td>
            <td id="formSelectDb" class="type">boolean
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">false</td>
            <td id="formSelectDb" class="summary"> リストに｢全て｣を追加(デフォルト：無効)
</td>
          </tr>
                    <tr>
            <td id="formSelectDb" class="name">listsep
</td>
            <td id="formSelectDb" class="type">string
</td>
            <td id="formSelectDb" class="indispensable">-</td>
            <td id="formSelectDb" class="default">"&lt;br&gt;\n"</td>
            <td id="formSelectDb" class="summary"> 各エンティティを区切る文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="getSession_title"></a>
        getSession
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">セッションの値を返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="getSession" class="name">name
</td>
            <td id="getSession" class="type">string
</td>
            <td id="getSession" class="indispensable">○</td>
            <td id="getSession" class="default">-</td>
            <td id="getSession" class="summary">取り出したいキー名前
</td>
          </tr>
                    <tr>
            <td id="getSession" class="name">namespace
</td>
            <td id="getSession" class="type">string
</td>
            <td id="getSession" class="indispensable">○</td>
            <td id="getSession" class="default">-</td>
            <td id="getSession" class="summary">名前空間の名前
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="gmoMapping_title"></a>
        gmoMapping
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">ショップIDを元にショップ名を取得して返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="gmoMapping" class="name">shopId
</td>
            <td id="gmoMapping" class="type">string
</td>
            <td id="gmoMapping" class="indispensable">○</td>
            <td id="gmoMapping" class="default">-</td>
            <td id="gmoMapping" class="summary">ショップID
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="gmoShopIdLister_title"></a>
        gmoShopIdLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">ショップ ID のリストをselect or radio で表示します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">name
</td>
            <td id="gmoShopIdLister" class="type">string
</td>
            <td id="gmoShopIdLister" class="indispensable">○</td>
            <td id="gmoShopIdLister" class="default">-</td>
            <td id="gmoShopIdLister" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">selected
</td>
            <td id="gmoShopIdLister" class="type">string
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">空文字</td>
            <td id="gmoShopIdLister" class="summary"> selectedにする値
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">addShopNameFlag
</td>
            <td id="gmoShopIdLister" class="type">boolean
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">ture</td>
            <td id="gmoShopIdLister" class="summary"> ショップ名を追記
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">mode
</td>
            <td id="gmoShopIdLister" class="type">string
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">select</td>
            <td id="gmoShopIdLister" class="summary"> select/radio
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">attribs
</td>
            <td id="gmoShopIdLister" class="type">array
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">array()</td>
            <td id="gmoShopIdLister" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">addEmpty
</td>
            <td id="gmoShopIdLister" class="type">boolean
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">false</td>
            <td id="gmoShopIdLister" class="summary"> 先頭項目に空項目を追加
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">addAllItem
</td>
            <td id="gmoShopIdLister" class="type">boolean
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">false</td>
            <td id="gmoShopIdLister" class="summary"> 先頭項目に【全て】の項目を追加
</td>
          </tr>
                    <tr>
            <td id="gmoShopIdLister" class="name">listsep
</td>
            <td id="gmoShopIdLister" class="type">string
</td>
            <td id="gmoShopIdLister" class="indispensable">-</td>
            <td id="gmoShopIdLister" class="default">'&lt;br&gt;'</td>
            <td id="gmoShopIdLister" class="summary"> 各エンティティを区切る文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="haifunSplit_title"></a>
        haifunSplit
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">文字列をハイフン（-）で分割し、指定した添字の値を返す。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="haifunSplit" class="name">$str
</td>
            <td id="haifunSplit" class="type">string
</td>
            <td id="haifunSplit" class="indispensable">○</td>
            <td id="haifunSplit" class="default">-</td>
            <td id="haifunSplit" class="summary">文字列（電話番号・郵便番号など）
</td>
          </tr>
                    <tr>
            <td id="haifunSplit" class="name">$no
</td>
            <td id="haifunSplit" class="type">string
</td>
            <td id="haifunSplit" class="indispensable">○</td>
            <td id="haifunSplit" class="default">-</td>
            <td id="haifunSplit" class="summary">配列分割後の取得したい添字
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="hiddenLister_title"></a>
        hiddenLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">与えられた配列から hidden タグを書き出します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="hiddenLister" class="name">hiddenList
</td>
            <td id="hiddenLister" class="type">Mikoshiva_Controller_Mapping_Form_Interface
</td>
            <td id="hiddenLister" class="indispensable">○</td>
            <td id="hiddenLister" class="default">-</td>
            <td id="hiddenLister" class="summary">アクションフォーム
</td>
          </tr>
                    <tr>
            <td id="hiddenLister" class="name">deleteList
</td>
            <td id="hiddenLister" class="type">array
</td>
            <td id="hiddenLister" class="indispensable">-</td>
            <td id="hiddenLister" class="default">array()</td>
            <td id="hiddenLister" class="summary"> 削除する値
</td>
          </tr>
                    <tr>
            <td id="hiddenLister" class="name">attribs
</td>
            <td id="hiddenLister" class="type">array
</td>
            <td id="hiddenLister" class="indispensable">-</td>
            <td id="hiddenLister" class="default">array()</td>
            <td id="hiddenLister" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="html_select_dateLister_title"></a>
        html_select_dateLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">{html_select_date}拡張したものです。
<br>
基本的な仕様は同プラグインに従います。
<br>
＜相違点＞
<br>
■selectタグのname/idを個別に指定できます。
<br>
　指定しなければname/idは空文字になります。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_name
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">空文字</td>
            <td id="html_select_dateLister" class="summary"> '年'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_name
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">空文字</td>
            <td id="html_select_dateLister" class="summary"> '月'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_name
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">空文字</td>
            <td id="html_select_dateLister" class="summary"> '日'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">time
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">現在の時間</td>
            <td id="html_select_dateLister" class="summary"> 使用する日付/時間（timestamp/ YYYY-MM-DD）
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">start_year
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">現在の年</td>
            <td id="html_select_dateLister" class="summary"> ドロップダウンリストの始めの年 (年を表す数字又は現在の年からの相対年数(+/- N))
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">end_year
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">start_yearと同じ</td>
            <td id="html_select_dateLister" class="summary"> ドロップダウンリストの終わりの年 (年を表す数字又は現在の年からの相対年数(+/- N))
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">display_years
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">TRUE</td>
            <td id="html_select_dateLister" class="summary"> 年のフォームを表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">display_months
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">TRUE</td>
            <td id="html_select_dateLister" class="summary"> 月のフォームを表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">display_days
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">TRUE</td>
            <td id="html_select_dateLister" class="summary"> 日のフォームを表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%m</td>
            <td id="html_select_dateLister" class="summary"> 月の表示フォーマット(strftime)
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%02d</td>
            <td id="html_select_dateLister" class="summary"> 日の表示のフォーマット(sprintf)
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_value_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%m</td>
            <td id="html_select_dateLister" class="summary"> 月の値のフォーマット(strftime)
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_value_format
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">%d</td>
            <td id="html_select_dateLister" class="summary"> 日の値のフォーマット (sprintf)
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">field_order
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">YMD</td>
            <td id="html_select_dateLister" class="summary"> フィールドを表示する順序
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">field_separator
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">\n</td>
            <td id="html_select_dateLister" class="summary"> フィールド間に表示する文字列
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_as_text
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">FALSE</td>
            <td id="html_select_dateLister" class="summary"> 年をテキストとして表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">reverse_years
</td>
            <td id="html_select_dateLister" class="type">boolean
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">FALSE</td>
            <td id="html_select_dateLister" class="summary"> 年を逆順で表示する
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">field_array
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> name属性が与えられた場合、結果の値を name[Day],name[Month],name[Year]の形の連想配列にしてPHPに返す
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_size
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 年のselectタグにsize属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_size
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 月のselectタグにsize属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_size
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 日のselectタグにsize属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">all_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 全てのselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 年のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 月のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_extra
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 日のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">year_empty
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 年のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。 例えば、セレクトボックスに "年を選択して下さい" と表示させる時に便利です。 年を選択しないことを示唆するのに、time属性に対して "-MM-DD" という値が指定できることに注意してください。
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">month_empty
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 月のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。月を選択しないことを示唆するのに、 time属性に対して "YYYY--DD" という値が指定できることに注意してください。
</td>
          </tr>
                    <tr>
            <td id="html_select_dateLister" class="name">day_empty
</td>
            <td id="html_select_dateLister" class="type">string
</td>
            <td id="html_select_dateLister" class="indispensable">-</td>
            <td id="html_select_dateLister" class="default">null</td>
            <td id="html_select_dateLister" class="summary"> 日のセレクトボックスの最初の要素に、指定した文字列をlabelとして、 空文字 "" のvalueを持たせます。日を選択しないことを示唆するのに、 time属性に対して "YYYY-MM-" という値が指定できることに注意してください。
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="html_select_timeLister_title"></a>
        html_select_timeLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">{html_select_time}を拡張したものです。
<br>
基本的な仕様は同プラグインに従います。
<br>
＜相違点＞
<br>
■selectタグのname/idを個別に指定できます。
<br>
　指定しなければname/idは空文字になります。
<br>
■セパレータを指定できます
<br>
　html_select_dateと同様に、field_separatorが指定できます。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="html_select_timeLister" class="name">hour_name
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> '時'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">minute_name
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> '分'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">second_name
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> '秒'のselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">meridian_name
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> AM/PMのselectタグのname, idを指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">field_separator
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">空文字</td>
            <td id="html_select_timeLister" class="summary"> 各selectボックス間の区切り文字を指定します
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">time
</td>
            <td id="html_select_timeLister" class="type">timestamp
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">現在の時間</td>
            <td id="html_select_timeLister" class="summary"> 使用する日付/時間
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">display_hours
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">TRUE</td>
            <td id="html_select_timeLister" class="summary"> 時を表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">display_minutes
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">TRUE</td>
            <td id="html_select_timeLister" class="summary"> 分を表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">display_seconds
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">TRUE</td>
            <td id="html_select_timeLister" class="summary"> 秒を表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">display_meridian
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">TRUE</td>
            <td id="html_select_timeLister" class="summary"> am/pm を表示するかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">use_24_hours
</td>
            <td id="html_select_timeLister" class="type">boolean
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">TRUE</td>
            <td id="html_select_timeLister" class="summary"> 24 時間クロックを用いるかどうか
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">minute_interval
</td>
            <td id="html_select_timeLister" class="type">integer
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">1</td>
            <td id="html_select_timeLister" class="summary"> ドロップダウンリストの分間隔
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">second_interval
</td>
            <td id="html_select_timeLister" class="type">integer
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">1</td>
            <td id="html_select_timeLister" class="summary"> ドロップダウンリストの秒間隔
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">field_array
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">null</td>
            <td id="html_select_timeLister" class="summary"> 結果の値をこの名前の配列に渡して出力
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">all_extra
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">null</td>
            <td id="html_select_timeLister" class="summary"> 全てのselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">hour_extra
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">null</td>
            <td id="html_select_timeLister" class="summary"> 時間のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">minute_extra
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">null</td>
            <td id="html_select_timeLister" class="summary"> 分のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">second_extra
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">null</td>
            <td id="html_select_timeLister" class="summary"> 秒のselect/inputタグに拡張属性を追加
</td>
          </tr>
                    <tr>
            <td id="html_select_timeLister" class="name">meridian_extra
</td>
            <td id="html_select_timeLister" class="type">string
</td>
            <td id="html_select_timeLister" class="indispensable">-</td>
            <td id="html_select_timeLister" class="default">null</td>
            <td id="html_select_timeLister" class="summary"> am/pmのselect/inputタグに拡張属性を追加
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="imploader_title"></a>
        imploader
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">配列要素を指定された文字列により連結して返します
<br>
要素が配列でない場合はそのまま返します
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="imploader" class="name">pieces
</td>
            <td id="imploader" class="type">array
</td>
            <td id="imploader" class="indispensable">○</td>
            <td id="imploader" class="default">-</td>
            <td id="imploader" class="summary">対象となる配列
</td>
          </tr>
                    <tr>
            <td id="imploader" class="name">glue
</td>
            <td id="imploader" class="type">string
</td>
            <td id="imploader" class="indispensable">-</td>
            <td id="imploader" class="default">&lt;br&gt;</td>
            <td id="imploader" class="summary"> 連結に使用する文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="lineToBr_title"></a>
        lineToBr
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">改行コードを改行タグに変換します
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="lineToBr" class="name">str
</td>
            <td id="lineToBr" class="type">string
</td>
            <td id="lineToBr" class="indispensable">○</td>
            <td id="lineToBr" class="default">-</td>
            <td id="lineToBr" class="summary">変換対象の文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>






<!-- =================================================================
    ■newTab■
====================================================================== -->
    <table class="container" >
      <tr>
        <th class="funcname" >
          <a name="newTab_title"></a>
          newTab
        </th>
      </tr>
      <tr>
        <td style="font-family: MS Gothic">新しいタブを開くjavascriptを返します。

          <br>
          <br>
          <table class="details">
            <tr class="table_header">
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>protected</td>
              <td>string</td>
              <td>-</td>
              <td>false</td>
              <td>タブを保護するか</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>onloadList</td>
              <td>array</td>
              <td>-</td>
              <td>[]</td>
              <td>リンクを押下された際に実行したいメソッド名を記述した配列</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{newTab url="/test1/test2/test4" formId="tab3From" protected=true onloadList="[\'alert(123)\', \'alert(456)\']"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
KO_newTab(\'/test1/test2/test4\', true, \'tab3From\', [\'alert(123)\', \'alert(456)\']);
'); ?>
            </pre>

          <a href="#" style="text-align:right">TOP</a>

        </td>
      </tr>
    </table>






<!-- =================================================================
    ■newTabButton■
====================================================================== -->
    <table class="container" >
      <tr>
        <th class="funcname" >
          <a name="newTabButton_title"></a>
          newTabButton
        </th>
      </tr>
      <tr>
        <td style="font-family: MS Gothic">新しいタブを開くjavascriptをボタンタグに実装した形で返します。

          <br>
          <br>
          <table class="details">
            <tr class="table_header">
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>name</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>ボタン名</td>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>protected</td>
              <td>string</td>
              <td>-</td>
              <td>false</td>
              <td>タブを保護するか</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>onloadList</td>
              <td>array</td>
              <td>-</td>
              <td>[]</td>
              <td>リンクを押下された際に実行したいメソッド名を記述した配列</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{newTabButton name="新しいタブボタン" url="/test1/test2/test4" formId="tab3From"  protected=true onloadList="[\'alert(123)\', \'alert(456)\']"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
<input type="button" onclick="KO_newTab(\'/test1/test2/test4\', true, \'tab3From\', [\'alert(123)\', \'alert(456)\']);" value="新しいタブボタン">
'); ?>
            </pre>

          <a href="#" style="text-align:right">TOP</a>

        </td>
      </tr>
    </table>






<!-- =================================================================
    ■newTabLink■
====================================================================== -->
    <table class="container" >
      <tr>
        <th class="funcname" >
          <a name="newTabLink_title"></a>
          newTabLink
        </th>
      </tr>
      <tr>
        <td style="font-family: MS Gothic">新しいタブを開く javascript をリンク( a タグ)に実装した形で返します。

          <br>
          <br>
          <table class="details">
            <tr class="table_header">
              <th>属性名</th>
              <th>型</th>
              <th>必須</th>
              <th>デフォルト</th>
              <th>概要</th>
            </tr>
            <tr>
              <td>name</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>ボタン名</td>
            </tr>
            <tr>
              <td>url</td>
              <td>string</td>
              <td>○</td>
              <td>-</td>
              <td>遷移先 URL</td>
            </tr>
            <tr>
              <td>protected</td>
              <td>string</td>
              <td>-</td>
              <td>false</td>
              <td>タブを保護するか</td>
            </tr>
            <tr>
              <td>formId</td>
              <td>string</td>
              <td>-</td>
              <td>null</td>
              <td>フォームの ID 名</td>
            </tr>
            <tr>
              <td>onloadList</td>
              <td>array</td>
              <td>-</td>
              <td>[]</td>
              <td>リンクを押下された際に実行したいメソッド名を記述した配列</td>
            </tr>
          </table>

            <h2 id="使用例">サンプルソース</h2>

            <pre class="wiki"><?php
highlight_string('<?php
{newTabLink name="新しいタブリンク" url="/test1/test2/test4" formId="tab3From" protected=true onloadList="[\'alert(123)\', \'alert(456)\']"}
'); ?>
            </pre>

            <h2 id="使用例">結果</h2>

            <pre class="wiki"><?php
highlight_string('<?php
<a onclick="KO_newTab(\'/test1/test2/test4\', true, \'tab3From\', [\'alert(123)\', \'alert(456)\']);" href="#">新しいタブリンク</a>
'); ?>
            </pre>

          <a href="#" style="text-align:right">TOP</a>

        </td>
      </tr>
    </table>






    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="prefectureLister_title"></a>
        prefectureLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">都道府県のリスト(select/radioタグ)を作成します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="prefectureLister" class="name">name
</td>
            <td id="prefectureLister" class="type">string
</td>
            <td id="prefectureLister" class="indispensable">○</td>
            <td id="prefectureLister" class="default">-</td>
            <td id="prefectureLister" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="prefectureLister" class="name">selected
</td>
            <td id="prefectureLister" class="type">string
</td>
            <td id="prefectureLister" class="indispensable">-</td>
            <td id="prefectureLister" class="default">空文字</td>
            <td id="prefectureLister" class="summary"> 選択された要素
</td>
          </tr>
                    <tr>
            <td id="prefectureLister" class="name">attribs
</td>
            <td id="prefectureLister" class="type">array
</td>
            <td id="prefectureLister" class="indispensable">-</td>
            <td id="prefectureLister" class="default">array()</td>
            <td id="prefectureLister" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="prefectureLister" class="name">addEmpty
</td>
            <td id="prefectureLister" class="type">boolean
</td>
            <td id="prefectureLister" class="indispensable">-</td>
            <td id="prefectureLister" class="default">false</td>
            <td id="prefectureLister" class="summary"> リストに空白を追加
</td>
          </tr>
                    <tr>
            <td id="prefectureLister" class="name">addAllItem
</td>
            <td id="prefectureLister" class="type">boolean
</td>
            <td id="prefectureLister" class="indispensable">-</td>
            <td id="prefectureLister" class="default">false</td>
            <td id="prefectureLister" class="summary"> リストに｢全て｣を追加
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="requestToId_title"></a>
        requestToId
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">Ajax で指定されたURLへリクエスト(idで指定されたフォーム部品の情報をpost)を行い、
<br>
結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うjavascriptを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="requestToId" class="name">url
</td>
            <td id="requestToId" class="type">string
</td>
            <td id="requestToId" class="indispensable">○</td>
            <td id="requestToId" class="default">-</td>
            <td id="requestToId" class="summary">アクション URL
</td>
          </tr>
                    <tr>
            <td id="requestToId" class="name">targetId
</td>
            <td id="requestToId" class="type">string
</td>
            <td id="requestToId" class="indispensable">○</td>
            <td id="requestToId" class="default">-</td>
            <td id="requestToId" class="summary">結果書き込み先(htmlタグのID)
</td>
          </tr>
                    <tr>
            <td id="requestToId" class="name">formId
</td>
            <td id="requestToId" class="type">string
</td>
            <td id="requestToId" class="indispensable">-</td>
            <td id="requestToId" class="default">null</td>
            <td id="requestToId" class="summary"> リクエスト時にpostするフォーム部品のID
</td>
          </tr>
                    <tr>
            <td id="requestToId" class="name">append
</td>
            <td id="requestToId" class="type">string
</td>
            <td id="requestToId" class="indispensable">-</td>
            <td id="requestToId" class="default">OVERWRITE</td>
            <td id="requestToId" class="summary"> 追記モード(FRONT/前方へ挿入/BACK:後方へ追記/OVERWRITE:上書き)
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="requestToIdButton_title"></a>
        requestToIdButton
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">Ajax で指定されたアクションへリクエストを行い、
<br>
結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うボタンタグを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="requestToIdButton" class="name">name
</td>
            <td id="requestToIdButton" class="type">string
</td>
            <td id="requestToIdButton" class="indispensable">○</td>
            <td id="requestToIdButton" class="default">-</td>
            <td id="requestToIdButton" class="summary">ボタンのvalue
</td>
          </tr>
                    <tr>
            <td id="requestToIdButton" class="name">url
</td>
            <td id="requestToIdButton" class="type">string
</td>
            <td id="requestToIdButton" class="indispensable">○</td>
            <td id="requestToIdButton" class="default">-</td>
            <td id="requestToIdButton" class="summary">アクション URL
</td>
          </tr>
                    <tr>
            <td id="requestToIdButton" class="name">targetId
</td>
            <td id="requestToIdButton" class="type">string
</td>
            <td id="requestToIdButton" class="indispensable">○</td>
            <td id="requestToIdButton" class="default">-</td>
            <td id="requestToIdButton" class="summary">結果書き込み先(htmlタグのID)
</td>
          </tr>
                    <tr>
            <td id="requestToIdButton" class="name">formId
</td>
            <td id="requestToIdButton" class="type">string
</td>
            <td id="requestToIdButton" class="indispensable">-</td>
            <td id="requestToIdButton" class="default">null</td>
            <td id="requestToIdButton" class="summary"> リクエスト時にpostするフォームのID
</td>
          </tr>
                    <tr>
            <td id="requestToIdButton" class="name">append
</td>
            <td id="requestToIdButton" class="type">string
</td>
            <td id="requestToIdButton" class="indispensable">-</td>
            <td id="requestToIdButton" class="default">OVERWRITE</td>
            <td id="requestToIdButton" class="summary"> 追記モード(FRONT/前方へ挿入/BACK:後方へ追記/OVERWRITE:上書き)
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="requestToIdLink_title"></a>
        requestToIdLink
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">Ajax で指定されたアクションへリクエストを行い、
<br>
結果を指定されたIDを持つHTMLタグへ追記無いし上書きを行うjavascriptを
<br>
リンク(aタグ)に実装した形で返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="requestToIdLink" class="name">name
</td>
            <td id="requestToIdLink" class="type">string
</td>
            <td id="requestToIdLink" class="indispensable">○</td>
            <td id="requestToIdLink" class="default">-</td>
            <td id="requestToIdLink" class="summary">リンクテキスト
</td>
          </tr>
                    <tr>
            <td id="requestToIdLink" class="name">url
</td>
            <td id="requestToIdLink" class="type">string
</td>
            <td id="requestToIdLink" class="indispensable">○</td>
            <td id="requestToIdLink" class="default">-</td>
            <td id="requestToIdLink" class="summary">アクション URL
</td>
          </tr>
                    <tr>
            <td id="requestToIdLink" class="name">targetId
</td>
            <td id="requestToIdLink" class="type">string
</td>
            <td id="requestToIdLink" class="indispensable">○</td>
            <td id="requestToIdLink" class="default">-</td>
            <td id="requestToIdLink" class="summary">結果書き込み先(htmlタグのID)
</td>
          </tr>
                    <tr>
            <td id="requestToIdLink" class="name">formId
</td>
            <td id="requestToIdLink" class="type">string
</td>
            <td id="requestToIdLink" class="indispensable">-</td>
            <td id="requestToIdLink" class="default">null</td>
            <td id="requestToIdLink" class="summary"> リクエスト時にpostするフォームのID
</td>
          </tr>
                    <tr>
            <td id="requestToIdLink" class="name">append
</td>
            <td id="requestToIdLink" class="type">string
</td>
            <td id="requestToIdLink" class="indispensable">-</td>
            <td id="requestToIdLink" class="default">OVERWRITE</td>
            <td id="requestToIdLink" class="summary"> 追記モード(FRONT/前方へ挿入/BACK:後方へ追記/OVERWRITE:上書き)
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="searchNotFoundMessage_title"></a>
        searchNotFoundMessage
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">検索結果が取得出来なかった時のメッセージを返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td class="nodata" > - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
            <td class="nodata"> - </td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="staffInfo_title"></a>
        staffInfo
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">スタッフID及び、カラム名を元に、スタッフ情報を返します。
<br>
カラム名を複数指定した場合、それらを連結して返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="staffInfo" class="name">staffId
</td>
            <td id="staffInfo" class="type">string
</td>
            <td id="staffInfo" class="indispensable">○</td>
            <td id="staffInfo" class="default">-</td>
            <td id="staffInfo" class="summary">スタッフID
</td>
          </tr>
                    <tr>
            <td id="staffInfo" class="name">column
</td>
            <td id="staffInfo" class="type">string
</td>
            <td id="staffInfo" class="indispensable">○</td>
            <td id="staffInfo" class="default">-</td>
            <td id="staffInfo" class="summary">カラム名、またはカラム名の配列(array(column1, column2, ...)
</td>
          </tr>
                    <tr>
            <td id="staffInfo" class="name">separater
</td>
            <td id="staffInfo" class="type">string
</td>
            <td id="staffInfo" class="indispensable">-</td>
            <td id="staffInfo" class="default">空文字</td>
            <td id="staffInfo" class="summary"> 戻り値のセパレータ(column1_data[separater]column2_data[separater]...columnN_data)
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="statusLister_title"></a>
        statusLister
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">ステータス・タイプを元に、それが含む意味のリスト(select/radioフォーム)を作成します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="statusLister" class="name">name
</td>
            <td id="statusLister" class="type">string
</td>
            <td id="statusLister" class="indispensable">○</td>
            <td id="statusLister" class="default">-</td>
            <td id="statusLister" class="summary">フォームのname, id
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">selected
</td>
            <td id="statusLister" class="type">string
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">空文字</td>
            <td id="statusLister" class="summary"> 選択された要素
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">status
</td>
            <td id="statusLister" class="type">string
</td>
            <td id="statusLister" class="indispensable">○</td>
            <td id="statusLister" class="default">-</td>
            <td id="statusLister" class="summary">ステータス・タイプ
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">mode
</td>
            <td id="statusLister" class="type">string
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">select</td>
            <td id="statusLister" class="summary"> select/radio
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">deleteList
</td>
            <td id="statusLister" class="type">array
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">array()</td>
            <td id="statusLister" class="summary"> 削除リスト
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">attribs
</td>
            <td id="statusLister" class="type">array
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">array()</td>
            <td id="statusLister" class="summary"> selectタグに指定する追加の属性 '属性名' =&gt; 値
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">addEmpty
</td>
            <td id="statusLister" class="type">boolean
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">false</td>
            <td id="statusLister" class="summary"> リストに空白を追加
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">addAllItem
</td>
            <td id="statusLister" class="type">boolean
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">false</td>
            <td id="statusLister" class="summary"> リストに｢全て｣を追加
</td>
          </tr>
                    <tr>
            <td id="statusLister" class="name">listsep
</td>
            <td id="statusLister" class="type">string
</td>
            <td id="statusLister" class="indispensable">-</td>
            <td id="statusLister" class="default">'&lt;br&gt;'</td>
            <td id="statusLister" class="summary"> 各エンティティを区切る文字列
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
    <table class="container" >
    <tr>
      <th class="funcname" >
        <a name="statusMapping_title"></a>
        statusMapping
      </th>
    </tr>
    <tr>
      <td style="font-family: MS Gothic">コードから、その意味を取得して返します。
<br>
<br>
        <table class="details">
          <tr class="table_header"><th>属性名</th><th>型</th><th>必須</th><th>デフォルト</th><th>概要</th></tr>
                    <tr>
            <td id="statusMapping" class="name">code
</td>
            <td id="statusMapping" class="type">string
</td>
            <td id="statusMapping" class="indispensable">○</td>
            <td id="statusMapping" class="default">-</td>
            <td id="statusMapping" class="summary">コード
</td>
          </tr>
                  </table>
      <a href="#" style="text-align:right">TOP</a></td>
    </tr>
  </table>
  </div>
このドキュメントは<a href="http://mikoshiva/test/manual/helper-all">helper-all</a>によって生成されました。
</body>
</html>
