<?php
/** @package Mikoshiva_Controller */
require_once 'Zend/Controller/Request/Simple.php';

/** Zend_Controller_Request_Abstract */
require_once 'Zend/Controller/Request/Abstract.php';

/**
 * 
 * 
 * @package Mikoshiva_Controller
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/22
 * @version SVN:$Id: Simple.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Controller_Request_Simple extends Zend_Controller_Request_Simple
{
    
    /**
     * __issetへのエイリアス
     *
     * @param $key
     * @return boolean
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/22
     * @version SVN:$Id: Simple.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function has($key) {
        return $this->__isset($key);
    }
    
    /**
     * 
     *
     * @param $key
     * @return bolean
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/02/22
     * @version SVN:$Id: Simple.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __isset($key)
    {
        switch (true) {
            case isset($this->_params[$key]):
                return true;
            default:
                return false;
        }
    }
}
