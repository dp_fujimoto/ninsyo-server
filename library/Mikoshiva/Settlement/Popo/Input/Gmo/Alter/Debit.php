<?php
/** @package Popo */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Input_Gmo_Alter
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Input_Gmo_Alter_Debit extends Popo_Abstract {


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'ShopID' => null,
        'OrderID' => null,
//////////////        'Method' => null,
//////////////        'PayTimes' => null,
        'Amount' => null,
    );


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * ShopID
     *
     * @return  ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getShopID() {
        return $this->_properties['ShopID'];
    }


    /**
     * ShopID
     *
     * @param  $ShopID ShopID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setShopID($ShopID) {
        $this->_properties['ShopID'] = $ShopID;
    }


    /**
     * OrderID
     *
     * @return string Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getOrderID() {
        return $this->_properties['OrderID'];
    }


    /**
     * OrderID
     *
     * @param string $OrderID OrderID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setOrderID($OrderID) {
        $this->_properties['OrderID'] = $OrderID;
    }


////////////////    /**
////////////////     * Method
////////////////     *
////////////////     * @return string Method
////////////////     *
////////////////     * @author T.Tsukasa <taniguchi@kaihatsu.com>
////////////////     * @since 2010/01/20
////////////////     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
////////////////     *
////////////////     */
////////////////    public function getMethod() {
////////////////        return $this->_properties['Method'];
////////////////    }
////////////////
////////////////
////////////////    /**
////////////////     * Method
////////////////     *
////////////////     * @param string $Method Method
////////////////     *
////////////////     * @author T.Tsukasa <taniguchi@kaihatsu.com>
////////////////     * @since 2010/01/20
////////////////     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
////////////////     *
////////////////     */
////////////////    public function setMethod($Method) {
////////////////        $this->_properties['Method'] = $Method;
////////////////    }
////////////////
////////////////
////////////////    /**
////////////////     * PayTimes
////////////////     *
////////////////     * @return string PayTimes
////////////////     *
////////////////     * @author T.Tsukasa <taniguchi@kaihatsu.com>
////////////////     * @since 2010/01/20
////////////////     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
////////////////     *
////////////////     */
////////////////    public function getPayTimes() {
////////////////        return $this->_properties['PayTimes'];
////////////////    }
////////////////
////////////////
////////////////    /**
////////////////     * PayTimes
////////////////     *
////////////////     * @param string $PayTimesd PayTimes
////////////////     *
////////////////     * @author T.Tsukasa <taniguchi@kaihatsu.com>
////////////////     * @since 2010/01/20
////////////////     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
////////////////     *
////////////////     */
////////////////    public function setPayTimes($PayTimes) {
////////////////        $this->_properties['PayTimes'] = $PayTimes;
////////////////    }


    /**
     * Amount
     *
     * @return  Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAmount() {
        return $this->_properties['Amount'];
    }


    /**
     * Amount
     *
     * @param  $Amount Amount
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: Debit.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAmount($Amount) {
        $this->_properties['Amount'] = $Amount;
    }


}

























