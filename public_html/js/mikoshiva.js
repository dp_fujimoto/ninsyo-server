
/****
$(document).ready(function(){
	
	// 初期設定
		
		// #nav直下の全li要素の中から最初のli要素に.selectを追加
		$('#nav > li:first').addClass('select');
		// #nav直下の全li要素にマウスオーバーしたらリンク要素に偽装
		$('#nav > li').hover(function(){
			$(this).css('cursor','pointer');
		},function(){
			$(this).css('cursor','default');
		});
		
		// #tab直下の全div要素を非表示
		$('#tab > div').hide();
		// #tab直下の全div要素の中から最初のdiv要素を表示
		$('#tab > div:first').show();

	
});
*********/

function init() {
	
	// #nav直下の全li要素の中から最初のli要素に.selectを追加
	$('#nav > li:first').addClass('select');
	// #nav直下の全li要素にマウスオーバーしたらリンク要素に偽装
	$('#nav > li').hover(function(){
		$(this).css('cursor','pointer');
	},function(){
		$(this).css('cursor','default');
	});
	
	// #tab直下の全div要素を非表示
	$('#tab > div').hide();
	// #tab直下の全div要素の中から最初のdiv要素を表示
	$('#tab > div:first').show();
}


var tabNumber = 0;
var tabStatus = {};

/**
 * 親メニューがクリックされたときに新しいタブを開きます
 * 
 * @return
 */
function OYA_newTab() {
	
	// ◆タブナンバーをカウントアップ
	++tabNumber;
		
	// ◆タブを初期化
	$('#tab').html('<ul id="nav"></ul>');

	// ◆tab直下の全div要素を非表示
	$('#tab > div').hide();


	// ◆タブを作成
	$('<li id="tab_li_' + tabNumber + '" onclick="KO_focusTab(this)">test nav' + (tabNumber) + '（__<span onclick="KO_closeTab(this)">閉じる</span>__）</li>').addClass('select').appendTo('#nav');
	
	// ◆コンテンツの箱を作成し、読み込み完了までローディングアニメーションを表示
	$('<div id="test' + (tabNumber) + '"><p align="center"><img src="/img/loading.gif"></p></div>').appendTo('#tab');
	
	// ◆アクションを叩きコンテンツの箱に上書き
	$.post('/staff/read', null, function(data, status){
	    $('#test' + tabNumber).html('<span onclick="KO_newTab()">test tab' + (tabNumber) + '</span><br>' + data)
	});
	
	// ◆nav直下の全li要素にマウスオーバーしたらリンク要素に偽装
	$('#nav > li').hover(
		function(){$(this).css('cursor','pointer');},
		function(){$(this).css('cursor','default');}
	);

	tabStatus['test' + tabNumber] = {save: true};
	
}

/**
 * アクションが叩かれたときに自分のタブに表示する処理
 * 
 * @return
 */
function KO_currentTab() {

	$.post('/staff/read', null, function(data, status){
		$('#test' + tabNumber).html('<span onclick="KO_newTab()">test tab' + (tabNumber) + '</span><br>' + data)
	});
}

/**
 * アクションを叩くときに新しいタブを作成する処理
 * 
 * @return
 */
function KO_newTab() {
	
	// ◆タブナンバーをカウントアップ
	++tabNumber;
	
			
	// ◆nav直下の全 li 要素の class 属性を削除
	//   一度全てのタブを非アクティブにする
	$('#nav > li').removeClass('select');
	
	
	// ◆tab 直下の全 div 要素（コンテンツ）を非表示
	$('#tab > div').hide();
	
	
	// ◆クリックしたタブのインデックス番号と同じdiv要素をフェード表示
	$('#tab > div').eq($('#nav > li').index(this)).fadeIn();

	
	// ◆タブを新規作成し、アクティブ表示にする
	$('<li id="tab_li_' + tabNumber + '" onclick="KO_focusTab(this)">test nav' + (tabNumber) + '（__<span onclick="KO_closeTab(this)">閉じる</span>__）</li>').addClass('select').appendTo('#nav');
	
	
	// ◆コンテンツの箱を作成し、ローディングアニメーションを表示
	$('<div id="test' + tabNumber + '"><p align="center"><img src="/img/loading.gif"></p></div>').appendTo('#tab');
	
	
	// ◆アクションを叩き、結果を新規コンテンツに埋める
	$.post('/staff/read', null, function(data, status){
	    $('#test' + tabNumber).html('<span onclick="KO_newTab()">test tab' + (tabNumber) + '</span><br>' + data)
	});
	
	
	// ◆nav直下の全li要素にマウスオーバーしたらリンク要素に偽装
	$('#nav > li').hover(
		function(){$(this).css('cursor','pointer');},
		function(){$(this).css('cursor','default');}
	);


	tabStatus[tabNumber] = {save: false};
}

/**
 * 選択されたタブに対しフォーカスを当てます
 * 
 * @param ko_menu
 * @return
 */
function KO_focusTab(ko_menu) {

	
	// ◆#nav 直下の全 li 要素の class 属性を削除
	//   一度全てのタブを非アクティブにする
	$('#nav > li').removeClass('select');
	
	
	// ◆クリックした li 要素に select を追加
	//   クリックしたタブをアクティブにする
	$(ko_menu).addClass('select');
	
	
	// ◆#tab直下の全div要素を非表示
	//   全コンテンツを一度非アクティブにする
	$('#tab > div').hide();
	
	
	// ◆クリックしたタブのインデックス番号と同じdiv要素をフェード表示
	//   クリックされたタブのコンテンツ内容を表示
	$('#tab > div').eq( $('#nav > li').index(ko_menu) ).fadeIn();

	
	// ◆nav直下の全li要素にマウスオーバーしたらリンク要素に偽装
	$('#nav > li').hover(
		function(){$(this).css('cursor','pointer');},
		function(){$(this).css('cursor','default');}
	);
}


/**
 * 選択されたタブを削除します
 * 
 * @param ko_menu
 * @return
 */
function KO_closeTab(ko_menu) {
	
	var currentTabnumber = $('#nav > li').index( $(ko_menu).parent() );
	
	// 選択されたタブを削除
	$('#tab > div').eq( $('#nav > li').index( $(ko_menu).parent() ) ).remove();
	
	// 選択されたタブのコンテンツを削除
	$(ko_menu).parent().remove();
	//$('#nav > li').eq(mae).addClass('select');
	//KO_focusTab($('#nav > div').eq(mae));
	tabStatus[currentTabnumber].save = false;
}

/**
 * 全てのタブを閉じます
 * 
 * @return
 */
function KO_closeTabAll() {
	
	$('#tab').html('');
	tabStatus = {};
}


/**
 * 非アクティブなタブを閉じます
 * 
 * @return
 */
function KO_closeTabNotActive() {
	
	var newTabStatus = {};
	for (var i in tabStatus) {
		if (tabStatus[i].save === false) {
			
			if (!$('#tab_li_' + i)) continue;
			$('#tab_li_' + i).remove();
			$('#test' + i).remove();

		} else if ( tabStatus[i].save === true ) {
			newTabStatus[i] = {save: true}
		}
	}
	tabStatus = newTabStatus;
}






