<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ブランド登録</h2>

            <!-- // 必須入力メッセージ -->
            (<span class="COM_fontRed">*</span>)のある項目は必ず入力してください。

            <!-- // エラーメッセージ -->
            {if $message|count > 0}
                <p class="COM_errorMsg">
                    {imploader pieces=$message glue='<br>'}
                </p>
            {/if}

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                <input type="hidden" name="brandId" value="{$form->getBrandId()}">
                <input type="hidden" name="logoNoneFlag" value="{$form->getLogoNoneFlag()}">
                <input type="hidden" name="bannerVerticalNoneFlag" value="{$form->getBannerVerticalNoneFlag()}">
                <input type="hidden" name="bannerSidewaysNoneFlag" value="{$form->getBannerSidewaysNoneFlag()}">
                <input type="hidden" name="logoUpdateDatetime" value="{$form->getLogoUpdateDatetime()}" >
                <input type="hidden" name="bannerVerticalUpdateDatetime" value="{$form->getBannerVerticalUpdateDatetime()}" >
                <input type="hidden" name="bannerSidewaysUpdateDatetime" value="{$form->getBannerSidewaysUpdateDatetime()}" >

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ID
                        </td>
                        <td class="COM_nowrap">{if !isBlankOrNull($form->getBrandId())}{$form->getBrandId()}{else}新規登録中{/if}</td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            部署<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            {divisionLister name="divisionId" mode="select" selected=$form->getDivisionId()}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ブランド名<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="brandName" value="{$form->getBrandName()}" size="80" maxlength="200">
                            <br>
                            <span class="COM_fontGrey">お客様が目にすることを前提とした名前<br>iosアプリ用は表示を考慮して長い名前に注意</span>
                        </td>
                    </tr>
                    {*
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ブランドコード<span class="COM_fontRed COM_bold">*</span>
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="brandCode" value="{$form->getBrandCode()}" size="30" maxlength="30">
                            <br>
                            <span class="COM_fontGrey">管理上のコード</span>
                        </td>
                    </tr>
                    *}
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            メモ
                        </td>
                        <td class="COM_nowrap">
                            <textarea name="memo" cols="70" rows="4">{$form->getMemo()}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            状態
                        </td>
                        <td class="COM_nowrap">
                            {if !isBlankOrNull($form->getBrandId())}
                                <input type="checkbox" name="deleteFlag" value="1" {if $form->getDeleteFlag() === '1'}checked{/if}>削除する
                            {else}
                                <input type="hidden" name="deleteFlag" value="0">新規
                            {/if}
                        </td>
                    </tr>
                </table>

                <br>
                【用途がライブラリーの場合は下記の項目は入力推奨】

                <table class="COM_table">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            ロゴURL
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="logoUrl" value="{$form->getLogoUrl()}" size="100" maxlength="500">
                            <br>
                            <span class="COM_fontGrey">リスト表示に使用  横{$smarty.const.BLAND_LOGO_W}px 縦{$smarty.const.BLAND_LOGO_H}pxの縮尺で作成してください</span>
                            {if $form->getLogoNoneFlag() === '1'}
                                <input type="hidden" name="logoUpdateFlag" value="1">
                            {else}
                                <hr style="border-style: dashed;">
                                <input type="checkbox" name="logoUpdateFlag" value="1" {if $form->getLogoUpdateFlag() === '1'}checked{/if}>
                                ←URLを変更、またはURLにある画像データを変更したらチェック
                                <br>
                                <span class="COM_bold">最終更新日：{$form->getLogoUpdateDatetime()}</span>
                                <br>
                                <span class="COM_fontRed">ロゴに関して変更があった場合、チェックボックスにチェックを入れないとユーザーに反映されません</span>
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            バナー縦画像URL
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="bannerVerticalUrl" value="{$form->getBannerVerticalUrl()}" size="100" maxlength="500">
                            <br>
                            <span class="COM_fontGrey">画面を横にした場合に使うバナー画像 横{$smarty.const.BLAND_BANNER_V_W}px 縦{$smarty.const.BLAND_BANNER_V_H}pxの縮尺で作成してください</span>
                            {if $form->getBannerVerticalNoneFlag() === '1'}
                                <input type="hidden" name="bannerVerticalUpdateFlag" value="1">
                            {else}
                                <hr style="border-style: dashed;">
                                <input type="checkbox" name="bannerVerticalUpdateFlag" value="1" {if $form->getBannerVerticalUpdateFlag() === '1'}checked{/if}>
                                ←URLを変更、またはURLにある画像データを変更したらチェック
                                <br>
                                <span class="COM_bold">最終更新日：{$form->getBannerVerticalUpdateDatetime()}</span>
                                <br>
                                <span class="COM_fontRed">バナー縦画像に関して変更があった場合、チェックボックスにチェックを入れないとユーザーに反映されません</span>
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            バナー横画像URL
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="bannerSidewaysUrl" value="{$form->getBannerSidewaysUrl()}" size="100" maxlength="500">
                            <br>
                            <span class="COM_fontGrey">画面を横にした場合に使うバナー画像 横{$smarty.const.BLAND_BANNER_S_W}px 縦{$smarty.const.BLAND_BANNER_S_H}pxの縮尺で作成してください</span>
                            {if $form->getBannerSidewaysNoneFlag() === '1'}
                                <input type="hidden" name="bannerSidewaysUpdateFlag" value="1">
                            {else}
                                <hr style="border-style: dashed;">
                                <input type="checkbox" name="bannerSidewaysUpdateFlag" value="1" {if $form->getBannerSidewaysUpdateFlag() === '1'}checked{/if}>
                                ←URLを変更、またはURLにある画像データを変更したらチェック
                                <br>
                                <span class="COM_bold">最終更新日：{$form->getBannerSidewaysUpdateDatetime()}</span>
                                <br>
                                <span class="COM_fontRed">バナー横画像に関して変更があった場合、チェックボックスにチェックを入れないとユーザーに反映されません</span>
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            注釈
                        </td>
                        <td class="COM_nowrap">
                            <input type="text" name="description" value="{$form->getDescription()}" size="80" maxlength="132">
                            <br>
                            <span class="COM_fontGrey">お客様が目にすることを前提としたこのブランドの説明文。改行不可</span>
                        </td>
                    </tr>
                </table>

                <br>
                <hr>
                <br>

                <!-- // ページ下部のボタン開始 -->
                <div>
                    {currentTabButton name='　　　　　　次へ　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/product/brand/confirm" tabId=$tabId  formId='inputForm'}
                </div>
                <!-- // ページ下部のボタン終了 -->

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->