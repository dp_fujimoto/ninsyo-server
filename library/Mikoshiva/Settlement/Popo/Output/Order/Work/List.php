<?php
/** @package Mikoshiva_Settlement */

require_once 'Popo/Abstract.php';


/**
 * Mikoshiva_Settlement_Popo_Output_Order_Work_List
 *
 * @package Mikoshiva_Settlement
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Popo_Output_Order_Work_List extends Popo_Abstract {


    /**
     * Properties
     *
     * @var array Properties
     */
    protected $_properties = array(
        'normal' => null,
        'auth' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }



    /**
     * 標準
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Order 標準
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getNormal() {
		return $this->_properties['normal'];
    }


    /**
     * 標準
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Order $normal 標準
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setNormal(Mikoshiva_Settlement_Popo_Output_Order $normal) {
		$this->_properties['normal'] = $normal;
    }


    /**
     * 仮売上
	 *
     * @return Mikoshiva_Settlement_Popo_Output_Order 仮売上
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function getAuth() {
		return $this->_properties['auth'];
    }


    /**
     * 仮売上
	 *
	 * @param Mikoshiva_Settlement_Popo_Output_Order $auth 仮売上
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     * @version SVN:$Id: List.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     *
     */
    public function setAuth(Mikoshiva_Settlement_Popo_Output_Order $auth) {
		$this->_properties['auth'] = $auth;
    }
}

























