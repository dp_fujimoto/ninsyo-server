<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'><?php echo $title = 'DB からテンプレートを取得して自動でセットしたい'; ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1><?php echo $title; ?></h1>



            <!-- ========================================
              ■DB からテンプレートを取得して自動でセットしたい
            ==========================================-->
            <h2>サンプル</h2>

            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Simple.php\';
include_once \'Mikoshiva/Mail.php\';

// 顧客情報を取得する
$simple       = new Mikoshiva_Db_Simple();
$customerPopo = $simple->simpleOneSelectById(\'Mikoshiva_Db_Ar_MstCustomer\', 1);


// メールクラスのインスタンスを生成する
$mail = new Mikoshiva_Mail();


// 置換リストを作成
$replaceList                = array();
$replaceList[]              = $customerPopo;
$replaceList[\'DOKI_DOKI\'] = \'ドキドキ\';


// メールテンプレートを取得しセット
// 件名・本文・From がセットされます
$mail->setMailTemplateById(\'MstMailTemplate\', 1, $replaceList);

// 送信
$mail->send();
');
?>
            </pre>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















