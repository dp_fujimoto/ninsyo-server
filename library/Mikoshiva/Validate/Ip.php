<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Ip
 */
require_once 'Zend/Validate/Ip.php';

/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Ip.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Ip extends Zend_Validate_Ip
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_IP_ADDRESS => "%COLUMN_NAME%は IP アドレスではないようです"
    );
}
