<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Validate - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/validate">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> バリデータ"><?php echo basename(__FILE__, '.php') ?> バリデータ</h1>

            <p>
               このバリデータは指定した拡張子以外のファイルがアップロードされた場合にエラーとします。
            </p>

            <div class="system-message">
              <p>
                アップロードファイルの検証は、内部的に通常のリクエスト値とは別で検証を行います。<br>
                このためアプロードファイルの検証定義は file セクションの中に記述して下さい。
              </p>
            </div>

            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2>使用例</h2>

            <h3>通常の検証方法</h3>
            <p>
              アップロードされたファイルが jpg,jpeg,gif,png 以外で有った場合にエラーを返します。
            </p>
            <pre class="wiki"><?php
highlight_string('<?php

    #file セクション ----------------------------
    file:

      #テストアップロードファイル０１
      file1:
        0: [Extension, false, [jpg, jpeg, gif, png]]
        # もしくは
        #0: [Extension, false, \'php,exe\']
        #0: [Extension, false, {0: jpg, 1: jpeg, 2: gif, 3: png}]
        allowEmpty: false
'); ?>
            </pre>

            <h3>拡張子の大文字小文字を区別する場合</h3>
            <p>
              第三引数に case: true を加えることで大文字小文字の区別をする様になります。<br>
              以下の例ではアップロードされたファイルが JPG で有った場合にエラーとなります。
            </p>
            <pre class="wiki"><?php
highlight_string('<?php

    #file セクション ----------------------------
    file:

      #テストアップロードファイル０１
      file1:
        0: [Extension, false, {0: jpg, 1: jpeg, 2: gif, 3: png, {case: true}}]
        allowEmpty: false
'); ?>
            </pre>


  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>