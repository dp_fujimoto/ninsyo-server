<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'Popo/MstInformationHistory.php';

/**
 * スタッフ情報の更新を行う。
 *
 * @author         fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/28
 * @version        SVN: $Id:
 *
 */
class Tool_Models_Information_Complete
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * スタッフ情報の更新を行う。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        //
        // スタッフ情報の更新を行う。
        //
        //-----------------------------------------------
        // 空のデータはnullにするため、配列にして処理してからactionFormに詰める
        $data = Mikoshiva_Utility_Copy::copyToArray($actionForm);
        logInfo('(' . __LINE__ . ')', 'foreach：ループ開始');
        foreach ($data as $k => $v) {
            logInfo('(' . __LINE__ . ')', 'loop：ループキー：' . $k);
            $data[$k] = Mikoshiva_Utility_Convert::blankToNull($v);
        }
        Mikoshiva_Utility_Copy::copyArrayToActionFormProperties($actionForm,$data);

        $db = new Mikoshiva_Db_Simple();

        // popoの宣言とactionFormの値詰め替え
        $informationHistory = new Popo_MstInformationHistory();

        // 更新時は元のデータを取得し、更新内容を上書きしてアップデートを行う
        if ($actionForm->getInformationHistoryId() > 0) {
            logInfo('(' . __LINE__ . ')', 'if：スタッフIDあり');
            $informationHistory = $db->simpleOneSelectById('Mikoshiva_Db_Ar_MstInformationHistory', $actionForm->getInformationHistoryId());

        }

        $informationHistory->setStaffId( $context->getStaff()->mstStaff->getStaffId() );
        $informationHistory->setSubject( $actionForm->getSubject() );
        $informationHistory->setBody( $actionForm->getBody() . $actionForm->getFileInfo() );
        if ($actionForm->getDeleteFlag() === '1') {
            $informationHistory->setDeleteFlag('1');
            $informationHistory->setDeletionDatetime( Mikoshiva_Date::mikoshivaNow() );
        } else {
            $informationHistory->setDeleteFlag('0');
            $informationHistory->setDeletionDatetime(null);
        }
        // 更新（シンプルクラス）
        $result = $db->simpleOneCreateOrUpdateById( 'Mikoshiva_Db_Ar_MstInformationHistory', $informationHistory  );

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return $actionForm;
    }
}
