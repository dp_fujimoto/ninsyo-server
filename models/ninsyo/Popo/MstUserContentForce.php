<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_user_content_force
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstUserContentForce extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'muf';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'userContentForceId' => null,
        'userContentId' => null,
        'startDate' => null,
        'endDate' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * muf_user_content_force_id の値を返します
	 *
     * @return int muf_user_content_force_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserContentForceId() {
		return $this->_properties['userContentForceId'];
    }


    /**
     * muf_user_content_force_id の値をセットします
	 *
	 * @param int $userContentForceId muf_user_content_force_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserContentForceId($userContentForceId) {
		$this->_properties['userContentForceId'] = $userContentForceId;
    }


    /**
     * muf_user_content_id の値を返します
	 *
     * @return int muf_user_content_id
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUserContentId() {
		return $this->_properties['userContentId'];
    }


    /**
     * muf_user_content_id の値をセットします
	 *
	 * @param int $userContentId muf_user_content_id
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUserContentId($userContentId) {
		$this->_properties['userContentId'] = $userContentId;
    }


    /**
     * muf_start_date の値を返します
	 *
     * @return date muf_start_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getStartDate() {
		return $this->_properties['startDate'];
    }


    /**
     * muf_start_date の値をセットします
	 *
	 * @param date $startDate muf_start_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setStartDate($startDate) {
		$this->_properties['startDate'] = $startDate;
    }


    /**
     * muf_end_date の値を返します
	 *
     * @return date muf_end_date
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getEndDate() {
		return $this->_properties['endDate'];
    }


    /**
     * muf_end_date の値をセットします
	 *
	 * @param date $endDate muf_end_date
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setEndDate($endDate) {
		$this->_properties['endDate'] = $endDate;
    }


    /**
     * muf_delete_flag の値を返します
	 *
     * @return char muf_delete_flag
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * muf_delete_flag の値をセットします
	 *
	 * @param char $deleteFlag muf_delete_flag
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * muf_deletion_datetime の値を返します
	 *
     * @return datetime muf_deletion_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * muf_deletion_datetime の値をセットします
	 *
	 * @param datetime $deletionDatetime muf_deletion_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * muf_registration_datetime の値を返します
	 *
     * @return datetime muf_registration_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * muf_registration_datetime の値をセットします
	 *
	 * @param datetime $registrationDatetime muf_registration_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * muf_update_datetime の値を返します
	 *
     * @return datetime muf_update_datetime
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * muf_update_datetime の値をセットします
	 *
	 * @param datetime $updateDatetime muf_update_datetime
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * muf_update_timestamp の値を返します
	 *
     * @return timestamp muf_update_timestamp
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * muf_update_timestamp の値をセットします
	 *
	 * @param timestamp $updateTimestamp muf_update_timestamp
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























