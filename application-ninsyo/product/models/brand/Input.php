<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * 指定したブランドIDの情報を入力画面にプリセットする。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Brand_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定した商品IDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $brandId = $actionForm->getBrandId();

        if (!isBlankOrNull($brandId)) {

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $brandData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstBrand', $brandId);

            $actionForm = Mikoshiva_Utility_Copy::copyPopoToActionFormProperties($actionForm, $brandData);

            if (isBlankOrNull($actionForm->getLogoUpdateDatetime())) {
                $actionForm->setLogoNoneFlag('1');
            }
            if (isBlankOrNull($actionForm->getBannerVerticalUpdateDatetime())) {
                $actionForm->setBannerVerticalNoneFlag('1');
            }
            if (isBlankOrNull($actionForm->getBannerSidewaysUpdateDatetime())) {
                $actionForm->setBannerSidewaysNoneFlag('1');
            }

        } else {
            // 新規登録時は部署のみ初期設定
            $actionForm->setDivisionId($context->getStaff()->changeDivisionId);

            $actionForm->setLogoNoneFlag('1');
            $actionForm->setBannerVerticalNoneFlag('1');
            $actionForm->setBannerSidewaysNoneFlag('1');
        }

        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }

}
