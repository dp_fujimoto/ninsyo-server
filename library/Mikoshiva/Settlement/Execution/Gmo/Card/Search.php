<?php
/** @package Mikoshiva_Settlement */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';
require_once 'Mikoshiva/Utility/Copy.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Card/Save.php';

/**
 * Mikoshiva_Settlement_Gmo_Exec_Alter_Abstract
 *
 * @package Mikoshiva_Settlement
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Search.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Card_Search extends Mikoshiva_Settlement_Execution_Gmo_Abstract {


    /**
     * 初期化処理
     *
     * @param Popo_Abstract $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Search.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function init(Mikoshiva_Settlement_Popo_Input_Gmo_Card_Search $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Popo_Input_Gmo_Card_Search */
        $inputPopo;


        // コンフィグからサイト ID とサイトパスを取得
        $siteId   = $gmoConfig['SITE_ID'];
        $sitePass = $gmoConfig['SITE_PASS'];


        // サイト ID とサイトパスをPOPO に詰め直す
        $inputPopo->setSiteID($siteId);
        $inputPopo->setSitePass($sitePass);


        // GMO 処理結果 POPO クラス
        include_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Card/Search.php';
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Card_Search();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['SEARCH_CARD_PGM'];

        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }




    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Exec/Mikoshiva_Settlement_Gmo_Exec_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/04
     * @version SVN:$Id: Search.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Exception.php';


        /* @var $_inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save */
        $_inputPopo = $inputPopo;


        // サイト ID
        if(isBlankOrNull($_inputPopo->getSiteID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイト ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        // サイトパスワード
        if(isBlankOrNull($_inputPopo->getSitePass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイトパスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        // 会員 ID
        if(isBlankOrNull($_inputPopo->getMemberID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】会員 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Card_Exception($message);
        }


        return true;
    }
}












