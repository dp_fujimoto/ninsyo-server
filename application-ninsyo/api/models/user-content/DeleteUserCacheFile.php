<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';


/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_UserContent_DeleteUserCacheFile extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        //-----------------------------------------------
        // キャッシュファイル削除
        //-----------------------------------------------
        $res_dir = opendir(USER_CONTENT_XML_FILES);
        while ($fileName = readdir($res_dir)){
            if (!is_dir(USER_CONTENT_XML_FILES . '/' . $fileName)) {
                unlink(USER_CONTENT_XML_FILES . '/' . $fileName);
            }
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }
}
