<?php
/**
 * @package Mikoshiva_Utility
 */

/**
 * require
 */
require_once 'Mikoshiva/Logger.php';
require_once 'Mikoshiva/Utility/Exception.php';


/**
 * セッションユーティリティクラス
 *
 * @package Mikoshiva_Utility
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/21
 * @version SVN:$Id: Session.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Utility_Session {

    /**
     * セッションにセットされた部門IDを返します。
     * ただし、部門変更が行われていて、かつフラグとしてtrueが渡された場合、
     * 変更後の部門IDを返します。
     *
     * @param  boolean $flagChangeDivision フラグ(default true)
     * @return int                      部門ID
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/22
     * @version SVN:$Id: Session.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public static function getNowDivisionId($changedDivisionFlag = true) {
        logInfo(LOG_START);
        $session = new Zend_Session_Namespace('staff');

        /* @var $mstDivision Popo_MstDivision */
        $mstDivision = $session->mstDivision;


        //フラグがfalseの場合、自身の所属部門のIDを返す
        if($changedDivisionFlag === false) {
            logInfo(LOG_END);
            return (int)$mstDivision->getDivisionId();
        }



        //---------------------------------------------------------------
        // フラグがtrueの場合、変更後の部門IDが存在すればそれを返す
        // それがない場合、自身の所属部門のIDを返す
        //---------------------------------------------------------------
        if(isset($session->changeDivisionId)) {
            logInfo(LOG_END);
            return (int)$session->changeDivisionId;
        }

        //部門IDがなければ空文字を返す
        logInfo(LOG_END);
        return (int)$mstDivision->getDivisionId();
    }


}












