<?php
/** @package Mikoshiva_Gmo */

/**
 * require
 */
require_once 'Mikoshiva/Settlement/Execution/Gmo/Abstract.php';
require_once 'Mikoshiva/Settlement/Popo/Output/Gmo/Member/Save.php';

/**
 * 会員の参照を行います
 *
 * @package Mikoshiva_Gmo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/02
 * @version SVN:$Id: Search.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Member_Search extends Mikoshiva_Settlement_Execution_Gmo_Abstract {




    /**
     * 初期化処理
     *
     * @param Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save $inputPopo
     * @param array $gmoConfig
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Execution/Gmo/Mikoshiva_Settlement_Execution_Gmo_Abstract#init($inputPopo, $gmoConfig)
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/19
     * @version SVN:$Id: Search.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function init(Popo_Abstract $inputPopo, array $gmoConfig) {


        // ログ開始
        logInfo(LOG_START);


        /* @var $inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Card_Save */
        $inputPopo;


        // コンフィグからサイト ID とサイトパスを取得
        $siteId   = $gmoConfig['SITE_ID'];
        $sitePass = $gmoConfig['SITE_PASS'];


        // サイト ID とサイトパスをPOPO に詰め直す
        $inputPopo->setSiteID($siteId);
        $inputPopo->setSitePass($sitePass);


        // GMO 結果 POPO クラスをセット
        $this->_outputPopo = new Mikoshiva_Settlement_Popo_Output_Gmo_Member_Save();


        // GMO 接続 URL をセット
        $this->_gmoConnectioniUrl = $gmoConfig['SAVE_MEMBER_PGM'];

        // dumper($this->_gmoConnectioniUrl);
        // dumper($inputPopo);
        // dumper($gmoConfig);
        // exit;
    }


    /**
     * 必須パラメータに値が設定されているかチェックします
     *
     * @return unknown_type
     * (non-PHPdoc)
     * @see Mikoshiva/Settlement/Gmo/Exec/Mikoshiva_Settlement_Gmo_Exec_Abstract#_checkParameter()
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/04
     * @version SVN:$Id: Search.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function _checkParameter(Popo_Abstract $inputPopo) {


        // ログ開始
        logInfo(LOG_START);

        include_once 'Mikoshiva/Settlement/Execution/Gmo/Member/Exception.php';


        /* @var $_inputPopo Mikoshiva_Settlement_Gmo_Popo_Input_Member_Save */
        $inputPopo;

        // サイト ID
        if(isBlankOrNull($inputPopo->getSiteID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイト ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Member_Exception($message);
        }


        // サイトパスワード
        if(isBlankOrNull($inputPopo->getSitePass())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】サイトパスワード は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Member_Exception($message);
        }


        // 会員 ID
        if(isBlankOrNull($inputPopo->getMemberID())) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】会員 ID は必須項目です。";
            logDebug($message);
            throw new Mikoshiva_Settlement_Execution_Gmo_Member_Exception($message);
        }


        // ログ終了
        logInfo(LOG_END);


        return true;
    }
}












