<?php
/**
 * NinsyoAPI用基底クラス
 * NinsyoAPIにアクセスする部品は必ずこのクラスを継承します。
 *
 * @author k.fujimoto
 * @since version - 2012/09/07
 */
abstract class Mikoshiva_Plugin_Ninsyo_Abstruct extends Mikoshiva_Plugin_Abstruct {
    /**
     * コンストラクタ
     *
     */
    public function __construct() {
        parent::__construct(_HTTP_ . MIKOSHIVA_DOMAIN . INSERT_USER_CONTENT_ACTION);
    }
}