<?php
/**
 * @package Mikoshiva_Session
 */

/**
 * require
 */
require_once 'Mikoshiva/Utility/String.php';

/**
 * トークン処理を行います
 *
 * @package Mikoshiva_Session
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/09
 * @version SVN:$Id: Token.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Session_Token {

    public static function createToken(Zend_View_Abstract $view) {

        // ログ開始
        logInfo(LOG_START);

        //-----------------------------------------
        // ■セッションにトークン ID をセット
        //-----------------------------------------
        $sessoin = new Zend_Session_Namespace('token');
        $sessoin->tokenId = Mikoshiva_Utility_String::createRandomString();


        //-----------------------------------------
        // ■view にトークン ID をセット
        //-----------------------------------------
        $view->assign('tokenId', $sessoin->tokenId);

        // ログ開始
        logInfo(LOG_END);
    }


    /**
     *
     *
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/20
     * @version SVN:$Id: Token.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function checkToken(Zend_Controller_Request_Abstract $req) {

        // ログ開始
        logInfo(LOG_START);


        // リクエストの値からトークンＩＤを取得
        $tokenId = $req->getParam('tokenId');
        if (! isset($tokenId)) {
            include_once 'Mikoshiva/Session/Token/Exception.php';
            throw new Mikoshiva_Session_Token_Exception('【Mikoshiva_Session_Token::checkToken】リクエストからトークンＩＤが取得出来ませんでした。');
        }

        // セッションの値からトークンＩＤを取得
        $session = new Zend_Session_Namespace('token');
        if (!isset($session->tokenId)) {
            include_once 'Mikoshiva/Session/Token/Exception.php';
            throw new Mikoshiva_Session_Token_Exception('【Mikoshiva_Session_Token::checkToken】セッションからトークンＩＤが取得出来ませんでした。');
        }


        Mikoshiva_Logger::debug("リクエストのトークンID：{$tokenId}");
        Mikoshiva_Logger::debug("セッションのトークンID：{$session->tokenId}");

        // ログ開始
        logInfo(LOG_END);
        return ($session->tokenId === $tokenId);

    }


    /**
     *
     *
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/20
     * @version SVN:$Id: Token.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public static function deleteToken() {

        // ログ開始
        logInfo(LOG_START);

        $sessoin = new Zend_Session_Namespace('token');
        unset($sessoin->tokenId);

        // ログ開始
        logInfo(LOG_END);
    }

}





