<?php

require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

/**
 * 指定したコンテンツIDの情報を入力画面にプリセットする。
 *
 * @author         K.fujimoto <fujimoto@d-publishing.jp>
 * @since        2012/06/25
 * @version        SVN: $Id:
 *
 */
class Product_Models_Content_Input
    extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * 指定したコンテンツIDの情報を入力画面にプリセットする。
     *
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        if (isBlankOrNull($actionForm->getDivisionId())) {
            $actionForm->setDivisionId($context->getStaff()->changeDivisionId);
        }

        $contentId = $actionForm->getContentId();
        $conditionNo = array();
        $productCode = array();
        $serverName = array();
        $designatedDate = array();
        $packageNumber = array();
        $productShippingCode = array();
        $permissionMonth = array();
        $permissionDay = array();
        $needPeriodMonth = array();
        $needPeriodDay = array();
        $campaignCode = array();
        $campaignSalesCode = array();
        $subscriptionType = array();
        $productType = array();
        $chargeType = array();
        $cancelOkFlag = array();
        $maxConditionNum = 0;

        // コピー機能
        $copyFlag = '0';
        $copyContentId = $actionForm->getCopyContentId();

        if (!isBlankOrNull($copyContentId)) {
            $copyFlag = '1';
            $contentId = $copyContentId;
        }

        if (!isBlankOrNull($contentId)) {

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $contentData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstContent', $contentId);

            $actionForm = Mikoshiva_Utility_Copy::copyPopoToActionFormProperties($actionForm, $contentData);

            if (isBlankOrNull($actionForm->getImageUpdateDatetime())) {
                $actionForm->setImageNoneFlag('1');
            }
            if (isBlankOrNull($actionForm->getMediaUpdateDatetime())) {
                $actionForm->setMediaNoneFlag('1');
            }

            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $productData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstProduct', $contentData->getProductId());
            $actionForm->setBrandId($productData->getBrandId());


            $db = null;
            $db = new Mikoshiva_Db_Simple();

            $brandData = $db->simpleOneSelectById('Mikoshiva_Db_NinsyoAr_MstBrand', $productData->getBrandId());
            $actionForm->setDivisionId($brandData->getDivisionId());


            //-----------------------------------------------
            // 条件取得
            //-----------------------------------------------
            // SQL作成
            $sql = "";
            $sql .= "SELECT *";
            $sql .= " FROM mst_condition";
            $sql .= " WHERE mcd_delete_flag = '0'";
            $sql .= " AND mcd_content_id = ?";

            $params = array();
            $params[] = $contentId;

            // SQL実行＋全件取得
            $res = array();
            $res = $this->_db->query($sql,$params);

            $count = 0;
            while($v = $res->fetch(Zend_Db::FETCH_ASSOC)){
                if ($v['mcd_having_flag'] === '1') {
                    $conditionNo[$count] = 1;
                } else if ($v['mcd_servise_in_flag'] === '1') {
                    $conditionNo[$count] = 2;
                } else if ($v['mcd_subscription_flag'] === '1') {
                    $conditionNo[$count] = 3;
                } else if ($v['mcd_bundle_flag'] === '1') {
                    $conditionNo[$count] = 4;
                } else if ($v['mcd_purchase_flag'] === '1') {
                    $conditionNo[$count] = 5;
                } else if ($v['mcd_shipping_data_flag'] === '1') {
                    $conditionNo[$count] = 6;
                } else if ($v['mcd_none_condition_flag'] === '1') {
                    $conditionNo[$count] = 9;
                } else {
                    $conditionNo[$count] = 0;
                }

                $productCode[$count] = $v['mcd_product_code'];
                $serverName[$count]  = $v['mcd_server_name'];
                $designatedDate[$count] = $v['mcd_designated_date'];
                $packageNumber[$count]  = $v['mcd_package_number'];
                $productShippingCode[$count]  = $v['mcd_product_shipping_code'];
                $cancelOkFlag[$count]  = $v['mcd_cancel_ok_flag'];
                $permissionMonth[$count]  = $v['mcd_permission_month'];
                $permissionDay[$count]  = $v['mcd_permission_day'];
                $needPeriodMonth[$count]  = $v['mcd_need_period_month'];
                $needPeriodDay[$count]  = $v['mcd_need_period_day'];
                $campaignCode[$count]  = $v['mcd_campaign_code'];
                $campaignSalesCode[$count]  = $v['mcd_campaign_sales_code'];
                $subscriptionType[$count]  = $v['mcd_subscription_type'];
                $productType[$count]  = $v['mcd_product_type'];
                $chargeType[$count]  = $v['mcd_charge_type'];

                if ($maxConditionNum < $count) {
                    $maxConditionNum = $count;
                }

                $count++;
            }
        } else {
            $actionForm->setImageNoneFlag('1');
            $actionForm->setMediaNoneFlag('1');
        }

        if (!isBlankOrNull($copyContentId)) {
            $actionForm->setContentId(null);
            $actionForm->getImageUpdateDatetime(null);
            $actionForm->setImageNoneFlag('1');
            $actionForm->getMediaUpdateDatetime(null);
            $actionForm->setMediaNoneFlag('1');
        }

        $actionForm->setConditionNo($conditionNo);
        $actionForm->setProductCode($productCode);
        $actionForm->setServerName($serverName);
        $actionForm->setDesignatedDate($designatedDate);
        $actionForm->setPackageNumber($packageNumber);
        $actionForm->setProductShippingCode($productShippingCode);
        $actionForm->setCancelOkFlag($cancelOkFlag);
        $actionForm->setPermissionMonth($permissionMonth);
        $actionForm->setPermissionDay($permissionDay);
        $actionForm->setNeedPeriodMonth($needPeriodMonth);
        $actionForm->setNeedPeriodDay($needPeriodDay);
        $actionForm->setCampaignCode($campaignCode);
        $actionForm->setCampaignSalesCode($campaignSalesCode);
        $actionForm->setSubscriptionType($subscriptionType);
        $actionForm->setProductType($productType);
        $actionForm->setChargeType($chargeType);
        $actionForm->setMaxConditionNum($maxConditionNum);


        logInfo('(' . __LINE__ . ')', 'return：終了');
        return null;
    }

}
