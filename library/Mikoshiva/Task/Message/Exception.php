<?php
require_once 'Mikoshiva/Exception.php';

/**
 * Mikoshivaタスク通知例外
 *
 * @package Mikoshiva_Task
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/03/15
 * @version SVN:$Id: Exception.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Task_Message_Exception extends Mikoshiva_Exception {

}