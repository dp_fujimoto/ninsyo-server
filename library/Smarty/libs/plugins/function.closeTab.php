<?php
require_once 'Mikoshiva/Logger.php';


 /**
 * 指定されたIDを持つタブを閉じるjavascriptを返します。
 *
 * @param  array   $params
 *                       string tabId 閉じるタブのタブID
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.closeTab.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_closeTab($params, &$smarty) {


    // ログ開始
    logInfo(LOG_START);


    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default          = array();
    $default['tabId'] = null;


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['tabId']) ) {
        $message = "【" . __FUNCTION__ . " （" . __LINE__ . "）tabId は必須です】";
        logDebug($message);
        throw new Mikoshiva_View_Helper_Exception($message);
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    logInfo(LOG_END);
    return "closeTab(_tabRootInfo, {$data['tabId']}, false);";
    // return "KO_closeTabAllRight({$data['tabId']});";
}


