<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * require
 */
require_once 'Mikoshiva/Validate/Ip.php';

/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Hostname.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Hostname extends Zend_Validate_Hostname
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::IP_ADDRESS_NOT_ALLOWED  => "%COLUMN_NAME%は IP アドレスのようですが IP アドレスは許されていません",
        self::UNKNOWN_TLD             => "%COLUMN_NAME%は DNS ホスト名のようですが TLD が一覧に見つかりません",
        self::INVALID_DASH            => "%COLUMN_NAME%は DNS ホスト名のようですが不適当な位置にダッシュ (-) があります",
        self::INVALID_HOSTNAME_SCHEMA => "%COLUMN_NAME%は DNS ホスト名のようですが TLD %tld%のホスト名スキーマに合いません",
        self::UNDECIPHERABLE_TLD      => "%COLUMN_NAME%は DNS ホスト名のようですが TLD 部を展開できません",
        self::INVALID_HOSTNAME        => "%COLUMN_NAME%は DNS ホスト名の構造に合いません",
        self::INVALID_LOCAL_NAME      => "%COLUMN_NAME%は有効なローカルネットワーク名ではないようです",
        self::LOCAL_NAME_NOT_ALLOWED  => "%COLUMN_NAME%はローカルネットワーク名のようですがローカルネットワーク名は許されていません"
    );

    /**
     * @param Zend_Validate_Ip $ipValidator OPTIONAL  Mikoshiva_Validate_Ip instance is used if omitted
     * @return void;
     */
    public function setIpValidator(Zend_Validate_Ip $ipValidator = null)
    {
        if ($ipValidator === null) {
            $ipValidator = new Mikoshiva_Validate_Ip();
        }
        parent::setIpValidator($ipValidator);
    }
}
