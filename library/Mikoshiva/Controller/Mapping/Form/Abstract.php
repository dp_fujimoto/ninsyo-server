<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Validate/Auto/Checker.php';

/**
 * アクションフォームの抽象クラス
 *
 * @package Mikoshiva_Controller
 * @author kawakami <kawakami@kaihatsu.com>
 * @since 2010/02/16
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 *
 */
abstract class Mikoshiva_Controller_Mapping_Form_Abstract
    implements Mikoshiva_Controller_Mapping_Form_Interface {

    /**
     * フォームのプロパティ
     * @var array
     */
    protected $_properties = array();

    /**
     * DB クラス
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;


    /**
     * コンストラクタ
     * プロパティを作成します。
     *
     * @param array $confProperties (config)
     * @param array $param (get/post)
     */
    public function __construct(array $confProperties, array $param) {

        // データベースオブジェクトを取得
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();

        // @sekiyatodo リクエスト値をアクションフォームマッピング配列に詰め替え
        $additionalProperties = array();
        $additionalProperties['tabId']      = null; // tabIdを保持します
        $additionalProperties['module']     = null; // モジュール名を保持します
        $additionalProperties['controller'] = null; // コントローラ名を保持します
        $additionalProperties['action']     = null; // アクション名を保持します

        // リクエスト値をループ
        $properties = array();
        $properties = $additionalProperties + $confProperties;

        foreach($param as $k => $v){
            // 配列の存在するキー名だけ取得
            if(array_key_exists($k, $properties)){
                $properties[$k] = $v;
            }
        }

        // クラスプロパティに代入する
        $this->_properties = $properties;
        Zend_Registry::set('actionForm', $this);
    }

    /**
     * アクションフォームの初期化処理を行います。
     * 初期化処理を行う場合は、オーバーライドして実装してください。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reset(array $config, Zend_Controller_Request_Abstract $request) {

    }

    /**
     * validation.ymlの定義に従ってバリデート処理を行います。
     * 独自バリデート処理を行う場合は、オーバーライドして実装してください。
     *
     * @param array $config
     * @param Zend_Controller_Request_Abstract $request
     * @return array $message
     */
    public function validate(array $config, Zend_Controller_Request_Abstract $request) {

        // ログスタート
        logInfo(LOG_START);

        $message = array();

        // 空の配列で有れば検証しない
        if (count($config) !== 0) {

            $v = new Mikoshiva_Validate_Auto_Checker();
            $v->setValidationTargetData($this->_properties);
            $v->setValidationDefinitiontList($config);
            $message = $v->check();
        }

        // ログエンド
        logInfo(LOG_END);

        return $message;
    }

    /**
     * アクションフォームの値を取得します。
     *
     * @param string $name
     * @return object
     */
    public function get($name) {
        if (isset($this->_properties[$name])) {
        	return $this->_properties[$name];
        } else {
        	return null;
        }
    }

    /**
     * アクションフォームに値を保存します。
     *
     * @param string $name
     * @param mix $value
     */
    public function set($name, $value) {
        if (array_key_exists($name, $this->_properties)) {
            $this->_properties[$name] = $value;
        } else {
        	throw new Mikoshiva_Exception(
        	   '【' . get_class($this) . '】プロパティ [' . $name . '] は存在しません。');
        }
    }

    /**
     * 動的なメンバ変数に対応するset/getメソッドの呼び出しに対応します。
     *
     * @param string $name
     * @param array $value
     * @return unknown_type
     */
    public function __call($name, $value) {
        // @sekiyatodo View側でget/setメソッド呼び出し時に、プロパティの値を返す。

        // get/setの接頭語 取得
        $prefix = '';
        $prefix = substr($name,0,3);

        // アクションマッピング配列のキー名
        $key = '';

        //-----------------------------------------------
        // getCustomerId / setCustomerId が呼ばれた場合
        //-----------------------------------------------
        if($prefix === 'get' || $prefix === 'set'){
            logInfo('setter/getterあり [' . $prefix . ']');

            // 接頭語のget/setをを削除
            $name = substr_replace($name,'',0,3);

            // 最初の文字 取得
            $firstWord = '';
            $firstWord = substr($name,0,1);

            // それ以降の文字列 取得
            $lastWord = '';
            $lastWord = substr($name,1);

            // キー名を作成
            $key = strtolower($firstWord).$lastWord;

        //-----------------------------------------------
        // customerId が呼ばれた場合
        //-----------------------------------------------
        } else {
             logInfo('setter/getter無し [' . $key . ']');
            // キー名にする
            $key = $name;
        }

        // プロパティが存在しない場合、例外を投げます。
        if (! array_key_exists($key, $this->_properties)) {
            logInfo('プロパティが存在しない場合、例外を投げます。');
            Mikoshiva_Logger::debug('##################################################');
            Mikoshiva_Logger::debug($key, 'key');
            Mikoshiva_Logger::debug($this->_properties, 'properties');
            Mikoshiva_Logger::debug('##################################################');

            throw new Mikoshiva_Configuration_Exception(
                '【Mikoshiva_Controller_Mapping_Form_Abstract】プロパティ[' . $key . ']が存在しません。');
        }

        // セッターゲッターの処理を行います。
        if ($prefix === 'set') {
            logInfo('Setter開始');

            // セッター
            $this->_properties[$key] = $this->getValue($value);

            logInfo('Setter終了');

        } else {
            logInfo('Getter開始');

            // ゲッター
            $result = null;
            $result = $this->_properties[$key];

            logInfo('Getter終了');
            return $result;
        }

    }

    /**
     * プロパティ名を配列で返します。
     * @param array $properties
     */
    public function getPropertyNames() {
    	return array_keys($this->_properties);
    }

   /**
     * アクションフォームの内容を配列で返します。
     *
     * @return array $properties
     */
    public function toArrayProperty() {
        return $this->_properties;
    }

   /**
     * valueが存在する場合に値を返します
     * @param array $value
     */
    private function getValue(array $values) {
        $value = null;
        if (isset($values[0])) {
            $value = $values[0];
        }
        return $value;
    }

}