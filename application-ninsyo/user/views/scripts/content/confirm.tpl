<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>

        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">ユーザーコンテンツ登録確認</h2>

            <!-- // 確認メッセージ -->
            <span>この内容で登録しますか？</span>
            <br>
            <br>

            <!-- // フォーム開始 -->
            <form name="inputForm" id="inputForm" method="post" onSubmit="return false;">

                <!-- // hidden -->
                <input type="hidden" name="tokenId" id="tokenId" value="{$tokenId}">
                {hiddenLister hiddenList=$form}

                <span class="COM_bold">【コンテンツについて】</span>
                <br>

                <table class="COM_table COM_tableWidthLong">
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            サーバー名
                        </td>
                        <td class="COM_nowrap">
                            {$form->getServerName()}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            メールアドレス
                        </td>
                        <td class="COM_nowrap">
                            {$form->getEmail()}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            パスワード
                        </td>
                        <td class="COM_nowrap">
                            {$form->getPassword()}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_bgColorLight COM_nowrap COM_tdWidthBasic">
                            登録するコンテンツ
                        </td>
                        <td class="COM_nowrap">
                            <br>
                            【選択したコンテンツ】
                            <br>
                            {if count($form->getContentList()) > 0}
                                <div style="height:300px;overflow-y:auto;">
                                    <table class="COM_table">
                                        <tr>
                                            <td class="COM_nowrap COM_bgColorDeep">ブランド名</td>
                                            <td class="COM_nowrap COM_bgColorDeep">商品名</td>
                                            <td class="COM_nowrap COM_bgColorDeep">コンテンツ名</td>
                                            <td class="COM_nowrap COM_bgColorDeep">コンテンツID</td>
                                        </tr>
                                        {foreach from=$form->getContentList() key="k" item="v"}
                                        <tr>
                                            <td class="COM_nowrap">{$v[0]}</td>
                                            <td class="COM_nowrap">{$v[1]}</td>
                                            <td class="COM_nowrap">{$v[2]}</td>
                                            <td class="COM_nowrap">{$k}</td>
                                        </tr>
                                        {/foreach}
                                    </table>
                                </div>
                            {else}
                                <p class="COM_bold COM_fontRed">有効なコンテンツIDがありませんでした</p>
                            {/if}
                            {if count($form->getFreeContentList()) > 0}
                                <hr>
                                【選択したコンテンツ以外で無条件で自動登録されるコンテンツ】
                                <br>
                                <div style="height:200px;overflow-y:auto;">
                                    <table class="COM_table" height="">
                                        <tr>
                                            <td class="COM_nowrap COM_bgColorDeep">ブランド名</td>
                                            <td class="COM_nowrap COM_bgColorDeep">商品名</td>
                                            <td class="COM_nowrap COM_bgColorDeep">コンテンツ名</td>
                                            <td class="COM_nowrap COM_bgColorDeep">コンテンツID</td>
                                        </tr>
                                        {foreach from=$form->getFreeContentList() key="k" item="v"}
                                        <tr>
                                            <td class="COM_nowrap">{$v[0]}</td>
                                            <td class="COM_nowrap">{$v[1]}</td>
                                            <td class="COM_nowrap">{$v[2]}</td>
                                            <td class="COM_nowrap">{$k}</td>
                                        </tr>
                                        {/foreach}
                                    </table>
                                </div>
                            {/if}
                        </td>
                    </tr>
                </table>

                <br>
                <hr>
                <br>


                {currentTabButton name='　　　戻る　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/user/content/confirm-back" tabId=$tabId  formId='inputForm'}
                　　　
                {currentTabButton name='　　　　　　　登録　　　　　　　' url=$smarty.const.SUB_FOLDER_PATH|cat:"/user/content/complete" tabId=$tabId  formId='inputForm'}

            </form>
            <!-- // フォーム終了 -->

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->