<!-- // メインテーブル開始 -->
<table class="COM_layautTable">

    <tr>
        <!-- // ペイン開始 -->
        <td id="COM_painSingle">

            <!-- // メインタイトル -->
            <h2 id="page_title_{$tabId}">対象ブランドのコンテンツ一覧</h2>

            ※バナー縦用=w{$smarty.const.BLAND_BANNER_V_W},h{$smarty.const.BLAND_BANNER_V_H}。
            バナー横用=w{$smarty.const.BLAND_BANNER_S_W},h{$smarty.const.BLAND_BANNER_S_H}。
            ロゴ=w{$smarty.const.BLAND_LOGO_W},h{$smarty.const.BLAND_LOGO_H}。
            商品画像=w{$smarty.const.PRODUCT_IMAGE_W},h{$smarty.const.PRODUCT_IMAGE_H}。
            コンテンツ画像=w{$smarty.const.CONTENT_IMAGE_W},h{$smarty.const.CONTENT_IMAGE_H}で表示(単位px)<br>

            {$rowCount1 = 0}
            {$rowCount2 = 0}

            {* 検索結果あり *}
            {if count($search) > 0}

                {$bannarUrl = ''}
                <table class="COM_table">
                {foreach from=$search key="k" item="v"}
                    <tr>
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">ブランド名</td>
                        <td class="COM_nowrap">{$v['info']['mbr_brand_name']}</td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">ロゴ</td>
                        <td class="COM_nowrap">
                            {if !isBlankOrNull($v['info']['mbr_logo_url'])}
                                <img src="{$v['info']['mbr_logo_url']}" width="{$smarty.const.BLAND_LOGO_W}px" height="{$smarty.const.BLAND_LOGO_H}px">
                            {else}なし
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">バナー縦用</td>
                        <td class="COM_nowrap">
                            {if !isBlankOrNull($v['info']['mbr_banner_vertical_url'])}
                                <img src="{$v['info']['mbr_banner_vertical_url']}" width="{$smarty.const.BLAND_BANNER_V_W}px" height="{$smarty.const.BLAND_BANNER_V_H}px">
                            {else}なし
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="COM_nowrap COM_bgColorDeep COM_bold">バナー横用</td>
                        <td class="COM_nowrap">
                            {if !isBlankOrNull($v['info']['mbr_banner_sideways_url'])}
                                <img src="{$v['info']['mbr_banner_sideways_url']}" width="{$smarty.const.BLAND_BANNER_S_W}px" height="{$smarty.const.BLAND_BANNER_S_H}px">
                            {else}なし
                            {/if}
                        </td>
                    </tr>
                {/foreach}
                </table>

                <br>
                <hr>
                <br>

                <input type="button" value="　　画像表示OFF　　" id="chgButton" onClick="PDT_chgImageDisplay{$tabId}();">

                <!-- // 検索結果リスト開始 -->
                <table class="COM_table PDT_tableWidthBasic">
                    <tr>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap" id="th1">商品画像</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">商品名</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap" id="th2">コンテンツ画像</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">コンテンツ名</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">用途</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">タイプ</td>
                        <td class="COM_bgColorDeep COM_bold COM_center COM_nowrap">メディアURL</td>
                    </tr>

                    {assign var="lineCount" value='0'}
                    {foreach from=$search key="k" item="v"}
                        {foreach from=$v['data'] key="k2" item="v2"}
                            {if $lineCount === '1'}
                                {assign var="lineColor" value="COM_bgColorLight"}
                                {assign var="lineCount" value='0'}
                            {else}
                                {assign var="lineColor" value=''}
                                {assign var="lineCount" value='1'}
                            {/if}
                            {$flag = 0}
                            {foreach from=$v2['data'] key="k3" item="v3"}
                                <tr>
                                    {if $flag === 0}
                                        <td class="COM_nowrap {$lineColor}" title="{$v2['info']['mpr_description']}" rowspan="{$v2['info']['rowCount']}" id="td1-{$rowCount1}">
                                            {if !isBlankOrNull($v2['info']['mpr_image_url'])}
                                                <img src="{$v2['info']['mpr_image_url']}" width="{$smarty.const.PRODUCT_IMAGE_W}px" height="{$smarty.const.PRODUCT_IMAGE_H}px">
                                            {else}なし
                                            {/if}
                                        </td>
                                        <td class="COM_nowrap {$lineColor}" title="{$v2['info']['mpr_description']}" rowspan="{$v2['info']['rowCount']}">
                                            <p class="COM_fontBlue COM_bold">{$v2['info']['mpr_product_name']}</p>
                                        </td>
                                        {$flag = 1}
                                        {$rowCount1 = $rowCount1 + 1}
                                    {/if}

                                    <td class="COM_nowrap {$lineColor}" id="td2-{$rowCount2}">
                                        {if !isBlankOrNull($v3['mct_image_url'])}
                                            <img src="{$v3['mct_image_url']}" width="{$smarty.const.CONTENT_IMAGE_W}px" height="{$smarty.const.CONTENT_IMAGE_H}px">
                                        {else}なし
                                        {/if}
                                    </td>
                                    <td class="COM_nowrap {$lineColor}" title="{$v3['mct_description']}">
                                        {$v3['mct_content_name']}
                                    </td>
                                    <td class="COM_nowrap {$lineColor}" title="{$v3['mct_description']}">
                                        {statusMapping code=$v3['mct_application_type']}
                                    </td>
                                    <td class="COM_nowrap {$lineColor}">
                                        {$v3['mct_media_type']|strtoupper}
                                    </td>
                                    <td class="COM_nowrap {$lineColor}">
                                        <a href="{$v3['mct_media_url']}" target="_blank">{$v3['mct_media_url']}</a>
                                    </td>
                                </tr>
                                {$rowCount2 = $rowCount2 + 1}
                            {/foreach}
                        {/foreach}
                    {/foreach}
                </table>
                <!-- // 検索結果リスト終了 -->

            {else}

                <p>該当する検索結果はありません。</p>

            {/if}

        </td>
        <!-- // ペイン終了 -->

    </tr>

</table>
<script type="text/javascript">COM_chgTabText('{$tabId}');</script>
<!-- // メインテーブル終了 -->
<!-- // カレントJavascript開始 -->
<script type="text/javascript">

    //-----------------------------------------------
    // 表示切替
    //-----------------------------------------------
    function PDT_chgImageDisplay{$tabId}(){

        var flag = 0;
        var obj1 = document.getElementById('th1');
        var obj2 = document.getElementById('th2');

        if (obj1) {
            if (obj1.className == 'COM_bgColorDeep COM_bold COM_center COM_nowrap COM_displayNone') {
                obj1.className = 'COM_bgColorDeep COM_bold COM_center COM_nowrap';
                obj2.className = 'COM_bgColorDeep COM_bold COM_center COM_nowrap';
                document.getElementById('chgButton').value = '　　画像表示OFF　　';
            } else {
                obj1.className = 'COM_bgColorDeep COM_bold COM_center COM_nowrap COM_displayNone';
                obj2.className = 'COM_bgColorDeep COM_bold COM_center COM_nowrap COM_displayNone';
                document.getElementById('chgButton').value = '　　画像表示ON　　';
                flag = 1;
            }
        }

        if (flag == 0) {
            for (var i = 0;i <= {$rowCount1};i++) {
                obj1 = document.getElementById('td1-' + i);
                if (obj1) {
                    if (obj1.className == 'COM_nowrap COM_bgColorLight COM_displayNone') {
                        obj1.className = 'COM_nowrap COM_bgColorLight';
                    } else {
                        obj1.className = 'COM_nowrap';
                    }
                }
            }
            for (var i = 0;i <= {$rowCount2};i++) {
                obj2 = document.getElementById('td2-' + i);
                if (obj2) {
                    if (obj2.className == 'COM_nowrap COM_bgColorLight COM_displayNone') {
                        obj2.className = 'COM_nowrap COM_bgColorLight';
                    } else {
                        obj2.className = 'COM_nowrap';
                    }
                }
            }
        } else {
            for (var i = 0;i <= {$rowCount1};i++) {
                obj1 = document.getElementById('td1-' + i);
                if (obj1) {
                    if (obj1.className == 'COM_nowrap COM_bgColorLight') {
                        obj1.className = 'COM_nowrap COM_bgColorLight COM_displayNone';
                    } else {
                        obj1.className = 'COM_nowrap COM_displayNone';
                    }
                }
            }
            for (var i = 0;i <= {$rowCount2};i++) {
                obj2 = document.getElementById('td2-' + i);
                if (obj2) {
                    if (obj2.className == 'COM_nowrap COM_bgColorLight') {
                        obj2.className = 'COM_nowrap COM_bgColorLight COM_displayNone';
                    } else {
                        obj2.className = 'COM_nowrap COM_displayNone';
                    }
                }
            }
        }
    }

</script>
<!-- // カレントJavascript終了 -->