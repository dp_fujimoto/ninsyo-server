<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 Tips - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/tips">＜＜　戻る</a></p>

            <h1>再請求</h1>


            <!-- ========================================
              ■使用例
            ==========================================-->
            <h2 id="使用例">使用例</h2>

            <div class="system-message">
              <p>
                実行クラスは以下の場合をシステムエラーとみなして例外を投げます。<br>
                <span style="color: #bb0000">※GMO における決済の失敗（エラー）は false を返します。</span>
              </p>
              <ol>
                <li>必須の値がセットされていない場合</li>
                <li>GMO 決済 API への接続に失敗した場合</li>
                <li>何らかの理由でレスポンスが取得出来なかった場合</li>
              </ol>
            </div>

            <h3>サンプルソース</h3>
            <pre class="wiki"><?php
highlight_string('<?php


        //-------------------------------------------------------
        // ▼再請求に必要な値をセットします
        //-------------------------------------------------------
        include_once \'Mikoshiva/Settlement/Popo/Input/Recharge/Run.php\';
        $rechargeInputPopo = null;
        $rechargeInputPopo = new Mikoshiva_Settlement_Popo_Input_Recharge_Run();
        $rechargeInputPopo->setCardNo(\'4111111111111126\');                        // カード番号
        $rechargeInputPopo->setExpire(\'1212\');                                    // カード有効期限
        $rechargeInputPopo->setShopID(\'testpg0000153\');                           // ショップＩＤ
        $rechargeInputPopo->setOrderID(\'DP-test173824-002-201006-44\');            // オーダーＩＤ
        $rechargeInputPopo->setAmount(\'909\');                                     // 金額


        //-------------------------------------------------------
        // ▼再請求処理を行ないます
        //-------------------------------------------------------
        include_once \'Mikoshiva/Settlement/Execution/Recharge/Run.php\';
        // インスタンスを生成
        $rechargeClass = null;
        $rechargeClass = new Mikoshiva_Settlement_Execution_Recharge_Run();

        // 実行
        $ret = null;
        $ret = $rechargeClass->execute($rechargeInputPopo); // 実行（true：成功 / false：失敗）

        // 決済結果 POPO ( Mikoshiva_Settlement_Popo_Output_Recharge_Run が返る )
        $rechargeOutputPopo = null;
        $rechargeOutputPopo = $rechargeClass->getOutput();


        //-------------------------------------------------------
        // ▼dumper で中身を確認
        //-------------------------------------------------------
        dumper($ret);
        dumper($rechargeOutputPopo);

        // 処理に失敗していたら GMO のエラーコードを日本語化します
        if ($ret === false) {
            dumper(Mikoshiva_Utility_Gmo::getErrorMessage($rechargeOutputPopo->getErrInfo()));
        }


'); ?>
            </pre>

            <h3>サンプル結果</h3>
            <pre class="wiki"><?php
highlight_string('<?php


bool(false)

object(Mikoshiva_Settlement_Popo_Output_Recharge_Run)#109 (1) {
  ["_properties:protected"] => array(2) {
    ["ErrCode"] => string(3) "E01"
    ["ErrInfo"] => string(9) "E01040010"
  }
}

string(48) "既にオーダーID が存在しています。"



'); ?>
            </pre>


            <h2>Mikoshiva_Settlement_Popo_Input_Recharge_Run（決済に必要な値を保持する POPO</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>必須</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>CardNo</td>
                <td>●</td>
                <td>string</td>
                <td>カード番号</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Expire</td>
                <td>●</td>
                <td>string</td>
                <td>カード有効期限</td>
                <td><br></td>
              </tr>
              <tr>
                <td>ShopID</td>
                <td>●</td>
                <td>string</td>
                <td>ショップ ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>OrderID</td>
                <td>●</td>
                <td>string</td>
                <td>オーダー ID</td>
                <td><br></td>
              </tr>
              <tr>
                <td>Amount</td>
                <td>●</td>
                <td>string</td>
                <td>金額</td>
                <td><br></td>
              </tr>
            </table>


            <h2>Mikoshiva_Settlement_Popo_Output_Recharge_Run（処理結果を保持する POPO）</h2>
            <h3>プロパティ</h3>
            <table class="wiki">
              <tr>
                <th>プロパティ名</th>
                <th>型</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>ErrCode</td>
                <td>string</td>
                <td>エラーコード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続さた文字列
              </td>
              </tr>
              <tr>
                <td>ErrInfo</td>
                <td>string</td>
                <td>エラー詳細コード</td>
                <td>
                  エラー発生時のみ<br>
                  複数ある場合は | で接続さた文字列
              </td>
              </tr>
            </table>



            <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>



  </div>





<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






















