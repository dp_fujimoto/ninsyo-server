<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Zend/View/Helper/FormHidden.php';
require_once 'Zend/View/Interface.php';

/**
 * 与えられた配列から hidden タグを書き出します
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/18
 * @version SVN:$Id: HiddenLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_HiddenLister extends Mikoshiva_View_Helper_Abstract
{

    public $view;


    /**
     * 与えられた配列から hidden タグを書き出します
     *
     * @param string $name       フォーム名
     * @param array  $hiddenList 値のリスト
     * @param array  $deleteList 削除する値
     * @param array  $attribs    追加項目
     * @return hidden
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2009/12/18
     * @version SVN:$Id: HiddenLister.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function hiddenLister($formName, array $hiddenList = array(), array $deleteList = array(), array $attribs = array()) {


        $newHiddenList = '';
        foreach($hiddenList as $name => $value) {

            $_name = '';

            // 配列の場合
            if (is_array($value)) {

                foreach ($value as $name2 => $value2) {

                    // 削除リストに存在していればコンテニュー
                    if (array_key_exists($name, $deleteList) && array_key_exists($name2, $deleteList[$name])) {
                        continue;
                    }

                    // name 属性を生成
                    $_name = $formName . '[' . $name .  '][' . $name2 . ']';

                    // hidden タグを生成
                    $newHiddenList .= $this->view->formHidden($_name, $value2, $attribs)
                                   . "\n";
                }

            } else {

                // 削除リストに存在していればコンテニュー
                if (array_key_exists($name, $deleteList)) {
                    continue;
                }

                // string の場合
                $_name = $formName . '[' . $name .  ']';
                $newHiddenList .= $this->view->formHidden($_name, $value, $attribs)
                               . "\n";
            }
        }

        return $newHiddenList;
    }

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function scriptPath($script)
    {
        return $this->view->getScriptPath($script);
    }
}
