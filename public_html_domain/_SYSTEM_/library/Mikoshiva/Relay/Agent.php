<?php

$scriptNameArr = explode('/',$_SERVER["SCRIPT_NAME"]);
$directoryPath = $_SERVER['DOCUMENT_ROOT'];

for ($i = 1; $i < count($scriptNameArr); $i++) {
    $directoryPath .= '/' . $scriptNameArr[$i-1];
}
$directoryPath = str_replace('//','/',$directoryPath);

require_once $directoryPath . '/_SYSTEM_/config/define.php';
require_once 'PEAR/HTTP/Client.php'; 
/**
 * ヘッダー等のHTTPな部分を隠ぺいします。
 * 
 * getHeader等あってもいいかも。
 * 
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/04/27
 * @version SVN:$Id: Agent.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
 */
class Mikoshiva_Relay_Agent {
    
    protected $_cookie;
    protected $_client;
    protected $_response;
    
    public function __construct($cookie = null) {
        $this->_client = new HTTP_Client(null, null, unserialize($cookie));
        $this->_client->getCookieManager()->serializeSessionCookies(true);
    }
    
    /**
     * postによるログインが必要な場合に使用します
     *
     * @param $loginUrl
     * @param $params   POSTするキーと値の連想配列
     * @return unknown_type
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Agent.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    public function login($loginUrl, $params) {
        $loginParams = array('next_url' => $loginUrl);
        foreach($params as $k => $v) {
            $loginParams[$k] = $v;
        }
        
        $this->_client->post($loginUrl, $loginParams); 
    }
    
    /**
     * GETリクエストでWebページを取得します
     *
     * @param $url
     * @param $params
     * @param $preEncoded
     * @param $headers
     * @return unknown_type
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Agent.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    public function getResponseByGet($url, $params = null, $preEncoded = false, $headers = array()) {
        $this->_client->get($url, $params, $preEncoded, $headers);
        
        $response = $this->_client->currentResponse(); 
        $this->_response = $response;

        $this->_cookie = serialize($this->_client->getCookieManager());
        
        return $response['body'];
        
    }
    
    /**
     * POSTリクエストでWebページを取得します
     *
     * @param $url
     * @param $params
     * @param $preEncoded
     * @param $files
     * @param $headers
     * @return unknown_type
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/27
     * @version SVN:$Id: Agent.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    public function getResponseByPost($url, $params = null, $preEncoded = false, $files = array(), $headers = array()) {
        $this->_client->post($url, $params, $preEncoded, $files, $headers);
        
        $response = $this->_client->currentResponse(); 
        $this->_response = $response;

        $this->_cookie = serialize($this->_client->getCookieManager());
        
        return $response['body'];
        
    }
    
    /**
     * Mikoshivaのクッキーを返します。
     *
     * @return unknown_type
     * 
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/05/10
     * @version SVN:$Id: Agent.php,v 1.1 2012/09/21 07:08:36 fujimoto Exp $
     */
    public function getCookie() {
        //print_r(unserialize($this->_cookie));
        return $this->_cookie;
    }
    
}


?>