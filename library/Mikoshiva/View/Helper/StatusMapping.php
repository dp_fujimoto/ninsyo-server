<?php

/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Mikoshiva/Utility/Mapping.php';
require_once 'Mikoshiva/Utility/Exception.php';
require_once 'Mikoshiva/Logger.php';

/**
 * カラム名とステータス(短縮コード)から、備考/説明を取得して返します。
 *
 * @package Mikoshiva_View
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/01/14
 * @version SVN:$Id: StatusMapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_View_Helper_StatusMapping extends Mikoshiva_View_Helper_Abstract {
    /**
     * カラム名とステータス(短縮コード)から、備考/説明を取得して返します。
     *
     * @param  string $columnName カラム名
     * @param  string $status     ステータス
     * @return string $result     備考/説明
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/01/14
     * @version SVN:$Id: StatusMapping.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    public function statusMapping($columnName, $status) {
        logInfo(LOG_START);
        $result = '';
        try{
            $result = Mikoshiva_Utility_Mapping::statusMapping($columnName, $status);
        } catch (Mikoshiva_Utility_Exception $e){
            $result = '';
        }
        logInfo(LOG_END);
        return $result;
    }
}
