<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_LessThan
 */
require_once 'Zend/Validate/LessThan.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: LessThan.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_LessThan extends Zend_Validate_LessThan
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_LESS => "%COLUMN_NAME%は %max% 未満ではありません"
    );
}
