<?php

/** @package Mikoshiva_Validate */

/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/17
 * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
abstract class Mikoshiva_Validate_Abstract extends Zend_Validate_Abstract {



    /**
     * アクションフォームをセットします
     *
     * @param Mikoshiva_Form $form
     * @return void
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/02/09
     * @version SVN:$Id: Abstract.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
     */
    protected function setForm(Mikoshiva_Form $form) {
        $this->_form = $form;
    }
}