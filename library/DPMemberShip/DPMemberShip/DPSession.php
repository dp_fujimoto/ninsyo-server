<?php
require_once('DPConfig.php');
class DPSession{
	
	function check_session(){
		// セッション継続検査
		// emailがセッションに存在すればOK
		session_start();
		if(!$_SESSION[KEY_EMAIL])
			 header("Location: ".URL_SESSION_ERROR.'?'.KEY_MESSAGE.'='.urlencode (MSG_SESSION_FAIL));
	}
	
	/**
	 * プロダクトコード権限検査
	 * 認可が内場合は、URL_SESSION_ERRORへ転送する。
	 * 認可有の場合は特になにもしない。
	 */
	function check_authz($pcode,$ym){
		$email = $_SESSION[KEY_EMAIL];
		$authfile = AUTH_FILE_SERVER_DIR."/".$pcode.$ym.'.csv' ;
		$res = false;
		$fp = fopen($authfile,"r");
		if(!$fp) throw new Exception("Can't open $authfile");
		if(!$fp) header("Location: ".URL_SESSION_ERROR);
		while(feof($fp) == false) {
		    $line = fgets($fp);
		    if(preg_match("/$email,/",$line)){
		    	$res = true;
		    	break;
		    }
		}
		fclose($fp);
		if(!$res) header("Location: ".URL_SESSION_ERROR
							.'?'.KEY_MESSAGE.'='.urlencode (MSG_AUTHZ_FAIL));
	}
}
?>