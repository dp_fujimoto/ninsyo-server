<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * mst_mail_template
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_MstMailTemplate extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'mmt';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'mailTemplateId' => null,
        'templateType' => null,
        'templateKey' => null,
        'mailType' => null,
        'fromName' => null,
        'fromEmail' => null,
        'subject' => null,
        'textBody' => null,
        'htmlBody' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * メールテンプレートID の値を返します
	 *
     * @return int メールテンプレートID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMailTemplateId() {
		return $this->_properties['mailTemplateId'];
    }


    /**
     * メールテンプレートID の値をセットします
	 *
	 * @param int $mailTemplateId メールテンプレートID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMailTemplateId($mailTemplateId) {
		$this->_properties['mailTemplateId'] = $mailTemplateId;
    }


    /**
     * テンプレートタイプ の値を返します
	 *
     * @return varchar テンプレートタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateType() {
		return $this->_properties['templateType'];
    }


    /**
     * テンプレートタイプ の値をセットします
	 *
	 * @param varchar $templateType テンプレートタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateType($templateType) {
		$this->_properties['templateType'] = $templateType;
    }


    /**
     * テンプレートキー の値を返します
	 *
     * @return varchar テンプレートキー
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTemplateKey() {
		return $this->_properties['templateKey'];
    }


    /**
     * テンプレートキー の値をセットします
	 *
	 * @param varchar $templateKey テンプレートキー
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTemplateKey($templateKey) {
		$this->_properties['templateKey'] = $templateKey;
    }


    /**
     * メールタイプ の値を返します
	 *
     * @return varchar メールタイプ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMailType() {
		return $this->_properties['mailType'];
    }


    /**
     * メールタイプ の値をセットします
	 *
	 * @param varchar $mailType メールタイプ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMailType($mailType) {
		$this->_properties['mailType'] = $mailType;
    }


    /**
     * 送信元名 の値を返します
	 *
     * @return varchar 送信元名
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromName() {
		return $this->_properties['fromName'];
    }


    /**
     * 送信元名 の値をセットします
	 *
	 * @param varchar $fromName 送信元名
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromName($fromName) {
		$this->_properties['fromName'] = $fromName;
    }


    /**
     * 送信元メールアドレス の値を返します
	 *
     * @return varchar 送信元メールアドレス
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getFromEmail() {
		return $this->_properties['fromEmail'];
    }


    /**
     * 送信元メールアドレス の値をセットします
	 *
	 * @param varchar $fromEmail 送信元メールアドレス
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setFromEmail($fromEmail) {
		$this->_properties['fromEmail'] = $fromEmail;
    }


    /**
     * タイトル の値を返します
	 *
     * @return varchar タイトル
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSubject() {
		return $this->_properties['subject'];
    }


    /**
     * タイトル の値をセットします
	 *
	 * @param varchar $subject タイトル
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSubject($subject) {
		$this->_properties['subject'] = $subject;
    }


    /**
     * テキスト本文 の値を返します
	 *
     * @return mediumtext テキスト本文
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTextBody() {
		return $this->_properties['textBody'];
    }


    /**
     * テキスト本文 の値をセットします
	 *
	 * @param mediumtext $textBody テキスト本文
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTextBody($textBody) {
		$this->_properties['textBody'] = $textBody;
    }


    /**
     * HTML本文 の値を返します
	 *
     * @return mediumtext HTML本文
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getHtmlBody() {
		return $this->_properties['htmlBody'];
    }


    /**
     * HTML本文 の値をセットします
	 *
	 * @param mediumtext $htmlBody HTML本文
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setHtmlBody($htmlBody) {
		$this->_properties['htmlBody'] = $htmlBody;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























