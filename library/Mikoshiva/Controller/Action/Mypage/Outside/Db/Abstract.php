<?php
/** @package Mikoshiva_Controller */

/**
 * require
 */
require_once 'Mikoshiva/Controller/Action/Mypage/Outside/Abstract.php';

/**
 *
 * @package Mikoshiva_Controller
 * @author admin
 *
 */
abstract class Mikoshiva_Controller_Action_Mypage_Outside_Db_Abstract extends Mikoshiva_Controller_Action_Mypage_Outside_Abstract {

    /**
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;
    /**
     * (non-PHPdoc)
     * @see library/Zend/Controller/Zend_Controller_Action#init()
     */
    public function init()
    {
        // データベースハンドルの取得
        /* @var $_db Zend_Db_Adapter_Abstract */
        $db = Zend_Db::factory('Pdo_Mysql', Array(
            'host'     => DB_PAYMENT_HOST,
            'username' => DB_PAYMENT_USER_NAME,
            'password' => DB_PAYMENT_PASSWORD,
            'dbname'   => DB_PAYMENT_DB_NAME
        ));
        $db->query('SET NAMES utf8');
        $this->_db = $db;
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
    }
}
