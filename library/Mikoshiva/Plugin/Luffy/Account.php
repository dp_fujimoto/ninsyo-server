<?php
require_once 'Mikoshiva/Plugin/Luffy/Abstruct.php';

/**
 * Luffyアカウント関連のAPIを呼び出します。
 *
 * @author k.shimowaki
 * @since version - 2011/12/16
 */
class Mikoshiva_Plugin_Luffy_Account extends Mikoshiva_Plugin_Luffy_Abstruct {


    /**
     * アカウント登録を行います。
     */
	public function createAccount(Popo_MstCustomer $customer, Popo_MstDirection $direction, Popo_MstCampaignProduct $campaignProduct, Popo_MstProductShipping $productShipping) {

        try{

            // 必須項目チェック
            if (isBlankOrNull($customer->getCustomerId()) || isBlankOrNull($direction->getDirectionId())
                || isBlankOrNull($customer->getEmail()) || isBlankOrNull($customer->getLastName())
                || isBlankOrNull($customer->getFirstName()) || isBlankOrNull($direction->getProposalDatetime())) {

                throw new Exception('必須項目が不足しています。');
            }

            // パラメータセット
            $this->client->setParameterPost('status', 'CREATE_ACCOUNT');                                        // ステータス
            $this->client->setParameterPost('customerId', $customer->getCustomerId());                          // 顧客ID
            $this->client->setParameterPost('directionId', $direction->getDirectionId());                       // 指示ID
            $this->client->setParameterPost('email', $customer->getEmail());                                    // メールアドレス
            $this->client->setParameterPost('lastName', $customer->getLastName());                              // 姓
            $this->client->setParameterPost('firstName', $customer->getFirstName());                            // 名
            $this->client->setParameterPost('proposalDatetime', $direction->getProposalDatetime());             // 申込日時

            $this->client->setParameterPost('productShippingCode', $productShipping->getProductShippingCode()); // 発送コード
            $this->client->setParameterPost('endlessFlag', $campaignProduct->getEndlessFlag());                 // 永久フラグ



			logDebug($this->client->getParameterRequest());
			logDebug($this->client->getUri());

			// 実行
			$response = $this->client->request('POST');
		} catch (Exception $e) {

			// エラー処理
			$this->error($e);
			return null;
		}
		logDebug($this->client->getLastRequest());
		logDebug($this->client->getLastResponse());

		return $this->retFormat($response->getBody());

	}

    /**
     * アカウントの取消を行います。
     */
    public function cancelAccount(Popo_MstCustomer $customer, Popo_MstDirection $direction) {

        try{

            // 必須項目チェック
            if (isBlankOrNull($customer->getCustomerId()) || isBlankOrNull($direction->getDirectionId())
                || isBlankOrNull($customer->getEmail()) || isBlankOrNull($customer->getLastName())
                || isBlankOrNull($customer->getFirstName()) || isBlankOrNull($direction->getProposalDatetime())) {

                throw new Exception('必須項目が不足しています。');
            }

            // パラメータセット
            $this->client->setParameterPost('status', 'CANCEL_ACCOUNT');                              // ステータス
            $this->client->setParameterPost('customerId', $customer->getCustomerId());                // 顧客ID
            $this->client->setParameterPost('directionId', $direction->getDirectionId());             // 指示ID
            $this->client->setParameterPost('email', $customer->getEmail());                          // メールアドレス
            $this->client->setParameterPost('lastName', $customer->getLastName());                    // 姓
            $this->client->setParameterPost('firstName', $customer->getFirstName());                  // 名
            $this->client->setParameterPost('proposalDatetime', $direction->getProposalDatetime());   // 申込日時



            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return null;
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }

    /**
     * アカウントの退会を行います。
     */
    public function withdrawalAccount(Popo_MstCustomer $customer, Popo_MstDirection $direction) {

        try{

            // 必須項目チェック
            if (isBlankOrNull($customer->getCustomerId()) || isBlankOrNull($direction->getDirectionId())
                || isBlankOrNull($customer->getEmail()) || isBlankOrNull($customer->getLastName())
                || isBlankOrNull($customer->getFirstName()) || isBlankOrNull($direction->getProposalDatetime())
                ) {

                throw new Exception('必須項目が不足しています。');
            }

            if ($direction->getProductType() === 'PRODUCT_TYPE_SINGLE' && isBlankOrNull($direction->getServiceStopDate())) {
                $now = new Mikoshiva_Date();
            	$direction->setServiceStopDate($now->toString('yyyy-MM-dd'));
            } else if ($direction->getProductType() !== 'PRODUCT_TYPE_SINGLE' && isBlankOrNull($direction->getServiceStopDate())) {
                throw new Exception('必須項目が不足しています。');
            }

            // パラメータセット
            $this->client->setParameterPost('status', 'WITHDRAWAL_ACCOUNT');                          // ステータス
            $this->client->setParameterPost('customerId', $customer->getCustomerId());                // 顧客ID
            $this->client->setParameterPost('directionId', $direction->getDirectionId());             // 指示ID
            $this->client->setParameterPost('email', $customer->getEmail());                          // メールアドレス
            $this->client->setParameterPost('lastName', $customer->getLastName());                    // 姓
            $this->client->setParameterPost('firstName', $customer->getFirstName());                  // 名
            $this->client->setParameterPost('proposalDatetime', $direction->getProposalDatetime());   // 申込日時
            $this->client->setParameterPost('serviceStopDate', $direction->getServiceStopDate());     // サービス終了日

            $cancelDate = null;
            if ($direction->getSingleCancelFlag() === '1') {
                $cancelDate = new Mikoshiva_Date($direction->getSingleCancelDatetime(),'yyyy-MM-dd');
            } else if ($direction->getSubscriptionCancelFlag() === '1') {
                $cancelDate = new Mikoshiva_Date($direction->getSubscriptionCancelDatetime(),'yyyy-MM-dd');
            }
            $this->client->setParameterPost('cancelDate', $cancelDate->toString('yyyy-MM-dd'));     // 解約日

            logDebug($this->client->getParameterRequest());
            logDebug($this->client->getUri());

            // 実行
            $response = $this->client->request('POST');
        } catch (Exception $e) {

            // エラー処理
            $this->error($e);
            return null;
        }
        logDebug($this->client->getLastRequest());
        logDebug($this->client->getLastResponse());

        return $this->retFormat($response->getBody());

    }
}