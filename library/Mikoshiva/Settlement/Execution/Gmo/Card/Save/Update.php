<?php


require_once 'Mikoshiva/Settlement/Execution/Gmo/Card/Save/Abstract.php';


/**
 * カード更新
 *
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/03/19
 * @version SVN:$Id: Update.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Settlement_Execution_Gmo_Card_Save_Update extends Mikoshiva_Settlement_Execution_Gmo_Card_Save_Abstract {


    /**
     * カード登録連番
     * @var string カード登録連番
     */
    protected $_cardSeq = '0';
}