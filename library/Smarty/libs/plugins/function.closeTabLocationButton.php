<?php
require_once 'Mikoshiva/View/Helper/Exception.php';

 /**
 * 指定されたIDを持つタブを閉じるjavascript をボタンタグに実装した形で返します。
 *
 * @param  array   $params
 *                       string name  ボタンのvalue
 *                       string tabId 閉じるタブのタブID
 * @param  Smarty  &$smarty
 * @return string
 *
 * @author M.Ikuma <ikumamasayuki@gmail.com>
 * @since 2010/02/02
 * @version SVN:$Id: function.closeTabLocationButton.php,v 1.2 2012/12/19 03:43:41 fujimoto Exp $
 */
function smarty_function_closeTabLocationButton($params, &$smarty) {


    // ログ開始
    Mikoshiva_Logger::info(LOG_START);


    //-------------------------------------------------------------------------------------------
    //■デフォルト値を定義
    //-------------------------------------------------------------------------------------------
    $default             = array();
    $default['name']     = '';      // ボタン名
    $default['saveName'] = null;    // ポインター Tab 名


    //-------------------------------------------------------------------------------------------
    //■入力値に存在しないキーをデフォルト値で置換
    //-------------------------------------------------------------------------------------------
    $data = array();
    $data = $params + $default;


    //-------------------------------------------------------------------------------------------
    //■入力値チェック
    //-------------------------------------------------------------------------------------------
    if(isBlankOrNull($data['name']) ) {
        throw new Mikoshiva_View_Helper_Exception('【' . __FUNCTION__ . '】第一引数 name は必須です。');
    }


    //-------------------------------------------------------------------------------------------
    //■処理を開始
    //-------------------------------------------------------------------------------------------
    require_once dirname(__FILE__) . '/function.closeTabLocation.php';
    $script = '';
    $script = smarty_function_closeTabLocation($data, $smarty);


    //-------------------------------------------------------------------------------------------
    //■値を取得
    //-------------------------------------------------------------------------------------------
    $_name = $data['name'];


    // ログ終了
    Mikoshiva_Logger::info(LOG_END);



    return "<input type=\"button\" value=\"{$_name}\" onclick=\"{$script}\">";
}































