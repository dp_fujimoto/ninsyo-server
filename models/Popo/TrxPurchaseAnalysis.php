<?php
/** @package Popo */

require_once 'Popo/Table/Abstract.php';


/**
 * trx_purchase_analysis
 *
 * @package Popo
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/20
 */
class Popo_TrxPurchaseAnalysis extends Popo_Table_Abstract {


    /**
    * テーブルのカラムの Prefix
    *
    * @var array
    */
    protected $_columnPrefix = 'tpa';


    /**
     *
     *
     * @var array
     */
    protected $_properties = array(
        'purchaseAnalysisId' => null,
        'purchaseId' => null,
        'salesLetterId' => null,
        'campaignSalesId' => null,
        'trafficSource1' => null,
        'trafficSource2' => null,
        'trafficSource3' => null,
        'advertisementCode' => null,
        'magazineSource' => null,
        'deleteFlag' => null,
        'deletionDatetime' => null,
        'registrationDatetime' => null,
        'updateDatetime' => null,
        'updateTimestamp' => null,
    );


    /**
     * var_export の復元用マジックメソッドです
     *
     * @param array $arr
     * @return multitype:
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/03/25
     */
    public static function __set_state(array $arr) {
        include_once 'Mikoshiva/Utility/Copy.php';
        return Mikoshiva_Utility_Copy::copyToPopo(new self(), $arr['_properties']);
    }


    /**
     * コンストラクタ
     *
     * @param $data
     * @return unknown_type
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     */
    public function __construct(array $dataList = array()) {

        // プロパティを初期化
        parent::__constract($dataList);
    }




    /**
     * 購入履歴分析ID の値を返します
	 *
     * @return int 購入履歴分析ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseAnalysisId() {
		return $this->_properties['purchaseAnalysisId'];
    }


    /**
     * 購入履歴分析ID の値をセットします
	 *
	 * @param int $purchaseAnalysisId 購入履歴分析ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseAnalysisId($purchaseAnalysisId) {
		$this->_properties['purchaseAnalysisId'] = $purchaseAnalysisId;
    }


    /**
     * 購入履歴ID の値を返します
	 *
     * @return int 購入履歴ID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getPurchaseId() {
		return $this->_properties['purchaseId'];
    }


    /**
     * 購入履歴ID の値をセットします
	 *
	 * @param int $purchaseId 購入履歴ID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setPurchaseId($purchaseId) {
		$this->_properties['purchaseId'] = $purchaseId;
    }


    /**
     * セールスレターID の値を返します
	 *
     * @return int セールスレターID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getSalesLetterId() {
		return $this->_properties['salesLetterId'];
    }


    /**
     * セールスレターID の値をセットします
	 *
	 * @param int $salesLetterId セールスレターID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setSalesLetterId($salesLetterId) {
		$this->_properties['salesLetterId'] = $salesLetterId;
    }


    /**
     * キャンペーンセールスID の値を返します
	 *
     * @return int キャンペーンセールスID
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getCampaignSalesId() {
		return $this->_properties['campaignSalesId'];
    }


    /**
     * キャンペーンセールスID の値をセットします
	 *
	 * @param int $campaignSalesId キャンペーンセールスID
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setCampaignSalesId($campaignSalesId) {
		$this->_properties['campaignSalesId'] = $campaignSalesId;
    }


    /**
     * トラフィックソース１ の値を返します
	 *
     * @return varchar トラフィックソース１
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTrafficSource1() {
		return $this->_properties['trafficSource1'];
    }


    /**
     * トラフィックソース１ の値をセットします
	 *
	 * @param varchar $trafficSource1 トラフィックソース１
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTrafficSource1($trafficSource1) {
		$this->_properties['trafficSource1'] = $trafficSource1;
    }


    /**
     * トラフィックソース２ の値を返します
	 *
     * @return varchar トラフィックソース２
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTrafficSource2() {
		return $this->_properties['trafficSource2'];
    }


    /**
     * トラフィックソース２ の値をセットします
	 *
	 * @param varchar $trafficSource2 トラフィックソース２
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTrafficSource2($trafficSource2) {
		$this->_properties['trafficSource2'] = $trafficSource2;
    }


    /**
     * トラフィックソース３ の値を返します
	 *
     * @return varchar トラフィックソース３
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getTrafficSource3() {
		return $this->_properties['trafficSource3'];
    }


    /**
     * トラフィックソース３ の値をセットします
	 *
	 * @param varchar $trafficSource3 トラフィックソース３
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setTrafficSource3($trafficSource3) {
		$this->_properties['trafficSource3'] = $trafficSource3;
    }


    /**
     * 広告コード の値を返します
	 *
     * @return varchar 広告コード
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getAdvertisementCode() {
		return $this->_properties['advertisementCode'];
    }


    /**
     * 広告コード の値をセットします
	 *
	 * @param varchar $advertisementCode 広告コード
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setAdvertisementCode($advertisementCode) {
		$this->_properties['advertisementCode'] = $advertisementCode;
    }


    /**
     * メルマガソース の値を返します
	 *
     * @return varchar メルマガソース
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getMagazineSource() {
		return $this->_properties['magazineSource'];
    }


    /**
     * メルマガソース の値をセットします
	 *
	 * @param varchar $magazineSource メルマガソース
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setMagazineSource($magazineSource) {
		$this->_properties['magazineSource'] = $magazineSource;
    }


    /**
     * 削除フラグ の値を返します
	 *
     * @return char 削除フラグ
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeleteFlag() {
		return $this->_properties['deleteFlag'];
    }


    /**
     * 削除フラグ の値をセットします
	 *
	 * @param char $deleteFlag 削除フラグ
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeleteFlag($deleteFlag) {
		$this->_properties['deleteFlag'] = $deleteFlag;
    }


    /**
     * 削除日時 の値を返します
	 *
     * @return datetime 削除日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getDeletionDatetime() {
		return $this->_properties['deletionDatetime'];
    }


    /**
     * 削除日時 の値をセットします
	 *
	 * @param datetime $deletionDatetime 削除日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setDeletionDatetime($deletionDatetime) {
		$this->_properties['deletionDatetime'] = $deletionDatetime;
    }


    /**
     * 登録日時 の値を返します
	 *
     * @return datetime 登録日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getRegistrationDatetime() {
		return $this->_properties['registrationDatetime'];
    }


    /**
     * 登録日時 の値をセットします
	 *
	 * @param datetime $registrationDatetime 登録日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setRegistrationDatetime($registrationDatetime) {
		$this->_properties['registrationDatetime'] = $registrationDatetime;
    }


    /**
     * 更新日時 の値を返します
	 *
     * @return datetime 更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateDatetime() {
		return $this->_properties['updateDatetime'];
    }


    /**
     * 更新日時 の値をセットします
	 *
	 * @param datetime $updateDatetime 更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateDatetime($updateDatetime) {
		$this->_properties['updateDatetime'] = $updateDatetime;
    }


    /**
     * システム更新日時 の値を返します
	 *
     * @return timestamp システム更新日時
     *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function getUpdateTimestamp() {
		return $this->_properties['updateTimestamp'];
    }


    /**
     * システム更新日時 の値をセットします
	 *
	 * @param timestamp $updateTimestamp システム更新日時
	 *
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/20
     *
     */
    public function setUpdateTimestamp($updateTimestamp) {
		$this->_properties['updateTimestamp'] = $updateTimestamp;
    }


}

























