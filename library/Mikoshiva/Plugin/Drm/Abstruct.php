<?php
require_once 'Mikoshiva/Plugin/Abstruct.php';
require_once 'Mikoshiva/Plugin/Drm/Output.php';

/**
 * DRMAPI用基底クラス
 * DRMAPIにアクセスする部品は必ずこのクラスを継承します。
 *
 * @author k.shimowaki
 * @since version - 2010/03/17
 */
abstract class Mikoshiva_Plugin_Drm_Abstruct extends Mikoshiva_Plugin_Abstruct {

	/**
	 * コンストラクタ
	 *
	 */
	public function __construct() {
		parent::__construct(DEF_DRM_API_DRM_SERVER_URL);
    }

    /**
     * 返却用のフォーマットに変換します。
     *
     * @param string $body
     * @return array
     *
     * @author k.shimowaki
     */
    protected function retFormat($body) {

		$out = new Mikoshiva_Plugin_Drm_Output();
		$ex = explode('||',$body);

		if (count($ex) === 1) {

			$out->setResult('error');
			$out->setErrorMessage('DRM連動システムで致命的なエラーが発生しました。');
			// システムエラーなどの際にログを書き出し（MIKOSHIVA 側のシステムエラー）
			$this->_restoreLogger();

		} else {

			$out->setResult($ex[0]);
			$out->setErrorCode($ex[1]);
			$out->setErrorMessage($ex[2]);
			$out->setExecTime($ex[3]);

			if ($ex[0] === 'error') {
				$out->setErrorMessage('DRM連動システムでエラーが発生しました。');
			    // システムエラーなどの際にログを書き出し（DRM 側のシステムエラー）
    			$this->_restoreLogger();
			}
		}
		return $out;
    }



    /**
     * リストア用のログを書き出します
     *
     * @author 谷口司
     */
    protected function _restoreLogger() {

        //------------------------------------------------------------------
        // ■復旧用のログを作成します
        //------------------------------------------------------------------
        // リクエストしようとしていた値を取得
        $parameterRequest = array();
        $parameterRequest = $this->client->getParameterRequest();

        // 値がある場合のみ処理を行います
        if ((is_array($parameterRequest) === true)             // リクエストしようとしていた値が有る
            && (isset($parameterRequest['pathCode']) === true) // pathCode が有る
        ) {
            // 処理日時・日付を追加
            $parameterRequest['RESTORE_DATE']       = Mikoshiva_Date::mikoshivaNow('yyyy-MM-dd');                      // 処理日付
            $parameterRequest['RESTORE_DATETIME']   = Mikoshiva_Date::mikoshivaNow('yyyy-MM-dd HH:mm:ss');             // 処理日時
            //$parameterRequest['REMOTE_ADDR']        = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : ''; // IP アドレス 保留（mikoshiva→123directの場合、）

            // クエリ文字列に変換
            $queryString = '';
            $queryString = http_build_query($parameterRequest);

            // ファイル名を生成
            $pathCode    = $parameterRequest['pathCode'];
            $logFileName = "restore_{$pathCode}";

            // ログを書き出し
            // mikoshiva/systems/logs/temporary/_drm-api-restore-log/restore_CUSTOMER_CREATE_2012-04-19.txt
            logTemporary($queryString, $logFileName, '_drm-api-restore-log');
        }
    }
}