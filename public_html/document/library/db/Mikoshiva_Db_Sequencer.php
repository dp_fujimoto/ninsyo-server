
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/header.php' ?>

  <div class=Section1 style='layout-grid:18.0pt'>

        <div style='border:none;border-bottom:solid #365F91 1.5pt;padding:0mm 0mm 1.0pt 0mm'>
          <h1><span style='font-family:"ＭＳ ゴシック"'>Mikoshiva ドキュメント一覧 &gt; ライブラリ（共通部品） ドキュメント &gt; DB 関連 - <?php echo basename(__FILE__, '.php') ?></span></h1>
        </div>

          <p style="text-align: right; padding-right: 2em;">更新日時 <?php echo date( 'Y/m/d (D) H:i:s', filemtime(__FILE__) ) ?></p>
          <p style="text-align: left padding-right: 2em;"><a href="/document/library">＜＜　戻る</a></p>

            <h1 id="<?php echo basename(__FILE__, '.php') ?> クラス"><?php echo basename(__FILE__, '.php') ?> クラス</h1>

            <ol>
              <li><a href="#nextSequenceId">Mikoshiva_Db_Sequencer#nextSequenceId - シーケンステーブルから第一引数に指定されたシーケンスの番号をインクリメントして取得します。</a></li>
             <li><a href="#closeConnection">Mikoshiva_Db_Sequencer#closeConnection - データベースに接続中の場合コネクションをクローズします。</a></li>
            </ol>




            <!-- ==============================================
            ■Mikoshiva_Db_Sequencer#nextSequenceId
            ============================================== -->
            <h2 id="Mikoshiva_Db_Sequencer#nextSequenceId"><a name="nextSequenceId">Mikoshiva_Db_Sequencer#nextSequenceId</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              シーケンステーブルから第一引数に指定されたシーケンスの番号を<br>
              インクリメントして取得します。
            </p>
            <p>
              インスタンス生成時に専用のコネクションを取得するため、<br>
              他のコネクションに対するトランザクションに影響を与えることはありません。<br>
              <br>
              ただし、シーケンス番号取得後は明示的に「closeConnection()」メソッドを呼び出してください。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>1</td>
                <td>$sequenceName</td>
                <td>string</td>
                <td><br></td>
                <td>シーケンス名</td>
                <td>
                  ここで指定されたシーケンス名に紐付くシーケンス番号を<br>
                  シーケンステーブルよりインクリメントして取得します。
                </td>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>int</td>
                <td><br></td>
                <td>インクリメントされたシーケンス番号</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Sequencer.php\';

// シーケンステーブルから問い合わせIDを取得します。
$id  = 0;
$seq = null;
$seq = new Mikoshiva_Db_Sequencer();
$id  = $seq->nextSequenceId(\'tid_inquiry_id\');      // シーケンス名指定して実行
$seq->closeConnection();                            // コネクションクローズ

dumper($id);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
int(4)
</pre>




            <!-- ==============================================
            ■Mikoshiva_Db_Sequencer#closeConnection
            ============================================== -->
            <h2 id="Mikoshiva_Db_Sequencer#closeConnection"><a name="closeConnection">Mikoshiva_Db_Sequencer#closeConnection</a>&emsp;&emsp;<a href="#">▲ TOP ▲</a></h2>

            <h3>プロパティ</h3>
            <p>
              データベースに接続中の場合コネクションをクローズします。
            </p>
            <p>
              コネクションがクローズされている場合は処理を行いません。
            </p>

            <h4>引数</h4>
            <table class="wiki">
              <tr>
                <th>引数番号</th>
                <th>引数名</th>
                <th>型</th>
                <th>デフォルト値</th>
                <th>意味</th>
                <th>説明</th>
              </tr>
              <tr>
                <td>戻り値</td>
                <td><br></td>
                <td>boolean</td>
                <td><br></td>
                <td>クローズ処理を行った場合はtrue, <br></>行わなかった場合(切断済)はfalseが返ります。</td>
              </tr>
            </table>


            <h3>使用例</h3>
            <pre class="wiki">
<?php
highlight_string('<?php

include_once \'Mikoshiva/Db/Sequencer.php\';

// シーケンステーブルから問い合わせIDを取得します。
$bool = false;
$seq = null;
$seq = new Mikoshiva_Db_Sequencer();
$seq->nextSequenceId(\'tid_inquiry_id\');             // シーケンス名指定して実行
$bool = $seq->closeConnection();                    // コネクションクローズ

dumper($bool);
');
?>
            </pre>


            <h3>結果</h3>
<pre class="wiki">
bool(true)
</pre>








  </div>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/copy.php' ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/document/_parts/footer.php' ?>






























