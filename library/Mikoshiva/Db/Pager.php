<?php
/**
 * @package Mikoshiva_Db_Pager
 */

/**
 *
 */
require_once 'Mikoshiva/Db/Pager/Exception.php';
require_once 'Smarty/libs/plugins/function.currentTabLink.php';
require_once 'Smarty/libs/plugins/function.hiddenLister.php';

/**
 * ページャークラス
 *
 * @package Mikoshiva_Db_Pager
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2009/12/10
 * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
 */
class Mikoshiva_Db_Pager {


    /**
     * dbのインスタンスを保持します
     * @var unknown_type
     */
    private $_db;

    /**
     * pagerBar(各ページへのリンク等を含むhtmlの文字列) を保持します
     * @var string pagerBar(各ページへのリンク等を含むhtmlの文字列) を保持します
     */
    private $_pagerBar;

    /**
     * 合計ヒット件数 を保持します
     * @var int 合計ヒット件数 を保持します
     */
    protected $_totalCount;


    /**
     * コンストラクタ
     *
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/26
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function __construct() {
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
    }

    /**
     * pagerBar(各ページへのリンク等を含むhtmlの文字列) を返します
     *
     * @return string $this->_pagerBar pagerBar(各ページへのリンク等を含むhtmlの文字列)
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/16
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getPagerBar() {
        logInfo(LOG_START);
        return $this->_pagerBar;
        logInfo(LOG_END);
    }

    /**
     * 合計ヒット件数 を返します
     *
     * @return int $this->_totalCount 合計ヒット件数
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/22
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function getTotalCount() {
        logInfo(LOG_START);
        return $this->_totalCount;
        logInfo(LOG_END);
    }




    /**
     * 与えられた引数を元にリミットをかけてクエリを実行します
     *
     *
     * @param Mikoshiva_Controller_Mapping_Form_Pager_Abstract $actionForm
     * @param integer                                          $perPage
     * @param string                                           $sql
     * @param array                                            $params
     * @param $fetchMode
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/16
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function fetchAllLimit(
                                  Mikoshiva_Controller_Mapping_Form_Pager_Abstract $actionForm,
                                  $perPage,
                                  $sql,
                                  $params = array(),
                                  $fetchMode = null) {

        $db = $this->_db;
        if(isBlankOrNull($actionForm->getPagerCurrentPage())) {
            $actionForm->setPagerCurrentPage('1');
        }
        if(isBlankOrNull($perPage)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】与えられた引数に 1ページ毎の件数 が設定されていません";
            logDebug($message);
            throw new Mikoshiva_Db_Pager_Exception($message);
        }
        if(preg_match('/where.* limit [^)]*$/i', $sql) > 0) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】limit句を含んだSQLには対応していません";
            logDebug($message);
            throw new Mikoshiva_Db_Pager_Exception($message);
        }


        // 全体件数取得用のSQLを作成
        $sqlCount = '';
        $sqlCount = preg_replace('/^.*?select.*? from/i', 'SELECT count(*) as cnt FROM', $sql );

        $work = array();
        $work = $db->fetchAll($sqlCount, $params, $fetchMode);

        // 全体で何件あるか取得、アクションフォームにセット
        $totalCount = '';
        $totalCount = $work[0]['cnt'];
        $actionForm->setPagerTotalCount($totalCount);
        $this->_totalCount = $totalCount;


        // 現在のページ番号から、次のselect時に「何件目から先のデータを、1ページ分読み込むか」を計算する
        //   例えば、現在が2ページ目で、1ページごとに20件表示する場合、
        //   from は41件目から先なので、fromは40(1件目=0)になる
        $from = '';
        $from = $perPage * $actionForm->getPagerCurrentPage() - $perPage;

        // sqlにlimit句を追加
        $sqlLimit = '';
        $sqlLimit = $sql . ' limit ' . $from . ',' . $perPage;

        // select 実行
        $result = array();
        $result = $db->fetchAll($sqlLimit, $params, $fetchMode);

        // ページャーバー(ビューで表示する、 ...345678... のようなリンク)をアクションフォームに追加
        $this->_pagerBar = $this->_makePagerBar($actionForm, $perPage, $totalCount);
        $actionForm->setPagerBar($this->_pagerBar);

        return $result;
    }




    /**
     * 与えられた引数を元にリミットをかけてクエリを実行します
     *
     *
     * @param Mikoshiva_Controller_Mapping_Form_Pager_Abstract $actionForm
     * @param integer                                          $perPage
     * @param string                                           $sql
     * @param array                                            $params
     * @param $fetchMode
     * @return Zend_Db_Statement_Interface
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/16
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    public function fetchAllLimitStatement(
                                  Mikoshiva_Controller_Mapping_Form_Pager_Abstract $actionForm,
                                  $perPage,
                                  $sql,
                                  $params = array()) {

        $db = $this->_db;
        if(isBlankOrNull($actionForm->getPagerCurrentPage())) {
            $actionForm->setPagerCurrentPage('1');
        }
        if(isBlankOrNull($perPage)) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】与えられた引数に 1ページ毎の件数 が設定されていません";
            logDebug($message);
            throw new Mikoshiva_Db_Pager_Exception($message);
        }
        if(preg_match('/where.* limit [^)]*$/i', $sql) > 0) {
            $message = "【" . __CLASS__ . "#" . __FUNCTION__ . " （" . __LINE__ . "）】limit句を含んだSQLには対応していません";
            logDebug($message);
            throw new Mikoshiva_Db_Pager_Exception($message);
        }


        $work = array();
        $work = $db->query($sql, $params);

        // 全体で何件あるか取得、アクションフォームにセット
        $totalCount = $work->rowCount();
        $work = null;
        $actionForm->setPagerTotalCount($totalCount);
        $this->_totalCount = $totalCount;


        // 現在のページ番号から、次のselect時に「何件目から先のデータを、1ページ分読み込むか」を計算する
        //   例えば、現在が2ページ目で、1ページごとに20件表示する場合、
        //   from は41件目から先なので、fromは40(1件目=0)になる
        $from = '';
        $from = $perPage * $actionForm->getPagerCurrentPage() - $perPage;

        // sqlにlimit句を追加
        $sqlLimit = '';
        $sqlLimit = $sql . ' limit ' . $from . ',' . $perPage;

        // select 実行
        $result = array();
        $result = $db->query($sqlLimit, $params);

        // ページャーバー(ビューで表示する、 ...345678... のようなリンク)をアクションフォームに追加
        $this->_pagerBar = $this->_makePagerBar($actionForm, $perPage, $totalCount);
        $actionForm->setPagerBar($this->_pagerBar);

        return $result;
    }


    /**
     * pagerBar(各ページへのリンク等を含むhtmlの文字列)を作成します。
     *
     * @param  Mikoshiva_Controller_Mapping_Form_Pager $actionForm
     * @return string
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/16
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    private function _makePagerBar($actionForm, $perPage, $totalCount) {
        logInfo(LOG_START);

        //$perPage     = (int)$actionForm->getPagerPerPage();
        $perPage     = (int)$perPage;
        $currentPage = (int)$actionForm->getPagerCurrentPage();
        $tabId       = $actionForm->getTabId();
        $totalCountPage   = (int)ceil($totalCount / $perPage);

        // 検索条件を取得
        $searchString = '';
        //$searchString = $this->_makeSearchString($actionForm);

        // ページ番号以外の、リクエスト先URLを作成
        $baseUrl      = '';
        $baseUrl      =  '/' . $actionForm->getModule()
                       . '/' . $actionForm->getController()
                       . '/' . $actionForm->getAction()
                       //. $searchString
                       . '/pagerPerPage/' . $perPage
                       . '/pagerCurrentPage/';


        $page         ='';
        //--------------------------------------------------------------------
        // ■ カレントページより前のページへのリンクを生成
        //--------------------------------------------------------------------
        $minPageLink            = ''; // 1ページ目へのリンク
        $previousPageLink       = ''; // 前ページへのリンク
        $previousPageNumberLink = '';       // 1..45678のような各ページへのリンク
        if($currentPage !== 1) {
            // << FirstPage
            // ------------------------------------------------------------------------
            $page = 1;
            $name = '最初のページ'; // リンクテキスト
            $minPageLink = $this->_makeTabLink($name, $baseUrl . $page, $tabId);

            // <  PreviousPage
            // ------------------------------------------------------------------------
            $page = $currentPage - 1;
            $name = ' ＜ '; // リンクテキスト
            $previousPageLink = $this->_makeTabLink($name, $baseUrl . $page, $tabId);

            // カレントページが6を超える時は、ナビゲーション文字列に ... を追加   ...345  のように使う ...
            // ------------------------------------------------------------------------
            $count = 0;
            $work = '';
            while($currentPage - $count > 1 && $count < 5) {
                $count++;
                $page = $currentPage - $count;
                $work = $this->_makeTabLink($page, $baseUrl . $page, $tabId) . ' ' .$work;
            }
            if($currentPage > 6) {
                $work = '...' . $work;
            }
            $previousPageNumberLink .= $work;
        }

        //--------------------------------------------------------------------
        // ■ カレントページより後のページへのリンクを生成
        //--------------------------------------------------------------------
        $maxPageLink        = ''; // 1ページ目へのリンク
        $nextPageLink       = ''; // 前ページへのリンク
        $nextPageNumberLink = '';       // 1..45678のような各ページへのリンク

        if($currentPage !== $totalCountPage) {
            // >  nextPage
            // ------------------------------------------------------------------------
            $page = $currentPage + 1;
            if($page > $totalCountPage) {
                $page = $totalCountPage;
            }
            $name = ' ＞ '; // リンクテキスト
            $nextPageLink = $this->_makeTabLink($name, $baseUrl . $page, $tabId);

            // >> lastPage
            // ------------------------------------------------------------------------
            $page = $totalCountPage;
            $name = '最後のページ'; // リンクテキスト
            $maxPageLink = $this->_makeTabLink($name, $baseUrl . $page, $tabId);

            // カレントページが6を超える時は、ナビゲーション文字列に ... を追加   123...  のように使う ...
            // ------------------------------------------------------------------------
            $count = 0;
            $work = '';
            while($currentPage + $count < $totalCountPage && $count < 5) {
                $count++;
                $page = $currentPage + $count;
                if($page < 1) {
                    break;
                }
                $work .= ' ' . $this->_makeTabLink($page, $baseUrl . $page, $tabId);
            }
            if($totalCountPage - $currentPage -$count > 1) {
                $work .= '...';
            }
            $nextPageNumberLink .= $work;
        }


        //------------------------------------------------------------------
        // ■ページャーの HTML を作成
        //------------------------------------------------------------------
        // 現在日時を取得
        $now = Mikoshiva_Date::mikoshivaNow('yyyyMMddHHmmss');
        // hiddenLister
        $hiddenLister = smarty_function_hiddenLister(
                array('hiddenList' => $actionForm),
                new stdClass,
                new stdClass
        );
        // HTML
        $result  = '';
        $result .= '<form name="pagerForm_' . $now . '" id="pagerForm' . $now . '" method="post" onsubmit="return false; style="display: inline;">' . "\n";
        $result .= $hiddenLister . "\n";
        $result .= $minPageLink . $previousPageLink . $previousPageNumberLink . $currentPage . $nextPageNumberLink . $nextPageLink . $maxPageLink . '<br>' . "\n";
        $result .= $currentPage . ' / ' . $totalCountPage  . 'ページ<br>' . "\n";
        $result .= '</form>';
        return $result;
    }

    /**
     * Mikoshivaのタブ機能に特化したリンク文字列を作成して返します(ヘルパーのcurrentTabLink相当)。
     *
     * @param $name
     * @param $url
     * @param $tabId
     * @param $formId
     * @param $script
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/16
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    private function _makeTabLink($name, $url, $tabId, $formId = 'null', $script = '') {

        /*
        $result  = '';
        $result .= '<a href="#" onclick="KO_currentTab(';
        $result .= '\'' . $url . '\', ';
        $result .= '\'' . $tabId . '\', ';
        $result .= '\'' . $formId . '\', ';
        $result .=  $script . '[]';
        $result .= ');">';
        $result .= $name . '</a>' . "\n";
        */
        $result  = '';
        $default           = array();
        $default['name']   = $name;
        $default['url']    = $url;
        $default['tabId']  = $tabId;
        $default['formId'] = 'pagerForm' . Mikoshiva_Date::mikoshivaNow('yyyyMMddHHmmss');
        $result .= smarty_function_currentTabLink($default, new stdClass);

        return $result;
    }

    /**
     * 与えられたアクションフォームから、検索条件を取得し、urlとして出力します。
     *
     * module, controller, action, 及びページャー関係のプロパティを除いた、すべてのプロパティを
     * urlencodeして返します
     *
     * @param $actionForm
     * @return unknown_type
     *
     * @author M.Ikuma <ikumamasayuki@gmail.com>
     * @since 2010/04/16
     * @version SVN:$Id: Pager.php,v 1.2 2012/12/19 03:43:23 fujimoto Exp $
     */
    private function _makeSearchString($actionForm) {
        $source = $actionForm->toArrayProperty();
        unset($source['model']);
        unset($source['controller']);
        unset($source['action']);
        unset($source['tabId']);
        unset($source['pagerPerPage']);
        unset($source['pagerCurrentPage']);
        unset($source['pagerBar']);
        $result = '';
        foreach($source as $k => $v) {
            $result .= '/' . $k;
            $result .= '/' . urlencode($v);
        }
        return $result;
    }
}
