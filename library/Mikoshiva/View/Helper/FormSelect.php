<?php
/** @package Mikoshiva_View */

/**
 * require
 */
require_once 'Mikoshiva/View/Helper/Abstract.php';
require_once 'Zend/View/Helper/FormSelect.php';
require_once 'Mikoshiva/Logger.php';


/**
 * formSelect のラップクラス
 *
 * @package Mikoshiva_View
 * @author T.Tsukasa <taniguchi@kaihatsu.com>
 * @since 2010/01/17
 * @version SVN:$Id: FormSelect.php,v 1.1 2012/09/21 07:08:06 fujimoto Exp $
 */
class Mikoshiva_View_Helper_FormSelect extends Zend_View_Helper_FormSelect
{
    /**
     * formSelect のラップクラス
     *
     * @param string  $name       name 属性
     * @param string  $value      selected
     * @param string  $attribs    追加項目
     * @param array   $options    options
     * @param boolean $addEmpty   true:先頭行に空項目を作成
     * @param boolean $addAllItem true:【全て】の項目を追加
     * @param string  $listsep    区切文字
     * @return unknown_type
     * (non-PHPdoc)
     * @see Zend/View/Helper/Zend_View_Helper_FormSelect#formSelect($name, $value, $attribs, $options, $listsep)
     * @author T.Tsukasa <taniguchi@kaihatsu.com>
     * @since 2010/01/17
     * @version SVN:$Id: FormSelect.php,v 1.1 2012/09/21 07:08:06 fujimoto Exp $
     */
    public function formSelect($name,
                               $value      = null,
                               $attribs    = null,
                               $options    = null,
                               $addEmpty   = false,
                               $addAllItem = false,
                               $listsep    = "<br>\n") {

        // ログスタート
        logInfo(LOG_START);


        if($addEmpty === true || $addAllItem === true) {

            $addArray = array();
            if ($addEmpty)   $addArray['']  = '';
            if ($addAllItem) $addArray['0'] = '全て';

            if (is_array($options)) $options = $addArray + $options;
            else                    $options = $addArray;

        }


        // ログ終了
        logInfo(LOG_END);

        return parent::formSelect($name, $value, $attribs, $options, $listsep);
    }


    /**
     * Builds the actual <option> tag
     *
     * @param string $value Options Value
     * @param string $label Options Label
     * @param array  $selected The option value(s) to mark as 'selected'
     * @param array|bool $disable Whether the select is disabled, or individual options are
     * @return string Option Tag XHTML
     */
    protected function _build($value, $label, $selected, $disable)
    {

        //--------------------------------------------------------------------
        // Zend_View_Helper_FormSelect のコピーです！！！
        // selected の in_array 部分が型チェックを行っていないためコピーしました
        //--------------------------------------------------------------------
        if (is_bool($disable)) {
            $disable = array();
        }

        $opt = '<option'
             . ' value="' . $this->view->escape($value) . '"'
             . ' label="' . $this->view->escape($label) . '"';
        // selected?
        if (in_array((string) $value, $selected, true)) {
            $opt .= ' selected="selected"';
        }

        // disabled?
        if (in_array($value, $disable)) {
            $opt .= ' disabled="disabled"';
        }

        $opt .= '>' . $this->view->escape($label) . "</option>";

        return $opt;
    }
}
