<?php

/**
 * 業務クラス
 * 他の業務から呼ばれるクラス
 *
 * 【ファンクション集】
 *
 * @author kawakami
 *
 */
class Tool_Models_Operation {

    /**
    * アクションログを保存する
    *
    */
    function insertActionLog($text){

        require_once 'tool/models/classes/InsertActionLog.php';

        $class = null;
        $class = new Tool_Models_Classes_InsertActionLog();

        $res = $class->execute($text);

        return $res;

    }
}