<?php
/**
 * @package Mikoshiva_Validate
 */

/**
 * @see Zend_Validate_Between
 */
require_once 'Zend/Validate/Between.php';


/**
 * 
 * 
 * @package Mikoshiva_Validate
 * @author admin
 * @since 2010/02/10
 * @version SVN:$Id: Between.php,v 1.2 2012/12/19 03:43:22 fujimoto Exp $
 */
class Mikoshiva_Validate_Between extends Zend_Validate_Between
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_BETWEEN        => "%COLUMN_NAME%は%min%以上%max%以下ではありません",
        self::NOT_BETWEEN_STRICT => "%COLUMN_NAME%は%min%以下か%max%以上です"
    );
}
