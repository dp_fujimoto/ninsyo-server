<?php
require_once 'Mikoshiva/Controller/Model/Db/Abstract.php';
require_once 'Mikoshiva/Controller/Mapping/Form/Interface.php';
require_once 'Mikoshiva/Controller/Mapping/ActionContext.php';

require_once 'api/models/Operation.php';
require_once 'api/models/user-content/GetEdgeTradeItem.php';
require_once 'api/models/user-content/GetRegistrationDate6.php';

/**
 * モデルクラス
 * @author fujimoto
 *
 */
class Api_Models_Content_GetConditionProductCode extends Mikoshiva_Controller_Model_Db_Abstract {

    /**
     * execute
     * @see application/models/Model/Model_Abstract#execute($actionForm, $context)
     * @param Mikoshiva_Controller_Mapping_Form_Interface $actionForm
     * @param Mikoshiva_Controller_Mapping_ActionContext $context
     * @return object
     */
    public function execute(Mikoshiva_Controller_Mapping_Form_Interface $actionForm,
                                Mikoshiva_Controller_Mapping_ActionContext $context) {
        logInfo('(' . __LINE__ . ')', '開始');

        $productCode = $actionForm->getProductCode();
        $nowDateStrtotime = strtotime(Mikoshiva_Date::mikoshivaNow('yyyy-MM-dd'));

        //-----------------------------------------------
        // コンテンツ条件情報取得
        //-----------------------------------------------
        // SQL作成
        $sql = "";
        $sql .= "SELECT mdi_division_name, mcd_product_code";
        $sql .= " FROM mst_condition";
        $sql .= " INNER JOIN mst_content ON mcd_content_id = mct_content_id";
        $sql .= " INNER JOIN mst_product ON mct_product_id = mpr_product_id";
        $sql .= " INNER JOIN mst_brand ON mpr_brand_id = mbr_brand_id";
        $sql .= " INNER JOIN mst_division ON mbr_division_id = mdi_division_id";
        $sql .= " WHERE 1 = 1";
        $sql .= "   AND mcd_delete_flag = '0'";
        $sql .= "   AND mct_delete_flag = '0'";
        $sql .= "   AND mpr_delete_flag = '0'";
        $sql .= "   AND mbr_delete_flag = '0'";
        $sql .= "   AND mct_application_type = ?";
        $sql .= "   AND mcd_server_name = ?";
        $sql .= " GROUP BY mdi_division_name, mcd_product_code";

        $params   = array();
        $params[] = 'APPLICATION_TYPE_WEB_PAGE_AUTH';
        $params[] = $actionForm->getServerName();

        // SQL実行＋全件取得
        $res = array();
        $res = $this->_db->query($sql,$params);

        $data = array();
        while($row = $res->fetch(Zend_Db::FETCH_ASSOC)){
            $data[] = $row;
        }

        // クラス配列を返す
        logInfo('(' . __LINE__ . ')', 'return：終了');
        return json_encode($data);
    }
}
